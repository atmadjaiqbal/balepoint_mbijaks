<?php defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public function __construct()
	{
		  parent::__construct();
			date_default_timezone_set('Asia/Jakarta');
	}
}

class Application extends MX_Controller
{
	public $data;
	public $member       = FALSE;
	public $current_url  = '';


	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');

		$this->load->helper('url');
		$this->load->helper('bijak');

		$this->load->library('core/bijak');

		$this->initApplication();
	}


	public function initApplication()
	{
		$this->load->library('session');

		$baseURL					= base_url();
		$this->current_url   = $baseURL.$this->uri->uri_string() .'/?'.$_SERVER['QUERY_STRING'];
		$this->member        = $this->session->userdata('member');

		$showHeader = TRUE;
		$meta       = array(
			array('name'   => 'description', 'value' => ' bijak indonesia politik social network'),
			array('name'   => 'keyword', 'value' => ' bijak indonesia politik social network')
		);


		// Initilize Global / Default Params
		$this->data = array(
			'title'           => 'Bijaks | Life & Politics',
			'base_url'        => $baseURL,
			'current_url'     => $this->current_url,
			'member' 	      => $this->member,
			'meta'            => $meta,
			'show_header'     => $showHeader,
			'template_path'   => 'template/'
		);


		// Do not cache, if cache_header set to FALSE
		//$this->cacheHeaderDisabled();
	}


	public function authorize()
	{
		if(!$this->member)
		{
			redirect(base_url().'home/login/?redirect='.$this->current_url);
		}
	}


	public  function cacheHeaderDisabled($is_cache=FALSE)
	{
		if (!$is_cache)
		{
			$this->output->set_header("HTTP/1.0 200 OK");
			$this->output->set_header("HTTP/1.1 200 OK");
			$this->output->set_header("Expires: 0");
			$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
			$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
			$this->output->set_header("Pragma: no-cache");
		}
	}


	public  function debug($data, $is_vardump = false, $is_exit = false)
	{
		echo '<pre style="border:1px solid red; margin-top:50px; padding: 10px; font-size: 11px; background: #efefef; white-space: pre-wrap;">';
		if(!$is_vardump)
			print_r($data);
		else
			var_dump($data);
		echo '</pre>';

		if($is_exit)  exit;
	}


	public  function setOutput($data, $type='json')
	{
		if ($type == 'json') {
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data));
			//exit();
		}
	}

	public  function getClasssname()
	{
		$return = strtolower(get_class($this));
		return $return;
	}

}

/* End of file file.php */