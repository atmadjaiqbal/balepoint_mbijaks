<!DOCTYPE html>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Bijaks Life and Politics" />
    <meta name="keywords" content="life & politics, hukum, parlemen, presiden, politik, skandal, survey, politisi, komunitas, partai, pemilu, bijaks" />
    <meta charset="utf-8" />
    <meta name="author" content="Sunbird" />
    <meta name="bijaks-site-verification" content="" />
    <?php if(!isset($_GET['norefresh'])){ ?>
    <meta HTTP-EQUIV="refresh" CONTENT="600"/>
    <?php } ?>

    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.politik.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.aktor.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.profile.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.suksesi.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bijaks.komunitas.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>
<!--    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" media="screen"/> -->
<!--    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" media="screen"/> -->
    <!-- link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/slideshow.css" /-->
    <link rel='stylesheet' type="text/css" href="<?php echo base_url(); ?>assets/css/powermap.css">    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/plugins/datepicker/datepicker.css" />

    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/lazyload/jquery.lazyload.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/lazyload/jquery.scrollstop.js"></script>

    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.easing.1.3.min.js"></script>
    <!--script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/slides.min.jquery.js"></script-->
    <!--script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.metadata.js"></script-->
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.labelify.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>        
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/plugins/datepicker/bootstrap-datepicker.js"></script>

    <!-- Jquery Validation -->
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validation/jquery.validationEngine.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validation/jquery.validationEngine-en.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/jquery.validation/validationEngine.jquery.css" type="text/css" />

    <!-- alertify -->
    <script src="<?php echo base_url(); ?>assets/plugins/alertify/alertify.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/alertify/themes/alertify.core.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/alertify/themes/alertify.default.css" id="toggleCSS" />

    <?php if (!empty($scripts)) : ?>
        <?php foreach ($scripts as $script) : ?>
            <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>


    <script type="text/javascript">
        var Settings = <?php
	     $settings = array(
	      //       'accountID' => @$member['account_id'],
	             'base_url' => base_url()
	      //       'count_content_action_url' => base_url('ajax/count_content_action'),
	       //      'count_content_url' => base_url('ajax/count_content_result'),
	       //      'count_content_callback' => base_url('ajax/count_content'),
	       //      'count_page_url' => base_url('ajax/count_page_result'),
	      //       'count_page_callback' => base_url('ajax/count_page'),
	      //       'like_uri' => base_url('ajax/like'),
	     //        'unlike_uri' => base_url('ajax/unlike'),
	      //       'dislike_uri' => base_url('ajax/dislike'),
	       //      'undislike_uri' => base_url('ajax/undislike'),
	       //      'like_page_uri' => base_url('ajax/like_page'),
	       //      'dislike_page_uri' => base_url('ajax/dislike_page'),
	       //      'follow_page_uri' => base_url('ajax/follow_page')
	     );
	     echo json_encode($settings);
	     ?>;

    </script>

    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script type="text/javascript">if(jQuery(window).width()>1024){document.write("<"+"script src='<?php echo base_url(); ?>assets/js/jquery.preloader.js'></"+"script>");}	</script>
    <script type="text/javascript" language="javascript">
   /*     jQuery(window).load(function() {
            $x = $(window).width();
            if($x > 1024)
            {
                jQuery("#content .row").preloader();    }

            jQuery('.magnifier').touchTouch();
            jQuery('.spinner').animate({'opacity':0},1000,'easeOutCubic',function (){jQuery(this).css('display','none')});
        }); */
    </script>


    <!--[if lt IE 8]>
<!--    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>-->
    <!--[endif]-->
    <!--[if (gt IE 9)|!(IE)]><!-->
    <!--<![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/docs.css" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ie.css" type="text/css" media="screen">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400' rel='stylesheet' type='text/css'>
    <![endif]-->
</head>
<body>
<div class="header-wrapper">
<center>
<header>
    <div class="nav-container">
         <!-- Start Navigation Menu ---->
         <ul class="nav">
           <li class=" nav-home">
               <a title="Start Here" href="<?php echo base_url() ?>">
                   <span>HOME</span>
               </a>
<!--               <div class="nav-title">Start Here</div> -->
           </li>
           <li class=" nav-berita dropdown">
               <a title="All about news" href="<?php echo base_url(); ?>headline"><span><strong>BERITA</strong> <!--<b class="caret"></b>--></span></a>
<!--               <div class="nav-title">All about News</div>
               <ul class="dropdown-menu">
                   <li class="<?php if(isset($isNews) && $isNews == "") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/'>Berita Terakhir</a></li>
                   <li class="<?php if(isset($isNews) && $isNews == "hot-issues") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/index/hot-issues/'>Berita Terpanas</a></li>
                   <li class='divider'>&nbsp;</li>
                   <li class="<?php if(isset($isNews) && $isNews == "ekonomi") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/index/ekonomi/'>Berita Ekonomi</a></li>
                   <li class="<?php if(isset($isNews) && $isNews == "hukum") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/index/hukum/'>Berita Hukum</a></li>
                   <li class="<?php if(isset($isNews) && $isNews == "parlemen") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/index/parlemen/'>Berita Parlemen</a></li>
                   <li class="<?php if(isset($isNews) && $isNews == "nasional") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/index/nasional/'>Berita Nasional</a></li>
                   <li class="<?php if(isset($isNews) && $isNews == "reses") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/index/reses/'>Berita Reses</a></li>
                   <li class="<?php if(isset($isNews) && $isNews == "daerah") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/index/daerah/'>Berita Daerah</a></li>
                   <li class="<?php if(isset($isNews) && $isNews == "internasional") { echo "active"; } ?>"><a href='<?php echo base_url() ?>news/index/internasional/'>Berita Internasional</a></li>
               </ul> -->
           </li>
           <li class=" nav-sentimen dropdown">
              <a title="Sentimen Indonesia" href="#" class="dropdown-toggle" data-toggle="dropdown"><span><strong>SENTIMEN</strong> <b class="caret"></b></span></a>
<!--              <div class="nav-title">Survey, Scandals, Powermap</div> -->
              <ul class="dropdown-menu">
                 <li class="<?php if(isset($isMenuSurvey) && $isMenuSurvey == "") { echo "active"; } ?>"><a href="<?php echo base_url() ?>survey/">Polling Komunitas</a></li>
                 <li class="<?php if(isset($isMenuScandal) && $isMenuScandal == "") { echo "active"; } ?>"><a href='<?php echo base_url() ?>scandal/'>Scandal</a></li>
                 <li class="<?php if(isset($isMenuSuksesi) && $isMenuSuksesi == "") { echo "active"; } ?>"><a href='<?php echo base_url() ?>suksesi/'>Suksesi Dan Survey</a></li>
                 <li class="<?php if(isset($isMenuPowermap) && $isMenuPowermap == "") { echo "active"; } ?>"><a href='<?php echo base_url() ?>powermap/'>Powermap</a></li>
              </ul>
           </li>

           <li class=" nav-politik">
              <a title="Indonesia Politics" href="<?php echo base_url(); ?>politik"><span><strong>PROFILE</strong></span></a>
<!--              <div class="nav-title">Life & Politics</div> -->
           </li>

           <li class=" nav-komunitas" style="margin-left:<?php echo (strlen($this->member['username']) > 20 ? '90px;' : '145px;'); ?>">
               <a title="Growth Together" href="<?php echo base_url(); ?>komunitas/<?=!empty($this->member['user_id']) ? $this->member['user_id'] : ''; ?>"><span><strong>KOMUNITAS</strong></span></a>
<!--               <div class="nav-title">Politic Discussion</div> -->
           </li>

        </ul>

        <!-- End Navigation Menu -->
        <!-- Start User Tools Menu --
        <!--<div id="user-tool" style="width:220px;"> -->
        <ul class="nav" id="user-menu-login">
            <?php if(is_array($this->member) && count($this->member)>0) { 	?>
                <!--             <li class="enabofnotify">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                   <img style="height: 18px;border-radius: 2px 2px 2px 2px;" alt="" src="<?php echo base_url(); ?>public/image/ico-notify.png"> <span class="enabofnotify totalnotification">*</span>
                </a>
                <ul class="dropdown-menu">
                   <li><a rel="nofollow" href="<?php echo base_url();?>comunity/friendrequest">Friend requests [<span class="numberofnotification">*</span>]</a></li>
                   <li><a rel="nofollow" href="<?php echo base_url();?>comunity/follow">Follow [<span class="numberoffollow">*</span>]</a></li>
                   <li><a rel="nofollow" href="<?php echo base_url();?>comunity/userlike">Like [<span class="numberoflike">0</span>]</a></li>
                   <li><a rel="nofollow" href="<?php echo base_url();?>comunity/userunlike">Dislike [<span class="numberofunlike">0</span>]</a></li>
                   <li><a rel="nofollow" href="<?php echo base_url();?>comunity/usercomment">Comment [<span class="numberofcomment">0</span>]</a></li>
                </ul>
             </li>
             <li>&nbsp;&nbsp;&nbsp;</li>-->
                <li class="dropdown nav-userprofile">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img id="icon_pp" style="height: 23px; background-color: #EEEEEE; border-radius: 2px 2px 2px 2px;" alt="" src="<?php if ($this->member['account_type'] == 0 ||$this->member['account_type'] == 2) { echo icon_url($this->member['xname'],'user/'.$this->member['user_id']) ; } else {  echo icon_url($this->member['xname'],'politisi/'.$this->member['user_id']) ; } ?>">&nbsp;
                        <?php if (strlen($this->member['username']) > 20): ?>
                            <?php echo substr($this->member['username'],0,16); ?>..
                        <?php else : ?>
                            <?php echo $this->member['username']; ?>
                        <?php endif;?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" style="padding: 15px 0px 0px 0px; background-color: #222222;">
                        <li><a rel="nofollow" href="<?php echo base_url();?>komunitas/profile/<?php echo $this->member['page_id']; ?>">Informasi Dasar</a></li>
                        <li><a rel="nofollow" href="<?php echo base_url();?>komunitas/profile/<?php echo $this->member['page_id']; ?>">Ganti Password</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url()?>home/logout">Log Out</a></li>
                    </ul>
                </li>
            <?php } else { ?>
                <li class="dropdown nav-login">
                    <a href="<?php echo base_url()?>home/login"><span><strong>LOGIN/REGISTER<span></strong></a>
<!--                    <div class="nav-title">Join Bijaks Community</div>  -->
                </li>

            <?php } ?>

        </ul><!-- /#user-menu-login -->
        <!--</div> <!--/#user-tool -->

         <!-- Start Searching Menu --> 
        <div class="navbar-search" style="margin-left:0px;margin-top: -3px;">
          <form action="<?php echo base_url().'search';?>">
            <input type="text" name="term" class="search-query span2" placeholder="Pencarian">
          </form>
        </div>
      <!--   End Searching Menu -->


     </div>
</header>
</center>
</div>
<div class="bg-atas">

</div>

<script>

    function goToPage() {
        window.location = '<?php echo base_url(); ?>';
    }

    $(document).ready(function() {

        $(function(){
            $('.carousel').carousel({
                interval: 5000
            });
        });

        <?php
          $success = $this->session->flashdata('message_success');
          $error = $this->session->flashdata('message_error');
          if (!empty($success))
          {
        ?>
            alertify.alert("<?php echo $success; ?>");
       <?php
         }

         if (!empty($error))
         {
       ?>
            alertify.alert("<?php echo $error; ?>");
       <?php
         }
       ?>
    });
</script>
