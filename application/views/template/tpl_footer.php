
<footer>
    <div class="container" style="width: 960px;">
      <div class="row-fluid">
        <div class="span3">
          <div class="footer-logo">
              <!-- <span style="font-size: 14px;">powered by</span>
              <div style="font-size: 28px;"><span style="font-size: 28px;color: #fdae27;">IGG</span>/Tek</div> -->
          </div>
        </div>
        <div class="span9  nav-footer">
          <div class="pull-right" style="float: right;">
            <ul class="nav">
            <li class=" nav-tentang"><a title="Tentang bijaks" href="<?php echo base_url() ?>home/login"><span>Login</span></a></li>
             <li class=" nav-tentang"><a title="Tentang bijaks" href="<?php echo base_url() ?>page/tentang"><span>Tentang</span></a></li>
             <li class=" nav-contact"><a title="Alamat bijaks" href="<?php echo base_url() ?>page/kontak"><span>Kontak</span></a></li>
             <li class=" nav-kebijakan"><a title="Kebijakan privasi dan policy" href="<?php echo base_url() ?>page/privasi"><span>Kebijakan Privasi</span></a></li>
             <li class=" nav-syarat"><a title="Syarat dan ketentuan" href="<?php echo base_url() ?>page/syarat_ketentuan"><span>Syarat &amp; Ketentuan</span></a></li>
            </ul>
          </div>
        </div>
        </div>
    </div>
</footer>

    <script>
        $(function() {
            $("img.lazy").lazyload({
                effect : "fadeIn",
                failure_limit : 13,
                event: "scrollstop"
            });
        });
    </script>

</body>
    <script>
     (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })
     (window,document,'script','//www.google-analytics.com/analytics.js','ga');
     ga('create', 'UA-40169954-1', 'bijaks.net');
     ga('send', 'pageview');
    </script>
</html>
