<div class="sub-header-container" style="margin-top: 31px;">
	
	<div class="logo-small-container">
		<img src="<?php echo base_url('assets/images/logo.png'); ?>" >
	</div>
	<div class="right-container" style="background-color: #EFEDED;">
		<div class="banner">
			<img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
		</div>
		<div class="category-news">
			<div class="category-news-title category-news-title-right category-news-title-<?php echo category_color(strtolower($category)); ?>">
				<h1><?php

                    switch(strtoupper($category))
                    {
                        case 'HOT-PROFILE' :
                            echo 'PROFILE'; break;
                        case 'SUKSESI' :
                            echo 'SUKSESI DAN SURVEY'; break;
                        case 'SURVEY' :
                            echo 'POLLING KOMUNITAS'; break;
                        default :
                            echo strtoupper($category); break;
                    }
                    ?></h1>
			</div>
			
		</div>
	</div>
	
</div>
<div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
<div id='disclaimer' class="sub-header-container"></div>
