<div class="col-3 social last-<?php echo $line['type']; ?>">
    <div class="media">
        <div style="background:
            url('<?php echo $line['image_url']; ?>') no-repeat;" class="pull-left circular">
            <img class="media-object" src="<?php echo $line['image_url']; ?>">
        </div>
        <!-- <a href="#" class="pull-right"> -->
        <img class="pull-right" src="<?php echo base_url() ?>assets/images/<?php echo $line['type']; ?>_icon_large.png">
        <!-- </a> -->
        <div class="media-body">
            <strong class="media-heading"><a href="<?php echo base_url(); ?>komunitas/profile/<?php echo $line['page_id']; ?>"><?php echo $line['page_name']; ?></a></strong><br>
            <a class="" title="<?php echo $line['title']; ?>" href="<?php echo $line['news_url'];?>"><?php echo (strlen($line['title']) > 26) ? substr($line['title'], 0, 24). '...' : $line['title']; ?></a>
        </div>
    </div>
</div>
