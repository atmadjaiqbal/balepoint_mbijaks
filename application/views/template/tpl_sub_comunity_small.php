<div class="social-small social-small-transparent last-<?php //echo category_color($category); ?>">
    <?php if($msg == 'ok'){ ?>
    <div class="media">
        <div style="background:
            url('<?php echo $line['image_url']; ?>') no-repeat; background-position: center; background-size:30px 30px;" class="pull-left circular">
            <!-- <img class="media-object" src="<?php echo $line['image_url']; ?>"> -->
        </div>
        
        <img class="pull-right ikon-social-small" src="<?php echo base_url() ?>assets/images/<?php echo $line['tipe']; ?>_icon_small.png">
        
        <div class="media-body">
            <strong class="media-heading"><a href="<?php echo base_url(); ?>komunitas/profile/<?php echo $line['page_id']; ?>"><?php echo $line['display_name']; ?></a></strong><br>
            <?php 
                    $now = time();
                    $txt = time_since(strtotime($line['entry_date']));
                    if($line['tipe'] == 'comment'){
                        $txt = (strlen($line['teks']) > 26) ? substr($line['teks'], 0, 24). '...' : $line['teks'] ;
                    } 
            ?>
            <span class=""><?php echo $txt; ?></span>
        </div>
    </div>
    <?php }else{ ?>
<!--    <div class="media">-->
<!--        <div class="span1">-->
<!--        </div>-->
<!--        <div class="media-body">-->
<!--            <strong class="media-heading">Tidak ada aktifitas user.</strong><br>-->
<!--        </div>-->
<!--    </div>-->
    <?php } ?>

</div>