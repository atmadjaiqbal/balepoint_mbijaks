<?php foreach($survey_terkait as $i=>$row){ ?>
<?php
$url = base_url() .'survey/index/'. $row['survey_id'] . "-" . urltitle($row['name']);
$survey_title = (strlen($row['name']) > 26) ? substr($row['name'], 0, 26) . ' ...' : $row['name'];
    $survey_title = $row['name'];

$is_open = 'Masih Berlangsung';
if($row['begin_date'] >= mysql_datetime() || $row['expired_date'] <= mysql_datetime()){
    $is_open = 'Sudah Selesai';
}
?>
<h4><a title="<?php echo $row['name']; ?>" href="<?php echo $url;?>"><?php echo $survey_title; ?></a> </h4>
<span class=""><?php echo $is_open;?></span>
<!--<div class="score-place score-place-overlay score" data-id="--><?//=$row['content_id'];?><!--">-->
<!--</div>-->
<?php

foreach($row['question'] as $key => $val) {
    $values = $legends = $option_id = $option_value = $series_data = array(); $i = 0;
    $chartData = array();

    $_wordlen = strlen($val['question']);
    $_baris = ceil($_wordlen / 40);
    $_wordTitle = '';
    $_y = 0;
    for ($_i = 1; $_i <= $_baris; $_i++) {
        $_wordTitle .= substr($val['question'], $_y, 41) . '<br/>';
        if($_i == 1)
        {
            $_y = 40 + $_i;
        } else {
            $_y = 40 + $_y;
        }
    }

    $chartData['title_chart'] = str_replace( '&#8230;','...', $_wordTitle);

    foreach($val['option'] as $opt) {
        $legends[]        = ucwords($opt['name']);
        $values[]         = (isset($opt['votes'])) ? $opt['votes'] : 0;
        $option_id[]      = (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0;
        $option_value[]   = (isset($opt['name'])) ? $opt['name'] : '';
        $series_data[]    = array(
            'name'=> ucwords($opt['name']),
            'id' => (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0,
            'voted' => (isset($opt['votes'])) ? $opt['votes'] : 0
        );
    }
    if (!empty($values)) {
        $chartData['width']        = 222;
        $chartData['height']       = 280;
        $chartData['values']       = $values;
        $chartData['legends']      = $legends;
        $chartData['show_legend']  = 'true';
        $chartData['question_id']  = $val['question_id'];
        $chartData['survey_id']    = $row['survey_id'];
        $chartData['option_id']    = $option_id;
        $chartData['option_value'] = $option_value;
        $chartData['series_data']  = $series_data;
        $chartData['url']          = $url .'?source=survey_list';
        $chartData['memberlogin']  = ''; // $memberlogin;
        $this->load->view('survey/survey_chart', $chartData);
    }

}
?>
<div class=" score-place score-place-full score" data-id="<?php echo $row['content_id']; ?>" ></div>
<div class="time-line-content" data-cat="survey" data-uri="<?=$row['content_id'];?>">
    <?=$row['last'];?>
</div>

<?php } ?>
<br>