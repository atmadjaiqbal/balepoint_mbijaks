<?php
$url = base_url() .'survey/index/'. $row['survey_id'] . "-" . urltitle($row['name']);
$survey_title = (strlen($row['name']) > 26) ? substr($row['name'], 0, 26) . ' ...' : $row['name'];

$is_open = 'Masih Berlangsung';
if($row['begin_date'] >= mysql_datetime() || $row['expired_date'] <= mysql_datetime()){
    $is_open = 'Sudah Selesai';
}
?>
<h4 style="font-size:18px;text-align:center;height: 38px;">
    <a title="<?php echo ucwords($row['name']); ?>" href="<?php echo $url;?>"><?php echo ucwords($row['name']); ?></a>
</h4>
<span class=""><?php echo $is_open;?></span>
<!--<div class="score-place score-place-overlay score" data-id="--><?//=$row['content_id'];?><!--">-->
<!--</div>-->
<?php

foreach($row['question'] as $key => $val) {
    $values = $legends = $option_id = $option_value = $series_data = array(); $i = 0;
    $chartData = array();

    $_wordlen = strlen($val['question']);
    $_baris = ceil($_wordlen / 40);
    $_wordTitle = '';
    $_y = 0;
    for ($_i = 1; $_i <= $_baris; $_i++) {
        $_wordTitle .= substr($val['question'], $_y, 41) . '<br/>';
        if($_i == 1)
        {
            $_y = 40 + $_i;
        } else {
            $_y = 40 + $_y;
        }
    }

    $chartData['title'] = str_replace( '&#8230;','...', $_wordTitle);

    foreach($val['option'] as $opt) {
        $legends[]        = ucwords($opt['name']);
        $values[]         = (isset($opt['votes'])) ? $opt['votes'] : 0;
        $option_id[]      = (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0;
        $option_value[]   = (isset($opt['name'])) ? $opt['name'] : '';
        $series_data[]    = array(
            'name'=> ucwords($opt['name']),
            'id' => (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0,
            'voted' => (isset($opt['votes'])) ? $opt['votes'] : 0
        );
    }
    if (!empty($values)) {
        $chartData['cid']  = $row['content_id'];
        $chartData['width']        = 222;
        $chartData['height']       = 176;
        $chartData['values']       = $values;
        $chartData['legends']      = $legends;
        $chartData['show_legend']  = ($show_legends) ? $show_legends : 'true';
        $chartData['question_id']  = $val['question_id'];
        $chartData['survey_id']    = $row['survey_id'];
        $chartData['option_id']    = $option_id;
        $chartData['option_value'] = $option_value;
        $chartData['series_data']  = $series_data;
        $chartData['url']          = $url .'?source=survey_list';
        $chartData['memberlogin']  = ''; // $memberlogin;
        $this->load->view('template/survey/survey_mini_chart', $chartData);
    }

}
?>

<div class=" score-place score-place-full score" data-id="<?php echo $row['content_id']; ?>" ></div>


<div class="time-line-content" data-cat="survey" data-uri="<?=$row['content_id'];?>">
</div>