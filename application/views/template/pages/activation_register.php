<div class="container" style="margin-top: 31px;">
  <div class="sub-header-container">
    <div class="logo-small-container">
        <a href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
        </a>
    </div>
    <div class="right-container" style="background-color: #ffffff;">
        <div class="banner">
            <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
        </div>
        <div class="category-news">
            <div class="category-news-title category-news-title-right category-news-title-merah">
                <h1>AKTIVASI AKUN ANDA</h1>
            </div>

        </div>
    </div>
  </div>

  <div class="sub-header-container" style="background-color: #fff;margin-top:160px;">
  <div style="margin-left: 20px;padding-top: 30px;margin-right: 20px;">
      <p><strong>Bijaks.net</strong> merupakan portal Social Network (SN) politik pertama dan utama di Indonesia, yang memadukan unsur jaringan sosial, berita dan arsip terkait dunia politik Indonesia, mulai dari poltisi, partai politik maupun dinamika politik Indonesia.Setiap hari, <strong>Bijaks.net</strong> menghadirkan sesuatu yang baru dan fresh, baik dari sisi pemberitaan maupun kumpulan arsip politik Indonesia, mulai dari eksekutif, legislatif, maupun yudikatif. Dilengkapi juga informasi serta data figur-figur stake holders politik Indonesia, baik secara individu, keluarga dan kelompoknya, maupun posisi dalam kancah dunia politik Indonesia melalui peran mereka di partai politik dan di perusahaan-perusahaan mereka di Indonesia. Semua itu tersaji secara konprehensif dan terkoneksi satu sama lainnya di <strong>Bijaks.net</strong>, baik melalui berita, database maupun tamplan grafis di power map.</p>
      <p><strong>Bijaks.net</strong> juga menampilkan fasilitas untuk dapat mengikuti perjalanan pesta demokrasi di Indonesia baik melalui Pilkada, Pemilu Legislatif dan Pilpres mulai dari prediksi, quick count hingga hasil resmi dari KPU atau KPUD, lengkap dengan eksposur kandidat dan hal terkait lainnya. Tak hanya data, foto dan grafis. <strong>Bijaks.net</strong> menyediakan fasilitas sosial media network terbaru di Indonesia, yang mandiri baik secara system maupun konten. <strong>Bijaks.net</strong> merupakan portal Social Network (SN) politik pertama dan utama di Indonesia, yang memadukan unsur jaringan sosial, berita dan arsip terkait dunia politik Indonesia, mulai dari poltisi, partai politik maupun dinamika politik Indonesia.</p>
      <p>Setiap hari, <strong>Bijaks.net</strong> menghadirkan sesuatu yang baru dan fresh, baik dari sisi pemberitaan maupun kumpulan arsip politik Indonesia, mulai dari eksekutif, legislatif, maupun yudikatif. Dilengkapi juga informasi serta data figur-figur stake holders politik Indonesia, baik secara individu, keluarga dan kelompoknya, maupun posisi dalam kancah dunia politik Indonesia melalui peran mereka di partai politik dan di perusahaan-perusahaan mereka di Indonesia. Semua itu tersaji secara konprehensif dan terkoneksi satu sama lainnya di <strong>Bijaks.net</strong>, baik melalui berita, database maupun tamplan grafis di power map.</p>
      <p><strong>Bijaks.net</strong> juga menampilkan fasilitas untuk dapat mengikuti perjalanan pesta demokrasi di Indonesia baik melalui Pilkada, Pemilu Legislatif dan Pilpres mulai dari prediksi, quick count hingga hasil resmi dari KPU atau KPUD, lengkap dengan eksposur kandidat dan hal terkait lainnya. Tak hanya data, foto dan grafis. <strong>Bijaks.net</strong> menyediakan fasilitas sosial media network terbaru di Indonesia, yang mandiri baik secara system maupun konten.</p>


      <div class='row-fluid'>
          <div class='span12'>
              <h4>Profil Anda </h4>
              <div id='message_signup' class='hide'><b>Email anda telah tervalidasi, mohon mengisi profil anda.</b></div>
          </div>
          <form id="signupform" name="signupform" method="POST" action="<?php echo base_url(); ?>home/profileregister">
              <div class="row-fluid">
                  <div class="span4">
                      <div class="control-group">
                          <div class="controls">
                              <input type="hidden" name="uid" autocomplete="off" value="<?php echo $account_id;?>" readonly/>
                              <input type="text" id="email" name="email" size="35" autocomplete="off" value="<?php echo $email;?>" readonly/>
                              <input type="hidden" name="uid" autocomplete="off" value="<?php echo $account_id;?>" readonly/>
                          </div>
                      </div>
                      <div class="control-group">
                          <div class="controls">
                              <input type="text" id="fname" class="inputlogin" name="fname" size="35"  autocomplete="off" placeholder="Nama Awal"/>
                          </div>
                      </div>
                      <div class="control-group">
                          <div class="controls">
                              <input type="text" id="lname" name="lname" class="inputlogin" size="35" value=""  autocomplete="off" placeholder="Nama Belakang"/>
                          </div>
                      </div>
                  </div>
                  <div class="span4">
                      <div class="control-group">
                          <div class="controls">
                              <label for="gender">Gender</label>
                              <select name="gender" id="gender" >
                                  <option value="">Select Sex:</option>
                                  <option value="M">Male</option>
                                  <option value="F">Female</option>
                              </select>
                          </div>
                      </div>
                  </div>
                  <div class="span4">
                      <div class="control-group">
                          <div class="controls">
                              <label for="dob">I was born on</label>
                              <input type="text" id="dob" name="dob" size="50" maxlength="100"  />
                          </div>
                      </div>
                      <div class="control-group">
                          <div class="controls">
                              <input type="submit" class="loginbutton" name="inputsubmit" value="Daftar" />
                          </div>
                      </div>
                  </div>

              </div>
          </form>


      </div>
  </div>
  </div>
</div>
<br/>

<script>
        $(document).ready(function() {

            $('#signupform').validate({
                rules: {
                    fname: {required: true},
                    lname: {required: true},
                    gender: {required: true},
                    bod: {required: true},
                },
                messages: {
                    fname: {required: "Please enter your first name."},
                    fname: {required: "Please enter your last name."},
                    gender: {required: "Please choose your gender."},
                    bod: {required: "Please enter your birth of date"}
                },
                errorPlacement: function(error, element) {
/*                    error.css('color', 'red');
                    offset = element.offset();
                    error.css('top', offset.top);
                    error.css('position', 'absolute');
                    error.css('left', offset.left + element.outerWidth()+ 10);
                    error.css('width','250px');
                    error.insertAfter(element); */

                    $('.message_signup b').text('');
                    $('.message_signup b').append(error);
                    $('.message_signup').show();



                    //error.addClass('message');  // add a class to the wrapper
                },
                submitHandler: function(form) {
                    $("#loader_signup").show('slow');
                    $('#message_signup b').text('');

                    var options = {
                        method: 'POST', dataType: 'json',
                        success: function(data) {
                            if(data.message_code == 'captcha') {
                                $('#message_signup').append('Salah input kode keamanan, silahkan input kembali !');
                                //Recaptcha.reload();

                            } else if(data.message_code == 'success') {
                                $('#message_signup').append(data.message);
                                $("#signupform").hide('slow');
                                //Recaptcha.reload();

                            } else {
                                $("#message_signup b").append(data.message);
                            }

                            $('#message_signup').show();

                            return false;
                        }
                    };
                    $(form).ajaxSubmit(options);
                    return false;
                }
            }); // $('#signupform').validate()

            $('#dob').datepicker({dateFormat: 'yy-mm-dd', changeYear: true, changeMonth: true, yearRange: "-80:-12", defaultDate: new Date(1990,01,01) });

        });
</script>
