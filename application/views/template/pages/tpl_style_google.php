<?php $this->load->view($template_path .'tpl_header');?>

		<!-- Page container -->
		<section class="page-container page-boxed">
			<div class="row-fluid">
				<article class="span12 data-block">
					<div class="data-container">
						<header>
							<h3>GOOGLE STYLE</h3>
						</header>
						<section>

<h2>Google+ Buttons / Icon Buttons</h2>

<div class="buttons">
  <button><span class="glabel">Button</span></button>
  <a href="#" class="button on"><span class="glabel">Active</span></a>
  <a href="#" class="button left"><span class="glabel">Left</span></a>
  <a href="#" class="button middle"><span class="glabel">Middle</span></a>
  <a href="#" class="button right"><span class="glabel">Right</span></a>
  
  <div class="xx">
    <a href="#" class="button left "  title="Home" data-toggle="tooltip"><span class="icon icon108"></span></a>
    <a href="#" class="button middle " title="Photos" data-toggle="tooltip"><span class="icon icon148"></span></a>
    <a href="#" class="button middle" title="Music" data-toggle="popover" data-placement="top"><span class="icon icon134"></span></a>
    <a href="#" class="button right" title="Save" data-toggle="popover" data-placement="bottom"><span class="icon icon67"></span></a>
  </div> <!-- /.tiptip -->
  
  <a href="#" class="button left"><span class="icon icon22"></span></a>
  <a href="#" class="button middle"><span class="icon icon177"></span></a>
  <a href="#" class="button right"><span class="icon icon153"></span></a>
  
  <a href="#" class="button on"><span class="icon icon125"></span></a>
  
  <a href="#" class="button left"><span class="icon icon127"></span></a>
  <a href="#" class="button middle"><span class="icon icon84"></span></a>
  <a href="#" class="button right"><span class="icon icon186"></span></a>
</div> <!-- /.buttons -->

<h2>Google+ Action Buttons</h2>

<div class="buttons">
  <button class="action blue"><span class="glabel">Save</span></button>
  <button class="action red"><span class="glabel">Upload</span></button>
  <button class="action green"><span class="glabel">Comment</span></button>
  <button class="action"><span class="glabel">Cancel</span></button>
</div> <!-- /.buttons -->

<h2>Google+ Icon Buttons with Label</h2>

<div class="buttons">
  <a href="#" class="button"><span class="icon icon4"></span><span class="glabel">Profile</span></a>
  
  <a href="#" class="button"><span class="icon icon19"></span><span class="glabel">Find</span></a>
  
  <a href="#" class="button left"><span class="icon icon63"></span><span class="glabel">Left</span></a>
  <a href="#" class="button middle"><span class="icon icon58"></span><span class="glabel">Middle</span></a>
  <a href="#" class="button right"><span class="icon icon64"></span><span class="glabel">Right</span></a> 
  
  <a href="#" class="button left"><span class="icon icon108"></span><span class="glabel">Normal</span></a>
  <a href="#" class="button middle"><span class="icon icon125"></span><span class="label red">Red</span></a>
  <a href="#" class="button middle"><span class="icon icon191"></span><span class="label blue">Blue</span></a>
  <a href="#" class="button middle"><span class="icon icon151"></span><span class="label green">Green</span></a>
  <a href="#" class="button right"><span class="icon icon177"></span><span class="label yellow">Yellow</span></a>
</div> <!-- /.buttons -->

<h2>gdropdown Menu Buttons</h2>

<div class="buttons">
  <div class="gdropdown">
    <a href="#" class="button"><span class="glabel">File</span><span class="toggle"></span></a>
    <div class="gdropdown-slider">
      <a href="#" class="ddm"><span class="glabel">New</span></a>
      <a href="#" class="ddm"><span class="glabel">Save</span></a>
    </div> <!-- /.gdropdown-slider -->
  </div> <!-- /.gdropdown -->
  
  <div class="gdropdown">
    <a href="#" class="button"><span class="icon icon55"></span><span class="glabel">File</span><span class="toggle"></span></a>
    <div class="gdropdown-slider">
      <a href="#" class="ddm"><span class="icon icon68"></span><span class="glabel">New</span></a>
      <a href="#" class="ddm"><span class="icon icon92"></span><span class="glabel">Open</span></a>
      <a href="#" class="ddm"><span class="icon icon67"></span><span class="glabel">Save</span></a>
    </div> <!-- /.gdropdown-slider -->
  </div> <!-- /.gdropdown -->
  
  <div class="gdropdown left">
    <a href="#" class="button left"><span class="icon icon96"></span><span class="glabel">Options</span><span class="toggle"></span></a>
    <div class="gdropdown-slider">
      <a href="#" class="ddm"><span class="icon icon127"></span><span class="glabel">Profile</span></a>
      <a href="#" class="ddm"><span class="icon icon125"></span><span class="glabel">Inbox (3)</span></a>
      <a href="#" class="ddm"><span class="icon icon196"></span><span class="glabel">Settings</span></a>
      <a href="#" class="ddm negative"><span class="icon icon151"></span><span class="glabel">Logout</span></a>
    </div> <!-- /.gdropdown-slider -->
  </div> <!-- /.gdropdown -->
  <div class="gdropdown right">
    <button class="right"><span class="icon icon116"></span><span class="glabel">Actions</span><span class="toggle"></span></button>
    <div class="gdropdown-slider">
      <a href="#" class="ddm"><span class="icon icon122"></span><span class="glabel">Lock</span></a>
      <a href="#" class="ddm"><span class="icon icon123"></span><span class="glabel">Unlock</span></a>
    </div> <!-- /.gdropdown-slider -->
  </div> <!-- /.gdropdown -->
  
  <div class="gdropdown">
    <a href="#" class="button"><span class="icon icon197"></span><span class="glabel">World Regions</span><span class="toggle"></span></a>
    <div class="gdropdown-slider">
      <a href="#" class="ddm"><span class="glabel">The Americas</span></a>
      <a href="#" class="ddm"><span class="glabel">Europe &amp; Eurasia</span></a>
      <a href="#" class="ddm"><span class="glabel">Africa / Middle East</span></a>
      <a href="#" class="ddm"><span class="glabel">East Asia &amp; The Pacific</span></a>
      <a href="#" class="ddm"><span class="glabel">South &amp; Central Asia</span></a>
    </div> <!-- /.gdropdown-slider -->
  </div> <!-- /.gdropdown -->
  
  <a href="#" class="button left"><span class="icon icon108"></span></a>
  <div class="gdropdown middle">
    <a href="#" class="button middle"><span class="icon icon68"></span><span class="toggle"></span></a>
    <div class="gdropdown-slider">
      <a href="#" class="ddm"><span class="icon icon3"></span><span class="glabel">Add</span></a>
      <a href="#" class="ddm"><span class="icon icon145"></span><span class="glabel">Edit</span></a>
      <a href="#" class="ddm"><span class="icon icon186"></span><span class="glabel">Trash</span></a>
    </div> <!-- /.gdropdown-slider -->
  </div> <!-- /.gdropdown -->
  <a href="#" class="button right"><span class="icon icon151"></span></a>
</div> <!-- /.buttons -->


<div class="buttons">
  <a class="button left" href="http://www.socialite-media.co.za"><span class="icon icon119"></span><span class="glabel">Social-IT-e Media</span></a>
  <a class="button middle" href="http://shrapp.nl"><span class="icon icon42"></span><span class="glabel">The Blog</span></a>
  <a class="button right" href="http://code.brucegalpin.com/google-plus-ui-buttons"><span class="icon icon120"></span><span class="glabel">Documentation</span></a>
</div>


						</section>
					</div>
				</article>
			</div>
		</section>

<?php $this->load->view($template_path .'tpl_footer');?>