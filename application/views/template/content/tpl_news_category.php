<?php
			$result = $result[0];
			$news_url = base_url() . "news/article/" . $result['topic_id'] . "-" . $result['id'] . "/" . trim( str_replace('/', '', $result['slug']));
			$short_title = (strlen ($result['title']) > 26) ? substr($result['title'], 0, 26). '...' : $result['title'];
            $short_title = $result['title'];
	 ?>
<div class="span4" xmlns="http://www.w3.org/1999/html">
	<div class="hp-place">

        <div class="home-title-section hp-label hp-label-<?php echo category_color($category); ?>">
            <span class="hitam"><a href="<?php echo base_url(); ?>news/index/<?=$category;?>"><?php echo strtoupper($category); ?></a></span>
        </div>
		<a href="<?php echo $news_url; ?>"><h4 title="<?php echo $result['title']; ?>"><?php echo $short_title; ?></h4></a>
		<img class="news-image lazy" alt='<?php echo $result['title']; ?>' data-original="<?php echo $result['image_thumbnail']; ?>">
        <div class="score-place score-place-overlay score" data-id="<?php echo $result['content_id']; ?>" >
         </div>
		<div class="row-fluid politisi-terkait-container">
			<div class="span2"><span>Politisi Terkait</span></div>
			<?php if (count($result['news_profile']) > 0){ ?>
			<?php foreach ($result['news_profile'] as $key => $pro) { ?>
                <?php
                $img_pola = key_exists('score', $pro) ? 'img-polaroid-'.$pro['score'] : '';
                ?>
				<?php if($key === 4) break; ?>
				<div class="span2">
                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
					<img class="img-polaroid <?php echo $img_pola;?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                    </a>
				</div>
			<?php } ?>
				<?php if (count($result['news_profile']) > 4){ ?>
				<div class="span2">
					<span>More</span>
				</div>
				<?php } ?>
			<?php }else{ ?>
			<div class="span6"><span>Tidak ada politisi terkait</span></div>
			<?php } ?>
			
		</div>
	</div>
	<div class="time-line-content" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo $result['content_id']; ?>">
		<?php //echo $sub_cat['timeline']; ?>
	</div>
	
</div>