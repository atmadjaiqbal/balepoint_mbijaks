<!-- <div class="span4"> -->
	<?php
    if(!empty($news_detail))
    {
			$news_url = base_url() . "news/article/" . $news_detail['topic_id'] . "-" . $news_detail['id'] . "/" . trim( str_replace('/', '', $news_detail['slug']));
			$short_title = (strlen ($news_detail['title']) > 50) ? substr($news_detail['title'], 0, 50). '...' : $news_detail['title'];
            $title_2_line = (strlen($news_detail['title']) < 36 ) ? 'news-index-title-2-line' : '';

            $short_title = $news_detail['title'];
	 ?>

        <h4 class="news-index-title <?=$title_2_line;?>" title="<?php echo $news_detail['title']; ?>"><a href="<?php echo $news_url; ?>"><?php echo $short_title; ?></a></h4>

        <p class="news-index-date" style=""><?php echo (!empty($news_detail['date']) ? mdate('%d %M %Y - %h:%i', strtotime($news_detail['date'])) : ''); ?></p>
	<img class="news-image lazy" alt='<?php echo $news_detail['title']; ?>' data-original="<?php echo $news_detail['image_thumbnail']; ?>">
<div class="score-place score-place-overlay score" data-id="<?php echo $news_detail['content_id']; ?>" >
</div>
	<div class="row-fluid politisi-terkait-container">
		<div class="span2"><span>Politisi Terkait</span></div>
		<?php if (count($news_detail['news_profile']) > 0){ ?>
		<?php foreach ($news_detail['news_profile'] as $key => $pro) { ?>
            <?php
            $img_pola = key_exists('score', $pro) ? 'img-btm-border-'.$pro['score'] : '';
            ?>
			<?php if($key === 4) break; ?>
			<div class="span2">
                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
				<img class="img-btm-border <?php echo $img_pola;?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
			    </a>
            </div>
		<?php } ?>
			<?php if (count($news_detail['news_profile']) > 4){ ?>
			<div class="span2">
				<span>More</span>
			</div>
			<?php } ?>
		<?php }else{ ?>
		<div class="span6"><span>Tidak ada politisi terkait</span></div>
		<?php } ?>
		
	</div>
	<div class="time-line-content" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo $news_detail['content_id']; ?>">
		
	</div>

<?php
    }
?>
<!-- </div> -->
