<div class="span4">
	<div class="hp-place ">
    
    
<!--		<div class="hp-label hp-label-<?php echo category_color($category); ?>"> -->
<!--			<a class="pull-right hp-label-index" href='--><?php //echo base_url(); ?><!--news/index/--><?//=$category;?><!--'><img src="--><?php //echo base_url(); ?><!--/assets/images/list_icon.png"></a>-->
<!--			<h4><?php echo strtoupper($category); ?></h4> -->
<!--		</div> -->

        <div class="home-title-section hp-label hp-label-<?php echo category_color($category); ?>">
            <span class="hitam"><a href="<?php echo base_url(); ?>news/index/<?=$category;?>"><?php echo strtoupper($category); ?></a></span>
        </div>




		<div class="hp-place-<?php echo $category; ?>">
            <div class="hp-place-rela-con">
            <div class="hp-place-rela hs-place-<?php echo $category; ?>">
		<?php 
			// echo '<pre>';
			// var_dump($result);
			// echo '</pre>';
           if(!empty($result)) 
           { 
		?>
		<?php foreach ($result as $key => $value) { ?>
			<?php
                   if(!empty($value['topic_id']) && !empty($value['id']) && !empty($value['slug']))
                   {
					   $news_url = base_url() . "news/article/" . $value['topic_id'] . "-" . $value['id'] . "/" . trim( str_replace('/', '', $value['slug']));
                   } else {
                        $news_url = "";
                   }

                   if(!empty($value['title']))
                   {
					    $short_title = (strlen ($value['title']) > 23) ? substr($value['title'], 0, 23). '...' : $value['title'];
                       $short_title = $value['title'];
                   } else {
                        $short_title = ""; 
                   }

                   if(!empty($value['image_thumbnail']))
                   {
                     $_image_thumb = $value['image_thumbnail'];
                   } else {
                     $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
                   }

			 ?>
			<div id="media-headline" class="media">
				<div class="media-image pull-left" style="background:
            url('<?php echo $_image_thumb; ?>') no-repeat; background-position : center; background-size:auto 80px;">
				
				</div>
			  	<div class="media-body">
			    	<h5 class="media-heading"><a title="<?php echo !empty($value['title']) ? $value['title'] : ''; ?>" href="<?php echo $news_url; ?>"><?php echo $short_title; ?></a></h5>
                    <p class="news-date" style="margin-top:-2px;"><?php echo !empty($value['date']) ? mdate('%d %M %Y - %h:%i', strtotime($value['date'])) : ''; ?></p>
			  	    <div class="score-place score-place-left score" data-tipe="1" data-id="<?php echo !empty($value['content_id']) ? $value['content_id'] : 0;?>"  style="margin-top:-17px;"></div>
                 </div>
			</div>
		<?php } ?>
        
		<?php } ?>
            </div>
            </div>
        </div>
        


    </div>
    <div class="row-fluid">
        <div class="span6 text-left">
            <a data-holder="hs-place-<?php echo $category; ?>" data-tipe="1" class="btn btn-mini next-list">5 Berikutnya</a>
        </div>
        <div class="span6 text-right">
            <a href="<?php echo base_url(); ?>news/index/<?php echo $category; ?>" class="btn btn-mini">Selengkapnya</a>
        </div>
    </div>
    <p></p>
    <div class="hp-footer last-<?php echo category_color($category); ?>"></div>
</div>
