<?php

foreach($suksesi as $key => $val){
    $suksesi_url = base_url() . 'suksesi/index/'.$val['id_race'].'-'.urltitle($val['race_name']);
    $map_photo = '';
    $logo_photo = '';
    foreach($val['photos'] as $foto){
        if(intval($foto['type']) === 2){
            $map_photo = $foto['thumb_url'];
        }elseif(intval($foto['type']) === 1){
            $logo_photo = $foto['thumb_url'];
        }
    }

    $status_suksesi = 'Survey / Prediksi';
    $index_trace_status = 0;
    foreach($val['status'] as $idx => $status){
        if(intval($status['draft']) == 1){
            if(intval($status['status']) == 1)
            {
                $status_suksesi = 'Survey / Prediksi';
            } else {
                if(intval($status['status']) == 2)
                {
                    $status_suksesi = 'Putaran Pertama';
                } else {
                    $status_suksesi = 'Putaran Kedua';
                }
            }
            //        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
            $index_trace_status = $idx;
        }
    }

    $index_trace_status_quick = 1;
    $index_trace_status_survey = 0;


    $total_kandidat = count( $val['status'][$index_trace_status]['kandidat'] );
    $tabSection = "section-tab-".$val['id_race'];
    ?>
    <!-- OPEN TAB -->
    <?php //if(!empty($val['status'][$index_trace_status]['lembaga'])):?>
    <?php
    $t = 0;

    ?>
    <div class="row-fluid">
    <div class="span12">

    <!-- ul class="inline ul-inline" -->

    <table summary='' class='home-race-pilpres' >
    <tr>
        <td style="width: 100px; height: 65px;vertical-align: bottom;text-align: center;padding-top:5px;"></td>
        <!-- KANDIDAT SURVEY -->
        <?php foreach ($val['status'][0]['kandidat'] as $kes => $vs) {
            $kandidat_page_id = $vs['page_id'];
            $kandidat_name  = $vs['page_name'];
            $kandidat_link  = base_url() . 'aktor/profile/' . $vs['page_id'];

            if($vs['profile_badge_url'] <> 'None')
            {
                $kandidat_pic = $vs['profile_badge_url'];
            } else {
                $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
            }

            $pasangan_page_id = $vs['page_id_pasangan'];

            if($vs['page_name_pasangan'] != 'None' && $vs['page_name_pasangan'] != 'None' &&
                $vs['page_name_pasangan'] != 'Tidak Ada Pasangan ' &&  $vs['page_name_pasangan'] != 'Pasangan Politisi' &&
                ! empty($vs['page_name_pasangan'])
            )
            {
                $pasangan_name  = $vs['page_name_pasangan'];
            } else {
                $pasangan_name  = '';
            }

            $pasangan_link  = base_url() . 'aktor/profile/' . $vs['page_id_pasangan'];

            if($vs['profile_badge_pasangan_url'] <> 'None')
            {
                $pasangan_pic = $vs['profile_badge_pasangan_url'];
            } else {
                $pasangan_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
            }

            ?>

            <?php
            if($pasangan_page_id != 'belummenentukanpilihan539509e07277f')
            {
                ?>
                <td style="width: 60px; height: 60px;vertical-align: bottom;text-align: center;padding-top:5px;">
                    <?php if ($kes == 0){ ?>
                        <img class="pilpres-image" src="http://www.bijaks.net/assets/images/pilpres-2014/prabowo-hatta.png">
                    <?php }else{ ?>
                        <img class="pilpres-image" src="http://www.bijaks.net/assets/images/pilpres-2014/jokowi-jk.png">
                    <?php } ?>
                </td>
            <?php } ?>

        <?php } ?>
    </tr> <!-- end of candidate -->


    <?php
    $t=0; $_lembaga = array();$sort_name = array();
    foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $sortlembaga){
        $sort_name[]  = $sortlembaga['lembaga_name'];
    }
    if(in_array('Rekapitulasi KPU', $sort_name)){
        krsort($val['status'][$index_trace_status]['lembaga']);
    }


    foreach ($val['status'][1]['lembaga'] as $lk => $vl) {

        // if(!empty($vl['kandidat'][$racekey]['score']['created_date']) && $vl['kandidat'][$racekey]['score']['created_date'] <> 'None')
        // {
        //     $score_date = date('d/m/Y H:i:s', strtotime($vl['kandidat'][$racekey]['score']['created_date']));
        // } else {
        //     $score_date = date('d-m-Y', strtotime($vl['start_date']));;
        // }

        if(!empty($vl['kandidat'][0]['score']['created_date']) && $vl['kandidat'][0]['score']['created_date'] <> 'None')
        {
            if(!empty($vl['kandidat'][0]['score']['update_date']) && $vl['kandidat'][0]['score']['update_date'] <> 'None'){
                $score_date = date('d/m/Y H:i:s', strtotime($vl['kandidat'][0]['score']['update_date']));    
            }else{
                $score_date = date('d/m/Y H:i:s', strtotime($vl['kandidat'][0]['score']['created_date']));
            }
            
        } else {
            // $score_date = date('d/m/Y', strtotime($lembaga['start_date']));;
            $score_date = '';
        }

        ?>
        <tr>
            <td class="pilpres-race-title">
                <a style="cursor: default;font-size: 12px;"><?php echo $vl['lembaga_name'];?></a><br/>
                <em><small><?php echo $score_date;?></small></em>
            </td>
            <!-- SURVEY HASIL-->
            <?php
        $quick_count_exist = false;
        foreach($val['status'][1]['lembaga'] as $racekey => $lembaga):


            if($lembaga['page_id'] == $vl['page_id']){
                $quick_count_exist = true;

                if(!empty($lembaga['start_date']) && $lembaga['start_date'] <> 'None')
                {
                    $lembaga_date = date('d/m/Y H:i:s', strtotime($lembaga['start_date']));
                } else {
                    $lembaga_date = '';
                }

                if(!empty($lembaga['kandidat'][$racekey]['score']['created_date']) && $lembaga['kandidat'][$racekey]['score']['created_date'] <> 'None')
                {
                    $score_date = date('d/m/Y H:i:s', strtotime($lembaga['kandidat'][$racekey]['score']['created_date']));
                } else {
                    $score_date = date('d-m-Y', strtotime($lembaga['start_date']));;
                }


                $lembaga_name  = $lembaga['lembaga_name'];
                if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];

                $id_lembaga = $lembaga['id_trace_lembaga'];
                $words  = preg_split("/\s+/", $lembaga_name);
                $acronym = $lembaga['lembaga_alias']; // (count($word)s == 1) ? substr($words[0], 0,3) : '';
                $tabName = 'tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                $target = 'conten-tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];

                if($acronym == 'RK') {
                    $backtab = 'style="background-color:#cecece;"';
                } else {
                    $backtab = '';
                }

                ?>

                <?php

                $total_score    = 0;
                $winner         = '';
                $winner_score   = 0;
                $pct = 0;
                foreach($lembaga['kandidat'] as $kandidat) {
                    if(count($kandidat['score']) > 0){
                        $score = $kandidat['score'];
                        $kandidat = $kandidat['kandidat'];
                        if ($score['score_type'] == '1') {
                            $total_score += (int) $score['score'];
                        }
                        if ($score['score'] > $winner_score) {
                            $winner             = $kandidat['page_id'];
                            $winner_score   = $score['score'];
                        }
                    }
                }
                foreach($lembaga['kandidat'] as $kandidatkey => $kandidat)
                {

                    $score = $kandidat['score'];
                    $kandidat = $kandidat['kandidat'];
                    if(!empty($score)){

                        if ($score['score_type'] == '0') {
                            $pct            = number_format($score['score'],2);
                        } else {
                            $pct            = number_format(($score['score'] / $total_score)*100,2);
                        }
                    }

                    if($lembaga_name == 'Rekapitulasi KPU')
                    {
                        $winner_bar     = ($winner == $kandidat['page_id'] ) ? '' : '';
                        $winner_text = ($winner == $kandidat['page_id'] ) ? '<div class="pilpres-bar-1">Pemenang  '.$pct.' %</div>' : '<div class="pilpres-bar-1">'.$pct.' %</div>';
                    } else {
                        $winner_bar     = ($winner == $kandidat['page_id'] ) ? 'background-color:#4A3ED6;;' : 'background-color:#EC282A;';
                        $winner_text = ($winner == $kandidat['page_id'] ) ? '<div class="pilpres-bar-2">'.$pct.' %</div>' : '<div class="pilpres-bar-2">'.$pct.' %</div>';

                    }

                    ?>

                    <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
                        <?php $margintop = ''; if ($kandidatkey != 2){ $margintop = ''; } ?>
                        <!-- <div class='pilpres-percentage-box'> -->
                            <div class='pilpres-percentage-bar ' style='height:<?php echo $pct; ?>px;<?=$winner_bar;?>'>
                                <?=$winner_text;?>
                            </div>

                        <!-- </div> -->
                    </td>

                <?php
                }

                ?>
                <?php
                $t++;
                break;
            }else{
                ?>

            <?php
            }
        endforeach;
?>
        </tr>

    <?php } ?>



    </table>
    <!-- /ul-->
    </div>
    </div>
<?php } //endif; ?>

