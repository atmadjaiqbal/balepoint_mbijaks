<?php
$suksesi_url = base_url() . 'suksesi/index/'.$val['id_race'].'-'.urltitle($val['race_name']);
$map_photo = '';
$logo_photo = '';
foreach($val['photos'] as $foto){
    if(intval($foto['type']) === 2){
        $map_photo = $foto['thumb_url'];
    }elseif(intval($foto['type']) === 1){
        $logo_photo = $foto['thumb_url'];
    }
}
$status_suksesi = 'Survey / Prediksi';
$index_trace_status = 0;
foreach($val['status'] as $idx => $status){
    if(intval($status['draft']) == 1){
        if(intval($status['status']) == 1)
        {
            $status_suksesi = 'Survey / Prediksi';
        } else {
            if(intval($status['status']) == 2)
            {
                $status_suksesi = 'Putaran Pertama';
            } else {
                $status_suksesi = 'Putaran Kedua';
            }
        }
//        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
        $index_trace_status = $idx;
    }
}


$total_kandidat = count( $val['status'][$index_trace_status]['kandidat'] );
$tabSection = "section-tab-".$val['id_race'];
?>
<div class="row-fluid">
    <div class="span7">
        <div class="row-fluid">
            <div class="span3 suksesi-img-logo">
                <img src="<?php echo $logo_photo; ?>">
            </div>
            <div class="span9">
                <a href="<?php echo $suksesi_url; ?>"><h4 title="<?php echo $val['race_name']; ?>"><?php echo (strlen($val['race_name']) > 40) ? substr($val['race_name'], 0, 40).'...' : $val['race_name']; ?></h4></a>

            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <table class="table">
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Daftar pemilih tetap</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo number_format($val['daftar_pemilih_tetap'],0,'','.'); ?> Jiwa</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Jumlah kandidat</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo $total_kandidat; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Tanggal pelaksanaan</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo mdate('%d %M %Y', strtotime($val['tgl_pelaksanaan'])); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Status</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <strong><?php echo (isset($val['status_akhir_suksesi']) && $val['status_akhir_suksesi'] ? $val['status_akhir_suksesi'] : $status_suksesi);?></strong></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
    <div class="span5 suksesi-img-map">
        <img class="lazy" data-original="<?php echo $map_photo; ?>">
        <br>
        <br>
        <br>
        <div class="score-place score-place-overlay score" data-id="<?php echo $val['content_id']; ?>" >
        </div>
        <!--        <div class="score-place score-place-overlay score" data-id="--><?php ////echo $val['content_id']; ?><!--" >-->
        <!--        </div>-->
    </div>
</div>

<!-- ### TABBABLE ########################################################## -->
<?php if(!empty($val['status'][$index_trace_status]['lembaga'])):?>
    <div class="row-fluid">

        <div class="home-content-suksesi tabbable tabs-above" id="<?=$tabSection;?>">

            <?php
            $t = 0;
            $limit_lembaga = 7;
            $limit_candidat = 10;

            ?>
            <ul class="nav nav-tabs nav-home-suksesi">
                <?php
                $t=0; $_lembaga = array();$sort_name = array();
                foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $sortlembaga){$sort_name[]  = $sortlembaga['lembaga_name'];}
                if(in_array('Rekapitulasi KPU', $sort_name)){krsort($val['status'][$index_trace_status]['lembaga']);}

                foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $lembaga):
                    if($racekey >= $limit_lembaga){ break; }
                    $lembaga_name  = $lembaga['lembaga_name'];
                    if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];

                    $id_lembaga = $lembaga['id_trace_lembaga'];
                    $words	= preg_split("/\s+/", $lembaga_name);
                    $acronym = (count($words) == 1) ? substr($words[0], 0,3) : '';
                    $tabName = 'tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                    $target = 'conten-tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                    if(count($words) > 1) {
                        foreach($words as $w) {
                            if (isset($w[0])) {
                                if(ctype_alnum($w[0]))  $acronym .= $w[0];
                            }
                        }
                    } else {
                        $acronym = substr($lembaga_name, 0,3);
                    }
                    $acronym = strtoupper($acronym);
                    if($acronym == '') $acronym = '&hellip;' ;
                    if($acronym == 'RK') { $acronym = 'KPU'; $backtab = 'style="background-color:#cecece;"'; } else { $backtab = '';}

                    $_lembaga[$t] = $acronym;

                    ?>
                    <li id="list-<?=$tabName;?>" class=" <?php if ($t==0) echo ' active' ;?>">
                      <a <?php echo $backtab; ?> id="<?=$tabName; ?>" href="#<?=$target; ?>"><?=$acronym;?></a>
                    </li>
                    <?php
                    $t++;
                endforeach;

                if(in_array('KPU', $_lembaga))
                {

                } else {
                ?>
                    <li id="list-tab-<?php echo $index_trace_status;?>-9999-<?php echo $id_lembaga;?>">
                        <a style="background-color:#cecece;" id="tab-<?php echo $index_trace_status;?>-9999-<?php echo $id_lembaga;?>" href="#conten-tab-<?php echo $index_trace_status;?>-9999-<?php echo $id_lembaga;?>">KPU</a>
                    </li>
                <?php
                }

                ?>
            </ul>

            <div class="tab-content" id="content-<?=$tabSection;?>" style="overflow: hidden;zoom: 1; margin-top:-10px;">
                <?php
                $t = 0;
                $limit_lembaga = 7;
                $limit_candidat = 10;
                foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $lembaga):

                    if($racekey >= $limit_lembaga){ break; }
                    $tabName = 'tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                    //$lembaga_name =  (strpos($lembaga['page_id'], ' ') == TRUE) ?  $lembaga['page_id'] : $lembaga['page_name'];
                    $lembaga_name  = $lembaga['lembaga_name'];
                    if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];

                    if(!empty($lembaga['start_date']) && $lembaga['start_date'] <> 'None')
                    {
                        $lembaga_date = '<span class="text-date">('.date('d-m-Y', strtotime($lembaga['start_date'])).')</span>';
                    } else {
                        $lembaga_date = '';
                    }

                    ?>
                    <div class="tab-pane" id="conten-<?=$tabName;?>" <?=($t==0) ? ' style="display: block;"' : ' style="display: none;"';?> >
                        <!-- ### isi tab ####################################### -->
                        <h5><?=$lembaga_name.' '.$lembaga_date?></h5>
                        <table summary='' class='home-race-candidates'>
                            <?php
                            $total_score 	= 0;
                            $winner		 	= '';
                            $winner_score	= 0;
                            $pct = 0;
                            foreach($lembaga['kandidat'] as $kandidat) {
                                if(count($kandidat['score']) > 0){
                                    $score = $kandidat['score'];
                                    $kandidat = $kandidat['kandidat'];
                                    if ($score['score_type'] == '1') {
                                        $total_score += (int) $score['score'];
                                    }
                                    if ($score['score'] > $winner_score) {
                                        $winner 			= $kandidat['page_id'];
                                        $winner_score 	= $score['score'];
                                    }
                                }
                            }
                            foreach($lembaga['kandidat'] as $kandidatkey => $kandidat) :

                                $score = $kandidat['score'];
                                $kandidat = $kandidat['kandidat'];
                               // if($kandidatkey >= $limit_candidat){break;}

                                $kandidat_page_id = $kandidat['page_id'];
                                $kandidat_name	= $kandidat['page_name'];
                                $kandidat_link	= base_url() . 'aktor/profile/' . $kandidat['page_id'];

                                $pasangan_page_id = $kandidat['page_id_pasangan'];

                                if($kandidat['page_name_pasangan'] != 'None' && $kandidat['page_name_pasangan'] != 'None' &&
                                   $kandidat['page_name_pasangan'] != 'Tidak Ada Pasangan ' &&  $kandidat['page_name_pasangan'] != 'Pasangan Politisi' &&
                                   ! empty($kandidat['page_name_pasangan'])
                                )
                                {
                                    $pasangan_name	= $kandidat['page_name_pasangan'];
                                } else {
                                    $pasangan_name	= '';
                                }

                                $pasangan_link	= base_url() . 'aktor/profile/' . $kandidat['page_id_pasangan'];

                                $profile_kandidat = $this->redis_slave->get('profile:detail:'.$kandidat_page_id);
                                $arrProfKandidat = @json_decode($profile_kandidat, true);

                                $pasangan_profile_kandidat = $this->redis_slave->get('profile:detail:'.$pasangan_page_id);
                                $arrPasanganProfKandidat = @json_decode($pasangan_profile_kandidat, true);

                                $partai_kandidat_id = $arrProfKandidat['partai_id'];
                                $partai_kandidat_pasangan_id = $arrPasanganProfKandidat['partai_id'];

                                $prof_partai_kandidat = $this->redis_slave->get('profile:detail:'.$partai_kandidat_id);
                                $arrProfpartaiKandidat = @json_decode($prof_partai_kandidat, true);

                                $prof_partai_paskandidat = $this->redis_slave->get('profile:detail:'.$partai_kandidat_pasangan_id);
                                $arrProfpartaiPasKandidat = @json_decode($prof_partai_paskandidat, true);

                                if($val['id_race'] != '275')
                                {
                                   if(isset($arrProfpartaiKandidat['alias']))
                                   {
                                      $kandidat_partai = ($arrProfpartaiKandidat['alias'] <> 'None' ? '('.$arrProfpartaiKandidat['alias'].')' : '');
                                   } else { $kandidat_partai = ''; }
                                } else {
                                    if(isset($arrProfKandidat['alias']))
                                    {
                                        $kandidat_partai = ($arrProfKandidat['alias'] <> 'None' ? '('.$arrProfKandidat['alias'].')' : '');
                                    } else { $kandidat_partai = ''; }
                                }

                                if(isset($arrProfpartaiPasKandidat['alias']))
                                {
                                    $pasangan_partai = ($arrProfpartaiPasKandidat['alias'] <> 'None' ? '('.$arrProfpartaiPasKandidat['alias'].')' : '');
                                } else { $pasangan_partai = ''; }

                                $kandidat_logo_partai = ($arrProfKandidat['icon_partai_url'] <> 'None' ? '<img src="'.$arrProfKandidat['icon_partai_url'].'" width="25px" height="25px" >' : '');
                                $pasangan_logo_partai = ($arrPasanganProfKandidat['icon_partai_url'] <> 'None' ? '<img src="'.$arrPasanganProfKandidat['icon_partai_url'].'" width="25px" height="25px" >' : '');

                                if($kandidat['profile_badge_url'] <> 'None')
                                {
                                    $kandidat_pic = $kandidat['profile_badge_url'];
                                } else {
                                    $kandidat_pic = $arrProfKandidat['icon_partai_url'];
                                    if($kandidat_pic == 'None')
                                    {
                                        $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                                    }
                                }

                                if($kandidat['profile_badge_pasangan_url'] <> 'None')
                                {
                                    $pasangan_pic = $kandidat['profile_badge_pasangan_url'];
                                } else {

                                    $pasangan_pic = $arrPasanganProfKandidat['icon_partai_url'];
                                    if($pasangan_pic == 'None')
                                    {
                                        $pasangan_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                                    }
                                }

                                if(!empty($score)){

                                    if ($score['score_type'] == '0') {
                                        $pct			= number_format($score['score'],2);
                                    } else {
                                        $pct			= number_format(($score['score'] / $total_score)*100,2);
                                    }
                                }

                                if($lembaga_name == 'Rekapitulasi KPU')
                                {
                                    $winner_bar 	= ($winner == $kandidat['page_id'] ) ? 'background-color:#128405;' : '';
                                    $winner_text = ($winner == $kandidat['page_id'] ) ? '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-weight:bold;">Pemenang  '.$pct.' %</div>' : '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-weight:bold;">'.$pct.' %</div>';
                                } else {
                                    $winner_bar 	= ($winner == $kandidat['page_id'] ) ? 'background-color:#11097A;' : '';
                                    $winner_text = ($winner == $kandidat['page_id'] ) ? '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-weight:bold;">'.$pct.' %</div>' : '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-weight:bold;">'.$pct.' %</div>';
                                }

                                ?>
                                <tr>
                                    <td valign="bottom">
                                        <ul class="suksesi-tabel-rekapitulasi-legenda">
                                            <li><span class="race-detail-candidates-num"><?php echo $kandidat['nomor_urut'];?></span></li>
                                        </ul>
                                    </td>

                        <?php
                        if($val['id_race'] != '275')
                        {
                        ?>
                                    <td class='home-race-candidate'>
                                        <a href='<?=$kandidat_link?>'>
                                            <div  title="<?=$kandidat_name?>" style="background:
                                                url(<?=$kandidat_pic?>) no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi">

                                            </div>
                                        </a>
                                    </td>
                                    <td class='home-race-candidate'>
                                        <a href='<?=$pasangan_link?>'>
                                            <div  title="<?=$pasangan_name?>" style="background:
                                                url(<?=$pasangan_pic?>) no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi">

                                            </div>
                                        </a>
                                    </td>
                        <?php
                        } else {
                        ?>
                                    <td class='home-race-candidate' colspan="2" style="width: 150px;text-align: center;">
                                     <a href='<?=$kandidat_link?>'>
                                       <div  title="<?=$kandidat_name?>" style="background:
                                        url(<?=$kandidat_pic?>) no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi">

                                       </div>
                                     </a>
                                    </td>
                        <?php
                        }
                        ?>
                                    <td class='home-race-percentage'>
                                        <div class='home-race-percentage-box'><div class='home-race-percentage-bar' style='width:<?=$pct?>%;<?=$winner_bar;?>'></div><?=$winner_text;?></div>
                                        <p style="margin-top:-7px;font-size: 9px;font-weight: normal;">
                                            <?php echo $kandidat_name.' '.$kandidat_partai;?>
                                        </p>
                                        <p style="font-size: 9px;font-weight: normal;">
                                            <?php echo $pasangan_name.' '.$pasangan_partai; ?>
                                        </p>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>

                    </div>
                    <?php
                    $t++;
                endforeach;

                if(in_array('KPU', $_lembaga))
                {

                } else {
                ?>
                <div class="tab-pane" id="conten-tab-<?php echo $index_trace_status;?>-9999-<?php echo $id_lembaga;?>" style="display: none;">
                  <h5>Rekapitulasi KPU</h5><br/>
                  <p>Data Belum Tersedia</p>
                </div>
                <?php
                }
                ?>
            </div>


        </div>
    </div>
<?php endif;?>




<script>
    $(document).ready(function() {
        var height_suksesi = $('#conten-<?=$tabName;?>').height();
        $('#conten-tab-<?php echo $index_trace_status;?>-9999-<?php echo $id_lembaga;?>').css({'height':+height_suksesi+'px','background-color':'#fff'});
    });
</script>
