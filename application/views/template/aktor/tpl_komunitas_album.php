<?php foreach($photo as $row=>$val){ ?>
<?php if($row % 4 == 0){ ?>
<div class="row-fluid">
<?php } ?>
    <div class="span3">
        <a class="komu-blog-title" href="<?=base_url('komunitas/photo/'.$user['user_id'].'/'.$val['content_id'].'/'.url_title($val['title']));?>">
            <h4><?=(strlen($val['title']) > 16) ? substr(ucwords($val['title']),0, 16) : ucwords($val['title']);?></h4>
        </a>
        <?php if(count($val['photo']) > 0){?>
        <?php foreach($val['photo'] as $ros=>$phot){ ?>
            <?php if($ros == 1) break; ?>
            <img src="<?=thumb_url($phot['attachment_title']);?>">
            <?php } ?>
        <?php }else{ ?>
        <img src="<?=base_url().'assets/images/icon-blog.png';?>">
        <?php } ?>
        <p class="komu-photo-count"><?=$val['count'];?> Photos</p>
    </div>
<!--    <div class="span9">-->
<!--        <a class="komu-blog-title" href="--><?//=base_url('komunitas/blog/'.$user['user_id'].'/'.$val['content_id'].'/'.url_title($val['title']));?><!--"><h4>--><?//=$val['title'];?><!--</h4></a>-->
<!--        <p class="komu-blog-date">--><?//=mdate('%M %d, %Y - %h:%s', strtotime($val['entry_date']));?><!--</p>-->
<!--        <p>-->
<!--            --><?//=trim(strip_tags(character_limiter($val['description'], 200),'p'));?>
<!--        </p>-->
<!--    </div>-->
<?php if($row % 4 == 3 || ($row == count($photo)-1)){ ?>
</div>
<div class="div-line-small"></div>
<?php } ?>

<?php } ?>