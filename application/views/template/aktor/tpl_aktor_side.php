
<div id="politisi-carousel" data-interval="3000" class="carousel-politisi slide">
    <div class="carousel-inner">
        <div class="item active politisi-carousel-image">
            <img class="komu-side-img" src="<?php echo $user['badge_url']; ?>">
            <!--<img class="komu-side-img" src=""-->
        </div>
        <?php
        $k=1; foreach($list_photo as $kes => $item){
            if($k > 4) break;
        ?>
            <div class="item politisi-carousel-image">
                <img src="<?php echo $item['badge_url'];?>">
            </div>
            <?php $k++; } ?>
        <?php
        if(!empty($user['thumb_partai_url']) && $user['thumb_partai_url'] != 'None')
        {
        ?>
           <div class="politisi-carousel-logo-partai">
               <img class="" src="<?php echo $user['thumb_partai_url']; ?>">
           </div>

        <?php
        }
        ?>


    </div>
    <div class="politisi-carousel-control-container">
        <div class="politisi-carousel-control-thumbs">
        <!-- Carousel thumb --->
           <ol class="politisi-carousel-indicators">
               <li data-target="#politisi-carousel" data-slide-to="0">
                   <img class="img-polaroid" src="<?php echo $user['badge_url']; ?>">
               </li>
           <?php
           $i=1; foreach($list_photo as $kem => $itema){
           if($i > 4) break;
           ?>
              <li data-target="#politisi-carousel" data-slide-to="<?php echo $i; ?>">
                  <img class="img-polaroid" src="<?php echo $itema['thumb_url'];?>">
              </li>
           <?php $i++; } ?>

           <?php
           if( $i < 2 || $i < 3 || $i < 4)
           {
              for ($o = 1; $o < (5 - $i); $o++) {
           ?>
               <li class="politisi-detail-noimage"></li>
           <?php
              }
           }
          ?>

          </ol>
        </div>
    </div>
</div>

<!--img class="komu-side-img" src="<?=badge_url($user['attachment_title'], 'politisi/'.$user['page_id']);?>"-->
<?php if($this->member['user_id'] == $user['page_id'] && $this->member['account_id'] == $user['account_id']){ ?>
<div class="row-fluid">
    <div class="span12 score-place-overlay">
        <a id="change_pp" class="btn-flat btn-flat-red pull-right"><i class="icon-camera"></i></a>
    </div>
</div>
<?php } ?>

<div class="row-fluid aktor-side-nav-large aktor-side-nav-bg">
    <div class="span9">
        <a href="<?=base_url('aktor/profile/'.$user['page_id']);?>" class="aktor-side-nav-link-user"><h4><?=ucwords($user['page_name']);?></h4></a>
    </div>
    <?php if($this->member['user_id'] == $user['page_id'] && $this->member['account_id'] == $user['account_id']){ ?>
    <div class="span3">
        <a href="<?=base_url('aktor/profile/'.$user['page_id']);?>" class="btn-flat btn-flat-gray pull-right">Edit</a>
    </div>
    <?php } ?>
</div>
<div class="row-fluid aktor-side-nav">
    <table border="0">
        <!--
        <tr><td class="aktor-side-table-title aktor-side-table-title-col1">Jenis Kelamin</td>
            <td class="aktor-side-table-title aktor-side-table-title-col2"><?=$user['gender']=='F' ? 'Perempuan' : 'Laki Laki';?></td></tr>
        -->
        <tr><td class="aktor-side-table-title aktor-side-table-title-col1">Tanggal Lahir</td>
            <td class="aktor-side-table-title aktor-side-table-title-col2">
            <?php
                if(isset($user['birthday']) && !empty($user['birthday']))
                {
                    echo date('d F Y', strtotime($user['birthday']));
                }
            ?>
            </td>
        </tr>
        <tr>
            <td class="aktor-side-table-title aktor-side-table-title-col1">Partai</td>
            <td class="aktor-side-table-title aktor-side-table-title-col2"><?php echo ($user['partai_name']!='None' ? $user['partai_name'] : '');?></td>
        </tr>
        <tr>
            <td class="aktor-side-table-title aktor-side-table-title-col1">Posisi</td>
            <td class="aktor-side-table-title aktor-side-table-title-col2"><?=$user['posisi'];?></td>
        </tr>
    </table>
</div>
<br>


<div class="row-fluid aktor-side-nav komu-side-nav-bg">
    <div class="span12">
        <a href="<?=base_url('aktor/'.$user['page_id']);?>" class="aktor-side-nav-link" ><span>Wall</span></a>
    </div>
</div>

<div class="row-fluid komu-side-nav">
    <div class="span12">
        <a href="<?=base_url('aktor/scandals/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span>Scandal</span>
            <span class="pull-right"><?=$user['count_scandal'];?></span>
        </a>
    </div>
</div>

<div class="row-fluid komu-side-nav komu-side-nav-bg">
    <div class="span12">
        <a href="<?=base_url('aktor/news/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span>Berita</span>
            <span class="pull-right"><?=$user['count_news'];?></span>
        </a>
    </div>
</div>

<div class="row-fluid komu-side-nav">
    <div class="span12">
        <a href="<?=base_url('aktor/photo/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span>Photos</span>
            <span class="pull-right"><?=$user['count_photo'];?></span>
        </a>
    </div>
</div>
<div class="row-fluid komu-side-nav komu-side-nav-bg">
    <div class="span12">
        <a href="<?=base_url('aktor/follow/'.$user['page_id']);?>" class="komu-side-nav-link">
            <span>Follower</span>
            <span class="pull-right"><?=$user['count_follower'];?></span>
        </a>
    </div>
</div>

<?php foreach($follow as $index=>$val){ ?>
    <?php if($index % 6 == 0){ ?>
        <div class="row-fluid komu-side-nav-large">
    <?php } ?>
    <div class="aktor-side-friend-list">
        <a href="<?=base_url('komunitas/'.$val['page_id']);?>">
            <img src="<?=icon_url($val['attachment_title']);?>">
        </a>
    </div>
    <?php if($index % 6 == 5 || $index == count($follow) - 1){?>
        </div>
    <?php } ?>
<?php } ?>

<br>

<script>
    $(document).ready(function() {
        $('.carousel-politisi').carousel({interval:false})
    });
</script>
