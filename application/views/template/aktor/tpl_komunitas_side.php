<img class="komu-side-img" src="<?=badge_url($user['attachment_title'], 'politisi/'.$user['page_id']);?>">
<?php if($this->member['user_id'] == $user['user_id'] && $this->member['account_id'] == $user['account_id']){ ?>
<div class="row-fluid">
    <div class="span12 score-place-overlay">
        <a id="change_pp" class="btn-flat btn-flat-red pull-right"><i class="icon-camera"></i></a>
    </div>
</div>
<?php } ?>

<div class="row-fluid komu-side-nav-large komu-side-nav-bg">
    <div class="span9">
        <a href="<?=base_url('aktor/profile/'.$user['user_id']);?>" class="komu-side-nav-link-user"><h4><?=ucwords($user['display_name']);?></h4></a>
    </div>
    <?php if($this->member['user_id'] == $user['user_id'] && $this->member['account_id'] == $user['account_id']){ ?>
    <div class="span3">
        <a href="<?=base_url('aktor/profile/'.$user['user_id']);?>" class="btn-flat btn-flat-gray pull-right">Edit</a>
    </div>
    <?php } ?>
</div>

<div class="row-fluid komu-side-nav komu-side-nav-bg">
    <div class="span12">
        <a class="komu-side-nav-link" ><span><?=$user['gender']=='F' ? 'Perempuan' : 'Laki Laki';?>, <?=$user['current_city'];?></span></a>
    </div>
</div>
<br>
<div class="row-fluid komu-side-nav komu-side-nav-bg">
    <div class="span12">
        <a href="<?=base_url('aktor/'.$user['user_id']);?>" class="komu-side-nav-link" ><span>Wall</span></a>
    </div>
</div>

<div class="row-fluid komu-side-nav">
    <div class="span12">
        <a href="#" class="komu-side-nav-link">
            <span>Scandal</span>
            <span class="pull-right"><?=$user['count_photo'];?></span>
        </a>
    </div>
</div>

<div class="row-fluid komu-side-nav komu-side-nav-bg">
    <div class="span12">
        <a href="" class="komu-side-nav-link">
            <span>Berita</span>
            <span class="pull-right"><?=$user['count_photo'];?></span>
        </a>
    </div>
</div>

<div class="row-fluid komu-side-nav">
    <div class="span12">
        <a href="<?=base_url('komunitas/photo/'.$user['user_id']);?>" class="komu-side-nav-link">
            <span>Photos</span>
            <span class="pull-right"><?=$user['count_photo'];?></span>
        </a>
    </div>
</div>
<div class="row-fluid komu-side-nav komu-side-nav-bg">
    <div class="span12">
        <a href="<?=base_url('komunitas/follow/'.$user['user_id']);?>" class="komu-side-nav-link">
            <span>Follow</span>
            <span class="pull-right"><?=$user['count_follow']+$user['count_follower'];?></span>
        </a>
    </div>
</div>

<br>
<div class="row-fluid komu-side-nav-large">
    <div class="span12">
        <a href="<?=base_url('komunitas/friends/'.$user['user_id']);?>" class="komu-side-nav-link-user"><h4>Follower</h4></a>
    </div>
</div>

<?php foreach($user['friend'] as $index=>$val){ ?>
<?php if($index % 6 == 0){ ?>
<div class="row-fluid komu-side-nav-large">
<?php } ?>
    <div class="komu-side-friend-list">
        <a href="<?=base_url('komunitas/'.$val['user_id']);?>">
          <img src="<?=icon_url($val['attachment_title']);?>">
        </a>
    </div>
<?php if($index % 6 == 5 || $index == count($user['friend']) - 1){?>
</div>
<?php } ?>
<?php } ?>

<?php if($this->member['user_id'] == $user['user_id'] && $this->member['account_id'] == $user['account_id']){ ?>
<!--- MODAL UPDATE PP -->
<div id="pp_modal" class="modal hide fade">
    <div class="modal-container">
        <div class="modal-sub-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Ganti Photo Profile <!--<em>(klik pada image untuk menjadikan foto profile)</em>--></h3>
            </div>
            <div class="modal-body">
                <div id="form_foto_side" class="row-fluid">
                    <div class="span5">
                        <form id="form_pp" name="form_pp" method="post" enctype="multipart/form-data" action="<?php echo base_url().'timeline/post_profile_picture';?>" target="ppuploadiframe">
                            <div class="textarea_holder">
                                <input type="file" name="userfile" id="userfile">
                                <div class="pull-right">
                                    <input type="submit" class="btn-flat btn-flat-dark" id="btnpostpp" value="Upload">

                                </div>
                            </div>
                        </form>
                        <iframe name="ppuploadiframe" id="ppuploadiframe" style="display:none;"></iframe>

                    </div>
                </div>
                <div class="row-fluid">
                    <div id="modal_body_pp" class="span12" style="height: 420px;">
                <iframe id="iframe_modal_pp" width="100%" height="420px" src="" frameborder=0 scrolling=no></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <div class="modal-footer">-->
    <!--        <a href="#" class="btn">Close</a>-->
    <!--        <a href="#" class="btn btn-primary">Save changes</a>-->
    <!--    </div>-->
</div>
<?php } ?>
