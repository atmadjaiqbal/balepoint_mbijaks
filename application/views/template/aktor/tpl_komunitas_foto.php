<?php foreach($photo as $row=>$val){ ?>

<div class="row-fluid">
    <div class="span3">
        <img src="<?=thumb_url($val['attachment_title']);?>">

    </div>
    <div class="span9">
        <a data-id='<?=$val['content_id'];?>' data-user="<?=$user['user_id'];?>" class="komu-blog-title" href="#"><h4 class="komu-wall-list-uname"><?=ucwords($val['title']);?> <span class="komu-blog-date"><?=mdate('%M %d, %Y - %h:%s', strtotime($val['entry_date']));?></span></h4> </a>
        <div class="score-place score-place-left score " data-tipe="1" data-id="<?php echo $val['content_id']; ?>" >
            </div>
       <p>
            <?=trim(strip_tags(character_limiter($val['description'], 200),'p'));?>
        </p>
    </div>

</div>
<div class="div-line-small"></div>

<?php } ?>