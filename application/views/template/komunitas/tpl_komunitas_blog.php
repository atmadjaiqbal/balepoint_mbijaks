<?php foreach($blogs as $row=>$val){ ?>
    <div class="row-fluid">
        <div class="row-fluid">
            <a class="komu-blog-title" href="<?=base_url('komunitas/blog/'.$user['user_id'].'/'.$val['content_id'].'/'.url_title($val['title']));?>"><h4><?=$val['title'];?></h4></a>
        </div>
        <div class="row-fluid">
            <div class="span5">
                <p class="komu-blog-date"><?=mdate('%M %d, %Y - %h:%s', strtotime($val['entry_date']));?></p>
            </div>
            <div class="span7">
                
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <?php if(count($val['photo']) > 0){?>
                <?php foreach($val['photo'] as $photo){ ?>
                <img src="<?=thumb_land_url($photo['attachment_title']);?>">
                <?php } ?>
                <?php }else{ ?>
                <img src="<?=base_url().'assets/images/icon-blog.png';?>">
                <?php } ?>
                <div class="score-place score-place-overlay score" data-tipe="0" data-id="<?php echo $val['content_id']; ?>" >
                </div>
            </div>
            <div class="span8">
                <p>
                    <?=trim(strip_tags(character_limiter($val['content_blog'], 400),'p'));?>
                </p>

            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 text-right">
                <a href="<?=base_url('komunitas/blog/'.$user['user_id'].'/'.$val['content_id'].'/'.url_title($val['title']));?>" class="btn-flat btn-flat-red">MORE</a>
            </div>
        </div>
    </div>
    <div class="div-line-small"></div>

<?php } ?>

<script>
    if(<?=$count_result;?> < 10 && <?=$count_result;?> != -1){
        $('#blog_load_more').parent().parent().remove();
//        $('#div_line_bottom').remove();
    }
</script>