<?php
if(!empty($news))
{
   foreach($news as $berita)
   {
     if(!empty($berita['title']))
     {                  	
	
	
    $news_url = base_url() . "news/article/" . (isset($berita['topic_id']) ? $berita['topic_id'] : '') . "-" .
                (isset($berita['id']) ? $berita['id'] : '') . "/" . trim( str_replace('/', '', (isset($berita['slug']) ? $berita['slug'] : '')));
    $short_title = (isset($berita['title']) ? (strlen ($berita['title']) > 26) ? substr($berita['title'], 0, 26). '...' : $berita['title'] : '');
?>
<div class="profile-news-section">
  <a href="<?php echo $news_url; ?>"><h4 title="<?php echo isset($berita['title']) ? $berita['title'] : ''; ?>"><?php echo $berita['title']; ?></h4></a>
  <div class="news-date"><?php echo isset($berita['date']) ? mdate('%d %M %Y - %h:%i', strtotime($berita['date'])) : ''; ?></div>
  <div class="profile-news-image"> 
  <img alt='<?php echo isset($berita['title']) ? $berita['title'] : ''; ?>' src="<?php echo isset($berita['image_thumbnail']) ? $berita['image_thumbnail'] : ''; ?>">
  <div class="score-place score-place-overlay score" data-id="<?php echo isset($berita['content_id']) ? $berita['content_id'] : ''; ?>" ></div>
  </div>
  <div class="profile-news-article">
    <div class="related-politicians clearfix">
      <div class="pull-left related-politician-label"><span>Politisi Terkait</span></div>
    <?php if (!empty($berita['news_profile'])){ ?>
        <?php foreach ($berita['news_profile'] as $key => $pro) { ?>
            <?php if($key === 4) break; ?>
            <div class="pull-left related-politician-img">
                <img title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
            </div>
        <?php } ?>
<!--
        <?php if(count($berita['news_profile']) > 4){ ?>
            <div class="span2">
                <span>More</span>
            </div>
        <?php } ?>
-->
    <?php }else{ ?>
        <div class="pull-left related-politician-img"><span>Tidak ada politisi terkait</span></div>
    <?php } ?>

    </div>
  <div class="time-line-content" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo isset($berita['content_id']) ? $berita['content_id'] : '0'; ?>"></div>
  <div class="profile-isi-berita">
      <p><?php echo isset($berita['excerpt']) ? (strlen($berita['excerpt']) > 200 ? substr($berita['excerpt'], 0, 200) .' ...' : $berita['excerpt']) : ''; ?></p>
  </div>
 </div>  

<script>
/*
    $(function(){

        comment_list('#comment_<?php echo $berita['content_id'];?>');

        $('#comment_load_more_<?php echo $berita['content_id'];?>').click(function(e){
            e.preventDefault();
            comment_list('#comment_<?php echo $berita['content_id'];?>');
        });

        $('#send_coment_<?php echo $berita['content_id'];?>').click(function(e){
            var id = $('#comment_type_<?php echo $berita['content_id'];?>').data('id');
            var val = $('#comment_type_<?php echo $berita['content_id'];?>').val();
            var please = '<div id="comment_loader_<?php echo $berita['content_id'];?>" class="loader"></div>';
            $('#comment_<?php echo $berita['content_id'];?>').prepend(please);
            $('#comment_loader_<?php echo $berita['content_id'];?>').css('visibility', 'visible');
            $.post(Settings.base_url+'timeline/post_comment/', {'id':id, 'val':val}, function(data){
                $('#comment_loader_<?php echo $berita['content_id'];?>').remove();
                if(data.rcode == 'ok'){
                    $('#comment_<?php echo $berita['content_id'];?>').prepend(data.msg);
                    $('.score').each(function(a, b){
                        var id = $(b).data('id');
                        var tipe = '0';
                        if($(b).data('tipe')){
                            tipe = $(b).data('tipe');
                        }
                        $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                    });
                }else{
                    alert(data.msg);
                }
                $('#comment_type_<?php echo $berita['content_id'];?>').val('');
            })
        });

    });

    function comment_list(holder)
    {
        var id = $(holder).data('id');
        var page = $(holder).data('page');

        var please = '<div id="comment_loader_<?php echo $berita['content_id'];?>" class="loader"></div>';
        $(holder).append(please);
        $('#comment_loader_<?php echo $berita['content_id'];?>').css('visibility', 'visible');

        var uri = Settings.base_url+'timeline/content_comment/'+ id + '/'+page;
        $.get(uri, function(data){
            $(holder).append(data);
            $(holder).data('page', parseInt(page) + 1);
            $('#comment_loader_<?php echo $berita['content_id'];?>').hide('blind').remove();
        })

    }
*/
</script>
<!--
  <div class="row-fluid">
     <h5>BERI KOMENTAR</h5>
     <div class="media media-comment">
            <div class="pull-left media-side-left">
                <div style="background:
                    url('<?php //echo $comment['image_url']; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                </div>
            </div>

            <div class="media-body">
                <div class="row-fluid">
                    <input id='comment_type_<?php echo $berita['content_id'];?>' data-id="<?=$berita['content_id'];?>" type="text" name="comment" class="media-input" <?php echo ((!$this->member) ? 'disabled="disabled"' : '');?>>
                </div>
                <div class="row-fluid">
                    <div class="span12 text-right">
                    <?php
                    if(!$this->member)
                    {
                    ?>
                        <a class="btn-flat btn-flat-dark">Register</a>
                        <a class="btn-flat btn-flat-dark">Login</a>
                    <?php
                    } else {
                    ?>
                        <a id="send_comment_<?php echo $berita['content_id'];?>" class="btn-flat btn-flat-gray">Send</a>
                    <?php
                    }
                    ?>
                    </div>
                </div>
            </div>
     </div>
  </div>
  <div id="comment_<?php echo $berita['content_id'];?>" data-page="1" data-id="<?php echo $berita['content_id'];?>" class="row-fluid comment-container"></div>
  <div class="row-fluid komu-follow-list komu-wall-list-bg">
    <div class="span12 text-center">
       <a data-page="1" data-id="<?php echo $berita['content_id'];?>" id="comment_load_more_<?php echo $berita['content_id'];?>" class="komu-follow-list-more" href="#">LOAD MORE</a>
    </div>
  </div>
-->    

</div>
<?php
    }
  }
}
?>
