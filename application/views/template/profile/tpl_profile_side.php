<div class="badge-info">
    <div style="width:300px; height:300px;">
      <a href="<?=base_url('profile/detailprofile/'.$detailProf['page_id']);?>"><img src="<?php echo $detailProf['badge_url']; ?>"></a>
<?php if($this->member['user_id'] == $user['user_id'] && $this->member['account_id'] == $user['account_id']){ ?>
      <div class="row-fluid">
        <div class="span12 score-place-overlay">
           <a id="change_pp" class="btn-flat btn-flat-red pull-right"><i class="icon-camera"></i></a>
        </div>
      </div>
<?php } ?>
    </div>
    
    <div class="row-fluid komu-side-nav-large komu-side-nav-bg">
      <div class="span9">
        <a href="<?=base_url('aktor/profile/'.$user['user_id']);?>" class="komu-side-nav-link-user"><h4><?=ucwords($user['display_name']);?></h4></a>
      </div>
      <?php if($this->member['user_id'] == $user['user_id'] && $this->member['account_id'] == $user['account_id']){ ?>
      <div class="span3">
        <a href="<?=base_url('aktor/profile/'.$user['user_id']);?>" class="btn-flat btn-flat-gray pull-right">Edit</a>
      </div>
      <?php } ?>
    </div>
        
    <div class="badge-detail-info">
      <div class="row-fluid komu-side-nav">
         <div class="span12">
            <a href="<?=base_url('profile/wall/'.$detailProf['page_id']);?>" class="komu-side-nav-link" ><span>Wall</span></a>
         </div>
      </div>

      <div class="row-fluid komu-side-nav komu-side-nav-bg">
         <div class="span12">
            <a href="<?=base_url('profile/listScandals/'.$detailProf['page_id']);?>" class="komu-side-nav-link">
               <span>Scandal</span>
               <span class="pull-right"><?php echo $count_scandal; ?></span>
            </a>
         </div>
      </div>

      <div class="row-fluid komu-side-nav">
         <div class="span12">
            <a href="<?=base_url('profile/listArticle/'.$detailProf['page_id']);?>" class="komu-side-nav-link">
               <span>Berita</span>
               <span class="pull-right"><?php echo $count_news; ?></span>
            </a>
         </div>
      </div>

      <div class="row-fluid komu-side-nav  komu-side-nav-bg">
         <div class="span12">
            <a href="<?=base_url('komunitas/photo/'.$detailProf['page_id']);?>" class="komu-side-nav-link">
               <span>Photos</span>
               <span class="pull-right"><?php echo $count_photo; ?></span>
            </a>
         </div>
      </div>

      <div class="row-fluid komu-side-nav">
         <div class="span12">
            <a href="<?=base_url('komunitas/follow/'.$detailProf['page_id']);?>" class="komu-side-nav-link">
               <span>Follow</span>
               <span class="pull-right"><!--<?=$user['count_follow']+$user['count_follower'];?>--></span>
            </a>
         </div>
      </div>
   </div>
</div>

