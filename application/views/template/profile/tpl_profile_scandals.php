<?php
if(!empty($skandals))
{

//echo "<pre>";
//print_r($skandals);
//echo "</pre>";

  $skandal_url = base_url() . 'scandal/index/'.$skandals['scandal_id'].'-'.urltitle($skandals['scandal_title']);
  $skandal_photos = (count($skandals['photos']) > 0) ? $skandals['photos'][0]['large_url'] : '';
  $short_title = (isset($skandals['scandal_title']) ? (strlen ($skandals['scandal_title']) > 26) ? substr($skandals['scandal_title'], 0, 26). '...' : $skandals['scandal_title'] : '');
  $css_image = 'news-image-skandal';
  $limit_terkait = 7;
  if(isset($skandals['tipe'])){
    $limit_terkait = 4;
    $css_image .= '-'.$skandals['tipe'];
    $skandal_photos = (count($skandals['photos']) > 0) ? $skandals['photos'][0]['thumb_landscape_url'] : '';
  }
?>

<div class="profile-skandal-section">
  <a href="<?php echo $skandal_url; ?>"><h4 title="<?php echo $skandals['scandal_title']; ?>"><?php echo $short_title; ?></h4></a>
  <img class="profile-news-image" alt='<?php echo isset($skandals['scandal_title']) ? $skandals['scandal_title'] : ''; ?>' src="<?php echo isset($skandals['photos'][0]['large_url']) ? $skandals['photos'][0]['large_url']  : ''; ?>">
  <div class="score-place score-place-overlay score" data-id="<?php echo isset($skandals['content_id']) ? $skandals['content_id'] : ''; ?>" ></div>
  <div class="related-politicians">

    <div class="pull-left related-politician-label"><span>Politisi Terkait</span></div>
    <?php if (!empty($skandals['players'])){ ?>
        <?php foreach ($skandals['players'] as $key => $pro) { ?>
            <?php if($key === 4) break; ?>
            <div class="pull-left related-politician-img">
                <img title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
            </div>
        <?php } ?>
<!--
        <?php if(count($berita['news_profile']) > 4){ ?>
            <div class="span2">
                <span>More</span>
            </div>
        <?php } ?>
        
        

-->
    <?php }else{ ?>
        <div class="pull-left related-politician-img"><span>Tidak ada politisi terkait</span></div>
    <?php } ?>


     <div class="pull-left related-politician-label-status">Status</div><div class="pull-left related-politician-status"><?php echo ucwords($skandals['details']['status']); ?></div>
     <div class="pull-left related-politician-label-status">Dampak</div><div class="pull-left related-politician-status"><?php echo (strlen($skandals['details']['uang']) > 60 ? substr($skandals['details']['uang'], 0, 70) .'...' : $skandals['details']['uang']); ?></div>
  </div>
    
  
  <div class="time-line-content" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo isset($skandals['content_id']) ? $skandals['content_id'] : '0'; ?>"></div>
</div>
<?php
}
?>
