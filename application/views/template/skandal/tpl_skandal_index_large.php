<?php

$_judul = str_replace('!', '',$val['title']);
$_judul = str_replace('#', '',$_judul);
$_judul = str_replace('$', '',$_judul);

$skandal_url = base_url() . 'scandal/index/'.$val['scandal_id'].'-'.urltitle($_judul);

$skandal_photos = (count($val['photos']) > 0) ? $val['photos'][count($val['photos'])-1]['large_url'] : '';
$css_image = 'news-image-skandal';
$limit_terkait = 11;
if(isset($val['tipe'])){
    $limit_terkait = 4;
    $css_image .= '-'.$val['tipe'];
    $skandal_photos = (count($val['photos']) > 0) ? $val['photos'][count($val['photos'])-1]['large_url'] : '';
}

$headx_photos = null; // (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
$headm_photos = null; //(count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';

foreach($val['photos'] as $kes => $item){
    if(key_exists('type', $item)){
        if(intval($item['type']) == 1){
            $headx_photos = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                $headx_photos = $item['original_url'];
            }

            $headm_photos = $item['thumb_url'];
            break;
        }
    }
}


?>

<h4 title="<?php echo $val['title']; ?>"><a href="<?php echo $skandal_url; ?>"><?php echo $val['title']; // (strlen($val['title']) > 40) ? substr($val['title'], 0, 40).'...' : $val['title']; ?></a></h4>
<div id="large-carousel<?php echo $val['content_id']; ?>" data-interval="3000" data-pause="hover" class="carousel-large carousel slide">
  <div class="carousel-inner">
      <?php if($headx_photos != null){ ?>
      <div class="active item photo-carousel">
          <img src="<?=$headx_photos;?>">
      </div>
      <?php } ?>

    <?php $_x_active='';
      $k= ($headx_photos != null) ? 1 : 0;
      $max = 5; foreach($val['photos'] as $kes => $item){ ?>
        <?php
        if($k >= $max) break;

        if(isset($item['type']))
        {
            if($item['type'] == 1)
            {
                $max += 1;
                continue;
                $_x_active = 'active';
            } else {
                if($k == 0 && $headx_photos == null)
                {
                  $_x_active = 'active';
                } else {
                  $_x_active = '';
                }
            }
        } else {
            if($k == 0 && $headx_photos == null) { $_x_active = 'active'; } else { $_x_active = ''; }
        }
        ?>
        <div class="<?php echo $_x_active; ?> item large-carousel-image">
            <?php
            $img_uri_car = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){

                $img_uri_car = $item['original_url'];
            }
            ?>
            <img class="<?php echo $css_image; ?> lazy" alt='<?php echo $val['title']; ?>' src="<?=$img_uri_car;?>">
        </div>
        <?php $k++; } ?>

  </div>
  <div class="score-place score-place-overlay score" data-id="<?php echo $val['content_id']; ?>" ></div>
  <div class="large-control-container">
    <div class="large-carousel-control">
        <!-- Carousel thumb -->
        <ol class="large-carousel-indicators">
        <?php if($headx_photos != null){ ?>
        <li data-target="#large-carousel<?php echo $val['content_id']; ?>" data-slide-to="0" class="active">
            <img class="img-polaroid img-polaroid-scandal" src="<?php echo $headm_photos ;?>" >
        </li>
        <?php } ?>

        <?php $set_active='';
        $i= ($headx_photos != null) ? '1' : 0;
        $maxm = 5; foreach($val['photos'] as $kem => $item){ ?>
        <?php
        if($i >= $maxm) break;
        if(isset($item['type']))
        {
            if($item['type'] == 1) {
                $maxm += 1;
                continue;
                $set_active = 'active';
            } elseif($i == 0 && $headx_photos == null) {
                $set_active = 'active';
            }
        } else {
            if($i == 0 && $headx_photos == null) {
                $set_active = 'active';
            } else { $set_active = '';}
        }
        ?>
        <li data-target="#large-carousel<?php echo $val['content_id']; ?>" data-slide-to="<?php echo $i; ?>" class="<?php echo $set_active; ?>">
            <img class="img-polaroid img-polaroid-scandal" src="<?php echo $item['thumb_url'] ;?>" >
        </li>

        <?php $i++; } ?>

        <?php
//            if($i < 6 || $i < 5 || $i < 4 || $i < 3 || $i < 2)
//            {
//                for ($o = 1; $o <= (6 - $i); $o++) {
//                    ?>
        <!--                    <li style="margin-right:0px;">-->
        <!--                        <div class="img-polaroid img-polaroid-scandal"></div>-->
        <!--                    </li>-->
        <!--                --><?php
//
//                }
//            }
        ?>

    </ol>
    </div>
  </div>


<!--img class="<?php //echo $css_image; ?> lazy" alt='<?php //echo $val['title']; ?>' data-original="<?php // echo $skandal_photos; ?>"-->
    <div class="skandal-detail-under-slider">
        <hr class="hr-black">

        <div class="row-fluid" >
        <div class=""> 
            <span><strong>&nbsp;Politisi Terkait</strong></span>
            <?php if (count($val['players']) > 11){ ?>
                <a class="pull-right" href="<?php echo $skandal_url; ?>">More &nbsp;</a>    
            <?php } ?>
        </div>
        
        <!-- <div class="span3 span2-pd-left"><p>Politisi Terkait</p></div> -->
        <div class="">
            <?php if (count($val['players']) > 0){
                // if(count($skandal['players']) > 6) $limit_terkait = 6;
             ?>
                <ul class="ul-img-hr">
                <?php foreach ($val['players'] as $key => $pro) { ?>
                    <?php if($key === $limit_terkait) break; ?>
                <?php
                if( $pro['profile_icon_url'] != 'None'){
                    $img_politik = $pro['profile_icon_url'];
                    if(!file_get_contents($img_politik)){
                        $img_politik = base_url().'assets/images/icon/no-image-politisi.png';
                    }
                } else {
                    $img_politik = base_url().'assets/images/icon/no-image-politisi.png';
                }
                ?>
                    <li class="">
                        <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                            <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo $img_politik; ?>">
                        </a>
                    </li>
                <?php } ?>
                <?php //if (count($val['players']) > 6){ ?>
                    <!-- <div class="politisi-terkait-skandal">
                        <span><a href="<?php echo $skandal_url; ?>">More</a></span>
                    </div> -->
                <?php //} ?>
                </ul>
            <?php }else{ ?>
                <span>Tidak ada politisi terkait</span>
            <?php } ?>
        </div>

    </div>
        <hr class="hr-black">
        <div class="row-fluid" >
            <div class="span2 span2-pd-left"><span>Status</span></div>
            <div class="span10"><span><?php echo ucwords($val['status']); ?></span></div>
        </div>
        <hr class="hr-black">
        <div class="row-fluid" >
            <div class="span2 span2-pd-left"><span>Kejadian</span></div>
            <div class="span10"><span><?php echo mdate('%d %M %Y', strtotime($val['date'])); ?></span></div>
        </div>
        <hr class="hr-black">
        <div class="row-fluid" >
            <div class="span2 span2-pd-left"><span>Dampak</span></div>
            <div class="span10"><span><?php echo $val['uang']; ?></span></div>
        </div>
    </div>

</div>

    <div class="time-line-content" data-cat="skandal" data-uri="<?php echo $val['content_id']; ?>">
        <?php //$this->load->view('template/tpl_sub_comunity_small'); ?>
    </div>


<script>
    $(document).ready(function() {
        $('.carousel-large').carousel({interval:false})
    });

</script>