<?php
$skandal_url = base_url() . 'scandal/index/'.$skandal['scandal_id'].'-'.urltitle($skandal['title']);

$skandal_photos = (count($skandal['photos']) > 0) ? $skandal['photos'][count($skandal['photos'])-1]['large_url'] : '';
$css_image = 'news-image-skandal';
$limit_terkait = 7;
// if(isset($skandal['tipe'])){
//     $limit_terkait = 4;
//     $css_image .= '-'.$skandal['tipe'];
//     $skandal_photos = (count($skandal['photos']) > 0) ?  $skandal['photos'][count($skandal['photos'])-1]['thumb_landscape_url'] : '';
// }

$headx_photos = (count($skandal['photos']) > 0) ? $skandal['photos'][0]['large_url'] : '';
$headm_photos = (count($skandal['photos']) > 0) ? $skandal['photos'][0]['thumb_url'] : '';

foreach($skandal['photos'] as $kes => $item){
    if(key_exists('type', $item)){
        if(intval($item['type']) == 1){
            $headx_photos = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                $headx_photos = $item['original_url'];
            }

            $headm_photos = $item['thumb_url'];
            break;
        }
    }
}

?>
<div class="span4" xmlns="http://www.w3.org/1999/html">
	<div class="hp-place">
        <div class="home-title-section hp-label hp-label-skandal">
            <span class="hitam"><a href="#">SKANDAL</a></span>
        </div>
		<a href="<?php echo $skandal_url; ?>"><h4 title="<?php echo $skandal['title']; ?>"><?php echo $skandal['title']; //(strlen($skandal['title']) > 26) ? substr($skandal['title'], 0, 26).'...' : $skandal['title']; ?></h4></a>
		
		<div id="carousel-footer" class="carousel-terkait carousel slide">
		    <div class="carousel-inner">
		        <?php if($headx_photos != null){ ?>
		        <div class="active item terkait-carousel-image">
		            <img src="<?=$headx_photos;?>">
		        </div>
		        <?php } ?>

		        <?php $_x_active='';
		        $k= ($headx_photos != null) ? 1 : 0;
		        $max = 5; foreach($skandal['photos'] as $kes => $item){ ?>
		        <?php
		        if($k >= $max) break;

		        if(isset($item['type']))
		        {
		            if($item['type'] == 1)
		            {
		                $max += 1;
		                continue;
		                $_x_active = 'active';
		            } else {
		                if($k == 0 && $headx_photos == null)
		                {
		                    $_x_active = 'active';
		                } else {
		                    $_x_active = '';
		                }
		            }
		        } else {
		            if($k == 0 && $headx_photos == null) { $_x_active = 'active'; } else { $_x_active = ''; }
		        }
		        ?>
		        <div class="<?php echo $_x_active; ?> item terkait-carousel-image">
		            <?php
		            $img_uri_car = $item['large_url'];
		            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){

		                $img_uri_car = $item['original_url'];
		            }
		            ?>
		            <img class="<?php echo $css_image; ?> lazy" alt='<?php echo $skandal['title']; ?>' src="<?=$img_uri_car;?>">
		        </div>
		        <?php $k++; } ?>

		    </div>
		    <div class="score-place score-place-overlay score" data-id="<?php echo $skandal['content_id']; ?>"></div>

		    <div class="terkait-control-container">
		        <div class="terkait-carousel-control">
		            <!-- Carousel thumb -->
		            <ol class="terkait-carousel-indicators">
		                <?php if($headx_photos != null){ ?>
		                <li data-target="#carousel-footer" data-slide-to="0" class="active">
		                    <img class="img-polaroid img-polaroid-scandal-mini" src="<?php echo $headm_photos ;?>" >
		                </li>
		                <?php } ?>

		                <?php $set_active='';
		                $i= ($headx_photos != null) ? '1' : 0;
		                $maxm = 5; foreach($skandal['photos'] as $kem => $item){ ?>
		                <?php
		                if($i >= $maxm) break;
		                if(isset($item['type']))
		                {
		                    if($item['type'] == 1) {
		                        $maxm += 1;
		                        continue;
		                        $set_active = 'active';
		                    } elseif($i == 0 && $headx_photos == null) {
		                        $set_active = 'active';
		                    }
		                } else {
		                    if($i == 0 && $headx_photos == null) {
		                        $set_active = 'active';
		                    } else { $set_active = '';}
		                }
		                ?>
		                <li data-target="#carousel-footer" data-slide-to="<?php echo $i; ?>" class="<?php echo $set_active; ?>">
		                    <img class="img-polaroid img-polaroid-scandal-mini" src="<?php echo $item['thumb_url'] ;?>" >
		                </li>

		                <?php $i++; } ?>

		                <?php
		//            if($i < 6 || $i < 5 || $i < 4 || $i < 3 || $i < 2)
		//            {
		//                for ($o = 1; $o <= (6 - $i); $o++) {
		//                    ?>
		                <!--                    <li style="margin-right:0px;">-->
		                <!--                        <div class="img-polaroid img-polaroid-scandal"></div>-->
		                <!--                    </li>-->
		                <!--                --><?php
		//
		//                }
		//            }
		                ?>

		            </ol>
		        </div>
		    </div>
		</div>

		<div class="skandal-detail-under-slider">
		    <hr class="hr-black">
		    <div class="row-fluid" >
		    	<div class=""> 
		    		<span><strong>&nbsp;Politisi Terkait</strong></span>
		    		<?php if (count($skandal['players']) > 6){ ?>
		                <a class="pull-right" href="<?php echo $skandal_url; ?>">More &nbsp;</a>    
	                <?php } ?>
		    	</div>
		    	
		        <!-- <div class="span3 span2-pd-left"><p>Politisi Terkait</p></div> -->
		        <div class="">
		            <?php if (count($skandal['players']) > 0){
		            	// if(count($skandal['players']) > 6) $limit_terkait = 6;
		             ?>
		             	<ul class="ul-img-hr">
		                <?php foreach ($skandal['players'] as $key => $pro) { ?>
		                    <?php if($key === $limit_terkait) break; ?>
		                <?php
		                if( $pro['profile_icon_url'] != 'None'){
		                    $img_politik = $pro['profile_icon_url'];
		                    if(!file_get_contents($img_politik)){
		                        $img_politik = base_url().'assets/images/icon/no-image-politisi.png';
		                    }
		                } else {
		                    $img_politik = base_url().'assets/images/icon/no-image-politisi.png';
		                }
		                ?>
		                    <li class="">
		                        <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
		                            <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo $img_politik; ?>">
		                        </a>
		                    </li>
		                <?php } ?>
		                <?php if (count($skandal['players']) > 6){ ?>
		                    <!-- <div class="politisi-terkait-skandal">
		                        <span><a href="<?php echo $skandal_url; ?>">More</a></span>
		                    </div> -->
		                <?php } ?>
		                </ul>
		            <?php }else{ ?>
		                <span>Tidak ada politisi terkait</span>
		            <?php } ?>
		        </div>

		    </div>
		    <!-- <hr class="hr-black"> -->
		    <!-- <div class="row-fluid" >
		        <div class="span3 span2-pd-left"><span>Status</span></div>
		        <div class="span9"><span><?php echo ucwords($skandal['status']); ?></span></div>
		    </div> -->
		    <!-- <hr class="hr-black">
		    <div class="row-fluid" >
		        <div class="span3 span2-pd-left"><p>Kejadian</p></div>
		        <div class="span9"><span><?php echo mdate('%d %M %Y', strtotime($skandal['date'])); ?></span></div>
		    </div>
		    <hr class="hr-black">
		    <div class="row-fluid" >
		        <div class="span3 span2-pd-left"><p>Dampak</p></div>
		        <div class="span9"><span><?php echo $skandal['uang']; ?></span></div>
		    </div> -->
		</div>

		<!-- <img class="news-image lazy" alt='<?php echo $skandal['title']; ?>' data-original="<?php echo $headx_photos; ?>">
        <div class="score-place score-place-overlay score" data-id="<?php echo $skandal['content_id']; ?>" >
        </div>
		<div class="row-fluid politisi-terkait-container politisi-terkait-container-skandal">
			<div class="span2"><p>Politisi Terkait</p></div>
			<?php if (count($skandal['players']) > 0){ ?>
			<?php foreach ($skandal['players'] as $key => $pro) { ?>
				<?php if($key === 4) break; ?>
				<div class="span2">
					<a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                    <img class="img-polaroid img-polaroid-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo $pro['profile_icon_url']; ?>">
                    </a>
				</div>
			<?php } ?>
				<?php if (count($skandal['players']) > 4){ ?>
				<div class="span2">
					<span>More</span>
				</div>
				<?php } ?>
			<?php }else{ ?>
			<div class="span6"><span>Tidak ada politisi terkait</span></div>
			<?php } ?>
			
		</div> -->

		<div class="time-line-content" data-cat="skandal" data-uri="<?php echo $skandal['content_id']; ?>">
	
		</div>

	</div>
	
	
</div>

<script>
    $(document).ready(function() {
        $('.carousel-footer').carousel({interval:false})
    });

</script>


