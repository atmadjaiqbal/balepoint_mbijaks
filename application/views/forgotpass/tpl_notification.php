<div class="container" style="margin-top: 31px;">
  <div class="sub-header-container">
	<div class="logo-small-container">
		<img src="<?php echo base_url('assets/images/logo.png'); ?>" >
	</div>
	<div class="right-container">
		<div class="banner">
			<img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
		</div>
		<div class="category-news">
			<div class="category-news-title category-news-title-right category-news-title-merah">
				<h1><?php echo $message_title; ?></h1>
			</div>
			
		</div>
	</div>
  </div>	
  <div id="loginpage" class="sub-header-container">    
<div class='aboutbijaks'>

    <div class='hot-profile'>
      <ul>
<?php
       foreach ($hot_profile as $row)
       {
          $_profile_url = base_url().'aktor/profile/'.$row['page_id'];
?>
          <li>
             <span data-id="<?php echo $row['page_id']; ?>">
               <img src='<?php echo $row['badge_url']; ?>' data-src='<?php echo $row['badge_url']; ?>'
                    title="<?php echo $row['page_name']; ?>" alt=""/>
             </span>
          </li>
<?php
       }
?>
      </ul>
    </div>
	
<p><strong>Bijaks.net</strong> merupakan portal Social Network (SN) politik pertama dan utama di Indonesia, yang memadukan unsur jaringan sosial, berita dan arsip terkait dunia politik Indonesia, mulai dari poltisi, partai politik maupun dinamika politik Indonesia.</p>
<p>Setiap hari, <strong>Bijaks.net</strong> menghadirkan sesuatu yang baru dan fresh, baik dari sisi pemberitaan maupun kumpulan arsip politik Indonesia, mulai dari eksekutif, legislatif, maupun yudikatif.</p>
</div>

<div class="loginarea">
			
	<div class="notice-message">
	  <p><?php echo $message;?></p>
	  <p><a href="<?php echo base_url().'home/login';?>">Click here to login to your account.</a></p>
	</div>

</div>
  </div>
</div>
<br/>
