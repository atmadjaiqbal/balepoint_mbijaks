<div class="container" style="margin-top: 31px;">
  <div class="sub-header-container">
	<div class="logo-small-container">
		<img src="<?php echo base_url('assets/images/logo.png'); ?>" >
	</div>
	<div class="right-container">
		<div class="banner">
			<img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
		</div>
		<div class="category-news">
			<div class="category-news-title category-news-title-right category-news-title-merah">
				<h1>ERROR 404</h1>
			</div>
			
		</div>
	</div>
  </div>	
  <div id="error-box" class="sub-header-container"> 
   <div class="error-title"></div>
   <div class="error-notice">  
    <p>The Page you are looking for cannot be found</p> 
   </div>
  </div>
</div>
<br/>
