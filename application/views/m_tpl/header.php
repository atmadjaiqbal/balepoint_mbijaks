<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta http-equiv="expires" content="1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($head_title)) ? 'Bijaks | '.$head_title : 'Bijaks | Life & Politics';?></title>

    <meta name="description" content="Bijaks Life and Politics" />
    <?php //$_politisi = ''; if(!empty($polterkait)) {foreach($polterkait as $rowpol) { $_politisi .= $rowpol .', '; } } ?>
    <?php $_topnews = ''; if(!empty($topnews)) {foreach($topnews as $rwtnw) { $_topnews .= $rwtnw .', '; } } ?>
    <meta name="keywords" content="bijaks, life & politics, <?php echo $_topnews;?> caleg, korupsi, suksesi, hukum, parlemen, presiden, <?php echo (isset($polterkait)) ? $polterkait : '';?>, politik, skandal, survey, politisi, komunitas, partai, pemilu" />
    <meta charset="utf-8" />
    <meta name="author" content="Bijaks Team" />
    <meta name="google-site-verification" content="rejsgiic-Lqkw0BFeUiclU5GF3JUfwZOVx_VTcXJHTM" />
    <meta name="robots" content="index,follow">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })
            (window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-40169954-1', 'bijaks.net');
        ga('require', 'displayfeatures');
        ga('send', 'social', 'socialNetwork', 'socialAction', 'socialTarget', {'page': 'optPagePath'});
        ga('send', 'pageview');
    </script>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="Cache-Control" CONTENT="private, max-age=5400, pre-check=5400"/>
    <meta http-equiv="Expires" CONTENT="<?php echo date(DATE_RFC822,strtotime("1 day")); ?>"/>

    <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.ico');?>">

    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/mobile/m_bijaks.css');?>">
    <!-- <link rel="stylesheet" href="<?php //echo base_url('assets/css/mobile/m.bijaks.min-0.1.css');?>"> -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url('assets/js/jquery-1.9.1.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/masonry.pkgd.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>

    <?php if (!empty($scripts)) : ?>
        <?php foreach ($scripts as $script) : ?>
            <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/<?php echo $script; ?>"></script>
        <?php endforeach; ?>
    <?php endif; ?>
    <script src="<?php echo base_url('assets/js/jquery.lazyload.min');?>"></script>
    <script type="text/javascript">
        var Settings = <?php
        if(str_replace("http://","",current_url()) == $_SERVER['SERVER_ADDR'])
        {
           $settings = array('base_url' => $_SERVER['SERVER_ADDR']);
        } else {
	       $settings = array('base_url' => base_url());
        }
	    echo json_encode($settings);
	    ?>;
    </script>
    
</head>
<style type="text/css">
#greetmember{
    margin-top: 16px;
    color: #fff;
}
</style>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=408067049378496";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<!-- HEADER -->
<div class="navbar navbar-nomarginbottom navbar-inverse navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url();?>"><img class="bjk-logo" src="<?php echo base_url('assets/images/bijaks_logo.png');?>"></a>
            <form class="pull-right form-search" id="form_search" action="<?php echo base_url().'search';?>" role="form">
                <input type="text" placeholder="Search" name="term" class="form-control input-sm input-search">
                <input type="submit" class="btn btn-default btn-sm btn-search" value="Cari">
            </form>
            <?php /*
            <?php 
            if($this->member){
            ?>
            <div id="greetmember">Hi <?php echo $this->member['username'];?></div>
            <?php    
            }
            ?>
            */?>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <!--li class=""><a href="< ?php echo base_url('headline');?>">BERITA</a></li-->
                <li class=""><a href="<?php echo base_url('scandal');?>">SKANDAL</a></li>
                <li class=""><a href="<?php echo base_url('politik');?>">PROFILE</a></li>

                <?php if($this->member){?>
                <li class=""><a href="<?php echo base_url('komunitas/profile/'.$this->member['page_id']);?>">MY PROFILE</a></li>
                <?php }else{ ?>
                <li class=""><a href="<?php echo base_url('home/login');?>">LOGIN</a></li>
                <?php };?>
                
                <?php /*
                <?php if($this->member){?>
                <li class=""><a href="<?php echo base_url('komunitas/profile/'.$this->member['page_id']);?>">INFORMASI PRIBADI</a></li>
                <?php };?>
                */?>

                <li>&nbsp;</li>
                <li class="" style="height: 2em;"><a href="<?php echo base_url('page/tentang');?>" style="font-size: .8em;">TENTANG</a></li>
                <li class="" style="height: 2em;"><a href="<?php echo base_url('page/privasi');?>" style="font-size: .8em;">KEBIJAKAN PRIVASI</a></li>
                <li class="" style="height: 2em;"><a href="<?php echo base_url('page/term_condition');?>" style="font-size: .8em;">SYARAT & KETENTUAN</a></li>

<!--                <li class=""><a href="--><?php //echo base_url('survey');?><!--">POLLING KOMUNITAS</a></li>-->
<!--                <li class=""><a href="--><?php //echo base_url('suksesi');?><!--">SUKSESI DAN SURVEY</a></li>                -->
                <!-- li class=""><a href="< ?php echo base_url('caleg');?>">CALEG 2014</a></li -->
                
            </ul>
            
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 text-right">
            <!-- <small><em>Disclaimer: data terkumpul sejak 2010 - sekarang.</em></small> -->
            <small><em> </em></small>
        </div>
    </div>
</div>