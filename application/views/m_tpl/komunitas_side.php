<div class="container" style="border: solid 1px #cecece;border-radius: 7px 7px 7px 7px;">
    <div class="row">
        <div class="col-xs-12 text-center">
            <img class="lazy komu-side-img" src="<?=badge_url($user['attachment_title']);?>">        
        </div>

        <?php /*
        <?php if($this->member['user_id'] == $user['user_id'] && $this->member['account_id'] == $user['account_id']){ ?>
        <div class="col-xs-6 text-center">
            <a id="change_pp" class="btn-flat btn-flat-red pull-right"><i class="icon-camera"></i></a>
        </div>
        <div class="col-xs-6 text-center">
            &nbsp;
        </div>
        <?php } ?>
        <div class="col-xs-12 text-center">
            <a href="<?=base_url('komunitas/profile/'.$user['user_id']);?>" class="komu-side-nav-link-user"><h4><?=ucwords($user['display_name']);?></h4></a>
        </div>
        <?php if($this->member['user_id'] == $user['user_id'] && $this->member['account_id'] == $user['account_id']){ ?>
        <div class="col-xs-12 text-center bottom_border">
            <a href="<?=base_url('komunitas/profile/'.$user['user_id']);?>" class="btn-flat btn-flat-gray pull-right">Edit</a>    
        </div>
        <?php };?>
        */?>
    </div>
    <div class="row row_odd mtop1">
        <div class="col-xs-12 text-left ">
            <?php /*<a href="<?=base_url('komunitas/'.$user['user_id']);?>" class="komu-side-nav-link" ><span>Wall</span></a>*/?>
            <span style="font-weight: bold;">Wall</span>
        </div>
    </div>
    <div class="row row_even mtop1">
        <div class="col-xs-8 text-left">
            <a href="<?=base_url('komunitas/blog/'.$user['user_id']);?>" class="komu-side-nav-link"><span>Blogs</span></a>
        </div>
        <div class="col-xs-4 text-left">
            <a href="<?=base_url('komunitas/blog/'.$user['user_id']);?>" >
                <span class="pull-right"><?=$user['count_blog'];?></span>
            </a>
        </div>
    </div>
    <div class="row row_odd mtop1">
        <div class="col-xs-8 text-left">
            <?php /*<a href="<?=base_url('komunitas/photo/'.$user['user_id']);?>" class="komu-side-nav-link"><span>Photos</span></a>*/?>
            <span>Photos</span>
        </div>
        <div class="col-xs-4 text-left">
            <?php /*
            <a href="<?=base_url('komunitas/photo/'.$user['user_id']);?>">
                <span class="pull-right"><?=$user['count_photo'];?></span>
            </a>
            */?>
            <span class="pull-right"><?=$user['count_photo'];?></span>
        </div>
    </div>
    <div class="row row_even mtop1">
        <div class="col-xs-8 text-left">
            <?php /*<a href="<?=base_url('komunitas/follow/'.$user['user_id']);?>" class="komu-side-nav-link"><span>Follow</span></a>*/?>
            <span>Follow</span>
        </div>
        <div class="col-xs-4 text-left">
            <?php /*
            <a href="<?=base_url('komunitas/follow/'.$user['user_id']);?>">
                <span class="pull-right"><?=$user['count_follow']+$user['count_follower'];?></span>
            </a>
            */?>
            <span class="pull-right"><?=$user['count_follow']+$user['count_follower'];?></span>
        </div>
    </div>
    <div class="row row_odd mtop1">
        <div class="col-xs-8 text-left">
            <?php /*<a href="<?=base_url('komunitas/friends/'.$user['user_id']);?>" class="komu-side-nav-link"><span>Friends</span></a>*/?>
            <span>Friends</span>
        </div>
        <div class="col-xs-4 text-left">
            <?php /*
            <a href="<?=base_url('komunitas/friends/'.$user['user_id']);?>">
                <span class="pull-right"><?=$user['count_friend'];?></span>
            </a>
            */?>
            <span class="pull-right"><?=$user['count_friend'];?></span>
        </div>
    </div>
    
    <?php /*
    <?php if($user['count_friend'] > 0):?>
    <div class="row komu-side-nav-large">
        <div class="col-xs-12 text-left">
            <a href="<?=base_url('komunitas/friends/'.$user['user_id']);?>" class="komu-side-nav-link-user"><h4>Friends</h4></a>
        </div>
    </div>
    <div class="row komu-side-nav-large">
        <?php 
        foreach($user['friend'] as $index=>$val):
        ?>
        <div class="col-xs-3">
            <a href="<?=base_url('komunitas/'.$val['user_id']);?>">
                <img src="<?=icon_url($val['attachment_title']);?>" style="width: 15px;">
            </a>
        </div>
        <?php
        endforeach;
        ?>
    </div>
    <?php endif;?>
    */?>
    
</div>


<?php if($this->member['user_id'] == $user['user_id'] && $this->member['account_id'] == $user['account_id']){ ?>
<!--- MODAL UPDATE PP -->
<div id="pp_modal" class="modal hide fade">
    <div class="modal-container">
        <div class="modal-sub-container">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Ganti Photo Profile <!--<em>(klik pada image untuk menjadikan foto profile)</em>--></h3>
            </div>
            <div class="modal-body">
                <div id="form_foto_side" class="row-fluid">
                    <div class="span5">
                        <form id="form_pp" name="form_pp" method="post" enctype="multipart/form-data" action="<?php echo base_url().'timeline/post_profile_picture';?>" target="ppuploadiframe">
                            <div class="textarea_holder">
                                <input type="file" name="userfile" id="userfile">
                                <div class="pull-right">
                                    <input type="submit" class="btn-flat btn-flat-dark" id="btnpostpp" value="Upload">

                                </div>
                            </div>
                        </form>
                        <iframe name="ppuploadiframe" id="ppuploadiframe" style="display:none;"></iframe>

                    </div>
                </div>
                <div class="row-fluid">
                    <div id="modal_body_pp" class="span12" style="height: 420px;">
                <iframe id="iframe_modal_pp" width="100%" height="420px" src="" frameborder=0 scrolling=no></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <div class="modal-footer">-->
    <!--        <a href="#" class="btn">Close</a>-->
    <!--        <a href="#" class="btn btn-primary">Save changes</a>-->
    <!--    </div>-->
</div>
<?php } ?>
