<?php
/**
 * Created by JetBrains PhpStorm.
 * User: alam
 * Date: 13/03/14
 * Time: 11:30
 * To change this template use File | Settings | File Templates.
 */
?>
<br>
    <div class="container footer">
    	<small>&copy; 2014 <a href="<?php echo base_url();?>">Bijaks</a></small>
    </div>

</body>


<script>
    $(document).ready(function() {
        $('#blog').masonry({
            itemSelector: '.content',
            columnWidth: 10
        });

        $("img.lazy").lazyload();


    });


  </script>

</html>