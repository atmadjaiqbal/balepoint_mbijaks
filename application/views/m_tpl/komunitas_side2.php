<div class="row" style="border: solid 1px #cecece;border-radius: 7px 7px 7px 7px;margin-left: .2em;margin-right: .2em;">
    <div class="col-xs-12 text-center">
        <a href="<?=base_url('komunitas/profile/'.$user['user_id']);?>" ><h4><?=ucwords($user['display_name']);?></h4></a>
    </div>
    <div class="col-xs-8">
        <img class="lazy" src="<?=badge_url($user['attachment_title']);?>" style="width: 100%;margin-bottom: 1em;">
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-6 text-left">
                <a href="<?=base_url('komunitas/blog/'.$user['user_id']);?>" class="komu-side-nav-link">
                <span>Blogs</span>
                </a>
            </div>
            <div class="col-xs-6 text-left">
                <a href="<?=base_url('komunitas/blog/'.$user['user_id']);?>" class="komu-side-nav-link">
                <span class="pull-right"><?=$user['count_blog'];?></span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-left">
                <span>Photos</span>
            </div>
            <div class="col-xs-6 text-left">
                <span class="pull-right"><?=$user['count_photo'];?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-left">
                <span>Follow</span>
            </div>
            <div class="col-xs-6 text-left">
                <span class="pull-right"><?=$user['count_follow']+$user['count_follower'];?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-left">
                <span>Friends</span>
            </div>
            <div class="col-xs-6 text-left">
                <span class="pull-right"><?=$user['count_friend'];?></span>
            </div>
        </div>

    </div>
</div>
