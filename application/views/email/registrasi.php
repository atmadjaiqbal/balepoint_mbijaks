<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
    <title>bijaks mail</title>
    <style type='text/css'>
        html, body { height: 100%; }
        body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0; padding: 0; font-family: sans-serif; }
        img { outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
        a, img { border:none; }
        table { border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
        table td { border-collapse: collapse; margin: 0; padding: 0; }
    </style>
</head>
<body bgcolor='#c9caca'>
<table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'>
    <tr height='20'><td colspan='3'></td></tr>
    <tr>
        <td>&nbsp;</td>
        <td width='604' valign='top'>
            <table width='604' align='center' border='0' cellspacing='0' cellpadding='0'>
                <tr height='98'><td background='<?php echo bijak_app();?>public/assets/img/mail/head.png'></td></tr>
                <tr><td background='<?php echo bijak_app();?>public/assets/img/mail/body.png'>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td width='20'>&nbsp;</td>
                            <td>
                                <!-- ####################################### -->
                                <p>Terima kasih atas perhatian dan kesediaan anda untuk menjadi member Bijaks.net.</p>
                                <p>Silahkan ikuti panduan registrasi member Bijaks.net berikut ini:</p>
                                <p>username : <?php echo $email; ?></p>
                                <!--<p>password : <?php /*echo $password; */?></p>-->
                                <p>Untuk login silahkan kunjungi halaman berikut <?php echo bijak_app();?>home/login </p>
                                <p>Salam,</p>
                                <p>Administrator Bijaks.net</p>
                                <!-- ####################################### -->
                            </td>
                            <td width='20'>&nbsp;</td>
                        </tr>
                    </table>
                </td></tr>
                <tr height='12'><td background='<?php echo bijak_app();?>public/assets/img/mail/foot.png'></td></tr>
            </table>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr height='40'><td colspan='3'></td></tr>
</table>
</body>
</html>
