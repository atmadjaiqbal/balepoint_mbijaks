			<div class="comment row-fluid" id="cmntitem_<?php echo $cmnt['comment_id'];?>">
				<div class="comment-user span1"><img src="<?php echo icon_url($member['xname']);?>" alt=""></div>
				<div class="comment-content span11">
					<?php if($cmnt['account_id'] == $member['account_id']):?>				
					<div class="comment-content-remove">
	        			<a class="makeright icon-close" name="cmdDelComment" id="cmdDelComment_<?php echo $cmnt['comment_id'];?>" title="hapus komentar">
		                    	<i class="icon-remove half-transparent"></i>

	                	</a>
	        		</div>
        			<?php endif;?>
					<p class="comment-info"><a href="<?php echo base_url().'komunitas/wall?uid='.$cmnt['account_id'];?>"><?php echo $cmnt['display_name'];?></a> @ <?php echo date('F j, Y', strtotime($cmnt['entry_date']));;?></p>
					<p>
					<?php echo  $cmnt['text_comment'];?>
					</p>
				</div>
			</div>
			<?php if($cmnt['account_id'] == $member['account_id']):?>				
			<script>
				$("#cmdDelComment_<?php echo $cmnt['comment_id'];?>").click(function(){
					$.ajax({
				        		url: "<?php echo base_url().'ajax/comment_remove/'?>",
						        type: "post",
				        		data: {id: "<?php echo $cmnt['comment_id']?>"},
						        success: function(response, textStatus, jqXHR){
						        	$("#cmntitem_<?php echo $cmnt['comment_id'];?>").slideUp('slow', function() { $(this).remove(); } );
						        },
				        		error: function(jqXHR, textStatus, errorThrown){
						            alert(
				        		        "The following error occured: "+
				                		textStatus, errorThrown
						            );
				        		},
						        complete: function(){
						          
						          
                                  <?php
                                  if(isset($content_date))
                                  {
                                  ?>
                                  
                                  $(".content_<?php echo $content_id?>").load('<?php echo base_url(); ?>ajax/count_content', { 'id': '<?php echo $content_id; ?>', 'tipe' : '2', 'res_type' : '2', 'content_date' : '<?php echo $content_date; ?>' } );
                                  
						          
                                  <?php
                                  }
                                  else
                                  {
                                  ?>
                                  
                                  $(".content_<?php echo $content_id?>").load('<?php echo base_url(); ?>ajax/count_content', { 'id': '<?php echo $content_id; ?>', 'tipe' : '2', 'res_type' : '2' } );
                                  
                                  <?php
                                  }
                                  ?>
				        		}
				  			});
				});
			</script>
			<?php endif;?>
