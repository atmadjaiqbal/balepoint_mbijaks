<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('news_url'))
{
	function news_url()
	{
		return "http://news.bijaks.net/"; //"http://175.41.167.254/";
	}
}

if ( ! function_exists('media_url'))
{
	function media_url()
	{		
		return base_url(); //"http://46.137.197.113/bijak/";
	}
}

if ( ! function_exists('news_foto_path'))
{
	function news_foto_path()
	{
		return "uploads";
	}
}

if ( ! function_exists('textWrap'))
{
   function textWrap($text) {
        $new_text = '';
        $text_1 = explode('>',$text);
        $sizeof = sizeof($text_1);
        for ($i=0; $i<$sizeof; ++$i) {
            $text_2 = explode('<',$text_1[$i]);
            if (!empty($text_2[0])) {
                $new_text .= preg_replace('#([^\n\r .]{25})#i', '\\1  ', $text_2[0]);
            }
            if (!empty($text_2[1])) {
                $new_text .= '<' . $text_2[1] . '>';   
            }
        }
        return $new_text;
    } 
}    