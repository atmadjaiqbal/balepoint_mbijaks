<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('recursive_array_search'))
{
    function recursive_array_search($needle,$haystack) {
        foreach($haystack as $key=>$value) {
            $current_key=$key;
            if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value))) {
                return $current_key;
            }
        }
        return false;
    }
}

/**
 * This function is identical to PHPs date() function, except that it allows date codes to be formatted using the MySQL style,
 * where each code letter is preceded  with a percent sign:  %Y %m %d etc...
*/
if ( ! function_exists('mysql_date'))
{
	function mysql_date($datestr = '', $time = '')
	{
		if ($datestr == '')
			return '';

		if ($time == '')
			$time = now();

		$datestr = str_replace('%\\', '', preg_replace("/([a-z]+?){1}/i", "\\\\\\1", $datestr));
		return date($datestr, $time);
	}
}



/**
 * Extends date helper to return current time in MySQL datetime format
*/
if(!function_exists('mysql_datetime'))
{
	function mysql_datetime($date=null) {

	    if(!$date) $date = time();         // use now() instead of time() to adhere to user setting

	    if(is_numeric($date) && strlen($date)==10) {
	        return mysql_date("%Y-%m-%d %H:%i:%s", $date);
	    }   else    {
	        // try to use now()
	        return mysql_date("%Y-%m-%d %H:%i:%s", now());
		}
	}
}

/**
 * Take a MySQL datetime var and turn it into PHP's Unix Epoch time
 */
if(!function_exists('datetime_to_unix'))
{
	function datetime_to_unix($date) {
	    if(!$date) {
	    	return false;
	    }   else    {
	   	return date('U', strtotime($date));
		}
	}
}



if(!function_exists('time_passed'))
{

	function time_passed($timestamp)
	{
	     $timestamp =  datetime_to_unix($timestamp);
	     $diff = time() - (int)$timestamp;

	     if ($diff == 0)
	          return 'baru saja';

	     $intervals = array
	     (
	         1                   => array('tahun',    31556926),
	         $diff < 31556926    => array('bulan',   2628000),
	         $diff < 2629744     => array('minggu',    604800),
	         $diff < 604800      => array('hari',     86400),
	         $diff < 86400       => array('jam',    3600),
	         $diff < 3600        => array('menit',  60),
	         $diff < 60          => array('detik',  1)
	     );

	      $value = floor($diff/$intervals[1][1]);
	      return $value.' '.$intervals[1][0].($value > 1 ? '' : '').' lalu';
	}
}


if ( ! function_exists('textWrap'))
{
   function textWrap($text) {
        $new_text = '';
        $text_1 = explode('>',$text);
        $sizeof = sizeof($text_1);
        for ($i=0; $i<$sizeof; ++$i) {
            $text_2 = explode('<',$text_1[$i]);
            if (!empty($text_2[0])) {
                $new_text .= preg_replace('#([^\n\r .]{25})#i', '\\1  ', $text_2[0]);
            }
            if (!empty($text_2[1])) {
                $new_text .= '<' . $text_2[1] . '>';
            }
        }
        return $new_text;
    }

}

if (!function_exists('urlpageid'))
{
	function urlpageid($str) {
		$trans = array(
				' ' => '',
				'-' => '',
				'&' => '',
				',' => '',
				'%' => '',
				'+' => '',
				'/' => '',
				'?' => '',
				'(' => '',
				')' => '',
				'\'' => '',
				'"' => '',
				'.' => '',
				'~' => '',
				'`' => '',
				'!' => '',
				'@' => '',
				'#' => '',
				'$' => '',
				'^' => '',
				'*' => '',
				'_' => '',
				'=' => '',
				'\\' => '',
				'|' => '',
				'{' => '',
				'}' => '',
				'[' => '',
				']' => '',
				':' => '',
				';' => '',
				'<' => '',
				'>' => '',
		);
		return trim(strtr(strtolower($str),$trans));
	}
}

if (!function_exists('urltitle'))
{
	function urltitle($str) {
		$trans = array(
			' ' => '_',
			'-' => '_dash_',
			'&' => '_and_',
			',' => '_comma_',
			'%' => '_prcnt_',
			'+' => '_plus_',
			'/' => '_slash_',
			'?' => '_question_',
			'(' => '_opt_',
			')' => '_cpt_',
		);
		return trim(substr(strtr(strtolower($str),$trans), 0, 70));
	}
}

if (!function_exists('rev_urltitle'))
{
	function rev_urltitle($str)
	{
		$trans = array(
			'-'				=> ' ',
			'_dash_'		=> '-',
			'_and_'			=> '&',
			'_comma_'		=> ',',
			'_prcnt_'		=> '%',
			'_plus_'		=> '+',
			'_slash_'		=> '/',
			'_question_'	=> '?',
			'_opt_'			=> '(',
			'_cpt_'			=> ')',
			'_'				=> ' ',
		);
		return trim(strtr($str,$trans));
	}
}

if (!function_exists('full_urltitle'))
{
	function full_urltitle($str) {
		$trans = array(
			' ' => '_',
			'-' => '_dash_',
			'&' => '_and_',
			',' => '_comma_',
			'%' => '_prcnt_',
			'+' => '_plus_',
			'/' => '_slash_',
			'?' => '_question_',
			'(' => '_opt_',
			')' => '_cpt_',
		);
		return trim(strtr($str,$trans));
	}
}

if(!function_exists('time_since'))
{
	function time_since($ptime) {
	    $etime = time() - $ptime;

	    if ($etime < 1)
	    {
	        return '0 seconds';
	    }

	    $a = array( 12 * 30 * 24 * 60 * 60  =>  'tahun',
	                30 * 24 * 60 * 60       =>  'bulan',
	                24 * 60 * 60            =>  'hari',
	                60 * 60                 =>  'jam',
	                60                      =>  'menit',
	                1                       =>  'detik'
	                );

	    foreach ($a as $secs => $str)
	    {
	        $d = $etime / $secs;
	        if ($d >= 1)
	        {
	            $r = round($d);
	            return $r . ' ' . $str . ' lalu';
	        }
	    }
	}
}

if(!function_exists('category_color'))
{
	function category_color($cat) {
		$coklat = array('terkini', 'hot-issues', 'hukum', 'parlemen', 'ekonomi');
		$hitam = array('nasional', 'daerah', 'internasional' );
		$pink = array('reses');
		$merah = array('skandal');
		$kuning = array('hot_profile', 'suksesi');
		$biru = array('survey');

		$color = 'pink'; 
		if(in_array($cat, $coklat)){
			$color = 'coklat';
		} elseif (in_array($cat, $hitam)) {
			$color = 'hitam';
		}elseif (in_array($cat, $merah)) {
			$color = 'merah';
		}elseif (in_array($cat, $kuning)) {
			$color = 'kuning';
		}elseif (in_array($cat, $biru)) {
			$color = 'biru';
		}

		return $color;

	}

}