<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Config for the CodeIgniter Redis library
 *
 * @see ../libraries/Redis.php
 */

// Connection details

// Connection details
//$config['redis_master']['host'] = '192.168.52.5';                // IP address or host
$config['redis_master']['host'] = 'localhost'; 
$config['redis_master']['port'] = '6379';                        // Default Redis port is 6379
$config['redis_master']['password'] = 'foobar';                        // Can be left empty when the server does not require AUTH

//$config['redis_slave']['host'] = '192.168.52.5';
$config['redis_slave']['host'] = 'localhost';
$config['redis_slave']['port'] = '6379';
$config['redis_slave']['password'] = 'foobar';
//$config['redis_slave']['port'] = '6381';
//$config['redis_slave']['password'] = 'slave321';

/*
$config['redis_master']['host'] = '10.142.74.167';                // 10-142-74-167 IP address or host
$config['redis_master']['port'] = '6380';                        // Default Redis port is 6379
$config['redis_master']['password'] = 'master321';                        // Can be left empty when the server does not require AUTH

$config['redis_slave']['host'] = '10.142.74.167';
$config['redis_slave']['port'] = '6381';
$config['redis_slave']['password'] = 'slave321';
*/

/*
$config['redis_master']['host'] = '10.130.35.211';                // IP address or host
$config['redis_master']['port'] = '6380';                        // Default Redis port is 6379
$config['redis_master']['password'] = 'master321';                        // Can be left empty when the server does not require AUTH

$config['redis_slave']['host'] = '10.130.35.211';
$config['redis_slave']['port'] = '6381';
$config['redis_slave']['password'] = 'slave321';


$config['redis_master']['host'] = '10.143.18.46'; //'54.254.3.74';                // IP address or host
$config['redis_master']['port'] = '6380';                        // Default Redis port is 6379
$config['redis_master']['password'] = 'master321';                        // Can be left empty when the server does not require AUTH

$config['redis_slave']['host'] = '10.143.18.46'; // '54.254.3.74';
$config['redis_slave']['port'] = '6381';
$config['redis_slave']['password'] = 'slave321';
*/
