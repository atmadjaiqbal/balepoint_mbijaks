<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notice extends Application {


	public function __construct()
	{
		parent::__construct();

        $data = $this->data;
	}


	public function error_404()
	{
     $data['title'] = "Bijaks | Life & Politics";
     $html['html']['content']  = $this->load->view('error_notify', $data, true);
     $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
     $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
     $this->load->view('template/tpl_one_column', $html, false);
   }
   
}

?>   