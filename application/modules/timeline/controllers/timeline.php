<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TimeLine extends Application {

	function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $data = $this->data;
        $this->load->library('timeline_lib');
        $this->load->library('komunitas/komunitas_lib');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->load->library('redis', array('connection_group' => 'master'), 'redis_master');
        $this->redis_slave = $this->redis_slave->connect();
        $this->redis_master = $this->redis_master->connect();
    }

    function index($type='comment', $id=0)
    {
    	$where = ($id == 0) ? '1 = 1' : 'topic_id = '.$id;
    	$data = array();
    	$data['type'] = $type;
    	if ($type == 'comment'){
    		$data['line'] = $this->timeline_lib->getLastComment($where, $limit=1, $offset=0)->row_array();
    	}elseif($type == 'like'){
    		$data['line'] = $this->timeline_lib->getLastLike($where, $limit=1, $offset=0)->row_array();
    	}elseif($type == 'dislike'){
    		$data['line'] = $this->timeline_lib->getLastDislike($where, $limit=1, $offset=0)->row_array();
    	}
    	
    	$data['line']['image_url'] = icon_url($data['line']['attachment_title'], 'politisi/'.$data['line']['user_id']);
    	$data['line']['news_url']= base_url() . "news/article/" . $data['line']['topic_id'] . "-" . $data['line']['news_id'] . "/" . trim( str_replace('/', '', $data['line']['content_url']));
    	// echo '<pre>';
    	// var_dump($data);
    	// echo '</pre>';

    	return $this->load->view('template/tpl_sub_comunity', $data, true);
    }

    function topic($id=0)
    {
        $key = 'topic:last:activity:'.$id;
        $topic_last = $this->redis_slave->get($key);
//        echo $topic_last;
        $ls = @json_decode($topic_last, True);
//        var_dump($ls[0]->account_id);
        $result = array();
        foreach($ls as $row => $val){

            $data['line'] = $val;
            $data['line']['type'] = $val['tipe'];
            $data['line']['image_url'] = icon_url($data['line']['attachment_title'], 'politisi/'.$data['line']['page_id']);
            $data['line']['news_url']= base_url() . "news/article/" . $data['line']['topic_id'] . "-" . $data['line']['news_id'] . "/" . trim( str_replace('/', '', $data['line']['content_url']));
            $result[$row] = $this->load->view('template/tpl_sub_comunity', $data, true);
        }

        return $result;
    }

    function disclaimer()
    {
        // $_wording = "Disclaimer: data terkumpul sejak 2010 - sekarang. Kami akan mengikutsertakan data sebelum tahun 2010 secepatnya";
        $_wording = " ";
        $data['wording'] = $_wording;
        $this->load->view('template/tpl_disclaimer', $data);
    }

    function sub_header($tipe='bijak')
    {
        $key = $tipe.':last:activity';
//        $tipe = int($tipe);
        if(is_numeric($tipe)){
            $key = 'topic:last:activity:'.$tipe;
        }
//        print $key;
        $topic_last = $this->redis_slave->get($key);
//        echo $topic_last .'<br>';
        $ls = @json_decode($topic_last, True);
//        var_dump($ls);
//        exit;
        $result = array();
        if(!empty($ls))
        {
        foreach($ls as $row => $val){

            $data['line'] = $val;
            $data['line']['type'] = $val['tipe'];
            $data['line']['image_url'] = icon_url($data['line']['attachment_title'], 'politisi/'.$data['line']['page_id']);

            if($tipe == 'scandal'){
                $data['line']['news_url']= base_url() . "scandal/index/" . $val['location'] . "-" . urltitle($val['title']) . "/";
            }elseif($tipe == 'suksesi'){
                    $data['line']['news_url']= base_url() . "suksesi/index/" . $val['location'] . "-" . urltitle($val['title']) . "/";
            }elseif($tipe == 'survey'){
                $data['line']['news_url']= base_url() . "survey/index/" . $val['location'] . "-" . urltitle($val['title']) . "/";
            }
            elseif($tipe == 'bijak'){
                $data['line']['news_url']= base_url() . "news/article/" . $data['line']['topic_id'] . "-" . $data['line']['news_id'] . "/" . trim( str_replace('/', '', $data['line']['content_url']));
            }elseif($tipe == 'profile'){
                $data['line']['news_url']= base_url() . "profile/detailprofile/" . $data['line']['profile_id'];
            }else{
                $data['line']['news_url']= base_url() . "news/article/" . $data['line']['topic_id'] . "-" . $data['line']['news_id'] . "/" . trim( str_replace('/', '', $data['line']['content_url']));
            }

            $result[$row] = $this->load->view('template/tpl_sub_comunity', $data, true);
        }

//        var_dump($result);
        foreach($result as $row => $value ){
            echo $value;
        }
        }
    }

    function last($tipe='content',$id=0, $result='false', $category='terkini')
    {
        $data['category'] = $category;

        $data['msg'] = 'bad';
        $key = $tipe.':last:activity:' . $id;

        $activity = $this->redis_slave->get($key);
        
        $data['line'] = json_decode($activity, true);
        if($activity != NULL){
            $data['msg'] = 'ok';
            $data['line']['image_url'] = icon_url($data['line']['attachment_title'], 'politisi/'.$data['line']['page_id']);
        }

        // $this->output->set_content_type('application/json');
        // $this->output->set_output($activity);

     //    echo '<pre>';
    	// var_dump($data['line']);
    	// echo '</pre>';
        if($result == 'true'){
            $dt = $this->load->view('template/tpl_sub_comunity_small', $data, True);
            return $dt;
        }
    	$this->load->view('template/tpl_sub_comunity_small', $data);
    }

    function comment($content_id='', $limit=0, $offset=0)
    {
//        $limit = 9;
        if(empty($content_id)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'bad request';
            $this->message($data);
        }

//        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){
            $offset += 1; // (intval($page) - 1);
        }

        $result = array();
        $zrange = $limit + $offset;

        // get comment key
        $last_key = $this->redis_slave->get('comment:key:' .$content_id);

        $list_key = 'comment:list:'.$last_key.':';

        $list_len = $this->redis_slave->lLen($list_key.$content_id);
        if ($zrange > intval($list_len)){
            $zrange = $list_len;
        }

        if($offset > intval($list_len)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'bad request';
            $this->message($data);
        }

        $comment_list = $this->redis_slave->lRange($list_key.$content_id, $offset, $zrange);

        foreach($comment_list as $row=>$val){
            $comment_detail = $this->redis_slave->get('comment:detail:'.$val);
            $comment_array = @json_decode($comment_detail, true);
            if(!empty($comment_array)){
                $comment_array['image_url'] = icon_url($comment_array['attachment_title']);
            }
            $result[$row] = $comment_array;
        }
        $dt['result'] = $result;
        $dt['count_result'] = count($result);
        $htm = $this->load->view('timeline_comment', $dt, true);

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    function profile_comment($page_id='', $limit=0, $offset=0)
    {
//        $limit = 9;
        if(empty($page_id)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'bad request';
            $this->message($data);
        }

//        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){
            $offset += 1; // (intval($page) - 1);
        }

        $result = array();
        $zrange = $limit + $offset;

        $list_key = 'profile:comment:list:';

        $list_len = $this->redis_slave->lLen($list_key.$page_id);
        if ($zrange > intval($list_len)){
            $zrange = $list_len;
        }

        if($offset > intval($list_len)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'bad request';
            $this->message($data);
        }

        $comment_list = $this->redis_slave->lRange($list_key.$page_id, $offset, $zrange);

        foreach($comment_list as $row=>$val){
            $comment_detail = $this->redis_slave->get('profile:comment:detail:'.$val);
            $comment_array = @json_decode($comment_detail, true);
            if(!empty($comment_array)){
                $comment_array['image_url'] = icon_url($comment_array['attachment_title']);
            }
            $result[$row] = $comment_array;
        }
        $dt['result'] = $result;
        $dt['count_result'] = count($result);
        $htm = $this->load->view('timeline_comment', $dt, true);

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    function frame_comment($content_id='')
    {
        $data['id'] = $content_id;
        $this->load->view('template/timeline/tpl_comment', $data);
    }

    function content_comment($content_id='', $page=1)
    {
        $limit = 9;
        if(empty($content_id)){
            echo '';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){
            $offset += (intval($page) - 1);
        }

        $result = array();
        $zrange = $limit + $offset;

        // get comment key
        $last_key = $this->redis_slave->get('comment:key:' .$content_id);

        $list_key = 'comment:list:'.$last_key.':';

        $list_len = $this->redis_slave->lLen($list_key.$content_id);
        if ($zrange > intval($list_len)){
            $zrange = $list_len;
        }

        if($offset > intval($list_len)){
            echo '';
            return;
        }

        $comment_list = $this->redis_slave->lRange($list_key.$content_id, $offset, $zrange);

        foreach($comment_list as $row=>$val){
            $comment_detail = $this->redis_slave->get('comment:detail:'.$val);
            $comment_array = @json_decode($comment_detail, true);
            if(!empty($comment_array)){
                $comment_array['image_url'] = icon_url($comment_array['attachment_title']);
            }
            $result[$row] = $comment_array;
        }
        $data['result'] = $result;
        $data['count_result'] = count($comment_list);
        $this->load->view('timeline_comment', $data);
    }

    function post_comment()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('val', '', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Komentar harus di isi';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $val = $this->input->post('val', true);

        if(is_bool($id) || is_bool($val)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        // check if content exist
        $select = 'count(*) as count';
        $where = array('content_id' => trim($id));
        $cnt = $this->timeline_lib->get_content($select, $where)->row_array(0);
        if(intval($cnt['count']) == 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $cid = sha1( uniqid().time() );

        $data_post = array(
            'comment_id' => $cid,
            'account_id' => $this->member['account_id'],
            'content_id' => $id,
            'text_comment' => trim($val),
            'entry_date' => mysql_datetime()
        );

        $this->timeline_lib->insert_comment($data_post);

        $result = array();

        // get comment key
        $last_key = $this->redis_slave->get('comment:key:'.$id);
//        echo $last_key;
        if(empty($last_key)){
            $last_key = $this->redis_slave->incr('comment:key:'.$id);
//            echo $last_key;
        }

        // insert to redis
        $last_comment = $this->timeline_lib->get_comment($cid);
//        var_dump($last_comment->result_array());

        foreach($last_comment->result_array() as $row=>$value){
            $this->redis_master->lPush('comment:list:'.$last_key.':'. $id, $value['comment_id']);
            $this->redis_master->set('comment:detail:' . $value['comment_id'], json_encode($value));

            $value['image_url'] = icon_url($value['attachment_title']);

            $result[$row] = $value;
        }
        $dt['count_result'] = -1;
        $dt['result'] = $result;
        $htm = $this->load->view('timeline_comment', $dt, true);

        $lst_score = $this->timeline_lib->get_content_score($id)->result_array();

        $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

        /*** INSERT LOG */
        $data_changed = array(
            'id' => $id,
            'tipe' => 'content',
            'post_type' => 2,
            'insert_date' => mysql_datetime(),
            'insert_by' => $this->member['account_id'],
            'status' => 1
        );

        $ids = array('id' => $id);
        $this->timeline_lib->changed_redis($data_changed, $ids);



        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    function post_status()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('val', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user', '', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('userfile', '', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Title harus di isi';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $val = $this->input->post('val', true);
        $user = $this->input->post('user', true);

        if(is_bool($id) || is_bool($val) || empty($val) || empty($user)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $cid = sha1( uniqid().time() );
        $this->isBijaksUser($user);
        // check if friend
//        if($id != $this->member['account_id']){
//            $select = 'count(*) as total';
//            $where = array('tf.account_id' => $this->member['account_id'], 'tf.friend_account_id' => trim($id));
//            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
//            if(intval($is_friend['total']) == 0){
//                $data['rcode'] = 'bad';
//                $data['msg'] = 'Anda harus login !';
//                $this->message($data);
//            }
//        }

        //check if friend
        if($id != $this->member['account_id']){
            $select = 'count(*) as total';
            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$this->user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$this->user['account_id']."')";
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if(intval($is_friend['total']) == 0){
                $data['rcode'] = 'bad';
                $data['msg'] = 'this content is not your friend';
                $this->message($data);
            }
        }


        $data_content = array(
            'content_id' => $cid,
            'account_id' => $this->member['account_id'],
            'content_group_type' => 10,
            'page_id' => $user,
            'description' => trim($val),
            'content_type' => 'TEXT',
            'entry_date' => mysql_datetime()
        );

        $this->timeline_lib->insert_content($data_content);

        // get wall key redis
        $last_key = $this->redis_slave->get('wall:user:key:'.$this->member['account_id']);

        // insert content_id to own list
        $this->redis_master->lPush('wall:user:list:'.$last_key.':'.$this->member['account_id'], $cid);

        // get content_detail insert to redis
        $content_detail = $this->timeline_lib->get_content_by_id($cid);
        if($content_detail->num_rows() > 0){
            $cnt_detail = @json_encode($content_detail->row_array());
            $this->redis_master->set('wall:user:detail:'.$cid, $cnt_detail);
        }

        // get friend list
        $friend_list = $this->komunitas_lib->get_friend_list($this->member['account_id'], $limit=0, $offset=0);
        foreach($friend_list->result_array() as $row=>$val)
        {
            $laskey = $this->redis_slave->get('wall:user:key:'.$val['friend_account_id']);
            $this->redis_master->lPush('wall:user:list:'.$laskey.':'.$val['friend_account_id'], $cid);
        }

        $dt['wall_list'] = $content_detail->result_array();

        $htm = $this->load->view('template/komunitas/tpl_komunitas_wall', $dt, true);

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    function post_blog()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('content_blog', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user', '', 'required|trim|xss_clean');


        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Title harus di isi';
            $this->message($data);
        }

        $file_name = $this->input->post('nm', true);

        $id = $this->input->post('id');
        $title = $this->input->post('title', true);
        $content_blog = $this->input->post('content_blog', true);
        $user = $this->input->post('user', true);

        $content_id = $this->input->post('cid');

        $cid = sha1( uniqid().time() );

        if(intval($content_id) != 0){
            $cid = $content_id;
        }

        // check if own blog
        if(intval($content_id) != 0){
            $select = 'count(*) as total';
            $where = array(
                'content_id' => $content_id,
                'account_id' => $this->member['account_id'],
                'content_group_type' => 11,
                'page_id' => $this->member['user_id']
            );
            $content = $this->timeline_lib->get_content($select, $where)->row_array(0);
            if(intval($content['total']) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'not found';
                $this->message($data);
            }
        }

        // check if my wall
        if($id != $this->member['account_id'] and $user != $this->member['user_id']){
            $data['rcode'] = 'bad';
            $data['msg'] = 'not found';
            $this->message($data);
        }

        if(intval($content_id) === 0){
            $data_content = array(
                'content_id' => $cid,
                'account_id' => $this->member['account_id'],
                'content_group_type' => 11,
                'page_id' => $user,
                'title' => trim($title),
                'description' => character_limiter(strip_tags($content_blog), 200),
                'content_type' => 'TEXT',
                'entry_date' => mysql_datetime()
            );

            $this->timeline_lib->insert_content($data_content);

            $data_blog = array(
                'content_id' => $cid,
                'flag' => 1,
                'content_blog' => $content_blog
            );
            $this->timeline_lib->insert_content_blog($data_blog);

            $dt_upt = array('group_content_id' => null);
            $where = array('group_content_id' => $cid);
            $this->timeline_lib->update_content($dt_upt, $where);

            // update blog foto group content id
            if(!is_bool($file_name) && $file_name != null){
                if(is_array($file_name)){
                    foreach($file_name as $val){
                        $dt_upt = array('group_content_id' => $cid);
                        $where = array('content_id' => $val);
                        $this->timeline_lib->update_content($dt_upt, $where);
                    }
                }else{
                    $dt_upt = array('group_content_id' => $cid);
                    $where = array('content_id' => $file_name);
                    $this->timeline_lib->update_content($dt_upt, $where);
                }
            }


            // get wall key redis
            $last_key = $this->redis_slave->get('wall:user:key:'.$this->member['account_id']);

            // insert content_id to own list
            $this->redis_master->lPush('wall:user:list:'.$last_key.':'.$this->member['account_id'], $cid);

            // get friend list
            $friend_list = $this->komunitas_lib->get_friend_list($this->member['account_id'], $limit=0, $offset=0);
            foreach($friend_list->result_array() as $row=>$val)
            {
                $last_key_user = $this->redis_slave->get('wall:user:key:'.$val['friend_account_id']);

                $this->redis_master->lPush('wall:user:list:'.$last_key_user.':'.$val['friend_account_id'], $cid);
            }


        }else{
            $data_content = array(
                'title' => trim($title),
                'description' => character_limiter(strip_tags($content_blog), 200),
                'last_update_date' => mysql_datetime()
            );

            $where = array(
                'content_id' => $content_id
            );

            $this->timeline_lib->update_content($data_content, $where);

            $data_blog = array(
                'content_blog' => $content_blog
            );
            $this->timeline_lib->update_content_blog($data_blog, $where);

            $dt_upt = array('group_content_id' => null);
            $where = array('group_content_id' => $content_id);
            $this->timeline_lib->update_content($dt_upt, $where);

            // update blog foto group content id
            if(!is_bool($file_name) && $file_name != null){
                if(is_array($file_name)){
                    foreach($file_name as $val){
                        $dt_upt = array('group_content_id' => $content_id);
                        $where = array('content_id' => $val);
                        $this->timeline_lib->update_content($dt_upt, $where);
                    }
                }else{
                    $dt_upt = array('group_content_id' => $content_id);
                    $where = array('content_id' => $file_name);
                    $this->timeline_lib->update_content($dt_upt, $where);
                }
            }

        }

        // get content_detail insert to redis
        $content_detail = $this->timeline_lib->get_content_by_id($cid);
        if($content_detail->num_rows() > 0){
            $cnt_detail = @json_encode($content_detail->row_array());
            $this->redis_master->set('wall:user:detail:'.$cid, $cnt_detail);
        }

//        $dt['wall_list'] = $content_detail->result_array();
//
//        $htm = $this->load->view('template/komunitas/tpl_komunitas_wall', $dt, true);

        $blog_uri = base_url('komunitas/blog/'.$this->member['user_id'].'/'.$cid.'/'.url_title($title));

        $data['rcode'] = 'ok';
        $data['msg'] = $blog_uri;
        $this->message($data);

    }

    function post_photo()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', '', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('userfile', '', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'title dan photo harus di isi';
            $this->message($data);
        }

        $id = $this->input->post('id', true);
        $user = $this->input->post('user', true);
        $title = $this->input->post('title', true);
        $desc = $this->input->post('description', true);

        $cid = sha1( uniqid().time() );

        // check if friend
        /*if($id != $this->member['account_id']){
            $select = 'count(*) as total';
            $where = array('tf.account_id' => $this->member['account_id'], 'tf.friend_account_id' => trim($id));
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if(intval($is_friend['total']) == 0){
                $data['rcode'] = 'bad';
                $data['msg'] = 'add sebagai teman untuk dapat posting';
                $this->message($data);
            }
        }*/

        //check if friend
        if($id != $this->member['account_id']){
            $select = 'count(*) as total';
            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$id."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$id."')";
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if(intval($is_friend['total']) == 0){
                $data['rcode'] = 'bad';
                $data['msg'] = 'this content is not your friend';
                $this->message($data);
            }
        }

        $config['upload_path'] = FCPATH.'public/upload/image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000'; // Kb
        $config['file_name'] = $cid;
        $config['max_width']  = '1024'; // in pixel
        $config['max_height']  = '800'; // in pixel

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('userfile') )
        {
            $data['rcode'] = 'bad';
            $data['msg'] = $this->upload->display_errors();
            $this->message($data);
        }

        $var_upload = $this->upload->data();

        // resize image
        $this->image_resize('600', '800', 'large', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], FALSE);

        $this->image_resize('120', '130', 'thumb', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('320', '240', 'thumb/portrait', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('180', '320', 'thumb/landscape', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('208', '208', 'badge', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('36', '36', 'icon', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $select = 'content_id';
        $where = array(
            'account_id' => $id,
            'content_group_type' => 20,
            'page_id' => $user,
            'content_type' => 'GROUP'
        );
        $group_content = $this->timeline_lib->get_content($select, $where);
        $group_content_id = null;
        if($group_content->num_rows() > 0){
            $group_content_array = $group_content->row_array(0);
            $group_content_id = $group_content_array['content_id'];
        }

        $data_content = array(
            'content_id' => $cid,
            'account_id' => $this->member['account_id'],
            'content_group_type' => 20,
            'group_content_id' => $group_content_id,
            'page_id' => $user,
            'title' => $title,
            'description' => $desc,
            'content_type' => 'IMAGE',
            'entry_date' => mysql_datetime()
        );

        $this->timeline_lib->insert_content($data_content);

        $data_content_attachment = array(
            'content_id' => $cid,
            'attachment_length' => $var_upload['file_size'],
            'attachment_type' => $var_upload['file_type'],
            'attachment_title' => $var_upload['file_name'],
            'attachment_path' => 'public/upload/image/',
            'attachment_width' => $var_upload['image_width'],
            'attachment_height' => $var_upload['image_height']
        );
        $this->timeline_lib->insert_content_attachment($data_content_attachment);

        // get wall key redis
        $last_key = $this->redis_slave->get('wall:user:key:'.$this->member['account_id']);

        // insert content_id to own list
        $this->redis_master->lPush('wall:user:list:'.$last_key.':'.$this->member['account_id'], $cid);

        // get content_detail insert to redis
        $content_detail = $this->timeline_lib->get_content_by_id($cid);
        if($content_detail->num_rows() > 0){
            $cnt_detail = @json_encode($content_detail->row_array());
            $this->redis_master->set('wall:user:detail:'.$cid, $cnt_detail);
        }

        // get friend list
        $friend_list = $this->komunitas_lib->get_friend_list($this->member['account_id'], $limit=0, $offset=0);
        foreach($friend_list->result_array() as $row=>$val)
        {
            $last_key_user = $this->redis_slave->get('wall:user:key:'.$val['friend_account_id']);

            $this->redis_master->lPush('wall:user:list:'.$last_key_user.':'.$val['friend_account_id'], $cid);
        }

        // get html template
        $dt['wall_list'] = $content_detail->result_array();

        $htm = $this->load->view('template/komunitas/tpl_komunitas_wall', $dt, true);

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    function post_blog_photo()
    {
        if($this->isLogin() == false){
            $data['resultcode'] = 'failed';
            $data['result'] = 'Anda harus login !';
            $data['file_name'] = '';
            $data['file'] = '';
            $this->load->view('template/timeline/blog_upload_image', $data);
            exit;
        }

        $cid = sha1( uniqid().time() );

        $config['upload_path'] = FCPATH.'public/upload/image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '3000'; // Kb
        $config['file_name'] = $cid;
        $config['max_width']  = '1024'; // in pixel
        $config['max_height']  = '800'; // in pixel

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('userfile') )
        {
            $data['resultcode'] = 'failed';
            $data['result'] = $this->upload->display_errors();
            $data['file_name'] = '';
            $data['file'] = '';
            $this->load->view('template/timeline/blog_upload_image', $data);
            exit;
//            $this->message($data);
        }

        $var_upload = $this->upload->data();

        // resize image
        $this->image_resize('600', '800', 'large', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], FALSE);

        $this->image_resize('120', '130', 'thumb', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('320', '240', 'thumb/portrait', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('180', '320', 'thumb/landscape', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('208', '208', 'badge', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('36', '36', 'icon', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $id = $this->member['account_id'];
        $user = $this->member['user_id'];

        $data_content = array(
            'content_id' => $cid,
            'account_id' => $id,
            'content_group_type' => 59,
            'page_id' => $user,
            'title' => 'Blogs Pictures',
            'content_type' => 'BLOG_PHOTOS',
            'entry_date' => mysql_datetime()
        );

        $this->timeline_lib->insert_content($data_content);

        $data_content_attachment = array(
            'content_id' => $cid,
            'attachment_length' => $var_upload['file_size'],
            'attachment_type' => $var_upload['file_type'],
            'attachment_title' => $var_upload['file_name'],
            'attachment_path' => 'public/upload/image/',
            'attachment_width' => $var_upload['image_width'],
            'attachment_height' => $var_upload['image_height']
        );
        $this->timeline_lib->insert_content_attachment($data_content_attachment);

        $data['resultcode'] = 'ok';
        $data['result'] = '';
        $data['file_name'] = badge_url($var_upload['file_name']);
        $data['file'] = $cid;
        $this->load->view('template/timeline/blog_upload_image', $data);
    }

//    function set_profile_picture()
//    {
//        if($this->isLogin() == false){
//            $data['rcode'] = 'bad';
//            $data['msg'] = 'Anda harus login !';
//            $this->message($data);
//        }
//    }

    function post_photo_album()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('gcid', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('title', '', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('userfile', '', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'title dan photo harus di isi';
            $this->message($data);
        }

        $gcid = $this->input->post('gcid', true);
        $title = $this->input->post('title', true);
        $desc = $this->input->post('description', true);

        $cid = sha1( uniqid().time() );

        $config['upload_path'] = FCPATH.'public/upload/image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000'; // Kb
        $config['file_name'] = $cid;
        $config['max_width']  = '1024'; // in pixel
        $config['max_height']  = '800'; // in pixel

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('userfile') )
        {
            $data['rcode'] = 'bad';
            $data['msg'] = $this->upload->display_errors();
            $this->message($data);
        }

        $var_upload = $this->upload->data();

        // resize image
        $this->image_resize('600', '800', 'large', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], FALSE);

        $this->image_resize('120', '130', 'thumb', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('320', '240', 'thumb/portrait', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('180', '320', 'thumb/landscape', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('208', '208', 'badge', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('36', '36', 'icon', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $data_content = array(
            'content_id' => $cid,
            'account_id' => $this->member['account_id'],
            'content_group_type' => 40,
            'group_content_id' => $gcid,
            'page_id' => $this->member['user_id'],
            'title' => $title,
            'description' => $desc,
            'content_type' => 'IMAGE',
            'entry_date' => mysql_datetime()
        );

        $this->timeline_lib->insert_content($data_content);

        $data_content_attachment = array(
            'content_id' => $cid,
            'attachment_length' => $var_upload['file_size'],
            'attachment_type' => $var_upload['file_type'],
            'attachment_title' => $var_upload['file_name'],
            'attachment_path' => 'public/upload/image/',
            'attachment_width' => $var_upload['image_width'],
            'attachment_height' => $var_upload['image_height']
        );
        $this->timeline_lib->insert_content_attachment($data_content_attachment);

        // get content_detail insert to redis
        $content_detail = $this->timeline_lib->get_content_by_id($gcid);
        if($content_detail->num_rows() > 0){
            $content_album = $content_detail->row_array();
            $content_album['photo'] = $this->komunitas_lib->get_foto_album($gcid)->result_array();
            $cnt_detail = @json_encode($content_album);
            $this->redis_master->set('wall:user:detail:'.$gcid, $cnt_detail);
        }

        $photo = $this->komunitas_lib->get_foto_album($gcid, 1, 0);

        $dt['user'] = $this->member;
        $dt['photo'] = $photo->result_array();
        $htm = $this->load->view('template/komunitas/tpl_komunitas_foto', $dt, true);

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    function post_profile_picture()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $cid = sha1( uniqid().time() );

        $config['upload_path'] = FCPATH.'public/upload/image/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '30000'; // Kb
        $config['file_name'] = $cid;
        $config['max_width']  = '1024'; // in pixel
        $config['max_height']  = '800'; // in pixel

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('userfile') )
        {
            $data['rcode'] = 'bad';
            $data['msg'] = $this->upload->display_errors();
            $this->message($data);
        }

        $var_upload = $this->upload->data();

        // resize image
        $this->image_resize('600', '800', 'large', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], FALSE);

        $this->image_resize('120', '130', 'thumb', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('320', '240', 'thumb/portrait', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('180', '320', 'thumb/landscape', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $this->image_resize('208', '208', 'badge', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);
        $this->image_resize('36', '36', 'icon', $var_upload['file_name'], $var_upload['image_width'], $var_upload['image_height'], TRUE);

        $photo_profile = $this->komunitas_lib->get_foto_profile($this->member['account_id'], $this->member['user_id'], 0, 0);
        $gcid = null;
        if($photo_profile->num_rows() > 0)
        {
            $ppp = $photo_profile->row_array(0);
            $gcid = $ppp['content_id'];
        }

        $data_content = array(
            'content_id' => $cid,
            'account_id' => $this->member['account_id'],
            'content_group_type' => 22,
            'group_content_id' => $gcid,
            'page_id' => $this->member['user_id'],
            'title' => 'Profile Pictures',
            'content_type' => 'IMAGE',
            'entry_date' => mysql_datetime()
        );

        $this->timeline_lib->insert_content($data_content);

        $data_content_attachment = array(
            'content_id' => $cid,
            'attachment_length' => $var_upload['file_size'],
            'attachment_type' => $var_upload['file_type'],
            'attachment_title' => $var_upload['file_name'],
            'attachment_path' => 'public/upload/image/',
            'attachment_width' => $var_upload['image_width'],
            'attachment_height' => $var_upload['image_height']
        );
        $this->timeline_lib->insert_content_attachment($data_content_attachment);

        //update tusr_account
        $where = array('account_id' => $this->member['account_id']);
        $data_p = array('profile_content_id' => $cid);
        $this->komunitas_lib->tusr_account($data_p, $where);


        $where = array('page_id' => $this->member['user_id']);
        $data_p = array('profile_content_id' => $cid);
        $this->komunitas_lib->tobject_page($data_p, $where);

        // get wall key redis
        $last_key = $this->redis_slave->get('wall:user:key:'.$this->member['account_id']);

        // insert content_id to own list
        $this->redis_master->lPush('wall:user:list:'.$last_key.':'.$this->member['account_id'], $cid);

        // get content_detail insert to redis
        $content_detail = $this->timeline_lib->get_content_by_id($cid);
        if($content_detail->num_rows() > 0){
            $cnt_detail = @json_encode($content_detail->row_array());
            $this->redis_master->set('wall:user:detail:'.$cid, $cnt_detail);
        }

        // get friend list
//        $friend_list = $this->komunitas_lib->get_friend_list($this->member['account_id'], $limit=0, $offset=0);
//        foreach($friend_list->result_array() as $row=>$val)
//        {
//            // get wall key redis
//            $last_key_user = $this->redis_slave->get('wall:user:key:'.$val['friend_account_id']);
//
//            $this->redis_master->lPush('wall:user:list:'.$last_key_user.':'.$val['friend_account_id'], $cid);
//        }


        // change redis photo profile icon
        $wall_list = $this->komunitas_lib->get_wall_list_content_id($this->member['account_id'], $this->member['user_id'], $limit=1000, $offset=0);

        $last_key = $this->redis_slave->get('wall:user:key:'. $this->member['account_id']);
        if(empty($last_key))
            $last_key = 0;
        $rkey = $this->redis_master->incr('wall:user:key:'. $this->member['account_id']);
        $pipe = $this->redis_master->multi();
        foreach($wall_list->result_array() as $row=>$val){
            $pipe->rPush('wall:user:list:'.$rkey.':'.$this->member['account_id'], $val['content_id']);
        }
        $pipe->exec();

//            $this->redis_master->delete('wall:user:detail:'.$cid);

        /*** INSERT LOG */
        $data_changed = array(
            'id' => $this->member['account_id'],
            'tipe' => 'wall',
            'post_type' => 1,
            'insert_date' => mysql_datetime(),
            'insert_by' => $this->member['account_id'],
            'status' => 1
        );

        $ids = array('id' => $this->member['account_id']);
        $this->timeline_lib->changed_redis($data_changed, $ids);

        // update session
        $member = $this->member;
        $member['xname'] = $var_upload['file_name'];
        $this->session->set_userdata('member', $member);

        $this->isLogin();

        $data['rcode'] = 'ok';
        $data['badge_url'] = badge_url($var_upload['file_name'], $this->member['user_id']);
        $data['icon_url'] = icon_url($var_upload['file_name'], $this->member['user_id']);
        $this->message($data);

    }

    function post_album()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('desc', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('cid', '', 'required|trim|xss_clean');


        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Title harus di isi';
            $this->message($data);
        }

        $title = $this->input->post('title', true);
        $desc = $this->input->post('desc', true);

        $content_id = $this->input->post('cid');

        $cid = sha1( uniqid().time() );
//        echo $cid;
        if(intval($content_id) != 0){
            $cid = $content_id;
        }

        // check if own album
        if(intval($content_id) != 0){
            $select = 'count(*) as total';
            $where = array(
                'content_id' => $content_id,
                'account_id' => $this->member['account_id'],
                'content_group_type' => 40,
                'page_id' => $this->member['user_id']
            );
            $content = $this->timeline_lib->get_content($select, $where)->row_array(0);
            if(intval($content['total']) == 0)
            {
                show_404();
            }
        }

        if(intval($content_id) == 0){
            $data_content = array(
                'content_id' => $cid,
                'account_id' => $this->member['account_id'],
                'content_group_type' => 40,
                'page_id' => $this->member['user_id'],
                'title' => $title,
                'description' => $desc,
                'content_type' => 'GROUP',
                'entry_date' => mysql_datetime()
            );
            $this->timeline_lib->insert_content($data_content);

            // get wall key redis
            $last_key = $this->redis_slave->get('wall:user:key:'.$this->member['account_id']);

            // insert content_id to own list
            $this->redis_master->lPush('wall:user:list:'.$last_key.':'.$this->member['account_id'], $cid);

            // get friend list
            $friend_list = $this->komunitas_lib->get_friend_list($this->member['account_id'], $limit=0, $offset=0);
            foreach($friend_list->result_array() as $row=>$val)
            {
                $last_key_user = $this->redis_slave->get('wall:user:key:'.$val['friend_account_id']);

                $this->redis_master->lPush('wall:user:list:'.$last_key_user.':'.$val['friend_account_id'], $cid);
            }


        }else{
            $data_content = array(
                'title' => $title,
                'description' => $desc,
                'last_update_date' => mysql_datetime()
            );
            $where = array(
                'content_id' => $content_id
            );

            $this->timeline_lib->update_content($data_content, $where);
        }

        // get content_detail insert to redis
        $content_detail = $this->timeline_lib->get_content_by_id($cid);
        if($content_detail->num_rows() > 0){
            $cnt_detail = @json_encode($content_detail->row_array());
            $this->redis_master->set('wall:user:detail:'.$cid, $cnt_detail);
        }

        $blog_uri = base_url('komunitas/photo/'.$this->member['user_id'].'/'.$cid.'/'.url_title($title));
        redirect($blog_uri);

    }

    function post_dasar()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('fname', 'Nama Depan', 'required|trim|xss_clean');
        $this->form_validation->set_rules('lname', 'Nama Belakang', 'required|trim|xss_clean');
        $this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'required|trim|xss_clean');
        $this->form_validation->set_rules('current_city', 'Lokasi', 'required|trim|xss_clean');


        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';

            $data['msg']['fname'] = form_error('fname');
            $data['msg']['lname'] = form_error('lname');
            $data['msg']['birthday'] = form_error('birthday');
            $data['msg']['current_city'] = form_error('current_city');

            $this->message($data);
        }

        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $gender = $this->input->post('gender');
        $birthday = $this->input->post('birthday');
        $current_city = $this->input->post('current_city');
        $about = $this->input->post('about');

        $dt_usr_account = array(
            'display_name' => $fname.' '.$lname,
            'fname' => $fname,
            'lname' => $lname,
            'gender' => $gender,
            'birthday' => $birthday,
            'current_city' => $current_city,
            'updated_date' => mysql_datetime()
        );

        $wh = array('account_id' => $this->member['account_id'], 'user_id' => $this->member['user_id']);

        $this->komunitas_lib->tusr_account($dt_usr_account, $wh);

        $dt_tobj = array(
            'page_name' => $fname.' '.$lname,
            'about' => $about,
            'last_update_date' => mysql_datetime()
        );
        $wh = array('page_id' => $this->member['user_id']);
        $this->komunitas_lib->tobject_page($dt_tobj, $wh);

        // update session
        $member = $this->member;
        $member['username'] = $fname.' '.$lname;
        $this->session->set_userdata('member', $member);

        $this->isLogin();

        // change cache
        $wall_list = $this->komunitas_lib->get_wall_list_content_id($this->member['account_id'], $this->member['user_id'], $limit=1000, $offset=0);

        $last_key = $this->redis_slave->get('wall:user:key:'. $this->member['account_id']);
        if(empty($last_key))
            $last_key = 0;
        $rkey = $this->redis_master->incr('wall:user:key:'. $this->member['account_id']);
        $pipe = $this->redis_master->multi();
        foreach($wall_list->result_array() as $row=>$val){
            $pipe->rPush('wall:user:list:'.$rkey.':'.$this->member['account_id'], $val['content_id']);
        }
        $pipe->exec();

//            $this->redis_master->delete('wall:user:detail:'.$cid);

        /*** INSERT LOG */
        $data_changed = array(
            'id' => $this->member['account_id'],
            'tipe' => 'wall',
            'post_type' => 1,
            'insert_date' => mysql_datetime(),
            'insert_by' => $this->member['account_id'],
            'status' => 1
        );

        $ids = array('id' => $this->member['account_id']);
        $this->timeline_lib->changed_redis($data_changed, $ids);


        $data['rcode'] = 'ok';
        $data['msg'] = 'submit berhasil';
        $this->message($data);

    }

    function post_kontak()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('address', 'alamat', 'required|trim|xss_clean');
        $this->form_validation->set_rules('city_state', 'kota', 'required|trim|xss_clean');
        $this->form_validation->set_rules('country', 'negara', 'required|trim|xss_clean');
        $this->form_validation->set_rules('province_state', 'propinsi', 'required|trim|xss_clean');


        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';

            $data['msg']['address'] = form_error('address');
            $data['msg']['city_state'] = form_error('city_state');
            $data['msg']['country'] = form_error('country');
            $data['msg']['province_state'] = form_error('province_state');

            $this->message($data);
        }

        $address = $this->input->post('address');
        $city_state = $this->input->post('city_state');
        $country = $this->input->post('country');
        $postalcode = $this->input->post('postalcode');
        $province_state = $this->input->post('province_state');

        // check if data kontak exist
        $wh = array('page_id' => $this->member['user_id']);
        $ad = $this->komunitas_lib->get_info_address($select='count(*) as total', $wh)->row_array();
        if(intval($ad['total']) == 0){
            $dt_usr_kontak = array(
                'page_id' => $this->member['user_id'],
                'address' => $address,
                'city_state' => $city_state,
                'country' => $country,
                'postalcode' => $postalcode,
                'province_state' => $province_state
            );

            $this->komunitas_lib->insert_kontak($dt_usr_kontak);

        }else{
            $dt_usr_kontak = array(
                'address' => $address,
                'city_state' => $city_state,
                'country' => $country,
                'postalcode' => $postalcode,
                'province_state' => $province_state
            );

            $this->komunitas_lib->update_kontak($dt_usr_kontak, $wh);
        }


        $data['rcode'] = 'ok';
        $data['msg'] = 'submit berhasil';
        $this->message($data);
    }

    public function post_vote()
    {
        //$this->isLogin();
        $this->load->library('user_agent');
        $referal = $this->agent->referrer();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('cid', 'content id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('survey_id', 'survey id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('question_id', 'question id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('vote', 'option id', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('cid', form_error('cid'));
            $this->session->set_flashdata('survey_id', form_error('survey_id'));
            $this->session->set_flashdata('question_id', form_error('question_id'));
            $this->session->set_flashdata('vote', form_error('vote'));
            $this->session->set_flashdata('rcode', 'ok');

            redirect($referal, 'refresh');
        }


        $cid = $this->input->post('content_id');
        $survey_id = $this->input->post('survey_id');
        $question_id = $this->input->post('question_id');
        $vote = $this->input->post('vote');

        // CHECK IF CONTENT
        $select = 'count(*) as total';
        $where = array(
            'content_id' => $cid
        );
        $content = $this->timeline_lib->get_content($select, $where)->row_array(0);
        if(intval($content['total']) == 0)
        {
            $this->session->set_flashdata('rcode', 'ok');
            $this->session->set_flashdata('msg', 'data tidak di temukan');
            redirect($referal, 'refresh');
        }

        //check if survey
        $select = 's.*, t.`content_id`';
        $where = array(
            'survey_id' => $survey_id
        );

        $survey = $this->timeline_lib->get_survey($select, $where);
        if(intval($survey->num_rows()) == 0)
        {
            $this->session->set_flashdata('rcode', 'ok');
            $this->session->set_flashdata('msg', 'data tidak di temukan');
            redirect($referal, 'refresh');
        }

        // check if not expired
        $srv = $survey->row_array();
        if($srv['begin_date'] > mysql_datetime()){
            $this->session->set_flashdata('rcode', 'ok');
            $this->session->set_flashdata('msg', 'Survey belum di mulai');
            redirect($referal, 'refresh');
        }elseif($srv['expired_date'] < mysql_datetime()){
            $this->session->set_flashdata('rcode', 'ok');
            $this->session->set_flashdata('msg', 'Survey sudah selesai');
            redirect($referal, 'refresh');
        }

        //check if question
        $select = '*';
        $where = array(
            'survey_id' => $survey_id,
            'question_id' => $question_id
        );

        $quest = $this->timeline_lib->get_question($select, $where);
        if(intval($quest->num_rows()) == 0)
        {
            $this->session->set_flashdata('rcode', 'ok');
            $this->session->set_flashdata('msg', 'data tidak di temukan');
            redirect($referal, 'refresh');
        }

        //check if option
        $select = '*';
        $where = array(
            'question_option_id' => $vote,
            'question_id' => $question_id
        );

        $opt = $this->timeline_lib->get_question_option($select, $where);
        if(intval($opt->num_rows()) == 0)
        {
            $this->session->set_flashdata('rcode', 'ok');
            $this->session->set_flashdata('msg', 'data tidak di temukan');
            redirect($referal, 'refresh');
        }

        $option_array = $opt->row_array(0);
/* ---- */
        //check if already vote
        $select = 'count(*) as total';
        $where = array(
            'question_id' => $question_id,
            'account_id' => $this->member['account_id']
        );

        $is_vote = $this->timeline_lib->get_question_answer($select, $where)->row_array(0);

        if(intval($is_vote['total']) > 0)
        {
            $this->session->set_flashdata('rcode', 'ok');
            $this->session->set_flashdata('msg', 'anda sudah pernah vote survey ini');
            redirect($referal, 'refresh');
        }
/* -------- */
        $data_vote = array(
            'question_id' => $question_id,
            'question_option_id' => $vote,
            'account_id' => $this->member['account_id'],
            'answer' => $option_array['name'],
            'answer_date' => mysql_datetime()
        );
        $this->timeline_lib->insert_question_answer($data_vote);

        //update vote count
        $select = 'count(*) as total';
        $where = array(
            'question_option_id' => $vote,
            'question_id' => $question_id

        );

        $total_vote = $this->timeline_lib->get_question_answer($select, $where)->row_array(0);

        $data_total_vote = array('votes' => $total_vote['total']);
        $this->timeline_lib->update_question_option($data_total_vote, $where);

        //insert into redis
        $survey_array = $survey->row_array(0);
        $quest = $quest->result_array();
        foreach($quest as $row=>$val){
            $select = '*';
            $where = array(
                'question_id' => $val['question_id']
            );

            $option = $this->timeline_lib->get_question_option($select, $where);
            $option = $option->result_array();
            foreach($option as $ind => $value){
                $select = '*';
                $where = array(
                    'question_option_id' => $value['question_option_id'],
                    'question_id' => $val['question_id']
                );

                $all_vote = $this->timeline_lib->get_question_answer($select, $where);
                $value['score'] = $all_vote->num_rows();
                $value['answer'] = $all_vote->result_array();
                $option[$ind] = $value;
            }
            $val['option'] = $option;
            $quest[$row] = $val;
        }
        $survey_array['question'] = $quest;
        $this->redis_master->set('survey:detail:'.$survey_id, json_encode($survey_array));

        /*** INSERT LOG */
        $data_changed = array(
            'id' => $cid,
            'tipe' => 'content',
            'post_type' => 2,
            'insert_date' => mysql_datetime(),
            'insert_by' => $this->member['account_id'],
            'status' => 1
        );

        $ids = array('id' => $cid);
        $this->timeline_lib->changed_redis($data_changed, $ids);

        $where = array('content_id' => $cid);
        $data_content = array('status' => 2);

        $this->timeline_lib->update_tcontent_comment($where, $data_content);

        $this->session->set_flashdata('rcode', 'ok');
        $this->session->set_flashdata('msg', 'berhasil vote survey');
        redirect($referal, 'refresh');

    }

    public function post_vote_ajax()
    {
        $this->isLogin();

//        $this->load->library('user_agent');
//        $referal = $this->agent->referrer();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('cid', 'content id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('survey_id', 'survey id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('question_id', 'question id', 'required|trim|xss_clean');
        $this->form_validation->set_rules('vote', 'option id', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Field tidak lengkap !';
            $this->message($data);

        }


        $cid = $this->input->post('content_id');
        $survey_id = $this->input->post('survey_id');
        $question_id = $this->input->post('question_id');
        $vote = $this->input->post('vote');

        // CHECK IF CONTENT
        $select = 'count(*) as total';
        $where = array(
            'content_id' => $cid
        );
        $content = $this->timeline_lib->get_content($select, $where)->row_array(0);
        if(intval($content['total']) == 0)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Data tidak di temukan !';
            $this->message($data);
        }

        //check if survey
        $select = 's.*, t.`content_id`';
        $where = array(
            'survey_id' => $survey_id
        );

        $survey = $this->timeline_lib->get_survey($select, $where);
        if(intval($survey->num_rows()) == 0)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Data tidak di temukan !';
            $this->message($data);
        }

        // check if not expired
        $srv = $survey->row_array();
        if($srv['begin_date'] > mysql_datetime()){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Maaf, survey belum di mulai !';
            $this->message($data);
        }elseif($srv['expired_date'] < mysql_datetime()){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Maaf, survey sudah selesai !';
            $this->message($data);
        }

        //check if question
        $select = '*';
        $where = array(
            'survey_id' => $survey_id,
            'question_id' => $question_id
        );

        $quest = $this->timeline_lib->get_question($select, $where);
        if(intval($quest->num_rows()) == 0)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Data tidak di temukan !';
            $this->message($data);
        }

        //check if option
        $select = '*';
        $where = array(
            'question_option_id' => $vote,
            'question_id' => $question_id
        );

        $opt = $this->timeline_lib->get_question_option($select, $where);
        if(intval($opt->num_rows()) == 0)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Data tidak di temukan !';
            $this->message($data);
        }

        $option_array = $opt->row_array(0);

        //check if already vote
        $select = 'count(*) as total';
        $where = array(
            'question_id' => $question_id,
            'account_id' => $this->member['account_id']
        );

        $is_vote = $this->timeline_lib->get_question_answer($select, $where)->row_array(0);

        if(intval($is_vote['total']) > 0)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Sudah pernah vote !';
            $this->message($data);
        }

        $data_vote = array(
            'question_id' => $question_id,
            'question_option_id' => $vote,
            'account_id' => $this->member['account_id'],
            'answer' => $option_array['name'],
            'answer_date' => mysql_datetime()
        );
        $this->timeline_lib->insert_question_answer($data_vote);

        //update vote count
        $select = 'count(*) as total';
        $where = array(
            'question_option_id' => $vote,
            'question_id' => $question_id

        );

        $total_vote = $this->timeline_lib->get_question_answer($select, $where)->row_array(0);

        $data_total_vote = array('votes' => $total_vote['total']);
        $this->timeline_lib->update_question_option($data_total_vote, $where);

        //insert into redis
        $survey_array = $survey->row_array(0);
        $quest = $quest->result_array();
        foreach($quest as $row=>$val){
            $select = '*';
            $where = array(
                'question_id' => $val['question_id']
            );

            $option = $this->timeline_lib->get_question_option($select, $where);
            $option = $option->result_array();
            foreach($option as $ind => $value){
                $select = '*';
                $where = array(
                    'question_option_id' => $value['question_option_id'],
                    'question_id' => $val['question_id']
                );

                $all_vote = $this->timeline_lib->get_question_answer($select, $where);
                $value['score'] = $all_vote->num_rows();
                $value['answer'] = $all_vote->result_array();
                $option[$ind] = $value;
            }
            $val['option'] = $option;
            $quest[$row] = $val;
        }
        $survey_array['question'] = $quest;
        $this->redis_master->set('survey:detail:'.$survey_id, json_encode($survey_array));

        /*** INSERT LOG */
        $data_changed = array(
            'id' => $cid,
            'tipe' => 'content',
            'post_type' => 2,
            'insert_date' => mysql_datetime(),
            'insert_by' => $this->member['account_id'],
            'status' => 1
        );

        $ids = array('id' => $cid);
        $this->timeline_lib->changed_redis($data_changed, $ids);

        $where = array('content_id' => $cid);
        $data_content = array('status' => 2);

        $this->timeline_lib->update_tcontent_comment($where, $data_content);

        $data['rcode'] = 'bad';
        $data['msg'] = 'Berhasil vote survey !';
        $this->message($data);

    }

    function report_spam()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('content_id', 'cid', 'required|trim|xss_clean');
        $this->form_validation->set_rules('report_message', 'message', 'required|trim|xss_clean');
        $this->form_validation->set_rules('tipe', 'post tipe', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
//            $data['msg'] = 'data yang di input tidak valid';
            $data['msg']['content_id'] = form_error('content_id');
            $data['msg']['report_message'] = form_error('report_message');
            $data['msg']['tipe'] = form_error('tipe');

            $this->message($data);
        }
        $tipe = $this->input->post('tipe');
        $cid = $this->input->post('content_id');
        $message = $this->input->post('report_message');

        // CHECK IF CONTENT
        $content_result = array();
        if($tipe == 'content'){
            $select = '*';
            $where = array(
                'content_id' => $cid
            );
            $content = $this->timeline_lib->get_content($select, $where);
            if(intval($content->num_rows()) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'content not found';
                $this->message($data);
            }

            $content_result = $content->row_array(0);
        }elseif($tipe == 'comment'){
            $content = $this->timeline_lib->get_comment($cid);
            if(intval($content->num_rows()) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'content not found';
                $this->message($data);
            }
            $content_result = $content->row_array(0);
        }

        //check if friend
        /*$select = 'count(*) as total';
        $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$content_result['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$content_result['account_id']."')";
        $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
        if(intval($is_friend['total']) == 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'this content is not your friend';
            $this->message($data);
        }*/

        // CHECK KALO BELOM SUBMIT
        $select = 'count(*) as total';
        $where = array(
            'content_id' => $cid,
            'report_from' => $this->member['account_id'],
        );

        $repot = $this->timeline_lib->get_report_spam($select, $where)->row_array(0);
        if(intval($repot['total']) > 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'already submit report';
            $this->message($data);
        }

        // submit
        $cid_spam = sha1( uniqid().time() );
        $data_report = array(
            'tcontent_report_spam_id' => $cid_spam,
            'content_id' => $cid,
            'content_type' => strtoupper($tipe),
            'account_id' => $content_result['account_id'],
            'report_from' => $this->member['account_id'],
            'message' => $message,
            'created_date' => mysql_datetime()
        );

        $this->timeline_lib->report_spam($data_report);
        $data['rcode'] = 'ok';
        $data['msg'] = 'success';
        $this->message($data);
    }

    function delete_content()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('content_id', 'cid', 'required|trim|xss_clean');
        $this->form_validation->set_rules('tipe', 'tipe post', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';

            $data['msg']['content_id'] = form_error('content_id');
            $data['msg']['report_message'] = form_error('report_message');
            $data['msg']['tipe'] = form_error('tipe');

            $this->message($data);
        }

        $cid = $this->input->post('content_id');
        $tipe = $this->input->post('tipe');

        // CHECK IF OWN CONTENT

        if($tipe == 'content'){
            $select = 'count(*) as total';
            $where = array(
                'content_id' => $cid,
                'account_id' => $this->member['account_id'],
                'page_id' => $this->member['user_id']
            );
            $content = $this->timeline_lib->get_content($select, $where)->row_array(0);
            if(intval($content['total']) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'content not found';
                $this->message($data);
            }

            $where = array('content_id' => $cid);
            $data_content = array('status' => 2);

            $this->timeline_lib->update_tcontent($where, $data_content);

            /*###### GET WALL LIST #########*/
            $wall_list = $this->komunitas_lib->get_wall_list_content_id($this->member['account_id'], $this->member['user_id'], $limit=1000, $offset=0);

            $last_key = $this->redis_slave->get('wall:user:key:'. $this->member['account_id']);
            if(empty($last_key))
                $last_key = 0;
            $rkey = $this->redis_master->incr('wall:user:key:'. $this->member['account_id']);
            $pipe = $this->redis_master->multi();
            foreach($wall_list->result_array() as $row=>$val){
                $pipe->rPush('wall:user:list:'.$rkey.':'.$this->member['account_id'], $val['content_id']);
            }
            $pipe->exec();

//            $this->redis_master->delete('wall:user:detail:'.$cid);

            /*** INSERT LOG */
            $data_changed = array(
                'id' => $this->member['account_id'],
                'tipe' => 'wall',
                'post_type' => 3,
                'insert_date' => mysql_datetime(),
                'insert_by' => $this->member['account_id'],
                'status' => 1
            );

            $ids = array('id' => $this->member['account_id']);
            $this->timeline_lib->changed_redis($data_changed, $ids);

            $where = array('content_id' => $cid);
            $data_content = array('status' => 2);

            $this->timeline_lib->update_tcontent_comment($where, $data_content);

            /** delete COMMENT */
            $last_key = $this->redis_slave->get('comment:key:'.$cid);
//            if(empty($last_key))
//                $last_key = 0;

            $this->redis_master->delete('comment:list:'.$last_key.':'.$cid);

        }elseif($tipe == 'comment'){
            $select = '*';
            $where = array(
                'comment_id' => $cid,
                'account_id' => $this->member['account_id']
            );
            $content = $this->timeline_lib->get_tcontent_comment($select, $where);
            if(intval($content->num_rows()) == 0)
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'content not found';
                $this->message($data);
            }

            $content_first = $content->row_array(0);
//            var_dump($content_first);

            $where = array('comment_id' => $cid);
            $data_content = array('status' => 2);

            $this->timeline_lib->update_tcontent_comment($where, $data_content);

            /** delete COMMENT */
            $last_key = $this->redis_slave->get('comment:key:'.$content_first['content_id']);
            if(empty($last_key))
                $last_key = 0;

            $rkey = $this->redis_slave->incr('comment:key:'.$content_first['content_id']);

            $coment_list = $this->timeline_lib->get_comment_by_content($content_first['content_id']);
            foreach($coment_list->result_array() as $row=>$val){
                $this->redis_master->rPush('comment:list:'.$rkey.':'.$content_first['content_id'], $val['comment_id']);
            }

            $this->redis_master->delete('comment:list:'.$last_key.':'.$content_first['content_id']);
            $this->redis_master->delete('comment:detail:'.$cid);

            $lst_score = $this->timeline_lib->get_content_score($content_first['content_id'])->result_array();

            $this->redis_master->set('content:last:score:'.$content_first['content_id'], json_encode($lst_score));

        }

        $data['rcode'] = 'ok';
        $data['msg'] = 'oke';
        $this->message($data);

    }

    function unfollow(){
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('tipe', '', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'bad';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $user = $this->input->post('user');
        $tipe = $this->input->post('tipe');

        if(intval($tipe) == 1){
            if($this->member['account_id'] != $id){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $where = array('account_id' => $id, 'page_id' => $user);

        }elseif(intval($tipe) == 2){
            if($this->member['user_id'] != $user){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $where = array('account_id' => $id, 'page_id' => $user);
        }elseif(intval($tipe) == 3){
            if($this->member['account_id'] != $id){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $where = array('account_id' => $id, 'page_id' => $user);
        }

        $this->timeline_lib->delete_follow($where);

        /*###### GET WALL LIST #########*/
        $wall_list = $this->komunitas_lib->get_wall_list_content_id($this->member['account_id'], $this->member['user_id'], $limit=1000, $offset=0);

        $last_key = $this->redis_slave->get('wall:user:key:'. $this->member['account_id']);
        if(empty($last_key))
            $last_key = 0;

        $rkey = $this->redis_master->incr('wall:user:key:'. $this->member['account_id']);
        $pipe = $this->redis_master->multi();
        foreach($wall_list->result_array() as $row=>$val){
            $pipe->rPush('wall:user:list:'.$rkey.':'.$this->member['account_id'], $val['content_id']);
        }
        $pipe->exec();

//            $this->redis_master->delete('wall:user:detail:'.$cid);

        /*** INSERT LOG */
        $data_changed = array(
            'id' => $this->member['account_id'],
            'tipe' => 'wall',
            'post_type' => 3,
            'insert_date' => mysql_datetime(),
            'insert_by' => $this->member['account_id'],
            'status' => 1
        );

        $ids = array('id' => $this->member['account_id']);
        $this->timeline_lib->changed_redis($data_changed, $ids);

        $data['rcode'] = 'ok';
        $data['msg'] = 'oke';
        $this->message($data);
    }

    function unfriend(){
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('tipe', '', 'required|trim|xss_clean');
//        $this->form_validation->set_rules('fid', '', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'no valid';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $user = $this->input->post('user');
        $tipe = $this->input->post('tipe');
        $fid = $this->input->post('fid');

        $need_redis = false;

        if(intval($tipe) == 1){
            // UNFRIEND
            if($this->member['account_id'] != $id){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);

            }
            $where = "(account_id ='". $id ."' and friend_account_id='".$user."') or (account_id ='". $user ."' and friend_account_id='".$id."')";
            $this->timeline_lib->delete_friend($where);

            $need_redis = true;

        }elseif(intval($tipe) == 2){
            // FRIEND TERIMA

            if($this->member['account_id'] != $id){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $dt = array('flag' => 1, 'response_date' => mysql_datetime());
            $where = array('friend_request_id' => $fid);
            $this->timeline_lib->update_friend_request($where, $dt);
            $friend_id = sha1( uniqid().time() );
            $dt_insert = array(
                'friend_id' => $friend_id,
                'account_id' => $this->member['account_id'],
                'friend_account_id' => $user,
                'friend_date' => mysql_datetime()
            );
            $this->timeline_lib->insert_friend($dt_insert);
            $need_redis = true;

        }elseif(intval($tipe) == 3){
            if($this->member['account_id'] != $id){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $dt = array('flag' => 1);
            $where = array('friend_request_id' => $fid);
            $this->timeline_lib->update_friend_request($where, $dt);
        }elseif(intval($tipe) == 4){
            if($this->member['account_id'] != $id){
                $data['rcode'] = 'bad';
                $data['msg'] = 'bad';
                $this->message($data);
            }
            $dt = array('flag' => 2, 'response_date' => mysql_datetime());
            $where = array('friend_request_id' => $fid);
            $this->timeline_lib->update_friend_request($where, $dt);
        }

        if($need_redis){
            /*###### GET WALL LIST #########*/
            $wall_list = $this->komunitas_lib->get_wall_list_content_id($this->member['account_id'], $this->member['user_id'], $limit=1000, $offset=0);

            $last_key = $this->redis_slave->get('wall:user:key:'. $this->member['account_id']);
            if(empty($last_key))
                $last_key = 0;

            $rkey = $this->redis_master->incr('wall:user:key:'. $this->member['account_id']);
            $pipe = $this->redis_master->multi();
            foreach($wall_list->result_array() as $row=>$val){
                $pipe->rPush('wall:user:list:'.$rkey.':'.$this->member['account_id'], $val['content_id']);
            }
            $pipe->exec();

//            $this->redis_master->delete('wall:user:detail:'.$cid);

            /*** INSERT LOG */
            $data_changed = array(
                'id' => $this->member['account_id'],
                'tipe' => 'wall',
                'post_type' => 3,
                'insert_date' => mysql_datetime(),
                'insert_by' => $this->member['account_id'],
                'status' => 1
            );

            $ids = array('id' => $this->member['account_id']);
            $this->timeline_lib->changed_redis($data_changed, $ids);

        }

        $data['rcode'] = 'ok';
        $data['msg'] = 'oke';
        $this->message($data);
    }

    function add_friend(){
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('message', '', 'required|trim|xss_clean');


        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'pesan harus di isi';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $message = $this->input->post('message');

        // check if bijaks user
        $select = 'count(*) as total';
        $where = array('ta.account_id' => $id);

        $users = $this->komunitas_lib->get_user($select, $where)->row_array(0);
//        var_dump($users);
        if(intval($users['total']) == 0)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = '404';
            $this->message($data);
        }

        // check if already request
        $where = array('account_id' => $this->member['account_id'],
            'to_account_id' => $id, 'flag' => 0);
        $is_request = $this->timeline_lib->get_friend_request($select, $where)->row_array(0);
        if(intval($is_request['total']) > 0)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'sudah request sebagai teman';
            $this->message($data);
        }

        $fid = sha1( uniqid().time() );
        $data_re = array(
            'friend_request_id' => $fid,
            'account_id' => $this->member['account_id'],
            'to_account_id' => $id,
            'request_msg' => $message,
            'request_date' => mysql_datetime(),
            'flag' => 0
        );
        $this->timeline_lib->insert_friend_request($data_re);

        $data['rcode'] = 'ok';
        $data['msg'] = 'berhasil request teman, menunggu konfirmasi pertemanan';
        $this->message($data);
    }

    private function image_resize($height, $width, $path, $file_name, $image_width, $image_height, $crop)
    {
        if($height == 0 || $width == 0 || $image_width == 0 || $image_height == 0)
            return;


        $this->load->library('image_lib');
        // Resize image settings
        $config['image_library'] = 'gd2';
        $config['source_image'] = './public/upload/image/'.$file_name;
        $config['new_image'] = "./public/upload/image/".$path."/".$file_name;
        // $config['maintain_ratio'] = TRUE;
        // $config['create_thumb'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        if(!$crop)
        {
            if($height > $image_height && $width > $image_width)
            {
                @copy($config['source_image'],$config['new_image']);
            }
            else
            {
                $config['master_dim'] = 'width';
                $config['maintain_ratio'] = TRUE;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
            }

        }
        else
        {
            if($height > $image_height && $width > $image_width)
            {
                @copy($config['source_image'],$config['new_image']);
            }
            else
            {
                if($image_width/$image_height > $width/$height)
                {
                    $config['master_dim'] = 'height';
                    $config['x_axis'] = (($image_width*($height/$image_height)) - $width) / 2;
                    $config['y_axis'] = 0;
                }
                else
                {
                    $config['master_dim'] = 'width';
                    $config['x_axis'] = 0;
                    $config['y_axis'] = (($image_height*($width/$image_width))  - $height) / 2;
                }

                $config['maintain_ratio'] = TRUE;
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $config['source_image'] = $config['new_image'];
                $config['maintain_ratio'] = FALSE;
                $this->image_lib->initialize($config);
                $this->image_lib->crop();
            }
        }

    }

    function last_score($content_id, $result='false', $tipe='0')
    {
        $key = 'content:last:score:'. $content_id;
        $get_redis = $this->redis_slave->get($key);
        $list_count = @json_decode($get_redis, true);


        if($this->isLogin()){
            if(count($list_count) > 0){
                foreach($list_count as $row=>$val){
                    if($val['tipe'] == 'like'){
                        $select = 'count(*) as total';
                        $where = array('account_id' => $this->member['account_id'], 'content_id' => $content_id);
                        // check if like
                        $data_like = $this->timeline_lib->get_content_like($select, $where)->row_array(0);
                        $is_like = $data_like['total'];
                        $val['is_vote'] = (intval($is_like) == 0) ? false : true;

                    }elseif($val['tipe'] == 'dislike'){
                        $select = 'count(*) as total';
                        $where = array('account_id' => $this->member['account_id'], 'content_id' => $content_id);
                        //check if dislike
                        $data_dislike = $this->timeline_lib->get_content_dislike($select, $where)->row_array(0);
                        $is_dislike = $data_dislike['total'];
                        $val['is_vote'] = (intval($is_dislike) == 0) ? false : true;
                    }
                }
            }
        }

        $data['id'] = $content_id;
        $data['count'] = $list_count;

        if($result == 'true'){
            if(intval($tipe) == 0){
                $html = $this->load->view('timeline_score', $data, true);
            }elseif(intval($tipe) == 1){
                $html = $this->load->view('timeline_score_simple', $data, true);
            }
            return $html;
        }
        if(intval($tipe) == 0){
            $this->load->view('timeline_score', $data);
        }elseif(intval($tipe) == 1){
            $this->load->view('timeline_score_simple', $data);
        }

    }

    function content_vote()
    {
        if($this->isLogin() == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);

        }

        $id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $account_id = $this->member['account_id'];

        if(is_bool($id) || is_bool($tipe)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        // check if content exist
        $select = 'count(*) as count';
        $where = array('content_id' => $id);
        $cnt = $this->timeline_lib->get_content($select, $where)->row_array(0);
        if(intval($cnt['count']) == 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        if($tipe == 'like'){
            $select = 'count(*) as total';
            $where = array('account_id' => $account_id, 'content_id' => $id);
            // check if like
            $data_like = $this->timeline_lib->get_content_like($select, $where)->row_array(0);
            $is_like = $data_like['total'];

            //check if dislike
            $data_dislike = $this->timeline_lib->get_content_dislike($select, $where)->row_array(0);
            $is_dislike = $data_dislike['total'];

            if(intval($is_like) > 0){
                $this->timeline_lib->delete_content_like($where);

                $lst_score = $this->timeline_lib->get_content_score($id)->result_array();

                $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

                $total_like = $lst_score[1]['total'];

//                $select = 'count(*) as total';
//                $where = array('content_id' => $id);
//
//                $data_like = $this->timeline_lib->get_content_like($select, $where)->row_array(0);
//                $total_like = $data_like['total'];

                $data['rcode'] = 'ok';
                $data['msg'] = $lst_score; //$total_like;
                $this->message($data);
            }elseif(intval($is_dislike) > 0){
                $this->timeline_lib->delete_content_dislike($where);
                $data_insert = array(
                                'account_id' => $account_id,
                                'content_id' => $id,
                                'entry_date' => mysql_datetime()
                            );
                $this->timeline_lib->insert_content_like($data_insert);

                $lst_score = $this->timeline_lib->get_content_score($id)->result_array();

                $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

                $total_like = $lst_score[1]['total'];

                $data['rcode'] = 'ok';
                $data['msg'] = $lst_score; // $total_like;
                $this->message($data);
            }else{
                $data_insert = array(
                    'account_id' => $account_id,
                    'content_id' => $id,
                    'entry_date' => mysql_datetime()
                );
                $this->timeline_lib->insert_content_like($data_insert);

                $lst_score = $this->timeline_lib->get_content_score($id)->result_array();

                $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

                $total_like = $lst_score[1]['total'];

                $data['rcode'] = 'ok';
                $data['msg'] = $lst_score;// $total_like;
                $this->message($data);
            }
        }elseif($tipe == 'dislike'){
            $select = 'count(*) as total';
            $where = array('account_id' => $account_id, 'content_id' => $id);
            // check if like
            $data_like = $this->timeline_lib->get_content_like($select, $where)->row_array(0);
            $is_like = $data_like['total'];

            //check if dislike
            $data_dislike = $this->timeline_lib->get_content_dislike($select, $where)->row_array(0);
            $is_dislike = $data_dislike['total'];

            if(intval($is_like) > 0){
                $this->timeline_lib->delete_content_like($where);
                $data_insert = array(
                    'account_id' => $account_id,
                    'content_id' => $id,
                    'entry_date' => mysql_datetime()
                );
                $this->timeline_lib->insert_content_dislike($data_insert);

                $lst_score = $this->timeline_lib->get_content_score($id)->result_array();

                $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

                $total_dislike = $lst_score[2]['total'];

                $data['rcode'] = 'ok';
                $data['msg'] = $lst_score; // $total_dislike;
                $this->message($data);
            }elseif(intval($is_dislike) > 0){
                $this->timeline_lib->delete_content_dislike($where);

                $lst_score = $this->timeline_lib->get_content_score($id)->result_array();

                $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

                $total_dislike = $lst_score[2]['total'];

                $data['rcode'] = 'ok';
                $data['msg'] = $lst_score; // $total_dislike;
                $this->message($data);
            }else{
                $data_insert = array(
                    'account_id' => $account_id,
                    'content_id' => $id,
                    'entry_date' => mysql_datetime()
                );
                $this->timeline_lib->insert_content_dislike($data_insert);

                $lst_score = $this->timeline_lib->get_content_score($id)->result_array();

                $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

                $total_dislike = $lst_score[2]['total'];

                $data['rcode'] = 'ok';
                $data['msg'] = $lst_score; // $total_dislike;
                $this->message($data);
            }
        }

        /*** INSERT LOG */
        $data_changed = array(
            'id' => $id,
            'tipe' => 'content',
            'post_type' => 2,
            'insert_date' => mysql_datetime(),
            'insert_by' => $this->member['account_id'],
            'status' => 1
        );

        $ids = array('id' => $id);
        $this->timeline_lib->changed_redis($data_changed, $ids);

        $data['rcode'] = 'bad';
        $data['msg'] = 'Anda harus login !';
        $this->message($data);
    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    private function isLogin($return=false)
    {
        $cr = current_url();
        $this->member = $this->session->userdata('member');
        if(!$this->member['logged_in']){
            if($this->input->is_ajax_request()){
                return false;
            }else{
                if($return){
                    return false;
                }else{
                    redirect(base_url().'home/login?frm='.urlencode($cr));
                }
            }

        }

        return true;
        //exit('please');
    }

    private function isBijaksUser($user_id)
    {
        $select = 'ta.`account_id`, ta.`user_id`, ta.`display_name`, ta.account_type, ta.`registration_date`, ta.`last_login_date`, ta.`email`, ta.`fname`, ta.`lname`,
ta.`gender`, ta.`birthday`, ta.`current_city`, tp.`profile_content_id`, tp.`about`, tp.`personal_message`, tat.`attachment_title`';
        $user_id = urldecode($user_id);
        $where = array('ta.user_id' => strval($user_id));

        $users = $this->komunitas_lib->get_user($select, $where);

        if($users->num_rows() > 0)
        {
            $this->user = $users->row_array(0);
            if($this->user['account_type'] != 0){
                show_404();
            }

        }else{
            show_404();
        }
    }

}