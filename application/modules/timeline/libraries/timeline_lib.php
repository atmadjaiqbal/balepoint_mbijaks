<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Timeline_Lib {

	protected  $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database('slave');
		$this->db_master  = $this->CI->load->database('master', true);
	}

	function getLastComment($where='1=1', $limit=1, $offset=0)
	{
		$query = 'SELECT * FROM 
					(
						SELECT tc.comment_id, tc.`account_id`, tc.`content_id`, tc.`text_comment`, tc.`entry_date`, tt.news_id, tt.content_url,
						ta.display_name, ta.user_id, tp.`profile_content_id`, tac.attachment_title, t.title, t.content_group_type, tht.topic_id
						FROM tcontent_comment tc 
						JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
						JOIN `tusr_account` ta ON ta.`account_id`=tc.`account_id`
						JOIN  `tobject_page` tp ON tp.page_id = ta.`user_id`
						LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
						JOIN tcontent t ON t.`content_id`=tc.`content_id` 
						JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
						WHERE t.`content_group_type` = 12 AND tc.`status` = 0
						ORDER BY entry_date DESC
						) AS last_comment WHERE ';
		
		$query .= $where;
		$query .= ' LIMIT '.$limit.' OFFSET '.$offset;
		
		$query = $this->CI->db->query($query);
		// echo $this->CI->db->last_query();
		return $query;
	}

	function getLastLike($where='1=1', $limit=1, $offset=0)
	{
		$query = 'SELECT * FROM (
					SELECT tc.`account_id`, tc.`content_id`, tc.`entry_date`, ta.display_name, ta.user_id, tt.news_id, tt.content_url,
					tp.`profile_content_id`, tac.attachment_title, t.title, t.content_group_type, tht.topic_id
					FROM tcontent_like tc 
					JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
					JOIN `tusr_account` ta ON ta.`account_id`=tc.`account_id`
					JOIN  `tobject_page` tp ON tp.page_id = ta.`user_id`
					LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
					JOIN tcontent t ON t.`content_id`=tc.`content_id` 
					JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
					WHERE t.`content_group_type` = 12
					ORDER BY entry_date DESC
				) AS last_like WHERE ';
		
		$query .= $where;
		$query .= ' LIMIT '.$limit.' OFFSET '.$offset;
		
		$query = $this->CI->db->query($query);
		// echo $this->CI->db->last_query();
		return $query;
	}

	function getLastDislike($where='1=1', $limit=1, $offset=0)
	{
		$query = 'SELECT * FROM (
					SELECT tc.`account_id`, tc.`content_id`, tc.`entry_date`, ta.display_name, ta.user_id, tt.news_id, tt.content_url,
					tp.`profile_content_id`, tac.attachment_title, t.title, t.content_group_type, tht.topic_id
					FROM tcontent_dislike tc 
					JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
					JOIN `tusr_account` ta ON ta.`account_id`=tc.`account_id`
					JOIN  `tobject_page` tp ON tp.page_id = ta.`user_id`
					LEFT JOIN tcontent_attachment tac ON tac.`content_id` = tp.`profile_content_id`
					JOIN tcontent t ON t.`content_id`=tc.`content_id` 
					JOIN tcontent_has_topic tht ON tht.`content_id` = tc.`content_id`
					WHERE t.`content_group_type` = 12
					ORDER BY entry_date DESC
				) AS last_dislike WHERE ';
		
		$query .= $where;
		$query .= ' LIMIT '.$limit.' OFFSET '.$offset;
		
		$query = $this->CI->db->query($query);
		// echo $this->CI->db->last_query();
		return $query;
	}

    function get_content($select="*", $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('tcontent');
        return $data;
    }

    function get_content_like($select='*', $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('tcontent_like');
        return $data;
    }

    function get_content_dislike($select='*', $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('tcontent_dislike');
        return $data;
    }

    function insert_content_like($data=array())
    {
        $this->db_master->insert('tcontent_like', $data);
    }

    function insert_content_dislike($data=array())
    {
        $this->db_master->insert('tcontent_dislike', $data);
    }

    function update_content_like($data=array(), $where=array())
    {
        $this->db_master->where($where);
        $this->db_master->update('tcontent_like', $data);

    }

    function update_content_dislike($data=array(), $where=array())
    {
        $this->db_master->where($where);
        $this->db_master->update('tcontent_dislike', $data);

    }

    function update_content($data=array(), $where=array())
    {
        $this->db_master->where($where);
        $this->db_master->update('tcontent', $data);

    }

    function update_content_blog($data=array(), $where=array())
    {
        $this->db_master->where($where);
        $this->db_master->update('tcontent_blog', $data);

    }

    function delete_content_like($where=array())
    {
        $this->db_master->where($where);
        $this->db_master->delete('tcontent_like');

    }

    function delete_content_dislike($where=array())
    {
        $this->db_master->where($where);
        $this->db_master->delete('tcontent_dislike');

    }

    function changed_redis($data=array(), $id=array())
    {
        $this->db_master->insert('tb_changed_redis', $data);

    }

    function delete_follow($where)
    {
        $this->db_master->where($where);
        $this->db_master->delete('tusr_follow');
    }

    function delete_friend($where)
    {
        $this->db_master->where($where);
        $this->db_master->delete('tusr_friend');
//        echo $this->db_master->last_query();
    }

    function insert_friend($data)
    {

        $this->db_master->insert('tusr_friend', $data);
//        echo $this->db_master->last_query();
    }

    function update_friend_request($where, $data)
    {
        $this->db_master->where($where);
        $this->db_master->update('tusr_friend_request', $data);
//        echo $this->db_master->last_query();
    }

    function insert_friend_request($data)
    {

        $this->db_master->insert('tusr_friend_request', $data);
//        echo $this->db_master->last_query();
    }

    function get_friend_request($select="*", $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('tusr_friend_request');
        return $data;
    }

    function get_survey($select="*", $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->from('survey s');
        $this->db_master->join('tcontent t', 't.location = s.survey_id AND t.content_group_type = 54', 'left');
        $this->db_master->where($where);
        $data = $this->db_master->get();
        return $data;
    }

    function get_question($select="*", $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('question');
        return $data;
    }

    function get_question_option($select="*", $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('question_option');
        return $data;
    }

    function update_question_option($data=array(), $where=array())
    {
        $this->db_master->where($where);
        $this->db_master->update('question_option', $data);

    }

    function get_question_answer($select="*", $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('question_answer');
        return $data;
    }

    function insert_question_answer($data=array())
    {
        $this->db_master->insert('question_answer', $data);
    }

    function get_content_score($content_id)
    {
        $query = "SELECT * FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment' AS tipe, content_id, COUNT(tc.`content_id`) AS total
                    FROM tcontent_comment tc WHERE `status` = 0 AND content_id = '$content_id'
                )AS `comment` UNION ALL
                SELECT * FROM
                (
                    SELECT 'like' AS tipe, content_id, COUNT(`content_id`) AS total
                    FROM tcontent_like tl WHERE content_id = '$content_id'
                )AS `like` UNION ALL
                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, content_id, COUNT(`content_id`) AS total
                    FROM tcontent_dislike tl WHERE content_id = '$content_id'
                )AS `dislike`
            )AS last_count
            ";

        $data = $this->db_master->query($query);
        return $data;
    }

    function get_content_profile_score($page_id)
    {
        $query = "SELECT * FROM
            (
                SELECT * FROM
                (
                    SELECT 'comment_text' AS tipe, page_id, COUNT(pc.`page_id`) AS total
                    FROM page_comment pc WHERE `status` = 0 AND page_id = '$page_id'
                )AS `comment` UNION ALL
                SELECT * FROM
                (
                    SELECT 'like' AS tipe, page_id, COUNT(`page_id`) AS total
                    FROM page_like tl WHERE page_id = '$page_id'
                )AS `like` UNION ALL
                SELECT * FROM
                (
                    SELECT 'dislike' AS tipe, page_id, COUNT(`page_id`) AS total
                    FROM page_dislike tl WHERE page_id = '$page_id'
                )AS `dislike`
            )AS last_count
            ";

        $data = $this->db_master->query($query);
        return $data;
    }

    function get_comment_by_content($content_id)
    {
        $query = "SELECT tc.*, tp.`page_id`, tp.`page_name`, tat.`attachment_title`
            FROM tcontent_comment tc
            LEFT JOIN tusr_account ta ON ta.`account_id`=tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`
            where tc.content_id = '$content_id' and tc.status = 0
            ORDER BY tc.entry_date desc ";
        $data = $this->db_master->query($query);
        return $data;
    }

    function get_comment($comment_id)
    {
        $query = "SELECT tc.*, tp.`page_id`, tp.`page_name`, tat.`attachment_title`
            FROM tcontent_comment tc
            LEFT JOIN tusr_account ta ON ta.`account_id`=tc.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id`=ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`
            where tc.comment_id = '$comment_id' and tc.status = 0
            ORDER BY tc.entry_date desc ";
        $data = $this->db_master->query($query);
        return $data;
    }

    function get_comment_politisi($comment_id)
    {
        $sql = "SELECT pc.page_comment_id, pc.account_id, pc.page_id, pc.comment_text as 'text_comment', pc.entry_date,
                pc.status, tp.`page_id`, tp.`page_name`, tat.`attachment_title`
        FROM page_comment pc
        LEFT JOIN tusr_account ta ON ta.`account_id` = pc.`account_id`
        LEFT JOIN tobject_page tp ON tp.`page_id` = ta.`user_id`
        LEFT JOIN tcontent_attachment tat ON tat.`content_id` = tp.`profile_content_id`
        WHERE pc.page_comment_id = '".$comment_id."' AND pc.status = 0
        ORDER BY pc.entry_date DESC";
        $data = $this->db_master->query($sql);
        return $data;
    }

    function insert_comment($data=array())
    {
        $this->db_master->insert('tcontent_comment', $data);

    }

    function insert_comment_profile($data=array())
    {
        $this->db_master->insert('page_comment', $data);
    }

    function insert_content($data=array())
    {
        $this->db_master->insert('tcontent', $data);
//        echo $this->db_master->last_query();
    }

    function insert_content_attachment($data=array())
    {
        $this->db_master->insert('tcontent_attachment', $data);
    }

    function insert_content_blog($data=array())
    {
        $this->db_master->insert('tcontent_blog', $data);
    }

    function get_content_by_id($content_id)
    {
        $query = "SELECT st.*,
                tat.attachment_title, c_blog.content_blog,
                ta.`user_id` AS from_page_id, ta.account_type as from_account_type, tp.page_name AS from_page_name, tc.attachment_title AS from_attachment_title,
                tp2.page_id AS to_page_id, ta2.account_type as to_account_type, tp2.page_name AS to_page_name, tc2.attachment_title AS to_attachment_title
                FROM tcontent st
                LEFT JOIN tcontent_attachment tat ON tat.content_id = st.content_id
                LEFT JOIN tcontent_blog c_blog ON c_blog.content_id = st.content_id
                LEFT JOIN tusr_account ta ON ta.`account_id` = st.account_id
                LEFT JOIN tobject_page tp ON tp.page_id = ta.user_id
                LEFT JOIN tcontent_attachment tc ON tc.content_id = tp.profile_content_id
                LEFT JOIN tobject_page tp2 ON tp2.page_id = st.page_id
                LEFT JOIN tusr_account ta2 ON ta2.`user_id` = tp2.page_id
                LEFT JOIN tcontent_attachment tc2 ON tc2.content_id = tp2.profile_content_id
                WHERE st.content_id = '$content_id'
          ";

        $data = $this->db_master->query($query);
        return $data;

    }

    function report_spam($data=array())
    {
        $this->db_master->insert('tcontent_report_spam', $data);
    }

    function get_report_spam($select='*', $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('tcontent_report_spam');
        return $data;
    }

    function get_tcontent_comment($select='*', $where=array())
    {
        $this->db_master->select($select);
        $this->db_master->where($where);
        $data = $this->db_master->get('tcontent_comment');
        return $data;
    }

    function update_tcontent($where=array(), $data=array())
    {
        $this->db_master->where($where);
        $this->db_master->update('tcontent', $data);
    }

    function update_tcontent_comment($where=array(), $data=array())
    {
        $this->db_master->where($where);
        $this->db_master->update('tcontent_comment', $data);
    }



}