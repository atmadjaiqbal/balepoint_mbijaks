<?php if(count($result) > 0){ ?>
<?php foreach ($result as $key => $comment) { ?>
	<div class="row-fluid comment-row">
        <div class="comment-side-left">
			<div style="background:
	            url('<?=$comment['image_url']; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">
	            
	        </div>
        </div>
        
        <div class="span10">
            <div class="dropdown pull-right">
                <a data-toggle="dropdown" data-id="<?=$comment['comment_id'];?>" class="flag hide dropdown-toggle"><i class="icon-flag " ></i></a>
                <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
                    <li role="presentation"><a class="report-spam" data-tipe="comment" data-id="<?=$comment['comment_id'];?>" role="menuitem" tabindex="-1" href="#">report spam</a></li>
                    <?php if($this->member['account_id'] == $comment['account_id']){ ?>
                    <li role="presentation"><a class="remove-status" data-tipe="comment" data-id="<?=$comment['comment_id'];?>" role="menuitem" tabindex="-1" href="#">remove</a></li>
                    <?php } ?>
                </ul>
            </div>
            <a href="<?=base_url();?>komunitas/<?=$comment['page_id'];?>" class="comment-heading-comment"><?php echo strtoupper($comment['page_name']); ?></a>
            <?php 
                    $now = time();
                    $txt_time = time_since(strtotime($comment['entry_date']));
                    $txt = $comment['text_comment'];
                    
            ?>
            <p class="comment-time">@ <?php echo mdate('%d %M %y - %h:%i', strtotime($comment['entry_date'])); ?></p>
            <p class="comment-text" style="word-wrap: break-word;width: 90%;"><?php echo $txt; ?></p>
        </div>
	</div>

<?php } ?>
<?php }else{ ?>

<?php } ?>

<script>
    if(<?=$count_result;?> < 10 && <?=$count_result;?> != -1){
        $('#load_more_place').remove();
        $('#div_line_bottom').remove();
        $('#comment_modal_load_more').parent().parent().remove();
    }
</script>