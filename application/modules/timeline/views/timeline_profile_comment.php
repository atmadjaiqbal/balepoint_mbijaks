<?php if(count($result) > 0){ ?>
<?php foreach ($result as $key => $comment) { ?>
	<div class="row-fluid comment-row">
        <div class="comment-side-left">
			<div style="background:
	            url('<?php echo $comment['image_url']; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">
	            
	        </div>
        </div>
        
        <div class="span10">
            <div class="dropdown pull-right">
                <a data-toggle="dropdown" data-id="<?=$comment['page_comment_id'];?>" class="flag hide dropdown-toggle"><i class="icon-flag " ></i></a>
                <ul id="menu1" class="dropdown-menu" role="menu" aria-labelledby="drop4">
                    <li role="presentation"><a class="report-spam" data-tipe="comment" data-id="<?=$comment['page_comment_id'];?>" role="menuitem" tabindex="-1" href="#">report spam</a></li>
                    <?php if($this->member['account_id'] == $comment['account_id']){ ?>
                    <li role="presentation"><a class="remove-status" data-tipe="comment" data-id="<?=$comment['page_comment_id'];?>" role="menuitem" tabindex="-1" href="#">remove</a></li>
                    <?php } ?>
                </ul>
            </div>
            <a href="<?=base_url();?>komunitas/<?=$comment['page_id'];?>" class="comment-heading-comment"><?php echo strtoupper($comment['page_name']); ?></a>
            <?php 
                    $now = time();
                    $txt_time = time_since(strtotime($comment['entry_date']));
                    $txt = $comment['text_comment'];
                    
            ?>
            <p class="comment-time">@ <?php echo mdate('%d %M %y - %h:%i', strtotime($comment['entry_date'])); ?></p>
            <p class="comment-text"><?php echo $txt; ?></p>
        </div>
	</div>

<?php } ?>
<?php }else{ ?>

<?php } ?>
