<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Topten_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->db = $this->CI->load->database('slave', true);
    }


    function get_topten($select="*", $where=array(), $group_by, $order_by='tpt.no_urut')
    {
        $this->db->select($select);
        $this->db->from('tobject_page_topten tpt');
        $this->db->join('master_topten_category tpc', 'tpc.id_topten_category = tpt.id_topten_category', 'left' );
        $this->db->where($where);
        $this->db->group_by($group_by);
        $this->db->order_by($order_by);
        $data = $this->db->get();
        return $data;
    }

    function get_topten_youtube($select="*", $where=array(), $group_by, $order_by='tpt.no_urut')
    {
        $this->db->select($select);
        $this->db->from('topten_youtube tpt');
        $this->db->join('master_topten_category tpc', 'tpc.id_topten_category = tpt.id_topten_category', 'left' );
        $this->db->where($where);
        $this->db->group_by($group_by);
        $this->db->order_by($order_by);
        $data = $this->db->get();
        return $data;
    }

    function get_topten_category($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->where($where);
        $this->db->order_by('created_date', 'desc');
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get('master_topten_category');
        return $data;
    }

    function get_topten_news($select="*", $where=array(), $group_by, $order_by='tpt.no_urut')
    {
        $this->db->select($select);
        $this->db->from('topten_news tn');
        $this->db->join('master_topten_category tpc', 'tpc.id_topten_category = tn.id_topten_category', 'left' );
        $this->db->where($where);
        $this->db->group_by($group_by);
        $this->db->order_by($order_by);
        $data = $this->db->get();
        return $data;
    }

    function get_index_topten($category)
    {
        $sql = "SELECT tpt.topten_id, tpt.page_id, mtc.category_name, tpt.no_urut, tpt.deskripsi
                FROM tobject_page_topten tpt WHERE tpt.id_topten_category = $category
                INNER JOIN master_topten_category mtc ON tpt.id_topten_category = mtc.id_topten_category
                ORDER BY mtc.id_topten_category ASC, tpt.no_urut ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}