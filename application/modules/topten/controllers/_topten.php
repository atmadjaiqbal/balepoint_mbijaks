<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Topten extends Application
{
    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->helper('m_date');

        $this->load->helper('text');
        $this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');

        $this->load->library('aktor/aktor_lib');
        $this->load->library('caleg/caleg_lib');
        $this->load->library('topten_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->library('suksesi/suksesi_lib');
    }

    public function index($id=1)
    {
        $where = array('id_topten_category' => $id);

        $topten = $this->topten_lib->get_topten_category($select='*', $where);
        if($topten->num_rows == 0){
            show_404();
        }

        $topten_array = $topten->row_array(0);
        $where = array('tpt.id_topten_category' => $topten_array['id_topten_category']);
        $select = 'tpt.*, tpc.category_name';
        $politisi = $this->topten_lib->get_topten($select, $where, $group_by='', $order_by='tpt.no_urut');
        $result = array();
        $youtube = $this->topten_lib->get_topten_youtube($select, $where, $group_by='', $order_by='tpt.no_urut');

        $wherenot = array('id_topten_category !=' => $topten_array['id_topten_category']);
        //$wherenot = array('id_topten_category !=' => $topten_array['id_topten_category'], 'set_active' => '1');
        $toplainnya = $this->topten_lib->get_topten_category('*', $wherenot);
        $resultlain = array();$i = 0;
        foreach($toplainnya->result_array() as $rwCat)
        {
            $id_category = $rwCat['id_topten_category'];
            $resultlain[$i]['topten_id'] = $rwCat['id_topten_category'];
            $resultlain[$i]['categories_name'] = $rwCat['category_name'];
            $resultlain[$i]['created_date'] = $rwCat['created_date'];
            $resultlain[$i]['viewer'] = $rwCat['viewer'];
            $resultlain[$i]['set_color'] = $rwCat['set_color'];

            $where = array('tpt.id_topten_category' => $rwCat['id_topten_category']);
            $select = 'tpt.*, tpc.category_name';
            $polilain = $this->topten_lib->get_topten($select, $where, $group_by='', $order_by='tpt.no_urut');
            $rslain = $polilain->result_array(); $_polisiti = array();
            foreach($rslain as $key=>$value){
                $where = array('page_id' => $value['page_id']);
                $topelse = $this->redis_slave->get('profile:detail:'.$value['page_id']);
                $other_array = json_decode($topelse, true);
                $_polisiti = $other_array['page_name'];
                $other_array['deskripsi'] = $value['deskripsi'];
                $other_array['list_photo'] = $this->aktor_lib->get_foto_profile_album(40, $value['page_id'], 8, 0);
                $other_array['scandal_count'] = count($other_array['scandal']);
                $other_news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                $other_array['news_count'] = $other_news_terkait['cou'];
                $other_foll = $this->aktor_lib->get_follower($value['page_id']);
                $other_array['follower_count'] = $other_foll->num_rows();
                $resultlain[$i]['politisi'][$key] = $other_array;
            }
            $i++;
        }

        $data['toptenlain'] = $resultlain;

        $ket = 'Keterangan';
        switch($id){
            case 1: $ket = 'Keterangan'; break;
            case 2: $ket = 'Keterangan'; break;
            case 3: $ket = 'Kekayaan'; break;
            case 4: $ket = 'Keterangan'; break;
        }


        if($topten_array['topten_type'] == '1')
        {
            $data['ket'] = $ket; $_polisiti = array();
            foreach($politisi->result_array() as $key=>$value){
                $where = array('page_id' => $value['page_id']);
                $users = $this->redis_slave->get('profile:detail:'.$value['page_id']);
                $user_array = json_decode($users, true);
                $user_array['deskripsi'] = $value['deskripsi'];
                $user_array['deskripsi_lain'] = $value['deskripsi_lain'];

                $politisi_terkait[] = $user_array['page_name'];

                // $user_array['list_photo'] = $this->aktor_lib->get_foto_profile($value['page_id'], 8, 0)->result_array();

                $user_array['scandal_count'] = (array_key_exists('scandal', $user_array)) ? count($user_array['scandal']) : 0;

                $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                $user_array['news_count'] = $news_terkait['cou'];

                $foll = $this->aktor_lib->get_follower($value['page_id']);
                $user_array['follower_count'] = $foll->num_rows();
                $user_array['topten_type'] = $value['topten_type'];
                $user_array['topten_id'] = $value['topten_id'];

                $result[$key] = $user_array;
            }
        } else {
            $data['ket'] = $ket; $_polisiti = array();
            foreach($youtube->result_array() as $key=>$value){
                $result[$key] = $value;
            }
            $politisi_terkait = array();
        }

        $data['head_title'] = $topten_array['category_name'];
        $data['polterkait'] = implode($politisi_terkait, ', ');

        $data['top_category'] = $topten_array;
        $data['politisi'] = $result;

        if($topten_array['topten_type'] == '2')
        {
            $html['html']['content']  = $this->load->view('mobile/youtube_view', $data, true);
        } else {
            $html['html']['content'] = $this->load->view('mobile/index_view', $data, true);
        }

        $this->load->view('m_tpl/layout', $html);
    }

}