<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Topten extends Application
{
    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->helper('m_date');

        $this->load->helper('text');
        $this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');

        $this->load->library('aktor/aktor_lib');
        $this->load->library('caleg/caleg_lib');
        $this->load->library('topten_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->library('suksesi/suksesi_lib');
    }

    public function index($id=1) {
        $rkey = 'topten:list';
        $toptenlist = $this->redis_slave->lrange($rkey, 0, 300);
        $restopten = array();
        foreach($toptenlist as $key => $value)
        {
            if($value == $id) continue;
            $rowTopten = $this->redis_slave->get('topten:detail:'. $value);
            $jsontopten = @json_decode($rowTopten, true);
            $restopten[$key] = $jsontopten;
        }

        $data['toptenlain'] = $restopten;

        $html['html']['content'] = $this->load->view('mobile/list', $data, true);

        $this->load->view('m_tpl/layout', $html);
    }

    public function toptens($id=1)
    {
        $where = array('id_topten_category' => $id);
        $data['id'] = !empty($id) ? $id : 1;

        /*
        *    Detail Topten - MySQL Way
        */

        // $topten = $this->topten_lib->get_topten_category($select='*', $where);
        // if($topten->num_rows == 0){
        //     show_404();
        // }

        // $topten_array = $topten->row_array(0);
        // $where = array('tpt.id_topten_category' => $topten_array['id_topten_category']);
        // $select = 'tpt.*, tpc.category_name';
        // $politisi = $this->topten_lib->get_topten($select, $where, $group_by='', $order_by='tpt.no_urut');
        // $result = array();
        // $youtube = $this->topten_lib->get_topten_youtube($select, $where, $group_by='', $order_by='tpt.no_urut');
        // $wheren = array('tn.id_topten_category' => $topten_array['id_topten_category']);
        // $topnews = $this->topten_lib->get_topten_news('tn.*, tpc.category_name', $wheren, $group_by='', $order_by='tn.no_urut');

        $key = 'topten:detail:'.$id;
        $topten_array = (array)json_decode($this->redis_slave->get($key));

        /*
        *    Topten Lainnya - MySQL Way
        */

        //$wherenot = array('id_topten_category !=' => $topten_array['id_topten_category']);
        // $wherenot = array('id_topten_category !=' => $topten_array['id_topten_category'], 'set_active' => '1');
        // $toplainnya = $this->topten_lib->get_topten_category('*', $wherenot);
        // $resultlain = array();$i = 0;
        // foreach($toplainnya->result_array() as $rwCat)
        // {
        //     $id_category = $rwCat['id_topten_category'];
        //     $resultlain[$i]['topten_id'] = $rwCat['id_topten_category'];
        //     $resultlain[$i]['categories_name'] = $rwCat['category_name'];
        //     $resultlain[$i]['created_date'] = $rwCat['created_date'];
        //     $resultlain[$i]['viewer'] = $rwCat['viewer'];
        //     $resultlain[$i]['set_color'] = $rwCat['set_color'];

        //     $where = array('tpt.id_topten_category' => $rwCat['id_topten_category']);
        //     $select = 'tpt.*, tpc.category_name';
        //     $polilain = $this->topten_lib->get_topten($select, $where, $group_by='', $order_by='tpt.no_urut');
        //     $rslain = $polilain->result_array(); $_polisiti = array();
        //     foreach($rslain as $key=>$value){
        //         $where = array('page_id' => $value['page_id']);
        //         $topelse = $this->redis_slave->get('profile:detail:'.$value['page_id']);
        //         $other_array = json_decode($topelse, true);
        //         $_polisiti = $other_array['page_name'];
        //         $other_array['deskripsi'] = $value['deskripsi'];
        //         $other_array['list_photo'] = $this->aktor_lib->get_foto_profile_album(40, $value['page_id'], 8, 0);
        //         $other_array['scandal_count'] = count($other_array['scandal']);
        //         $other_news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
        //         $other_array['news_count'] = $other_news_terkait['cou'];
        //         $other_foll = $this->aktor_lib->get_follower($value['page_id']);
        //         $other_array['follower_count'] = $other_foll->num_rows();
        //         $resultlain[$i]['politisi'][$key] = $other_array;
        //     }
        //     $i++;
        // }

        $rkey = 'topten:list';
        $toptenlist = $this->redis_slave->lrange($rkey, 0, 300);
        $restopten = array();
        foreach($toptenlist as $key => $value)
        {
            if($value == $id) continue;
            $rowTopten = $this->redis_slave->get('topten:detail:'. $value);
            $jsontopten = @json_decode($rowTopten, true);
            $restopten[$key] = $jsontopten;
        }

        $data['toptenlain'] = $restopten;

//         $ket = 'Keterangan';
//         switch($id){
//             case 1: $ket = 'Keterangan'; break;
//             case 2: $ket = 'Keterangan'; break;
//             case 3: $ket = 'Kekayaan'; break;
//             case 4: $ket = 'Keterangan'; break;
//         }

//         switch($topten_array['topten_type'])
//         {
//             case '1' :

//                 $data['ket'] = $ket; $_polisiti = array();
//                 foreach($politisi->result_array() as $key=>$value){
//                     //$user_array['topten_type'] = $value['topten_type'];
//                     $where = array('page_id' => $value['page_id']);
//                     $users = $this->redis_slave->get('profile:detail:'.$value['page_id']);
//                     $user_array = json_decode($users, true);
//                     $_polisiti[] = $user_array['page_name'];
//                     $user_array['deskripsi'] = $value['deskripsi'];
//                     $user_array['deskripsi_lain'] = $value['deskripsi_lain'];
//                     //   $where = array('page_id' => $val['caleg_id']);
//                     //   $skandal_terkait = $this->caleg_lib->get_scandal_player($select='count(*) as cou', $where)->row_array(0);

//                     $user_array['list_photo'] = $this->aktor_lib->get_foto_profile_album(40, $value['page_id'], 8, 0);
//                     $user_array['scandal_count'] = count($user_array['scandal']);
//                     $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
//                     $user_array['news_count'] = $news_terkait['cou'];
//                     $foll = $this->aktor_lib->get_follower($value['page_id']);
//                     $user_array['follower_count'] = $foll->num_rows();
//                     $user_array['topten_type'] = $value['topten_type'];
//                     $user_array['topten_id'] = $value['topten_id'];

//                     $result[$key] = $user_array;
//                 } break;

//             case '2' :

//                 $data['ket'] = $ket; $_polisiti = array();
//                 foreach($youtube->result_array() as $key=>$value){
//                     $result[$key] = $value;
//                 } break;

//             case '3' :

//                 $data['ket'] = $ket; $_polisiti = array();
//                 foreach($topnews->result_array() as $key => $value)
//                 {
//                     $rkey_min = 'news:detail:'; $berita = array();$nw = 0;
//                     $have_redis_empty = false; $news = array();
//                     $newslist = explode(",", $value['news_list']);
//                     foreach($newslist as $val)
//                     {
//                         $berita = $this->redis_slave->get($rkey_min.$val);
//                         $beritason = json_decode($berita, true);
// //                       if(empty($beritason)){ $have_redis_empty = true; continue;}
//                         $value['news'][$nw] = $beritason;
//                         $nw++;
//                     }

//                     $result[$key] = $value;
//                 }
//                 break;
//         }

        $params = ['toptenType'=>$topten_array['topten_type'],'toptenId'=>$topten_array['id_topten_category']];
        $keyContent = 'topten:content:'.$id;
        $tempContent = @json_decode($this->redis_slave->get($keyContent), true);
            
        switch($topten_array['topten_type']){
            case '1' :
                $politisi = $tempContent;
                $ket = 'Keterangan : ';
                $data['ket'] = $ket; 
                $_polisiti = array();

                foreach($politisi as $key=>$value){
                    //$user_array['topten_type'] = $value['topten_type'];
                    $where = array('page_id' => $value->page_id);
                    $users = $this->redis_slave->get('profile:detail:'.$value->page_id);
                    $user_array = json_decode($users, true);
                    $_polisiti[] = $user_array['page_name'];
                    $user_array['deskripsi'] = $value->deskripsi;
                    $user_array['deskripsi_lain'] = $value->deskripsi_lain;
                    //   $where = array('page_id' => $val['caleg_id']);
                    //   $skandal_terkait = $this->caleg_lib->get_scandal_player($select='count(*) as cou', $where)->row_array(0);

                    $user_array['list_photo'] = $this->aktor_lib->get_foto_profile_album(40, $value->page_id, 8, 0);
                    $user_array['scandal_count'] = @count($user_array['scandal']);
                    $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                    $user_array['news_count'] = $news_terkait['cou'];
                    $foll = $this->aktor_lib->get_follower($value->page_id);
                    $user_array['follower_count'] = $foll->num_rows();
                    $user_array['topten_type'] = $value->topten_type;
                    $user_array['topten_id'] = $value->topten_id;


                    $rkey_min = 'news:detail:'; 
                    $berita = array();
                    $nw = 0;
                    $have_redis_empty = false; 
                    $news = array();
                    if(!empty($value->news_list)){
                        $newslist = explode(",", $value->news_list);
                        foreach($newslist as $val){
                            $berita = $this->redis_slave->get($rkey_min.$val);
                            $beritason = json_decode($berita, true);
//                       if(empty($beritason)){ $have_redis_empty = true; continue;}
                            $user_array['news'][$nw] = $beritason;
                            $nw++;
                        }
                    } else {
                        $user_array['news'] = '';
                    }

                    $result[$key] = $user_array;
                } break;

            case '2' :
                $youtube = $tempContent;
                $ket = 'Keterangan : ';
                $data['ket'] = $ket; 
                $_polisiti = array();
                foreach($youtube as $key=>$value){
                    $result[$key] = $value;
                }; 
                break;

            case '3' :
                $topnews = $tempContent;
                $ket = 'Kekayaan : ';
                $data['ket'] = $ket; 
                $_polisiti = array();
                foreach($topnews as $key => $value){
                    $rkey_min = 'news:detail:'; 
                    $berita = array();
                    $nw = 0;
                    $have_redis_empty = false; 
                    $news = array();
                    if(!empty($value->news_list)){
                      $newslist = explode(",", $value->news_list);
                      foreach($newslist as $val){
                         $berita = $this->redis_slave->get($rkey_min.$val);
                         $beritason = json_decode($berita, true);
//                       if(empty($beritason)){ $have_redis_empty = true; continue;}
                         $value->news[$nw] = $beritason;
                         $nw++;
                      }
                    } else {
                      $value->news = '';
                    }                

                    $result[$key] = $value;
                }
            break;
        }
        // echo "<pre>";var_dump($tempContent);echo "</pre>";exit();

        $data['head_title'] = $topten_array['category_name'];
        $data['polterkait'] = $_polisiti;
        $data['top_category'] = $topten_array;
        $data['politisi'] = $result;

        switch($topten_array['topten_type'])
        {
            case '1' :
                $html['html']['content'] = $this->load->view('mobile/index_view', $data, true);
                break;

            case '2' :
                $html['html']['content']  = $this->load->view('mobile/youtube_view', $data, true);
                break;

            case '3' :
                $html['html']['content'] = $this->load->view('mobile/news_view', $data, true);
                break;
        }

        $this->load->view('m_tpl/layout', $html);
    }

}
