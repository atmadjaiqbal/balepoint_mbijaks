<!DOCTYPE HTML>
<html lang="en-US" dir="ltr" class="no-js">
<head>
    <meta charset="utf-8">
    <title>Bijaks - <?php echo (key_exists('category_name', $top_category)) ? ucwords(strtolower($top_category['category_name'])) : '';?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

<!--    <script>-->
<!--        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){-->
<!--            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })-->
<!--                (window,document,'script','//www.google-analytics.com/analytics.js','ga');-->
<!--        ga('create', 'UA-40169954-1', 'bijaks.net');-->
<!--        ga('send', 'pageview');-->
<!--    </script>-->

    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>

    <link href="<?php echo base_url(); ?>assets/css/bijaks.politer.css" rel="stylesheet">
    <!--    <link href="--><?php //echo base_url(); ?><!--assets/css/bootstrap-responsive.css" rel="stylesheet">-->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
    <![endif]-->

</head>
<body>
<div id="header" class="navbar-fixed-top">
    <div class="navbar-top-ten navbar-top-ten-<?php echo (key_exists('id_topten_category', $top_category)) ? $top_category['id_topten_category'] : '1';?>">
        <div class="container-960 container-960-header-<?php echo (key_exists('id_topten_category', $top_category)) ? $top_category['id_topten_category'] : '1';?>">
            <div class="row-fluid">
                <div class="span6">
<!--                    <img class="caleg-image-header" src="--><?php //echo base_url();?><!--assets/images/caleg-image-header.png">-->
<!--                    <a class="brand" href="#">-->
<!--                        <img class="bijaks-logo" src="--><?php //echo base_url();?><!--assets/images/bijaks_logo.png">-->
<!--                    </a>-->
                </div>
                <div class="span6">
                    <!-- <div class="text-header-place">
                        <p class="p-white"><?php //echo (key_exists('category_name', $top_category)) ? $top_category['category_name'] : '';?></p>
                    </div> -->

                </div>
            </div><!--/.nav-collapse -->
        </div>
    </div>
    <div class="div-line-header div-line-header-<?php echo (key_exists('id_topten_category', $top_category)) ? $top_category['id_topten_category'] : '1';?>">
    </div>
</div>


