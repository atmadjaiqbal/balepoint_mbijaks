<!-- footer -->
<div id="footer" class="footer-<?php echo (key_exists('id_topten_category', $top_category)) ? $top_category['id_topten_category'] : '1';?>">
<!--    <div class="container text-right">-->
<!--        <p>Copyright &copy; 2014, Bijaks.net. All Right Reserved.</p>-->

<!--    </div>-->
        <div class="container-960">
            <div class="row-fluid">
                <div class="span3">
                    <div class="footer-logo">
                        <!-- <span style="font-size: 14px;">powered by</span>
                        <div style="font-size: 28px;"><span style="font-size: 28px;color: #fdae27;">IGG</span>/Tek</div> -->
                    </div>
                </div>
                <div class="span9">
                    <div class="pull-right">
                        <ul class="foo-nav">
                            <li class=""><a id="tentang" title="Tentang bijaks" href="#">Tentang</a></li>
                            <li class="foo-divider"></li>
                            <li class=""><a id="kontak" title="Alamat bijaks" href="">Kontak</a></li>
                            <li class="foo-divider"></li>
                            <li class=""><a id="privasi" title="Kebijakan privasi dan policy" href="#">Kebijakan Privasi</a></li>
                            <li class="foo-divider"></li>
                            <li class=""><a id="ketentuan" title="Syarat dan ketentuan" href="#">Syarat &amp; Ketentuan</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>

<div id="myModal" style="width: 650px;" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Modal header</h3>
    </div>
    <div style="padding-bottom: 30px;" class="modal-body">

    </div>
<!--    <div class="modal-footer">-->
<!--        <a href="#" class="btn">Close</a>-->
<!--        <a href="#" class="btn btn-primary">Save changes</a>-->
<!--    </div>-->
</div>

</body>


<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js" ></script>
<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscrollHorizontal.min.js" ></script>

<script>
    $(document).ready(function(){
    	$('.galery-img').click(function(ev){
            ev.preventDefault();
            var uri = $(this).data('uri');
            var pg = $(this).data('pg');
            $('#main-image-' + pg).attr('src', uri);
        })
        var img_size = 80;

        $(".politer-galery-container").each(function(i, x){
//            e.preventDefault();
            var co = $(x).data('zount');
            var width = parseInt(co) * img_size;
            console.debug(width);
            $(x).width(width );
        });

//        console.debug($(".politer-galery-container").width());
        $('.politer-galery').slimScrollH({
            height : '70px',
            width: '490px',
            alwaysVisible: true,
            allowPageScroll : true,
            railColor : '#333',
            color: '#B8B8B8',
            opacity : 1,
            railOpacity : 1,
            distance : '3px'
        });

        $('.modal-body').slimscroll({
            height : '400px',
            alwaysVisible: true,
            allowPageScroll : true,
            color: '#B8B8B8',
            opacity : .7
        })

        $('#tentang').click(function(e){
            e.preventDefault();
            $('.modal-header h3').text('TENTANG BIJAKS');
            $.get('<?php echo base_url('page/about');?>', function(dt){
                $('.modal-body').html(dt);
            })
            $('#myModal').modal('show');
        });

        $('#kontak').click(function(e){
            e.preventDefault();
            $('.modal-header h3').text('KONTAK BIJAKS');
            $.get('<?php echo base_url('page/contact');?>', function(dt){
                $('.modal-body').html(dt);
            })
            $('#myModal').modal('show');
        });

        $('#privasi').click(function(e){
            e.preventDefault();
            $('.modal-header h3').text('KEBIJAKAN PRIVASI');
            $.get('<?php echo base_url('page/privacy');?>', function(dt){
                $('.modal-body').html(dt);
            })
            $('#myModal').modal('show');
        });
        $('#ketentuan').click(function(e){
            e.preventDefault();
            $('.modal-header h3').text('SYARAT DAN KETENTUAN');
            $.get('<?php echo base_url('page/term_condition');?>', function(dt){
                $('.modal-body').html(dt);
            })
            $('#myModal').modal('show');
        });

        $('#myModal').on('hidden', function(){
            $('.modal-header h3').text('');
            $('.modal-body').html('');
        })

    });

</script>

</html>