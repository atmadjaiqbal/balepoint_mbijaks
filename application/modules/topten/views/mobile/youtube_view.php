<h4 class="kanal-title a-block-<?php echo $top_category['id_topten_category'] <= 4 ? $top_category['id_topten_category'] : '1';?>"><?php echo $top_category['category_name'];?></h4>
<?php
foreach($politisi as $key=>$value){
?>
    <div class="row">
        <div class="col-xs-12 col-sm-12">

            <?php if($value['topten_link'] == 0) { ?>
                <a style="cursor: default;"><h4 class="media-heading"><?php echo $value['top_title'];?></h4></a>
            <?php } else { ?>
                <a href="<?php echo $value['topten_url']; ?>"><h4 class="media-heading"><?php echo $value['top_title'];?></h4></a>
            <?php } ?>

            <?php if(!empty($value['top_youtube'])) {?>
            <div style="text-align: left;">
                <iframe style="width: 100%;height: auto;" src="<?php echo $value['top_youtube']; ?>" frameborder="0" allowfullscreen></iframe>
            </div>
            <?php } else { ?>
                <img style="width: 200px;height: auto;" src="<?php echo $value['top_img'];?>">
            <?php } ?>

            <span class="img-media-list"><?php echo $value['top_description'];?></span>
        </div>
    </div>
    <hr class="line-mini">
<?php }?>

<hr class="line-mini">
<h4 class="kanal-title kanal-title-gray">TOP 10 LAINNYA</h4>

<?php
if(!empty($toptenlain))
{
    foreach($toptenlain as $rwlain)
    {
        ?>
        <div class="row">
            <a href="<?php echo base_url('topten/toptens') .'/'. $rwlain['id_topten_category'];?>">
                <div class="col-xs-12 col-sm-12">
                    <h4 class="media-heading"><?php echo $rwlain['category_name']; ?></h4>
                    <em><small><?php echo date('d M Y', strtotime($rwlain['created_date'])); ?></small></em>
                    <em><small><?php //echo $rwlain['viewer'];?> --- viewers</small></em>
                </div>
            </a>
        </div>
        <hr class="line-mini">
    <?php
    }
}
?>
