<?php
foreach($news as $key=>$result){

    $_topic_id = !empty($result['topic_id']) ? $result['topic_id'] : '';
    $_id = !empty($result['id']) ? $result['id'] : '';
    $_slug = !empty($result['slug']) ? $result['slug'] : '';
    $_title = !empty($result['title']) ? $result['title'] : '';

    $news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
    $short_title = (strlen ($_title) > 38) ? substr($_title, 0, 38). '...' : $_title;
    if(!empty($result['image_thumbnail']))
    {
        $_image_thumb = $result['image_thumbnail'];
    } else {
        $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
    }

    if($key > 5) break;
    ?>

    <div class="row">
      <div class="col-xs-12 col-sm-12">
         <div style="width: 85px;float:left;">
            <a class="" href="<?php echo $news_url;?>">
               <img class="img-topten-list" src="<?php echo $_image_thumb;?>" alt="<?php echo !empty($result['title']) ? $result['title'] : ''; ?>" >
            </a>
         </div>
         <div style="width: 265px;;height:50px;float:left;">
            <a class="" href="<?php echo $news_url;?>">
                <h5 class="media-heading-nomargin"><?php echo $_title; ?></h5>
            </a>
            <small class="media-heading-nomargin"><?php echo !empty($result['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($result['date'])) : ''; ?></small>
         </div>
      </div>
    </div>
    <hr class="line-mini">

<?php } ?>
