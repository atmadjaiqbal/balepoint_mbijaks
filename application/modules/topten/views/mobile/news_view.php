<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>
<style type="text/css">
  .mob1 .thumb1 {
    float: left;
  }
  .mob1 .thumb1 img {
    width: 50px;
    margin-right: 5px;
  }
  .gallery, .gallery2 {
    list-style: none outside none;
    padding-left: 0;
    margin-left: 0px;
  }
  .gallery li {
      display: block;
      float: left;
      height: 50px;
      margin-bottom: 7px;
      margin-right: 0px;
      width: 60px;
  }
  .gallery li a {
      height: 100px;
      width: 100px;
  }
  .gallery li a img {
      max-width: 55px;
  }
  #lg-outer {
      background: rgba(0,0,0,0.6) !important;
  }
</style>
<script type="text/javascript">
$(document).ready(function(){
    $(".gallery").lightGallery();
    $(".gallery2").lightGallery();
})
</script>
<h4 class="kanal-title a-block-<?php echo $top_category['id_topten_category'] <= 4 ? $top_category['id_topten_category'] : '1';?>"><?php echo $top_category['category_name'];?></h4>
<p style="text-align: justify;"><?php echo $top_category['description'];?></p>
<?php
$i = 1;
foreach($politisi as $key=>$value){
?>
    <div class="row">
       <div class="col-xs-4 col-sm-4 nopadding">
        <?php
          if ($id == 48) {
            ?>
            <ul id="light-gallery" class="gallery2">
                <li data-src="<?php echo $value['top_img'];?>">
                    <a href="#">
                        <img src="<?php echo $value['top_img'];?>" style="display: block;width: 100%;" />
                    </a>
                </li>
            </ul>
            <!-- <img class="top-image-large" src="<?php echo $val['top_img'];?>" id="image-large-<?php echo $val['topten_news_id'];?>"> -->
            <?php
          }
          else {
            ?>
            <img style="width: 100%;height: auto;" src='<?php echo $value['top_img'];?>'>
            <?php
          }
          ?>        
       </div>
       <div class="col-xs-8 col-sm-8">
            <div class="div-line-index">#<?php echo $i; ?></div><br/>
           <h4><a style="cursor: default;margin-top:15px;margin-left:0px;"><?php echo $value['top_title'];?></a></h4>
       </div>

    </div>
    <div class="row">
       <div class="col-xs-12 col-sm-12">
           <div class="img-media-list" style="text-align: justify;"><?php echo $value['top_description'];?></div><br/>
           <div style="clear: both;"></div>
           <!-- <div>Beritanya disini : </div>
           <div>
            <?php $dt['news'] = $value['news']; $this->load->view('mobile/tpl_topten_news', $dt); ?>
          </div> -->
       </div>
    </div>

    <hr class="line-mini">
<?php
  $i++;
}
?>

<hr class="line-mini">
<h4 class="kanal-title kanal-title-gray">TOP 10 LAINNYA</h4>

<?php
if(!empty($toptenlain))
{
    foreach($toptenlain as $rwlain)
    {
        ?>
        <div class="row">
            <a href="<?php echo base_url('topten/toptens') .'/'. $rwlain['id_topten_category'];?>">
                <div class="col-xs-12 col-sm-12">
                    <h4 class="media-heading"><?php echo $rwlain['category_name']; ?></h4>
                    <em><small><?php echo date('d M Y', strtotime($rwlain['created_date'])); ?></small></em>
                    <em><small><?php //echo $rwlain['viewer'];?> --- viewers</small></em>
                </div>
            </a>
        </div>
        <hr class="line-mini">
    <?php
    }
}
?>
