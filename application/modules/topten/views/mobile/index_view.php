<h4 class="kanal-title a-block-<?php echo $top_category['id_topten_category'] <= 4 ? $top_category['id_topten_category'] : '1';?>"><?php echo $top_category['category_name'];?></h4>
<?php
    foreach($politisi as $key=>$value){
    	if(!array_key_exists('page_name', $value))
    		continue;

    	$url = base_url('aktor/profile/'.$value['page_id']);
		$pp = (count($value['profile_photo']) > 0) ? $value['profile_photo'][0]['badge_url'] : '';
		$content_date = ($value['partai_name'] != 'None') ? ' - '.$value['partai_name'] : '';
?>

<div class="row">
   <div class="col-xs-4 col-sm-4 nopadding">
  	  <a class="pull-left " href="<?php echo $url;?>">
         <img class="img-media-list" src="<?php echo $pp;?>" alt="<?php echo $value['page_name'];?>" >
      </a>
   </div>
   <div class="col-xs-8 col-sm-8">
      <?php if($value['topten_type'] == '0') { ?>
	  <a class="" href="<?php echo $url;?>">
         <h4 class="media-heading"><?php echo $value['page_name'];?><small><?php echo $content_date;?></small></h4>
      </a>
      <?php } else { ?>
          <a style="cursor: default;"><h4 class="media-heading"><?php echo $value['page_name'];?><small><?php echo $content_date;?></small></h4></a>
      <?php } ?>
      <?php if(!empty($value['deskripsi'])){ ?>
         <span class="img-media-list"><?php echo $ket .' : '. $value['deskripsi'];?></span>
      <?php } ?>

      <?php if(!empty($value['deskripsi_lain'])){ ?>
         <span class="img-media-list"><?php echo $value['deskripsi_lain'];?></span>
      <?php } ?>

      <?php if($value['topten_type'] == '0') { ?>
      <a target="__blank" class="a-block a-block-<?php echo (key_exists('id_topten_category', $top_category)) ? $top_category['id_topten_category'] : '1';?>" href="<?php echo base_url();?>aktor/scandals/<?php echo $value['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $value['scandal_count'];?></span></a>
      <a target="__blank" class="a-block" href="<?php echo base_url();?>aktor/news/<?php echo $value['page_id'];?>">BERITA <span class="clearfix"><?php echo $value['news_count'];?></span></a>
            <!-- <a target="__blank" class="a-block a-block-<?php echo (key_exists('id_topten_category', $top_category)) ? $top_category['id_topten_category'] : '1';?>" href="<?php echo base_url();?>aktor/follow/<?php echo $value['page_id'];?>">FOLLOWER <span class="clearfix"><?php echo $value['follower_count'];?></span></a> -->
      <?php } ?>

   </div>
</div>
<hr class="line-mini">
	
<?php }?>

<hr class="line-mini">
<h4 class="kanal-title kanal-title-gray">TOP 10 LAINNYA</h4>

<?php
if(!empty($toptenlain))
{
    foreach($toptenlain as $rwlain)
    {
?>
<div class="row">
  <a href="<?php echo base_url('topten/toptens') .'/'. $rwlain['id_topten_category'];?>">
  <div class="col-xs-12 col-sm-12">
     <h4 class="media-heading"><?php echo $rwlain['category_name']; ?></h4>
     <em><small><?php echo date('d M Y', strtotime($rwlain['created_date'])); ?></small></em>
     <em><small><?php // echo $rwlain['viewer'];?> --- viewers</small></em>
  </div>
  </a>
</div>
<hr class="line-mini">
<?php
    }
}
?>
