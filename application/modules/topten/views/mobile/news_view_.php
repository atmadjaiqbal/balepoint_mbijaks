<h4 class="kanal-title a-block-<?php echo $top_category['id_topten_category'] <= 4 ? $top_category['id_topten_category'] : '1';?>"><?php echo $top_category['category_name'];?></h4>
<?php
$i = 1;
foreach($politisi as $key=>$value){
?>
    <div class="row">
       <div class="col-xs-4 col-sm-4 nopadding">
          <a class="pull-left " href="<?php echo $url;?>">
             <img style="width: 100%;height: auto;" src='<?php echo $value['top_img'];?>'>
          </a>
       </div>
       <div class="col-xs-8 col-sm-8">
            <div class="div-line-index">#<?php echo $i; ?></div><br/>
           <h4><a style="cursor: default;margin-top:15px;margin-left:0px;"><?php echo $value['top_title'];?></a></h4>
       </div>

    </div>

    <div class="row">
       <div class="col-xs-12 col-sm-12">
           <span class="img-media-list"><?php echo $value['top_description'];?></span>
           <p>Beritanya disini : </p>
           <div><?php $dt['news'] = $value['news']; $this->load->view('mobile/tpl_topten_news', $dt); ?></div>
       </div>
    </div>

    <!-- hr class="line-mini"-->
<?php
  $i++;
}
?>

<hr class="line-mini">
<h4 class="kanal-title kanal-title-gray">TOP 10 LAINNYA</h4>

<?php
if(!empty($toptenlain))
{
    foreach($toptenlain as $rwlain)
    {
        ?>
        <div class="row">
            <a href="<?php echo base_url('topten/toptens') .'/'. $rwlain['topten_id'];?>">
                <div class="col-xs-12 col-sm-12">
                    <h4 class="media-heading"><?php echo $rwlain['categories_name']; ?></h4>
                    <em><small><?php echo date('d M Y', strtotime($rwlain['created_date'])); ?></small></em>
                    <em><small><?php //echo $rwlain['viewer'];?> --- viewers</small></em>
                </div>
            </a>
        </div>
        <hr class="line-mini">
    <?php
    }
}
?>
