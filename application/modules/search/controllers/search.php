<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends Application
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('m_date');

		$this->load->helper('text');
		$this->load->helper('seourl');

		$this->load->library('search/search_lib');
		$this->load->library('caleg/caleg_lib');

      $this->load->module('timeline/timeline');
      $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
      $this->redis_slave = $this->redis_slave->connect();		
	}

	public function politisi()
	{
		$term = $_GET['term'];

		$query = $this->search_lib->searchPolitisi($term);

		$row = $query;

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($row));
	}

	public function index()
	{
      	$data 	= $this->data;
      	$data['term'] = (!isset($_GET['term']) ? NULL : $_GET['term']);
		
		$data['politisi'] = $this->ajax_politisi($data['term'], $tipe=1);
		$data['news'] = $this->ajax_news($data['term'], $tipe=1);
		$data['scandal'] = $this->ajax_scandal($data['term'], $tipe=1);
		
      	$html['html']['content'] = $this->load->view('mobile/search_index', $data, true);
        $this->load->view('m_tpl/layout', $html);

	}

	public function ajax_politisi($term, $tipe=1)
	{
		$max = 10;
      	$limit = 10;
		$page = 1;
		$per_page = $this->input->get('per_page');
		if(!is_bool($per_page)){
			if(!empty($per_page)){
				$page = $per_page;	
			}
		}

		if($max == intval($page)){
			echo '';
			return;
		}

		$offset = ($limit*$page) - $limit;

		$searchpolitisi = array();			
		$politisis = $this->search_lib->searchPolitisi($term, $limit, $offset);
		foreach ($politisis as $key => $value) {
			$poli_detail = $this->redis_slave->get('profile:detail:'.$value['page_id']);
            $arr_poli = @json_decode($poli_detail, true);

			$arr_poli['scandal_count'] = (array_key_exists('scandal', $arr_poli)) ? count($arr_poli['scandal']) : 0;

            $where = array('page_id' => $value['page_id']);

            $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
            $arr_poli['news_count'] = $news_terkait['cou'];
            $searchpolitisi[$key] = $arr_poli;
		}

		$data['searchpolitisi'] = $searchpolitisi;

		if($tipe == 1){
			return $this->load->view('search/mobile/search_list_politisi.php', $data, true);
		}else{
			$this->load->view('search/mobile/search_list_politisi.php', $data);
		}



	}

	public function ajax_news($term, $tipe=1)
	{
		$max = 10;
      	$limit = 10;
		$page = 1;
		$per_page = $this->input->get('per_page');
		if(!is_bool($per_page)){
			if(!empty($per_page)){
				$page = $per_page;	
			}
		}

		if($max == intval($page)){
			echo '';
			return;
		}

		$offset = ($limit*$page) - $limit;

		$searchnews = array();			
		$politisis = $this->search_lib->searchNews($term, $limit, $offset, "");
		foreach ($politisis as $key => $value) {
			$poli_detail = $news_detail = $this->redis_slave->get('news:detail:'.$value['news_id']);
            $news_array = @json_decode($poli_detail, true);
            
            $searchnews[$key] = $news_array;
		}

		$data['searchnews'] = $searchnews;

		if($tipe == 1){
			return $this->load->view('search/mobile/search_list_news.php', $data, true);
		}else{
			$this->load->view('search/mobile/search_list_news.php', $data);
		}

	}

	public function ajax_scandal($term, $tipe=1)
	{
		$max = 10;
      	$limit = 10;
		$page = 1;
		$per_page = $this->input->get('per_page');
		if(!is_bool($per_page)){
			if(!empty($per_page)){
				$page = $per_page;	
			}
		}

		if($max == intval($page)){
			echo '';
			return;
		}

		$offset = ($limit*$page) - $limit;

		$result = array();			
		$politisis = $this->search_lib->searchScandal($term, $limit, $offset);
		foreach ($politisis as $key => $value) {
			$skandal_detail = $this->redis_slave->get('scandal:min:'.$value['scandal_id']);
            $skandal_array = @json_decode($skandal_detail, true);

            $skandal_photo = $this->redis_slave->get('scandal:photo:'.$value['scandal_id']);
            $skandal_photo_array = @json_decode($skandal_photo, true);

            $skandal_players = $this->redis_slave->get('scandal:players:'.$value['scandal_id']);
            $skandal_players_array = @json_decode($skandal_players, true);

            $skandal_relations = $this->redis_slave->get('scandal:relations:'.$value['scandal_id']);
            $skandal_relations_array = @json_decode($skandal_relations, true);

            $skandal_array['photos'] = array_reverse($skandal_photo_array, true);
            $skandal_array['players'] = $skandal_players_array;
            $skandal_array['relations'] = $skandal_relations_array;

            $result[$key] = $skandal_array;
            
		}

		$data['searchscandal'] = $result;

		if($tipe == 1){
			return $this->load->view('search/mobile/search_list_scandal.php', $data, true);
		}else{
			$this->load->view('search/mobile/search_list_scandal.php', $data);
		}

	}


	public function ajax_member()
	{
      $data 			= $this->data;
		$data['term'] 	= (!isset($_GET['term'])) ? NULL : $_GET['term'];

		if($data['term'])
		{
			$data['limit']						= 10;
			$offset 								= (isset($_GET['offset']) ? intval($_GET['offset']) : 0);
			$data['searchmembers'] 			= $this->search_lib->searchMember($data['term'], $data['limit'], $offset, "");
			$data['member_next_offset'] 	= $data['limit'] + $offset;
			$data['term'] 						= htmlspecialchars($data['term']);

			$this->load->view('search/search_list_member', $data);
		}
	}


	public function ajax_sentimen()
	{
      $data 			= $this->data;
		$data['term'] 	= (!isset($_GET['term'])) ? NULL : $_GET['term'];

		if($data['term'])
		{
			$data['limit']						= 10;
			$offset 								= (isset($_GET['offset']) ? intval($_GET['offset']) : 0);
			$data['searchsentimen'] 			= $this->search_lib->searchSentimen($data['term'], $data['limit'], $offset, "");
			$data['sentimen_next_offset'] 	= $data['limit'] + $offset;
			$data['term'] 						= htmlspecialchars($data['term']);

			$this->load->view('mobile/search_list_sentimen', $data);
		}
	}

	public function ajaxsentimensearch()
	{
		$term = (!isset($_GET['term']) ? NULL : $_GET['term']);
		$mode = (!isset($_GET['mode']) ? 'all' : strtolower($_GET['mode']));
		if($term)
		{
			$limit = 10;
			$offset = (isset($_GET['offset']) ? intval($_GET['offset']) : 0);
			$query = $this->search_lib->searchSentimen($term, $limit, $offset);
			$data['searchsentimen'] = $query;
			$data['sentimen_next_offset'] = $limit + $offset;

			$data['term'] = htmlspecialchars($term);
			$data['mode'] = $mode;
			$member = $this->session->userdata('member');
			$data['member'] = $member;

			$this->load->view('include/search_sentimen_list', $data);

		}
	}



 	private function checkMember() {
 	   $this->load->model('member', 'member_model');
		if($this->member) {
			$this->member['current_city'] = $this->member_model->getUserCurrentCity($this->member['account_id']);
		} else {
			redirect($this->data['base_url'].'home/login/?redirect='.$this->current_url);
		}
	}

   private function checkUID() {
		$uID = $this->input->post('uID');
		if(isset($_GET['uid']) && ! empty($_GET['uid'])) {
			$this->uID = $_GET['uid'];
			$this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);

		} else if(! empty($uID)) {
			$this->uID = $uID;
			$this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);

		} else {
			$this->uID  = $this->member['account_id'];
			$this->isLogin = true;
		}
	}

	private function isLogin($str1, $str2) {
		if(! strcmp($str1, $str2)) {
			return true;
		} else {
			return false;
		}
	}
}