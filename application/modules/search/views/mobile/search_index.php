<h4 class="kanal-title kanal-title-gray">HASIL PENCARIAN</h4>

<?php
	$searchpolitisi = trim($politisi);
	if(!empty($searchpolitisi)){ ?>
		<div class="row">
			<div class="col-xs-12" id="container_politisi">
				<h5>POLITISI</h5>
				<hr class="line-mini">
				<?php echo $searchpolitisi; ?>
				
			</div>
			<div class="col-xs-12 text-right">
				<a data-page='2' id="more_politisi" data-term="<?php echo $term; ?>" class="btn btn-default btn-sm">Load more <img id="loader_poli" src="<?php echo base_url('assets/images/loading.gif');?>"></a>
			</div>
		</div>
<?php } ?>

<?php
	$searchnews = trim($news);
	if(!empty($searchnews)){ ?>
		<div class="row">
			<div class="col-xs-12" id="container_news">
				<h5>BERITA</h5>
				<hr class="line-mini">
				<?php echo $searchnews; ?>
				
			</div>
			<div class="col-xs-12 text-right">
				<a data-page='2' id="more_news" data-term="<?php echo $term; ?>" class="btn btn-default btn-sm">Load more <img id="loader_news" src="<?php echo base_url('assets/images/loading.gif');?>"></a>
			</div>
		</div>
<?php } ?>

<?php
	$searchscandal = trim($scandal);
	if(!empty($searchscandal)){ ?>
		<div class="row">
			<div class="col-xs-12" id="container_scandal">
				<h5>SCANDAL</h5>
				<hr class="line-mini">
				<?php echo $searchscandal; ?>
				
			</div>
			<div class="col-xs-12 text-right">
				<a data-page='2' id="more_scandal" data-term="<?php echo $term; ?>" class="btn btn-default btn-sm">Load more <img id="loader_scandal" src="<?php echo base_url('assets/images/loading.gif');?>"></a>
			</div>
		</div>
<?php } ?>


<script type="text/javascript">
	$(function(){
		$('#loader_poli').hide();
		$('#loader_news').hide();
		$('#loader_scandal').hide();

		$('#more_politisi').click(function(){
			$('#loader_poli').show();
			var per_page = $(this).data('page');
			var term = $(this).data('term');
			$.get('<?php echo base_url();?>search/ajax_politisi/'+ term + '/2', {'per_page':per_page}, function(data){
				console.debug(data.trim());
				if(data.trim()){
					$('#container_politisi').append(data);
					$('#more_politisi').data('page', per_page + 1);
					$('#loader_poli').hide();
				}else{
					$('#more_politisi').remove();
				}
				
			});
		})

		$('#more_news').click(function(){
			$('#loader_news').show();
			var per_page = $(this).data('page');
			var term = $(this).data('term');
			$.get('<?php echo base_url();?>search/ajax_news/'+ term + '/2', {'per_page':per_page}, function(data){
				console.debug(data.trim());
				if(data.trim()){
					$('#container_news').append(data);
					$('#more_news').data('page', per_page + 1);
					$('#loader_news').hide();
				}else{
					$('#more_news').remove();
				}
				
			});
		})

		$('#more_scandal').click(function(){
			$('#loader_scandal').show();
			var per_page = $(this).data('page');
			var term = $(this).data('term');
			$.get('<?php echo base_url();?>search/ajax_scandal/'+ term + '/2', {'per_page':per_page}, function(data){
				console.debug(data.trim());
				if(data.trim()){
					$('#container_scandal').append(data);
					$('#more_scandal').data('page', per_page + 1);
					$('#loader_scandal').hide();
				}else{
					$('#more_scandal').remove();
				}
				
			});
		})

	})
</script>