<?php foreach($searchmembers as $guestmember):?>
<?php $image = thumb_url($guestmember['attachment_title'],'user/'.$guestmember['user_id']); ?>
<?php $url =base_url().'komunitas/profile/'.$guestmember['user_id']; ?>
<div class="member">
   <div class="member-foto">
		<a href="<?php echo $url;?>">
		<img src="<?php echo $image;;?>">
		</a>
   </div>
   
   <div class="member-detail">
	  <a href="<?php echo $url;?>" title="<?php echo $url;?>">
       <strong><?php echo ucwords($guestmember['display_name']);?></strong>
      </a>&nbsp;
	  <?php
	    	if($guestmember['gender'] == 'M')
	    		echo 'Laki-laki, ';
	    	else 
	    		echo 'Perempuan, ';
	    	
	    	if(!empty($guestmember['birthday']))
	    	{
	         $birthDate = explode("-", $guestmember['birthday']); // yyyy-mm-dd
	         //get age from date or birthdate
	         $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md") ? ((date("Y")-$birthDate[0])-1):(date("Y")-$birthDate[0]));	    			    		
	    	} else {
	    	   $age = "-";	
	    	}	
         echo $age.' tahun'.($guestmember['current_city'] != NULL ? ', '.$guestmember['current_city'].'.' : '.');	          
	  ?>
      <div class="score-place score" style="margin-left:-35px;margin-top:0px;" data-tipe="1" data-id="<?php echo $guestmember['account_id'];?>"></div>
   </div>
   	
</div>
<?php endforeach;?>

<?php if (count($searchmembers) ==$limit) : ?>
<section>
   <div class="row-fluid load-more-loader"></div>	
   <div class="row-fluid load-more" data-url="<?php echo base_url().'search/ajax_member?term='.urlencode($term).'&offset=';?>" data-page="<?=$member_next_offset;?>">
   	<a>Load More</a>
   </div>      						
</section>
<?php endif;?>