<?php $this->load->helper('seourl');?>
<?php foreach($searchsentimen as $sentimen):?>
<?php if ($sentimen['content_group_name'] =='skandal'):?>
<?php $url = base_url() .'scandal/index/'.$sentimen['id'].'-'. urltitle ( $sentimen['title']); ?>
<?php else :?>
<?php $url = base_url() .strtolower($sentimen['content_group_name']).'/index/'.$sentimen['id'].'-'. urltitle ( $sentimen['title']); ?>
<?php endif;?>

<div class="berita-politik" style="height:125px;">		
	<h4><a href="<?php echo $url;?>"><?php echo word_limiter($sentimen['title'], 10); ?></a></h4>
		<div class="text-smaller">
            <?php echo ucwords(strtolower($sentimen['content_group_name']));?>
		</div>
<!--        <div class="score-place score" data-tipe="1" data-id="<?php echo $sentimen['content_id'];?>"></div> -->
		<div><p><?php echo $sentimen['description']; ?></p></div>
		<?php if(!empty($sentimen['content_id'])):?>						
		<?php endif;?>
</div>

<?php endforeach;?>

<?php if (count($searchsentimen) ==$limit) : ?>
<section>
   <div class="row-fluid load-more-loader"></div>	
   <div class="row-fluid load-more" data-url="<?php echo base_url().'search/ajax_sentimen?term='.urlencode($term).'&offset=';?>" data-page="<?=$sentimen_next_offset;?>">
   	<a>Load More</a>
   </div>      						
</section>
<?php endif;?>