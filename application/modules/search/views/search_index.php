<div class="container" style="margin-top: 42px;">
  <div class="sub-header-container">
	<div class="logo-small-container">
       <a href="<?php echo base_url(); ?>">
		<img src="<?php echo base_url('assets/images/logo.png'); ?>" >
       </a>
	</div>
	<div class="right-container">
		<div class="banner">
			<img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
		</div>
		<div class="category-news">
			<div class="category-news-title category-news-title-right category-news-title-merah">
				<h1>HASIL PENCARIAN</h1>
			</div>
			
		</div>
	</div>
  </div>	
  <div id="search-box" class="sub-header-container">
     <div class="search-result">   
       <div class="search-title"><h4>POLITISI</h4></div>
         <div class="row-fluid search-listing">
<?php
	if(empty($searchpolitisi)){
		?><p>pencarian "<i><?php echo $term;?></i>" pada politisi tidak menemukan hasil</p><?php
	} else {
		$this->load->view('search/search_list_politisi.php');
	}
?>
         </div>
     
       <div class="search-title"><h4>BERITA POLITIK</h4></div>
         <div class="row-fluid search-listing">
<?php
	if(empty($searchnews)){
		?><p>pencarian "<i><?php echo $term;?></i>" pada news tidak menemukan hasil</p><?php
	} else {
		$this->load->view('search/search_list_news.php');
	}
?>
         </div>

       <div class="search-title"><h4>Members</h4></div>
          <div class="row-fluid search-listing">
<?php
	if(empty($searchmembers)){
		?><p>pencarian "<i><?php echo $term;?></i>" tidak menemukan hasil</p><?php
	} else {
		$this->load->view('search/search_list_member.php');
	}
?>
          </div>

       <div class="search-title"><h4>Sentimen</h4></div>
          <div class="row-fluid search-listing">
<?php
	if(empty($searchsentimen)){
		?><p>pencarian "<i><?php echo $term;?></i>" pada sentimen tidak menemukan hasil</p><?php
	} else {
		$this->load->view('search/search_list_sentimen.php');
	}
?>
         </div>
     
     </div>    
  </div>
  
  
</div>
<br/>
