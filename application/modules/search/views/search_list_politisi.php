<?php foreach($searchpolitisi as $politisi):?>
	<?php $image = thumb_port_url($politisi['attachment_title'],'politisi/'.$politisi['page_id']); ?>
	<div class="politisi">
		<div class="span1 politisi-image">
			<a href="<?php echo base_url().'aktor/profile/'.$politisi['page_id'];?>">
			<img src="<?php echo $image;?>">
			</a>
		</div>
		<div class="span10 politisi-about" style="vertical-align:top;">
			<a href="<?php echo base_url().'aktor/profile/'.$politisi['page_id'];?>">
				<h4><?php echo $politisi['page_name'];?></h4>
			</a>
            <div class="score-place score" style="margin-left:-35px;margin-top:-12px;" data-tipe="1" data-id="<?php echo $politisi['page_id'];?>"></div>
            <?php
				//$retval = strip_tags($politisi['about'], '<p>');
				$retval = strip_tags($politisi['about']);
	    		$array = explode(' ', $retval);
			    if (count($array) > 37) {
	        		array_splice($array, 37);
			        $retval = implode(' ', $array).' ...';
			    }
			    echo $retval;
			?>
		
		</div>
	</div>
<?php endforeach;?>


<?php if (count($searchpolitisi) ==$limit) : ?>
<section>
   <div class="row-fluid load-more-loader"></div>	
   <div class="row-fluid load-more" data-url="<?php echo base_url().'search/ajax_politisi?term='.urlencode($term).'&offset=';?>" data-page="<?=$politisi_next_offset;?>">
   	<a>Load More</a>
   </div>      						
</section>
<?php endif;?>
