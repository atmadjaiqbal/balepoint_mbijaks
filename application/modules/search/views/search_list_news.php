<?php foreach($searchnews as $berita):?>
<div class="berita-politik">		
	<h4>	
	<a href='<?php echo base_url() . "news/article/0-" . $berita['news_id'] . "/" . $berita['content_url']; ?>'><?php echo $berita['title']; ?></a>
	</h4>
   <div class="berita-photo">
     <a href='<?php echo base_url() . "news/article/0-" . $berita['news_id'] . "/" . $berita['content_url']; ?>'>
		<img src="http://news.bijaks.net/uploads<?php echo $berita['content_image_url']; ?>">
	  </a>
   </div>
   <div class="berita-content">
       <div class="score-place score" style="margin-left:-35px;margin-top:-5px;" data-tipe="1" data-id="<?php echo $berita['content_id'];?>"></div>
       <p><?php echo preg_replace("/<a href=.*?>(.*?)<\/a>/","",$berita['content_text']); ?></p>
   </div>
</div>
<?php endforeach;?>


<?php if (count($searchnews) ==$limit) : ?>
<section>
   <div class="row-fluid load-more-loader"></div>	
   <div class="row-fluid load-more" data-url="<?php echo base_url().'search/ajax_news?term='.urlencode($term).'&offset=';?>" data-page="<?=$news_next_offset;?>">
   	<a>Load More</a>
   </div>      						
</section>
<?php endif;?>