<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Search_Lib {

	private $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();

		$this->db         = $this->CI->load->database('slave', true);
	}

	public function searchNews($term, $limit, $offset, $account_id="")
	{
		$searchQuery = trim($term);

		$searchFieldName = "tc.title";
		//$searchCondition = $searchFieldName . " LIKE '%" . implode("%' OR " . $searchFieldName . " LIKE '%", $searchTerms) . "%'";
		$searchCondition = "$searchFieldName LIKE '%" . $term . "%'";
		$sql = "select tt.news_id
					from tcontent tc join tcontent_text tt on tc.content_id = tt.content_id
					where content_group_type = '12' AND content_type = 'TEXT' AND (" . $searchCondition . ")order by tc.entry_date desc LIMIT ".$offset.", ".$limit;
		$query = $this->db->query($sql)->result_array();

		// $cm         =  $this->CI->load->module('ajax');           	
		// if (!empty($query)) {
		// 	foreach ($query as $key => $row) {
  //  			$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['entry_date']));
  //  			$query[$key]['content_date']    = $contentDate;
  //  			$query[$key]['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate); 
  //  		}
		// }
		return $query;

	}

	public function searchPolitisi($term, $limit, $offset)
	{
		$searchQuery = trim($term);

     	$searchFieldName = "t1.page_name";
     	//$searchCondition = "$searchFieldName LIKE '%" . implode("%' OR $searchFieldName LIKE '%", $searchTerms) . "%'";
     	$searchCondition = "$searchFieldName LIKE '%" . $term . "%'";
		$sql = "select ta.content_id, t1.*,t1.entry_date, ta.attachment_title from tobject_page t1
				left join tusr_account t2 on t1.page_id = t2.user_id
				left join tcontent_attachment ta on t1.profile_content_id = ta.content_id 
				where (t1.user_page = 0 or t2.account_type = 1) and ($searchCondition) LIMIT ".$offset.", ".$limit;
		$query = $this->db->query($sql)->result_array();
		// $cm         = $this->CI->load->module('ajax');           	
		// if (!empty($query)) {
		// 	foreach ($query as $key => $row) {
  //  			$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['entry_date']));
  //  			$query[$key]['content_date']    = $contentDate;
  //  			$query[$key]['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate); 
  //  		}
		// }
		
		return $query;
	}

	public function searchMember($term, $limit, $offset)
	{
		$searchQuery = trim($term);

     	$searchFieldName = "display_name";
     	//$searchCondition = "$searchFieldName LIKE '%" . implode("%' OR $searchFieldName LIKE '%", $searchTerms) . "%'";
     	$searchCondition = "$searchFieldName LIKE '%" . $term . "%'";
		$sql = "SELECT ta.content_id, t1.*, tp.entry_date, ta.attachment_title FROM tusr_account t1
         		left join tobject_page tp on tp.page_id = t1.user_id
               left join tcontent_attachment ta on tp.profile_content_id = ta.content_id 
		      WHERE (t1.account_type = '0' AND t1.account_status = '1') AND ($searchCondition) LIMIT ".$offset.", ".$limit;
		$query = $this->db->query($sql)->result_array();
		$cm         = $this->CI->load->module('ajax');           	
		if (!empty($query)) {
			foreach ($query as $key => $row) {
   			$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['entry_date']));
   			$query[$key]['content_date']    = $contentDate;
   			$query[$key]['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate); 
   		}
		}
		
		return $query;
	}

	public function searchSentimen($term, $limit, $offset)
	{
		$searchQuery = trim($term);

     	$searchCondition = " ts.title  LIKE '%" . $term . "%'";
     	$searchConditionSuksesi = " race_name LIKE '%" . $term . "%'";
     	$searchConditionSurvey = " name LIKE '%" . $term . "%'";

		$sql = "SELECT tc.content_id, scandal_id id, ts.title, 'skandal' as content_group_name , updated_date, substr(ts.description, 1,100) description 
							FROM tscandal  ts
							LEFT JOIN tcontent tc on scandal_id = tc.location AND tc.group_content_id = '50' 
							WHERE main_scandal='0' AND publish='1' AND  ($searchCondition) LIMIT ".$offset.", ".$limit." 
					UNION 
				 SELECT tc.content_id, id_race id, race_name title, 'suksesi' as content_group_name, update_date  updated_date, substr(tr.description, 1,100) description 
				 			FROM trace  tr
				 			LEFT JOIN tcontent tc on id_race = tc.location AND tc.group_content_id = '56' 
				 			WHERE  publish='1' AND  ($searchConditionSuksesi) LIMIT ".$offset.", ".$limit." 
				 	UNION 
				 SELECT tc.content_id, survey_id id, name title, 'survey' as content_group_name, created_date updated_date,  substr(`desc`, 1,100) description 
				 			FROM survey  s
				 			LEFT JOIN tcontent tc ON survey_id = tc.location AND tc.content_group_type = '54' 
				 			WHERE publish='1' AND    ($searchConditionSurvey) LIMIT ".$offset.", ".$limit; 
		$query = $this->db->query($sql)->result_array();
		$cm         = $this->CI->load->module('ajax');           	
		if (!empty($query)) {
			foreach ($query as $key => $row) {
   			$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['updated_date']));
   			$query[$key]['content_date']    = $contentDate;
   			$query[$key]['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate); 
   		}
		}
		
		
		return $query;
	}


	public function searchScandal($term, $limit, $offset)
	{
		$searchQuery = trim($term);

     	$searchCondition = " ts.title  LIKE '%" . $term . "%'";
     	$searchConditionSuksesi = " race_name LIKE '%" . $term . "%'";
     	$searchConditionSurvey = " name LIKE '%" . $term . "%'";

		$sql = "SELECT scandal_id 
							FROM tscandal  ts
							LEFT JOIN tcontent tc on scandal_id = tc.location AND tc.group_content_id = '50' 
							WHERE main_scandal='0' AND publish='1' AND  ($searchCondition) LIMIT ".$offset.", ".$limit."";

		$query = $this->db->query($sql)->result_array();
		// $cm         = $this->CI->load->module('ajax');           	
		// if (!empty($query)) {
		// 	foreach ($query as $key => $row) {
  //  			$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['updated_date']));
  //  			$query[$key]['content_date']    = $contentDate;
  //  			$query[$key]['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate); 
  //  		}
		// }
		
		
		return $query;
	}
	
}

/* End of file file.php */