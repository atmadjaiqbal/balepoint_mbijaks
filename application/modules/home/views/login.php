<div class="container" style="margin-top: 31px;">
  <div class="sub-header-container">
	<div class="logo-small-container">
		<img src="<?php echo base_url('assets/images/logo.png'); ?>" >
	</div>
	<div class="right-container" style="background-color: #ffffff;">
		<div class="banner">
			<img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
		</div>
		<div class="category-news">
			<div class="category-news-title category-news-title-right category-news-title-merah">
				<h1>LOGIN &amp; REGISTER</h1>
			</div>
			
		</div>
	</div>
  </div>
  <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
  <div id='disclaimer' class="sub-header-container"></div>
  <div id="loginpage" class="sub-header-container">
<div class='aboutbijaks'>

    <div class='hot-profile'>
      <ul>
<?php
       foreach ($hotprofile as $row)
       {
          $_profile_url = base_url().'profile/detailprofile/'.$row['page_id'];
?>
          <li>
             <span data-id="<?php echo $row['page_id']; ?>">
               <img src='<?php echo $row['badge_url']; ?>' data-src='<?php echo $row['badge_url']; ?>'
                    title="<?php echo $row['page_name']; ?>" alt=""/>
             </span>
          </li>
<?php
       }
?>
      </ul>
    </div>
	
<p><strong>Bijaks.net</strong> merupakan portal Social Network (SN) politik pertama dan utama di Indonesia, yang memadukan unsur jaringan sosial, berita dan arsip terkait dunia politik Indonesia, mulai dari poltisi, partai politik maupun dinamika politik Indonesia.</p>
<p>Setiap hari, <strong>Bijaks.net</strong> menghadirkan sesuatu yang baru dan fresh, baik dari sisi pemberitaan maupun kumpulan arsip politik Indonesia, mulai dari eksekutif, legislatif, maupun yudikatif.</p>
</div>

<div class="loginarea">
  <div class="error-message">
     <span class='message_login' class="hide"><b></b></span>
  </div>
  <div id='login' class='login-section'>
    <h4>Sudah Member?</h4>
		<div class='logreg-block-body'>
			<div class='message' style='display:none'><b></b></div>
				<span id="loadingImg" class="hide"><img src="<?php echo base_url(); ?>assets/images/loading.gif"></span>
				<form id="formlogin" name="formlogin" method="POST" action="<?php echo base_url(); ?>home/loginaction">
				  <input type="text" class="inputlogin" id="email_login" name="email" size="35" autocomplete="off" tabindex="1" placeholder="Email"/>
   			  <input type="password" class="inputlogin" id="pass" name="pass" autocomplete="off"  tabindex="2" placeholder="Password"/>
 				  <input type="submit" class="loginbutton" value="Masuk" />
				</form>
		</div>					
	</div><!-- login-section -->
   <div class='forgot-section'>
     <h4>Lupa Kata Sandi?</h4>
      <div class="form-forgot">
  		   <form id="formforgot" name="formforgot" method="POST" action="<?php echo base_url(); ?>home/forgotPassword">
			   <input type="text" class="inputlogin" id="email_forgot" name="email" size="35" autocomplete="off" tabindex="3" placeholder="Email"/>
 			   <input type="submit" class="loginbutton" value="Reset Password" style="margin-top:15px;margin-left:0px !important;"/>
			</form>
      </div>
      <div class="input-captcha">
         <?php echo recaptcha();?>
      </div>        
   </div>
   <div class="register-section">
        <h4>Silakan Daftar</h4>
       <form id="signupform" name="signupform" method="POST" action="<?php echo base_url(); ?>home/emailregister">
        <div class="row-fluid">
            <div class="span5">
                <div class="control-group">
                    <div class="controls">
                        <input type="text" class="inputlogin" id="email" name="email" size="35" autocomplete="off" placeholder="Email" tabindex="4" />
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="password" class="inputlogin" id="passwd" name="passwd" autocomplete="off"  tabindex="2" placeholder="Password"/>
                    </div>
                </div>

            </div>
            <div class="span5">
                <div class="control-group">
                    <div class="controls">
                        <input type="text" class="inputlogin" id="fname" name="fname"  autocomplete="off" placeholder="Nama Awal" />
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <input type="text" class="inputlogin" id="lname" name="lname"  value="" autocomplete="off" placeholder="Nama Belakang"/>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls pull-right">
                        <input type="submit" class="loginbutton" name="inputsubmit" value="Daftar" />
                    </div>
                </div>
            </div>
        </div>
       </form>

   </div>   

</div>
<div id="loginsukses" style="display:none;">
    <div class="notice-message">
        <p class="suksesdaftar"></p>
    </div>
</div>


  </div>
</div>
</div>
<br/>


<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>

            <!-- TERKINI -->

            <?php echo $skandal_mini; ?>

            <!-- HOT ISSUE -->
            <?php //echo $issue; ?>
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>
<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>


<script>
function goToPage() {
	window.location = '<?php echo base_url(); ?>';
	}

$(document).ready(function() {

	$('#formlogin').validate({
		rules: {
			email_login: { required: true, email: true },
			pass: { required: true },
			fname: { required: true },
			lname: { required: true }
		},

		messages: {
			email_login: { required: "Please enter a valid email address", minlength: "Please enter a valid email address" },
		/*	pass: { required: "Provide a password" }, */
			fname: { required: "Please enter your first name" },
			lname: { required: "Please enter your last name" }
		},

		errorPlacement: function(error, element) {
 		   $('.message_login b').text('');
			$('.message_login b').append(error);
			//$('.message_login b').find("label").remove();
			$('.message_login').show();
			
//			error.insertBefore(element);
		},

		submitHandler: function(form) {
			//$("#formlogin").hide();
			$("#loadingImg").show('slow');
			var options = {
				method: 'POST', dataType: 'json',
				success: function(data) {
					$('.message_login b').text('');
					if(data.message != 'ok'){
							$('.message_login b').append(data.message);
							$("#loadingImg").hide();
							$("#formlogin").show('slow');
							$('.message_login').show();
               } else {
                  console.log(data.uid);
                  location.href = '<?php echo $referrer ?>';
                  /*
                  if(data.uid){
                     var uri = '<?php echo base_url().'komunitas/profile/?uid='; ?>'+data.uid+'&edit=1';
                     location.href = uri;
                  } else {
							location.href = '<?php echo $referrer ?>';
					   }
					   */
					}
					return false;
				}
			};

			$(form).ajaxSubmit(options);
			return false;
	   }
	});

   $('#formforgot').validate({
		rules: {
			email_forgot: { required: true, email: true }
         },   	
   	    messages: {
           email_forgot: {
				required: "Please enter a valid email address",
				minlength: "Please enter a valid email address"
         	}
   		},

		errorPlacement: function(error, element) {
 		   $('.message_login b').text('');
			$('.message_login b').append(error);
			$('.message_login').show();
		}  		   		
   });

	$('#signupform').validate({
		rules: {
			email: {
				required: true,
				email: true
			},
            passwd: {
				required: true,
				minlength: 6
			}
		},
		messages: {
			email: {
				required: "Please enter a valid email address",
				minlength: "Please enter a valid email address",
				remote: jQuery.format("this email is already taken.")
			},
			passwd1: {
				required: "Please enter a password",
				rangelength: jQuery.format("Enter at least {0} characters")
			},
			passwd2: {
				required: "Please enter a password",
				rangelength: jQuery.format("Enter at least {0} characters")
			}
        },
		errorPlacement: function(error, element) {
 		    $('.message_login b').text('');
			$('.message_login b').append(error);
			$('.message_login').show();

//			error.insertAfter(element);
			},

		submitHandler: function(form) {
//			$("#loader_signup").show('slow');
			$('.message_login b').text('');
            var options = {
				method: 'POST', dataType: 'json',
				success: function(data) {
				/*	if(data.code == 'captcha') {
						$('.message_login').append('Salah input kode keamanan, silahkan input kembali !');
						//Recaptcha.reload();

					} else*/ if(data.code == 'success') {
                        $('.loginarea').hide();
                        $('#loginsukses').show();
						$('.suksesdaftar').append(data.message);
						//$("#signupform").hide('slow');
						//Recaptcha.reload();

					} else {
                        $('.loginarea').hide();
                        $('#loginsukses').show();
                        $('.suksesdaftar').append(data.message);
					}
					//$("#loader_signup").hide();
					return false;
			   }
			};
			$(form).ajaxSubmit(options);
			return false;
		}
   });


//	$('#log_reg_face').height($('#log_reg_rite').height());
	});
</script>