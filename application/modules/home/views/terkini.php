<div class="block-content">
<?php
if(!empty($result) || isset($result))
{
   $_i = 1;
   foreach($result as $key => $row)
   {
      $_id = (!empty($row['id']) ? $row['id'] : 0);
      $_topic_id = (!empty($row['topic_id']) ? $row['topic_id'] : 0);
      $_slug = (!empty($row['slug']) ? $row['slug'] : '');
    
      $news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
      if(!empty($row['title']))
      {
        $short_title = (strlen ($row['title']) > 26) ? substr($row['title'], 0, 26). '...' : $row['title'];
      } else {
        $short_title = ""; 
      }

      $_title = !empty($row['title']) ? $row['title'] : '';
      $_thumnael = !empty($row['image_thumbnail']) ? $row['image_thumbnail'] : '';
      $_content_id = !empty($row['content_id']) ? $row['content_id'] : 0;

      if($_i == 1 || $_i == 2)
      {
?>
<div class="headline-index">
    <div class="headline-title clearfix">
        <h4 title="<?php echo $_title; ?>"><a href="<?php echo $news_url; ?>"><?php echo $_title; ?></a></h4>
        <span style="color:#cecece;font-size:9px;font-style:italic;"><?php echo (!empty($row['date']) ? mdate('%d %M %Y - %h:%i', strtotime($row['date'])) : ''); ?></span>
    </div>
    <div class="headline-detail clearfix">
        <div class='post-pic' style="margin-top:-5px;">
            <a href="<?php echo $news_url; ?>"><img class="lazy" data-original="<?php echo $_thumnael; ?>" alt="<?php echo $_title; ?>" style="width: 300px; height: 160px;"/></a>
            <div class="score-place score-place-overlay score" data-id="<?php echo $_content_id; ?>" ></div>
        </div>
        <div class="related-politicians clearfix" style="background-color: #DD8E6C;">
            <span class="pull-left related-politician-label">Politisi Terkait :</span>
            <?php if(!empty($result['news_profile'])) { ?>
            <?php if (count($result['news_profile']) > 0){ ?>
                <?php foreach ($result['news_profile'] as $key => $pro) { ?>
                    <?php if($key === 4) break; ?>
                    <span class="pull-left related-politician-img">
                        <img title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                    </span>
                <?php } ?>
                <?php if (count($result['news_profile']) > 4){ ?>
                    <div class="span2">
                        <span>More</span>
                    </div>
                <?php } ?>
            <?php }else{ ?>
                <span class="pull-left related-politician-img">Tidak ada politisi terkait</span>
            <?php } ?>
            <?php } ?>
        </div>
    </div>


<!--    <div class="time-line-content" data-cat="terkini" data-uri="<?php echo $_content_id; ?>"></div> -->
</div>
<?php

   if($_i == 2)
   {
       echo '<div style="margin-top:10px;border-bottom: 2px solid #cecece;margin-bottom: 5px;"></div>';
   }

    } else {
?>
    <div class="subheadline">
       <div class="img-sec">
          <img src="<?php echo $_thumnael; ?>" style="width:100px;height: 80px;">
       </div>
       <div class="title-sec">
         <p><a title="<?php echo $_title; ?>" href="<?php echo $news_url; ?>"><?php echo $_title;?></a></p>
         <small style="font-style:italic;"><?php echo (!empty($row['date']) ? mdate('%d %M %Y - %h:%i', strtotime($row['date'])) : ''); ?></small>
         <div class="score-place score" data-tipe="1" data-id="<?php echo $_content_id;?>" style="margin-top:-5px;"></div>
       </div>
    </div>
<?php
      }
      $_i++;
   }
}
?>
</div>
