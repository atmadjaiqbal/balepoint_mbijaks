<div class="row-fluid">
<?php
  foreach($suksesi as $key => $val)
  {
?>
     <div class="span6">
        <?php $dt['val'] = $val; ?>
        <?php $this->load->view('template/suksesi/tpl_suksesi_home_index', $dt); ?>
     </div>
<?php
  }
?>
</div>

<div class="row" id="home-suksesi" style="margin-top:20px;">

  <div id="suksesiCarousel" class="slide">
    <div class="carousel-inner">
<?php
if(!empty($moresuksesi))
{
    $y = 0;
    foreach($moresuksesi as $key => $val)
    {
        foreach($val['status'] as $idx => $status){
            if(intval($status['draft']) == 1){
                if(intval($status['status']) == 1)
                {
                    $status_suksesi = 'Survey / Prediksi';
                } else {
                    if(intval($status['status']) == 2)
                    {
                        $status_suksesi = 'Putaran Pertama';
                    } else {
                        $status_suksesi = 'Putaran Kedua';
                    }
                }
                $index_trace_status = $idx;
            }
        }

        $suksesi_url = base_url() . 'suksesi/index/'.$val['id_race'].'-'.urltitle($val['race_name']);
        $status_pilkada = (!empty($val['status_akhir_suksesi']) && $val['status_akhir_suksesi'] <> 'None' ? 'Status : '.$val['status_akhir_suksesi'] : '');

        $map_photo = '';
        $logo_photo = '';
        foreach($val['photos'] as $foto){
            if(intval($foto['type']) === 2){
                $map_photo = $foto['thumb_url'];
            }elseif(intval($foto['type']) === 1){
                $logo_photo = $foto['thumb_url'];
            }
        }

        $suksesi_item = '<a href="'.$suksesi_url.'">
                           <div class="content-logo">
                             <div><img class="lazy" src="'.$logo_photo.'"  style="width: 70px; height: 70px;"></div>
                             <div class="content-text">'.$val['race_name'].'</div>
                             <div class="content-text">'.date('d F Y',strtotime($val['tgl_pelaksanaan'])).'</div>
                             <div class="content-text"><strong><p style="font-size:10px;">'.$status_pilkada.'</p></strong></div>
                           </div>
                          </a>';
        if($val['id_race'] != '274' && $val['id_race'] != '275')
        {
          if($y == 1) $suksesi_img1 = $suksesi_item; if($y == 2) $suksesi_img2 = $suksesi_item;
          if($y == 3) $suksesi_img3 = $suksesi_item; if($y == 4) $suksesi_img4 = $suksesi_item;
          if($y == 5) $suksesi_img5 = $suksesi_item; if($y == 6) $suksesi_img6 = $suksesi_item;
          if($y == 7) $suksesi_img7 = $suksesi_item; if($y == 8) $suksesi_img8 = $suksesi_item;
          if($y == 9) $suksesi_img9 = $suksesi_item; if($y == 10) $suksesi_img10 = $suksesi_item;
          if($y == 11) $suksesi_img11 = $suksesi_item; if($y == 12) $suksesi_img12 = $suksesi_item;
          if($y == 13) $suksesi_img13 = $suksesi_item; if($y == 14) $suksesi_img14 = $suksesi_item;
          if($y == 15) $suksesi_img15 = $suksesi_item; if($y == 16) $suksesi_img16 = $suksesi_item;
          if($y == 17) $suksesi_img17 = $suksesi_item; if($y == 18) $suksesi_img18 = $suksesi_item;
          if($y == 19) $suksesi_img19 = $suksesi_item; if($y == 20) $suksesi_img20 = $suksesi_item;
          if($y == 21) $suksesi_img21 = $suksesi_item; if($y == 22) $suksesi_img22 = $suksesi_item;
          if($y == 23) $suksesi_img23 = $suksesi_item; if($y == 24) $suksesi_img24 = $suksesi_item;
          if($y == 25) $suksesi_img25 = $suksesi_item; if($y == 26) $suksesi_img26 = $suksesi_item;
          if($y == 27) $suksesi_img27 = $suksesi_item; if($y == 28) $suksesi_img28 = $suksesi_item;
          if($y == 29) $suksesi_img29 = $suksesi_item; if($y == 30) $suksesi_img30 = $suksesi_item;

          $y++;
        }
    }
?>
    <div class="item active">
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img1; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img2; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img3; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img4; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img5; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img6; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img7; ?></div>
        </div>
    </div>
    <div class="item">
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img8; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img9; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img10; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img11; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img12; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img13; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img14; ?></div>
        </div>
    </div>
    <div class="item">
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img15; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img16; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img17; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img18; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img19; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img20; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img21; ?></div>
        </div>
    </div>
    <div class="item">
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img22; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img23; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img24; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img25; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img26; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img27; ?></div>
        </div>
        <div style="float:left;margin-left:5px;">
            <div class="img-logo"><?php echo $suksesi_img28; ?></div>
        </div>
    </div>

<?php
}
?>
    </div>
      <a class="left carousel-control" href="#suksesiCarousel" data-slide="prev">‹</a>
      <a class="right carousel-control" href="#suksesiCarousel" data-slide="next">›</a>
  </div>
</div>
