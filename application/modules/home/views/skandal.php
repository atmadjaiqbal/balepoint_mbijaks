<div class="headline">

<?php
if(!empty($val))
{
  $skandal_url = base_url() . 'scandal/index/'.$val['scandal_id'].'-'.urltitle($val['title']);
  $skandal_photos = (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
  $css_image = 'news-image-skandal';
  $limit_terkait = 7;
  if(isset($val['tipe'])){
    $limit_terkait = 2;
    $css_image .= '-'.$val['tipe'];
    $skandal_photos = (count($val['photos']) > 0) ? $val['photos'][0]['thumb_landscape_url'] : '';
  }

    $headx_photos = (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
    $headm_photos = (count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';

    foreach($val['photos'] as $kes => $item){
        if(intval($item['type']) == 1){
            $headx_photos = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                $headx_photos = $item['original_url'];
            }

            $headm_photos = $item['thumb_url'];
            break;
        }
    }

?>


<!-- SCANDAL -->

    <div class="headline-title">
        <h4><a href="<?php echo $skandal_url; ?>"><?php echo $val['title']; // (strlen($val['title']) > 25) ? substr($val['title'], 0, 25).'...' : $val['title']; ?></a></h4>
    </div>
    <div class="headline-detail clearfix">

      <div id="myCarousel" data-interval="false" data-pause="hover" class="carousel-head slide post-pic">

        <div class="carousel-inner">
            <div class="active item photo-carousel">
                <img src="<?=$headx_photos;?>">
            </div>

        <?php $k=1; $max = 6; foreach($val['photos'] as $kes => $item){ ?>
          <?php
            if($k >= $max) break;

            if(isset($item['type']))
            {
                if($item['type'] == 1)
                {
                    $max += 1;
                    continue;
                    $_x_active = 'active';
                } else {
                    if($k == 0)
                    {
                        $_x_active = 'active';
                    } else {
                        $_x_active = '';
                    }
                }
            } else {
                //if($k == 1) { $_x_active = 'active'; } else { $_x_active = ''; }
            }
          ?>
          <div class="<?php echo $_x_active; ?> item photo-carousel">
          <?php
               $img_uri_car = $item['large_url'];
               if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){

                   $img_uri_car = $item['original_url'];
               }
            ?>
            <img src="<?=$img_uri_car;?>">
          </div>
        <?php $k++; } ?>

        </div>
        <div class="score-place score-place-overlay score" data-id="<?php echo $val['content_id']; ?>" ></div>

        <div class="photo-control-container">
<!--          <div class="photo-button">
             <a class="pull-left left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
          </div>-->
          <div class="photo-carousel-control">
                  <!-- Carousel thumb -->
            <ol class="photo-carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active" style="margin-right:0px;">
                    <img src="<?=$headm_photos;?>">
                </li>
              <?php $i=1; $maxm = 6; foreach($val['photos'] as $kem => $item){ ?>
                  <?php
                  if($i >= 6) break;
                  if(isset($item['type']))
                  {
                      if($item['type'] == 1)
                      {
                          $maxm += 1;
                          continue;
                          $set_active = 'active';
                      } else {
                          $set_active = '';
                      }
                  } else {
                      if($i == 1)
                      {
                          //$set_active = 'active';
                      } else {
                          $set_active = '';
                      }
                  }
                  ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $set_active; ?>" style="margin-right:0px;">
                  <img src="<?php echo $item['thumb_url'] ;?>">
                </li>

              <?php $i++; } ?>

              <?php
              if($i < 6 || $i < 5 || $i < 4 || $i < 3 || $i < 2)
              {
                  for ($o = 1; $o <= (6 - $i); $o++) {
              ?>
                 <li style="margin-right:0px;">
                   <div style="width: 50px;height: 46px;border: solid 1px #ffffff;border-right: none;"></div>
                 </li>
              <?php

                  }
              }
              ?>

            </ol>
          </div>
<!--          <div class="photo-button">
            <a class="pull-right right" href="#myCarousel" data-slide="next">&rsaquo;</a>
          </div> -->
        </div>
        <div class="row-fluid">
            <div class=""> 
              <span><strong>&nbsp;Politisi Terkait</strong></span>
              <?php if (count($val['players']) > 7){ ?>
                      <a class="pull-right" href="<?php echo $skandal_url; ?>">More &nbsp;</a>    
                    <?php } ?>
            </div>
            
            <div class="">
                <?php
                if (count($val['players']) > 0)
                {
                    echo '<ul class="ul-img-hr">';
                    foreach ($val['players'] as $key => $pro)
                    {
                        if($key === $limit_terkait) break;
                        ?>
                        <li class="">
                            <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="" src="<?php echo $pro['profile_icon_url']; ?>">
                            </a>
                        </li>
                        <?php
                    }
                    echo '</ul>';
                } else{
                    ?>
                    <span>Tidak ada politisi terkait</span>
                    <?php
                }
                ?>
            </div>
        </div>
        <hr class="hr-white">
        <div class="row-fluid">
            <div class="span2 span2-pd-left">
                <span><strong>Status</strong></span>
            </div>
            <div class="span10 span10-pd-right">
                <span class=""><?php echo ucwords($val['status']); ?></span>
            </div>
        </div>
        <hr class="hr-white">
        <div class="row-fluid">
            <div class="span2 span2-pd-left">
                <span><strong>Kejadian</strong></span>
            </div>
            <div class="span10 span10-pd-right">
                <span class=""><?php echo mdate('%d %M %Y', strtotime($val['date'])); ?></span>
            </div>
        </div>
        <hr class="hr-white">
        <div class="row-fluid">
          <div class="span2 span2-pd-left">
              <span class=""><strong>Dampak</strong></span>
          </div>
          <div class="span10 span10-pd-right">
              <span class=""><?php echo (strlen($val['uang']) > 100 ? substr($val['uang'], 0, 100) .'...' : $val['uang']); ?></span>
          </div>
        </div>
      </div>

    </div>
<?php
}
?>
</div>



<div class="powermapskandal">
  <img src="<?php echo base_url();?>assets/images/power-map-300.gif" style="width:330px;height:411px;">

<!--
    <div data-id="<?php ?>" class='home-skandal-detail-map'>
         POWERMAP 
        <?php //echo $row['pw_map']; ?>
    </div>
-->    
     <div class="time-line-content" data-cat="scandal" data-uri="<?php echo (isset($val['content_id']) ? $val['content_id'] : ''); ?>"></div>
</div>


<div class="headline-index" style="float:left;margin-top:10px;">
    <div class="title">
        <span class="index-title" style="float: left;">SKANDAL LAINNYA</span>
        <div style="float: right;">
            <a id="scandal_caro_left" class="left carousel-control" href="#scandalCarousel" data-slide="prev" style="font-size:45px;position: relative;float: left;width: 25px;height: 25px;">‹</a>
            <a id="scandal_caro_right" class="right carousel-control" href="#scandalCarousel" data-slide="next" style="margin-left:40px;margin-right:-10px;font-size:45px;position: relative;float: right;width: 25px;height: 25px;">›</a>
        </div>
    </div>
    <div id="scandalCarousel" class="lide">
        <div class="carousel-inner">
            <?php
            if (!empty($scandal_more))
            {
                $y = 1;
                $carousel_img1 = '';
                $carousel_img2 = '';
                $carousel_img3 = '';
                $carousel_img4 = '';
                $carousel_img5 = '';
                $carousel_img6 = '';
                $carousel_img7 = '';
                $carousel_img8 = '';
                $carousel_img9 = '';
                foreach($scandal_more as $i =>  $row)
                {
                    $morePhotos = '';
                    $moreURL = base_url() . 'scandal/index/'.$row['scandal_id'].'-'.urltitle($row['title']);
                    if(count($row['photos']) > 0)
                    {
                        foreach($row['photos'] as $pot => $pitem)
                        {
                            if(isset($pitem['type']))
                            {
                              if($pitem['type'] == '1')
                              {
                                $morePhotos = $pitem['large_url'];
                                if( file_exists('public/upload/image/skandal/'.$pitem['attachment_title'])){
                                    $morePhotos = $pitem['original_url'];
                                }
                              }
                            }
                        }

                        if(empty($morePhotos))
                        {
                            $morePhotos = $row['photos'][0]['large_url'];
                            if( file_exists('public/upload/image/skandal/'.$row['photos'][0]['attachment_title'])){
                                $morePhotos = $row['photos'][0]['original_url'];
                            }
                        }
                    } else {
                        $morePhotos = base_url(). 'assets/images/thumb/noimage.jpg';
                    }

                    $contentID = $row['content_id'];
                    $carousel_item = '<a href="'.$moreURL.'">
                            <img class="tooltip-bottom" title="'.$row['title'].'" src="'.$morePhotos.'"  style="width: 200px; height: 150px;">
                            <div class="score-place score-place-overlay score" data-id="'.$contentID.'" ></div>
                            <div class="scandal-overlay">
                                <p class="overlay-text"><strong>'.substr($row['title'],0 ,20).' ...</strong></p>
                            </div>
                          </a>';

                    if($y == 1) $carousel_img1 = $carousel_item;
                    if($y == 2) $carousel_img2 = $carousel_item;
                    if($y == 3) $carousel_img3 = $carousel_item;
                    if($y == 4) $carousel_img4 = $carousel_item;
                    if($y == 5) $carousel_img5 = $carousel_item;
                    if($y == 6) $carousel_img6 = $carousel_item;
                    if($y == 7) $carousel_img7 = $carousel_item;
                    if($y == 8) $carousel_img8 = $carousel_item;
                    if($y == 9) $carousel_img9 = $carousel_item;

                    $y++;
                }
                ?>
                <div class="item active">
                    <div style="float:left;margin-right: 14px;">
                        <div class="news-img-item"><?php echo $carousel_img1; ?></div>
                    </div>
                    <div style="float:left;margin-right: 14px;">
                        <div class="news-img-item"><?php echo $carousel_img2; ?></div>
                    </div>
                    <div style="float:left;">
                        <div class="news-img-item"><?php echo $carousel_img3; ?></div>
                    </div>
                </div>
                <div class="item">
                    <div style="float:left;margin-right: 14px;">
                        <div class="news-img-item"><?php echo $carousel_img4; ?></div>
                    </div>
                    <div style="float:left;margin-right: 14px;">
                        <div class="news-img-item"><?php echo $carousel_img5; ?></div>
                    </div>
                    <div style="float:left;">
                        <div class="news-img-item"><?php echo $carousel_img6; ?></div>
                    </div>
                </div>
                <div class="item">
                    <div style="float:left;margin-right: 14px;">
                        <div class="news-img-item"><?php echo $carousel_img7; ?></div>
                    </div>
                    <div style="float:left;margin-right: 14px;">
                        <div class="news-img-item"><?php echo $carousel_img8; ?></div>
                    </div>
                    <div style="float:left;">
                        <div class="news-img-item"><?php echo $carousel_img9; ?></div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function() {
        /*
         $('.carousel-control').live('click', function(e) {
         $(this).siblings('.carousel-inner').css( "overflow", "hidden" );
         });
         */
        var imgCount = 2;
        $('#scandalCarousel').carousel({
            interval:false
        });

        $('#scandalCarousel').bind('slide',function(en){
            console.debug(en);
            if(en.direction == 'left'){
                $('#scandal_caro_left').data('slide', 'prev')
                imgCount--;
                if(imgCount == 0){
                    $('#scandal_caro_right').data('slide', '');
                    $('#scandal_caro_left').data('slide', 'prev');
                }
            }else{
                $('#scandal_caro_right').data('slide', 'next');
                imgCount++;
                if(imgCount == 2){
                    $('#scandal_caro_left').data('slide', '');
                    $('#scandal_caro_right').data('slide', 'next');
                }

            }

        });

        $('.carousel-head').carousel({interval:false});


        var uri = "<?php echo base_url();?>powermap/skandal/";
        var width = '285';
        var height = '200';
        var el = $('.home-skandal-detail-map');

        var id = $(el[0]).data('id');
        var id2 = $(el[1]).data('id');

        /*$.ajax({
         type:'post',
         url:uri+id,
         data:{'w': width , 'h': height, 't' : '2'},
         async : false,
         success: function(dt){
         $(el[0]).append(dt);
         }
         });

         $.ajax({
         type:'post',
         url:uri+id2,
         data:{'w': width , 'h': height, 't' : '2'},
         async : false,
         success: function(dt){
         $(el[1]).append(dt);
         }
         });*/
    });
</script>

