<?php
$_name_acc = '';
$i = 0;
if (!empty($comments))
{
	foreach($comments as $row) {
		$date = date('d M Y', strtotime(str_replace('-', '/', $row['entry_date'])));
		if ($row['account_type'] === '1') {
			$url = base_url() . "politisi/index/" . $row['page_id'] ;
			$prefix_img = "politisi/";
		} else {
			$url = base_url() . "komunitas/profile/".$row['page_id'];
			$prefix_img = "user/";
		}

        if($row['page_name'] <> $_name_acc)
        {
            $_name_acc = $row['page_name'];
            if($i >= 30) break;

?>
<?php
if($i == 8 || $i == 23) { $_margin = 'margin-left:1px;margin-top:-2px'; } else { $_margin = ''; }

if($i == 0 || $i == 8 || $i == 15 || $i == 23)
{
?>
<div style="float:left;<?php echo $_margin; ?>">
<?php
}
?>


<div class="blog-section">
<?php
$comment_on ='';
if ($row['content_group_name'] == 'STATUS') {
    $comment_on_url = $url;
    $comment_on = "Status ". $row['page_name'];

} else if ($row['content_group_name'] == 'BLOG') {
    $comment_on_url 	= base_url() . "komunitas/profile/".$row['page_id'];
    $comment_on 		= "Blog : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";

} else if ($row['content_group_name'] == 'NEWS') {
    $comment_on_url 	= base_url().'news/article/0-'.$row['news_id'].'/'.$row['content_url'];
    $comment_on 		= "Berita : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";

} else if ($row['content_group_name'] == 'SCANDAL') {
    $comment_on_url 	= base_url().'scandal/index/'.$row['location'].'/'.  urltitle($row['title']) ;
    $comment_on 		= "Skandal : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";

} else if ($row['content_group_name'] == 'RACE') {
    $comment_on_url 	= base_url().'suksesi/index/'.$row['location'].'/'.  urltitle($row['title']) ;
    $comment_on 		= "Suksesi : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";

} else if ($row['content_group_name'] == 'SURVEY') {
    $comment_on_url 	= base_url().'survey/index/'.$row['location'].'/'.  urltitle($row['title']) ;
    $comment_on 		= "Suksesi : <a href='".$comment_on_url."'>". word_limiter($row['title'], 3) ."</a>";
}

if($i == 0)
{
?>
      <div class="blog-user-comment">
        <div class="blog-user-foto">
           <a href="<?php echo $url;?>">
              <img alt="<?php echo $row['page_name']; ?>" title="<?php echo $row['page_name']; ?>"
                   src='<?php echo badge_url($row['attachment_title'],'user/'.$row['user_id']) ; ?>'
                   data-src="<?php echo badge_url($row['attachment_title'],'user/'.$row['user_id']) ; ?>"
                   data-toggle='tooltip' class='tooltip-bottom' />
           </a>
        </div>

        <div class="blog-user-name">
            <span class="pull-right comment-date" style="margin-top:0px;"><?php echo time_passed($row['entry_date']);?></span>
            <a href="<?php echo $url;?>" title="<?php echo $row['page_name']; ?>">
               <?php echo substr(strtoupper($row['page_name']),0, 18); ?>
               <?php if (strlen($row['page_name']) > 19) { echo '...';} ?>
            </a>
           <p><a href="<?php echo $url;?>" title="<?php echo $row['text_comment']; ?>"><strong>
              <?php echo substr($row['text_comment'],0, 28); ?>
              <?php if (strlen($row['text_comment']) > 29) { echo '...';} ?>
           </strong></a></p>
           <div class="comment-on">di <?php echo $comment_on;?></div>

        </div>
      </div>
<?php
} else {

    if($i == 1) $_backcolor = 'style="background-color:#F7D788;"';
    if($i == 2) $_backcolor = 'style="background-color:#FCE9BA;"';
    if($i >=3) $_backcolor = '';

?>
      <div class="blog-list" <?php echo $_backcolor; ?>>
        <div class="blog-list-foto">
           <a href="<?php echo $url;?>">
               <img alt="<?php echo $row['page_name']; ?>" title="<?php echo $row['page_name']; ?>"
                    src='<?php echo badge_url($row['attachment_title'],'user/'.$row['user_id']) ; ?>'
                    data-src="<?php echo badge_url($row['attachment_title'],'user/'.$row['user_id']) ; ?>"
                    data-toggle='tooltip' class='tooltip-bottom' />
           </a>
        </div>
        <div class="blog-list-name">
           <span class="pull-right comment-date" style="margin-top:-10px;"><?php echo time_passed($row['entry_date']);?></span>
           <a href="<?php echo $url;?>" title="<?php echo $row['page_name']; ?>">
              <?php echo substr(strtoupper($row['page_name']),0, 18); ?>
              <?php if (strlen($row['page_name']) > 19) { echo '...';} ?>
           </a>
           <p><a href="<?php echo $url;?>" title="<?php echo $row['text_comment']; ?>"><strong>
              <?php echo substr($row['text_comment'],0, 35); ?>
              <?php if (strlen($row['text_comment']) > 35) { echo '...';} ?>
           </strong></a></p>
           <div class="comment-on">di <?php echo $comment_on;?></div>
        </div>

      </div>
<?php
}
?>
</div>

<?php
if($i == 14)
{
   echo '
      <div class="text-right" style="height:60px;margin-right:0px;margin-top:5px;">
         <a data-tipe="1" id="komentar_news_next" class="btn btn-mini" >15 Berikutnya</a>
      </div>
   ';
}

if($i == 29)
{
   echo '
      <div class="text-right" style="height:60px;margin-right:0px;margin-top:5px;">
         <a data-tipe="0" id="komentar_news_prev" class="btn btn-mini" >15 Sebelumnya</a>
      </div>
   ';
}


?>



<?php if($i == 7 || $i == 14 || $i == 22 || $i == 29) echo '</div>'; ?>
<?php
    $i++;
    }
  }
}
?>

