<?php

$i = 0;
$page_name = '';
$content_callback 	= base_url('ajax/count_content');
$content_like_uri 	= base_url('ajax/like');
$content_dislike_uri = base_url('ajax/dislike');

if (!empty($blogs))
{
   foreach($blogs as $row)
   {
      $content_id = $row['content_id'];
	  $content_date	= mysql_date("%d %M %Y - %h:%i", strtotime($row['entry_date']));
      $_blog_title = strip_tags($row['title']);
      $_blog_title = str_replace('*', '', $_blog_title);
      $_blog_title = str_replace(':','', $_blog_title);
      $_blog_title = str_replace('/','', $_blog_title);
      $_blog_title = str_replace('"','', $_blog_title);
      $_blog_title = str_replace("'","", $_blog_title);
      $_blog_title = str_replace(',','', $_blog_title);
      $_blog_title = str_replace('.','', $_blog_title);
      $_blog_title = str_replace('?','', $_blog_title);
      $_blog_title = str_replace('$','', $_blog_title);
      $_blog_title = str_replace('!','', $_blog_title);
      $_blog_title = str_replace('&','', $_blog_title);
      $_blog_title = str_replace('%','', $_blog_title);
      $_blog_title = str_replace('#','', $_blog_title);
      $_blog_title = str_replace('@','', $_blog_title);
      $_blog_title = str_replace(' ','_', $_blog_title);

      $url = base_url() . "komunitas/blog/".$row['page_id']."/".$row['content_id']."/".$_blog_title;
   	  if ($row['user_page'] == '0')
      {
   	    $url = base_url() . "aktor/profile/" . $row['page_id'] . "/" . $row['content_id'];
   	  }

      if(!empty($account_id))
      {
		$content_like_uri 	= ($row['is_like'] == '0') ? base_url('ajax/like') : base_url('ajax/unlike') ;
		$content_dislike_uri = ($row['is_dislike'] == '0') ? base_url('ajax/dislike') : base_url('ajax/undislike');
	  }

	  $content_data	= array(
			'content_id' 		=> $content_id,
			'content_date' 	=> $content_date,
			'count_comment' 	=> $row['count_comment'],
			'count_like' 		=> $row['count_like'],
			'count_dislike' 	=> $row['count_dislike'],
			'is_like' 			=> $row['is_like'],
			'is_dislike' 		=> $row['is_dislike'],
			'tipe' 				=> '2',
			'restype' 			=> '2',
			'callback'			=> $content_callback,
			'like_uri'			=> $content_like_uri,
			'dislike_uri'		=> $content_dislike_uri
	  );

      if($row['page_name'] <> $page_name)
      {
          $page_name = $row['page_name'];
          if($i == 27) break;

?>
<?php
      if($i == 5 || $i == 20) { $_margin = 'margin-left:1px;'; } else { $_margin = ''; }

      if($i == 0 || $i == 5 || $i == 12 || $i == 20)
      {
?>
        <div style="float:left;<?php echo $_margin; ?>">
<?php
      }
?>
<div class="blog-section">
<?php

if($i == 0)
{
?>
  <div class="blog-user-<?php echo $section; ?>">
     <div class="blog-user-foto">
       <a href="<?php echo base_url() . "komunitas/profile/".$row['page_id'];?>">
         <img alt="<?php echo $row['page_name']; ?>" title="<?php echo $row['page_name']; ?>"
              src='<?php echo badge_url($row['page_attachment'],'user/'.$row['page_id']) ; ?>'
              data-src="<?php echo badge_url($row['page_attachment'],'user/'.$row['page_id']) ; ?>"
              data-toggle='tooltip' class='tooltip-bottom' />
       </a>
     </div>
     <div class="blog-user-name">
         <a href="<?php echo base_url() . "komunitas/blog/".$row['page_id']."/".$row['content_id']."/".$_blog_title;?>" title="<?php echo $row['title']; ?>">
             <?php echo substr(strtoupper($row['page_name']),0, 18); ?>
             <?php if (strlen($row['page_name']) > 19) { echo '...';} ?>
         </a>
         <p><a href="<?php echo $url;?>" title="<?php echo $row['title']; ?>"><strong>
             <?php echo substr($row['title'],0, 32); ?>
             <?php if (strlen($row['title']) > 33) { echo '...';} ?>
         </strong></a></p>
     </div>
  </div>

  <div class="blog-content">
      <?php //echo $this->load->view('ajax/count_content_action', $content_data);?>
      <a href="<?php echo $url;?>">
          <img class="lazy" data-original="<?php echo (isset($row['image'])) ? $row['image'] : base_url() .'/assets/images/ico-blog.png'; ?>" alt="<?php echo $row['title']; ?>">
      </a>
      <div class="score-place score-place-overlay score" data-id="<?php echo $row['content_id']; ?>" ></div>
  </div>
<?php
} else {

    if($i == 1) $_backcolor = 'style="background-color:#F2C18C;"';
    if($i == 2) $_backcolor = 'style="background-color:#E8CEB2;"';
    if($i >= 3) $_backcolor = '';

?>
 <div class="blog-list" <?php echo $_backcolor; ?>>
    <div class="blog-list-foto">
        <a href="<?php echo base_url() . "komunitas/profile/".$row['page_id'];?>">
            <img alt="<?php echo $row['page_name']; ?>" title="<?php echo $row['page_name']; ?>"
                 src='<?php echo badge_url($row['page_attachment'],'user/'.$row['page_id']) ; ?>'
                 data-src="<?php echo badge_url($row['page_attachment'],'user/'.$row['page_id']) ; ?>"
                 data-toggle='tooltip' class='tooltip-bottom' />
        </a>
    </div>
     <div class="blog-list-name">
         <a href="<?php echo base_url() . "komunitas/blog/".$row['page_id']."/".$row['content_id']."/".$_blog_title;?>" title="<?php echo $row['title']; ?>">
            <?php echo substr(strtoupper($row['page_name']),0, 18); ?>
            <?php if (strlen($row['page_name']) > 19) { echo '...';} ?>
         </a>
         <p><a href="<?php echo $url;?>" title="<?php echo $row['title']; ?>"><strong>
            <?php echo substr($row['title'],0, 68); ?>
            <?php if (strlen($row['title']) > 69) { echo '...';} ?>
         </strong></a></p>
     </div>

 </div>
<?php

}
?>
</div>
<?php
 if($i == 11)
 {
   echo '
            <div class="text-right" style="height:60px;margin-right:0px;margin-top:5px;">
              <a id="opini_news_next" class="btn btn-mini">15 Berikutnya</a>
            </div>
   ';
 }

if($i == 26)
{
   echo '
            <div class="text-right" style="height:60px;margin-right:0px;margin-top:5px;">
              <a id="opini_news_prev" class="btn btn-mini" >12 Sebelumnya</a>
            </div>
   ';
}


?>

<?php if($i == 4 || $i == 11 || $i == 19 || $i == 26) echo '</div>'; ?>

<?php
    $i++;
    }
  }
}
?>