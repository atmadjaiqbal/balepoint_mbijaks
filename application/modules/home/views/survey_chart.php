<?php
if(! isset($width)) { $width = 300; }
if(! isset($height)) { $height = 200; }

if (isset($show_title)) {
    if (!$show_title) $title='';
}

?>
<script src="<?php echo base_url(); ?>assets/js/highcharts.js"></script>
<script type="text/javascript">
    $(function () {
        var chart;
        $(document).ready(function() {
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'container<?php echo $question_id; ?>',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    backgroundColor:'rgba(255, 255, 255, 0)'
                },
                title: {
                    text: "<?php echo $title; ?>",
                    style: {
                        color: '#a00',
                        fontSize: '11px',
                        fontFamily: 'Arvo, serif'
                    }, y: 10
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
                    percentageDecimals: 1
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            distance : 10,
                            formatter: function() {
                                return '<b>'+ this.point.y +'%</b>';
                            }
                        },
                        showInLegend: <?php echo (isset($show_legend)) ? $show_legend: 'true';?>
                    }
                },
                
                series: [{
                    type: '<?php echo (isset($chart_type)) ? $chart_type: 'pie';?>',
                    name: '<?php echo $title; ?>',
                    data: [
                        <?php
                             if (!empty($legends)) {
                                 $total = 0;
                                 foreach ($values as $value) {
                                     $total += (int)$value;
                                 }

                                 foreach ($legends as $key => $legend) {
                                     if($key > 0) { echo ","; }

                                     $persen             = ($values[$key] > 0) ? round($values[$key] / $total, 2) * 100 : 0;
                                     $optionID       = 0;
                                     $optionValue    = '';
                                     if (isset($option_id))      {$optionID = $option_id[$key];}
                                     if (isset($option_value))   {$optionValue = $option_value[$key];}
                                     echo "{ name: '" . addslashes($legend) . "', y: " . $persen . ", id: '".$optionID."'}";
                                     // echo "{ name: '" . addslashes($legend) . "', y: " . $persen . ", id: '".$optionID."', value: '".$optionValue."'}";
                                     // echo "{ name: '" . addslashes($legend) . "', y: " . $values[$key] . ", id: '".$optionID."', value: '".$optionValue."'}";
                                 }
                             }

                             ?>
                    ]
                }]
            });
        });

    });
</script>
<?php //var_dump($values); ?>
<div id="container<?php echo $question_id; ?>" style="height: <?php echo $height; ?>px; margin: 0 auto"></div>
