<?php

foreach($headline as $row){
    $segment_title = '';
    $short_title = '';
    $_color = "#9E5B3E;";
    $url = '';
    $image = '';
    $content_date = '';
    $hover_title = '';

    if ($row['content_type'] == 'TEXT')  {
        $segment_title = 'BERITA';
        $short_title = $row['result_row']['title']; // (strlen($row['result_row']['title']) > 45 ? substr($row['result_row']['title'], 0, 45) .' ...' : $row['result_row']['title']);   // "NEWS";
        $_color = "news";
        $url = base_url() . "news/article/0-" . $row['result_row']['id'] . "/" . $row['result_row']['slug'];
        $image	= $row['result_row']['image_medium'];
        $content_date 	= date('d M Y - H:i', strtotime($row['result_row']['date']));
        $hover_title = $row['result_row']['title'];
    }else if ($row['content_type'] == 'HOT_PROFILE'){
        $segment_title = 'PROFILE';
        $short_title = $row['result_row']['page_name'];
        $_color = "profile";
        $url = base_url('aktor/profile/'.$row['result_row']['page_id']);
        $image  = base_url().'public/img/no-image-politisi.png';
        if(count($row['result_row']['headline_photo']) > 0){
            $image  = ($row['result_row']['headline_photo'][0]['headline_url'] != 'None' ? $row['result_row']['headline_photo'][0]['headline_url'] : base_url().'public/img/no-image-politisi.png' );    
        }
        
        $content_date 	= $row['result_row']['partai_name'];
        $hover_title = $row['result_row']['page_name'];
    }else if ($row['content_type'] == 'SKANDAL'){
        $segment_title = 'SKANDAL';
        $short_title = $row['result_row']['title']; // (strlen($row['result_row']['title']) > 45 ? substr($row['result_row']['title'], 0, 45) .' ...' : $row['result_row']['title']);
        $hover_title = $row['result_row']['title'];
        $_color = "scandal";
        $url = base_url() . 'scandal/index/'.$row['result_row']['scandal_id'].'-'.$row['result_row']['title'];
        $idx_img = count($row['scandal_photo']) - 1;
        $image = (!empty($row['scandal_photo'][0]['large_url']) ? $row['scandal_photo'][0]['large_url'] : base_url().'public/img/no-image-politisi.png');
        $content_date 	= date('d M Y', strtotime($row['result_row']['date']));

    }else if ($row['content_type'] == 'SUKSESI'){
        $segment_title = 'SUKSESI';
        $short_title 	= $row['result_row']['race_name']; // (strlen($row['result_row']['race_name']) > 45 ? substr($row['result_row']['race_name'], 0, 45) .' ...' : $row['result_row']['race_name']);
        $hover_title = $row['result_row']['race_name'];
        $_color = "suksesi";
        $url 		= base_url('suksesi/index/'.$row['result_row']['id_race']. '-' . urltitle($row['result_row']['race_name']) . '/');
        $image = base_url().'public/img/large/noimage.jpg';
        if(count($row['result_row']['photos']) > 0){
            $image = (!empty($row['result_row']['photos'][0]['large_url']) ? $row['result_row']['photos'][0]['large_url'] : base_url().'public/img/large/noimage.jpg');
        }
        $content_date 	= date('d M Y', strtotime($row['result_row']['tgl_pelaksanaan']));
    }
?>

    <div class="row">
        <div class="col-xs-4 col-sm-4 nopadding">
            <a class="pull-left " href="<?php echo $url;?>">
                <img class="img-media-list" src="<?php echo $image;?>" alt="<?php echo $hover_title;?>" >
            </a>
            <!-- <h6 class="labes labes-<?php echo $_color;?>" style="background-color:<?php echo $_color;?>;"><?php echo $segment_title;?></h6> -->
        </div>
        <div class="col-xs-8 col-sm-8">
            <h5 class="media-heading-nomargin"><span class="labes labes-<?php echo $_color;?>"><?php echo $segment_title;?></span> <a class="" href="<?php echo $url;?>"><?php echo $short_title;?></a></h5>
            <small class="media-heading-nomargin"><?php echo $content_date;?></small>
        </div>

    </div>
    <hr class="line-mini">
<?php } ?>
