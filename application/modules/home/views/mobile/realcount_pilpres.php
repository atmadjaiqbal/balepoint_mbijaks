<div class="row-fluid">
  <div class="span12">
     <table summary='' class='home-race-pilpres' >
        <tr>
           <td style="width: 100px; height: 65px;vertical-align: bottom;text-align: center;padding-top:5px;"></td>
           <td style="width: 60px; height: 60px;vertical-align: bottom;text-align: center;padding-top:5px;">
               <img class="pilpres-image" src="http://www.bijaks.net/assets/images/pilpres-2014/prabowo-hatta.png">
           </td>
           <td style="width: 60px; height: 60px;vertical-align: bottom;text-align: center;padding-top:5px;">
               <img class="pilpres-image" src="http://www.bijaks.net/assets/images/pilpres-2014/jokowi-jk.png">
           </td>
        </tr>
        <tr>
           <td class="pilpres-race-title">
              <a style="cursor: default;font-size: 12px;">KPU</a><br/>
              <!-- em><small>Data masuk: 99%</small></em><br/ -->
              <em><small>Last update: 22-07-2014 18:51:01</small></em><br/>
              <em><small>Data yang di hasilkan dari rekapitulasi lembaga KPU</small></em>
           </td>
           <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
              <div class='pilpres-percentage-bar ' style='height:46.85px;background-color:#EC282A;'>
                <div class="pilpres-bar-2">46.85 %</div>
              </div>
           </td>
           <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
              <div class='pilpres-percentage-bar ' style='height:53.15px;background-color:#4A3ED6;;'>
                 <div class="pilpres-bar-2">53.15 %</div>
              </div>
           </td>
        </tr>

        <tr>
           <td class="pilpres-race-title">
             <a style="cursor: default;font-size: 12px;">KAWAL PEMILU</a><br/>
             <em><small>Data masuk: 98.02%</small></em><br/>
             <em><small>Last update: 22-07-2014 14:49:00</small></em>
             <em><small></small></em>
           </td>
           <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
             <div class='pilpres-percentage-bar ' style='height:46.84px;background-color:#EC282A;'>
                <div class="pilpres-bar-2">46.84 %</div>
             </div>
           </td>
           <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
             <div class='pilpres-percentage-bar ' style='height:52.89px;background-color:#4A3ED6;;'>
                <div class="pilpres-bar-2">53.15 %</div>
             </div>
           </td>
         </tr>

         <tr>
             <td class="pilpres-race-title">
                 <a style="cursor: default;font-size: 12px;">TIM SUKSES NO.1<br/>PRABOWO-HATTA</a><br/>
                 <em><small>Data masuk: 99.97%</small></em><br/>
                 <em><small>Last update: 15-07-2014</small></em>
                 <em><small></small></em>
             </td>
             <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-bar ' style='height:53.52px;background-color:#4A3ED6;'>
                     <div class="pilpres-bar-2">53.52 %</div>
                 </div>
             </td>
             <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-bar ' style='height:46.48px;background-color:#EC282A;'>
                     <div class="pilpres-bar-2">46.48 %</div>
                 </div>
             </td>
         </tr>

         <tr>
             <td class="pilpres-race-title">
                 <a style="cursor: default;font-size: 12px;">TIM SUKSES NO.2<br>JOKOWI-JK</a><br/>
                 <em><small>Data masuk: 90%</small></em><br/>
                 <em><small>Last update: 15-07-2014 20:39:00</small></em>
                 <em><small></small></em>
             </td>
             <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-bar ' style='height:46.54px;background-color:#EC282A;'>
                     <div class="pilpres-bar-2">46.54 %</div>
                 </div>
             </td>
             <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
                 <div class='pilpres-percentage-bar ' style='height:53.46px;background-color:#4A3ED6;;'>
                     <div class="pilpres-bar-2">53.46 %</div>
                 </div>
             </td>
         </tr>

     </table>
  </div>
</div>
