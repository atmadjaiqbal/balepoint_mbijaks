<?php foreach ($hot_profile as $kei=>$row)
{
$_profile_url = base_url().'aktor/profile/'.$row['page_id'];
$content_id = $row['profile_content_id']; 
$img_url = (count($row['headline_photo']) > 0) ? $row['headline_photo'][0]['badge_url'] : $row['badge_url'];
?>

<div class="row">
    <?php //if($kei == 1){ ?>
    <div class="col-xs-4 col-sm-4 nopadding">
        <a class="pull-left " href="<?php echo $_profile_url;?>">
            <img class="img-media-list lazy_" src='<?php echo $img_url; ?>' alt="<?php echo $row['page_name'];?>" <?php /*width="376" height="213"*/?>>
        </a>

    </div>
    <div class="col-xs-8 col-sm-8">
        <a class="" href="<?php echo $_profile_url;?>">
            <h5 class="media-heading"><?php echo $row['page_name']; ?> - <small><?php echo $row['partai_name'];?></small></h5>
        </a>
        <span class="pull-left"><strong>Posisi</strong> : <?php echo substr($row['posisi'],0,30) .'...';?></span>
        <span class="pull-left clearfix"><strong>Prestasi</strong> : <?php echo character_limiter($row['prestasi'], 27);?></span>
        

        <div class="pull-left clearfix"><strong>Skandal</strong> : 
        <?php 
        $p = 0;
        foreach($row['scandal'] as $item){
            if($p < 5){
                $skandal_uri = base_url() . 'scandal/index/'.$item['scandal_id'].'-'.urltitle($item['scandal_title']);
                $badge_color = "#666666";
                if ($item['pengaruh'] == '1')   $badge_color = "#953B39";
                if ($item['pengaruh'] == '2')   $badge_color = "#E2C402";
                if ($item['pengaruh'] == '3')   $badge_color = "#0ACC27";
        ?>
        <a class="labes " style="background-color:<?php echo $badge_color;?>;" href="<?php echo $skandal_uri; ?>">
            <small><?php
                if(strlen($item['scandal_title']) > 8 )
                {
                    $skandaljudul = substr($item['scandal_title'], 0, 10);
                } else {
                    $skandaljudul = $item['scandal_title'];
                }
                echo $skandaljudul;
                ?></small>
        </a>
        
        <?php        

            }
            $p++;
        }
        ?>    
        </div>

    </div>
    
    <?php /*}else{ ?>
    <div class="col-xs-12 col-sm-12">
        <a class="" href="<?php echo $_profile_url;?>">
            <h5 class="media-heading"><?php echo $row['page_name']; ?><?php echo ($row['partai_alias'] != 'None') ? '<small> - '.$row['partai_alias'].'</small>' : '';?></h5>
        </a>
        
    </div>
    <?php } */?>

</div>

<hr class="line-mini">
<?php } ?>