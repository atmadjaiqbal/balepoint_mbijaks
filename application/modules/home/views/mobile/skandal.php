<?php
if(!empty($skandal))
{
//    echo '<pre>';print_r($val);echo '</pre>';

   foreach($skandal as $key => $val) {

    $skandal_url = base_url() . 'scandal/index/'.$val['scandal_id'].'-'.urltitle($val['title']);
    $skandal_photos = (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
    $css_image = 'news-image-skandal';
    $limit_terkait = 7;
    if(isset($val['tipe'])){
        $limit_terkait = 2;
        $css_image .= '-'.$val['tipe'];
        $skandal_photos = (count($val['photos']) > 0) ? $val['photos'][0]['thumb_landscape_url'] : '';
    }

    $headx_photos = (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
    $headm_photos = (count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';

    foreach($val['photos'] as $kes => $item){
        if(intval($item['type']) == 1){
            $headx_photos = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                $headx_photos = $item['original_url'];
            }

            $headm_photos = $item['thumb_url'];
            break;
        }
    }

?>

<div class="row">
    <div class="col-xs-4 col-sm-4 nopadding">
        <a class="" href="<?php echo $skandal_url; ?>">
            <img class="img-media-list" src="<?php echo $headx_photos;?>" alt="<?php echo $val['title'];?>">
        </a>

    </div>
    <div class="col-xs-8 col-sm-8">
        <a class="" href="<?php echo $skandal_url; ?>">
            <h5 class="media-heading-nomargin"><?php echo $val['title'];?></h5>
        </a>
        <small class="media-heading-nomargin"><?php echo mdate('%d %M %Y %H:%i', strtotime($val['content_date'])); ?></small>
    <!-- </div>
    
</div>

<div class="row">
    <div class="col-xs-12"> -->
        <div><strong>Status</strong> : <?php echo ucwords($val['status']); ?>, </div>
        <div><strong>Dampak</strong> : <?php echo (strlen($val['uang']) > 80 ? substr($val['uang'], 0, 80) .'...' : $val['uang']); ?></div>
        <?php /*(
        <p>Politisi Terkait :</p>
        <?php
        if (count($val['players']) > 0)
        {
            echo '<ul class="ul-img-hr">';
            foreach ($val['players'] as $key => $pro)
            {
                if($key === $limit_terkait) break;
                ?>
                <li class="">
                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                        <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="" src="<?php echo $pro['profile_icon_url']; ?>">
                    </a>
                </li>
                <?php
            }
            echo '</ul>';
        } else{
            ?>
            <span>Tidak ada politisi terkait</span>
            <?php
        }
        ?>
        */?>
    </div>
</div>
<hr class="line-mini">

<?php 
   }
} ?>

<?php foreach($scandal_more as $i =>  $row){
    $skandal_url = base_url() . 'scandal/index/'.$row['scandal_id'].'-'.urltitle($row['title']);
    $skandal_photos = (count($row['photos']) > 0) ? $row['photos'][0]['large_url'] : '';
    $css_image = 'news-image-skandal';
    $limit_terkait = 6;
//    if(isset($row['tipe'])){
//        $limit_terkait = 2;
//        $css_image .= '-'.$row['tipe'];
//        $skandal_photos = (count($row['photos']) > 0) ? $val['photos'][0]['thumb_landscape_url'] : '';
//    }

    $headx_photos = (count($row['photos']) > 0) ? $row['photos'][0]['large_url'] : '';
    $headm_photos = (count($row['photos']) > 0) ? $row['photos'][0]['thumb_url'] : '';

    foreach($row['photos'] as $kes => $item){
//        if(intval($item['type']) == 1){
            $headx_photos = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                $headx_photos = $item['original_url'];
            }

            $headm_photos = $item['thumb_url'];
            break;
//        }
    }
    ?>

<div class="row">
    <div class="col-xs-4 col-sm-4 nopadding">
        <a class="pull-left " href="<?php echo $skandal_url; ?>">
            <img class="img-media-list lazy_" src="<?php echo $headx_photos;?>" alt="<?php echo $val['title'];?>" >
        </a>

    </div>
    <div class="col-xs-8 col-sm-8">
        <a class="" href="<?php echo $skandal_url; ?>">
            <h5 class="media-heading-nomargin"><?php echo $row['title'];?></h5>
        </a>
        <small class="media-heading-nomargin"><?php echo mdate('%d %M %Y %H:%i', strtotime($row['updated_date'])); ?></small>
<!--     </div>
    <div class="col-xs-12"> -->
        <p class="media-heading-nomargin"><strong>Status</strong> : <?php echo ucwords($row['status']); ?></p>
        <p class="media-heading-nomargin"><strong>Dampak</strong> : <?php echo (strlen($row['uang']) > 80 ? substr($row['uang'], 0, 80) .'...' : $row['uang']); ?></p>
    </div>
    <!-- <div class="col-xs-12">
        <p>Politisi Terkait :</p>
        <?php
        // if (count($row['players']) > 0)
        // {
        //     echo '<ul class="ul-img-hr">';
        //     foreach ($row['players'] as $key => $pro)
        //     {
        //         if($key === $limit_terkait) break;
        //         ?>
        //         <li class="">
        //             <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
        //                 <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="" src="<?php echo $pro['profile_icon_url']; ?>">
        //             </a>
        //         </li>
        //         <?php
        //     }
        //     echo '</ul>';
        // } else{
        //     ?>
        //     <span>Tidak ada politisi terkait</span>
        //     <?php
        // }
        ?>
    </div> -->
</div>
<hr class="line-mini">

<?php } ?>