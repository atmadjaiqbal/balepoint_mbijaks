<!--
<script type="text/javascript">
    $(document).ready(function() {

        var id = '#dialog';

        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        //Set heigth and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});

        //transition effect
        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow",0.8);

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);

        //transition effect
        $(id).fadeIn(2000);

        //if close button is clicked
        $('.window .close').click(function (e) {
            //Cancel the link behavior
            e.preventDefault();

            $('#mask').hide();
            $('.window').hide();
        });

        //if mask is clicked
        $('#mask').click(function () {
            $(this).hide();
            $('.window').hide();
        });

    });

</script>

<style type="text/css">
    #mask {position:absolute;left:0;top:0;z-index:9000;background-color:#000;display:none;}
    #boxes .window {position:absolute;left:0;top:0;width:960px;height:700px;display:none; z-index:9999;padding:5px;}
    #boxes #dialog {width:960px;height:720px;padding:5px;background-color:#ffffff;}
    #boxes .close-overlay {color: #000000;font-family:helvetica, "Times New Roman", arial;position: absolute;top:0px;}
</style>

-->

<div id="boxes">
    <div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window">
        <img src="<?php echo base_url('assets/images/launch-bijak-fullpage.jpg'); ?>" style="background: rgba(0, 0, 0, 0);" >
        <a href="#" class="close">x</a>
    </div>
    <!-- Mask to cover the whole screen -->
    <div style="width: 1478px; height: 900px; display: none; opacity: 0.8;" id="mask"></div>
</div>


<!-- ######### headline ####### -->
<div class="container home" style="/*margin-top: 42px;*/">
  <div class="sub-header-container" >
    <?php if (isset($headline) && !empty($headline)) echo $headline; ?>
  </div>
  <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
  <div id='disclaimer' class="sub-header-container"></div>
</div>


<!-- row for event and caleg 2014 -->
<?php

if(isset($bijaks_ticker))
{
?>
<div class="container">
   <?php echo $bijaks_ticker; ?>
</div>
<?php
}

?>

<!--   row 2 hot profile -->
<div class="container">
    <div class="sub-header-container">
        <?php if (isset($hot_profile) && !empty($hot_profile)) echo $hot_profile; ?>
    </div>
</div>

    <!-- ROW 3 scandal, survey, hot issue -->
 <div class="container">
     <div class="sub-header-container">
        <div class="row-fluid">
            <div class="span8">
                <!-- SCANDAL -->
                <div class="row-fluid">
                    <div class="span12">
                        <div id="home-skandal">
                            <div class="home-title-section hp-label hp-label-skandal" style="background-color: #fa4242;">
                                <span class="hitam"><a href="<?=base_url();?>scandal">SKANDAL</a></span>

                            </div>

                            <?php echo $scandal;?>
                        </div>
                    </div>
                </div>
                <!-- SURVEY -->
                <div class="row-fluid">
                    <div class="span12">
                        <div  class="home-title-section hp-label hp-label-biru" style="background-color: #6b9bfc;">
                            <span class="hitam"><a href="<?=base_url();?>survey">POLLING KOMUNITAS</a></span>
                            </div>
                        <?php echo $survey;?>
                    </div>
                </div>

            </div>
            <div class="span4">
                <!-- HOT ISSUE -->
                <div class="row-fluid">
                    <div class="span12">
                        <div  class="home-title-section hp-label hp-label-hitam" style="background-color: #bbbbbb;">
                            <span class="hitam"><a href="<?php echo base_url().'headline'; ?>">HEADLINE SEBELUMNYA</a></span>
                        </div>

                        <div class="home-issue-container">
                            <div id="dv_next">
                                <?=$issue;?>
                            </div>
                        </div>

                        <div id="" class="row-fluid">
                            <div class="span6 text-left">
                                <a data-tipe="1" id="head_news_next" class="btn btn-mini" >20 Berikutnya</a>
                            </div>
                            <div class="span6 text-right">
                                <a href="<?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
     </div>
 </div>

<div class="container">
    <div class="sub-header-container">
        <img src="<?php echo base_url('assets/images/yuk-gabung.jpg'); ?>" style="width: 955px;">
    </div>
</div>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <div class="span8">

                <div id="home-opini">
                    <div class="home-title-section hp-label hp-label-coklat" style="background-color: #9c6851;margin-top:0px;">
                        <span class="hitam"><a style="cursor:default;">BLOG FAVORIT ANDA</a></span>
                    </div>

                    <div class="home-opini-container">
                        <div id="opini_next">
                            <?=$blog_favorit;?>
                        </div>
                    </div>

                </div>

                <div id="home-opini">
                    <div class="home-title-section hp-label hp-label-coklat" style="background-color: #9c6851;margin-top:0px;">
                        <span class="hitam"><a style="cursor:default;">KOMENTAR ANDA</a></span>
                    </div>

                    <div class="home-komentar-container">
                        <div id="komentar_next">
                            <?=$comment_list;?>
                        </div>
                    </div>

                </div>

            </div>

            <div class="span4">

                <div  class="home-title-section hp-label hp-label-hitam" style="background-color: #bbbbbb;">
                    <span class="hitam"><a href="<?php echo base_url().'news/index/'; ?>">BERITA TERKINI</a></span>
                </div>

                <div class="home-terkini-container">
                    <div id="terkini_next">
                        <?=$terkini;?>
                    </div>
                </div>

                <div id="" class="row-fluid">
                    <div class="span6 text-left">
                        <a data-tipe="1" id="terkini_news_next" class="btn btn-mini" >12 Berikutnya</a>
                    </div>
                    <div class="span6 text-right">
                        <a href="<?php echo base_url().'news/index/'; ?>" class="btn btn-mini" >Selengkapnya</a>
                    </div>
                </div>
<!--
                <div class="home-title-section hp-label hp-label-hitam" style="background-color: #bbbbbb;">
                    <span class="hitam"><a href="#">RESES</a></span>
                </div>

                <div class="home-reses-container">
                    <div id="reses_next">
                        <?=$reses;?>
                    </div>
                </div>

                <div id="" class="row-fluid">
                    <div class="span6 text-left">
                        <a data-tipe="1" id="reses_news_next" class="btn btn-mini" >5 Berikutnya</a>
                    </div>
                    <div class="span6 text-right">
                        <a href="<?php echo base_url().'news/index/reses'; ?>" class="btn btn-mini" >Selengkapnya</a>
                    </div>
                </div>

                -->

            </div>

        </div>

    </div>
</div>
<br/>

    <!-- ######### SUKSESI ####### -->
<div class="container">
    <div class="sub-header-container" style="margin-bottom: 0px;border-bottom: solid 2px #FE5A00;">
        <div id="" class="home-title-section hp-label hp-label-kuning" style="background-color: #FFE930;margin-bottom: 10px;">
            <span class="hitam">
                <a href="<?php echo base_url(); ?>suksesi">SUKSESI DAN SURVEY</a>
            </span>
<!--            <span style="float: right;margin-right: 10px;"><a href="--><?php //echo base_url(); ?><!--suksesi"><i class='icon-align-justify'></i></a></span>-->
        </div>
        <?php if (isset($suksesi) && !empty($suksesi)) :?>
            <?php echo $suksesi; ?>
        <?php endif;?>
    </div>
</div>
<br/>

<!-- ######### BANNER ####### -->
<div class="container" style="text-align: center;">
    <div class="sub-header-container">
    <img src="<?php echo base_url('assets/images/banner_indonesia_baik.png'); ?>">
    </div>
</div>
<br>

<!-- ######### BERITA LAIN ####### -->

<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>
<br/>
<script src="<?php echo base_url(); ?>public/script/jquery/highcharts.js"></script>
<script src="<?php echo base_url(); ?>public/script/jquery/modules/exporting.js"></script>
