<?php
$data['line'] = '';
if (!empty($headline)) :

    ?>


    <div id="slideheadline" class="carousel slide">

        <div id="slideouter">
            <div class="headlogo">
                <img class="lazy" data-original="<?php echo base_url('assets/images/bijaks_logo.png'); ?>" style="background: rgba(0, 0, 0, 0);" >
            </div>
            <div style="margin-top:50px;margin-left:20px;">
                <ul class=leaders>
                    <li><span><strong>People Likes</strong></span><span><?php echo number_format($ttllike['total_like']); ?></span>
                    <li><span><strong>People Comment</strong></span><span><?php echo number_format($ttlcomment['total_comment']); ?></span>
                    <li><span><strong>Total User</strong></span><span><?php echo number_format($ttluser['total_user']); ?></span>
                </ul>
            </div>
            <div style="margin-top: 20px;margin-left: 30px;">
                <div class="ic_scandal" style="float:left;"><span><strong><?php echo number_format($ttlscandal['total_scandal']); ?></strong></span></div>
                <div class="ic_suksesi" style="float:left;"><span><strong><?php echo number_format($ttlsuksesi['total_suksesi']); ?></strong></span></div>
                <div class="ic_tokoh" style="float:left;"><span><strong><?php echo number_format($ttltokoh['total_tokoh']); ?></strong></span></div>
            </div>


        </div>

        <div class="carousel-inner">


            <?php
            $i=0;
            $content_callback 	= base_url('ajax/count_content');
            $content_like_uri 	= '';
            $content_dislike_uri = '';


            foreach($headline as $row){
                $content_id		= $row['content_id'];
                //$content_date 	= mysql_date("%d %M %Y - %h:%i", strtotime($row['entry_date']));
                //$image			= (!empty($row['content_image_url'])) ? config_item('news_url').'uploads'.str_replace('-282x158','',$row['content_image_url']) : '';
                //$url				= base_url() . "news/article/0-" . $row['news_id'] . "/" . $row['content_url'];
                //$content_text 	= preg_replace("/\<a(.*)\>(.*)\<\/a\>/iU", "", $row['content_text']);

                if ($row['content_type'] == 'TEXT')  {
                    $title = word_limiter($row['title'], 5) . ' ...';   // "NEWS";
                    $_textTransform = "TERKINI";
                    $_color = "#9E5B3E;";
                    $url 		= base_url() . "news/article/0-" . $row['news_id'] . "/" . $row['content_url'];
                    $image	= (!empty($row['content_image_url'])) ? config_item('news_url').'uploads'.str_replace('-282x158','',$row['content_image_url']) : '';
                    $content_date 	= mysql_date("%d %M %Y - %h:%i", strtotime($row['entry_date']));
                }

                if ($row['content_type'] == 'HOT_PROFILE')  {
                    $title	= $row['page_name'];
                    $_textTransform = "PROFILE";
                    $_color = "#FF7E00;";
                    $url 		= base_url('aktor/profile/'.$row['page_id']);
                    $image	= (!empty($row['headline_attachment'])) ?  base_url().'public/upload/image/politisi/'.$row['page_id'].'/'.$row['headline_attachment'] : base_url().'public/img/no-image-politisi.png';

//                    $image = (count($row['profile_photo']) > 0) ? $row['profile_photo']['thumb_url'] : base_url().'public/img/no-image-politisi.png';

                    $content_date 	= mysql_date("%d %M %Y - %h:%i", strtotime($row['entry_date']));
                }

                if ($row['content_type'] == 'SCANDAL')  {
                    $title 	= word_limiter($row['title'], 5) . ' ...';
                    $_textTransform = "SCANDAL";
                    $_color = "#DC0000;";
                    $url 		= $row['url'];
                    $image	= large_url($row['photo'], 'skandal');
                    $content_date 	= mysql_date("%d %M %Y - %h:%i", strtotime($row['updated_date']));
                }

                if ($row['content_type'] == 'SUKSESI')  {
                    $title 	= word_limiter($row['race_name'], 5) . ' ...';
                    $_textTransform = "SUKSESI";
                    $_color = "#FFE930;";
                    $url 		= base_url('suksesi/index/'.$row['id_race']. '-' . urltitle($row['race_name']) . '/');
                    //$image	= large_url($row['photos'][0]['attachment_title']);
                    $image	= (!empty($row['photos'][1]['attachment_title'])) ?  base_url().'public/upload/image/suksesi/thumb/'.$row['photos'][1]['attachment_title'] : base_url().'public/img/large/noimage.jpg';

                    echo '<pre>';
                    var_dump($row['photos']);
                    echo '</pre>';
                    exit;

                    foreach($row['photos'] as $rw=>$ph){
                        if(intval($ph['type']) == 4){
                            $image = $ph['large_url'];
                            break;
                        }
                    }

                    $content_date = date("d M Y - H:i", strtotime($row['update_date']));
                }

                if(!empty($account_id)){
                    $content_like_uri 	= ($row['is_like'] == '0') ? base_url('ajax/like') : base_url('ajax/unlike') ;
                    $content_dislike_uri = ($row['is_dislike'] == '0') ? base_url('ajax/dislike') : base_url('ajax/undislike');
                }
                $content_data	= array(
                    'content_id' 		=> $content_id,
                    'content_date' 	=> $content_date,
                    'count_comment' 	=> $row['count_comment'],
                    'count_like' 		=> $row['count_like'],
                    'count_dislike' 	=> $row['count_dislike'],
                    'is_like' 			=> $row['is_like'],
                    'is_dislike' 		=> $row['is_dislike'],
                    'tipe' 				=> '2',
                    'restype' 			=> '2',
                    'callback'			=> $content_callback,
                    'like_uri'			=> $content_like_uri,
                    'dislike_uri'		=> $content_dislike_uri
                );


                ?>
                <div class="item <?php if($i==0) echo " active";?> slideimg">
                    <!--
					<div class="headline-subtitle-wrapper">
						<?php  /* echo $row['politisi']; */ ?>
					</div>
					-->

                    <img src="<?php echo $image;?>" alt="" style="width: 960px; height: 400px;">

                    <div class="carousel-caption" id="slidecaption">
                        <div class="sectioncategories" style="background-color:<?php echo $_color;?>opacity:0.8;"">
                            <p><?php echo $_textTransform; ?></p>
                        </div>
                        <div class="sectiontitle">
                            <div class="headline-date-view"><?php echo (isset($content_date)) ? $content_date : ''; ?></div>
                            <div class="headline-title"><a href="<?php echo $url; ?>"><?php echo $title; ?></a></div>
                        </div>
                        <div class="comunity-icon">
                            <img src="<?php echo base_url('assets/images/comunity_icon_head.png'); ?>" style="width: 45px; height: 110px;">
                        </div>
                        <div class="comunity-total">
                            <p style="font-family: helvetica;font-size: 15px;line-height: 30px;margin-left: 10px;margin-top: 10px;"><?php echo $row['count_comment'];?></p>
                            <p style="font-family: helvetica;font-size: 15px;line-height: 30px;margin-left: 10px;margin-top: 5px;"><?php echo $row['count_like'];?></p>
                            <p style="font-family: helvetica;font-size: 15px;line-height: 30px;margin-left: 10px;margin-top: 5px;"><?php echo $row['count_dislike'];?></p>
                        </div>

                    </div>
                </div>
                <?php
                $i++;

            }

            ?>
        </div>
        <ol class="carousel-indicators">
            <li data-target="#slideheadline" data-slide-to="0" class="active"></li>
            <li data-target="#slideheadline" data-slide-to="1"></li>
            <li data-target="#slideheadline" data-slide-to="2"></li>
            <li data-target="#slideheadline" data-slide-to="3"></li>
            <li data-target="#slideheadline" data-slide-to="4"></li>
            <li data-target="#slideheadline" data-slide-to="5"></li>
        </ol>
    </div>

<?php endif;?>
