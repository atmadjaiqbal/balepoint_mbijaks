<div>
    <div  class="home-title-section hp-label hp-label-hot-profile">
        <span class="hot">HOT PROFILE</span>
    </div>

    <div class='col-4' id='home-hot-profile-menu'>
        <ul>
<?php
            foreach ($hot_profile as $row)
            {
                $_profile_url = base_url().'aktor/profile/'.$row['page_id'];
?>
                <li>
                 <span data-id="<?php echo $row['page_id']; ?>">

                   <img src='<?php echo $row['badge_url']; ?>' data-src='<?php echo $row['badge_url']; ?>'
                        title="<?php echo $row['page_name']; ?>" alt=""/>
                 </span>
                </li>
<?php
            }
?>
        </ul>
    </div>

    <div class='col-5' id='home-hot-profile-details'>
<?php
        foreach ($hot_profile as $row)
        {
            $_profile_url = base_url().'aktor/profile/'.$row['page_id'];
            $content_id = $row['profile_content_id'];
?>
            <div class='home-hot-profile-detail'>
                <div class='clearfix'>
                    <div class='home-hot-profile-detail-pic'>
                        <img src='<?php echo $row['badge_url']; ?>' data-src='<?php echo $row['badge_url']; ?>' alt=''/>
                    </div>
                    <div class='home-hot-profile-detail-text'>
                        <a href="<?php echo $_profile_url; ?>">
                            <h4><?php echo (strlen($row['page_name'])> 25 ? substr($row['page_name'], 0 ,25) . '...' : $row['page_name']); ?></h4>
                        </a>
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Asosiasi</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <span class=""><?php echo $row['partai_name'];?></span>
                            </div>
                        </div>
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Posisi</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <span class=""><?php echo substr($row['posisi'],0,30) .'...';?></span>
                            </div>
                        </div>
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Prestasi</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <?php
                                $prestasi = character_limiter($row['prestasi'], 27);                           ?>
                                <span class=""><?php echo $prestasi;?></span>
                            </div>
                        </div>
                        <hr class="hr-black">
                        <div class="row-fluid">
                            <div class="span2 span2-pd-left">
                                <span><strong>Skandal</strong></span>
                            </div>
                            <div class="span10 span10-pd-right">
                                <?php $p = 0; ?>
                                <?php foreach ($row['scandal'] as $item){ ?>
                                <?php if($p <6){ ?>
                                    <?php
                                    $skandal_uri = base_url() . 'scandal/index/'.$item['scandal_id'].'-'.urltitle($item['scandal_title']);
                                    $badge_color = "#666666";
                                    if ($item['pengaruh'] == '1')	$badge_color = "#953B39";
                                    if ($item['pengaruh'] == '2')	$badge_color = "#E2C402";
                                    if ($item['pengaruh'] == '3')	$badge_color = "#0ACC27";
                                    ?>
                                    <a class="badge " style="background-color:<?php echo $badge_color;?>;" href="<?php echo $skandal_uri; ?>">
                                        <small><?php
                                            if(strlen($item['scandal_title']) > 8 )
                                            {
                                                $skandaljudul = substr($item['scandal_title'], 0, 10);
                                            } else {
                                                $skandaljudul = $item['scandal_title'];
                                            }
                                            echo $skandaljudul;
                                            ?></small>
                                    </a>
                                    <?php } ?>
                                <?php $p++; ?>
                                <?php }?>

                            </div>
                        </div>


                        <!--<table class='table-h'>
                            <tr class="nobottomborder">
                                <td colspan='2'>
                                    <a href="<?php /*echo $_profile_url; */?>">
                                     <h4><?php /*echo (strlen($row['page_name'])> 25 ? substr($row['page_name'], 0 ,25) . '...' : $row['page_name']); */?></h4>
                                    </a>
                                </td>
                            </tr>
                            <tr class='home-hot-profile-detail-skandal'>
                                <th style="width:60px;vertical-align: top;">Posisi</th>
                                <td><?php /*echo substr($row['posisi'],0,30) .'...';*/?>
                                </td>
                            </tr>
                            <tr class='home-hot-profile-detail-skandal'>
                                <th style="width:60px;vertical-align: top;">Prestasi</th>
                                <td>
                                    <?php /*$prestasi = $row['prestasi'] .", ". $row['prestasi_lain']; */?>
                                    <div id="profile_prestasi-<?php /*echo $row['page_id'];*/?>" style="display:block;">
                                        <?php /*echo substr($prestasi,0,27); */?>
                                        <?php /*if (strlen($prestasi) > 27 ): */?>... <?php /*endif;*/?>
                                    </div>

                                    <?php /*if (strlen($prestasi) > 27 ): */?>
                                        <div id="profile_prestasi_more-<?php /*echo $row['page_id'];*/?>" style="display:none;">
                                            <?php /*echo $prestasi;*/?>
                                        </div>
                                    <?php /*endif;*/?>
                                </td>
                            </tr>

                            <tr class='home-hot-profile-detail-skandal'><th style="width:60px;vertical-align: top;">Skandal</th><td>
                                    <?php /*if (!empty($row['scandal'])):*/?>
                                    <?php /*$p = 0; */?>
                                        <?php /*foreach ($row['scandal'] as $item) : */?>
                                            <?php /*if($p <6): */?>
                                            <?php
/*                                            $skandal_uri = base_url() . 'scandal/index/'.$item['scandal_id'].'-'.urltitle($item['scandal_title']);
                                            $badge_color = "#666666";
                                            if ($item['pengaruh'] == '1')	$badge_color = "#953B39";
                                            if ($item['pengaruh'] == '2')	$badge_color = "#E2C402";
                                            if ($item['pengaruh'] == '3')	$badge_color = "#0ACC27";
                                            */?>
                                            <a class="badge " style="background-color:<?php /*echo $badge_color;*/?>;" href="<?php /*echo $skandal_uri; */?>">
                                            <small><?php /*
                                            if(strlen($item['scandal_title']) > 8 )
                                            {
                                               $skandaljudul = substr($item['scandal_title'], 0, 10);
                                            } else {
                                               $skandaljudul = $item['scandal_title'];
                                            }
                                            echo $skandaljudul;
                                            */?></small>
                                            </a>
                                        <?php /*endif; */?>
                                        <?php /*$p++; */?>
                                        <?php /*endforeach;*/?>
                                    <?php //endif;?>
                                </td></tr>
                        </table>-->
                    </div>
                </div>
                <div class='home-hot-profile-detail-map'>
                    <img src="<?php echo base_url();?>assets/images/power-map-430.gif" style="width:430px;">
                </div>
<!--                <div data-uri="<?php echo base_url(); ?>powermap/polprofile/<?php echo  $row['page_id']; ?>" data-id="<?php echo $row['page_id']; ?>" id="pm_<?php echo $row['page_id']; ?>" class='home-hot-profile-detail-map'>
                </div>-->

            </div>
<?php
        }
?>

    </div>
</div>