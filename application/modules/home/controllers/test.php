<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends Application {


	public function __construct()
    {
        parent::__construct();
        $this->load->module('survey');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
    }


	public function index()
	{
		$this->load->library('core/cache_lib');

		$data				 = $this->data;
        // $data['scripts']     = array('bijaks.home.js', 'bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['scripts']     = array('bijaks.home.js');

        /** NEWS **/
        $data['nasional'] = $this->news->sesi('nasional', 9, 'true', 0);
        $data['hukum'] = $this->news->sesi('hukum', 9, 'true', 0);
        $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true', 0);
        $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true', 0);
        $data['internasional'] = $this->news->sesi('internasional', 9, 'true', 0);
        $data['indobarat'] = $this->news->sesi('indobarat', 9, 'true', 0);
        $data['indoteng'] = $this->news->sesi('indoteng', 9, 'true', 0);
        $data['indotim'] = $this->news->sesi('indotim', 9, 'true', 0);

        $data['lingkungan'] = $this->news->sesi('lingkungan', 4, 'true', 0);
        $data['kesra'] = $this->news->sesi('kesenjangan', 4, 'true', 0);
        $data['sara'] = $this->news->sesi('sara', 4, 'true', 0);
        $data['komunitas'] = $this->news->sesi('komunitas', 4, 'true', 0);
        $data['gayahidup'] = $this->news->sesi('gayahidup', 4, 'true', 0);
        $data['reses'] = $this->news->sesi('reses', 4, 'true', 0);


        // /** END NEWS **/

        $data['bijaks_ticker'] = $this->bijaksticker();

        $data['hot_profile'] = $this->hot_profile();
        $data['issue']       = $this->headline_list(1, 10);
        $data['scandal']     = $this->skandal();

        $html['html']['content'] = $this->load->view('home/mobile/index_test', $data, true);
        $this->load->view('m_tpl/layout2', $html);

	}

    public function headline()
    {
        $cm = $this->load->module('ajax');
        $data['topic_last_activity'] =  'headline';
        $arr_news = array(); $arr_profile = array(); $arr_scandal = array(); $arr_suksesi = array();$arr_total = array();$arr_profhead = array();
        $resTotal = $this->redis_slave->get("home:total");
        $arr_total = @json_decode($resTotal, true);
        $resultNews = $this->redis_slave->lrange("news:list:headline", 0, 4);
        if(count($resultNews))
        {
            foreach($resultNews as $key => $value)
            {
                $rowNews = $this->redis_slave->get('news:detail:'.$value);
                $arr_news = @json_decode($rowNews, true);
                $headlineNews[$key]['content_type'] = 'TEXT';
                $headlineNews[$key]['result_row'] = $arr_news;
                $headlineNews[$key]['content_id'] = $arr_news['content_id'];
                $rowComment = $this->home_lib->GetCountComment($arr_news['content_id']);
                $rowLike = $this->home_lib->GetCountLike($arr_news['content_id']);
                $rowDislike = $this->home_lib->GetCountDislike($arr_news['content_id']);
                $headlineNews[$key]['count_comment'] = $rowComment['Jumlah'];
                $headlineNews[$key]['count_like'] = $rowLike['Jumlah'];
                $headlineNews[$key]['count_dislike'] = $rowDislike['Jumlah'];
            }
            $data['headline'] = $headlineNews;
        }
        $resultProfile = $this->redis_slave->lrange("profile:list:headline", 0, 1);
        if(count($resultProfile))
        {
            foreach($resultProfile as $key => $value)
            {
               $rowProf = $this->redis_slave->get('profile:detail:'.$value);
                $arr_profile = @json_decode($rowProf, true);
                $headlineProfile[$key]['content_type'] = 'HOT_PROFILE';

                $headlineProfile[$key]['result_row'] = $arr_profile;
                $headlineProfile[$key]['content_id'] = $arr_profile['profile_content_id'];

                $rowComment = $this->home_lib->GetCountComment($arr_profile['profile_content_id']);
                $rowLike = $this->home_lib->GetCountLike($arr_profile['profile_content_id']);
                $rowDislike = $this->home_lib->GetCountDislike($arr_profile['profile_content_id']);
                $headlineProfile[$key]['count_comment'] = $rowComment['Jumlah'];
                $headlineProfile[$key]['count_like'] = $rowLike['Jumlah'];
                $headlineProfile[$key]['count_dislike'] = $rowDislike['Jumlah'];
            }
            $data['headline'][] = $headlineProfile[0];
        }

        $resultScandal = $this->redis_slave->lrange("scandal:list:headline", 0, 0);
        if(count($resultScandal))
        {
            foreach($resultScandal as $key => $value)
            {
                $rowScandal = $this->redis_slave->get('scandal:min:'.$value);
                $arr_scandal = @json_decode($rowScandal, true);
                $headlineScandal[$key]['content_type'] = 'SKANDAL';
                $headlineScandal[$key]['result_row'] = $arr_scandal;
                $rowFoto = $this->redis_slave->get('scandal:photo:'.$value);
                $arr_photo = @json_decode($rowFoto, true);
                $headlineScandal[$key]['scandal_photo'] = $arr_photo;
                $headlineScandal[$key]['content_id'] = $arr_scandal['content_id'];
                $rowComment = $this->home_lib->GetCountComment($arr_scandal['content_id']);
                $rowLike = $this->home_lib->GetCountLike($arr_scandal['content_id']);
                $rowDislike = $this->home_lib->GetCountDislike($arr_scandal['content_id']);
                $headlineScandal[$key]['count_comment'] = $rowComment['Jumlah'];
                $headlineScandal[$key]['count_like'] = $rowLike['Jumlah'];
                $headlineScandal[$key]['count_dislike'] = $rowDislike['Jumlah'];
            }
        }
        $headlineScandal[0]['content_id'] = $arr_scandal['content_id'];
        $data['headline'][] = $headlineScandal[0];

        $resultSuksesi = $this->redis_slave->lrange("suksesi:list:headline", 0, 1);
        if(count($resultSuksesi))
        {
            foreach($resultSuksesi as $key => $value)
            {
                $rowSuksesi = $this->redis_slave->get('suksesi:detail:'.$value);
                $arr_suksesi = @json_decode($rowSuksesi, true);
                $headlineSuksesi[$key]['content_type'] = 'SUKSESI';
                $headlineSuksesi[$key]['result_row'] = $arr_suksesi;
                $headlineSuksesi[$key]['content_id'] = $arr_suksesi['content_id'];
                $rowComment = $this->home_lib->GetCountComment($arr_suksesi['content_id']);
                $rowLike = $this->home_lib->GetCountLike($arr_suksesi['content_id']);
                $rowDislike = $this->home_lib->GetCountDislike($arr_suksesi['content_id']);
                $headlineSuksesi[$key]['count_comment'] = $rowComment['Jumlah'];
                $headlineSuksesi[$key]['count_like'] = $rowLike['Jumlah'];
                $headlineSuksesi[$key]['count_dislike'] = $rowDislike['Jumlah'];
            }
            $data['headline'][] = $headlineSuksesi[0];
        }
        foreach($arr_total as $rwKomu)
        {
            $data[$rwKomu['tipe']] = $rwKomu['total'];
        }

        return $this->load->view('home/mobile/headline', $data, true);
    }

    public function headline_list($page=1, $img_show = 5)
    {
        $limit = 9;

        $rkey = "news:list:headline";
        $offset = (($limit*$page) - $limit);
        if($page == 1){
            $offset = (($limit*$page) - $limit);
        }

        $rrange = $offset + $limit;
        $resultNews = $this->redis_slave->lrange($rkey, $offset, $rrange);
        // echo '<pre>';
        // var_dump($resultNews);
        // echo '</pre>';
        $result = array();

        $i = 0;
        foreach($resultNews as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_array = json_decode($rowNews, true);
            $result[$i] = $news_array;
            $result[$i]['topic_id'] = $news_array['categories'][0]['id'];
            $i++;
        }
        $data['val'] = $result;

        $data['img_show'] = $img_show;

        // echo '<pre>';
        // var_dump($result);
        // echo '</pre>';
        // exit();

        return $this->load->view('mobile/news_headline_list', $data, true);
    }

    public function terkini_list($page=1)
    {
        $topic_id = 0;
        $limit = 9;

        $offset = (($limit*$page) - $limit);
        //if($page == 1){
        //    $offset = (($limit*$page) - $limit);
        //}

        $rrange = $offset + $limit;
        $last_key = $this->redis_slave->get('news:key');
        $rkey = 'news:topic:list:'.$last_key.':0';
        $news = $this->redis_slave->lrange($rkey, $offset, $rrange);
        $resnews = array();

        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_array = json_decode($rowNews, true);
            $resnews[$i] = $news_array;
            $resnews[$i]['topic_id'] = $news_array['categories'][0]['id'];
            $i++;
        }
        $data['val'] = $resnews;

        return $this->load->view('mobile/terkini_headline_list', $data, true);
    }

    public function terkini()
    {
        $topic_id = 0;
        $last_key = $this->redis_slave->get('news:key');
        $rkey = 'news:topic:list:'.$last_key.':0';
        $news = $this->redis_slave->lrange($rkey, 0,1);
        $result = array(); $resmore = array();
        if(count($news) > 0){
            foreach ($news as $key => $value) {
                $news_detail = $this->redis_slave->get('news:detail:'.$value);
                $news_array = @json_decode($news_detail, true);
                $result[$key] = $news_array;
                $result[$key]['topic_id'] = $news_array['categories'][0]['id'];
            }
        }

        $morenews = $this->redis_slave->lRange($rkey, 1,3);
        $arr_more = array();
        if(count($morenews) > 0)
        {
            foreach($morenews as $key => $value)
            {
                $next_news = $this->redis_slave->get('news:detail:'.$value);
                $arr_more = @json_decode($next_news, true);
                $resmore[$key] = $arr_more;
                $resmore[$key]['topic_id'] = $arr_more['categories'][0]['id'];
            }
        }
        $data['result'] = $result;
        $data['resmore'] = $resmore;
        return $this->load->view('home/terkini', $data, true);
    }

    public function reses_list($page=1)
    {
        $data['category'] = 'reses';
        $topic_id = 0; $limit = 10; $id = 18;

        $offset = (($limit*$page) - $limit);
        //if($page == 1){
        //    $offset = (($limit*$page) - $limit) + 3;
       // }

        $rrange = $offset + $limit;
        $last_key = $this->redis_slave->get('news:key');

        $rkey = 'news:topic:list:'.$last_key.':'.$id;
        $news = $this->redis_slave->lrange($rkey, $offset, $rrange);

        $resnews = array();

        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_array = json_decode($rowNews, true);
            $resnews[$i] = $news_array;
            $resnews[$i]['topic_id'] = $news_array['categories'][0]['id'];
            $i++;
        }
        $data['val'] = $resnews;

        return $this->load->view('reses_headline_list', $data, true);
    }

    public function news($id=0, $category='terkini')
    {
            $data['category'] = $category;
            $result = array(); $nextarr = array();
            $last_key = $this->redis_slave->get('news:key');
            $rkey = 'news:topic:list:'.$last_key.':'.$id;
            $news = $this->redis_slave->lRange($rkey, 0, 0);
            if(count($news) > 0)
            {
                foreach ($news as $key => $value)
                {
                    $news_detail = $this->redis_slave->get('news:detail:'.$value);
                    $news_array = @json_decode($news_detail, true);
                    $result = $news_array;
                    $result['topic_id'] = $news_array['categories'][0]['id'];
                }
            }

            $nextnews = $this->redis_slave->lRange($rkey, 1, 5);
            if(count($nextnews) > 0)
            {
                foreach ($nextnews as $key => $value)
                {
                    $next_detail = $this->redis_slave->get('news:detail:'.$value);
                    $next_array = @json_decode($next_detail, true);
                    $nextarr[$key] = $next_array;
                    $nextarr[$key]['topic_id'] = $next_array['categories'][0]['id'];
                }
            }

            $data['row'] = $result;
            $data['nextresult'] = $nextarr;
            return $this->load->view('home/news', $data, true);
    }


    public function hot_profile()
    {
        $data['category'] = 'hot-profile'; $i = 1;
/*
        $profile_key = 'profile:list:hot';
        $list_profile = $this->redis_slave->lrange($profile_key, 0, 9);
        $arr_profile = array();
        if(count($list_profile))
        {
            foreach ($list_profile as $key => $value)
            {
                $profile_detail = $this->redis_slave->get('profile:detail:'.$value);
                $arr_profile = @json_decode($profile_detail, true);
                $result[] = $arr_profile;
            }
        }
*/
        $list_profile = $this->politik_lib->getHotlist(15);
        if(!empty($list_profile))
        {
            foreach ($list_profile as $key => $value)
            {
                $profile_detail = $this->redis_slave->get('profile:detail:'.$value['page_id']);
                $arr_profile = @json_decode($profile_detail, true);
                $result[$i] = $arr_profile;
                $i++;
            }
        }
        $data['hot_profile'] = $result;
        return $this->load->view('home/mobile/hot_profile', $data, true);
    }

    public function skandal()
    {
        //$ScandalAllKey = 'scandal:list:hot';
//        $ScandalAllKey = 'scandal:list:headline';
//        $skandal_list = $this->redis_slave->lRange($ScandalAllKey, 0, 0);
        $skandal_list = $this->scandal_lib->getScandalList('headline',0,1);

        $data['topic_last_activity'] = 'scandal';
        $skandalListArr = array();
        if(count($skandal_list) > 0)
        {
           foreach($skandal_list as $key => $value)
           {
               //$skandal_detail = $this->redis_slave->get('scandal:min:'.$value);
               //$skandal_array = @json_decode($skandal_detail, true);
               $skandal_array = $this->scandal_lib->getScandalDetail($value['scandal_id']);

               //$skandal_kronologi_array =

//               $skandal_kronologi = $this->redis_slave->get('scandal:kronologi:'.$value);
//               $skandal_kronologi_array = @json_decode($skandal_kronologi, true);

//               $skandal_photo = $this->redis_slave->get('scandal:photo:'.$value);
//               $skandal_photo_array = @json_decode($skandal_photo, true);

//               $skandal_players = $this->redis_slave->get('scandal:players:'.$value);
//               $skandal_players_array = @json_decode($skandal_players, true);

//               $skandal_relations = $this->redis_slave->get('scandal:relations:'.$value);
//               $skandal_relations_array = @json_decode($skandal_relations, true);
//               arsort($skandal_photo_array);


//               $skandal_array['photos'] = $skandal_photo_array;
//               $skandal_array['kronologi'] = $skandal_kronologi_array;
//               $skandal_array['players'] = $skandal_players_array;
//               $skandal_array['relations'] = $skandal_relations_array;
               $resskandal[$key] = $skandal_array;
           }


        }

        if(!empty($resskandal))
        {
          foreach($resskandal as $key => $value)
          {
             $data['val'] = $value;
          }
        }

//        $ScandalMoreKey = 'scandal:list:hot';
        //$ScandalMoreKey = 'scandal:list:headline';
//        $more_list = $this->redis_slave->lRange($ScandalMoreKey, 1, 9);

        $more_list = $this->scandal_lib->getScandalList('hot',0,9);
        $data['topic_last_activity'] = 'scandal';
        $MoreArr = array();
        if(count($more_list) > 0)
        {
            foreach($more_list as $key => $value)
            {
//                $Moredetail = $this->redis_slave->get('scandal:min:'.$value);
//                $MoreArr[$key] = @json_decode( $Moredetail, true);
//                $more_photo = $this->redis_slave->get('scandal:photo:'.$value);
//                $more_photo_array = @json_decode($more_photo, true);
//                $MoreArr[$key]['photos'] = $more_photo_array;
//
//                $skandal_players = $this->redis_slave->get('scandal:players:'.$value);
//                $skandal_players_array = @json_decode($skandal_players, true);
//                $MoreArr[$key]['players'] = $skandal_players_array;
                $MoreArr[$key] = $this->scandal_lib->getScandalDetail($value['scandal_id']);
            }
        }

        $data['scandal_more'] = $MoreArr;
        return $this->load->view('home/mobile/skandal', $data, true);
    }

    public function suksesi()
    {
        $arrSuksesi = array();
        $resultSuksesi = $this->redis_slave->lrange("suksesi:list:hot", 0, 0);
        if(count($resultSuksesi))
        {
            foreach($resultSuksesi as $key => $value)
            {
                $rowSuksesi = $this->redis_slave->get('suksesi:detail:'.$value);
                $arrsuksesi = @json_decode($rowSuksesi, true);
                $rsSuksesi[$key] = $arrsuksesi;
            }
        }

        //** More suksesi slider **//
        $arrMore = array();
        $resultMore = $this->redis_slave->lrange("suksesi:list:all", 2, 32);
        if(count($resultMore))
        {
            foreach($resultMore as $key => $value)
            {
                $rowMore = $this->redis_slave->get('suksesi:detail:'.$value);
                $arrMore = @json_decode($rowMore, true);
                $rsMore[$key] = $arrMore;
            }
            array_multisort($rsMore, SORT_DESC, $resultMore);
        }
        $data['suksesi'] = $rsSuksesi;
        $data['moresuksesi'] = $rsMore;
        // return $this->load->view('home/mobile/pilpres', $data, true);
        return $this->load->view('template/suksesi/tpl_suksesi_capres', $data, true);
        // return $this->load->view('home/mobile/suksesi', $data, true);
    }

    public function quickcount()
    {
        $arrSuksesi = array();
        $resultSuksesi = $this->redis_slave->lrange("suksesi:list:hot", 0, 0);
        if(count($resultSuksesi))
        {
            foreach($resultSuksesi as $key => $value)
            {
                $rowSuksesi = $this->redis_slave->get('suksesi:detail:'.$value);
                $arrsuksesi = @json_decode($rowSuksesi, true);
                $rsSuksesi[$key] = $arrsuksesi;
            }
        }
        $data['suksesi'] = $rsSuksesi;
        return $this->load->view('template/suksesi/tpl_quickcount_capres', $data, true);
    }


    public function survey()
    {
        $arrSurvey = array();
        $data['category'] = 'survey';
        $surveyKey = 'survey:list:all';
        $surveyList = $this->redis_slave->lrange($surveyKey, 0, 3);

        if(count($surveyList))
        {
            foreach($surveyList as $key => $value)
            {
                $rowSurvey = $this->redis_slave->get('survey:detail:'.$value);
                $arrSurvey = @json_decode($rowSurvey, true);
                $rsSurvey[$key] = $arrSurvey;
            }
        }

        $data['survey'] = $rsSurvey;
        return $this->load->view('home/mobile/survey', $data, true);
    }

    public function bijaksticker()
    {
        $this->load->model('content_model');
        $data['rsevent'] = $this->content_model->getContentEvent();
        $this->db->close();

        // $profile_key = 'profile:list:hot';
        // $list_profile = $this->redis_slave->lrange($profile_key, 0, 13);
        // $arr_profile = array();
        // if(count($list_profile))
        // {
        //     foreach ($list_profile as $key => $value)
        //     {
        //         $profile_detail = $this->redis_slave->get('profile:detail:'.$value);
        //         $arr_profile = @json_decode($profile_detail, true);
        //         $result[$key] = $arr_profile;
        //     }
        // }

        // $data['hot_profile'] = $result;
        return $this->load->view('home/mobile/sticker', $data, true);
    }

    public function pilpresindex()
    {
        $data = '';
        return $this->load->view('home/mobile/pilpres_index', $data, true);
    }


    private function GetLastUpdateComunity($BackColor=null)
    {
        $comunity = $this->load->module('ajax');
        $data['rsSocial'] = $comunity->GetLastSocialInfo();
        $data['BackColor'] = $BackColor;
        $data['width'] = "300px;";
        return $this->load->view('home/home_comunity_update', $data, true);
    }

    private function GetLastComment($BackColor=null)
    {
        $comunity = $this->load->module('ajax');
        $data['rsSocial'] = $comunity->GetCommentByContentGroupType('12');
        $data['BackColor'] = $BackColor;
        $data['width'] = "320px;";
        return $this->load->view('home/home_comunity_update', $data, true);
    }

    private function GetLastLike($BackColor=null)
    {
        $comunity = $this->load->module('ajax');
        $data['rsSocial'] = $comunity->GetLikeByContentGroupType('12');
        $data['BackColor'] = $BackColor;
        $data['width'] = "320px;";
        return $this->load->view('home/home_comunity_update', $data, true);
    }

    private function GetLastDislike($BackColor=null)
    {
        $comunity = $this->load->module('ajax');
        $data['rsSocial'] = $comunity->GetDisikeByContentGroupType('12');
        $data['BackColor'] = $BackColor;
        $data['width'] = "320px;";
        return $this->load->view('home/home_comunity_update', $data, true);
    }

    public function login()
	{

        $data['title'] = "Bijaks | Life & Politics";
        $data	= $this->data;
        $data['topic_last_activity'] =  '';

        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $this->load->helper('recaptcha');

        $profile_key = 'profile:list:hot';
        $list_profile = $this->redis_slave->lrange($profile_key, 0, 8);
        $arr_profile = array();
        if(count($list_profile))
        {
          foreach ($list_profile as $key => $value)
          {
            $profile_detail = $this->redis_slave->get('profile:detail:'.$value);
            $arr_profile = @json_decode($profile_detail, true);
            $result[$key] = $arr_profile;
          }
        }
      
        $data['hotprofile'] = $result;



		if($this->member)
		{
			redirect(base_url());
			exit();
		}

		if(isset($_GET['redirect']) && @$_GET['redirect'] != '')
		{
			$data['referrer'] = $_GET['redirect'];
		} else {
			$data['referrer'] = '/komunitas';
		}


        $data['hot_profile'] = $this->profile->hot('true');
        $data['terkini'] = $this->news->sesi('0', 0, 'true');

//               $data['issue'] = $this->news->sesi('hot-issues', 0, 'true');
        $data['survey'] = $this->survey->mini_survey();
        $data['reses']    = $this->headline->reses_list();
        $data['skandal_mini'] = $this->scandal->sesi('true');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 9, 'true');

        $html['html']['content']  = $this->load->view('home/mobile/login', $data, true);
//        $html['html']['content']  = $this->load->view('home/login', $data, true);
//        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
//        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
//        $this->load->view('template/tpl_one_column', $html, false);
        $this->load->view('m_tpl/layout', $html);
	}


	public function loginaction()
	{
		$this->load->library('core/member_lib');
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
            if(isset($_POST['email']) and isset($_POST['pass']))
            {
					if(!is_null($_POST['email']) and !is_null($_POST['pass']))
					{

						$email = $_POST['email'] ;
						$password = $_POST['pass'];

						$this->load->model('Member_model');

						$member = $this->Member_model->getMemberByEmail($email)->row_array();

						$row  = $member;
						$salt = ($row ? $row['salt'] : NULL);
						$pwd  = ($row ? $row['passwd'] : NULL);

						$msg = array('message' => 'Maaf, Terjadi kesalahan.');

						if (!isset($row['account_status'])) {
							$msg = array('message' => 'Maaf, Terjadi kesalahan.');

						} else  if ($row['account_status'] == '0') {
							$msg = array('message' => 'Maaf,  Akun anda belum aktif.');

						} elseif ($row['account_status'] == '2') {
							$msg = array('message' => 'Maaf untuk sementara account anda di non aktifkan, karena ada laporan penyalahgunaan konten. silahkan hubungi administrator.');

						} elseif ($row['account_status'] == '1') {
							if($pwd === $this->_generatePassword($password, $salt) and ($row['account_type'] == '0' or $row['account_type'] == '2'))
							{
								$sesdata = array(
									'username'  => $row['display_name'],
									'user_id' => $row['page_id'],
									'email'     => $row['email'],
									'logged_in' => TRUE,
									'account_type' => $row['account_type'],
									'account_id' => $row['account_id'],
									'profile_id' => $row['profile_content_id'],
									'page_id' => $row['page_id'],
									'xname' => $this->getMemberPhoto($row['profile_content_id'])
								);

								$this->session->set_userdata('member', $sesdata);


								$msg = array('message' => 'ok');
								$msg['uid']= $row['account_id'];

								// update last login date
								$loginData  = array('last_login_date' => mysql_datetime(), 'account_id' => $row['account_id']);
								$this->Member_model->moveLastLoginDate($loginData['account_id']);
								$this->Member_model->updateLastLoginDate($loginData);
								$this->member_lib->ShowNoticeLike($loginData['account_id'], 'y');
								$this->member_lib->ShowNoticeUnlike($loginData['account_id'], 'y');
								$this->member_lib->ShowNoticeComment($loginData['account_id'], 'y');
                                                                redirect(base_url());
                                                                exit;
							} else {
								$msg = array('message' => 'Email dan password salah.');
							}
						}

						echo json_encode($msg);
						exit();

					}
				}
		}
	}

	public function emailregister()
	{
		//$this->load->helper('recaptcha');

		$this->output->set_content_type('application/json');
		$msg = array('message' => 'Registrasi gagal!', 'code' => 'error');

		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
			if(isset($_POST['email']) /*and isset($_POST['passwd1']) and isset($_POST['passwd2'])*/ )
			{
				$email   = $_POST['email'];
				$passwd  = $_POST['passwd'];
				$fname   = $_POST['fname'];
				$lname   = $_POST['lname'];

				$this->load->model('Member_model');

				$exi = 0;
                $exi = $this->home_lib->checkEmail($email);

				if($exi > 0){
					//$msg = array('message' => 'email');
                    $msg = array('message' => 'Maaf, Terjadi kesalahan.');

                    $msg['message'] = "Registrasi gagal, email sudah terdaftar.";
                    $msg['code'] = 'error';
				} else {

				$max     = 14;
                //$passwd  = $this->generateRandomString(6);
				$salt    = $this->_generateSalt($max);
				$sec_pwd = $this->_generatePassword($passwd, $salt);
				$acid    = sha1( uniqid() );
				$uid     = trim(strtolower($fname)) . trim(strtolower($lname)) . uniqid();

				$data = array(
					'account_id'        => $acid,
					'user_id'           => $uid,
					'account_type'      => 0,
					'passwd'            => $sec_pwd,
					'registration_date' => mysql_datetime(),
					'email'             => $email,
					'fname'             => $fname,
					'lname'             => $lname,
					'display_name'      => $fname .' '.$lname,
					'signup_from'       => '1',
					'salt'              => $salt
				);

				$result = 1;
                $result = $this->Member_model->saveMember($data);

				if ($result > 0) {

					$this->load->config();
					$this->config->load('mail');
					$mail_config = $this->config->item('mail');
					$this->load->library('email', $mail_config);
					$this->email->set_newline("\r\n");
					$this->email->from('noreply.community@bijaks.net', 'Bijak Admin');
					$this->email->to($email);

					$this->email->subject('Bijak Registrasi : Email Konfirmasi');

					$mail_data['email'] = $email;
					$mail_data['account_id']   = $acid;
                    //$this->email->message($this->load->view('email/email_registrasi', $mail_data, true));
                    //$is_sent = $this->email->send();


/* Approval Login for new members */
                    $id = $mail_data['account_id'];

                    $rd_s = $this->generateRandomString(6);
                    $max = 14;
                    $salt = $this->_generateSalt($max);

                    $sec_pwd = $this->_generatePassword($passwd, $salt);

                    $where = array('account_id' => $id);
                    $data = array(
                        'passwd' => $sec_pwd,
                        'account_status' => 1,
                        'activation_date' => mysql_datetime(),
                        'salt' => $salt
                    );

                    $select = 'account_id, user_id, registration_date, email, fname, lname';
                    $users= $this->home_lib->getuser($select, $where)->row_array();
                    //$this->home_lib->updateuser($data, $where);
                    $this->email->set_newline("\r\n");
                    $this->email->from('community@bijaks.net', 'Community Bijak');
                    $this->email->to($email);
                    $this->email->subject('Bijak Registrasi');

                    $mail_data['email']	= $email;
                    $mail_data['account_id'] = $acid;
                    $mail_data['nama'] = $fname.' '.$lname;
                    $mail_data['password'] = $passwd;

                    $this->email->message($this->load->view('email/approve_mail', $mail_data, true));
                    $is_sent = $this->email->send();

                    /* ---- End Approval ---*/
					/*if ($is_sent) {
						$msg = array('message' => "Registrasi berhasil, silahkan check email untuk aktivasi.", 'message_code' => 'success');
					} else {
						 $msg['message'] = "Registrasi gagal, tidak dapat mengirim email.";
					}*/

					$msg['message'] = "Terima kasih atas perhatian dan kesediaan anda untuk menjadi member Bijaks.net.".
                       "Periksa inbox atau junk folder email anda, ikuti petunjuk  untuk proses berikutnya.";
                    $msg['code'] = 'success';

				} else {

					$msg['message'] = "Registrasi gagal, tidak dapat menyimpan data.";
                    $msg['code'] = 'error';
				}
              }
			}
		}
		echo json_encode($msg);
		exit();
	}


	public function logout()
	{
		$this->session->unset_userdata('member');
		redirect(base_url());
	}


	public function forgotpass()
	{
		$member = $this->session->userdata('member');
		$var['member'] = $this->session->userdata('member');
		if(!$member)
		{
			$this->load->helper('recaptcha');
			$this->load->view('forgotpass/forgot_pass_view', $var);
		}
		else
		{
				redirect(base_url().'komunitas/');
		}

	}

   public function forgotPassword()
   {
		$data	= $this->data;
      $data['title'] = "Bijaks | Life & Politics";
      $profile_key = 'profile:list:hot';
      $list_profile = $this->redis_slave->lrange($profile_key, 0, 8);
      $arr_profile = array();
      if(count($list_profile))
      {
        foreach ($list_profile as $key => $value)
        {
          $profile_detail = $this->redis_slave->get('profile:detail:'.$value);
          $arr_profile = @json_decode($profile_detail, true);
          $result[$key] = $arr_profile;
        }
      }
      
      $data['hot_profile'] = $result;

		$data['member'] = $this->session->userdata('member');
		$data['message_title'] = "RESET PASSWORD";
		if($this->input->server('REQUEST_METHOD') === 'POST')
		{
		  if(isset($_POST['email']))
		  {
			  $email = $_POST['email'] ;
			  $this->load->model('Member_model');
			  $member = $this->Member_model->getMemberByEmail_cache_900_row($email);
			  $row = $member;

			  if($row)
			  {
				 $account_id = $row->account_id;
				 $token = sha1( uniqid() );
				 $display_name = $row->fname . ' ' . $row->lname;
				 $this->Member_model->deletePasswordRequest($account_id);
				 $this->Member_model->setPasswordRequest($account_id, $token);

				 $mail_data = array('name' => $display_name, 'token' => $token);

				 $this->load->config();
				 $this->config->load('mail');
				 $mail_config = $this->config->item('mail');
				 $this->load->library('email', $mail_config);
				 $this->email->set_newline("\r\n");
				 $this->email->from('noreply.balepoint@gmail.com', 'Bijak Admin');
				 $this->email->to($email);

				 $this->email->subject('Bijak Password');

				 $mail_data = array('name' => $display_name, 'token' => $token);

				 $this->email->message($this->load->view('email/forgotpass', $mail_data, true));
				 try
				 {
					$this->email->send();
					$data['message'] = 'We have sent the further instruction to reset your password to your email. It might take a while to get to your email inbox.';
               $html['html']['content']  = $this->load->view('forgotpass/tpl_notification', $data, true);
               $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
               $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
               $this->load->view('template/tpl_one_column', $html, false);
				 } catch (Exception $e)  {
					$data['message'] = 'We have problem sending email. This error should never occur. Feel free to blame the developer.';
               $html['html']['content']  = $this->load->view('forgotpass/tpl_notification', $data, true);
               $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
               $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
               $this->load->view('template/tpl_one_column', $html, false);
				 }


			   } else {
				  $data['message'] = 'Invalid email address. Please provide the email you use to signup to www.bijak.com.';

              $html['html']['content']  = $this->load->view('forgotpass/tpl_notification', $data, true);
              $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
              $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
              $this->load->view('template/tpl_one_column', $html, false);
				}
			}
      }   	
   }



	public function resetpassword()
	{
		$data	= $this->data;
      $data['title'] = "Bijaks | Life & Politics";
      $profile_key = 'profile:list:hot';
      $list_profile = $this->redis_slave->lrange($profile_key, 0, 8);
      $arr_profile = array();
      if(count($list_profile))
      {
        foreach ($list_profile as $key => $value)
        {
          $profile_detail = $this->redis_slave->get('profile:detail:'.$value);
          $arr_profile = @json_decode($profile_detail, true);
          $result[$key] = $arr_profile;
        }
      }
      
      $data['hot_profile'] = $result;
		$data['message_title'] = "RESET PASSWORD";

		if(isset($_GET['token']))
		{
			$success = FALSE;
			$token = $_GET['token'];
			$this->load->model('Member_model');
			$query = $this->Member_model->getPasswordRequest($token);
			if($query->num_rows() > 0)
			{
				$row = $query->row();
				if($row->hour_age < 24)  // expires in 24 hours -----
				{					
					$member = $this->Member_model->getMemberByAccountId_cache_900_row($row->account_id);					
					$row2 = $member;										
					$sesdata = array(
						'username'  => $row2->display_name,
						'user_id' => $row2->page_id,
						'email'     => $row2->email,
						'logged_in' => TRUE,
						'account_type' => $row2->account_type, 
						'account_id' => $row2->account_id,
						'profile_id' => $row2->profile_content_id,
//						'xname' => $this->_fototitle($row2->profile_content_id),
						'affiliasi' => $this->Member_model->getUserAffiliasiText($row2->page_id)
					);
					$success = TRUE;

					//$this->session->set_userdata('member', $sesdata);
					$this->session->set_userdata('changepass', TRUE);
					$this->session->set_userdata('account_id', $row2->account_id);					
					$this->Member_model->deletePasswordByToken($token);

               $html['html']['content']  = $this->load->view('forgotpass/change_password', $data, true);
               $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
               $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
               $this->load->view('template/tpl_one_column', $html, false);
				}
			} 
			if(!$success)
			{
				$data['message'] = "Your password reset request is invalid. You may try again.";
            $html['html']['content']  = $this->load->view('forgotpass/tpl_notification', $data, true);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
            $this->load->view('template/tpl_one_column', $html, false);
			}
		}
		else
		{
			redirect(base_url().'komunitas/');
		}
	}

	public function resetpassnewpass()
	{
		$data	= $this->data;
      $data['title'] = "Bijaks | Life & Politics";
      $profile_key = 'profile:list:hot';
      $list_profile = $this->redis_slave->lrange($profile_key, 0, 8);
      $arr_profile = array();
      if(count($list_profile))
      {
        foreach ($list_profile as $key => $value)
        {
          $profile_detail = $this->redis_slave->get('profile:detail:'.$value);
          $arr_profile = @json_decode($profile_detail, true);
          $result[$key] = $arr_profile;
        }
      }
      
      $data['hot_profile'] = $result;
		$data['message_title'] = "RESET PASSWORD";

		$account_id = $this->session->userdata('account_id');
		$changepass = $this->session->userdata('changepass');

		if($account_id && $changepass)
		{
			$this->load->model('Member_model');
			$query = $this->Member_model->getMemberByAccountId_cache_900_row($account_id);
			$row = $query;
			if($row)
			{
				
				$newpassword = $this->input->post('newpasswd1');
				$newpassword2 = $this->input->post('newpasswd2');
				if($newpassword && $newpassword2 && $newpassword == $newpassword2 && $newpassword != '')
				{
					$max = 14;
					$salt = $this->_generateSalt($max);
					
					$this->session->unset_userdata('changepass');
					$this->session->unset_userdata('account_id');
					$this->Member_model->setUserPassword($account_id, $this->_generatePassword($newpassword, $salt), $salt);

					$mail_data = array('name' => $row->display_name); 
																		
					$this->load->config();
					$this->config->load('mail');
					$mail_config = $this->config->item('mail');
					$this->load->library('email', $mail_config);
					$this->email->set_newline("\r\n");
					$this->email->from('noreply.balepoint@gmail.com', 'Bijak Admin');
					$this->email->to($row->email);
			
					$this->email->subject('Bijak Password Changed');
			
			
					$this->email->message($this->load->view('email/pass_changed', $mail_data, true));
					
					try					
					{
						$this->email->send();
					} catch (Exception $e)  {
					}
               $data['message'] = "Your password reset is successful, continue to login.";
               $html['html']['content']  = $this->load->view('forgotpass/tpl_notification', $data, true);
               $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
               $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
               $this->load->view('template/tpl_one_column', $html, false);
				}
				else
				{
					$data['message'] = "Please make sure your password is not empty and confirmed on both fields.";
               $html['html']['content']  = $this->load->view('forgotpass/change_password', $data, true);
               $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
               $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
               $this->load->view('template/tpl_one_column', $html, false);
				}
			}
			else
			{
				redirect(base_url());
			}

		} 
		else
		{
			redirect(base_url());
		}

	}

	public function _fototitle($content_id) {
		
		$query = $this->Photo_model->getContentAttachment($content_id);			
		if(count($query) > 0){
			$row = $query;
			return $row->attachment_title;
			
		} else {
			return 'noimage.gif';
		}
		
	}

	public function _generateSalt($max)
	{
		  $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
		  $i = 0;
		  $salt = "";
		  while ($i < $max) {
				$salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
				$i++;
		  }

		  return $salt;
	}

	public function _generatePassword($pass, $salt)
	{
		return crypt($pass, $salt);
	}

    public function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public function getMemberPhoto($content_id)
	{
		$this->load->model('Photo_model');

		$query = $this->Photo_model->getContentAttachment($content_id);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row->attachment_title;
		} else {
			return 'noimage.gif';
		}
	}

	public function ajaxpopupnewscontent()
	{
		$this->loadSmartCache();
		$id = $this->input->post('id');
		$jsonurl = config_item('news_url')."mobile/get_post/?dev=1&id=".strval($id);


		  $key_string = get_class($this).'::'.__FUNCTION__.'::'.$jsonurl;

		  $key = $this->smartcache_home->generatekey($key_string);

		  if (($json_output = $this->smartcache_home->fetch($key)) === false){
			$ch = curl_init($jsonurl);
				
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_VERBOSE, 1);
				curl_setopt($ch, CURLOPT_HEADER, 1);
				$response = curl_exec($ch);
				
				$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
				$body        = substr($response, $header_size);
				$json_output = json_decode($body);
				
				$this->smartcache_home->store($key, $json_output);
		  }

		$conten 			= $json_output->post->content;
		// dmw: remove image & image container width
		/*
		$conten = preg_replace("@style=\"width: [0-9]+px\"@Us", "", $conten);
		$conten = preg_replace("@width=\"[0-9]+\"@Us", "", $conten);
		$conten = preg_replace("@height=\"[0-9]+\"@Us", "", $conten);
		*/
		$conten = strip_tags($conten, '<p>'); 
		$conten = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $conten); 

		$attachments 	= $json_output->post->attachments;
		$image_url		= '';
		$image_title 	= '';
		$image_caption = '';
		$image_height	= '';
		if (!empty($attachments)) {
			$image_url		= $attachments[0]->url;
			$image_title 	= $attachments[0]->title;
			$image_caption = $attachments[0]->caption;
			$image_height 	= $attachments[0]->images->full->height;
		}
		
		$msg = array(
			'message'			=> 'ok',
			'content' 			=> $conten, 
			'image_url'			=> $image_url,
			'image_title' 		=> $image_title,
			'image_caption' 	=> $image_caption,
			'image_height' 	=> $image_height,
			'category' 			=> $json_output->post->categories[0]->title
		);

		$this->output->set_content_type('application/json');

		$this->output->set_output(json_encode($msg));
	}

	public function loadSmartCache()
	{
		$cache_config = array(
			'container' => 'File',
			'ttl' => 120, // 900 Cache will expired in 15 minutes
			'File' => array(
				 'store' => APPPATH.'cache'.DIRECTORY_SEPARATOR.'controller'.DIRECTORY_SEPARATOR.strtolower(get_class($this)),
				 'auto_clean'      => false,
				 'auto_clean_life' => 900,
				 'auto_clean_all'  => false,
				 'max_estimate_process' =>  2, // Cache process about 2 second
				 'debug'  => false
			)
		);

		$this->load->library('Smartcache',$cache_config,'smartcache_home');
	}

    public function activation()
    {
        if(isset($_GET['uid'])){
            if(!is_null($_GET['uid'])){
                $account_id = $this->input->get('uid', true);

                $member = $this->home_lib->getMemberNotActive($account_id);
                if($member->num_rows() > 0)
                {
                    $row              = $member->row();
                    $account_status   = $row->account_status;
                    $email			   = $row->email;
                    if(intval($account_status) == 0)
                    {
                        $data = array('activation_date' => mysql_datetime() );

                        $this->home_lib->updateAccountStatus($data, $email);
                        $this->register($account_id);
                    }else{
                        redirect(base_url());
                    }
                }else{
                    redirect(base_url());
                }
            }
        }
        //redirect(base_url());
    }

    public function register($uid)
    {
        $data	= $this->data;
        $html['title'] = "Bijaks | Life & Politics";
        $data = $this->home_lib->getMemberByAccount($uid);
        $html['html']['content']  = $this->load->view('template/pages/activation_register', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    public function profileregister()
    {
        $this->output->set_content_type('application/json');
        $msg = array('message' => 'Registrasi gagal!', 'message_code' => 'error');

        if($this->input->server('REQUEST_METHOD') === 'POST')
        {
            if(isset($_POST['fname']) and isset($_POST['lname']) and isset($_POST['gender']) and isset($_POST['dob']))
            {
                $account_id = $_POST['uid'];

                $this->load->model('Member');

                $data = array();
                $data['fname']			   = $_POST['fname'];
                $data['lname']			   = $_POST['lname'];
                $data['display_name']	= $data['fname'].' '.$data['lname'];
                $data['gender']			= $this->input->post('gender');
                $data['birthday']		   = $this->input->post('dob');
                $data['current_city'] 	= '';
                $data['about'] 			= '';
                $data['politisi_id'] 	= '';
                $data['account_id']		= $_POST['uid'];
                $data['user_id']		   = $data['fname'] . $data['lname'] . uniqid();;
                $data['page_id']	      = $data['user_id']	;
                $data['affiliasi'] 		= $this->home_lib->getUserAffiliasiText($data['page_id']);
                $data['account_status'] = '1';


                $result = $result = $this->home_lib->updateMemberStatus($data);

                if ($result > 0) {
                    $msg = array('message' => "Profil anda berhasil diupdate,silahkan mencoba login.", 'message_code' => 'success');
                } else {
                    $msg['message'] = "Profil anda tidak dapat menyimpan data.";
                }

            }
        }

        echo json_encode($msg);
    }

    public function aasort(&$array, $key)
    {
        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii]=$va[$key];
        }
        asort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        $array=$ret;
    }

    public function BlogLatest()
    {
        $this->load->model('content_model');
        $page = $this->uri->segment(3); $limit = 10;
        $offset = ($limit*intval($page)) - $limit;
        $data['blogs'] = $this->content_model->getBlogList($offset, $limit);
        $data['section']    = 'opini';
        return $this->load->view('home/mobile/blog_latest', $data);
    }


}
