<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Home_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->db = $this->CI->load->database('slave', true);
    }

    public function CountLike($where=null)
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'Jumlah'
        FROM tcontent_like tc
        INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
        {$where}
QUERY;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function GetCountLike($content_id = NULL)
    {
        if(!empty($content_id))
        {
            $where = "WHERE tt.content_id = '".$content_id."' ";
            return $this->CountLike($where);
        }
    }

    public function CountDislike($where=null)
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'Jumlah'
        FROM tcontent_dislike tc
        INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
        {$where}
QUERY;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function GetCountDislike($content_id=null)
    {
        if(!empty($content_id))
        {
            $where = "WHERE tt.content_id = '".$content_id."' ";
            return $this->CountDislike($where);
        }
    }

    public function CountComment($where=null)
    {
        $sql = <<<QUERY
        SELECT COUNT(*) AS 'Jumlah'
        FROM tcontent_comment tc
        INNER JOIN tcontent_text tt ON tt.`content_id` = tc.`content_id`
        {$where}
QUERY;
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function GetCountComment($content_id=null)
    {
        if(!empty($content_id))
        {
            $where = "WHERE tt.content_id = '".$content_id."' ";
            return $this->CountComment($where);
        }
    }

    public function getuser($select, $where, $like=array(), $limit=0, $offset=0, $order_by='registration_date desc')
    {
        $this->db->select($select);
        $this->db->where($where);
        if(!empty($like)){
            $this->db->like($like);
        }

        if($limit != 0){
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by($order_by);
        $data = $this->db->get('tusr_account');
        $sr = $this->db->last_query();
        // echo $sr;
        return $data;
    }

    public function getMemberByAccount($account_id)
    {
        $sql = 'select ta.*, p.profile_content_id, p.page_id from tusr_account ta left join tobject_page p on p.page_id = ta.user_id where account_id = ?';
        $query = $this->db->query($sql, array($account_id))->row_array();
        return $query;
    }

    public function updateuser($data, $where)
    {
        $this->db->where($where);
        $this->db->update('tusr_account', $data);
    }

    public function getMemberNotActive($account_id)
    {
        $sql = 'select ta.*, p.profile_content_id, p.page_id, t.attachment_title from tusr_account ta left join
				tobject_page p on p.page_id = ta.user_id left join tcontent_attachment t on t.content_id = p.profile_content_id
				where ta.account_id = ? ';

        $query = $this->db->query($sql, array($account_id));
        return $query;
    }

    public function updateAccountStatus($data, $email)
    {
        $this->db->where('email', $email);
        $this->db->update('tusr_account', $data);
    }

    public function checkEmail($email)
    {
        $sql = "SELECT email FROM tusr_account WHERE email = '$email'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return count($result);
    }

    public function updateMemberStatus($data)
    {
        $result = false;
        $sql  = "UPDATE tusr_account SET
               account_status = ?,
   				display_name = ?,
   				gender = ?,
   				birthday = ?,
   				fname = ?,
   				lname = ?,
   				user_id = ?
               WHERE account_id = ?";

        $query = $this->db->query($sql, array(
                $data['account_status'] ,$data['display_name'], $data['gender'], $data['birthday'],
                $data['fname'],$data['lname'],$data['user_id'], $data['account_id'])
        );

        $result = $this->db->affected_rows();

        $sql  = "Select count(*) count_page from tobject_page where page_id = '".$data['user_id']."'";
        $page = $this->db->query($sql)->row_array();
        if ($page['count_page'] > 0) {
            $sql  = "UPDATE tobject_page SET page_name = '".$data['display_name']."' WHERE page_id = '".$data['user_id']."'";
            $query = $this->db->query($sql);
        } else {
            $sql  = "INSERT INTO tobject_page (page_id, page_name, profile_content_id, entry_date, entry_by, last_update_date, user_page) VALUES (
                 '".$data['user_id']."', '".$data['display_name']."',  NULL, SYSDATE(), '".$data['account_id']."',  SYSDATE(), 1)";
            $query = $this->db->query($sql);
        }
        return $result;
    }

    public function getUserAffiliasiText($user_id) {
        if(! empty($user_id)) {
            $rs = $this->getUserAffiliasi($user_id);
            $str = array();
            foreach($rs as $row) {
                if($row['page_name'] != $row['page_id']) {
                    $str[] = "<a href='" . base_url() . "politisi/index/" . $row['page_id'] . "' title='" . $row['page_name'] . "'>" . $row['page_name'] . "</a>";
                } else {
                    $str[] = "<a title='" . $row['page_name'] . "'>" . $row['page_name'] . "</a>";
                }
            }
            return implode(", ", $str);
        } else {
            return "";
        }
    }

    public function getUserAffiliasi($user_id) {
        if(! empty($user_id)) {
            // TO GET LIST OF POLITISI & PARTAI FROM AFFILIASI USER
            $sql = "SELECT politic_id as page_id FROM `tuser_affiliasi` WHERE user_id = ?";
            $query = $this->db->query($sql, array($user_id))->result_array();
            foreach($query as &$row) {
                $sql = "SELECT page_name, profile_content_id FROM `tobject_page` WHERE page_id = ?";
                $rs = $this->db->query($sql, array($row['page_id']))->result_array();
                if(! empty($rs)) {
                    $row['page_name'] = $rs[0]['page_name'];
                    $row['profile_content_id'] = $rs[0]['profile_content_id'];
                } else {
                    $row['page_name'] = $row['page_id'];
                    $row['profile_content_id'] = "";
                }
            }
            return $query;
        } else {
            return array();
        }
    }

    public function getLink($id,$type){
        if($type == 'i'){
            $select = 'image_link';
        }elseif($type == 't'){
            $select = 'text_link';
        }elseif($type == 'ios'){
            $select = 'ios_link';
        }elseif($type == 'a'){
            $select = 'android_link';
        }else{
            $select = 'android_link';
        }

        $tmp = explode('-', $id);
        $main_id = $tmp[0];
        $now = date('Y-m-d');
        $source = 'mobile';

        $sql = 'SELECT COUNT(*) nb FROM tbanner_hit WHERE banner_id="'.$main_id.'" AND hit_date="'.$now.'"';
        $nb = $this->db->query($sql)->row_array();

        if($nb['nb'] == 0){
            $sql = "INSERT INTO tbanner_hit(`banner_id`,`hit_date`,`".$source."`)VALUES('".$main_id."','".$now."','1')";
        }else{
            $sql = "UPDATE tbanner_hit SET ".$source."=".$source."+1 WHERE banner_id='".$main_id."' AND hit_date='".$now."'";
        };
        $this->db->query($sql);

        # Adding nb_clicks
        // $sql = "UPDATE tcontent_banner SET nb_clicks=nb_clicks+1 WHERE id='".$main_id."'";
        // $proc = $this->db->query($sql);
        
        $sql = "SELECT ".$select." AS link FROM tcontent_banner WHERE id='".$id."'";
        $result = $this->db->query($sql)->result_array();
        return $result;        
    }   


}
