<?php
$isSentimen = FALSE;
$this->load->view($template_path .'tpl_header');
?>

<div id='main'><div id='body' class='page'><div class='container'>

<div class='row row-col'><div class='col-border clearfix'>
    <div class='col col-1-4 col-side' id='col-left'>
        <?php
        include('member/guest_user_badge.php');
        $activePage = "contactInfo";
        include('member/profile_menu.php');
        if($member['account_id'] == $guestmember['account_id'])
            $this->load->view('komunitas/include_member/mini_friend_list.php');
        ?>
    </div>
    <div class='col col-1-2' id='col-main'>
        <div class='block'>
        <!-- ### MAIN ###################################################### -->
    <div class="contentright" id="profile_content_holder"><h1><?php echo $guestmember['username'];?></h1>
    <div class="profile_content_holder">

    <label><?php
        if($guestmember['gender'] == 'M')
            echo 'Laki-laki, ';
        else
            echo 'Perempuan, ';

         if(!empty($guestmember['birthday']))
         {
           $birthDate = explode("-", $guestmember['birthday']); // yyyy-mm-dd
           //get age from date or birthdate
           $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md") ? ((date("Y")-$birthDate[0])-1):(date("Y")-$birthDate[0]));
         } else { $age = 0; }  
         echo $age.' tahun';

    ?></label>
    <div class="clear"></div>
    </div><div class="profileinfo">
    <div class="uiBoxBlue">
        <label>Contact Information</label>
        <div class="makeright">

        </div>
        <div class="clear"></div>
    </div>

    <div class="viewinfo">
        <label>Phone:</label>
        <label name="value"><?php echo $address_info['phone'];?></label>
    </div>
    <div class="viewinfo">
        <label>Address:</label>
        <label name="value"><?php echo $address_info['address'];?></label>
    </div>
    <div class="viewinfo">
        <label>City/Town:</label>
        <label name="value"><?php echo $address_info['city_state'];?></label>
    </div>
    <div class="viewinfo">
        <label>Province/State:</label>
        <label name="value"><?php echo $address_info['province_state'];?></label>
    </div>
    <div class="viewinfo">
        <label>Country:</label>
        <label name="value"><?php echo $address_info['country'];?></label>
    </div>
    <div class="viewinfo">
        <label>Zip code:</label>
        <label name="value"><?php echo $address_info['postalcode'];?></label>
    </div>

    </div>
    </div>

    <?php if($member['account_id'] == $guestmember['account_id']):?>
    <div class="clear"></div>
    <div class="viewinfo"><a class="btn" id="editbutton">Edit</a></div>

    <div id="editform" class="hide">
    <h1>Informasi Kontak</h1>
    <form id="theform4update" name="theform4update" method="POST" action="<?php echo base_url().'komunitas/profileupdatecontact'?>">
    <div class="viewinfo">
        <label for="fname">Telepon:</label>
        <input type="text" name="phone" id="phone" size="40" value="<?php echo $address_info['phone'];?>">
    </div>
    <div class="viewinfo">
        <label for="gender">Alamat:</label>
        <textarea name="address" id="address" rows="3" cols="50"><?php echo $address_info['address'];?></textarea>

    </div>
    <div class="viewinfo">
        <label for="fname">Kota:</label>
        <input type="text" name="city_state" id="city_state" size="40" value="<?php echo $address_info['city_state'];?>">
    </div>
    <div class="viewinfo">
        <label for="fname">Propinsi:</label>
        <input type="text" name="province_state" id="province_state" size="40" value="<?php echo $address_info['province_state'];?>">
    </div>
    <div class="viewinfo">
        <label for="fname">Negara:</label>
        <input type="text" name="country" id="country" size="40" value="<?php echo $address_info['country'];?>">
    </div>
    <div class="viewinfo">
        <label for="fname">Kode pos:</label>
        <input type="text" name="postalcode" id="postalcode" size="40" value="<?php echo $address_info['postalcode'];?>">
    </div>

    <div class="viewinfo">
                <input type="button" class="btn" id="btnCancel" value="Cancel">
                <input type="submit" class="btn" id="btnSave" value="Save">
    </div>
    </form>

    </div>

    <script>
        $('#editbutton').click(function(){
            $('#editbutton').hide();
            $('#profile_content_holder').fadeOut('normal', function(){
                $('#editform').fadeIn('normal');
            });
        });
        $('#btnCancel').click(function(){
            $('#editform').fadeOut('normal', function(){
                $('#profile_content_holder').fadeIn('normal', function(){ $('#editbutton').show(); });
            });
        });
        $('#birthday').datepicker({
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            yearRange: "-80:-12",
            defaultDate: "<?php echo $guestmember['birthday'];?>"
        });

    </script>

    <?php endif;?>
        <!-- ### MAIN ###################################################### -->
        </div>
    </div>
    <div class='col col-1-4 col-side' id='col-rite'>
        <div class='block'>
        <?php
        include('member/favourite_politician_view.php');
        ?>
        </div>
    </div>
</div></div>

</div></div></div><!-- #main -->

<?php
$this->load->view($template_path .'tpl_footer');
?>