<?php if(count($follow_or_follower) > 0): ?>
<?php if($tab == '') $tab = 'politisi'; ?>
	<script>
	$(document).ready(function(){
        
	    $('#travel-pill-menu li').each(function() {
	        x = $(this).attr('data-target');
            
            if($(this).hasClass('active'))
            {
                $('#' + x).show();
            } else {
                
			     $('#' + x).hide();
            }
        
		});
        
        
		$('#travel-pill-menu li').click(function() {
			$(this).siblings().removeClass('active');
			$(this).addClass('active');
			t = $(this).attr('data-target');
			if(t == 'all') {
				$('.post-list').slideDown();
				}
			else {
				$('.post-list').slideUp(function() {
					$('#' + t).show();
				});
			}
		});

    });
	</script>
	<ul id="travel-pill-menu" class="btn-group" style="margin-left:0px !important; width:365px;">                       
		<li class="btn <?php if(isset($tab) && $tab == "politisi") { echo "active"; } ?>" data-target="follow-politisi"  style="width:33%;">Politisi / Lembaga</li>
    	<li class="btn" data-target="follow-follower" style="width:33%;">Follower</li>
    	<li class="btn" data-target="follow-follow" style="width:33%;">Follow</li>
	</ul>
        
    <?php
    
        $politisi_html = '';
        $follow_html = '';
        $follower_html = '';
        
        foreach($follow_or_follower as $k => $v)
        {
            if($v['user_type'] == 'politisi')
            {
                $politisi_html .= '<div class="row-fluid">';
                $politisi_html .= '<div class="cnt_img_small">';
                $politisi_html .= '<img src="'.icon_url($v['attachment_title'],'politisi/'.$v['page_id']).'" />';
                $politisi_html .= '</div>';
                
                $politisi_html .= '<div class="cnt-text-fix">';
                $politisi_html .= '<h4 class="no-margin" style="padding:0; margin:0;"><a href="'. base_url() .'politisi/index/'.$v['page_id'].'">'.$v['page_name'].'</a></h4>';
                $politisi_html .= '<div class="content_'.$v['page_id'].'" style="display:inline;"><small class="pid_'.$v['page_id'].'">
</small></div>';
                $politisi_html .= '</div>';
                
                $politisi_html .= '</div>';
                $politisi_html .= '<hr class="more-space" />';
            }
            
            if($v['user_type'] == 'user' && $v['type'] == 'follow')
            {
                $follow_html .= '<div class="row-fluid">';
                $follow_html .= '<div class="cnt_img_small">';
                $follow_html .= '<img src="'.icon_url($v['attachment_title'],'user/'.$v['page_id']).'" />';
                $follow_html .= '</div>';
                
                $follow_html .= '<div class="cnt-text-fix">';
                $follow_html .= '<h4 class="no-margin"><a href="'. base_url() .'comunity/profile?uid='.$v['account_id'].'">'.$v['page_name'].'</a></h4>';
                
                $follow_html .= '</div>';
                
                $follow_html .= '</div>';
                $follow_html .= '<hr class="more-space" />';
            }
            
            if($v['user_type'] == 'user' && $v['type'] == 'follower')
            {
                $follower_html .= '<div class="row-fluid">';
                $follower_html .= '<div class="cnt_img_small">';
                $follower_html .= '<img src="'.icon_url($v['attachment_title'],'user/'.$v['page_id']).'" />';
                $follower_html .= '</div>';
                
                $follower_html .= '<div class="cnt-text-fix">';
                
                $follower_html .= '<h4 class="no-margin"><a href="'. base_url() .'comunity/profile?uid='.$v['account_id'].'">'.$v['page_name'].'</a></h4>';
                
                $follower_html .= '</div>';
                
                $follower_html .= '</div>';
                $follower_html .= '<hr class="more-space" />';
            }
            
        }
    ?>
    
    
    
      	  
  	  	<div id='follow-politisi' class='post-list '> 
		<div class="cnt-text-fix"><h3>Politisi / Lembaga</h3></div>
            <?php echo $politisi_html; ?>
        </div>
      	  
  	  	<div id='follow-follower' class='post-list '> 
		<div class="cnt-text-fix"><h3>Follower</h3></div>
            <?php echo $follow_html; ?>
        </div>
      	  
  	  	<div id='follow-follow' class='post-list '> 
		<div class="cnt-text-fix"><h3>Follow</h3></div>
            <?php echo $follower_html; ?>
        </div>
    
    <?php // var_dump($follow_or_follower); ?>
    
<?php endif; ?>