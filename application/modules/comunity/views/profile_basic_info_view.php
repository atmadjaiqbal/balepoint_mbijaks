<?php
$isSentimen = FALSE;
$this->load->view($template_path .'tpl_header');
?>

<div id='main'><div id='body' class='page'><div class='container'>

<div class='row row-col'><div class='col-border clearfix'>
	<div class='col col-1-4 col-side' id='col-left'>
		<?php
		$this->load->view('comunity/member/include_ajax');
		$this->load->view('comunity/member/guest_user_badge');
		$data['activePage'] = 'basicInfo';
		$this->load->view('comunity/member/profile_menu', $data);
		if($member['account_id'] == $guestmember['account_id'])
			$this->load->view('komunitas/include_member/mini_friend_list');
		?>
	</div>
	<div class='col col-1-2' id='col-main'>
		<div class='block'>
		<!-- ### MAIN ###################################################### -->
<!-- -->
	<span class="pull-right" id="friendshipcontroll">
    <?php if($member['account_id'] != $guestmember['account_id']):?>
        <?php echo $count_profile; ?>
    <?php endif; ?>
	</span>

	<h2 class="no-margin"><?php echo $guestmember['username']; ?></h2>
	<div class="profileinfo">
	<label><?php
		if($guestmember['gender'] == 'M')
			echo 'Laki-laki, ';
		else
			echo 'Perempuan, ';

   	//get age from date or birthdate
		 $birthDate = explode("-", $guestmember['birthday']); // yyyy-mm-dd
		 if (!empty($birthDate[0])) {
   		 $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md") ? ((date("Y")-$birthDate[0])-1):(date("Y")-$birthDate[0]));
   	 } else {
   	   $age = ''; 
   	 }
		 echo $age.' tahun';

	?></label>
	<div class="clear"></div>
	</div>
	<div id="profile_content_holder" <?php echo ($isEdit) ? ' class="hide"' : '';?>>
		<div class="profileinfo">
			<div class="uiBoxBlue">
				<!-- <h3>Informasi Dasar</h3> -->
				<div class="makeright">

				</div>
				<div class="clear"></div>
			</div>

			<table class='table-h'>
				<tr><th>Nama</th><td class='colon'>:</td><td><?=$guestmember['username']?></td></tr>
				<tr><th>Jenis&nbsp;Kelamin</th><td class='colon'>:</td><td><?= $guestmember['gender'] == 'M' ? 'Laki-laki' : 'Perempuan'?></td></tr>
				<tr><th>Tanggal&nbsp;lahir</th><td class='colon'>:</td><td><?=date('F j, Y', strtotime($guestmember['birthday']))?></td></tr>
				<tr><th>Kota</th><td class='colon'>:</td><td><?=$guestmember['current_city']?></td></tr>
				<tr><th>Tentang&nbsp;saya</th><td class='colon'>:</td><td><?=textwrap($guestmember['about'])?></td></tr>
				<!-- <tr><th>Affiliasi</th><td class='colon'>:</td><td><? //$guestmember['affiliasi']; ?>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				</td></tr> -->
			</table>

		</div>

	</div>

<?php if($member['account_id'] == $guestmember['account_id']):?>
	<div class="clear"></div>
	<div class="viewinfo"  <?php echo ($isEdit) ? ' class="hide" ' : '';?>>
	   <a class="btn <?php echo ($isEdit) ? ' hide ' : '';?>" id="editbutton">Edit</a>
	</div>

	<div id="editform" <?php echo ($isEdit) ? '' : ' class="hide"';?>>
	<h3>Informasi Dasar</h3>
	<form id="theform4update" name="theform4update" method="POST" action="<?php echo base_url().'komunitas/profileupdatebasic'?>">

	<div class="viewinfo">
		<label for="fname">Nama depan:</label>
		<input style="width:80%" type="text" name="fname" id="fname" size="40" value="<?php echo $guestmember['fname'];?>">
	</div>
	<div class="viewinfo">
		<label for="fname">Nama belakang:</label>
		<input style="width:80%" type="text" name="lname" id="lname" size="40" value="<?php echo $guestmember['lname'];?>">
	</div>
	<div class="viewinfo">
		<label for="gender">Jenis kelamin:</label>
		<select name="gender" id="gender">
			<option value="M" <?php if($guestmember['gender'] == 'M') echo 'selected="selected"';?>>Laki-laki &nbsp;</option>
			<option value="F" <?php if($guestmember['gender'] != 'M') echo 'selected="selected"';?>>Perempuan &nbsp;</option>
		</select>
	</div>
	<div class="viewinfo">
		<label for="current_city">Tanggal lahir:</label>
		<input style="width:80%" type="text" name="birthday" id="birthday" size="40" value="<?php echo $guestmember['birthday'];?>">
	</div>
	<div class="viewinfo">
		<label for="current_city">Kota:</label>
		<input style="width:80%" type="text" name="current_city" id="current_city" size="40" value="<?php echo $guestmember['current_city'];?>">
	</div>
	<div class="viewinfo">
		<label for="about">Tentang saya:</label>
		<textarea style="width:80%" name="about" id="about" rows="3" cols="50"><?php echo textwrap($guestmember['about']);?></textarea>
	</div>
	<!-- <div class="viewinfo">
		<label for="about">Affiliasi:</label>

		<select id="politik_select" name="politik_select"></select>
		<a title="Tambah Politisi" id="tambah_politisi_button"><i class="icon-plus-sign"></i></a>
		<div id="form_tambah_politisi">
			Yang lain:
			<input class="input-short" id="input_politisi" type="text" name="politisi_add">
			<a title="Simpan Politisi" id="simpan_politisi"><i class="icon-ok-sign"></i></a>
		</div>
		<div id="politisi_value"></div>
	</div> -->

	<div class="viewinfo">
				<input type="button" class="btn" id="btnCancel" value="Cancel">
				<input type="submit" class="btn" id="btnSave" value="Save">
	</div>
	</form>

	</div>

	<script>



		$('#editbutton').click(function(){
			$('#editbutton').hide();
			$('#profile_content_holder').fadeOut('normal', function(){
				$('#editform').fadeIn('normal');
			});
		});
		$('#btnCancel').click(function(){
			$('#editform').fadeOut('normal', function(){
				$('#profile_content_holder').fadeIn('normal', function(){ $('#editbutton').show(); });
			});
		});
		$('#birthday').datepicker({
			dateFormat: 'yy-mm-dd',
			changeYear: true,
			changeMonth: true,
			yearRange: "-80:-12",
			defaultDate: "<?php echo $guestmember['birthday'];?>"
		});

	
	</script>

	<?php endif ;?>
		<!-- ### MAIN ###################################################### -->
		</div>
	</div>
	<div class='col col-1-4 col-side' id='col-rite'>
		<div class='block'>
		<?php
		include('member/favourite_politician_view.php');
		?>
		</div>
	</div>
</div></div>

</div></div></div><!-- #main -->

<div class="modal hide" id="modaldialog" style="width:460px;">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">x</button>
	<h3>Kirim Friend Request</h3>
  </div>
  <div class="modal-body" style="width:411px;">
	<form>
		Input pesan permintaan berteman:<br>
		<input style="width:80%" type="text" id="prompt_text" value="Halo, saya <?php echo $member['username']?>"/>
		<div> <span class="pull-right">
			<a class="btn" id="dialog_okbutton">Ok</a>
			<a class="btn" id="cancelbutton">Cancel</a>
			</span></div>
	</form>
  </div>
</div>

<script>
$('#cancelbutton').click(function(){
	$('#modaldialog').modal('hide');
});
</script>

<?php $this->load->view($template_path .'tpl_footer');?>
