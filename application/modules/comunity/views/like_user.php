<?php
$isSentimen = FALSE;
$this->load->view($template_path .'tpl_header');
?>
<div id='main'><div id='body' class='page'><div class='container'>
 <div class='row row-col'><div class='col-border clearfix'>
	<div class='col col-1-4 col-side' id='col-left'>
		<?php
		$this->load->view('comunity/member/include_ajax');
		$this->load->view('comunity/member/user_badge');
		$data['activePage'] = 'friend_request';
		$this->load->view('comunity/member/komunitas_menu', $data);
		$this->load->view('komunitas/include_member/mini_friend_list');
		?>
	</div>
	<div class='col col-1-2' id='col-main'>
		<div class='block'>
      <h2>Likes</h2>
      <div style="border-bottom: 1px solid #DDD; height:20px;margin:0 0 1em 0;"></div>
      <div style="padding-top:0;">
          <?php $this->load->view('comunity/like_user_list'); ?>      	
					<?php if ($UserLike['total_rows'] > 10 && $load_more): ?>
						<div id="loadmore" class=""></div>
						<div class="row-fluid load-more-loader"></div>
						<div class="row-fluid load-more-wrapper">
							<a id="read-loadmore" data-url="<?php echo base_url() .'comunity/userlikemore/';?>" data-page="<?php echo $page;?>">Load More</a>
						</div>
					<?php endif; ?>
      </div>        
		</div>
  </div>  				
	<div class='col col-1-4 col-side' id='col-rite'>
		<div class='block'>
			<?php
			$this->load->view('comunity/member/favourite_politician_view.php');
			?>
		</div>
	</div>
</div></div>

</div></div></div><!-- #main -->

<?php $this->load->view($template_path .'tpl_footer');?>
