<?php
$isSentimen = FALSE;
$this->load->view($template_path .'tpl_header');
?>

    <div id='main'><div id='body' class='page'><div class='container'>

        <div class='row row-col'><div class='col-border clearfix'>
           <div class='col col-1-4 col-side' id='col-left'>
           <?php
              $this->load->view('comunity/member/include_ajax');
              $this->load->view('comunity/member/guest_user_badge');
              $this->load->view('comnunity/member/profile_menu');
              $this->load->view('komunitas/include_member/mini_friend_list');
           ?>
           </div>
           <div class='col col-1-2' id='col-main'>
              <div class='block'>
                 <!-- -->
                 <?php if($content['account_id'] == $member['account_id'] || $content['flag'] == 1) { ?>
                 <?php if($content['account_id'] == $member['account_id']): ?>
                 <span class="pull-right">
		  	       <a id="removebutton_<?php echo $content['content_id']; ?>"><i class="icon-remove-sign"></i></a>
		         </span>
                 <?php endif;?>

                 <h2><?php echo $content['title'];?></h2>
                 <span style="float: right;padding:15px 0 0 0;" class="content_<?php echo $content['content_id'];?>">
			       <?php echo $content_count; ?>
		         </span>

                 <div class="comment-content" style="padding-left: 5px; padding-right: 5px; margin-bottom:5px;">
                    <a href="<?php echo base_url() . "komunitas/wall?uid=".$content['account_id'];?>">
                       <img src='/public/img/loading.gif' data-src="<?php echo badge_url($content['author_attachment'],'user/'.$member['user_id']) ; ?>"
                            style="width:46px; height:46px;padding:0px 5px 0px 0px;background-color: #FFFFFF;" />
                    </a>
                    <b><?php echo $content['author'];?></b>
                 </div>

                 <script>
                    <?php if($content['account_id'] == $member['account_id']):?>
                      $("#removebutton_<?php echo $content['content_id'];?>").click(function()
                      {
                        if(confirm("Hapus posting?"))
                        {
                           $.ajax({
                             url: "<?php echo base_url().'komunitas/ajaxremovepost'?>",
                             type: "post",
                             data: {id: "<?php echo $content['content_id']?>"},
                             success: function(response, textStatus, jqXHR){
                               $('#post_<?php echo $content['content_id']?>').slideUp('slow', function() { $(this).remove(); } );
                                    window.location = "<?php echo base_url().'blog';?>";
                               },
                                  error: function(jqXHR, textStatus, errorThrown){
                                  alert(
                                    "The following error occured: "+
                                    textStatus, errorThrown
                                  );
                               },
                               complete: function(){
                             }
                           });
                        }
                      });
                    <?php endif;?>
                 </script>
                 <div><?php echo $content['content_blog']; ?></div>
                 <hr class='more-space' />
                 <div class="more-space" style="font-weight: bolder; font-size:12px;">Beri komentar:</div>
                 <div id="myModalFooter"></div>
                 <script>
                    $('#myModalFooter').load('<?php echo base_url(); ?>ajax/comment_load/', { 'id': '<?php echo $content['content_id']?>' });
                 </script>

                 <?php } ?>
              </div>

           </div>
           <div class='col col-1-4 col-side' id='col-rite'>

               <div class='block'>
                   <?php include('include_member/favourite_politician_view.php');  ?>
               </div>
           </div>

            </div>
        </div>


            </div>
        </div>
    </div><!-- #main -->

<?php
$this->load->view($template_path .'tpl_footer');
?>