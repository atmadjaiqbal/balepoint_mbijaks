<?php
$isSentimen = FALSE;
$this->load->view($template_path .'tpl_header');
?>
<div id='main'><div id='body' class='page'><div class='container'>
 <div class='row row-col'><div class='col-border clearfix'>
	<div class='col col-1-4 col-side' id='col-left'>
		<?php
		$this->load->view('comunity/member/include_ajax');
		$this->load->view('comunity/member/user_badge');
		$data['activePage'] = 'member_group';
		$this->load->view('comunity/member/komunitas_menu', $data);
		$this->load->view('komunitas/include_member/mini_friend_list');
		?>
	</div>
	<div class='col col-1-2' id='col-main'>
		<div class='block'>
      <h3 id='group_title' style="display:block; border-bottom: solid #C1C1C1 0.6px; margin-bottom: 8px;"><?php echo $action == 'edit' ? 'Manage' : 'Create';?> Group</h3>
      <div class="message_top" id="msg" style="display:none"><b></b></div>
      <div style="border-bottom: 1px solid #DDD; height:20px;margin:0 0 1em 0;"></div>              
      <div style="padding-top:0;">
        <form id="frmGroupAdd" name="frmGroupAdd" method="POST" action="<?php echo base_url(); ?>comunity/savegroup">
				<?php if($action == 'edit'):?>
					<input type="hidden" value="<?php echo $content['content_id']; ?>" name="id" />
				<?php endif;?>
    		<div class="content">
         <div>
           <label for="txtgroupname" style="float:none;display:block">Nama Group</label>
           <input class="input-long" type="text" name="groupname" id="groupname" size="35" autocomplete="off" placeholder="type your group name">
         </div>
         <div style="margin-top:20px;">
           <label style="float:none;display:block">Group Type</label>
           <input type="radio" name="grouptype" id="opengroup" value="open">&nbsp;Open Group&nbsp;&nbsp;
           <input type="radio" name="grouptype" id="closegroup" value="closed">&nbsp;Close Group
         </div>
         <div style="margin-top:20px;">
           <label for="txtdeskripsi" style="float:none;display:block">Deskripsi</label>
           <textarea name="description" id="description" rows="8" cols="50" placeholder="type your group description"></textarea>
         </div>
         <div class="makeright">
           <input type="submit" class="btn savegroup" value="Submit">
         </div>      		
      	</div>
        </form>      	
      </div>
      <div style="border-bottom: 1px solid #DDD; height:20px;margin:0 0 1em 0;"></div>              
      <h2>Tambah Moderator</h2>
      <div style="padding-top:0;">
        <form id="frmAddModerator" name="frmAddModerator" method="POST">
    		<div class="content">
         <div style="float:left;">
           <input style="float:left;" type="text" name="moderatorname" id="txtmoderator" size="35" placeholder="Pilih moderator untuk group ini">
           <input stype="float:left;" type="submit" class="btn" id="btnSave" value="Tambah">
         </div>
        </div>
        </form>                  
      </div>         
      <h2>Moderator</h2>
      <div style="padding-top:0;">

      </div>                 
		</div>
  </div>
					
	<div class='col col-1-4 col-side' id='col-rite'>
		<div class='block'>
			<?php
			$this->load->view('comunity/member/favourite_politician_view.php');
			?>
		</div>
	</div>
	<script src="<?php echo base_url() ?>public/script/jquery/jquery.validate.min.js" ></script>	
	<script>
    $(document).ready(function() {

			$('#frmGroupAdd').validate({
				  rules: {
						groupname: "required",
						grouptype: "required",
						description: "required"
					},
					messages: {
						groupname: "<b>*</b>",
						grouptype: "<b>*</b>",
						description: "<b>*</b>"
					},
					errorPlacement: function(error, element) {
						error.css('color', 'red');
						albe = element.prev();
						offset = albe.offset();
						error.css('top', offset.top);
						error.css('position', 'absolute');
						error.css('left', offset.left + albe.children('b').outerWidth() + 8);
						error.css('width','250px');
						error.insertAfter(element);
					},
					submitHandler: function(form) {
						var options = {
							method: 'POST', dataType: 'json',
							success: function(data) {
						   console.log(data.message);
 							 if(data.message != 'OK'){
									$('.message_top b').text('');

									$('.message_top b').append(data.message);

									$('.message_top').show();
									goToByScroll('group_title');

							 } else {
									clear_form_elements('#blog_form');
									$('#groupname').text('');
									$('#grouptype').text('');
									$('#description').text('');

									$('.message_top b').text('');

									$('.message_top b').append('Group telah berhasil di update.');

									$('.message_top').show();

									goToByScroll('group_title');

                  setTimeout(function(){location.href=blogURI},3000);
			  			 }

							 return false;
							}
						};
						$(form).ajaxSubmit(options);
					  return false;
					}
			}); 
      
      
    });
  </script>
    
</div></div>

</div></div></div><!-- #main -->

<?php $this->load->view($template_path .'tpl_footer');?>

