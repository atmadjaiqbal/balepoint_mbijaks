<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Caleg extends Application
{
    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->helper('m_date');

        $this->load->helper('text');
        $this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');

        $this->load->library('aktor/aktor_lib');
        $this->load->library('caleg_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->library('suksesi/suksesi_lib');
    }

    public function index()
    {
        $province_list = $this->caleg_lib->get_province();
        $data['province'] = $province_list->result_array();

        $data['head_title'] = 'Caleg 2014';

        $html['html']['content'] = $this->load->view('mobile/caleg_index', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    private function detail($province='', $province_id='0', $dapil='', $dapil_id='0')
    {
        $province = urldecode($province);
        $dapil = urldecode($dapil);
        $where = array('province_id' => intval($province_id), 'dapil_id' => intval($dapil_id));
        $dapil_area = $this->caleg_lib->get_master_dapil($select='*', $where);

        $data['area'] = '';
        if($dapil_area->num_rows() > 0){
            $dapil_area_row = $dapil_area->row_array(0);
            $data['area'] = $dapil_area_row['area'];
        }

        $where = array('province_id' => intval($province_id));
        $pro_issue = $this->caleg_lib->get_issue($select='*', $where, $limit=0, $offset=0);
        $data['issue'] = $pro_issue->result_array();

        $dt = $this->caleg_lib->get_partai_byid($province_id);
        $wheres=array('province_id' => $province_id);
        $pro = $this->caleg_lib->get_province($select='*', $wheres, 1, 0);
        $data['kursi'] = '0';
        if($pro->num_rows() > 0){
            $data_kursi = $pro->row_array();
            $data['kursi'] = $data_kursi['total_kursi'];
        }

        $result = array();
        foreach($dt->result_array() as $row=>$val){
            $dt = $this->caleg_lib->get_calegs($province, $dapil, $val['partai_id']);
            $val['kandidat'] = $dt->num_rows();
            $val['img_url'] = badge_url($val['attachment_title'], 'politisi/'.$val['partai_id']);
            $result[$row] = $val;
        }
        $data['result'] = $result;
        $data['province'] = urldecode($province);
        $data['province_id'] = $province_id;
        $data['dapil'] = urldecode($dapil);

        $this->load->view('detail', $data);

    }

    public function get_dapil()
    {
        $province_id = $this->input->post('province_id', true);
        $province = $this->input->post('province', true);

        $where = array('province_id' => intval($province_id));
        $dt = $this->caleg_lib->get_master_dapil('*', $where);

//        $data['rcode'] = 'ok';
//        if($dt->num_rows() == 0){
//            $data['rcode'] = 'bad';
//        }
        $data['province'] = $province;
        $data['province_id'] = $province_id;
        $data['result'] = $dt->result_array();
        header("Access-Control-Allow-Origin: *");
        $this->load->view('mobile/dapil_list', $data);

//        $this->output->set_content_type('application/json');
//        $this->output->set_output(json_encode($data));

    }

    public function get_partai()
    {
        $province = $this->input->post('province', true);
        $dapil = $this->input->post('dapil', true);

        $dt = $this->caleg_lib->get_partai_byid();

        $result = array();
        foreach($dt->result_array() as $row=>$val){
            $val['img_url'] = badge_url($val['attachment_title'], 'politisi/'.$val['page_id']);

            $where = array('page_id' => $val['page_id']);
            $skandal_terkait = $this->caleg_lib->get_scandal_player($select='count(*) as cou', $where)->row_array(0);
            $val['scandal_count'] = $skandal_terkait['cou'];

            $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
            $val['news_count'] = $news_terkait['cou'];

            $result[$row] = $val;
        }

        
        $data['result'] = $result;
        $data['province'] = $province;
        $data['dapil'] = $dapil;

        // $this->output->set_content_type('application/json');
        // $this->output->set_output(json_encode($data));

        $this->load->view('mobile/partai_list', $data);

    }

    public function get_caleg()
    {
        $province = $this->input->post('province', true);
        $dapil = $this->input->post('dapil', true);
        $partai = $this->input->post('partai', true);
        
        $caleg_redis = $this->redis_slave->get('caleg:'.$province.':'.$dapil.':'.$partai);
        $caleg = @json_decode($caleg_redis, true);
            
        $result_caleg = array();
        foreach($caleg as $row=>$val){
            $val['caleg_img_url'] = badge_url($val['attachment_title'], 'politisi/'.$val['caleg_id']);
            // $val['partai_img_url'] = badge_url($val['partai_attachment_title'], 'politisi/'.$val['partai_id']);

            /** get profile */
            $profile = $this->redis_slave->get('profile:detail:'.$val['caleg_id']);
            $profiles = @json_decode($profile, true);
            $val['profile'] = $profiles;


            $result_caleg[$row] = $val;
        }

        $data['result'] = $result_caleg;
        $data['province'] = $province;
        $data['dapil'] = $dapil;

        // header("Access-Control-Allow-Origin: *");
        $this->load->view('mobile/caleg_list', $data);

    }
}