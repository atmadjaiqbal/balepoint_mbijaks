

<?php foreach ($result as $caleg) { 
    $pp = (count($caleg['profile']['profile_photo']) > 0) ? $caleg['profile']['profile_photo'][0]['badge_url'] : base_url('assets/images/badge/no-image-politisi.png');
  ?>
    <div class="row">
      <div class="col-xs-2 nopadding">
        <img class="img-media-list" src="<?php echo $pp;?>" >
      </div>
      <div class="col-xs-10">
        <a href="<?php echo base_url(). 'aktor/profile/' .$caleg['caleg_id'];?>"><h4 class="media-heading"><?php echo $caleg['caleg'];?></h4></a> 
        <a href="<?php echo base_url();?>aktor/scandals/<?=$caleg['caleg_id'];?>" class="labes labes-scandal"><?php echo $caleg['scandal_count'];?> Skandal</a>
        <a href="<?php echo base_url();?>aktor/news/<?=$caleg['caleg_id'];?>" class="labes labes-news"><?php echo $caleg['news_count'];?> Berita</a>
      </div>
    </div>
    <hr class="line-mini">
<?php } ?>

        
