
<div class="panel-group" id="accordion">
  
  <?php foreach ($result as $key => $value) { ?>
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h6 class="panel-title panel-title-xs">
          <a class="partai-list" data-id="<?php echo $value['page_id'];?>" data-dapil="<?php echo $dapil;?>" data-province="<?php echo $province;?>" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $value['page_id'];?>">
            <?php echo $value['alias'];?>
          </a>
        </h6>
      </div>
      <div id="collapse-<?php echo $value['page_id'];?>" class="panel-collapse collapse">
        <div class="panel-body panel-body-xs">
            <div class="row">
              <div class="col-xs-2 nopadding">
                <img class="img-media-list" src="<?php echo $value['img_url'];?>" >
              </div>
              <div class="col-xs-10">
                <a href="<?php echo base_url(). 'aktor/profile/' .$value['page_id'];?>"><h4 class="media-heading"><?php echo $value['partai'];?></h4></a> 
                <a href="<?php echo base_url();?>aktor/scandals/<?=$value['page_id'];?>" class="labes labes-scandal"><?php echo $value['scandal_count'];?> Skandal</a>
                <a href="<?php echo base_url();?>aktor/news/<?=$value['page_id'];?>" class="labes labes-news"><?php echo $value['news_count'];?> Berita</a>
              </div>
            </div>
            <hr class="line-mini">
            <img id="loader_<?php echo $value['page_id'];?>_<?php echo $province;?>_<?php echo $dapil;?>" src="<?php echo base_url('assets/images/loading.gif');?>">
            <div id="caleg_<?php echo $value['page_id'];?>_<?php echo $province;?>_<?php echo $dapil;?>">
              

            </div>
        </div>  
      </div>
    </div>

 <?php } ?>
</div>
