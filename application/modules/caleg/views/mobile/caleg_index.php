<div class="well well-sm">
  <form id="form_caleg" class="form" role="form">
    <div class="form-group">
      <label class="" for="province">Provinsi</label>
      <select onchange="dapil_change(this);" class="form-control" id="province" name="province">
        <option value="0">Pilih Provinsi</option>
        <?php foreach ($province as $key => $value) { ?>
          <option value="<?php echo $value['province_id'];?>"><?php echo $value['province'];?></option>
        <?php } ?>
      </select>
    </div>
    <div id="grup_dapil" class="form-group" >
      <label class="" for="dapil">Dapil</label>
      <select class="form-control" id="dapil" name="dapil">
        
      </select>
    </div>
    
    <button id="submit_caleg" type="submit" class="btn btn-default">Cari <img id="loader" src="<?php echo base_url('assets/images/loading.gif');?>"></button>
  </form>
</div>

<ol class="breadcrumb">
  <li><a id="province_bread" href="#"></a></li>
  <li><a id="dapil_bread" href="#"></a></li>
  
</ol>
<div class="row">
  <div id="caleg_list" class="col-xs-12 col-md-12">
    
  </div>
</div>

<script type="text/javascript">
  $(function(){
    $('#grup_dapil').hide();
    $('#loader').hide();
    $('.breadcrumb').hide();

    $('#form_caleg').on('submit', function(ev){
      ev.preventDefault();
      $('#loader').show();
      var dt = $(this).serialize();
      $.post('<?php echo base_url();?>caleg/get_partai', dt, function(dat){
        $('#province_bread').text($('#province option:selected').text());
        $('#dapil_bread').text($('#dapil option:selected').text());
        $('#caleg_list').html(dat);
        $('#loader').hide();
        $('.breadcrumb').show();
      })
      return false;
    })

    $('#caleg_list').on('click', '.partai-list', function(ev){
        ev.preventDefault();
        var province = $(this).data('province');
        var dapil = $(this).data('dapil');
        var partai = $(this).data('id');
        var loader = $('#loader_'+partai+'_'+province+'_'+dapil);
        var caleg_place = $('#caleg_'+partai+'_'+province+'_'+dapil);
        // console.debug(caleg_place);
        if(!caleg_place.html().trim()){
          $.post('<?php echo base_url('caleg/get_caleg');?>', {'province':province, 'dapil':dapil, 'partai':partai}, function(dt){
              caleg_place.html(dt);
              loader.remove();
          });
        }
    })

  })

  function dapil_change(sel){
    if(sel.value != '0'){
      $('#loader').show();
      $.post('<?php echo base_url();?>caleg/get_dapil', {'province_id':sel.value}, function(dt){
        $('#dapil').html(dt);
        $('#grup_dapil').show();
        $('#loader').hide();

      })
    }else{
        $('#grup_dapil').hide();
        $('#dapil').html('');
    }
  }
</script>