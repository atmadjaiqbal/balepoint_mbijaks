<?php foreach($result as $row=>$val){ ?>
    <li class="thumbnail thumbnail-white">
        <a target="_blank" href="<?php echo base_url();?>aktor/profile/<?=$val['caleg_id'];?>">
            <div class="partai-image-mini" style="background: transparent url('<?php echo $val['partai_img_url'];?>') left top no-repeat; background-size: cover;"></div>
            <img class="partai-image-large" width="200" height="200"  src="<?php echo $val['caleg_img_url'];?>">
<!--            <pre>-->
<!--                --><?php //var_dump($val['profile']['profile_photo']); ?>
<!--                --><?php //var_dump($val['profile']['headline_photo']); ?>
<!--                --><?php //var_dump($val['profile']['hotlist_photo']); ?>
<!--            </pre>-->
            <br>
            <?php if(is_array($val['profile'])){?>
            <?php foreach($val['profile']['profile_photo'] as $key=>$value){ ?>
                <?php if($key == 4) break; ?>
                <img class="img-polaroid" src="<?php echo $value['icon_url'];?>">
            <?php } ?>
            <?php } ?>
            <h5 class="caleg-name"><?=$row+1;?>. <a target="_blank" href="<?php echo base_url();?>aktor/profile/<?=$val['caleg_id'];?>"><?=$val['caleg'];?></a></h5>
            <?php
            $tr_birthday = trim($val['birthday']);
            $tr_prestasi = trim($val['prestasi']);
            $tr_about = trim($val['about']);
            ?>
            <?php if(!empty($tr_birthday) || !empty($tr_prestasi) || !empty($tr_about)){ ?>

                <?php $tr_birthday = trim($val['birthday']); if(!empty($tr_birthday)){ ?>
                <p class="caleg-date">
                    Tanggal Lahir : <?php echo mdate('%d %M %Y', strtotime($val['birthday']));?>
                </p>
                <?php } ?>
                <?php if(!empty($tr_about)){ ?>
                <p class="caleg-date">
                    <?php echo character_limiter($val['about'], 50);?>
                </p>
                <?php } ?>
                <?php $tr_prestasi = trim($val['prestasi']); if(!empty($tr_prestasi)){ ?>
                <p class="caleg-date">
                    Prestasi : <?php echo character_limiter($val['prestasi'], 50);?>
                </p>
                <?php } ?>

            <?php } ?>

        </a>
        <a href="<?php echo base_url();?>aktor/scandal/<?=$val['caleg_id'];?>" class="label label-important"><?php echo $val['scandal_count'];?> Skandal</a>
        <a href="<?php echo base_url();?>aktor/news/<?=$val['caleg_id'];?>" class="label label-info"><?php echo $val['news_count'];?> Berita</a>
    </li>
 <?php } ?>
