<?php $this->load->view('header');?>
<!-- ########## CONTENT ########### -->
<!-- Begin page content -->
<div class="container ">
    <div class="container-overlay hide">
        <div id="caleg_loader" class="loader hide"></div>
        <iframe id="graph_pie" width="100%" scrolling="no" frameborder="no" height="100%" name="graph_pie" src=""></iframe>
    </div>
</div>

<div class="container">
    <div class="row-fluid">
        <div class="span12">
            <img id="idn_maps_image" src="<?php echo base_url();?>assets/images/map-id.gif" width="960px" alt="indonesia" usemap="#map_id">

            <map id="map_idn" name="map_id">

                <?php foreach($province as $row=>$val){ ?>
                    <area data-title="<?php echo $val['province'];?>" data-id="<?php echo $val['province_id'];?>" data-kursi="<?php echo $val['total_kursi']; ?>" class="province" shape="poly" coords="<?php echo $val['coord_poly'];?>" title="<?php echo $val['province'];?>" href="#">
                <?php } ?>

            </map>

        </div>
    </div>
</div>


<?php $this->load->view('footer');?>