<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Caleg_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->db = $this->CI->load->database('slave', true);
    }

    function get_caleg($select="*", $where=array(), $group_by)
    {
        $this->db->select($select);
        $this->db->where($where);
        $this->db->group_by($group_by);
    }

    function get_dapil($province)
    {
        $query = "SELECT DISTINCT(dapil) FROM structure_caleg WHERE province = '$province'";
        $dt = $this->db->query($query);
        return $dt;

    }

    function get_partai($province)
    {
        $query = "SELECT p.partai, p.partai_id, tp.`profile_content_id`, tpa.alias,
ta.`attachment_title` FROM structure_caleg AS p
LEFT JOIN tobject_page tp ON tp.`page_id` = p.`partai_id`
LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tp.`profile_content_id`
LEFT JOIN tusr_account tpa on tpa.user_id = tp.page_id
WHERE province = '$province' group by partai_id";

        $dt = $this->db->query($query);
        return $dt;

    }

    function get_partai_byid()
    {
        $query = "select p.*, tp.page_name, tp.`profile_content_id`, tpa.alias,
ta.`attachment_title` from tusr_partai p LEFT JOIN tobject_page tp ON tp.`page_id` = p.`page_id`
LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tp.`profile_content_id`
LEFT JOIN tusr_account tpa on tpa.user_id = tp.page_id order by id_tusr_partai asc limit 0, 12 ";

        $dt = $this->db->query($query);
        return $dt;

    }

    function get_calegs($province, $dapil, $partai_id)
    {
        $query = "SELECT p.partai, p.partai_id, p.caleg, p.caleg_id, tp.`profile_content_id`, ta.`attachment_title`, left(tp.about, 200) as about,
tp2.`profile_content_id` AS partai_profile_content_id, ta2.`attachment_title` AS partai_attachment_title
FROM structure_caleg AS p
LEFT JOIN tobject_page tp ON tp.`page_id` = p.`caleg_id`
LEFT JOIN tcontent_attachment ta ON ta.`content_id` = tp.`profile_content_id`
LEFT JOIN tobject_page tp2 ON tp2.`page_id` = p.`partai_id`
LEFT JOIN tcontent_attachment ta2 ON ta2.`content_id` = tp2.`profile_content_id`
 WHERE province_id = '$province' AND dapil_id = '$dapil' AND p.partai_id = '$partai_id';";
        $dt = $this->db->query($query);
       // echo $this->db->last_query() . '<br>';
        return $dt;

    }

    function get_province($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->where($where);
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get('master_province');
        return $data;
    }

    function get_master_dapil($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->where($where);
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get('master_dapil');
//        echo $this->db->last_query();
        return $data;
    }

    function get_issue($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->where($where);
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get('province_issue');
        return $data;
    }

    function get_scandal_player($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->where($where);
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get('tscandal_player');
        return $data;
    }

    function get_news_player($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->where($where);
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get('tcontent_has_politic');
        return $data;
    }

    function get_object_page($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->from('tobject_page tp');
        $this->db->join('tusr_account ta', 'ta.user_id=tp.page_id', 'left');
        $this->db->where($where);
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get();
        return $data;
    }
}