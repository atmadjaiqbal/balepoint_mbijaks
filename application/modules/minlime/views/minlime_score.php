<div class="score-container">
    <div class="loader"></div>
    <ul class="content-score">
        <?php if($count != null){ ?>
        <?php foreach($count as $row=>$val){ ?>
            <?php
                $vote_class = 'light';
                if(key_exists('is_vote', $val)){
                    $vote_class = ($val['is_vote'] == 0) ? 'light' : 'dark';
                }
            ?>
        <li>
            <a class="score-btn" data-tipe='<?php echo $val['tipe']; ?>' data-id='<?php echo $id; ?>'>
            <img class="pull-left" src="<?php echo base_url().'assets/images/'.$val['tipe'].'_icon_small.png'; ?>">
            <span><?php echo $val['total']; ?></span>
            </a>
        </li>
        <?php } ?>
        <?php }else{ ?>
        <li>
            <a class="score-btn" data-tipe='comment' data-id='<?php echo $id; ?>'>
            <img class="pull-left" src="<?php echo base_url('assets/images/comment_icon_small.png'); ?>">
            <span>0</span>
            </a>
        </li>
        <li>
            <a class="score-btn" data-tipe='like' data-id='<?php echo $id; ?>'>
            <img class="pull-left" src="<?php echo base_url('assets/images/like_icon_small.png'); ?>">
            <span>0</span>
            </a>
        </li>
        <li>
            <a class="score-btn" data-tipe='dislike' data-id='<?php echo $id; ?>'>
            <img class="pull-left" src="<?php echo base_url('assets/images/dislike_icon_small.png'); ?>">
            <span>0</span>
            </a>
        </li>
        <?php } ?>
    </ul>
</div>