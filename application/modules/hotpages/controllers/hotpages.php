<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotpages extends Application {


    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
        $this->load->library('page/page_lib');
        $this->load->library('aktor/aktor_lib');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->model('content_model');

        $this->load->module('scandal/scandal');
        $this->load->module('news/news');
        $this->load->module('headline');
        $this->load->module('survey');

    }
    public function index()
    {
        $this->gaza();
    }

    public function gaza()
    {
        $data = $this->data;

        $rkey = 'news:list:headline';
        $where = "AND tc.title LIKE '%gaza%'";
        $news = $this->content_model->getNewsHotIssue($where, 0, 10); $resnews = array();
        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value['news_id']);
            $news_array = json_decode($rowNews, true);
            $news_array['entry_date'] = $value['entry_date'];
            $resnews[$i] = $news_array;
            $i++;
        }
        $data['news'] = $resnews;
        $html['html']['content'] = $this->load->view('hotpages/peristiwa_gaza', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function isis()
    {
        $data = $this->data;

        $rkey = 'news:list:headline';
        $where = "AND tc.title LIKE '% isis %'";
        $news = $this->content_model->getNewsHotIssue($where, 0, 10); $resnews = array();
        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value['news_id']);
            $news_array = json_decode($rowNews, true);
            $news_array['entry_date'] = $value['entry_date'];
            $resnews[$i] = $news_array;
            $i++;
        }
        $data['news'] = $resnews;
        $html['html']['content'] = $this->load->view('hotpages/peristiwa_isis', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }
    
    public function kabinet()
    {
        $data = $this->data;
        //$this->load->library('aktor/aktor_lib');
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts'] = array('bijaks.home.js', 'bijaks.js');
        $data['topic_last_activity'] =  '';

        $html['html']['content']  = $this->load->view('hotpages/m_kabinet_kerja', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function intrikcakapolri(){
        // $data= $this->data;
        $data['title'] = "Bijaks | Total Politik";
        $data['scripts']     = array('bijaks.home.js', 'bijaks.js');
        $data['topic_last_activity'] =  '';

        $data['data'] = [
            'narasibg'=>'Budi Gunawan adalah Calon tunggal Kepala Polisi Republik Indonesia pilihan Presiden Joko
            Widodo dan Megawati Soekarno Putri. Ia memang perwira yang cakap dan brilian. Beragam prestasi dan
            penghargaan didapat Budi Gunawan.<br/>
            Saat menjalani pendidikan Akademi Kepolisian angkatan 1983, Budi Gunawan lulus dengan predikat terbaik.
            Ia juga pernah meraih penghargaan Adhi Makayasa yang sangat prestisius.<br/>
            <div style="float: left;margin-right: 5px;"><img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/bg2.jpg " style="width: 100px;"/></div>
            Di setiap pendidikan kepolisian, Budi selalu meraih peringkat satu dan lulusan terbaik seperti saat  menempuh pendidikan di Sekolah Tinggi Ilmu Kepolisian-PTIK, Sekolah Staf dan Pimpina Polri, Sespati, dan Lemhanas. ',
            'title'=>'INTRIK POLITIK PEMILIHAN KAPOLRI',
            'narration'=>'Keputusan Jokowi memilih Budi Gunawan dan mengajukannya sebagai calon tunggal Kapolri memunculkan polemik.
        Polemik terjadi lantaran Komisi Pemberantasan Korupsi menetapkan Budi Gunawan sebagai tersangka dalam kasus kepemilikan rekening gendut. 
        <br/>Lalu polemik bergulir. Ada indikasi kuat dipilihnya Kepala Lembaga Pendidikan Polri Komjen Budi Gunawan karena dekat dengan 
        PDIP dan Megawati Soekarno Putri. Budi Gunawan dipilih oleh Presiden RI Joko Widodo menggantikan Jenderal Sutarman',
            'headline'=>'Penunjukkan calon Kapolri yang ternyata dijadikan tersangka oleh KPK dalam kasus  kepemilikan rekening gendut',
            'calonDiusung'=>[
                        [
                            'name'=>'KOMJEN POL. BUDI GUNAWAN',
                            'page_id'=>'komjenpoldrsbudigunawan51de4d9f36e8e',
                            'fullname'=>'Komjen Pol. Drs. Budi Gunawan S.H,MSi,Phd',
                            'ttl'=>'Surakarta, 11 Desember 1959',
                            'agama'=>'Islam',
                            'almamater'=>'Akademi Kepolisian (1983)',
                            'jabatan'=>'Kalemdikpol',
                            'img'=>'http://www.bijaks.net/public/upload/image/politisi/komjenpoldrsbudigunawan51de4d9f36e8e/badge/8f61076f53301b6e7861e7c6c9e0b346865b3890.jpg',
                            'page_id'=>'komjenpoldrsbudigunawan51de4d9f36e8e',
                            'pangkat'=>'Komisaris Jenderal',
                            'previous'=>[
                                ['jabatan'=>'Ajudan Presiden RI Megawati Soekarnoputri (2001-2004)'],
                                ['jabatan'=>'Karobinkar SSDM Polri (2004-2006)'],
                                ['jabatan'=>'Kaselapa Lemdiklat Polri (2006-2008)'],
                                ['jabatan'=>'Kapolda Jambi (2008-2009)'],
                                ['jabatan'=>'Kadiv Binkum Polri (2009-2010)'],
                                ['jabatan'=>'Kadiv Propam Polri (2010-2012)'],
                                ['jabatan'=>'Kapolda Bali (2012)'],
                                ['jabatan'=>'Kepala Lembaga Pendidikan dan Pelatihan Polri (2012-Sekarang)'],
                            ],
                            'images'=>[
                                ['imgUrl'=>'http://www.bijaks.net/assets/images/hotpages/cakapolri/bg1.jpg'],
                                ['imgUrl'=>'http://www.bijaks.net/assets/images/hotpages/cakapolri/bg2.jpg'],
                                ['imgUrl'=>'http://www.bijaks.net/assets/images/hotpages/cakapolri/bg3.jpg'],
                                ['imgUrl'=>'http://www.bijaks.net/assets/images/hotpages/cakapolri/bg4.jpg'],
                            ],
                        ]
                    ],

            'penyokong'=>[
                ['name'=>'Joko Widodo','jabatan'=>'Presiden RI','page_id'=>'irjokowidodo50ee1dee5bf19','logo'=>'http://www.bijaks.net/public/upload/image/politisi/indonesia5371ba70efd7b/badge/1dbb3dd9bd10114a278ed9777fe6f9ae382df811.png','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg' ],
                ['name'=>'Megawati Soekarnoputri','jabatan'=>'Ketua Umum PDIP','page_id'=>'megawatisoekarnoputri50ee62bce591e','logo'=>'http://www.bijaks.net/public/upload/image/politisi/partaidemokrasiindonesiaperjuangan5119ac6bba0dd/badge/partaidemokrasiindonesiaperjuangan5119ac6bba0dd_20130212_024922.jpg','img'=>'http://www.bijaks.net/public/upload/image/politisi/megawatisoekarnoputri50ee62bce591e/badge/03a4e973ba03f1ae7ba2234d4e8c61844b8669f6.jpg'],

            ],        

            'partaiPolitikPendukung'=>[
                ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
                ['page_id'=>'partaigolongankarya5119aaf1dadef'],
                ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
                ['page_id'=>'nasdem5119b72a0ea62'],
                ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
                ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
                ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ],

            'institusiPendukung'=>[
                ['page_id'=>'', 'name'=>'Menteri Dalam Negeri','logo'=>'http://www.bijaks.net/public/upload/image/politisi/kementriandalamnegeri531c1cbb6af24/thumb/b391f936d3d25df881427ebef81eae2180e78f1c.jpg'],
                ['page_id'=>'','name'=>'Menteri Koordinator Politik Hukum dan Keamanan','logo'=>'http://upload.wikimedia.org/wikipedia/id/e/e2/Logo_Kemenkopolhukam.png'],
                ['page_id'=>'','name'=>'Indonesian Police Watch','logo'=>'http://napzaindonesia.com/wp-content/uploads/2011/08/IPW.jpg']
            ],

            'partaiPolitikPenentang'=>[
                ['page_id'=>'partaidemokrat5119a5b44c7e4'],
                ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
                ['page_id'=>'partaikeadialndnpersatuanindonesia5119bd7483d2a'],
            ],
            'institusiPenentang'=>[
                ['page_id'=>'komisipemberantasankorupsi5192fb408b219','name'=>'Komisi Pemberantasan Korupsi','logo'=>'http://www.bijaks.net/public/upload/image/politisi/komisipemberantasankorupsi5192fb408b219/thumb/66c285e30ed48d4b4cb7101e3ee3ffe7980deb29.jpg','url'=>'http://www.bijaks.net/aktor/profile/komisipemberantasankorupsi5192fb408b219'],
                ['page_id'=>'','name'=>'TIM 9','logo'=>'http://www.bijaks.net/public/upload/image/politisi/tim9independen54db1f2a63520/badge/56e00d2fb307119a5a674e430619b419e2e99837.jpeg','url'=>'#'],
                ['page_id'=>'','name'=>'KOMNAS HAM','logo'=>'http://www.bijaks.net/public/upload/image/politisi/komnasham54c1eac938164/badge/e559bfdeda6bc708d46fcb9a29afb001bb2360ec.jpg','url'=>'http://www.bijaks.net/aktor/profile/komnasham54c1eac938164'],
                ['page_id'=>'','name'=>'Transparancy International Indonesia','logo'=>'http://www.ti.or.id/template/tii/images/logoTII_1.jpg','url'=>'http://www.ti.or.id/'],
                ['page_id'=>'','name'=>'Pusat Pelaporan dan Analisis Transaksi Keuangan (PPATK)','logo'=>'http://www.ppatk.go.id/web/images/logo_utama.png','url'=>'http://www.ppatk.go.id'],
                ['page_id'=>'icwindonesiancorruptionwatch534e45d49c5d9','name'=>'Indonesia Corruption Watch','logo'=>'http://www.bijaks.net/public/upload/image/politisi/icwindonesiancorruptionwatch534e45d49c5d9/badge/41c3255489b7e6c9e7387c77a6b573127ea9dfe4.jpg','url'=>'http://www.bijaks.net/aktor/profile/icwindonesiancorruptionwatch534e45d49c5d9'],
                ['page_id'=>'kontras531c20d0d0f90','name'=>'Kontras','logo'=>'http://www.bijaks.net/public/upload/image/politisi/kontras531c20d0d0f90/badge/0cbf1fc9308057d219ee4d62d6ee754476a21523.jpg','url'=>'http://www.kontras.org/'],
                ['page_id'=>'','name'=>'YLBHI','logo'=>'http://www.ylbhi.or.id/wp-content/uploads/2013/04/ylbhi_logo.png','url'=>'http://www.ylbhi.or.id/'],
                ['page_id'=>'','name'=>'Relawan Pro-Jokowi','logo'=>'https://pbs.twimg.com/media/BxxvUhHCEAAnSB5.jpg:large','url'=>'#'],

            ],
            'tokohPenentang'=>[
                ['name'=>'Susilo Bambang Yudoyono','logo'=>'http://www.bijaks.net/public/upload/image/politisi/susilobambangyudhoyono50ee672eb35c5/badge/39989d0890a536ffa7c758d668a09dbbb0b48a69.jpg','url'=>'http://www.bijaks.net/aktor/profile/susilobambangyudhoyono50ee672eb35c5'],
                ['name'=>'Prabowo Subianto','logo'=>'http://www.bijaks.net/public/upload/image/politisi/prabowosubiyantodjojohadikusumo50c1598f86d91/badge/059fd2fa2981621a0708c6a44243a003060151e6.jpg','url'=>'http://www.bijaks.net/aktor/profile/prabowosubiyantodjojohadikusumo50c1598f86d91'],
                ['name'=>'Mahfud MD','logo'=>'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/badge/3a1717b0f9432d353291a742de4479b53efc5822.jpg','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b'],
                ['name'=>'Andi Wijdjajanto','logo'=>'http://kanalsatu.com/images/20150205-142155_96.jpg','url'=>'#'],
                ['name'=>'Pratikno','logo'=>'http://www.bijaks.net/public/upload/image/politisi/pratikno540d1bf323949/badge/05af7cc18358cae9f41d818d58d9039adfc7099a.jpg','url'=>'http://www.bijaks.net/aktor/profile/pratikno540d1bf323949'],
                ['name'=>'Denny Indrayana','logo'=>'http://www.bijaks.net/public/upload/image/politisi/dennyindrayana50f51ec9ca991/badge/6a9ea736640f3a21b59eed1c123b037b2b760bb6.jpg','url'=>'http://www.bijaks.net/aktor/profile/dennyindrayana50f51ec9ca991'],
                ['name'=>'Syafii Maarif','logo'=>'http://www.thejakartapost.com/files/images/p24-a-1-1.jpg','url'=>'http://www.bijaks.net/aktor/profile/buyaahmadsyafiimaarif543f625a98a10'],
                ['name'=>'Jimly Asshiddiqie','logo'=>'http://www.bijaks.net/public/upload/image/politisi/jimlyasshiddiqie51cb99e738a36/badge/0670931aed7f5658b894614263594debfc5692e8.jpeg','url'=>'http://www.bijaks.net/aktor/profile/jimlyasshiddiqie51cb99e738a36'],
                ['name'=>'Imam Prasojo','logo'=>'http://upload.wikimedia.org/wikipedia/id/thumb/f/fa/Preskon_indonesia_berprestasi_award_imam_b_prasodjo-20080520-002-wawan.jpg/220px-Preskon_indonesia_berprestasi_award_imam_b_prasodjo-20080520-002-wawan.jpg','url'=>'http://id.wikipedia.org/wiki/Imam_B._Prasodjo'],
                ['name'=>'Todung Mulya Lubis','logo'=>'http://www.bijaks.net/public/upload/image/politisi/todungmulyalubis533a790b3fe41/badge/35a871a4bc67d17ff20c8ed3dfd9257eb5c15854.jpg','url'=>'http://www.bijaks.net/aktor/profile/todungmulyalubis533a790b3fe41'],
                ['name'=>'Erry Riyana Hardjapamekas','logo'=>'http://www.bijaks.net/public/upload/image/politisi/erryriyanahardjapamekas51a2b3bdb7874/badge/7df3df62f8fffc92d55b501d8cdf19f56877d557.jpg','url'=>'http://www.bijaks.net/aktor/profile/erryriyanahardjapamekas51a2b3bdb7874'],
                ['name'=>'Tumpak Hatorangan Panggabean','logo'=>'http://www.bijaks.net/public/upload/image/politisi/tumpakhatoranganpanggabean5193023cd7c5e/badge/1ead704c13a2634564cbb187f194d8e05ad2fc8e.jpg','url'=>'http://www.bijaks.net/aktor/profile/tumpakhatoranganpanggabean5193023cd7c5e'],
                ['name'=>'Bambang Widodo Umar','logo'=>'http://www.bijaks.net/public/upload/image/politisi/bambangwidodoumar51ef336d736aa/badge/b73be3163d2946c11c01f628d086274a85b25ff0.jpg','url'=>'http://www.bijaks.net/aktor/profile/bambangwidodoumar51ef336d736aa'],
            ],
            'pejabatKepolisianPenentang'=>[
                ['name'=>'Jenderal (Purn) Sutanto','logo'=>'http://www.bijaks.net/public/upload/image/politisi/jenderalpolisipurndrssutanto50f51010a58fa/badge/368c0259746181ae5f0a8d6120cfddadfc8e973c.jpg','url'=>'http://www.bijaks.net/aktor/profile/jenderalpolisipurndrssutanto50f51010a58fa'],
                ['name'=>'Komjen (Purn) Oegroseno','logo'=>'http://www.bijaks.net/public/upload/image/politisi/oegroseno52a91aeab1520/badge/142515740357e3e84d6c0fea2dffe34be0777bb6.jpg','url'=>'http://www.bijaks.net/aktor/profile/oegroseno52a91aeab1520'],
                ['name'=>'Jendral Sutarman','logo'=>'http://www.bijaks.net/public/upload/image/politisi/komjenpoldrssutarman51d2516f6eb74/badge/36548f460d48a13401e6afd25a13f18be615f234.jpg','url'=>'http://www.bijaks.net/aktor/profile/komjenpoldrssutarman51d2516f6eb74'],
                ['name'=>'Komjen Pol Suhardi Alius','logo'=>'http://www.bijaks.net/public/upload/image/politisi/suhardialius525b93d8efe1a/thumb/portrait/c31c983c6a789d089d5616ecab3b46e8788e4172.jpg','url'=>'http://www.bijaks.net/aktor/profile/suhardialius525b93d8efe1a'],
            ],
            'kronologi'=>[
                ['date'=>'9 Januari 2015','content'=>'Kompolnas menyerahkan lima nama calon Kapolri. Pada hari yang sama, Presiden Jokowi mengirimkan nama Komjen Budi Gunawan  sebagai calon tunggal Kapolri kepada DPR.'],
                ['date'=>'10 Januari 2015','content'=>'Ketua KPK Abraham Samad mengatakan, Jokowi tak melibatkan KPK untuk melakukan penelusuran rekam jejak saat memilih Komjen Budi Gunawan'],
                ['date'=>'12 Januari 2015','content'=>'Surat Presiden soal nama calon Kapolri dibacakan di rapat Paripurna DPR.'],
                ['date'=>'13 Januari 2015','content'=>'KPK menetapkan Komjen Budi Gunawan sebagai tersangka karena dugaan transaksi tidak wajar.'],
                ['date'=>'13 Januari 2015','content'=>'Presiden Jokowi menyatakan menunggu hasil keputusan DPR terhadap Komjen Budi Gunawan, sebelum menetapkan sikapnya sendiri.'],
                ['date'=>'14 Januari 2015','content'=>'Komisi III menyatakan Komjen Budi Gunawan layak dan patut sebagai Kapolri yang baru, menggantikan Jenderal Sutarman.'],
                ['date'=>'15 Januari 2015','content'=>'Rapat paripurna Dewan Perwakilan Rakyat (DPR) menetapkan Komisaris Jenderal Komjen Budi Gunawan sebagai calon kapolri menggantikan Jenderal Sutarman'],
                ['date'=>'16 Januari 2015','content'=>'Presiden Jokowi menunda pelantikan Komjen Budi Gunawan sebagai Kapolri yang sesuai dengan hasil rapat paripurna DPR.'],
                ['date'=>'28 Januari 2015','content'=>'Presiden Jokowi Widodo memanggil sembilan anggota tim independen untuk membahas polemik antara KPK dengan Polri.'],
                ['date'=>'29 Januari 2015','content'=>'Presiden Jokowi bertemu dengan Dewan Pembina Partai Gerindra, Prabowo Subianto. '],
            ],
            'quotePendukung'=>[
                ['from'=>'Jokowi','content'=>'"Kita menghormati KPK. Ada proses hukum di sini. Tetapi juga ada proses politik di DPR. Kita juga menghargai Dewan (Perwakilan Rakyat). Oleh sebab itu, sampai saat ini saya masih menunggu Sidang Paripurna. Sesudah selesai baru nanti akan kita putuskan, kebijakan apa yang akan kita ambil."','jabatan'=>'Presiden RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19'],
                ['from'=>'Tjahjo Kumolo','content'=>'"Ini masalah fakta dan yuridis, kalau kita mencermati dalam rangkaian proses ya Budi Gunawan sudah kapolri." ','jabatan'=>'Menteri Dalam Negeri','img'=>'http://www.bijaks.net/public/upload/image/politisi/tjahjokumolosh50f4fc41d9f04/badge/6d482165036d437e7ee1ea749dba121551c485ff.jpg','url'=>'http://www.bijaks.net/aktor/profile/tjahjokumolosh50f4fc41d9f04'],
                ['from'=>'Jenderal Badrodin Haiti','content'=>'“Pak Budi Gunawan masih akan menunggu proses praperadilannya."','jabatan'=>'Wakapolri','img'=>'http://www.bijaks.net/public/upload/image/politisi/badrodinhaiti531d430cb8e05/badge/bf1c71e9a9a4da57444be6bd6ee4204289caa29c.jpg','url'=>'http://www.bijaks.net/aktor/profile/badrodinhaiti531d430cb8e05'],
                ['from'=>'Neta S Pane','content'=>'“Presiden Jokowi harus memperjuangkan secara maksimal calon Kapolri yang sudah diusulkannya ke DPR, sehingga Jokowi tidak menjadi pecundang dan dituduh melecehkan DPR, Polri maupun melecehkan dirinya sendiri” ','jabatan'=>'Ketua Presidium Indonesia Police Watch (IPW)','img'=>'http://www.bijaks.net/public/upload/image/politisi/netaspane51be6ee808eb6/thumb/portrait/f9619dff06bb15cd22cebe278cd1b6a4d288ba6e.jpg','url'=>'http://www.bijaks.net/aktor/profile/netaspane51be6ee808eb6'],
                ['from'=>'Effendi Simbolon','content'=>'"Siapapun yang berniat menjatuhkan Jokowi, saatnya sekarang. Karena begitu banyak celahnya dan mudah-mudahan dua-duanya (Jokowi-JK) yang jatuh."','jabatan'=>'Politisi PDIP','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
                ['from'=>'Effendi Muara Sakti Simbolon','content'=>'Anda kader partai. Sadarlah. Tanpa partai, Anda bukan siapa-siapa.” ','jabatan'=>'Ketua PDIP','img'=>'http://delinewsindonesia.com/photo/1354305118effendi.jpg','url'=>''],
                ['from'=>'Fahri Hamzah','content'=>'"Kalau anda (Jokowi) tidak melantik, setidaknya ada 3 muka yang anda tampar (wajah Presiden, wajah DPR, wajah BG)."','jabatan'=>'Wakil Ketua DPR ','img'=>'http://www.bijaks.net/public/upload/image/politisi/fahrihamzahse5105e57490d09/badge/4b1333b525b60ef5d2347fa887d0525b7259068c.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09'],
                ['from'=>'Afrudin Jamal','content'=>'"Kami relawan nasional, mendukung Presiden Jokowi untuk segera melantik Komjen Pol Budi Gunawan sebagai Kapolri." ','jabatan'=>'Koordinator Relawan Nasional','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
            ],
            'quotePenentang'=>[
                ['from'=>'Usman Hamid','content'=>'"Belum pernah kewibawaan Presiden jatuh serendah ini. Lambatnya keputusan Presiden membuat hampir semua institusi negara jadi bulan-bulanan publik.”','jabatan'=>'Kontras','img'=>'http://www.bijaks.net/public/upload/image/politisi/usmanhamid54d41a069aa7a/badge/620b5a25d4fd36497c8e05c3eaaca075dd9b5593.jpg','url'=>'http://www.bijaks.net/aktor/profile/usmanhamid54d41a069aa7a'],
                ['from'=>'J-Flo','content'=>'"Kami akan mencabut dukungan jika Jokowi meneruskan pencalonan Budi Gunawan, dan relawan akan kembali turun ke jalan."','jabatan'=>'Aktris','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
                ['from'=>'Emerson Yuntho','content'=>'"Jokowi harus menarik surat pancalonan Budi Gunawan sebagai dianggap sebagai bentuk koreksi terhadap langkah yang terburu-terburu dalam menetapkan calon tunggal Kapolri"','jabatan'=>'Petisi Daring Change.org','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
                ['from'=>'Airlangga Pribadi','content'=>'"Ini bunuh diri politik Jokowi di hadapan pendukungnya, yang semula meyakini bahwa dia akan membawa agenda perubahan, menerapkan pemerintahan yang bersih." ','jabatan'=>'Pengamat Politik','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>''],
                ['from'=>'Pratikno','content'=>'"Akan lebih indah jika Budi Gunawan mundur dari pencalonan."','jabatan'=>'Menteri Sekeratris Negera','img'=>'http://www.bijaks.net/public/upload/image/politisi/pratikno540d1bf323949/badge/05af7cc18358cae9f41d818d58d9039adfc7099a.jpg','url'=>'http://www.bijaks.net/aktor/profile/pratikno540d1bf323949'],
                ['from'=>'Abraham Samad','content'=>'"Jadi sejak jauh sebelumnya kita sudah beritahu sebenarnya bahwa yang bersangkutan (Budi Gunawan) sudah punya catatan merah, jadi tidak elok kalau diteruskan, jadi ini tidak serta merta." ','jabatan'=>'Ketua KPK','img'=>'http://www.bijaks.net/public/upload/image/politisi/drabrahamsamadshmh5192f6e1b2ac4/badge/0c6f90c561ff0b3f38bc2411fb3c7d5473dd9dde.jpg','url'=>'drabrahamsamadshmh5192f6e1b2ac4'],
                ['from'=>'Jimly Asshidiqie','content'=>'"Kita harapkan tidak dilantik dan kemudian diajukan calon baru. Alasannya karena dia telah berstatus tersangka sehingga bukan hanya rule of law tetapi juga rule of ethics yang harus dijadikan pegangan." ','jabatan'=>'Wakil Ketua Tim 9','img'=>'http://upload.wikimedia.org/wikipedia/id/c/c4/Jimly_asshiddiqie.jpg','url'=>'http://id.wikipedia.org/wiki/Jimly_Asshiddiqie'],
                ['from'=>'Syafii Maarif','content'=>'"Ya namanya juga politik, pasti ditujukan untuk menyandera kebijakan Presiden yang sebenarnya akan membatalkan pencalonan Budi Gunawan sebagai Kapolri." ','jabatan'=>'Ketua TIM 9','img'=>'http://www.thejakartapost.com/files/images/p24-a-1-1.jpg','url'=>''],
                ['from'=>'Mahfud MD','content'=>'“Kalau dia jadi melantik Budi Gunawan yang sudah menjadi tersangka maka akan berbenturan dengan hukum pidana nantinya.” ','jabatan'=>'Mantan Ketua MK','img'=>'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/thumb/profdrmohammadmahfudmdshsu512c527ac591b_20130226_061354.jpg','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b'],
                ['from'=>'Oce Madril','content'=>'"Kalau sudah penetapan dari KPK, seharusnya Jokowi berani menarik pencalonan Budi Gunawan.”','jabatan'=>'Direktur Advokasi Pusat Kajian Antikorupsi Universitas Gadjah Mada','img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/5/52/Oce_Madril.jpg/220px-Oce_Madril.jpg','url'=>'http://id.wikipedia.org/wiki/Oce_Madril'],
                ['from'=>'Oegroseno','content'=>'"Ternyata ada proses yang tidak biasa. Tidak ada yang perhatikan mantan Kapolri Pak Sutarman. Lalu Pak Suhardi dicopot (sebagai Kabareskrim), kok segampang ini. Jangan-jangan ini jadi budaya baru." ','jabatan'=>'Mantan WakaPolri','img'=>'http://www.bijaks.net/public/upload/image/politisi/oegroseno52a91aeab1520/badge/142515740357e3e84d6c0fea2dffe34be0777bb6.jpg','url'=>'http://www.bijaks.net/aktor/profile/oegroseno52a91aeab1520'],

            ],
            'gelombangPenolakan'=>[
                [
                    'images'=>[
                        ['loc'=>'assets/images/hotpages/cakapolri/tutupmata1.jpg'],
                        ['loc'=>'assets/images/hotpages/cakapolri/tutupmata2.jpg'],
                    ],
                    'title'=>'Aksi “Tutup Mata”', 'aksi'=>'
                    <div style="float: left;margin-right: 5px;">
                        <img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/tutupmata1.jpg" style="width: 100px;"/>
                    </div>
Relawan pendukung Joko Widodo dan para aktivis antikorupsi 
                    mendesak Presiden Jokowi membatalkan pencalonan Budi Gunawan sebagai Kapolri. (15 Januari)<br/>
                    <div style="clear: both"></div>
                    '],
                [
                    'images'=>[
                        ['loc'=>'assets/images/hotpages/cakapolri/petisichange1.jpg'],
                    ],

                    'title'=>'Petisi Change.org','aksi'=>'
                    <div style="float: left;margin-right: 5px;">
                        <img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/petisichange1.jpg" style="width: 100px;"/>
                    </div>
                    Petisi daring "Jokowi, Jangan Menutup Mata Dalam Memilih
                    Calon Kapolri".  Digagas Emerson melalui situs change.org. Mendesak Presiden Jokowi membatalkan
                    pelantikan Komisaris Jenderal Budi Gunawan sebagai Kepala Polri.

                    <div style="clear: both"></div>

                    '],
                [
                    'images'=>[
                        ['loc'=>'assets/images/hotpages/cakapolri/sukabumi1.jpg'],
                        ['loc'=>'assets/images/hotpages/cakapolri/sukabumi2.jpg'],
                    ],
                    'title'=>'Demonstrasi di Sukabumi','aksi'=>'
                    <div style="float: left;margin-right: 5px;">
                        <img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/sukabumi2.jpg" style="width: 100px;"/>
                    </div>

                    Puluhan mahasiswa dari Ikatan Mahasiswa Muhammadiyah
                    (IMM) Sukabumi mendemo Gedung DPRD Kota Sukabumi. Mereke menolak adanya kepentingan politik dalam
                    proses hukum baik di Polri. (26 Januari).
                    <div style="clear: both"></div>

                    '],
                [
                    'images'=>[
                        ['loc'=>'assets/images/hotpages/cakapolri/yogyakarta1.jpg'],
                        ['loc'=>'assets/images/hotpages/cakapolri/yogyakarta2.jpg'],
                        ['loc'=>'assets/images/hotpages/cakapolri/yogyakarta3.jpg'],
                        ['loc'=>'assets/images/hotpages/cakapolri/yogyakarta4.jpg'],
                    ],
                    'title'=>'Demonstrasi di Yogyakarta','aksi'=>'
                    <div style="float: left;margin-right: 5px;">
                        <img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/yogyakarta2.jpg" style="width: 100px;"/>
                    </div>

                    Berbagai lembaga swadaya masyarakat (LSM) di
                    Yogyakarta mendatangi kantor Dewan Pimpinan Daerah Partai Demokrasi Indonesia Perjuangan (PDIP) DIY.
                    Mereka menuntut pembatalan Budi Gunawan sebagai calon Kapolri dan Megawati Soekarnoputri tidak
                    menghentikan intervensi Presiden Joko Widodo. (29 Januari).
                    <div style="clear: both"></div>

                    '],
                [
                    'images'=>[
                        ['loc'=>'assets/images/hotpages/cakapolri/hi1.jpg'],
                        ['loc'=>'assets/images/hotpages/cakapolri/hi2.jpg'],
                        ['loc'=>'assets/images/hotpages/cakapolri/hi3.jpg'],
                    ],
                    'title'=>'Demonstrasi di HI','aksi'=>'
                    <div style="float: left;margin-right: 5px;">
                        <img src="http://www.bijaks.net/assets/images/hotpages/cakapolri/hi1.jpg" style="width: 100px;"/>
                    </div>

                    Demonstrasi atas nama Koalisi Masyarakat Sipil di depan pos
                    Polisi Bundaran HI, Jakarta Pusat saat Car Free Day. Massa mendesak Jokowi untuk  membatalkan
                    pengangkatan Budi Gunawan jadi Kapolri. (18 Januari).
                    <div style="clear: both"></div>

                     '],
            ],
            'beritaTerkait'=>[
                ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/568eb1de0545834fbb72f2d2139916b33d65ca5a.jpg','shortText'=>'Pertemuan di restoran Tugu Kunstkring Paleis, Jalan Teuku Umar, Jakarta Pusat, malam itu berlangsung mendadak','link'=>'http://www.bijaks.net/scandal/index/15653-hidup_mati_mega_untuk_tersangka_budi_gunawan'],
                ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/83ffa77940c91cf95dd06f4b86c99afd64edaeef.jpg','shortText'=>'Penanganan kasus kriminalisasi Wakil Ketua Komisi Pemberantasan Korupsi (KPK), Bambang Widjojanto (BW), menemui jalan buntu','link'=>'http://www.bijaks.net/scandal/index/15758-siasat_jokowi__opt_biarkan_cpt__kriminalisasi_kpk'],
                ['img'=>'http://news.bijaks.net/uploads/2015/02/enam-opsi-jokowi-soal-nasib-bg-bukan-hal-baru-vdk91faV8Q-581x353.jpg','shortText'=>'Menteri Sekretaris Negara Pratikno mengatakan, enam opsi yang bisa diambil Presiden Joko Widodo bukanlah hal baru. “Opsi itu sudah diajukan oleh berbagai pihak, bukan opsi yang baru, telah muncul dari awal dari berbagai pihak.” kata Pratikno di Kompleks Istana Negara','link'=>'http://www.bijaks.net/news/article/0-91368/enam-opsi-jokowi-soal-nasib-budi-gunawan/'],
                ['img'=>'http://news.bijaks.net/uploads/2015/02/eggi-kpk-1301-529x353.jpg','shortText'=>'Komjen Budi Gunawan melalui kuasa hukumnya Eggi Sudjana mengatakan, tidak akan ','link'=>'http://www.bijaks.net/news/article/0-91950/budi-gunawan-minta-kpk-hormati-proses-hukum/'],
                ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/385287f0f1ac8631c2035ff52f5f704274a0f7e0.jpg','shortText'=>'Selasa, 16 Januari 2015 institusi Kepolisian Republik Indonesia (POLRI) geger dengan kabar pencopotan Komisaris Jenderal Suhardi Alius','link'=>'http://www.bijaks.net/scandal/index/15695-perang_bintang_di_tubuh_polri'],
                ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/6786056d886a4c5857048a7a528dcc69a0d45b32.jpg','shortText'=>'Nicolaas Jan Carel Francken dan istrinya, Irina Medvedeva, termasuk tokoh terkenal di Selandia Baru.','link'=>'http://www.bijaks.net/scandal/index/15663-uang_haram_anak_budi_gunawan'],
                ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/241f4c3fe5957f8bcc2c5fd697963e29254f7196.jpg','shortText'=>'Malang benar nasib Komjen Pol Budi Gunawan, calon tunggal Kapolri pilihan Presiden Jokowi.','link'=>'http://www.bijaks.net/scandal/index/15624-rekening_gendut_kapolri_pilihan_jokowi'],
                ['img'=>'http://news.bijaks.net/uploads/2015/02/fahri-hamzah2.jpg','shortText'=>'Wakil Ketua DPR, Fahri Hamzah mengatakan, tidak setuju jika Presiden Joko Widodo (Jokowi) mengeluarkan Perppu untuk panitia seleksi (Pansel) Pimpinan Komisi Pemberantasan Korupsi (KPK). Hal itu bukanlah sesuatu yang bisa mengatasi akar masalah yang terjadi selama ini.','link'=>'http://www.bijaks.net/news/article/7-91924/dpr-selesaikan-masalah-jokowi-tak-boleh-tambal-sulam'],
                ['img'=>'http://news.bijaks.net/uploads/2015/02/bambang-soesatyo.jpg','shortText'=>'Sekretaris Fraksi Golkar DPR, Bambang Soesatyo sangat menyayangkan sikap ','link'=>'http://www.bijaks.net/news/article/7-91841/jokowi-batal-lantik-bg-jadi-tamparan-keras-dpr'],
                ['img'=>'http://news.bijaks.net/uploads/2015/02/Pelantikan-Komjen-Budi-Batal-DPR-Tunggu-Penjelasan-Jokowi-627x261.jpg','shortText'=>'Pembatalan pelantikan Komjen Budi Gunawan sebagai Kepala Kepolisian RI (Kapolri) makin terang-benderang','link'=>'http://www.bijaks.net/news/article/9-91684/pelantikan-komjen-budi-batal-dpr-tunggu-penjelasan-jokowi'],
                ['img'=>'http://news.bijaks.net/uploads/2015/02/fadli-616x353.jpg','shortText'=>'Wakil Ketua DPR, Fadli Zon, mengatakan, pihaknya enggan menanggapi isu pembatalan','link'=>'http://www.bijaks.net/news/article/7-91631/dpr-takmau-tanggapi-pembatalan-pelantikan-bg'],
            ],
            'isiBawah'=>[
                ['name'=>'Surya Paloh','img'=>'http://www.bijaks.net/public/upload/image/politisi/suryapaloh511b4aa507a50/badge/3bbe78f966747a478c9416e6361a5bc28202f63d.jpg','jabatan'=>'Ketum Partai Nasdem','url'=>'http://www.bijaks.net/aktor/profile/suryapaloh511b4aa507a50'],
                ['name'=>'Jusuf Kalla','img'=>'http://www.bijaks.net/public/upload/image/politisi/drshmuhammadjusufkalla50ee870b99cc9/badge/6258b973c0c66b2d579434124018f46c3edd0f44.jpg','jabatan'=>'Wapres RI','url'=>'http://www.bijaks.net/aktor/profile/drshmuhammadjusufkalla50ee870b99cc9'],
                ['name'=>'Kompolnas','img'=>'http://cdn-media.viva.id/thumbs2/2013/10/23/226951_lembaga-komisi-kepolisian-nasional-di-indonesia--kompolnas-_663_382.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/kompolnas530b08ed8a7cd'],

            ],
            'analisa'=>'<div class="analisa_point">Setelah KPK menentapkan Budi Gunawan sebagai tersangka dalam kasus suap dan gratifikasi, Presiden Jokowi berada dalam kepungan tiga kekuatan besar. Pertama, parpol pengusungnya, PDIP dan partai koalisi yang mendesak Jokowi tetap melantik Budi Gunawan. Kedua, Jokowi menghadapi kepungan people power yang merupakan pendukungnya sendiri. Mereka aktif menolak Budi Gunawan. Ketiga, DPR itu sendiri yang sudah menyatakan Budi Gunawan layak dan patut sebagai Kapolri baru.</div>
                        <div class="analisa_point">Sebagai penyokong calon tunggal Kapolri, PDIP marah besar kepada Jokowi, lantaran tidak segera melantik Budi Gunawan. Ini kali kedua Jokowi menolak pesanan Megawati terkait Budi Gunawan. Setelah menolak Budi Gunawan sebagai Menteri Sekretaris Negara dan Kapolri.</div>    
                        <div class="analisa_point">Dalam situasi terjepit, Jokowi gagap dalam mengambil kebijakan. Solusi yang diambilnya adalah jalan tengah; menunda pelantikan Budi Gunawan. Namun demikian, publik tetap mengecam Presiden yang tidak segera mencabut Budi Gunawan sebagai calon Kapolri. Di lain sisi, DPR mengancam akan memakzulkan Presiden. PDIP juga menggalang dukungan untuk menjerat kebijakan Jokowi mengganti Budi Gunawan dengan calon Kapolri baru. </div>
                        <div class="analisa_point">Diluar dugaan, untuk menjawab anacaman pemakzulan, Jokowi membuat terobosan baru. Ia membentuk TIM 9 yang diketua Syafii Maarif dan bertemu Prabowo Subianto. Pada akhirnya, Jokowi tidak akan pernah melantik Budi Gunawan sebagai Kapolri. Sumber Tim 9 mengatakan bahwa Presiden memiliki calonnya sendiri untuk posisi Kapolri.  </div>',
            'karir'=>[
                ['mulai'=>'2001','berakhir'=>'2004','sbg'=>'Ajudan Presiden RI Megawati Soekarnoputri'],
                ['mulai'=>'2004','berakhir'=>'2006','sbg'=>'Kepala Biro Karir dan Perencanaan'],
                ['mulai'=>'2006','berakhir'=>'2008','sbg'=>'Kepala Sekolah Lanjutan Perwira Lembaga Pendidikan dan Latihan Polri'],
                ['mulai'=>'2008','berakhir'=>'2009','sbg'=>'Kapolda Jambi'],
                ['mulai'=>'2009','berakhir'=>'2010','sbg'=>'Kepala Divisi Pembinaan Hukum Polri '],
                ['mulai'=>'2010','berakhir'=>'2012','sbg'=>'Kepala Divisi Profesi dan Pengamanan Polri'],
                ['mulai'=>'2012','berakhir'=>'','sbg'=>'Kapolda Bali'],
            ],
            'seranganBuatBudiGunawan'=>'Jalan mulus Budi Gunawan mendapatkan posisi Kapolri tiba-tiba terhenti oleh
            putusan Komisi Pemberantasan Korupsi. Pada hari Selasa, 13 Januari 2015, lembaga anti rasuah tersebut
            menetapkan Budi Gunawan sebagai tersangka kasus suap dan gratifikasi ketika menjabat Kepala Biro Pembinaan
            Mabes Polri 2003-2006 dan jabatan lainnya.<br/><br/>
            "Kita ingin menyampaikan progress report dari kasus penyelidikan transaksi korupsi tidak wajar. KPK telah
            melakukan penyelidikan sejak Juli 2014, jadi sudah setengah tahun lebih kami melakukan penyelidikan atas
            kasus transaksi mencurigakan," jelas Abraham.<br/><br/>
            Abraham Samad juga menjelaskan bahwa sudah sejak lama BG mendapatkan catatan merah dari KPK. <br/>
            KPK menyangka Budi Gunawan dengan Pasal 12 a atau b, Pasal 5 ayat 2, Pasal 11, atau Pasal 12 B
            Undang-Undang Pemberantasan Korupsi juncto Pasal 55 ayat 1 KUHP. ',
            'polriSerangBaliKpk'=>[
                'narasi1'=>'Pasca Komisi Pemberantasan Korupsi menetapkan Budi Gunawan sebagai tersangka, serangan
                terhadap pimpinan KPK mulai berdatangan.',
                'narasi2'=>'Praktis, semua pimpinan KPK berstatus terlapor di Bareskrim Mabes Polri. Selain itu,
                beberapa penyidik dan karyawan KPK belakangan ini mendapatkan teror. Seorang pegawai KPK mengungkapkan
                bahwa teror disampaikan lewat pesan pendek, telepon, hingga berkali-kali dibuntuti saat pulang.
                "Salah satu pesan yang disampaikan adalah pembunuhan," kata sumber.<br/><br/>
                "Fakta-fakta teror itu sedang kami teliti lebih lanjut, kami sudah membentuk tim dan pada saatnya akan
                kami beri tahu kepada publik," kata Bambang, Rabu, 11 Februari 2015. ',
                'serangan'=>[
                    ['aksi'=>'Beredar di media sosial foto mirip Ketua KPK Abraham Samad tengah beradegan mesra
                    dengan seorang perempuan yang diduga Puteri Indonesia 2014, Elvira Devinamira Wirayanti.'],
                    ['aksi'=>'Sepekan setelah penetapan Budi Guawan sebagai tersangka oleh KPK, Jumat, 23 januari 2015,
                    Bareskrim Polri menangkap Wakil Ketua KPK Bambang Widjojanto (BW) di sebuah jalan raya di Depok,
                    sekitar pukul 07:30, Jumat, 23 Januari 2015.<br/>
                    Polri menetapkan BW sebagai tersangka dalam kasus pemberian keterangan palsu di depan sidang
                    sengketa Pilkada Kotawaringin Barat, Kalimantan Tengah, pada tahun 2010. Saat itu Bambang
                    Widjojanto menjadi pengacara pasangan bupati Ujang Iskandar - Bambang Purwanto dalam sengketa
                    Pemilukada Kotawaringin Barat, Kalimantan Tengah.'],
                    ['aksi'=>'Selanjutnya, Sabtu, 24 Januari 2015 Wakil Ketua KPK Adnan Pandu Praja dilaporkan ke
                    Bareskrim Mabes Polri atas tuduhan mengambil paksa saham milik PT Daisy Timber, Berau, Kalimantan
                    Timur, pada 2006.<br/>
                    Adnan Pandu disangkakan dengan pasal Tindak Pidana memasukkan Keterangan Palsu ke Dalam Akta
                    Autentik serta Turut Serta Melakukan Tindak Pidana Pasal 266 juncto Pasal 55 KUHP.'],
                    ['aksi'=>'Pada 22 Januari 2015, Abraham Samad dilaporkan oleh Direktur Eksekutif KPK Watch
                    Indonesia, Yusuf Sahide ke Bareskrim Polri. Laporan tertuang dalam surat laporan bernomor
                    LP/75/1/2015 Bareskrim. Ia diduga melanggar Pasal 36 Juncto 65 UU Nomor 30 Tahun 2002 tentang KPK,
                    berkaitan dengan pelanggaran etik. Samad juga dilaporkan karena adanya pertemuan dengan petinggi
                    salah satu partai. Juga menjanjikan bantuan hukum dan bisa meringankan hukuman Emir Moeis.'],
                    ['aksi'=>'Senin, 26 Januari, Ketua KPK Abraham Samad kembali dilaporkan ke Bareskrim Polri atas
                    tudingan melakukan pertemuan dengan petinggi Partai Demokrasi Perjuangan (PDIP), Hasto Kristyanto
                    dalam bursa calon Wakil Presiden Joko Widodo. Lobi politik tersebut diduga Abraham Samad memberikan
                    imbalan bantuan hukum bagi kader partai Emir Moeis.'],
                    ['aksi'=>'Lembaga Swadaya Masyarakat Gerakan Masyarakat Bawah Indonesia (LSM GMBI) melaporkan
                    Abraham Samad ke Bareskrim Polri atas dugaan kepemilikan senjata api (senpi) ilegal, Senin, Januari
                    2015. Laporan GMBI itu diterima Bareskrim dengan Laporan Polisi Nomor: LP/160/II/2015/Bareskrim‎
                    dengan terlapor Dr. Abraham Samad, SH.MH. Laporannya, diduga melakukan tindak pidana kepemilikan
                    senjata api tanpa izin.'],
                    ['aksi'=>'Giliran Zulkarnain diadukan oleh Fathur Rosyid ke Bareskrim Polri oleh Aliansi Masyarakat
                    Jawa Timur, Rabu 28 Januari 2015. Zulkarnain diduga melakukan tindak korupsi saat menangani kasus
                    korupsi bantuan sosial (bansos) Program Penanganan Sosial Ekonomi Masyarakat (P2SEM) Jawa Timur pada
                    tahun 2008. Zulkarnain diduga menerima uang suap sekitar Rp 5 miliar untuk menghentikan penyidikan
                    perkara tersebut. '],
                ],

            ],
            'narasiCalonBaru'=>'Presiden Joko Widodo dipastikan tidak akan melantik calon Kapolri Komjen Budi Gunawan.
            Presiden sedang menimbang-nimbang calon baru. Komisi Kepolisian Nasional (Kompolnas) sendiri telah
            menyerahkan enam nama calon Kapolri kepada Presiden Joko Widodo untuk dijadikan pertimbangan. Mereka
            adalah : ',
            'calonBaru'=>[
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/badrodinhaiti531d430cb8e05/badge/bf1c71e9a9a4da57444be6bd6ee4204289caa29c.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/badrodinhaiti531d430cb8e05',
                    'nama'=>'Komjen Pol Badrodin Haiti ',
                    'lulusan'=>'Akpol 1982',
                    'jabatanTerakhir'=>'Pelaksana Tugas (Plt.) Kapolri',
                    'lahir'=>'Jember, 24 Juli 1958',
                    'karir'=>[
                            ['tahun'=>'2004','jabatan'=>'Kapolda Banten'],
                            ['tahun'=>'2006','jabatan'=>'Kapolda Sulawesi Tengah'],
                            ['tahun'=>'2009','jabatan'=>'Kapolda Sumatera Utara'],
                            ['tahun'=>'2010','jabatan'=>'Kapolda Jawa Timur'],
                            ['tahun'=>'2011','jabatan'=>'Koorsahli Kapolri Mabes Polri'],
                            ['tahun'=>'2014','jabatan'=>'Wakil Kapolri'],
                        ]
                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/jenderaldwipriyatno51edcaa10f4f7/badge/3612e08f167db93f37f0817a668b834e3d5ce8b6.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/jenderaldwipriyatno51edcaa10f4f7',
                    'nama'=>'Komjen Pol Dwi Priyatno',
                    'lulusan'=>'Akpol 1982 ',
                    'jabatanTerakhir'=>'Inspektorat Pengawasan Umum (Irwasum) Polri ',
                    'lahir'=>'Purbalingga, 12 November 1959 ',
                    'karir'=>[
                        ['tahun'=>'2012','jabatan'=>'Sahlisospol Kapolri'],
                        ['tahun'=>'2013','jabatan'=>'Kapolda Jateng '],
                        ['tahun'=>'2014','jabatan'=>'Kapolda Metro Jaya '],
                    ]

                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/pututekobayuseno51bfc7763c3bd/thumb/portrait/7a9cd8745e07040c6a5315836383f4da0a6d9dd9.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/pututekobayuseno51bfc7763c3bd',
                    'nama'=>'Komjen Pol Putut Eko Bayu Seno',
                    'lulusan'=>'Akpol 1984 ',
                    'jabatanTerakhir'=>'Kepala Badan Pemeliharaan Keamanan Polri ',
                    'lahir'=>'Tulungagung, 28 Mei 1961',
                    'karir'=>[
                        ['tahun'=>'2004','jabatan'=>'Pamen Desumdaman Polri (Ajudan Presiden RI) '],
                        ['tahun'=>'2009','jabatan'=>'Wakapolda Metro Jaya'],
                        ['tahun'=>'2011','jabatan'=>'Kapolda Banten'],
                        ['tahun'=>'2011','jabatan'=>'Kapolda Jabar'],
                        ['tahun'=>'2012','jabatan'=>'Kapolda Metro Jaya'],
                    ]

                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/budiwaseso532bc404ac4cb/badge/2017f8db7c9f8d629580329f45ff441622d0adca.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/budiwaseso532bc404ac4cb',
                    'nama'=>'Komjen Pol Budi Waseso',
                    'lulusan'=>'Akademi Kepolisian 1984',
                    'jabatanTerakhir'=>'Kepala Badan Reserse Kriminal Polri ',
                    'lahir'=>'Februari 1960',
                    'karir'=>[
                        ['tahun'=>'2010','jabatan'=>'Kepala Pusat Pengamanan Internal Mabes Polri'],
                        ['tahun'=>'2012','jabatan'=>'Kapolda Gorontalo'],
                        ['tahun'=>'2013','jabatan'=>'Widyaiswara Utama Sespim Polri'],
                        ['tahun'=>'2014','jabatan'=>'Kepala Sekolah Staf dan Pimpinan Tinggi Polri'],
                    ]

                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/komjenpolanangiskandar516a6c028b562/thumb/portrait/komjenpolanangiskandar516a6c028b562_20130515_042440.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/komjenpolanangiskandar516a6c028b562',
                    'nama'=>'Komjen Pol Anang Iskandar',
                    'lulusan'=>'Akpol 1982',
                    'jabatanTerakhir'=>'Kepala Badan Narkotika Nasional (BNN) ',
                    'lahir'=>'Mojokerto, 18 Mei 1958 ',
                    'karir'=>[
                        ['tahun'=>'2006','jabatan'=>'Kapolwiltabes Surabaya'],
                        ['tahun'=>'2011','jabatan'=>'Kapolda Jambi'],
                        ['tahun'=>'2012','jabatan'=>'Kepala Divisi humas Polri'],
                    ]

                ],
                [
                    'img'=>'http://www.bijaks.net/public/upload/image/politisi/suhardialius525b93d8efe1a/thumb/portrait/c31c983c6a789d089d5616ecab3b46e8788e4172.jpg',
                    'url'=>'http://www.bijaks.net/aktor/profile/suhardialius525b93d8efe1a',
                    'nama'=>'Komjen Pol Suhardi Alius',
                    'lulusan'=>'Akpol 1985',
                    'jabatanTerakhir'=>'Sekretaris Utama Lemhannas',
                    'lahir'=>'Jakarta, 10 Mei 1962',
                    'karir'=>[
                        ['tahun'=>'2011','jabatan'=>'Wakapolda Metro Jaya '],
                        ['tahun'=>'2012','jabatan'=>'Kepala Divisi Hubungan Masyarakat (Kadiv Humas) Polri '],
                        ['tahun'=>'2013','jabatan'=>'Kabareskrim'],
                        ['tahun'=>'2013','jabatan'=>'Kapolda Jawa Barat'],
                    ]

                ],
            ]

        ];


        $html['html']['content']  = $this->load->view('hotpages/intrikcakapolri', $data, true);
        $this->load->view('m_tpl/layout', $html);

    }

    public function hukumanmati()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/hukuman_mati', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function lionair()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/lion_air', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function ahokvsdprd()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/ahokvsdprd', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function kisruhgolkar()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/kisruhgolkar', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function situsradikal()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/situsradikal', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function pssi()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/pssi_view', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function kaa()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/kaa_view', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function sabdaraja()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/sabdaraja', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function prostitusi()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/prostitusi', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function petral()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/petral', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function danaaspirasi()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/danaaspirasi', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function tolikara()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/tolikara', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function pasalhantu()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/pasalhantu', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function kampungpulo()
    {
        $data = $this->data;
        
        $html['html']['content'] = $this->load->view('hotpages/kampungpulo', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function terorisisdiparis()
    {
        $data = $this->data;
        
        $rkey = 'news:list:headline';
        $where = "AND tc.title LIKE '% isis %'";
        $news = $this->content_model->getNewsHotIssue($where, 0, 10); $resnews = array();
        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value['news_id']);
            $news_array = json_decode($rowNews, true);
            $news_array['entry_date'] = $value['entry_date'];
            $resnews[$i] = $news_array;
            $i++;
        }
        $data['news'] = $resnews;        

        $html['html']['content'] = $this->load->view('hotpages/terorisisdiparis', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function polaba()
    {
        $data = $this->data;

        $html['html']['content'] = $this->load->view('hotpages/polaba', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function kontroqurban()
    {
        $data = $this->data;

        $html['html']['content'] = $this->load->view('hotpages/kontroqurban', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function kampus_abal2()
    {
        $data = $this->data;

        $html['html']['content'] = $this->load->view('hotpages/kampusabal2', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }
    
    public function peristiwa()
    {
        $data['peristiwa'] = [
            [
                'title' => 'UJI COBA PILKADA SERENTAK',
                'url' => 'menguji_pilkada',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/pilkada2015/pilkada-banner.jpg',
                'text' => 'Setidaknya ada sebanyak 9 Provinsi dan ratusan kabupaten/kota menggelar Pemilihan Kepala Daerah (Pilkada) serentak, pada Rabu 9 Desember 2015. Gelaran pilkada secara serentak dengan skala nasional ini untuk pertama kalinya digelar di Indonesia untuk memilih pemimpin (kepala) daerah.'
            ],        
            [
                'title' => 'HMI, DARI KONGRES KE KONGRES',
                'url' => 'kongreshmi',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/kongreshmi/top.jpg',
                'text' => 'Himpunan Mahasiswa Islam (HMI) merupakan organisasi mahasiswa ekstra kampus yang berdiri pada 5 Februari 1947 di Yogyakarta, yang hingga sekarang menjadi organisasi tertua dan terbesar di Indonesia. Awal berdirinya HMI diprakarsai oleh kalangan mahasiswa yang menginginkan adanya sebuah organisasi yang mampu menampung dan menyalurkan setiap aspirasi mahasiswa-mahasiswa Islam.'
            ],

            [
                'title' => 'LOBI-LOBI LIAR KOMANDAN DPR',
                'url' => 'lobiliar',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/lobiliar/top.jpg',
                'text' => 'Tudingan Menteri ESDM, Sudirman Said perihal adanya pejabat tinggi yang melakukan lobi liar untuk meminta jatah saham ke PT Freeport  memicu kegaduhan politik di dalam negeri.'
            ],          
        
            [
                'title' => 'TEROR ISIS MENGGUNCANG PARIS',
                'url' => 'terorisisdiparis',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/terorisisdiparis/top.jpg',
                'text' => 'Rentetan serangan senjata, Bom dan aksi penyanderaan mengguncang pusat kota Paris, Prancis. Teror berdarah tersebut terjadi secara serentak di berbagai titik lokasi dalam kota mode itu , '
            ],          

            [
                'title' => 'KEJAHATAN KARTEL OBAT',
                'url' => 'kartelobat',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/kartelobat/top.jpg',
                'text' => 'Salah satu perusahaan Farmasi ternama di Indonesia diduga melakukan praktek suap terhadap ribuan dokter. Praktek tersebut berbentuk Kongkalikong antara perusahaan obat dan ribuan dokter. '
            ],          

            [
                'title' => 'MENGUAK TABIR SKANDAL SEKS PEJABAT',
                'url' => 'skandalsekspejabat',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/skandalsekspejabat/top.jpg',
                'text' => 'Maraknya skandal seks pejabat dengan artis merupakan fakta riil dan sering terjadi di dalam lingkaran kehidupan para pejabat.'
            ],          
            [
                'title' => 'BADAI RESTORASI',
                'url' => 'badairestorasi',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/badairestorasi/top.jpg',
                'text' => 'Partai NasDem yang membawa jargon \"gerakan perubahan\",  tidak serta merta membuat  kader partai tersebut bersih dari praktek korupsi. Tubuh partai Nasdem tengah terkoyak  setelah  beberapa elitnya diciduk KPK karena terbukti melakukan praktek korupsi.'
            ],              
            [
                'title' => 'PETAKA TAMBANG BERDARAH',
                'url' => 'petakatambang',
                'thumbUrl' => 'http://m.bijaks.net/assets/images/hotpages/tambang/salim-kancil.jpg',
                'text' => 'Tragedi berdarah di Lumajang, Jawa Timur dipicu oleh penolakan masyarakat terhadap aktivitas pertambangan yang  merusak lingkungan dan lahan pertanian milik warga'
            ],    
            [
                'title' => 'KABUT ASAP DAN BENCANA NASIONAL',
                'url' => 'daruratasap',
                'thumbUrl' => 'http://m.bijaks.net/assets/images/hotpages/asap/top.jpg',
                'text' => 'Setelah hampir setengah abad, bencana asap di Indonesia masih saja terjadi. Bahkan kian meluas di sejumlah wilayah Sumatera, Kalimantan, dan Jawa. Kita seakan tak pernah belajar, bahkan cenderung mengabaikannya'
            ],            
            
            [
                'title' => 'KONTROVERSI TANAH KAMPUNG PULO',
                'url' => 'kampungpulo',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/kampungpulo/top_kcl.jpg',
                'text' => 'Kamis 20/08/2015 pemrov DKI Jakarta melakukan pembongkaran bangunan di bantaran kali Ciliwung. Upaya Pemrov DKI Jakarta untuk merevitalisasi kali dengan menertibkan rumah dan bangunan milik warga mendapat respon keras dari warga. Warga kampung pulo menolak penggusuran, akhirnya bentrokanpun terjadi antara warga dan aparat. Bahkan Gubernur Ahok menurunkan ratusan polisi untuk mengamankan jalannya proses penggusuran tersebut.'
            ],
            [
                'title' => 'KONTROVERSI PASAL PENGHINAAN PRESIDEN',
                'url' => 'pasalhantu',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/pasalhantu/top_kcl.png',
                'text' => 'Presiden Joko Widodo (Jokowi) melalui pemerintahannya, menyedorkan 786 pasal dalam Rancangan Undang Undang (RUU) Kitab Undang Undang Hukum Pidana (KUHP) ke DPR RI. Dari ratusan pasal yang disedorkan, Jokowi memasukkan pasal mengenai pasal penghinaan presiden dan wakil presiden.'
            ],
            [
                'title' => 'DAMAI DI TOLIKARA',
                'url' => 'tolikara',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/tolikara/top_kcl.png',
                'text' => 'Kerusuhan berdarah terjadi disalah satu distrik di kabupaten Tolikara, Papua, menyusul protes yang dilakukan ratusan anggota jemaat Gereja Injil  Indonesia (GIDI) terhadap penyelenggaraan Salat Idul Fitri di lapangan Markas Komando (MAKO) rayon Militer pada Jumat, (17/07/2015).'
            ],
            [
                'title' => 'POLEMIK DANA ASPIRASI DPR RI',
                'url' => 'danaaspirasi',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/danaaspirasi/top_kcl.png',
                'text' => 'Dewan Perwakilan Rakyat (DPR) mengesahkan Peraturan DPR tentang tata cara pengusulan pembangunan daerah pemilihan (UP2DP) atau dana aspirasi dalam rapat paripurna. Total dana tersebut sebesar Rp 11,2 triliun atau 20 miliar bagi setiap anggota DPR RI.'
            ],
            [
                'title' => 'MENGGANYANG MAFIA MIGAS',
                'url' => 'petral',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/petral/top_kcl.jpg',
                'text' => 'Sejak Tanggal 13 mei 2015, pemerintah secara resmi membubarkan sekaligus melikuidasi anak usaha PT Pertamina (Persero), PT Pertamina Energy Trading Limited (Petral). Perusahaan yang berkantor di Singapura selama ini menjadi trader yang melakukan aktivitas jual-beli minyak dari negara-negara produsen, yang kemudian dijual ke Pertamina. Mekanisme seperti ini dinilai tidak efektif dan justru menjadi celah maraknya praktek korupsi yang dilakukan oleh mafia migas. Langkah pembubaran yang diambil oleh pemerintah ini dinilai tepat. Pertama untuk mengefisiensikan biaya pengadaan minyak. Dan kedua, untuk memutus mata rantai mafia-mafia migas yang selama ini menggerogoti tubuh Pertamina, terutama terkait pengadaan harga minyak dalam negeri.'
            ],
            [
                'title' => 'MENGINTIP ARTIS PSK',
                'url' => 'prostitusi',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/prostitusi/top_kcl.jpg',
                'text' => 'Menjamurnya prostitusi online jelas menambah daftar lokalisasi terselebung, seperti lokalisasi modus indekost, Penginapan dan Hotel atau Apartemen. Modus transaksinya lewat pesan Blackberri atau WA dengan mucikari, transfer uang muka lalu kencan dengan pelacur. Hal ini kemudian membuat pemerintah gencar melakukan razia untuk memberantas praktek pelacuran modus online.'
            ],
            [
                'title' => 'GONJANG GANJING SUKSESI SULTAN YOGYAKARTA',
                'url' => 'sabdaraja',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/sabdaraja/top_kcl.jpg',
                'text' => 'Keraton Yogyakarta sedang memanas. Dua buah Sabda raja yang dikeluarkan oleh Sri Sultan Hamenku Buwono X menuai polemik di internal kerajaan. Sabda Pertama memuat lima poin termasuk perubahan serta penghapusan Kalifatullah yang selama ini menjadi gelar bagi raja yang memerintah keraton Yogyakarta.'
            ],
            [
                'title' => 'PSSI DIAMBANG KEHANCURAN',
                'url' => 'pssi',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/pssi/img.jpg',
                'text' => 'Organisasi induk sepakbola Indonesia, Persatuan Sepakbola Seluruh Indonesia (PSSI) resmi dibekukan oleh Kemenpora. Langkah tegas tersebut diambil setelah surat teguran , dengan SP 3 yang dilayangkan ke PSSI terkait keikutsertaan Arema Cronus dan Persebaya dalam kompetisi liga Indonesia tidak digubris. Dua Klub asal jawa timur tersebut dinyatakaan tidak lolos persyaratan peserta kompetisi oleh Badan Olahraga Profesional Indonesia (BOPI) karena masalah legalitas dan dualisme kepemilikan klub.'
            ],
            [
                'title' => 'KONFERENSI ASIA AFRIKA',
                'url' => 'kaa',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/kaa/KAA.jpg',
                'text' => 'Konferensi Asia Afrika tahun 1955 menjadi tonggak penting dalam sejarah bangsa-bangsa Asia dan Afrika. Para delegasi yang berasal dari 29 negara peserta konferensi berkumpul di Bandung, Indonesia untuk membahas perdamaian, keamanan, dan pembangunan ekonomi di tengah-tengah berbagai masalah yang muncul di berbagai belahan dunia.'
            ],
            [
                'title' => 'KONTROVERSI SITUS ISLAM RADIKAL',
                'url' => 'situsradikal',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/situsradikal/top.jpg',
                'text' => 'Kementerian Komunikasi dan Informatika membredel situs atau media islam yang dinilai menyebarkan faham radikalisme dan seruan kebencian. Setidaknya ada 19 laman media Islam ditutup setelah Badan Nasional Penanggulangan Terorisme (BNPT)menganalisis potensi bahaya yang ditimbulkan dari isi laman-laman tersebut. Konten situs-situs tersebut mengandung unsur provokasi, penanaman ideologi ekstrem, radikal, dan teroris.'
            ],
            [
                'title' => 'RONDE TERAKHIR KISRUH GOLKAR',
                'url' => 'kisruhgolkar',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/kisruhgolkar/top.jpg',
                'text' => 'Partai Golkar di kancah perpolitikan nasional sedang berada pada kondisi labil. Lebih dari enam dekade terkahir sejak lahirnya Golkar, eksistensi Golkar relatif stabil sebagai partai kuat, yang mampu menancapkan pengaruhnya di setiap rezim yang dilaluinya. Namun di rezim pemerintahan Jokowi-JK, Golkar di bawah kepemimpinan Aburizal Bakrie diguncang kisruh internal yang hingga saat ini masih bergulir panas, dan entah sampai kapan akan berakhir. Kisruh Partai Golkar merupakan konsekuensi dari menguatnya adu kepentingan di internal partai yang memperebutkan pucuk pimpinan. Konflik internal partai berlambang beringin berimbas pada munculnya dualisme kepemimpinan, Aburizal Bakrie versi Munas Bali dan Agung Laksono versi Munas Jakarta. Kegaduhan politik di tubuh Golkar tidak hanya berimplikasi terbelahnya tubuh Golkar menjadi dua. Eskalasi konflik tidak hanya di dalam tetapi merembes ke luar lingkungan partai pasca SK Kemenkumham. '
            ],
            [
                'title' => 'MEMBONGKAR PEMBEGAL DANA SILUMAN',
                'url' => 'ahokvsdprd',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/top.jpg',
                'text' => 'Keterangan Gubernur DKI Jakarta, Basuki Tjaja Purnama (Ahok) terkait adanya penggelembungan dana ilegal RAPBD DKI yang berlangsung sejak tahun 2012 hingga 2015 menuai polemik yang semakin meluas. Adanyana siluman yang dilontarkan Gubernur Ahok spontan mendapat reaksi dari DPRD DKI Jakarta. Mereka mempermasalahkan pernyataan mantan politisi partai Gerindera tersebut. Kisruh berawal ketika Ahok dan DPRD bersitegang lantaran ditemukannya dana siluman dalam rancangan APBD 2015 yang diselundupkan oleh Dewan. Ahok menyebutkan nilai dana siluman tersebut sebesar Rp 12,1 triliun. Selanjutnya, Anggaran pendapatan belanja daerah (APBD) DKI 2015 tidak kunjung cair.'
            ],
            [
                'title' => 'LION AIR : POLITIK "SINGA" OMPONG',
                'url' => 'lionair',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/lionair/top.jpg',
                'text' => 'Sejak Rabu, 18 Februari sebanyak 54 penerbangan di bawah manajemen Lion Air Indonesia mengalami penundaan terbang. Ribuan penumpang telantar lantaran tak ada kejelasan dari pihak Lion Air kapan mereka bisa terbang. Dalam kasus ini, para penumpang telah dirugikan oleh buruknya managemen maskapai milik salah satu anggota Dewan Pertimbangan Presiden (Wantimpres), Rusdi Kirana itu.'
            ],
            [
                'title' => 'HUKUMAN MATI REZIM JOKOWI',
                'url' => 'hukumanmati',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/hukumanmati/top.png',
                'text' => 'Kehadiran pemerintahan baru di bawah Joko Widodo tidak mengubah banyak praktik hukuman mati di Indonesia. Di awal kepemimpinannya, Jokowi bahkan telah mengeksekusi 6 terpidana mati kasus narkoba di Nusa Kambangan dan Boyolali.'
            ],
            [
                'title' => 'INTRIK POLITIK PEMILIHAN KAPOLRI',
                'url' => 'intrikcakapolri',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/cakapolri/peristiwa_polri.png',
                'text' => 'Keputusan Jokowi memilih Budi Gunawan dan mengajukannya sebagai calon tunggal Kapolri memunculkan polemik. Polemik terjadi lantaran Komisi Pemberantasan Korupsi menetapkan Budi Gunawan sebagai tersangka dalam kasus kepemilikan rekening gendut.Lalu polemik bergulir. Ada indikasi kuat dipilihnya Kepala Lembaga Pendidikan Polri Komjen Budi Gunawan karena dekat dengan PDIP dan Megawati Soekarno Putri. Budi Gunawan dipilih oleh Presiden RI Joko Widodo menggantikan Jenderal Sutarman'
            ],
            [
                'title' => 'PERISTIWA GAZA MEMBARA',
                'url' => 'gaza',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/gazaa-hot-issue.jpg',
                'text' => 'Penjajahan bangsa yang dilakukan oleh Israel terhadap Palestinapada dasarnya bermotifkan perluasan kekuasaan oleh Israel . Sejarah ini berawal pada tahun 1934 hingga 1945, dimana pada tahun tersebut adalah tahun kekusaan penguasa yang terkenal ganas dan brutal dari Jerman yang bernama Adolf Hitler. Pada massa kepemimpinannya Hitler menumpas habis seluruh Bangsa Yahudi yang berada di Eropa, hingga bangsa Yahudi tersebut ketakutan dan diusir dari tanah Eropa. Melalui tekanan dan kebrutalan Hitler, akhirnya warga Yahudi lari dan kabur ke daerah Timur tengah, yang kebetulan daerah tersebut pada tahun 1946 sedang dijajah oleh Inggris. Melalui perundingan yahudi dengan Inggris, mereka meminta izin kepada Inggris untuk membentuk suatu Negara, akhirnya diberi sedikit daerah untuk warga Yahudi untuk mendirikan Negara yang diberi nama Jewish Land, atau yang sekarang lebih Kita kenal dengan bangsa Israel.'
            ],
            [
                'title' => 'PERISTIWA KEBRUTALAN ISIS',
                'url' => 'isis',
                'thumbUrl' => 'http://www.bijaks.net/assets/images/hotpages/isis/event-page-isis-landing.jpg',
                'text' => 'Tujuan asal dari ISIS adalah untuk mendirikan sebuah sistem khilafah di kawasan mayoritas Sunni di Irak. Menyusul keterlibatannya di Perang Saudara Syria, tujuan ini kemudian meluas untuk juga bisa mengontrol dan menguasai kawasan mayoritas Sunni di Suriah. Sebuah negara dengan sistem khilafah diproklamasikan pada 29 Juni 2014. Abu Bakar Al-Baghdadi yang saat ini dikenal sebagai Amirul Mukminin Khalifah Ibrahim dideklarasikan dan dilantik sebagai Khalifah. Kelompok ISIS ini lalu berganti nama menjadi Negara Islam.'
            ],
                          

        ];
        
        $html['html']['content'] = $this->load->view('hotpages/peristiwa_list', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }


    public function daruratasap()
    {
        $data = $this->data;

        $html['html']['content'] = $this->load->view('hotpages/asap', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function petakatambang()
    {
        $data = $this->data;

        $html['html']['content'] = $this->load->view('hotpages/petakatambang', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }    

    public function badairestorasi()
    {
        $data = $this->data;

        $html['html']['content'] = $this->load->view('hotpages/badairestorasi', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }      

    public function kartelobat()
    {
        $data = $this->data;

        $html['html']['content'] = $this->load->view('hotpages/kartelobat', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }  

    public function skandalsekspejabat()
    {
        $data = $this->data;

        $html['html']['content'] = $this->load->view('hotpages/skandalsekspejabat', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }       

    public function lobiliar()
    {
        $data = $this->data;
        
        $rkey = 'news:list:headline';
        $where = "AND tc.title LIKE '% setya novanto %'";
        $news = $this->content_model->getNewsHotIssue($where, 0, 10); $resnews = array();
        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value['news_id']);
            $news_array = json_decode($rowNews, true);
            $news_array['entry_date'] = $value['entry_date'];
            $resnews[$i] = $news_array;
            $i++;
        }
        $data['news'] = $resnews;        

        $html['html']['content'] = $this->load->view('hotpages/lobiliar', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }    

    public function kongreshmi()
    {
        $data = $this->data;
        // Data on page
        $data['data'] = [
            'block1'=>[
                'title'=>'Sejarah Terbentuknya HMI',
                'narasi'=>'
                    <img src="http://wartadki.com/foto_berita/65hmi.jpg" class="pic">
                    <p>Himpunan Mahasiswa Islam (HMI) merupakan organisasi mahasiswa ekstra kampus yang berdiri pada 5 Februari 1947 di Yogyakarta, yang hingga sekarang menjadi organisasi tertua dan terbesar di Indonesia. Awal berdirinya HMI diprakarsai oleh kalangan mahasiswa yang menginginkan adanya sebuah organisasi yang mampu menampung dan menyalurkan setiap aspirasi mahasiswa-mahasiswa Islam. </p>
                    <p>Agresi militer Belanda yang kembali dilancarkan membuat para tokoh-tokoh bangsa mengambil sikap yang berbeda dalam menghadapi situasi tersebut. Akibatnya terjadi polarisasi politik yang membuat perpolitikan di tanah air terbelah menjadi dua kubu yang saling bertolak belakang. Pihak pemerintah yang dipelopori oleh partai Sosialis mencoba menekan para tokoh-tokoh yang berhaluan islam agar patuh pada pemerintah dalam merespon ancaman dari Belanda. </p>
                    <img src="http://rakyatsulsel.com/folder-konten/themes/rakyatsulsel2015/tum.php?src=http://rakyatsulsel.com/folder-konten/uploads/2015/07/sekretariat-PB-HMI-e1437655762548.jpg&w=668&h=439" class="pic2">
                    <p>Kondisi sosio-politik yang tengah kacau balau tersebut dengan cepat menyebar ke seluruh sendi-sendi perpolitikan, tidak terkecuali bagi kalangan pemuda dan mahasiswa. Dua organisasi kepemudaan yakni, perserikatan mahasiswa yokyakarta dan gerakan pemuda Islam Indonesia  berada dalam kondisi labil, apakah akan ikut dengan pemerintah atau ke pihak oposisi. Akhirnya, beberapa mahasiswa diantaranya, Lefran Fane dan kawan-kawan menggagas  organisasi kemasiswaan yang yang berlatar belakang Islam agar segera dibentuk. Gagasan ini sebagai respon untuk mempertegas posisi dan sikap kalangan mahasiswa Islam untuk ikut memperjuangkan keutuhan NKRI dari ancaman Kolonialisme. Selain itu, organisasi mahasiswa Islam juga merupakan bentuk sikap perlawanan terhadap ancaman ideologi komunisme yang tengah dilancarkan oleh pemerintah melalui Partai Sosialis Indonesia (PNI) dan Partai Komunis Indonesia (PKI). </p>
                    <img src="http://riauone.com/photo/berita/dir112015/3174_Jelang-Kongres-HMI-ke-29--tidak-satupun-kandidat-dari-cabang-Pekanbaru-dan-Riau.jpg" class="pic">
                    <p>Pada tanggal 5 Februari 1947, di Yogyakarta, rapat penepatan berdirinya Himpunan Mahasiswa Islam (HMI) dan memutuskan kepengerusan struktur organisasi. Hadir dalam rapat tersebut adalah Lafran Pane, Karnoto Zarkasyi, Dahlan Husein, Maisaroh Hilal (cucu pendiri Muhammadiyah, KH. Ahmad Dahlan), Suwali, Yusdi Ghozali; tokoh utama pendiri Pelajar Islam Indonesia (PII), Mansyur, Siti Zainah (istri Dahlan Husein), Muhammad Anwar, Hasan Basri, Zulkarnaen, Tayeb Razak, Toha Mashudi dan Bidron Hadi.</p>
                    <p><b>Struktur Kepengurusan Organisasi :</b></p>
                    <p>
                    <table>
                        <tr>
                            <td class="title1">Ketua</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td>Lafran Pane</td>
                        </tr>
                        <tr>
                            <td class="title1">Wakil Ketua</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td>Asmin Nasution</td>
                        </tr>
                        <tr>
                            <td class="title1">Penulis I</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td>Anton Timoer Djailani, salah satu pendiri Pelajar Islam Indonesia (PII)</td>
                        </tr>
                        <tr>
                            <td class="title1">Penulis II</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td>Karnoto Zarkasyi</td>
                        </tr>
                        <tr>
                            <td class="title1">Bendahara I</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td>Dahlan Husein</td>
                        </tr>
                        <tr>
                            <td class="title1">Bendahara II</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td>Maisaroh Hilal</td>
                        </tr>
                        <tr>
                            <td class="title1">Anggota</td>
                            <td>&nbsp;:&nbsp;</td>
                            <td>
                                <ul>
                                    <li>Suwali</li>
                                    <li>Yusdi Gozali (pendiri Pelajar Islam Indonesia (PII))</li>
                                    <li>Mansyur</li>
                                </ul>
                            </td>
                        </tr>


                    </table>
                    </p>
                '
            ],

            'block7'=>[
                'title'=>'Kongres Pekanbaru dan Dugaan Anggaran 3 Miliar',
                'narasi'=>'
                    <img src="http://news.detakriaunews.com/foto_berita/2420151122_111649.jpg" class="pic">
                    <p>Kongres HMI ke 29 yang dilaksanakan pada tanggal 1-5 November 2015 digelar di Pekanbaru, Riau. Kongres kali ini disebutkan dalam konsolidasi nasional dari anggota-anggota HMI yang tersebar di komisariat di seluruh Indonesia. Tidak hanya itu dalam kongres ini akan dihadiri berbagai tokoh nasional yang juga merupakan dari alumni HMI yang tergabung dalam KAHMI (Korps Alumni HMI) seperti Jusuf Kalla, Akbar Tandjung, Mahfud MD, Zulkifli Hasan, Irman Gusman, Harry Azhar Azis, Husni Kamil Manik, Muhammad, Ferry Mursyidan Baldan, Yuddy Chrisnandy, M Nasir, Anies Baswedan, Azyumardi Azra, dan Taruna Ikrar.</p>
                    <p>Terdapat beberapa agenda yang akan dibahas dalam kongres tersebut seperti pemilihan ketua umum PB HMI periode selanjutnya. Selain itu juga ada pembahasan mengenai rekomendasi pengangkatan Lafran Pane salah satu dari pendiri HMI sebagai Pahlawan Nasional. </p>
                    <img src="https://anekainfounik.files.wordpress.com/2015/11/suasana-sidang-pleno-i-kongres-hmi-xxix-pekanbaru-riau.jpg" class="pic2">
                    <p>Kongres yang direncanakan hanya berlangsung 6 hari, kembali molor akibat beberapa kericuhan yang tak mengeenakkan. Mulai dari urusan Panasko yang tidak jelas, ditambah lagi urusan Rombongan Liar (Romli) yang kian hari kian membludak. Isu yang paling viral dimainkan oleh beberapa media, baik cetak, tv maupun online yaitu adanya pendanaan kongres yang menggunakan dana APBN dan APBD.</p>
                    <p>Dana sebanyak 7 Miliar diduga berasal dari pemda Pekanbaru 3 Miliar dan dari pemerintah pusat 4 Miliar itu, bahkan menjadi bidikan khusus bagi KPK, BPK dan beberapa LSM Anti Korupsi untuk menelusuri langsung penggunaan dana tersebut. Diantara isu yang digiring adalah bahwa anggaran kongres justru lebih besar daripada anggaran penanganan asap yang terjadi di Riau.</p>
                    <img src="http://4.bp.blogspot.com/-2Z24cEjqhnc/Vl50mrCfLwI/AAAAAAAAXy0/nKzDpkr-Cdc/s1600/kongres%2BHMI%2Bricuh.jpg" class="pic">
                    <p>Menanggapi hal tersebut, Wakil Presiden Jusuf Kalla yang merupakan salah satu senior di HMI menepis bahwa anggaran tersebut wajar di setiap acara kongres HMI. Ketua Umum domisioner PB HMI pun menguatkan pernyataan bahwa selama pelaksanaan kongres mereka akan menggunakan pihak BPK untuk mengawasi penggunaan anggaran tersebut. Kongres pun juga meninggalkan bekas disetiap sudut Gelanggang Olahraga yang diakibatkan oleh kericuhan yang sering terjadi.</p>


                '
            ],

            'block3'=>[
                'title'=>'Dualisme dan Islah HMI DIPO dan MPO',
                'narasi'=>'
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Pada Kongres HMI di Padang tahun 1986, terlihat bagaimana susahnya menyatukan sebuah perbedaan dan pertentangan apalagi keadaan saat itu pemerintah mengeluarkan kebijakan seluruh organisasi apapun itu harus memakai pancasila sebagai anggaran dasar dari organisasi. HMI harus menghadapi dilemma tersebut sehingga harus memilih untuk mengikuti kebijakan tersebut atau tidak. </p>
                    <img src="http://cdn-2.tstatic.net/tribunnews/foto/bank/images/20130315_Kongres_Himpunan_Mahasiswa_Islam_ke-28_2672.jpg" class="picprofil">
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Akhirnya HMI terpaksa mengalah untuk menerima kebijakan tersebut untuk menyelamatkan organisasi dari pembubaran. Namun, penerimaan tersebut berdampak pecahnya kepengurusan menjadi Dipo yang menjadikan NDP (Nilai Dasar Perjuangan) yang lebih menitikberatkan pada wacana Islam kebangsaan yang dipadukan dengan pemikiran teologi pembebasan (liberal), sedangkan MPO yang berlandaskan terhadap Khitah perjuangan menekankan pada wacana penafsiran islam sebagai pandangan hidup yang diselaraskan dengan pemikiran kesadaran keberislaman (teosofi transenden). </p>
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Pecahnya kepengurusan HMI yang terjadi pada tahun 1986 tersebut telah lama diwacanakan untuk mengadakan islah atau penyatuan kembali kepengurusan. Baru pada Kongres HMI di Palembang tahun 2008, wacana untuk mengadakan islah terbicarakan. Kedua HMI yang mengikuti kongres pada waktu itu akhirnya mencapai kata sepakat untuk melepaskan ego masing-masing yang telah lama terjadi dan  membicarakan kembali mengenai Islah organisasi. </p>
                    


                '
            ],

            'block4'=>[
                'title'=>'Jejak Langkah HMI',
                'narasi'=>'
                    <p>HMI sebagai salah satu organisasi kemahasiswaan yang masih eksis sampai sekarang telah mengalami berbagai fase-fase yang membentuk organisasi sampai sekarang sebagai berikut :</p>
                    <div class="clear"></div>
                    <p>
                        <div style="float: left;width: 81px;font-weight: bold;">
                        1946-1947
                        </div>
                        <div style="float: left;font-weight: bold;">
                        Pembentukan dan Konsolidasi HMI
                        </div>
                        <div class="clear"></div>
                        <div style="float: left;margin-left: 81px;">
                        Diawali akibat polarisasi kekuatan politik di kalangan mahasiwa Yogyakarta yang berhaluan sosialis. Pada Februari tahun 1947, HMI terbentuk untuk menjembatani pertentangan antara politik nasional dan islam. 
                        </div>
                        <div class="clear"></div>
                    
                        <div style="float: left;width: 81px;font-weight: bold;">
                        1948-1953
                        </div>
                        <div style="float: left;font-weight: bold;">
                        Pembinaan dan Perjuangan HMI
                        </div>
                        <div class="clear"></div>
                        <div style="float: left;margin-left: 81px;">
                        Indonesia yang sedang berjuang dalam revolusi untuk mempertahankan kemerdekaan membuat anggota-anggota HMI yang merupakan pemuda ikut serta mengangkat senjata dalam aksi memperjuangkan kemerdekaan. Pasca revolusi kemerdekaan, Tahun 1950 menjadi masa HMI untuk melakukan konsolidasi besar-besaran salah satunya pemindahan PB-HMI yang tadinya berada di Yogyakarta dipindahkan ke Jakarta. Tidak hanya itu juga pembentukan cabang-cabang HMI di berbagai daerah di Indonesia serta HMI mendeklarasikan bahwa HMI independen dari parpol manapun saat pemilu pertama Indonesia pada tahun 1955.
                        </div>
                        <div class="clear"></div>

                        <div style="float: left;width: 81px;font-weight: bold;">
                        1959-1965
                        </div>
                        <div style="float: left;font-weight: bold;">
                        Tantangan
                        </div>
                        <div class="clear"></div>
                        <div style="float: left;margin-left: 81px;">
                        Pada masa Orde Lama HMI mengalami ujian berat yaitu ancaman pembubaran oleh pemerintah atas hasutan dari PKI sebagaimana cerita Sulastomo yang juga menjabat sebagai Ketua umum PB HMI dalam bukunya “Hari-Hari yang Panjang Transisi Orde Lama ke Orde Baru”. Namun, pembubaran tersebut tidak terjadi karena sebelum dibubarkannya HMI terjadi jatuhnya Orde Lama berganti menjadi Orde Baru.
                        </div>
                        <div class="clear"></div>

                        <div style="float: left;width: 81px;font-weight: bold;">
                        1970
                        </div>
                        <div style="float: left;font-weight: bold;">
                        Kebangkitan Dalam Keilmuwan
                        </div>
                        <div class="clear"></div>
                        <div style="float: left;margin-left: 81px;">
                        Pada masa ini HMI benar-benar mengalami masa keemasan yaitu munculnya para pemikir dan pembaharuan di HMI, salah satunya yaitu Nurcholish Majid. Nurcholis Majid menyampaikan ide pembaharuan dengan topik Keharusan Pembaharuan pemikiran dalam islam dan masalah integrasi umat. Sebagai konsekuensinya di HMI timbul pergolakan pemikiran dalam berbagai substansi permasalahan timbul perbedaan pendapat, penafsiran dan interpretasi. Hal ini tercuat dalam bentuk seperti persoalan negara islam, islam kaffah, sampai pada penyesuaian dasar HMI dari Islam menjadi Pancasila.
                        </div>
                        <div class="clear"></div>

                        <div style="float: left;width: 81px;font-weight: bold;">
                        1983-1986
                        </div>
                        <div style="float: left;font-weight: bold;">
                        Dualisme dan Perpecahan HMI
                        </div>
                        <div class="clear"></div>
                        <div style="float: left;margin-left: 81px;">
                        Masa Orde Baru juga membuat HMI mengalami dinamika keorganisasian yang bahkan mengubah landasan dasar organisasi. Berawal dari peraturan pemerintah mengenai asas tunggal yang mengharuskan seluruh organisasi baik itu politik, kemasyarakatan, maupun kemahasiswaan. Kebijakan politik keseragaman yang diberlakukan oleh pemerintah Orde Baru juga berdampak ke HMI yang notabenenya adalah organisasi kemahasiswaan. Pada Kongres HMI 1983, terjadi perpecahan di dalam HMI apakah ingin mengikuti kebijakan pemerintahan, namun tidak sampai pecahnya organisasi. Baru pada tahun 1986 saat diadakannya Kongres di Padang, HMI memutuskan mengikuti pemerintah dengan mengubah asasnya menjadi Pancasila dan nasional (HMI Dipo). Kalangan yang tidak menerima akhirnya membentuk kepengurusan HMI MPO (Majelis Penyelamat Organisasi).
                        </div>
                        <div class="clear"></div>

                        <div style="float: left;width: 81px;font-weight: bold;">
                        1998-Sekarang
                        </div>
                        <div style="float: left;font-weight: bold;">
                        Reformasi 
                        </div>
                        <div class="clear"></div>
                        <div style="float: left;margin-left: 81px;">
                        Pasca Jatuhnya Orde Baru digantikan menjadi masa reformasi, terjadi pragmatism keorganisasian dan pengkaderan di dalam tubuh HMI. Banyak anggota HMI yang memiliki orientasi untuk mendapatkan kekuasaan sehingga organ HMI hanya sebagai batu loncatan untuk masuk ke dalam pemerintah. Perubahan orientasi yang menjangkiti kebanyakan kader HMI itu telah menghilangkan kekritisan dan keilmiahan HMI sebagai organisasi. Jika semasa emasnya muncul pemikir-pemikir ideologis HMI seperti Nurcholish Maji, Azyumardi Azra, Achmad Chatib, Endang Ashari, dan lain-lain. Pasca reformasi berbeda 180 derajat, hal itu jelas saja membuat banyak alumni HMI salah satunya Nurcholish Majid mengeluarkan gagasan untuk membubarkan HMI. 
                        </div>
                        <div class="clear"></div>
                        

                        
                        
                    </p>

               '
            ],

            'block5'=>[
                'title'=>'Para Tokoh dan Masa keemasan HMI',
                'narasi'=>'
                    <img src="http://www.andriewongso.com/files/uploads/articles/Nurcholish%20Madjid,%20Cendekiawan%20Indonesia_2014-08-29%2013:50:34_640x321-Nurcholis%20Madjid.jpg" class="pic">
                    <p>Era Orde Baru merupakan masa-masa ke-emasan di HMI. Di periode inilah HMI mencapai puncak kesuksesan baik secara intelektual maupun secara gerakan. HMI aktif dalam forum diskusi, kajian serta turun aksi. Intelektual di HMI disimbolkan dengan munculnya tokoh seperti Nurcholish Madjid. Meskipun tidak bisa ditampik, bahwa intelektual lainnya di HMI juga tidak bisa dianggap enteng.</p>
                    <p>Keberhasilan Nurcholis Madjid yang akrab di sapa Cak Nur pada tahun 1966-1969 yang berhasil membuat satu inovasi baru dalam merumuskan Ideologi HMI, dengan lahirnya Nilai Dasar Perjuangan (NDP) sebagai hasil dari buah pemikiran caknur hingga gagasan tersebut di resmikan sebagai Ideologi HMI sampai saat ini.</p>
                    <img src="http://media.viva.co.id/thumbs2/2013/02/06/191163_mahfud-md-diapit-akbar-tandjung-dan-jusuf-kalla_663_382.jpg" class="pic2">
                    <p>Di era ini jugalah HMI produktif melahirkan tokoh-tokoh nasional sampai sekarang. Munculnya tokoh sekelas Akbar Tanjung dan Anas Urbaningrum yang disimbolkan sebagai politisi kelas atas. Sementara dari kelompok pengusaha-penguasa muncul nama seperti Jusuf Kalla dan Bahlil Lahadalia (ketua Hipmi). </p>
                    <img src="http://bulan-bintang.org/wp-content/uploads/2012/12/presidium-kahmi-32.jpg" class="pic">
                    <p>Tokoh lainnya di penegakan hukum lahirlah nama-nama besar seperti Jimly Asshidiqie, Mahfud MD,  dan Yusril Ihza Mahendra sedangkan dari golongan akademisi muncul nama-nama seperti Azyumardi Azra, Anies Baswedan, Komaruddin Hidayat. Selain itu, tokoh-tokoh pentolan HMI juga banyak menduduki kursi di legislatif dan eksekutif bahkan sampai di tingkat yudikatif hingga sekarang.</p>
                '
            ],

            'block6'=>[
                'title'=>'Kongres HMI Dari Masa ke Masa',
                'narasi'=>'
                <img src="http://images.detik.com/customthumb/2013/03/15/10/180621_hmi2.jpg?w=460" class="pic">
                <div class="clear"></div>
                <p><b>Kongres Sebelum Dualisme Kepengurusan</b></p>
                <ul>
                    <li class="nodes">Kongres ke-1 di Yogyakarta pada tanggal 30 November 1947, dengan ketua terpilih HS Mintareja</li>
                    <li class="nodes">Kongres ke-2 di Yogyakarta pada tanggal 15 Desember 1951, dengan ketua terpilih A. Dahlan Ranuwiharja</li>
                    <li class="nodes">Kongres ke-3 di Jakarta pada tanggal 4 September 1953 dengan formatur terpilih Deliar Noer</li>
                    <li class="nodes">Kongres ke-4 di Bandung pada tanggal 14 Oktober 1955 dengan formatur terpilih Amir Rajab Batubara</li>
                    <li class="nodes">Kongres ke-5 di Medan pada tanggal 31 Desember 1957 dengan formatur terpilih Ismail Hasan Metareum</li>
                    <li class="nodes">Kongres ke-6 di Makassar (Ujungpandang) pada tanggal 20 Juli 1960 dengan formatur terpilih Nursal</li>
                    <li class="nodes">Kongres ke-7 di Jakarta pada tanggal 14 September 1963 dengan formatur terpilih Sulastomo</li>
                    <li class="nodes">Kongres ke-8 di Solo (Surakarta) pada tanggal 17 September 1966 dengan formatur terpilih Nurcholish Madjid</li>
                    <li class="nodes">Kongres ke-9 di Malang pada tanggal 10 Mei 1969 dengan formatur terpilih Nurcholish Madjid</li>
                    <li class="nodes">Kongres ke-10 di Palembang pada tanggal 10 Oktober 1971 dengan formatur terpilih Akbar Tanjung</li>
                    <li class="nodes">Kongres ke-11 di Bogor pada tanggal 12 Mei 1974 dengan formatur terpilih Ridwan Saidi</li>
                    <li class="nodes">Kongres ke-12 di Semarang pada tanggal 16 Oktober 1976 dengan formatur terpilih Chumaidy Syarif Romas</li>
                    <li class="nodes">Kongres ke-13 di Makassar (Ujungpandang) pada tanggal 12 Februari 1979 dengan formatur terpilih Abdullah Hehamahua</li>
                    <li class="nodes">Kongres ke-14 di Bandung pada tanggal 30 April 1981 dengan formatur terpilih Ahmad Zacky Siradj</li>
                    <li class="nodes">Kongres ke-15 di Medan pada tanggal 26 Mei 1983 dengan formatur terpilih Harry Azhar Aziz</li>
                    <li class="nodes">Kongres ke-16 di Padang pada tahun 1986, dengan formatur terpilih M. Saleh Khalid, terpecahnya HMI menjadi dua yakni HMI DIPO dan HMI MPO</li>
                </ul>
                
                <div class="clear"></div>
                <p><b>Kongres HMI DIPO</b></p>
                <ul>
                    <li class="nodes">Kongres ke-17, di Lhokseumawe, Aceh (6 Juli 1988) dengan formatur terpilih Herman Widyananda</li>
                    <li class="nodes">Kongres ke-18, di Jakarta (24 september 1990)dengan formatur terpilih Ferry Mursyidan Baldan</li>
                    <li class="nodes">Kongres ke-19, di Pekan baru (09 Desember 1992)dengan formatur terpilih M. Yahya Zaini</li>
                    <li class="nodes">Kongres ke-20, di Surabaya (29 Januari 1995)dengan formatur terpilih Taufik Hidayat</li>
                    <li class="nodes">Kongres ke-21 di Yogyakarta (26 Agustus 1997), dengan formatur terpilih Anas Urbaningrum</li>
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7dDnwukbwFrd-L4U272kyuOce1LE20lPZ27wjJ7wnrp8XRtAUkQ " class="pic2">
                
                    <li class="nodes">Kongres ke-22 di Jambi (03 Desember 1999), dengan formatur terpilih Fakhruddin</li>
                    <li class="nodes">Kongres ke-23 di Balikpapan (30 April 2002), dengan formatur terpilih Cholis Malik</li>
                    <li class="nodes">Kongres ke-24 di Jakarta (23 Oktober 2003), dengan formatur terpilih Hasanuddin</li>
                    <li class="nodes">Kongres ke-25 di Makassar (20 Februari 2006), dengan formatur Terpilih Fajar R Zulkarnaen</li>
                    <li class="nodes">Kongres ke-26 di Palembang (28 Juli 2008), dengan formatur terpilih Arip Musthopa</li>
                    <li class="nodes">Kongres ke-27 Depok pada tanggal 5 - 10 November 2010, dengan formatur terpilih Noer Fajriansyah</li>
                    <li class="nodes">Kongres ke-28 Jakarta pada tanggal 15 Maret - 15 April 2013, dengan formatur terpilih Arief Rosyid Hasan</li>
                </ul>


                <div class="clear"></div>
                <p><b>Kongres HMI MPO</b></p>
                <ul>
                    <li class="nodes">Kongres ke-16 di Yogyakarta pada tahun 1986, Ketua Umum : Eggy Sudjana</li>
                    <li class="nodes">Kongres ke-17 di Yogyakarta pada tanggal 5 Juli 1988, Ketua Umum : Tamsil Linrung</li>
                    <li class="nodes">Kongres ke-18 di Bogor pada tanggal 10 Oktober 1990, Ketua Umum : Masyhudi Muqarrabin</li>
                    <li class="nodes">Kongres ke-19 di Semarang pada tanggal 24 Desember 1992, Ketua Umum : Agusprie Muhammad</li>
                    <li class="nodes">Kongres ke-20 di Purwokerto pada tanggal 27 April 1995, Ketua Umum : Lukman Hakim Hassan</li>
                    <li class="nodes">Kongres ke-21 di Yogyakarta pada tanggal 28 Juli 1997, Ketua Umum : Imron Fadhil Syam</li>
                    <li class="nodes">Kongres ke-22 di Jakarta pada tanggal 26 Agustus 1999, Ketua Umum : Yusuf Hidayat</li>
                    <li class="nodes">Kongres ke-23 di Makassar pada tanggal 25 Juli 2001, Ketua Umum : Morteza Syafinuddin Al-Mandary</li>
                    <li class="nodes">Kongres ke-24 di Semarang pada tanggal 11 September 2003, Ketua Umum : Cahyo Pamungkas</li>
                    <li class="nodes">Kongres ke-25 di Palu pada tanggal 17 Agustus 2005, Ketua Umum : Muzakkir Djabir</li>
                    <li class="nodes">Kongres ke-26 di Jakarta Selatan pada tanggal 16 Agustus 2007, Ketua Umum : Syahrul Effendi Dasopang</li>
                    <li class="nodes">Kongres ke-27 di Yogyakarta pada tanggal 9 Juni 2009, Ketua Umum : Muhammad Chozin Amirullah</li>
                    <li class="nodes">Kongres ke-28 di Pekanbaru, Riau tanggal 14 - 19 Juni 2011, Ketua Umum : Alto Makmuralto</li>
                    <li class="nodes">Kongres ke-29 di Bogor pada tanggal 27 Juni 2013, Ketua Umum : Puji Hartoyo</li>

                </ul>



                '
            ],

            'right1'=>[
                'title'=>'Profil HMI',
                'narasi'=>'
                    <img src="http://3.bp.blogspot.com/-ohmE_M0Y-18/UazTjDEMxpI/AAAAAAAAACY/sJfiisgkUoc/s798/35Logo+HMI.jpg" class="picprofil" >
                        <table style="margin-left: 15px;margin-right: 15px;">
                            <tr>
                                <td style="width:90px;">Nama</td>
                                <td style="width:15px;">:</td>
                                <td>HMI</td>
                            </tr>
                            <tr>
                                <td>Prakarsa</td>
                                <td>:</td>
                                <td>Lafran Pane</td>
                            </tr>
                            <tr>
                                <td>Slogan</td>
                                <td>:</td>
                                <td>Yakin Usaha Sampai</td>
                            </tr>
                            <tr>
                                <td>Tagline</td>
                                <td>:</td>
                                <td>Iman, Ilmu, Amal</td>
                            </tr>
                            <tr>
                                <td>Pembentukan</td>
                                <td>:</td>
                                <td>5 Februari 1947 M / 14 Rabiul Awal 1366 H</td>
                            </tr>
                            <tr>
                                <td>Jenis</td>
                                <td>:</td>
                                <td>Organisasi Kemahasiswaan, Perkaderan dan Perjuangan</td>
                            </tr>                    
                            <tr>
                                <td>Falsafah Dasar</td>
                                <td>:</td>
                                <td>Nilai-Nilai Dasar Perjuangan (NDP)</td>
                            </tr>                    
                            <tr>
                                <td>Tujuan</td>
                                <td>:</td>
                                <td>Terbinanya insan akademis, pencipta, pengabdi yang bernafaskan Islam dan bertanggung jawab atas terwujudnya masyarakat adil makmur yang diridhoi Allah Subhanahu wata\'ala.</td>
                            </tr>                    
                            <tr>
                                <td>Kantor pusat</td>
                                <td>:</td>
                                <td>Jakarta, Indonesia</td>
                            </tr>                    
                            <tr>
                                <td>Ketua Umum</td>
                                <td>:</td>
                                <td>Mulyadi P Tamsir<br/>(2015-2017)</td>
                            </tr>  
                            <tr>
                                <td>Sekretaris Jenderal</td>
                                <td>:</td>
                                <td>Amijaya</td>
                            </tr>                                        
                        </table>
                '
            ],

            'block9'=>[
                'title'=>'Lafran Pane Konseptor HMI',
                'narasi'=>'
                    <img src="http://hminews.com/wp-content/uploads/kahmi-lafran-pane.jpg" class="picprofil">
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Lafran Pane lahir di kampung Pagurabaan, Tapanuli Selatan, Padang Sidempuan pada 5 Februari 1922. Lafran Pane berasal dari lingkungan keluarga yang nasionalis-muslim telah membentuk pribadi pane yang nasionalis religius. Itu terlihat saat Lafran Pane mengagas dan membentuk HMI, diawali dengan keadaan kebanyakan mahasiswa yang terjadi polarisasi dan kecenderungan berpikiran sosialis. Pane yang saat itu mahasiswa dari STI Yogyakarta dan menjadi salah satu anggota dari pengurus PMY melihat harus adanya organisasi yang meminimalisir pertentangan antara pemikiran agama dan nasional. Maka Ia mengagaas untuk terbentuknya sebuah organisasi keagamaan. Gagasan tersebut yang kemudian menjadi cikal bakal terbentuknya Himpunan Mahasiswa Islam pada 5 Februari 1947. </p>
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Terbentuknya HMI tersebut bagi Lafran Pane ingin menjembatani jurang pemisah antara kelompok nasionalis dan Islam yang terjadi selama, khususnya yang terjadi di kalangan mahasiswa. Selain itu, Lafran Pane juga ingin beranggapan bahwa islam dapat mengisi hajat hidup manusia serta menyelaraskan Islam dengan kebudayaan. Pandangan keislaman dan kebangsaan yang dimiliki oleh Lafran Pane juga menjadi pergerakan awal dari dari HMI dan terlihat sampai sekarang.</p>


                '
            ],    
            'block10'=>[
                'title'=>'Ketum Terpilih 2015-2017',
                'narasi'=>'
                    <img src="http://www.bijaks.net/assets/images/hotpages/kongreshmi/13-hari-kongres-mulyadi-tamsir-jadi-ketua-umum-pb-hmi-2015-2017.jpg" class="picprofil">
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Setelah beberapa hari molor dari agenda sebenarnya, akhirnya ketua umum terpilih PB HMI selesai pada Sabtu, 05/11/2015. Sebelumnya, pada Rabu (02/11/2015) Koorps HMI-Wati (KOHATI) juga telah berhasil memilih pemimpinnya yang juga sempat berlangsung alot.</p>
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Kongres mengantarkan Mulyadi P Thamsir, dari Cabang Sintang, Kalimantan Barat terpilih menjadi Ketua Umum PB HMI periode 2015-2017. Sementara di tubuh Kohati juga terpilih Farihatin yang merupakan mantan ketua umum Kohati Badko Kalimantan Barat.</p>
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Seperti memang ditakdirkan, baik Kongres maupun Munas PB HMI kali ini menelurkan kader terbaik dari Kalimantan Barat, yang merupakan pertama dalam sejarah. </p>
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Mulyadi P Tamsir sebelumnya adalah Sekretaris Jenderal PB HMI pada masa jabatan Arief Rosyid 2013-2015, dia juga tengah menyelesaikan kuliah S2 nya di Universitas Trisakti dengan jurusan Ekonomi.</p>
                    <p class="rightcol font_kecil" style="margin-right: 10px;">Sementara, Farihatin sebelumnya menjabat Wasekjend PB HMI. Perempuan dari Kota Pontianak ini juga merupakan mantan ketua umum Kohati Badko Kalimantan Barat yang kini tercatat sebagai mahasiswa magister pendidikan Universitas Negeri Jakarta (UNJ).</p>

                '
            ],    

            'institusiPendukung' => [
                ['link'=>'http://www.bijaks.net/aktor/profile/himpunanmahasiswaislam534f53bc9109e','image'=>'https://upload.wikimedia.org/wikipedia/id/thumb/8/8c/Lambang_HMI.jpg/100px-Lambang_HMI.jpg','title'=>'Himpunan Mahasiswa Islam'],
                ['link'=>'http://www.bijaks.net/aktor/profile/pemprovriau54d97aacb84e9','image'=>'http://infopku.com/wp-content/uploads/2014/08/335px-Riau_COA.svg_.png','title'=>'Pemrov Riau '],
            ],

           // 'partaiPendukung' => [
           //     ['link'=>'http://www.bijaks.net/aktor/profile/partaigolongankarya5119aaf1dadef ','image'=>'http://fajar.co.id/wp-content/uploads/2015/08/golkar324.jpg','title'=>'GOLKAR'],
           // ],
            

            'institusiPenentang' => [
                ['link'=>'#','image'=>'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpf1/t5.0-1/41592_101960588352_4415322_n.jpg','title'=>'KAHMI'],
            ],

            // 'partaiPenentang' => [
            //     ['link'=>'http://www.bijaks.net/aktor/profile/partaidemokrasiindonesiaperjuangan5119ac6bba0dd','image'=>'http://www.faktapost.com/foto_berita/61PDIP.png','title'=>'PDI-P'],
            //     ['link'=>'#','image'=>'http://www.mpr.go.id/uploads/fraksi/hanura.jpg','title'=>'HANURA'],
            //     ['link'=>'http://www.bijaks.net/aktor/profile/nasdem5119b72a0ea62','image'=>'http://2.bp.blogspot.com/-6WMmJUYkwHs/URTRolnNByI/AAAAAAAAS8E/1RaV9_ROfkA/s1600/nasdem_biru.png','title'=>'NASDEM'],
            //     ['link'=>'http://www.bijaks.net/aktor/profile/partaikebangkitanbangsa5119b257621a4','image'=>'http://1.bp.blogspot.com/-d9_P-ijkhgY/UTFOvvHOxsI/AAAAAAAABSM/ECen89I0mdc/s1600/PKB+Logo+Vektor+Partai+Kebangkitan+Bangsa.jpg','title'=>'PKB'],
            // ],

            // 'negaraPenentang' => [
            //     ['link'=>'','image'=> 'https://spektrumku.files.wordpress.com/2008/06/us-flag.jpg','title'=>'AS'],
            // ],

            'quotePendukung'=>[
                ['from'=>'salah satu Oknum HMI yang membawa senjata tajam','jabatan'=>'','img'=>'http://gagasanriau.com/wp-content/uploads/2015/01/HMI-MPO-Cabang-Pekanbaru-Saat-Menyampaikan-Orasinya.jpg','url'=>'#','content'=>'"Sengaja dibawa dan dipersiapkan dari daerah asal. Ini untuk menghadapi situasi yang ada di Pekanbaru, kalau sewaktu-waktu terjadi rusuh"'],
                ['from'=>'Jusuf Kalla','jabatan'=>'Wakil Presiden Indonesia','img'=>'http://www.bijaks.net/public/upload/image/politisi/drshmuhammadjusufkalla50ee870b99cc9/badge/6258b973c0c66b2d579434124018f46c3edd0f44.jpg ','url'=>'http://www.bijaks.net/aktor/profile/drshmuhammadjusufkalla50ee870b99cc9','content'=>'"Ya, di mana-mana, ini kan generasi muda, dan ini bagi daerah juga investasi. Investasi bahwa bagaimana generasi muda itu mengetahui Riau, Jika besok-besok mereka ini jadi pengusaha, pemerintah pasti juga memberikan partisipasi, dan ini bukan diberikan pribadi. Ini, kan, semuanya juga membantu generasi muda di seluruh Indonesia"'],
                ['from'=>'M Arief Rosyid Hasan','jabatan'=>'Ketua Umum Pengurus Besar Himpunan Mahasiswa Islam','img'=>'http://i1.wp.com/www.okeebos.com/wp-content/uploads/2014/10/HMI.jpg?resize=660%2C330','url'=>'http://www.bijaks.net/aktor/profile/muhammadariefrosyidhasan56646c758520f','content'=>'"Permasalahan ini seharusnya tidak terjadi. Kejadian ini disebabkan adanya miskomunikasi antara stakeholder dalam kegiatan ini"'],
                ['from'=>'Andi Rachman','jabatan'=>'Plt Gubernur Riau','img'=>'https://upload.wikimedia.org/wikipedia/id/thumb/2/26/Arsyadjuliandi.png/250px-Arsyadjuliandi.png','url'=>'http://www.bijaks.net/aktor/profile/arsyadjuliandirachman55f66895579c2','content'=>'"Inikan dalam mendukung kegiatan Pemuda, acara bertaraf nasional bahkan ada tamu dari Internasional. Jangan dilihat dari anggarannya, tapi dilihat dari multiplier efek acara itu"'],
                ['from'=>'Fatharyanto Lisda, S.Sos, Mkrim','jabatan'=>'ketua PANASKO HmI ke-29','img'=>'http://www.lantangriau.com/wp-content/uploads/2015/06/fatharyanto-Lisda.jpg','url'=>'#','content'=>'"Ini merupakan suatu kebanggaan putra terbaik Riau diamanahkan menjadi penyelenggara acara akbar dua tahunan Himpunan Mahasiswa Islam"'],
            ],

            'quotePenentang'=>[
                ['from'=>'Viva Yoga Mauladi','jabatan'=>'Presidium Majelis Nasional Korps Alumni Himpunan Mahasiswa Islam (KAHMI)','img'=>'http://news.bijaks.net/uploads/2015/09/Viva-Yoga-Mauladi.jpg','url'=>'http://www.bijaks.net/aktor/profile/vivayogamauladimsi50fba1f8b3b6f','content'=>'"Sebaiknya jangan terulang. Mari kita tunjukkan bahwa kader HMI adalah kaum intelektual, progresif, dan independen, Kader HMI harus memberi keteladanan dalam hidup berdemokrasi karena HMI berdiri sejak 5 Februari 1947"'],
                ['from'=>'Akbar Tanjung','jabatan'=>'Mantan Ketua Umum Himpunan Mahasiswa Indonesia (HMI)','img'=>'https://bangakbartandjung.files.wordpress.com/2014/03/akbar-tandjung-12.jpg','url'=>'http://www.bijaks.net/aktor/profile/akbartanjung503c2d29aaf6b','content'=>'"Saya sudah hubungi PB HMI dan Sekjennya untuk bisa ketemu malam ini"'],
                ['from'=>'Prof. Mahfud MD','jabatan'=>'Koordinator Presidium Majelis Korps Alumni Himpunan Mahasiswa Islam (HMI)','img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20151122_034035_harianterbit_20151004_024306_harianterbit_Mahfud_MD.jpg','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b','content'=>'"Berkongreslah dengan benar karena mungkin respons dan kesimpulan banyak orang jangan-jangan selama ini salah"'],
                ['from'=>'Usman','jabatan'=>'koordinator Forum Indonesia untuk Transparansi Anggaran (Fitra) Riau','img'=>'http://www.halloriau.com/foto_berita/21Usman%20FITRA.jpg','url'=>'#','content'=>'"Dana Rp 4 miliar itu bukan anggaran sedikit apalagi melihat HMI adalah organisasi mahasiswa. Maka anggaran diminta HMI tersebut tidak wajar bagi sekelas organisasi mahasiswa"'],
                ['from'=>'M Arief Rosyid Hasan','jabatan'=>'Ketua Umum Pengurus Besar Himpunan Mahasiswa Islam','img'=>'http://i1.wp.com/www.okeebos.com/wp-content/uploads/2014/10/HMI.jpg?resize=660%2C330','url'=>'http://www.bijaks.net/aktor/profile/muhammadariefrosyidhasan56646c758520f','content'=>'"Selaku Ketua Umum PB HMI meminta maaf kepada masyarakat Pekanbaru, Riau, jika ada yang kurang berkenan terhadap pelaksanaaan kongres"'],
                ['from'=>'Azhar','jabatan'=>'Ketua Harian Lembaga Adat Melayu Riau','img'=>'http://riauekbis.com/wp-content/uploads/2013/05/alazhar.jpg','url'=>'#','content'=>'"Perbuatan mereka tidak beradat. Meraja-raja di kampung raja. Menghulu-hulu di kampung penghulu. Rombongan liar sama dengan lanun"'],
                ['from'=>'Sudirman, S.Pdi','jabatan'=>'mantan pengurus Badko HMI Riau-Kepri','img'=>'http://www.lantangriau.com/wp-content/uploads/2015/11/Safrizal-Nst.jpg','url'=>'#','content'=>'"Banyak alasan kami menolak kongres HMI di Pekanbaru, pertama, tentang anggaran dana kongres, saya pesimis, menurut saya anggaran 2 Milyar itu hanya spekulasi, kecuali kalau sudah tertulis di buku lintang"'],
                ['from'=>'Safrizal Nasution','jabatan'=>'sekretaris Komite Nasional Pemuda Indonesia (KNPI)','img'=>'http://cdn-2.tstatic.net/pekanbaru/foto/bank/images/knpi-logo.jpg','url'=>'#','content'=>'"Sebaiknya mahasiswa yang diindikasi melakukan kerusuhan yang sebagian besar berasal dari Sulawesi tersebut sebaiknya dipulangkan saja"'],
                ['from'=>'Ray Rangkuti','jabatan'=>'pengamat politik dari Lingkar Madani (Lima)','img'=>'http://news.bijaks.net/uploads/2015/09/ray-rangkut.jpg','url'=>'http://www.bijaks.net/aktor/profile/rayrangkuti525f3c7e380f7','content'=>'"Mestinya HMI menggelontorkan dananya berapa yang dibutuhkan, kalau dananya sebesar Rp3 miliar buat mereka semua sangat tidak elok bagi organisasi seperti HMI. Sangat tidak pantas"'],

            ],

            'video'=>[
                        ['id'=>'nJ56Gl7mgNc'],
                        ['id'=>'s9nq0EutBIU'],
                        ['id'=>'_CMeOjBRBcI'],
                        ['id'=>'d9JuGKduqKg'],
                        ['id'=>'p234yWkQZqQ'],
                        ['id'=>'hkkmDTVV_70'],
                        ['id'=>'fDQDuYr0Awc'],
                        ['id'=>'RjAkLyDzw6A'],
                        ['id'=>'VPT7tGpWoic'],
                        ['id'=>'UHzR7nWv8Lg'],
                        ['id'=>'2WVRN4vjYVg'],
                        ['id'=>'Yriyy7mwntc'],
                        ['id'=>'9_AoumdmgxM'],
                        ['id'=>'cjzwO09QyQY'],
                        ['id'=>'Mf03vQnjM68'],
                        ['id'=>'ewkn0rK7glA'],
                        ['id'=>'RlzLYlIhbK0'],
                        ['id'=>'8xkk4Vnzzio'],
                        ['id'=>'t-7G8WzjSDU'],
                        ['id'=>'p7Ndv0E8W-4'],
                        ['id'=>'HkDkyw1tknA'],
            ],

            'foto'=>[
                        ['img'=>'http://analisadaily.com/assets/image/news/big/2015/11/panitia-akan-ajukan-izin-perpanjangan-kongres-hmi-di-pekanbaru-192067-1.jpg'],
                        ['img'=>'http://wahanariau.com/wp-content/uploads/2015/10/IMG_20151008_001749-635x350.jpg'],
                        ['img'=>'http://cdn-2.tstatic.net/makassar/foto/bank/images/hmi-ngamuk-pekanbaru_20151121_231821.jpg'],
                        ['img'=>'http://analisadaily.com/assets/image/news/big/2015/11/kecewa-dengan-panitia-kongres-hmi-makassar-buat-rusuh-di-pekanbaru-190582-1.jpg'],
                        ['img'=>'http://images.detik.com/community/media/visual/2015/11/21/d3d2908b-9278-4411-bd93-449e4619be14_169.jpg?w=780&q=90'],
                        ['img'=>'http://data.seruu.com/images/seruu/article/2015/11/24/hmi(2).jpg'],
                        ['img'=>'http://www.jejamo.com/wp-content/uploads/2015/11/Kerusahan-HMI-Pekanbaru.jpg'],
                        ['img'=>'http://inforiau.co/assets/berita/59767174301-hmi_rusuh.jpg'],
                        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/09/03/12/1040375/jokowi-jk-akan-hadiri-kongres-hmi-di-pekanbaru-18g.jpg'],
                        ['img'=>'http://img.antaranews.com/new/2015/11/ori/20151122493.jpg'],
                        ['img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTchrA2dmWTmNoT-NceMdY3KtifyaNwbW1Lbnvbv6DkgKM6mdMaTQ'],
                        ['img'=>'https://i.ytimg.com/vi/M00CtsRfvFI/hqdefault.jpg'],
                        ['img'=>'http://www.potretnews.com/assets/news/22112015/potretnewscom_tdjrb_1472.jpg'],
                        ['img'=>'http://www.potretkepri.com/wp-content/uploads/2015/11/kongres-hmi.jpg'],
                        ['img'=>'http://www.lantangriau.com/wp-content/uploads/2015/06/gambar-makan-HmI-300x300.jpg'],
                        ['img'=>'http://www.panasko-hmi.com/wp-content/uploads/2015/10/banner-2.jpg'],
                        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmT9Iic44C8KXjEw2NDEOoIfIU6fkoE-uQKeepAz14kW9RDnyx'],
                        ['img'=>'http://petahmelayu.com/wp-content/uploads/2015/11/f-senjata.jpg'],
                        ['img'=>'http://radarpekanbaru.com/assets/berita/large/41155542340-fb_img_1448385720908.jpg'],
                        ['img'=>'https://upload.wikimedia.org/wikipedia/id/4/41/Lafran-pane.jpg'],
                        ['img'=>'http://profile.ak.fbcdn.net/hprofile-ak-ash2/27463_100000892976906_1602_n.jpg'],
                        ['img'=>'http://3.bp.blogspot.com/-I6JoxiFdPV8/VRmbMaVe91I/AAAAAAAAAKA/S7mvXLhYUVk/s1600/255756_257003144445105_244588631_n.jpg'],
                        ['img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTvhoYiFNQofWQh1pMTNQO4_UljfLGUMZVqhJRJWT7nZbZnvZzmzg'],
                        ['img'=>'http://2.bp.blogspot.com/-US9WdHj4OVU/TfL3jmlw1dI/AAAAAAAAABg/d_eHNI0bPos/s200/lafran+pane.JPG'],
                        ['img'=>'http://2.bp.blogspot.com/_cPo4hK2ewnk/S-37GJq6qrI/AAAAAAAAAD4/xhcdEbpLFFI/s1600/HMI+tua_2.JPG'],
                        //['img'=>'http://s.kaskus.id/images/2014/03/11/6330872_20140311031351.jpg'],
                        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/20130315_Kongres_Himpunan_Mahasiswa_Islam_ke-28_2672.jpg'],
                        ['img'=>'http://images.detik.com/customthumb/2013/03/15/10/180621_hmi2.jpg?w=460'],
                        ['img'=>'http://img.bisnis.com/posts/2015/12/05/498833/hm11.jpg'],
                        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2013/01/15/122954/122954.jpg?w=668'],
                        ['img'=>'http://www.jpnn.com/picture/watermark/20151126_085310/085310_571183_Kongres_HMI_tak_Jelas_d.jpg'],
                        ['img'=>'https://mfakhruddinmuhdi.files.wordpress.com/2011/03/suasana_kongres_hmi_jambi-1999.jpg'],
                        ['img'=>'http://cdn.ar.com/images/stories/2013/03/HMI_demo_tolak_SBY.jpeg'],
                        //['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2013/04/18/178498/670x335/cerita-kisruh-kongres-hmi-yang-berlangsung-sebulan-lebih.jpg'],
                        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2013/03/15/138568/138568.jpg?w=668'],
                        ['img'=>'https://img.okezone.com/content/2015/12/02/340/1259383/panitia-dilempari-kursi-sidang-kongres-hmi-ricuh-wjlALSTjYA.jpg'],
                        //['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/11/28/630011/670x335/kongres-hmi-ricuh-satu-peserta-berlumuran-darah-di-wajah.jpg'],
                        ['img'=>'https://dwikisetiyawan.files.wordpress.com/2009/01/pb-hmi-1997-1999-pembukaan-kongres-hmi-jambi-tampak-depan-dari-kiri-mahadi-sinambela-m-amien-rais-anas-dan-alm-anniswati-m-kamaluddin.jpg'],
                        ['img'=>'http://redaksikota.com/wp-content/uploads/2015/11/Persiapan-Kongres-HMI.jpg'],
                        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/1075512/big/093932300_1449223601-kongres_HMI_ke-19.jpg'],
            ],
        /*
           'kronologi'=>[
               'list'=>[
                   ['date'=>'8 Juni 2015','content'=>'Anggota DPR berinisial SN, bersama dengan seorang pengusaha, telah beberapa kali memanggil serta bertemu dengan pimpinan PT Freeport Indonesia. Hari Senin, 8 Juni, adalah pertemuan ketiga. 
        Tepatnya pada pukul 14:00-16:00 WIB di sebuah hotel di kawasan SCBD, Jakarta Pusat, anggota DPR itu menjanjikan cara penyelesaian tentang kelanjutan kontrak PT FI dan meminta PT FI memberikan saham pada Jokowi dan Kalla.
        Anggota DPR ini menjanjikan sebuah cara penyelesaian kepada pihak yang sedang bernegosiasi dengan RI, sembari meminta saham perusahaan dan saham proyek pembangkit listrik'],
                   ['date'=>'16 November 2015','content'=>'Sudirman melaporkan anggota DPR berinisial SN tersebut — yang diduga sebagai Ketua DPR Setya Novanto — ke MKD hari ini. Namun Sudirman menolak menyebut nama Setya Novanto secara terang-terangan.
        Sore harinya, Jusuf Kalla dijadwalkan menerima Ketua DPR RI Setya Novanto di Kantor Wakil Presiden, Jalan Medan Merdeka Utara, Jakarta Pusat. 
        Berdasarkan informasi dari situs resmi Sekretariat Wakil Presiden pada Senin, Setya Novanto menemui Kalla pada pukul 15:00 WIB. Namun Setya membantah tudingan tersebut.'],
                   ['date'=>'17 November 2015','content'=>'Direktur Advokasi Pusat Studi Hukum dan Kebijakan Indonesia (PSHK) Ronald Rofiandri mendesak Setya Novanto mengundurkan diri sementara sebagai Ketua DPR RI untuk mempermudah proses pemeriksaan oleh MKD.
        Sementara itu, usai bertemu Setya di kantornya kemarin, Kalla menyatakan akan menunggu proses lebih lanjut di DPR.'],
                   ['date'=>'17 November 2015','content'=>'Nama Menteri Koordinator Politik, Hukum, dan Keamanan Luhut Binsar Panjaitan disebut mengetahui saham yang akan diberikan untuk Presiden Jokowi dan Wakil Presiden Jusuf Kalla.
        Dalam transkrip rekaman tersebut, pengusaha bernama Reza yang hadir bersama Setya Novanto menyebutkan keterlibatan Luhut dalam besaran saham untuk Pembangkit Listrik Tenaga Air (PLTA).
        Rencananya, mereka mencari referensi yang dapat bekerjasama dengan PT. Freeport Indonesia. Dalam skenario ini Freeport hanya akan memiliki saham sebesar 51 persen.'],
                   ['date'=>'19 November 2015','content'=>'Luhut juga membantah ada pertemuan antara dia dengan pihak Freeport.  Selain Luhut, nama Deputi I Kepala Staf Kepresidenan Darmawan Prasodjo juga disebut. Saat itu ia adalah anak buah Luhut, saat ia menjabat sebagai Kepala Staf Kepresidenan'],
                   ['date'=>'23 November 2015','content'=>'Rapat kasus dugaan pelanggaran kode etik Setya dijadwalkan akan digelar internal oleh Mahkamah Kehormatan Dewan (MKD). 
        Namun, anggota dan pimpinan MKD justru mempermasalahkan keabsahan Menteri Energi dan Sumber Daya Mineral Sudirman Said sebagai pelapor. Ketua Mahkamah Surahman Hidayat menuturkan, keabsahan pelapor itu tercantum dalam pasal 5 bab IV Peraturan Tata Beracara Mahkamah. 
        MKD selanjutnya akan mengadakan rapat lagi pada tanggal 24 November dan memanggil ahli hukum untuk mendalami peraturan tersebut. 
        MKD juga mempermasalah keabsahan barang bukti. Wakil Ketua Mahkamah Hardisoesilo menyebut laporan dan barang bukti berbeda. Durasi rekaman tertulis 120 menit di laporan tapi durasi barang bukti hanya 11 menit 38 detik
        '],
               ]
           ],
        */
        ];       

        $rkey = 'news:list:headline';
        $where = "AND tc.title LIKE '%kongres hmi%'";
        $news = $this->content_model->getNewsHotIssue($where, 0, 10); $resnews = array();
        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value['news_id']);
            $news_array = json_decode($rowNews, true);
            $news_array['entry_date'] = $value['entry_date'];
            $resnews[$i] = $news_array;
            $i++;
        }
        $data['news'] = $resnews;        

        $html['html']['content'] = $this->load->view('hotpages/kongreshmi', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }        

    public function menguji_pilkada()
    {
        $rkey = 'news:list:headline';
        $where = "AND tc.title LIKE '%pilkada%' OR tc.title LIKE '%serentak%'";
        $news = $this->content_model->getNewsHotIssue($where, 0, 10); $resnews = array();
        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value['news_id']);
            $news_array = json_decode($rowNews, true);
            $news_array['entry_date'] = $value['entry_date'];
            $resnews[$i] = $news_array;
            $i++;
        }
        $data['news'] = $resnews;        

        $html['html']['content'] = $this->load->view('hotpages/menguji_pilkada', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }
}