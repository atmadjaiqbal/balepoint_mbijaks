<h4 class="kanal-title kanal-title-gray">PERISTIWA KEBRUTALAN ISIS</h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="isis">
        <img src="http://www.bijaks.net/assets/images/hotpages/isis/event-page-isis-landing.jpg" style="width: 100%;height: auto;">
        <div class="history-title"><b>TAHUN 2015</b></div>
        <div class="history-info">
            <ul>
                <li>Sekitar <b>18 wanita mati</b> dibunuh dan mati dirajam di <b>Mosul, Utara Irak</b></li>
            </ul>
        </div>
        <div class="history-title"><b>APRIL 2015</b></div>
        <div class="history-info">
            <ul>
                <li>Diperkirakan hingga hari ini angka pengikut ISIS sudah mencapai <b>22 ribu orang</b></li>
            </ul>
        </div>
        <div class="history-title"><b>FEBRUARI 2015</b></div>
        <div class="history-info">
            <ul>
                <li>Sekitar <b>2.500 org</b> Etnis Uzbek gabung ISIS. Diantaranya <b>1000 pria-wanita dan 500 etnis Kirgiz</b></li>
                <li><b>Pilot Jordania Muath Al-Kasaesbeh dibakar hidup - hidup</b></li>
            </ul>
        </div>
        <div class="history-title"><b>JANUARI 2015</b></div>
        <div class="history-info">
            <ul>
                <li>Rekruitment anggota 5 negara bekas Uni Sovyet : <b>Kazakhstan, Kirgizstan, Tajikistan, Turkmenistan dan Uzbekistan</b></li>
                <li>Antara <b>2ribu - 4ribu org</b> berangkat ke Wilayah IS jadi teroris</li>
                <li>Sedikitnya <b>2.000 org</b> Asia Tengah gabung ISIS</li>
                <li>Warga Australia sudah <b>160 direkrut</b> di Iran. <b>30 tewas</b> dalam perang ISIS</li>
            </ul>
        </div>
        <div class="history-title"><b>Tahun 2014</b></div>
        <div class="history-info">
            <ul>
                <li>>100 Irak tewas</li>
                <li><b>>11 pejuang ISIS tewas</b> | 1 emir tewas</li>
                <li><b>>4 sipil tewas</b></li>
                <li>Perang dengan Turki & Siriah rebel</li>
                <li>ISIS keluar dari Aleppo, kuasai Ar-Raqqah</li>
                <li>Bom bunuh diri</li>
                <li>ISIS pisah dari Al-Qaeda</li>
                <li>Al-Nusra Front perang melawan ISIS</li>
                <li>Terror di Turki oleh ISIS Albania & perang di Istanbul</li>
                <li>Irak tuduh Arab Saudi & Qatar bantu ISIS</li>
                <li>ISIS mundur dr kota Allepo, Turki, Idlib Siria</li>
                <li><b>Perang dengan tentara Irak dan Suria</b></li>
                <li><b>>7 tewas</b> di eksekusi di kota Ar-Raqqah.</li>
                <li>Beberapa di salib mati & hidup2</li>
                <li><b>>1.700 tentara tewas</b></li>
                <li>>1.300 mahasiwa di sandera</li>
                <li>Kuasai kota Mosul, Fallujah, Tirkit, Tal Afar</li>
                <li>Kantor, bandara, universitas, konsulate diserang</li>
                <li>Jarah USD 429 juta dari bank sentral Mosul</li>
                <li><b>>1.737 tewas</b></li>
                <li>>1.186 sipil tewas | 90 tentara | 21 ISIS</li>
                <li>Hancurkan 2 makam & medjid Agung Shia</li>
                <li>Bom bunuh diri, kontrol ladang gas & minyak</li>
                <li>Abu Bakr al-Baghdadi sebagai khalifah</li>
                <li><b>>6.000 tewas</b></li>
                <li>>60.000 pengungsi</li>
                <li>Wanita di culik sebagai budak sex</li>
                <li>Wanita & anak2 di bunuh & kubur hidup hidup</li>
                <li>Memaksa kaum Yazidi masuk islam atau mati</li>
                <li>Genocide</li>
                <li>ISIS terus melebar di Irak</li>
            </ul>
        </div>
        <div class="history-title"><b>Tahun 2013</b></div>
        <div class="history-info">
            <ul>
                <li><b>>50 tewas, >100 terluka </b>dari bom bunih diri, penculikan</li>
                <li>Serang penjara Abu-Ghraib & bebaskan 500 anggota ISIS</li>
                <li>Serang Turki, Siria Utara & Irak utara</li>
                <li>Komandan Abu Bassir al-Jeblawi tewas</li>
                <li>2 pengungsi Irak di US dituduh bantu ISIS</li>
                <li>Penculikan dokter Jerman</li>
            </ul>
        </div>
        <div class="history-title"><b>Tahun 2009-2012</b></div>
        <div class="history-info">
            <ul>
                <li><b>600 tewas, >1000 terluka dari serangan</b>, bom bunuh diri ISIS</li>
                <li>Bank sentral & Gereja saat misa minggu di serang di Baghdad</li>
                <li>Baghdadi melakukan ekspansi ke Suriah.</li>
                <li>Baghdadi menyatakan penggabungan ISIS dengan Front Al Nusra</li>
                <li>ISIS menyatakan tidak lagi menjadi bagian Al-Qaeda.</li>
                <li>ISIS menyerang sektarian dan AS di bawah pimpinan Baghdadi</li>
                <li>Kepemimpinan ISIS digantikan oleh Abu Bakr al-Baghdadi.</li>
            </ul>
        </div>
        <div class="history-title"><b>Tahun 2007</b></div>
        <div class="history-info">
            <ul>
                <li><b>>2000 sipil tewas</b>, gereja-gereja hancur.</li>
                <li>bom bunuh diri,</li>
                <li>Abu Omar al-Baghdadi ultimatum Iran untuk stop bantu Syiah Irak</li>
                <li>USA mulai operaso "Arrowhead Ripper" leyapkan Al-Qaeda di Irak</li>
                <li>ISIS siapkan administrasi pemerintahan Islam pertama</li>
                <li>Kota Dora dan utara Irak di kontrol ISIS</li>
            </ul>
        </div>
        <div class="history-title"><b>Tahun 2006</b></div>
        <div class="history-info">
            <ul>
                <li>Pemimpin ISIS Zarqawi tewas di tangan tentara Amerika</li>
                <li>Abu Omar al-Baghdadi memimpin ISIS</li>
            </ul>
        </div>

        <div class="history-title"><b>Tahun 2004</b></div>
        <div class="history-info">
            <ul>
                <li>Pemimpin Tauhid, Abu Musab al Zarqawi dari Yordania menyatakan untuk setia kepada Al-Qaeda</li>
                <li>ISIS mulai melancarkan serangan bom kepada pemerintah Irak dan AS.</li>
            </ul>
        </div>
        <div class="history-title"><b>Tahun 2003</b></div>
        <div class="history-info">
            <ul>
                <li>ISIS resmi terbentuk karena serangan Amerika di Irak</li>
                <li>Al Zarqawi umumkan perang dengan Syiah Irak</li>
                <li>Tumbangnya kekuasaan Presiden Saddam Hussein</li>
                <li>Gerakan ISIS berawal dari gerakan Tauhid dan Jihad</li>
            </ul>
        </div>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="visimisi">ISIS : VISI, TUJUAN DAN IDEOLOGI</span></h5>
        <p>VISI : MEMBENTUK NEGARA KEKALIFAHAN ISLAM</b></p>
        <div class="tag-title"><p>TUJUAN ASAL DARI ISIS</p></div>
        <div style="margin-top:10px;">
            <p>Tujuan asal dari ISIS adalah untuk mendirikan sebuah sistem khilafah di kawasan mayoritas Sunni di Irak. Menyusul keterlibatannya di Perang Saudara Syria, tujuan ini kemudian meluas untuk juga bisa mengontrol dan menguasai kawasan mayoritas Sunni di Suriah. Sebuah negara dengan sistem khilafah diproklamasikan pada 29 Juni 2014. Abu Bakar Al-Baghdadi yang saat ini dikenal sebagai Amirul Mukminin Khalifah Ibrahim dideklarasikan dan dilantik sebagai Khalifah. Kelompok ISIS ini lalu berganti nama menjadi Negara Islam.</p>
            <p>Pada tanggal 4 Juli 2014, Persatuan Ulama Muslim Se-Dunia (IUMS), yang dipimpin oleh Syaikh Yusuf Qaradhawi, mengeluarkan pernyataan bahwa deklarasi khilafah yang dilakukan ISIS untuk wilayah di Irak dan Suriah tidak sah secara syariah Islam.</p>
            <p>Pada pertengahan 2014, kelompok ini merilis sebuah video berjudul "The End of Sykes-Picot" berbahasa Inggris kebangsaan Chili bernama Abu Safiya. Video ini mengumumkan niatan kelompok ini untuk menghilangkan semua perbatasan modern antara negara-negara Islam Timur Tengah, khususnya mengacu pada perbatasan yang ditetapkan oleh Perjanjian Sykes-Picot selama Perang Dunia</p>
        </div>
        <hr class="line-mini">
        <div class="tag-title"><p>IDEOLOGI DAN KEYAKINAN</p></div>
        <div style="margin-top:10px;">
            <p>ISIS asalah kelompok ektrimis yang mengikuti ideologi garis keras Al-Qaidah-nya Usamah bin Ladin dan menganut prinsip-prinsip jihad global. Sebagaimana Al-Qaidah dan kelompok jihadis modern yang lain, ISIS muncul dari ideologi Ikhwanul Muslimin (IM), gerakan politik Islam pertama yang berdiri tahun 1920-an di Mesir. ISIS menganut penafsiran Islam yang ekstrim dan anti-Barat. Ia mempromosikan kekerasan atas nama agama dan menganggap mereka yang tidak setuju dengannya sebagai kafir atau murtad. Saat ini, ISIS ingin mendirikan sebuah negara Islam yang berorientasi Salafi Wahabi di Irak, dan Suriah Raya atau Syam.</p>
            <p>Ideologi ISIS berasal dari cabang Islam modern yang bermaksud untuk kembali pada masa awal Islam atau yang dikenal dengan istilah Salafus Sholeh. Gerakan Salafi dibangkitkan kembali oleh Muhammad bin Abdul Wahab asal Arab Saudi</p>
            <p>Ideologi ISIS berasal dari cabang Islam modern yang bermaksud untuk kembali pada masa awal Islam atau yang dikenal dengan istilah Salafus Sholeh. Sebuah gerakan Salafi Ibnu Taimiyah dan dibangkitkan kembali dengan versi yang lebih ekstrim oleh Muhammad bin Abdul Wahab, Arab Saudi, sehingga ideologi ini dikenal dengan istilah Wahabi. Wahabi menolak bid'ah dalam agama yang ia percaya telah merusak spirit Islam. Namun sejumlah pengamat Sunni seperti Zaid Hamid, dan bahkan kalangan mufti Salafi dan mufti kalangan Jihadi seperti Adnan Al-Arur dan Abu Basir Al-Tartusi menyatakan bahwa ISIS dan kelompok teroris lain bukanlah kelompok Sunni tetapi Khawarij yang murtad yang mengabdi pada agenda anti Islam kaum imperialis.</p>
        </div>
        <p style="margin-top:10px;font-size: 9px;text-align: right;">dikutip dari http://www.alkhoirot.net/</p>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="ringkasan">KORBAN</span></h5>
        <p>Observatorium untuk HAM Inggris, mengatakan 2014 merupakan tahun paling mematikan selama empat tahun konflik Suriah :</p>
        <ul style="list-style: square;padding-left: 5px;margin-left: 15px;">
            <li>Kamis (1/1/2014), 76 ribu orang tewas, dgn rincian sebanyak 17.790 adalah warga sipil, termasuk 3.501 anak-anak. Sedikitnya 22.627 merupakan tentara pemerintah atau gerilyawan propemerintah.</li>
            <li>Januari, USA menyerang Suriah. Sekitar 17 serangan terhadap sasaran ISIS terjadi di kota Raqqa, Kobani dan Deir al-Zour di Suriah. Sedangkan di Irak serangan terjadi di Falluja, Mosul dan Sinjar.</li>
            <li>Sedikit-dikitnya 22.627 tentara pemerintah serdadu dan milisi tewas.</li>
            <li>15.000 pemberontak tewas dan hampir 17.000 petempur dari kelompok keras, termasuk Negara Islam dan Kubu Al-Nusra, perpanjangan Alqaida di Suriah.</li>
            <li>22 ribu luka-luka.</li>
            <li>Hingga November 2014 berdasarkan studi BBC, lebih dari 5000 jiwa jumlah korban tewas di Suriah.</li>
            <li>Sementara sudah sebanyak 664 serangan di 14 negara.</li>
            <li>Menurut investigasi BBC, sebanyak 7 orang meninggal di setiap jam.</li>
            <li>Negara yang paling banyak menjadi korban atas serangan Jihadis itu adalah; Iraq, Nigeria, Suriah dan Afghanistan, dengan korban 4.031 orang di empat negara itu, atau 80% dari total jumlah kematian.</li>
            <li>Jumat (31/10) sekitar 150 pejuang Peshmerga Kurdi Irak melintas dari Turki untuk bergabung dengan warga Kurdi Suriah yang mempertahankan kota dari serangan ISIS selama enam minggu.</li>
            <li>Bulan November, 100 milisi ISIS tewas dalam perang selama tiga hari di kota perbatasan strategis Suriah, Kobane.</li>
            <li>Senin (6/4/2015), melalui penggalian forensik, ditemukan 12 jasad hasil korban ISIS 2014.</li>
            <li>Februari, ISIS juga mengklaim bahwa merekalah yang bertanggung jawab atas pembunuhan 21 umat Kristen Koptik asal Mesir di kota Libya.</li>
            <li>Kamis (5/3), sedikitnya 13 pemimpin senior Front Nusra terbunuh dalam serangan udara di Hobait, provinsi Idlib, Suriah. Salah satu petinggi Front Nusra yang tewas adalah Abu Hammam al Shami, tokoh penting kelompok ini.</li>
            <li>Jumat (20/3), ISIS mengklaim bertanggung jawab atas serangan bom bunuh diri di dua masjid di ibu kota Yaman, Sanaa, yang menewaskan 137 orang.</li>
            <li>30 pasukan ISIS menyekap 20 pekerja medis saat menyerbu Rumah Sakit Ibn Sina di Sirte, Libya, pada Minggu (15/3).</li>
            <li>(31/03), ISIS mengeksekusi mati 37 warga sipil dan 2 anak-anak di Mabujeh.</li>
            <li>Di hari yang sama Kelompok radikal ISIS juga menyandera 50 warga sipil, rincinya 6 orang wanita diculik aliran Syiah Ismaili dan 40 orang aliran Sunni, termasuk 15 wanita.</li>
        </ul>
        <p style="margin-top:10px;font-size: 9px;text-align: right;">dikutip dari http://id.wikipedia.org/wiki/Negara_Islam_Irak_dan_Syam</p>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="ringkasan">SUMBER DANA</span></h5>
        <p>ISIS didaulat sebagai kelompok teroris paling kaya di dunia dengan dana jihad diperkirakan sekitar 2 miliar US Dollar</p>
        <ul style="list-style: square;padding-left: 5px;margin-left: 15px;">
            <li>US $ 429.000.000 dijarah dari bank sentral Mosul, serta jutaan tambahan dan sejumlah besar emas batangan yang dicuri dari bank lain di Mosul.</li>
            <li>ISIS secara rutin melakukan pemerasan, dengan menuntut uang dari sopir truk</li>
            <li>ISIS juga menghasilkan pendapatan dari produksi minyak mentah dan menjual tenaga listrik di Suriah utara</li>
            <li>Menerima dana dari pendonor swasta di negara-negara Teluk.</li>
            <li>Merampok bank dan toko emas telah menjadi sumber pendapatan lain.</li>
            <li>Dalam rilis November 2014, PBB mencatat: US$ 850 ribu - US$ 1,65 juta perhari, hasil penjualan minyak dari ladang-ladang yang ISIS kuasai.</li>
            <li>Hingga 2014, jumlah dana ISIS sekitar US$1,5 miliar atau sekitar Rp17,6 triliun, dana itu diperkirakan berasal dari investigasi Independent.</li>
            <li>Dana ajaib ISIS itu antara lain berasal dari tebusan para sandera yang diculik, penjarahan bank-bank di Irak, hingga penjualan minyak dalam jumlah besar.</li>
            <li>ISIS menguasai 60 persen dari aset minyak Suriah dan tujuh fasilitas produksi minyak di Irak.</li>
            <li>Minyak-minyak itu diperdagangkan dengan jaringan perantara dan geng-geng kriminal. Setiap harinya menghasilkan US$2 juta.</li>
            <li>Setiap hari ISIS menjual sekitar 30 ribu barel ke negara-negara tetangga termasuk Yordania, Kurdistan dan Turki tentunya melalui perantara ilegal.</li>
            <li>Melalui perampokan Bank sentral di Mosul, ISIS memperoleh dana lebih dari US$400 juta.</li>
            <li>Selain itu ISIS juga melakukan pencurian artefak kuno senilai US$36 juta yang berumur 8 ribu tahun. Barang-barang antik kuno itu salah satunya dicuri dari pegunungan Qalamoun barat, Damaskus.</li>
            <li>Auu$ 500 ribu dikirim dari Australia lewat transfer bank dan kurir.</li>
            <li>Rp 7 milliar dari bisnis obat-obatan herbal, jamu, dan madu.</li>
        </ul><br>
        <p>Dilansir Dari Daily Mail, Senin (23/2/2015), sumber intelijen melaporkan sepuluh sumber pendanaan ISIS khususnya di wilayah Irak dan Suriah, yaitu :</p>
        <ul style="list-style: square;padding-left: 5px;margin-left: 15px;">
            <li>ISIS menguasai pasokan listrik dan gas di wilayah yang mereka kuasai, kemudian menjualnya kepada penduduk yang tinggal di sana.</li>
            <li>ISIS memungut pajak dari penduduk.</li>
            <li>ISIS menerapkan pajak barang impor yang masuk.</li>
            <li>Di tengah kondisi perang, ISIS sanggup untuk mengekspor barang seperti buah-buahan, sayur-sayuran, dan sedikit hasil industri.</li>
            <li>Pajak internet.</li>
            <li>Denda yang diberlakukan di wilayah kekuasaannya, seperti denda bagi perokok, orang yang ketahuan meninggalkan salat, dan perempuan yang tidak menutup aurat.</li>
            <li>Jual Beli Tanah di wilayah kekuasaan ISIS.</li>
            <li>Penjualan narkotika dan rokok di pasar gelap.</li>
            <li>Merampas benda-benda museum kemudian dijualnya di pasar gelap.</li>
            <li>Menjual minyak bumi. Kegiatan ini merupakan sumber pemasukan terbesar ISIS.</li>
        </ul>
        <p>Dengan dukungan dana yang melimpah, kini ISIS sudah melebarkan sayapnya ke seluruh dunia. Kini kelompok militant itu sudah melakukan operasinya di Mesir, Libya, Somalia, dan Nigeria.</p>
        <p style="margin-top:10px;font-size: 9px;text-align: right;">dikutip dari http://id.wikipedia.org/wiki/Negara_Islam_Irak_dan_Syam</p>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="kekuatan">PETA KEKUATAN</span></h5>
        <img style="width: 100%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/Syria_areas_of_control_March_2014.png" alt="Syria_areas_of_control_March_2014.png">
        <div class="tag-title"><p>KEKUATAN NEGARA DAN MILITAN</p></div>
        <img style="margin-top:5px;width: 100%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/Syria_and_Iraq_2014-onward_War_map.png" alt="Syria_and_Iraq_2014-onward_War_map.png">
        <div class="row" style="margin-top:10px;">
            <div class="col-xs-12 col-sm-12">
              <div style="width: 310px;margin-top:5px;height: 12px;">
                <div style="float:left;background-color: #B4B2AE;border: solid 1px #000000;width:10px;height: 10px;"></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Dikendalikan oleh Islamic State of Iraq and Syria (ISIS)</p>
              </div>
              <div style="width: 310px;height: 12px;">
                <div style="float:left;background-color: #CAE7C4;border: solid 1px #000000;width:10px;height: 10px;"></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Dikendalikan oleh pemberontak Suriah lainnya</p>
              </div>
              <div style="width: 310px;height: 12px;margin-top:-5px;">
                <div style="float:left;background-color: #EBC0B3;border: solid 1px #000000;width:10px;height: 10px;"></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Dikendalikan oleh pemerintah Suriah</p>
              </div>
              <div style="width: 310px;height: 12px;margin-top:-5px;">
                <div style="float:left;background-color: #DB8CA6;border: solid 1px #000000;width:10px;height: 10px;"></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Dikendalikan oleh pemerintah Irak</p>
              </div>
              <div style="width: 310px;height: 12px;margin-top:-5px;">
                <div style="float:left;background-color: #E2D974;border: solid 1px #000000;width:10px;height: 10px;"></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Dikendalikan oleh suku kurdi Suriah</p>
              </div>
              <div style="width: 310px;height: 12px;margin-top:-5px;">
                <div style="float:left;background-color: #D7E074;border: solid 1px #000000;width:10px;height: 10px;"></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Dikendalikan oleh suku kurdi Irak</p>
              </div>
              <div class="pull-right"><p style="font-size: 9px;">Situasi militer saat ini (Agustus 2014)</p></div>
            </div>
        </div>
        <hr class="line-mini">

        <div class="tag-title"><p>PETA KEKUATAN DI SYRIA DAN IRAK</p></div>
        <img style="margin-top:5px;width: 100%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/Territorial_control_of_the_ISIS.png" alt="Territorial_control_of_the_ISIS.png">
        <div class="row" style="margin-top:10px;">
            <div class="col-xs-12 col-sm-12">
                <div style="width: 310px;margin-top:5px;height: 12px;">
                    <div style="float:left;background-color: #C12838;border: solid 1px #000000;width:10px;height: 10px;"></div>
                    <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Kawasan yang dikendalikan oleh ISIS</p>
                </div>
                <div style="width: 310px;height: 12px;">
                    <div style="float:left;background-color: #E7DFC9;border: solid 1px #000000;width:10px;height: 10px;"></div>
                    <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Daerah yang di klaim oleh ISIS</p>
                </div>
                <div style="width: 310px;height: 12px;margin-top:-5px;">
                    <div style="float:left;background-color: #FEFEE9;border: solid 1px #000000;width:10px;height: 10px;"></div>
                    <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:4px;width: 290px;">Selebihnya oleh Irak dan Suriah</p>
                </div>
                <div class="pull-right"><p style="font-size: 9px;">Catatan: Peta mencakup daerah tak berpenghuni.</p></div>
            </div>
        </div>
        <hr class="line-mini">

        <div class="tag-title"><p>KESENJANGAN KESUKUAN</p></div>
        <img style="margin-top:5px;width: 100%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/iraq-sectarian-divide.jpg" alt="iraq-sectarian-divide.jpg">
        <p>Irak adalah negara yang terpecah oleh garis sektarian. Ada tiga sekte besar: Muslim Syiah, Muslim Sunni dan Kurdi. Perdana Menteri Irak, Nuri al-Maliki, seorang Muslim Syiah. Muslim Sunni - minoritas di Irak - sering menemukan diri mereka ditinggalkan. Beberapa ahli mengatakan bahwa ISIS telah menemukan dasar antara komunitas Sunni Irak.</p>
        <hr class="line-mini">

        <div class="tag-title"><p>LOKASI MINYAK IRAK</p></div>
        <img style="margin-top:5px;width: 100%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/iraq_oil_infrastructure_map2.jpg" alt="iraq_oil_infrastructure_map2.jpg">
        <p>Perekonomian Irak tergantung pada produksi minyak. Negara ini memproduksi 3,3 juta barel per hari dan memiliki cadangan minyak terbesar keempat di dunia. Menurut OPEC, memiliki cadangan lebih dari 140 miliar barel di ladang minyak di sebelah timur, selatan dan utara negara itu.</p>
        <p>Militan dari sebuah kelompok yang disebut Negara Islam di Irak dan Suriah, atau ISIS, yang melancarkan serangan yang telah melihat petak besar Irak utara jatuh dari tangan pemerintah. ISIS, sebuah kelompok sempalan al-Qaeda, ingin mendirikan sebuah kekhalifahan, atau negara Islam, yang akan membentang dari Irak ke Suriah utara.</p>
        <p>Militan ekstrimis telah dibanjiri Mosul, kota terbesar kedua di Irak. Dalam beberapa pekan terakhir, mereka telah merebut kendali dari kota-kota Irak seperti Falluja dan Ramadi bagian dari otoritas, seperti yang mereka lakukan dengan kota-kota Suriah di perbatasan. Klik melalui peta untuk menemukan lebih banyak.</p>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="kekuatan">KEKUATAN MILITER</span></h5>
        <p>Rudal Stinger | M198 howitzer | Senjata DShK yang dipasang pada truk | Senjata anti-pesawat | Tembak dorong otomatis | Rudal Scud | Helikopter Blackhawk UH-60 | Pesawat Kargo | Humvee | Tank-55 | Howitzer M198 | Field Gun Tipe 59-1 | ZU-23-2 Anti-Aircraft Gun | Tank-72 | FIM-92 Stinger MANPAD | AK-47 | SA-16 MANPAD | Peluncur Roket Osa M79 | HJ-8 | Peluncur Granat RBG-6 | Senjata mesr DShK 1938 | Roket RPG-7s | Jet Tempur MiG</p>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="kekuatan">WILAYAH KEKUASAAN</span></h5>
        <img style="margin-top:5px;width: 100%;height: auto;" src="http://2.bp.blogspot.com/-TfBMzoJRbAQ/VKd6VuZTVyI/AAAAAAAAcvU/-ieQ8nSXD4I/s1600/negara%2Bisis.png" alt="WILAYAH KEKUASAAN">
        <p>ISIS telah menguasai kota kota antara lain : Irak di Baghdad | Anbar | Diyala | Kirkuk | Salah al-Din | Ninawa | Babil | Barakah | Al Kheir | Al Raqqah | Al Badiya | Halab | Idlib | Hama | Damaskus | Latakia </p>
        <table style="margin-bottom: 10px;">
            <tr style="border-bottom: dashed 1px #c4c4c4;">
                <td style="width: 30%;">Maret 2013</td><td style="width: 60%;">ISIS kuasai ibukota Raqqa</td>
            </tr>
            <tr style="border-bottom: dashed 1px #c4c4c4;">
                <td style="width: 30%;">Januari 2014</td>
                <td style="width: 60%;">
                    <ul style="list-style: square;padding-left: 10px;">
                        <li style="margin-left: 0px;">ISIL kuasai Kota Fallujah di Provinsi Anbar.</li>
                        <li style="margin-left: 0px;">ISIS kuasai Ramadi dan muncul di sejumlah kota yang berdekatan dengan perbatasan Turki dan Suriah.</li>
                    </ul>
                </td>
            </tr>
            <tr style="border-bottom: dashed 1px #c4c4c4;">
                <td style="width: 30%;">Juni 2014</td><td style="width: 60%;">ISIS kuasai Kota Mosul</td>
            </tr>
            <tr style="border-bottom: dashed 1px #c4c4c4;">
                <td style="width: 30%;">22 Juni 2014</td><td style="width: 60%;">ISIS kuasai kota Rutba di Irak barat</td>
            </tr>
            <tr style="border-bottom: dashed 1px #c4c4c4;">
                <td style="width: 30%;">7 Agustus 2014</td><td style="width: 60%;">ISIS Kuasai Pangkalan Militer Utama Suriah</td>
            </tr>
        </table>
        <img style="margin-top:5px;width: 100%;height: auto;" src="https://indocropcircles.files.wordpress.com/2014/08/isis-map.jpg?w=616&h=424" alt="WILAYAH KEKUASAAN">
        <img style="margin-top:5px;width: 100%;height: auto;" src="http://gdb.voanews.com/CAA793E6-C40D-4BF1-8D31-5CBE06B39EAA_cx0_cy13_cw0_mw1024_s_n_r1.jpg" alt="WILAYAH KEKUASAAN">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="berita_terkait">BERITA TERKAIT</span></h5>
        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="group_etnis">GROUP ETNIS DAN RELIGI: TARGET KEBRUTALAN ISIS</span></h5>
        <div id="accordion" class="panel-group">

            <div class="tag-title panel-heading" >
                <a data-toggle="collapse" data-parent="#accordion" href="#yazidis">KELOMPOK YAZIDIS</a>
            </div>
            <div id="yazidis" class="panel-collapse collapse in" style="margin-top:10px;">
                <img style="float:left;padding:5px;width: 60%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/yazidis01.jpg" alt="yazidi">
                <p>Yazidis adalah salah satu kelompok agama monotheistik yang terkecil dan tertua di dunia. Agama mereka bisa dianggap sebuah sekte pra Islam yang ditarik dari kekristenan, yahudi dan agama monotheistik kuno dari Zoroastrianism. Yazidis menyembah satu TUhan dan menghormati 7 malaikat. Tidak seperti Muslim dan Kristen, mereka menolak ide dari dosa, iblis dan neraka. Banyak Muslim menganggap mereka sebagai penyembah setan karena Yazidis memuja malaikat yang tradisinya menolak untuk mentaati TUhan.</p>
                <p>Perbedaan agama mereka telah membuat mereka menjadi target dari penganiayaan sepanjang sejarah, baru-baru ini pada waktu perang AS di Irak pada tahun 2007, lebih dari 700 orang tewas pada bom bunuh diri di desa Yazidi. Sebelumnya, mereka menjadi sasaran selama berabad-abad di bawah kekuasaan Kekaisaran Ottoman. Presiden Obama dan Menteri Luar Negri John Kerry mengatakan bahwa Yazidis tidak dilindungi, pembantaian mereka dapat dengan cepat meluas ke arah genosida. Untuk membantu orang-orang yang terjebak, Amerika telah mengirimkan bantuan kemanusiaan melalui udara. Obama telah menyetujui serangan udara terhadap pejuang Islamic State yang telah mengancam kaum Yazidis disana.</p>
                <img style="float:left;padding:5px;width: 60%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/yazidis02.jpg" alt="yazidi">
                <p>Laporan kebebasan beragama internasional, Departemen Luar Negeri Amerika pada tahun 2013 memperkiraan sekitar 500.000 kaum Yazidis tinggal di Irak Utara, kurang dari 1% dari populasi negara tersebut. 200.00 orang lainnya tinggal di bagian lain di dunia. Sama seperti Kurdi, mereka kebanyakan tinggal di utara Irak, banyak di kota Sinjar barat laut Provinsi Nineveh, berbatasan dengan wilayah Kurdi Irak. Provinsi tersebut adalah rumah bagi sebagian besar orang Arab dan Kurdi, yang telah berdesakan untuk menguasai selama berabad-abad. Tapi Yazidis juga berada di Turki, Siria, Armenia, Iran dan bagian dari wilayah Caucasus. Orang-orang disana berbahasa Kurdi dan keturunan Kurdi, tapi kebanyakan melihat mereka sebagai etnis yang unik.</p>
            </div>

            <div class="tag-title panel-heading" >
                <a data-toggle="collapse" data-parent="#accordion" href="#irak">KRISTEN IRAK</a>
            </div>
            <div id="irak" class="panel-collapse collapse" style="margin-top:10px;">
                <img style="float:left;padding:5px;width: 60%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/irakcrhitian01.jpg" alt="irakcrhitian01.jpg">
                <p>Sebelum menjadi target oleh ISIS, dalam jumlah besar (beberapa mengatakan sebanyak setengah) orang kristen di Irak melarikan diri dari negara itu diawal perang AS pada tahun 2003. Al Qaeda di Irak yang mempelopori ISIS, menjadikan minoritas Kristen di negara itu sebagai sasaran secara brutal. Menurut Departemen Luar Negeri, para pemimpin Kristen dan Organisasi non pemerintah memperkirakan ada sekitar 500.000 orang Kristen di Irak (telah menurun  menjadi sekitar 300.000) pada 5 tahun terakhir. Pada suatu titik lebih dari 1 juta orang Kristen telah tinggal di Irak. Kebanyakan orang Kristen adalah Kasdim, yang merupakan anggota Gereja Katolik Roma. Mereka mendominasi tempat tinggal di Irak utara.</p>
                <img style="float:left;padding:5px;width: 60%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/irakcrhitian02.jpg" alt="irakcrhitian02.jpg">
                <p style="line-height: 17px;">Kelompok pecahan Al-Qaeda telah mengambil alih kota penduduk Kristen terbesar yaitu Qaraqosh. Dan bulan sebelumnya, di kota penduduk Kristen kedua terbesar yaitu Mosul, mereka harus menjadi Islam, membayar denda atau hukuman mati. Pendeta Federico Lombardi, juru bicara Paus Francis menyebutkan bahwa:  "Umumnya komunitas Kristen terpengaruh, orang-orang melarikan diri dari desa mereka dikarenakan kekerasan yang terjadi akhir-akhir ini, mendatangkan malapetaka di seluruh dunia. Di Twitter, Paus berkata: "Aku meminta semua pria dan wanita dalam kehendak Tuhan untuk bergabung dalam doa untuk orang-orang Kristen di Irak dan semua masyarakat yang rentan". ISIS mengambil alih kota dengan populasi Kristen terbesar.</p>
                <p style="line-height: 17px;">Pendeta Federico Lombardi, juru bicara Paus Francis menyebutkan bahwa:  "Umumnya komunitas Kristen terpengaruh, orang-orang melarikan diri dari desa mereka dikarenakan kekerasan yang terjadi akhir-akhir ini, mendatangkan malapetaka di seluruh dunia. Di Twitter, Paus berkata: "Aku meminta semua pria dan wanita dalam kehendak Tuhan untuk bergabung dalam doa untuk orang-orang Kristen di Irak dan semua masyarakat yang rentan". ISIS mengambil alih kota dengan populasi Kristen terbesar.</p>
            </div>


            <div class="tag-title panel-heading" >
                <a data-toggle="collapse" data-parent="#accordion" href="#turkmen">TURKMEN</a>
            </div>
            <div id="turkmen" class="panel-collapse collapse" style="margin-top:10px;">
                <img style="float:left;padding:5px;width: 60%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/turkmen01.jpg" alt="turkmen01.jpg">
                <p>Mayoritas turkmen di dunia, orang yang berbahasa Turki, orang-orang pengembara tradisional, tinggal di Turkmenistan dan tempat lain di Asia Tengah. Tapi sebagian kecil dari mereka dapat ditemukan di Timur Tengah, tertutama di bagian utara Irak, Iran dan Turki. Kota Tal Afar, yang populasi terbesarnya dari orang Turkmen, terlibat baku tembak dalam kekerasan sekte antara Syiah dan Sunni selama perang Irak baru-baru ini. Serangan bunuh diri menewaskan 150 orang pada tahun 2007. Populasi kota ini menurun dari kira-kira 200.000 menjadi 80.000 dalam beberapa tahun. Turkmen Sunni membentuk 1% sampai 2% populasi Irak, menurut Departemen Luar Neger. Kelompok Turkmen Syiah yang lebih kecil tinggal juga disana.</p>
                <img style="float:left;padding:5px;width: 60%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/trukmen02.jpg" alt="trukmen02.jpg">
                <p>Kota Tal Afar, yang populasi terbesarnya dari orang Turkmen, terlibat baku tembak dalam kekerasan sekte antara Syiah dan Sunni selama perang Irak baru-baru ini. Serangan bunuh diri menewaskan 150 orang pada tahun 2007. Populasi kota ini menurun dari kira-kira 200.000 menjadi 80.000 dalam beberapa tahun. Turkmen Sunni membentuk 1% sampai 2% populasi Irak, menurut Departemen Luar Neger. Kelompok Turkmen Syiah yang lebih kecil tinggal juga disana.</p>
            </div>


            <div class="tag-title panel-heading" >
                <a data-toggle="collapse" data-parent="#accordion" href="#syiah">SYIAH</a>
            </div>
            <div id="syiah" class="panel-collapse collapse" style="margin-top:10px;">
                <img style="float:left;padding:5px;width: 60%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/shia01.jpg" alt="shia02.jpg">
                <p>Meskipun resiko dampak ISIS terhadap Yazidi, Turkmen, Kristen dan negara minoritas lain, resiko bagi mayoritas Muslim Syiah Irak jauh lebih luas. Dalam pencarian mereka untuk menciptakan kekhalifahan Islam yang membentang dari Suriah ke Irak, ISIS telah menargetkan Syiah di kedua negara.</p>
                <img style="float:left;padding:5px;width: 60%;height: auto;" src="http://www.bijaks.net/assets/images/hotpages/isis/shia02.jpg" alt="shia01.jpg">
                <p">Pada bulan Juni, sebuah kelompok mengklaim di Twitter bahwa telah menewaskan setidaknya 1.700 orang Syiah pada bulan Juni. ISIS juga memerangi pasukan presiden Siria President Bashar al-Assad. Assad adalah anggota dari kelompok sekte Alawite, pada cabang Islam Syiah. Seperti kebanyakan kaum minoritas lainnya di provinsi Nineveh, Syiah dan Alawi telah dinamakan kafir oleh ISIS. Secara keseluruhan Syiah melebihi Suni di Irak. Sebagian besar Baghdad didominasi oleh  Syiah, namun sebagian besar dari wilayah barat dan utara Irak memiliki populasi mayoritas Sunni.</p>
             </div>
        </div>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="kekuatan_militer">PHOTO KEBRUTALAN ISIS</span></h5>
<?php
$photoisis = array(
    0 => ( array ( 'ref_name' => 'Menembak mati militer suriah', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_01.png')),
    1 => ( array ( 'ref_name' => 'Menembak mati warga sipil', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_02.jpg')),
    2 => ( array ( 'ref_name' => 'Mem-bom dan membakar gereja', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_03.jpg')),
    3 => ( array ( 'ref_name' => 'Menembak mati warga sipil', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_04.jpg')),
    4 => ( array ( 'ref_name' => 'Militan ISIS di Irak bakar rokok dan tembakau shisha', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_05.jpg')),
    5 => ( array ( 'ref_name' => 'Menembak mati warga sipil yang tidak sepaham', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_06.png')),
    6 => ( array ( 'ref_name' => 'Menembak mati orang yang di penjara secara membabi buta', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_07.jpg')),
    7 => ( array ( 'ref_name' => 'Menembak mati orang yang tidak sepaham', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_08.jpg')),
    8 => ( array ( 'ref_name' => 'Menghancurkan situs makam nabi Yunus', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_09.png')),
    9 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_10.JPG')),
    10 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_11.jpg')),
    11 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_12.jpg')),
    12 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_13.jpg')),
    13 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_14.jpg')),
    14 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_15.jpg')),
    15 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_16.jpg')),
    16 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_17.jpg')),
    17 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_18.jpg')),
    18 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_19.jpg')),
    19 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_20.jpg')),
    20 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_21.jpg')),
    21 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_22.jpg')),
    22 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_23.jpg')),
    23 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_24.jpg')),
    24 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_25.jpg')),
    25 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_26.jpg')),
    26 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_27.jpg')),
    27 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_28.jpg')),
    28 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_29.jpg')),
    29 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_30.jpg')),
    30 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_31.jpg')),
    32 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_33.jpg')),
    33 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_34.jpg')),
    34 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.kompasislam.com/wp-content/uploads/2014/07/ISIS-sedang-mempertontonkan-kepala-Muhammad-Faris-als.-Abu-Abdulloh-Al-Halabi-salah-seorang-komandan-mujahidin-Ahrorusy-Syam-yang-telah-dipenggalnya.-300x166.png')),
    35 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://news.beritapasti.com/wp-content/uploads/alan-henning.jpg')),
    36 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://files.buktidansaksi.com/islam-bangsattt.jpg')),
    37 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://us.images.detik.com/content/2014/08/20/1148/isispenggal.jpg')),
    38 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'https://anekainfounik.files.wordpress.com/2014/08/foto-isis-pancung-pejuang-kurdi-di-irak.jpg?w=750&h=392')),
    39 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/photo_brutal_07.jpg')),
    40 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://statik.tempo.co/data/2014/06/17/id_298426/298426_620.jpg')),
    41 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://cdn.sindonews.net/dyn/620/content/2015/02/16/41/965182/rusia-isis-kelompok-barbar-PAd.jpg')),
    42 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://indonesiahariinidalamkata.com/indonesia/wp-content/uploads/2014/10/10702015_466526876822987_8202052549376711473_n.jpg')),
    43 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://files.buktidansaksi.com/munafik2.png')),
    44 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://files.buktidansaksi.com/beheaded1.png')),
    45 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://files.buktidansaksi.com/solo.jpg')),
    48 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://mirajnews.com/id/wp-content/uploads/sites/3/2015/01/SANDERA-JEPANG2.jpg')),
    49 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://i.ytimg.com/vi/Yh5l5HVMGwA/hqdefault.jpg')),
    50 => ( array ( 'ref_name' => '', 'ref_url' => '',
            'ref_img' => 'http://cdn.ar.com/images/stories/2014/09/bwad_mniqaaj2uh_small.jpg')),
);

?>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <ul>
                    <?php
                    foreach($photoisis as $key=>$val)
                    {
                        ?>
                        <li class="photo-isis">
                            <img src="<?php echo $val['ref_img']; ?>" alt=''/>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <hr class="line-mini">

        <img src="http://www.bijaks.net/assets/images/hotpages/isis/banner-diatas-peta.png" style="width: 100%;height: auto;">
        <img src="http://www.bijaks.net/assets/images/hotpages/isis/peta-isis.jpg" style="width:100%;height: auto;">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
            <p>Keterangan :</p>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>01</p></div>
                <p style="float:left;margin-left:5px;margin-top:0px;font-size: 10px;line-height:12px;width: 270px;">8-Agus-2014, Bendera dan poster ISIS dipasang di sejumlah tempat di Kota Jambi.</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>02</p></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:12px;width: 270px;">Lampung, Penangkapan Sal (26) dan Mut (24).</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>03</p></div>
                <p style="float:left;margin-left:5px;margin-top:0px;font-size: 10px;line-height:12px;width: 270px;">8-Feb-2014, Deklarasi dukungan Forum Aktivis Syariat Islam kepada ISIS di masjid Fathullah Ciputat, Tangerang Selatan.</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>04</p></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:12px;width: 270px;">16-Mar-2014, Aksi mendukung ISIS di Bundaran Hotel Indonesia, Jakarta.</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>05</p></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:12px;width: 270px;">Cianjur, diduga ada anggota ISIS.</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>06</p></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:12px;width: 270px;">12-Agus-2014, 7 orang ditangkap di Cilacap diantaranya.</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>07</p></div>
                <p style="float:left;margin-left:5px;margin-top:0px;font-size: 10px;line-height:12px;width: 270px;">15-Jul-2014, Forum Pendukung Daulah Islamiyah membaiat ratusan orang untuk mendukung ISIS di Masjid Baitul Makmur, Sukoharjo, Jawa Tengah.</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>08</p></div>
                <p style="float:left;margin-left:5px;margin-top:0px;font-size: 10px;line-height:12px;width: 270px;">20-Jul-2014, Deklarasi dukungan kepada ISIS di Lowokwaru, Kota Malang, Jawa Timur.<br/>8-Agus-2014, Penangkapan 2 orang di Ngawi, Jawa Timur: Guntur Pamungkas, Kardi.</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>09</p></div>
                <p style="float:left;margin-left:5px;margin-top:5px;font-size: 10px;line-height:12px;width: 270px;">Sulawesi Selatan, diduga ada anggota ISIS.</p>
            </div>
            <div style="width: 300px;height: 12px;">
                <div class="legend-ind"><p>10</p></div>
                <p style="float:left;margin-left:5px;margin-top:0px;font-size: 10px;line-height:12px;width: 270px;">Kawasan Poso, Sulawesi Tengah, sebagai sarang jaringan teroris</p>
            </div>
            </div>
        </div>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="visimisi">KRONOLOGI</span></h5>
        <div class="history-title"><b>12 Agustus 2014</b></div>
        <div class="history-info">
           <p>7 orang ditangkap di Cilacap diantaranya: Chep Hermawan | Dani Rahdani | Ludy Burdah Muslim | Aeb Lukman Nulhakim | Syaiful Bahri | Didin Samsudin | Ade Saefullah</p>
           <p>Barang bukti yang disita: dua lembar bendera | lima topi | empat kaos | satu buah pin | tiga lembar sebo (penutup muka)</p>
        </div>
        <div class="history-title"><b>9 Agustus 2014</b></div>
        <div class="history-info">
           <p>Penangkapan terhadap Afif Abdul Madjid</p>
        </div>
        <div class="history-title"><b>8 Agustus 2014</b></div>
        <div class="history-info">
           <p>Penangkapan 2 orang di Ngawi, Jawa Timur: Guntur Pamungkas | Kardi</p>
           <p>Barang bukti yg disita : 1 pucuk senjata api pistol Barreta. | 2 magazin | 21 butir amunisi | bendera ISIS di rumah Kardi</p>
        </div>
        <div class="history-title"><b>8 Agustus 2014</b></div>
        <div class="history-info">
           <p>Bendera dan poster ISIS dipasang di sejumlah tempat di Kota Jambi</p>
        </div>
        <div class="history-title"><b>22 Juli 2014</b></div>
        <div class="history-info">
           <p>Video ajakan warga indonesia bergabung dengan ISIS beredar di Youtube.</p>
        </div>
        <div class="history-title"><b>20 Juli 2014</b></div>
        <div class="history-info">
           <p>Deklarasi dukungan kepada ISIS di Lowokwaru, Kota Malang, Jawa Timur.</p>
        </div>
        <div class="history-title"><b>15 Juli 2014</b></div>
        <div class="history-info">
           <p>Forum Pendukung Daulah Islamiyah membaiat ratusan orang untuk mendukung ISIS di Masjid Baitul Makmur, Sukoharjo, Jawa Tengah.</p>
        </div>
        <div class="history-title"><b>14 Juli 2014</b></div>
        <div class="history-info">
           <p>Abu Bakar Ba'asyir, melalui ketua JAT Mochammad Achwan menyatakan mendukung terbentuknya ISIS.</p>
        </div>
        <div class="history-title"><b>16 Maret 2014</b></div>
        <div class="history-info">
           <p>Aki mendukung ISIS di Bundaran Hotel Indonesia, Jakarta.</p>
        </div>
        <div class="history-title"><b>8 Februari 2014</b></div>
        <div class="history-info">
           <p>Deklarasi dukungan Forum Aktivis Syariat Islam kepada ISIS di masjid Fathullah Ciputat, Tangerang Selatan</p>
        </div>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="visimisi">KOMENTAR TOKOH INDONESIA</span></h5>
<?php
$tokohIn = array(
    0 => ( array ( 'ref_name' => 'KH Ahmad Hasyim Muzadi', 'ref_title' => 'Mantan Ketua PBNU', 'ref_word' => 'Ini (ISIS) ... gerakan politik yang bisa mengancam kedaulatan dan konstitusi. ... kalau dipaksakan bisa merusak konstitusi dan integritas negara lainnya', 'ref_url' => 'http://www.bijaks.net/aktor/profile/khahmadhasyimmuzadi52d23543c7793',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/khahmadhasyimmuzadi52d23543c7793/thumb/a006ff97e7cf38fb162a1943159fb17bb325934a.jpg')),
    1 => ( array ( 'ref_name' => 'Ansyaad Mbai', 'ref_title' => 'Kepala Badan Nasional Penanggulangan Terorisme', 'ref_word' => 'warga negara Indonesia yang memberikan dukungan terhadap kelompok bersenjata yang tergabung dalam Islamic State of Iraq and Syria (ISIS) atau Negara Islam di Irak dan Suriah (NIIS) terancam hukuman..., WNI akan kehilangan kewarganegaraannya ...', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/Ansyaad_Mbai.jpg')),
    2 => ( array ( 'ref_name' => 'Abdul Kadir Karding', 'ref_title' => 'Ketua DPP PKB', 'ref_word' => '... kita perlu juga dikembangkan ajaran agama yang moderat , pluralistik, toleran, inklusif dan ... melihat Indonesia sebagai final negara kesatuan bukan melihat Islam sebagai daulah', 'ref_url' => 'http://www.bijaks.net/aktor/profile/abdulkadirkarding5285dbba60e22',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/abdulkadirkarding5285dbba60e22/thumb/beb97bf3100d6b673bd8282d55843bf9465e905f.jpg')),
    3 => ( array ( 'ref_name' => 'Lukman Hakim Saifuddin', 'ref_title' => 'Menteri Agama Indonesia', 'ref_word' => 'Islamic State in Iraq and al-Sham (ISIS) merupakan organisasi pergerakan yang berpaham radikal serta bertentangan dengan Pancasila sebagai dasar negara Indonesia.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/drshlukmanhakimsaifuddin511756084eb20',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/drshlukmanhakimsaifuddin511756084eb20/thumb/4832aeaca75228e5ea4959504f5d1ead0bd341bc.jpg')),
    4 => ( array ( 'ref_name' => 'Komaruddin Hidayat', 'ref_title' => 'Rektor Universitas Islam Negeri Syarif Hidayatullah, Jakarta', 'ref_word' => 'ISIS itu pandangan yang romantis dan utopis, jadi sangat tidak realistis, Yang namanya kekhalifahan itu catatan sejarah masa lalu. ... Di Indonesia, sama sekali tidak akan menarik.', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/KomaruddinHidayat.jpg')),
    5 => ( array ( 'ref_name' => 'Ridwan Kamil', 'ref_title' => 'Walikota Bandung', 'ref_word' => 'Sikat habis kalau ada (kelompok ISIS di Bandung), urusan ISIS itu sebenarnya tidak relevan di Indonesia.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/ridwankamil51c7d138bb02b',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/ridwankamil51c7d138bb02b/thumb/172b95201c4cc686196bb6e51746ebb4f1a900dc.jpg')),
    6 => ( array ( 'ref_name' => 'Prof. DR. Miftah Faridl', 'ref_title' => 'Ketua Majelis Ulama Indonesia (MUI) Kota Bandung', 'ref_word' => 'Kelompok ISIS ini jangan sampai memperkeruh suasana Indonesia menjadi pecah belah.', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/Prof-Dr-Miftah-Faridl.jpg')),
    7 => ( array ( 'ref_name' => 'Sri Sultan Hamengkubuwana X', 'ref_title' => 'Gubernur Daerah Istimewa Yogyakarta', 'ref_word' => 'Secara pribadi, saya harap masyarakat Jogja tidak usah terpancing.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/srisultanhamengkubuwonox519067cd71584',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/srisultanhamengkubuwonox519067cd71584/thumb/srisultanhamengkubuwonox519067cd71584_20130513_041622.jpg')),
    8 => ( array ( 'ref_name' => 'Susilo Bambang Yudoyono', 'ref_title' => 'Presiden Indonesia', 'ref_word' => 'Negara harus memiliki sikap atas itu (ISIS) agar tidak mengombang-ambingkan masyarakat kita, Itu yang patut kita perhatikan sekarang ini.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/susilobambangyudhoyono50ee672eb35c5',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/susilobambangyudhoyono50ee672eb35c5/thumb/39989d0890a536ffa7c758d668a09dbbb0b48a69.jpg')),
    9 => ( array ( 'ref_name' => 'Prof. Din Syamsudin', 'ref_title' => 'Ketua Umum MUI', 'ref_word' => 'Kita harus lebih hati-hati dalam menyikapi isu tentang ISIS. Jangan sampai justru kontraproduktif dengan arus utama gagasan Islam di negeri ini.', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/din.jpg')),
    10 => ( array ( 'ref_name' => 'Fahmi Salim, MA', 'ref_title' => 'Komisi Pengkajian dan penelitian MUI', 'ref_word' => 'Kita harus hati-hati dalam menyikapi isu ISIS agar tidak kontra produktif. Jangan sampai isu ISIS ini digunakan oleh oknum yang tidak bertanggungjawab untuk memojokkan gerakan islam yang mengusung dakwah Islam dan gagasan islam yang umum seperti syariah islam dan khilafah.', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/Fahmi-Salim.jpg')),
    11 => ( array ( 'ref_name' => 'KH. Ahmad Satori', 'ref_title' => 'Ketua Umum IKADI', 'ref_word' => 'ita harus hati-hati dalam membuat pernyataan penolakan terhadap ISIS. Kita setuju menolak kekerasan yang dilakukan oleh ISIS jangan sampai menolak ide Islam dan sesuatu yang sudah Ma\'lumun min Ad diin biddlaruroh.', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/kh_ahmad_satori.jpg')),
    12 => ( array ( 'ref_name' => 'Prof. Dr. KH. Said Aqil Siradj, M.A', 'ref_title' => 'Ketua Umum Pengurus Besar Nahdlatul Ulama', 'ref_word' => 'NU dengan tegas menolak adanya ISIS di Indonesia,', 'ref_url' => 'http://www.bijaks.net/aktor/profile/saidaqilsiradj5343c56457ee7',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/saidaqilsiradj5343c56457ee7/thumb/2f6bd1c33002ff3718a332c949b3635e0bf6093d.jpg')),
    13 => ( array ( 'ref_name' => 'Jenderal Moeldoko', 'ref_title' => 'Panglima TNI', 'ref_word' => '... kalau mereka (ISIS) macam-macam kami bisa sikat. gitu aja. ISIS tidak boleh berkembang di Indonesia, kita harus menolak, karena tidak sesuai dengan falsafah kita.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/jenderaltnimoeldoko5227e0280b4dc',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/jenderaltnimoeldoko5227e0280b4dc/thumb/dbb7be203a747c0fc8718926a1385401d1dfe9b7.jpg')),
    14 => ( array ( 'ref_name' => 'Komjen. Pol. Drs. Sutarman', 'ref_title' => 'Kapolri', 'ref_word' => 'Ditolak, tidak hanya dibubarkan. Ditolak, jadi ya enggak boleh. Sudah kita intruksikan semua.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/komjenpoldrssutarman51d2516f6eb74',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/komjenpoldrssutarman51d2516f6eb74/badge/e44bb7973c43f2b1b0a8dcc8b3f25b3fa855f7e7.jpg')),
    15 => ( array ( 'ref_name' => 'Joko Widodo', 'ref_title' => 'Gubernur DKI Jakarta', 'ref_word' => 'Tapi kita harus juga tetap waspada. Tidak boleh dibiarkan. Kalau memang sudah pada posisi betul-betul anarkis, membahayakan negara, harus ditindak tegas. ', 'ref_url' => 'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/thumb/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg')),
    16 => ( array ( 'ref_name' => 'Dino Patti Djalal', 'ref_title' => 'Foreign Policy Community of Indonesia', 'ref_word' => 'pemerintah dan tokoh agama kita kompak menolak ISIS dan tidak mengakui wilayah Irak dan Suriah.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/dinopattidjalaldjalal521c3eb12ca04',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/dinopattidjalaldjalal521c3eb12ca04/thumb/4bca258b8350f86b025e4b0ec16591d1c4df0145.jpg')),
    17 => ( array ( 'ref_name' => 'Agus Santoso', 'ref_title' => 'Wakil Ketua PPATK', 'ref_word' => 'Ada aliran dana dari Australia ke salah seorang yang terkait dengan terorisme. Dana tersebut untuk pembiayaan aksi terorisme. Jumlahnya cukup besar,.. ada yang mencapai Rp 7 miliar.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/agussantoso526f011a0bfec',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/agussantoso526f011a0bfec/badge/eb5f775242865ceac7d74f237b905afdef305d1c.jpg')),
    18 => ( array ( 'ref_name' => 'Jusuf Kalla', 'ref_title' => 'Wakil Presiden 2014-2019', 'ref_word' => 'Apabila berbicara tentang ISIS, di pikiran masyarakat adalah Islam yang bertindak brutal, Ini menakutkan.', 'ref_url' => 'http://www.bijaks.net/aktor/profile/agussantoso526f011a0bfec',
            'ref_img' => 'http://www.bijaks.net/public/upload/image/politisi/drshmuhammadjusufkalla50ee870b99cc9/badge/6258b973c0c66b2d579434124018f46c3edd0f44.jpg')),
    19 => ( array ( 'ref_name' => 'Aan Anshori', 'ref_title' => 'Pengamat Politik', 'ref_word' => 'secara kontemporer, ajaran bunuh membunuh dalam konteks ini telah dipraktekkan secara tidak langsung telah menjadikan anak didik sebagai kader ISIS. Sudah lama saya mencurigai ajaran Islam radikal disemai melalui institusi pendidikan formal.', 'ref_url' => '',
            'ref_img' => 'https://0.academia-photos.com/4928534/2126946/2497865/s200_mohammad.aan_anshori.jpg')),
    20 => ( array ( 'ref_name' => 'PBB', 'ref_title' => '', 'ref_word' => 'Kelompok terorganisasi yang melakukan kejahatan kemanusiaan dan perang.', 'ref_url' => 'http://id.wikipedia.org/wiki/Perserikatan_Bangsa-Bangsa',
            'ref_img' => 'http://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Emblem_of_the_United_Nations.svg/85px-Emblem_of_the_United_Nations.svg.png')),
    21 => ( array ( 'ref_name' => 'Phillips Hammond', 'ref_title' => 'Menlu Inggris', 'ref_word' => 'Kebrutalan ini akan dikonfrontasi dan dikalahkan. Ini hanya membuat kami semakin kuat dan lebih bertekad untuk mengalahkan ISIL (ISIS).', 'ref_url' => 'http://en.wikipedia.org/wiki/Philip_Hammond',
            'ref_img' => 'http://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Philip_Hammond%2C_Secretary_of_State_for_Defence.jpg/220px-Philip_Hammond%2C_Secretary_of_State_for_Defence.jpg'))
);

?>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div style="padding-left: 5px;padding-right: 5px;margin-top:-10px;">
                    <table style="margin-left:5px;margin-right: 5px;">
                        <?php
                        foreach($tokohIn as $key=>$val)
                        {
                            ?>
                            <?php if($key % 2 == 0){ ?>
                            <tr class="">
                        <?php } ?>
                            <td class="tokoh">
                                <div style="width:160px;height: 60px;">
                                    <div style="float:left;">
                                        <?php if(!empty($val['ref_url'])) { ?><a href="< ?php echo $val['ref_url'];?>"><?php } ?>
                                            <img src='<?php echo $val['ref_img'];?>'
                                                 data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $val['ref_name']; ?>" alt='<?php echo $val['ref_name']; ?>'/>
                                            <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                    </div>
                                    <div style="float:left;">
                                        <?php if(!empty($val['ref_url'])) { ?><a href="<?php echo $val['ref_url'];?>"><?php } ?>
                                            <p class="name"><?php echo $val['ref_name']; ?></p>
                                            <?php if(!empty($val['ref_url'])) { ?></a><?php } ?>
                                        <p class="title"><?php echo $val['ref_title']; ?></p>
                                    </div>
                                </div>
                                <p class="word"><?php echo $val['ref_word']; ?></p>

                            </td>
                            <?php if($key % 2 == 1 || $key == count($tokohIn) - 1){  ?>
                            </tr>
                        <?php } ?>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
        <hr class="line-mini">

        <h5 class="sub-kanal-title kanal-title-gray-soft"><span id="visimisi">ORGANISASI MENOLAK ISIS</span></h5>
<?php
$organ = array(
    0 => ( array ( 'ref_name' => 'Majelis Ulama Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_MUI.jpg')),
    1 => ( array ( 'ref_name' => 'Gerakan Pemuda Ansor', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_anshor.jpg')),
    2 => ( array ( 'ref_name' => 'Persatuan Tarbiyah Islamiyah', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_tarbiyah.jpg')),
    3 => ( array ( 'ref_name' => 'Dewan Masjid Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_dewanmasjid.jpg')),
/*    4 => ( array ( 'ref_name' => 'Front Pembela Islam', 'ref_title' => '', 'ref_word' => 'INTRUKSI!! Pengurus, Anggota & Simpatisan Laskar Pembela Islam seluruh NKRI DILARANG mengikuti gerakan apapun yg dilakukan oleh ISIS, FPI Mendukung Hamas Lawan Zionis israel namun FPI Tdk Mendukung ISIS. Karena ISIS suka membunuh sesama Muslim sprti yg terjadi di Iraq & Suriah hanya krn berbeda Madzhab', 'ref_url' => '',
            'ref_img' => base_url().'assets/images/hotpages/isis/fpi_logo.jpg')), */
    5 => ( array ( 'ref_name' => 'PB NAHDLATUL ULAMA', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_nu.jpg')),
    6 => ( array ( 'ref_name' => 'Muhammadiyah', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_muham.jpg')),
    7 => ( array ( 'ref_name' => 'Pemuda Persatuan Islam', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_persis.jpg')),
    8 => ( array ( 'ref_name' => 'Persatuan Ummat Islam', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_pui.jpg')),
    9 => ( array ( 'ref_name' => 'Dewan Da\'wah Islamiyah Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_ddii.jpg')),
    9 => ( array ( 'ref_name' => 'Lembaga Dakwah Islam Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_ldii.jpg')),
    10 => ( array ( 'ref_name' => 'Badan Komunikasi Pemuda Remaja Masjid Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_bpkprmi.jpg')),
    11 => ( array ( 'ref_name' => 'Ikatan Cendikiawan Muslim Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/icmi-isti.jpg')),
    12 => ( array ( 'ref_name' => 'Korps Alumni HMI', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_kahmi.jpg')),
    13 => ( array ( 'ref_name' => 'Ikatan Jamaah Ahlul Bait Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_ijabi.jpg')),
    14 => ( array ( 'ref_name' => 'Parisada Hindu Dharma Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_parisada.jpg')),
    15 => ( array ( 'ref_name' => 'Konferensi Wali Gereja Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_bishop.jpg')),
    16 => ( array ( 'ref_name' => 'Tao Indonesia', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_tao.jpg')),
    17 => ( array ( 'ref_name' => 'Vihara Mahavira Graha Pusat', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_mahavira.jpg')),
    18 => ( array ( 'ref_name' => 'Gereja Ortodox Syria', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://www.bijaks.net/assets/images/hotpages/isis/logo_ortodok.jpg')),
    19 => ( array ( 'ref_name' => '', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'https://pbs.twimg.com/profile_images/1668043445/logo-bima.jpg')),
    20 => ( array ( 'ref_name' => '', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://hizbut-tahrir.or.id/wp-content/uploads/2011/08/logohti.jpg')),
    21 => ( array ( 'ref_name' => '', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://upload.wikimedia.org/wikipedia/id/f/f5/Logo_MUI.png')),
    22 => ( array ( 'ref_name' => '', 'ref_title' => '', 'ref_word' => '', 'ref_url' => '',
            'ref_img' => 'http://upload.wikimedia.org/wikipedia/id/9/9a/Logo_JAT.jpg'))

);

?>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <?php
                foreach($organ as $key=>$val)
                {
                    ?>
                    <div class="content">
                        <div class="organisasi">
                           <img src="<?php echo $val['ref_img'];?>" alt='<?php echo $val['ref_name']; ?>'>
                        </div>
                        <div class="organisasi" style="float:left;"><p><?php echo $val['ref_name']; ?></p></div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <hr class="line-mini">

        <?php 
        $data = [
            'VIDEO'=>[
                ['id'=>'w1g34RtDpvg'],
                ['id'=>'nQ59OtcEsds'],
                ['id'=>'LkPnWs6Ccq8'],
                ['id'=>'RKpQP1AAtBk'],
                ['id'=>'u6YYwFi_boM'],
                ['id'=>'B6jWirK5c1A']
            ]
        ]
        ?>        

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <!-- h4 class="kanal-title kanal-title-cokelat">BLOG</h4>
        < ? php echo $blogs;?>
        <hr class="line-mini" -->

    </div>
</div>