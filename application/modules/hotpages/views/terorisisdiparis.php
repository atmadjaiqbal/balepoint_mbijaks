<?php
$data = [
    'block1'=>[
        'title'=>'SERANGAN TERORISME DI JANTUNG PARIS',
        'narasi'=>'
            <img src="http://cdn.sindonews.net/dyn/620/content/2015/11/14/41/1061481/teror-paris-prancis-berstatus-daurat-dan-tutup-perbatasan-qoP.JPG" class="pic">
            <p>Rentetan serangan senjata,  Bom dan aksi penyanderaan  mengguncang pusat kota Paris, Prancis. Teror berdarah tersebut terjadi secara serentak di berbagai titik lokasi dalam  kota mode itu , pada jumat malam (13/11) waktu setempat.  Setidaknya  ratusan korban jiwa berjatuhan dan jumlahnya diprediksi akan meningkat secara signifikan mengingat banyaknya korban luka-luka yang masih berada dalam kondisi kritis.</p> 
            <p>Presiden Prancis Francois Hollande  melalui pernyataan resminya mengintruksikan negara dalam keadaan darurat dan mengerahkan ribuan pasukan keamanan untuk mengendalikan keadaan. Hollande juga menuding kelompok ekstremis Islam, ISIS (Negara Islam Irak dan Suriah)  dibalik teror mengerikan itu. Sehari setelah serangan, Kelompok  ISIS  secara terbuka mengaku bertanggungjawab atas serangan di Paris melalui portal resmi mereka.</p> 
        '
    ],

    'block2'=>[
        'title'=>'Teror Paling Mematikan',
        'narasi'=>'
            <img src="http://www.aktualpost.com/wp-content/uploads/2015/11/teror-bom-paris1.jpg" class="pic">

            <p>Serangan teroris yang mengguncang jantung Prancis tersebut merupakan serangkaian aksi teror yang terjadi di Prancis dan menjadi serangan paling mematikan dalam kurung waktu satu dasawarsa terkahir ini.<p> 
            <p>Sebelum Bom Paris meledak pada jumat kemarin, tercatat setidaknya sudah terjadi 13 kali aksi teror terhadap warga sipil ataupun pihak keamanan Prancis.</p> 
            <img src="http://www.dw.com/image/0,,18184468_303,00.jpg" class="pic2">
            
            <p>Serangan Charlie Heddo  yang terjadi pada Januari silam termasuk salah satu serangan teroris yang banyak menelan korban jiwa.</p> 
        '
    ],

    'block3'=>[
        'title'=>'Prancis Target Utama Teror ISIS',
        'narasi'=>'
            <img src="http://images.detik.com/community/media/visual/2015/06/01/857a9c6a-1a4d-46c2-954b-e8e09171ae64_169.jpg?w=780&q=90" class="pic">
            <p>Kota Paris mendadak mencekam setelah sekelompok  orang dengan  senjata lengkap memuntahkan peluru dan melakukan aksi bom bunuh diri di kerumunan orang. Ratusan korban berjatuhan bersimpah darah, yang mengakibatkan kepanikan luar biasa di berbagai lokasi tempat. </p>
            <p>Aksi teror berdarah yang nyaris serentak di enam titik tempat yang berbeda itu  diklaim merupakan aksi balasan dari kelompok Teroris ISIS terhadap Prancis dan negara-negara sekutunya dalam memerangi ISIS . </p>
            <img src="https://img.okezone.com/content/2015/11/14/18/1249316/korban-tewas-serangan-teror-paris-jadi-153-orang-3KLYpNTjAV.jpg" class="pic2">
            <p>Dalam rilis yang dikeluarkan oleh kelompok itu melalui kanal resminya, mereka menyatakan bahwa Prancis menjadi target teratas serangan mereka karena keterlibatannya di perang Suriah dan Irak. Dan delapan teroris yang melakukan teror di Paris merupakan Jihadis yang sengaja dikirim untuk mengguncang kota tersebut dengan berbekal senapan semi-otomatis dan rompi bom melilit di tubuh mereka. </p>
            <p>Sejak akhir September lalu, Prancis memang salah satu negara yang ikut mengintervensi  perang yang masih berkecamuk di Timur Tengah, yang didalangi oleh ISIS yang menyerukan pembentukan negara kekhalifahan Islam. Prancis mengirimkan ribuan pasukan militernya untuk mendukung operasi militer Koalisi Barat untuk menggempur markas militan di Suriah.  Gudang-gudang logistik ISIS di bagian Utara Suriah menjadi target serangan jet-jet tempur Negeri Menara Eiffel tersebut</p>

        '
    ],

    'block4'=>[
        'title'=>'Identitas Pelaku',
        'narasi'=>'
            <img src="http://cdn.sindonews.net/dyn/620/content/2015/11/14/41/1061592/delapan-pelaku-teror-paris-tewas-Jq3.jpg" class="pic">
            <p>Pencarian mengenai identitas siapa pelaku dalam serangan penembakkan dan pemboman di Paris, Perancis pada jumat lalu (14/11/2015) masih ditingkatkan pencariannya oleh aparat setempat. Pencarian kemudian difokuskan ke TKP tempat terjadinya aksi terorisme.</p>
            <p>Akhirnya diketemukan oleh aparat setempat sebuah Paspor didekat ceceran tubuh pelaku pengeboman dan diketahui merupakan paspor dari WN Suriah. Selain itu, juga diketemukan sebuah paspor juga di TKP tempat penembakkan di Bataclan.</p>
            <img src="http://images.detik.com/community/media/visual/2015/11/16/b82dd775-6a8c-42c5-88e3-286773755b23.jpg?w=780&q=90" class="pic2">
            <p>Kesamaan paspor WN Suriah yang diketemukan dalam dua TKP kejadian menjadi fokus aparat dan otoritas pemerintah Perancis. Ternyata identitas tersebut berasal dari salah satu dari seorang pengungsi Suriah yang memasuki wilayah Perancis memalui Yunani.</p>
            <p>Hal itu juga diakui oleh seorang menteri di Negara Yunani mengenai kebenaran identitas pelaku seperti dilansir oleh AFP. Pemerintah Yunani membenarkan bahwa identitas bahwa pelaku tersebut teregistrasi dalam para pencari suaka yang memasuki wilayah Yunani.</p>


       '
    ],

    'block5'=>[
        'title'=>'RANGKAIAN SERANGAN',
        'narasi'=>'
            <img src="https://images.detik.com/community/media/visual/2015/11/14/3150c42c-d202-4d18-9344-e08d7335e990.jpg?w=3500&mark=undefined&image_body_visual_id=162689" class="pic">
            Serangkaian serangan muncul mulai setelah pukul 21.00 waktu setempat di area kehidupan malam di Paris di distrik 10  ibu kota Prancis itu, tak jauh dari Place de la Republique.
            <p>Tembakan datang dari bar Le Carillon, di Jalan Alibert 18.  Seorang lelaki tak bertopeng melepaskan tembakan dengan senjata semi-otomatis. </p>

            <p>Beberapa saksi juga menyebutkan bagaimana orang itu menyeberangi jalan dan membawa senjatanya ke sebuah restoran bernama Le Petit Cambodge (Kambooja Kecil). Saksi menyebutkan 10 orang terbunuh dan etrluka dalam serangan di Jalan Alibert ini.</p>

            <p>Beberapa menit kemudian, penembakan juga terjadi tak jauh dari Le Petit Cambogde di luar restoran Mc Donald dekat Canal St Martin. </p>
    
            <p>Di wilayah utara Paris, tepatnya di Stadion Stade de France, digelar pertandingan uji coba antara Tim Nasional Prancis melawan Jerman sebagai persiapan kedua tim menjelang Piala Eropa 2016. Presiden Prancis, Francois Hollande, hadir di sana.</p>

            <p>Kira-kira setengah jam setelah kick-off, setidaknya dua ledakan terdengar. Suara itu terdengar jelas di dalam stadion dan Presiden Hollande segera diamankan.</p>
            <img src="https://images.detik.com/community/media/visual/2015/11/14/7d3b28a8-1e92-44a7-aeda-c97e6a13bddc_169.jpg?w=620&mark=undefined&image_body_visual_id=162600" class="pic2">

            <p>21.50 Laporan berikutnya datang dari wilayah selatan. Restoran pertama yang diserang adalah bar La Belle Equipe di Jalan de Charonne di Distrik 11. Dua orang terlihat melepaskan tembakan dengan membuntahkan peluru ke arah teras kafe.Kemudian mereka kembali ke mobil dan meluncur menuju arah stasiun Charonne."</p>
            <p>22.00 Serangan paling mematikan malam itu terjadi di Distrik 11 di Boulevard Voltaire ketika sekelompok pria bersenjata mendatangi lokasi konser grup terkenal asal California, Amerika Serikat, Eagles of Death Metal. Para penyerang itu memasuki ruangan dan menembak ke udara. Salah satu dari para penyerang itu berteriak "Allahu Akbar". Para penyerang itu kemudian menjadikan penonton konser sebagai sandera. Beberapa tembakan terdengar pada pukul 00.30. Para penyerang mulai menembak dan puluhan orang dilaporkan tewas. Beberapa serangan lain dikabarkan terjadi seperti di Avenue de la Republique serta  Boulevard de Beaumarchais.</p>
        '
    ],

    'block6'=>[
        'title'=>' Daftar aksi teror di Prancis dalam 10 tahun terakhir',
        'narasi'=>'
        <ol>
            <li>Kelompok Front Nasional Pembebasan Corsica melancarkan serangan bom di kantor regional direktorat bea cukai dan perbendaharaan negara kota Nice. Serangan yang terjadi pada 20 Juli 2013 melukai 16 orang.</li>  

            <li>Pada 8 Oktober 2004, 10 orang terluka akibat serangan bom yang meledak di dekat Kedutaan Besar Republik Indonesia (KBRI) Paris. Kelompok Front Bersenjata Islam Prancis bertanggung jawab terhadap insiden tersebut.</li>
             
            <li>Dua personel Garda Sipil Spanyol yang tengah melakukan pengawasan terhadap anggota Euskadi Ta Askatasuna (ETA), kelompok yang menuntut kemerdekaan wilayah Basque di Spanyol tewas tertembak pada 1 Desember 2007</li>

            <li>Serangan bersenjata di kota Toulouse dan kota Montauban yang dilakukan oleh kelompok yang menamakan diri Mohammed Merah menewaskan tiga personel pasukan payung Prancis, seorang Rabbi Yahudi dan tiga anak sekolah Yahudi dan melukai lima orang lainnya. </li>

            <li>Pada 23 Mei 2013 seorang tentara Prancis di Paris terluka akibat ditusuk seseorang yang diduga sebagai kelompok Islam radikal.</li>
             
            <li>Pada 20 Desember 2014 seorang pria melakukan penusukan yang mengakibatkan luka bagi tiga petugas di kantor polisi. Penusuk tersebut tewas ditembak polisi.</li>
             
            <li>Pada 21 Desember 2014 belasan orang terluka akibat ditabrak mobil orang diduga merupakan simpatisan kelompok radikal di kota Dijon attack. Pelaku berhasil ditangkap polisi setelah mencoba bunuh diri.</li> 

            <li>Penembakan massal terjadi Pada 7 dan 9 Januari 2015 di kantor majalah satir Charlie Hebdo di Paris oleh dua orang yang mengaku anggota Al-Qaeda di Yaman, Said Kouachi dan Chérif Kouachi. Sebanyak 20 orang tewas dan 22 lainnya terluka akibat penembakan tersebut. Sedangkan dua penembakan dan penyanderaan di pasar halal Hypercacher, Paris dilakukan oleh 
            anggota ISIS</li>
             
            <li>Tanggal 3 Februari 2015, Tiga tentara penjaga pusat komunitas Yahudi di Nice terluka parah akibat diserang oleh seorang bernama Moussa Coulibaly. Belakangan diketahui aksi Moussa tidak terkait dengan penembakan massal di kantor Charlie Hebdo dan gerakan ISIS.</li>
             
            <li>Pada 19 April 2015 seorang yang mengaku mengaku pejuang Aljazair menembak mati seorang perempuan di kota Villejuif. </li>
             
            <li>Pada 26 Juni 2015 ada seorang tewas dan dua orang lainnya terluka dalam serangan bersenjata tajam di Saint Quentin-Fallavier. Di hari yang sama, seorang pengemudi dilaporkan dipenggal oleh kelompok terkait ISIS.</li>
             
            <li>Pada 21 Agustus 2015 terjadi aksi penembakan di kereta cepat Thalys dari Amsterdam menuju Paris. Empat orang dilaporkan terluka parah dalam insiden tersebut. </li>

            <li>Jumat malam,  13 November 2015 terjadi serangan teroris di beberapa tempat di pusat kota Paris. Laporan resmi dari media setempat melaporkan 153 jiwa melayang, dan melukai ratusan orang lainnya. Serangan bom bunuh dan penembakan brutal yang disertai aksi penyanderan tersebut dilakukan oleh para milisi ISIS.</li>         

        </ol>

        '
    ],

    'block7'=>[
        'title'=>'Lokasi Serangan dan Jumlah Korban',
        'narasi'=>'
            <p><img src="'.base_url('assets/images/hotpages/terorisisdiparis/map.jpg').'" style="width: 100%;"></p>
            
        '
    ],

    'block8'=>[
        'title'=>'Profil Kota Paris',
        'narasi'=>'
            <img src="http://4.bp.blogspot.com/-9OlQLJdd1Pc/UhrahNN_qOI/AAAAAAAAAe8/nOKi0kk8ZGI/s1600/eiffel-tower1-788299.jpg" class="picprofil" style="width: 100%;">
            
            
                <table style="margin-left: 15px;margin-right: 15px;">
                    <tr>
                        <td style="width:90px;">Negara</td>
                        <td style="width:15px;">:</td>
                        <td>Perancis</td>
                    </tr>
                    <tr>
                        <td>Bahasa Resmi</td>
                        <td>:</td>
                        <td>Bahasa Perancis</td>
                    </tr>
                    <tr>
                        <td>Mata Uang</td>
                        <td>:</td>
                        <td>Euro</td>
                    </tr>
                    <tr>
                        <td>Pemerintahan</td>
                        <td>:</td>
                        <td>Gubernur Anne Hidalgo (2014-sekarang)</td>
                    </tr>
                    <tr>
                        <td>Luas Wilayah</td>
                        <td>:</td>
                        <td>105.4 km2 (40.7 sq mi)</td>
                    </tr>
                    <tr>
                        <td><div style="margin-left: 20px;">Urban</div></td>
                        <td>:</td>
                        <td>2,844.8 km2(1,098.4 sq mi)</td>
                    </tr>
                    <tr>
                        <td><div style="margin-left: 20px;">Metro</div></td>
                        <td>:</td>
                        <td>17,174.4 km2(6,631.1 sq mi)</td>
                    </tr>
                    <tr>
                        <td>Populasi</td>
                        <td>:</td>
                        <td>2,241,346 Jiwa</td>
                    </tr>
                    <tr>
                        <td><div style="margin-left: 20px;">Urban</div></td>
                        <td>:</td>
                        <td>10,516,110 Jiwa</td>
                    </tr>
                    <tr>
                        <td><div style="margin-left: 20px;">Metro</div></td>
                        <td>:</td>
                        <td>12,161,542 Jiwa</td>
                    </tr>

                </table>
        '
    ],

    'block9'=>[
        'title'=>'Serangan Teroris di Eropa',
        'narasi'=>'
            <p class="rightcol font_kecil" style="margin-right: 10px;">
            <ol style="margin-right: 25px;margin-left:34px;">
                <li class="font_kecil">
                <strong>Museum Yahudi Brussels</strong><br/>
                Pada 24 Mei 2014 empat orang terbunuh di Museum Yahudi di Brussels, Belgia. Penyerang adalah mantan militan Perancis yang berhubungan dengan ISIS di Suriah.
                </li>
                <li class="font_kecil">
                <strong>Sekolah Yahudi Perancis</strong><br/>
                Pada Maret 2012, seorang pria bersenjata yang berkaitan dengan al-Qaidah membunuh tiga anak sekolah Yahudi, seorang rabbi, tiga pasukan keamanan.
                </li>
                <li class="font_kecil"> 
                <strong>Serangan di Oslo</strong><br/>
                Pada 22 Juli 2011, seorang anti-Muslim Anders Behring Breivik menanam sebuah bom di Oslo kemudian menyerang kamp pemuda yang berhubungan dengan Partai Buruh Norwegia di Pulau Utoya, menewaskan 77 orang, mayoritas remaja.
                </li>
                <li class="font_kecil">    
                <strong>Serangan kereta api di London</strong><br/>
                Pada 7 Juli 2005, 25 orang tewas dalam serangan bom bunuh diri di tiga kereta bawah tanah dan sebuah bus. Serangan yang terkoordinasi ini disebut sebagai yang terburuk di tanah Inggris. Serangan dilakukan oleh empat orang yang mengaku terinspirasi oleh al-Qaidah. 
                </li>
                <li class="font_kecil">
                <strong>Serangan kereta api di Madrid</strong><br/>
                Pada 11 Maret 2004, rangkaian bom di kereta api menewaskan 191 orang, dan melukai lebih dari 1.800 orang. Ini merupakan serangan teroris terburuk di Eropa sejak pengeboman Lockerbie pada 1988.
                    </li>
                <li class="font_kecil">
                <strong>Pengeboman di Konsulat Inggris</strong><br/>
                Pada November 2003, setidaknya 27 orang terbunuh dan lebih dari 400 orang terluka pada pengeboman di Konsulat Inggris dan pusat bank HSBC di Istanbul, Turki. Sepekan sebelumnya, seorang pelaku bom bunuh diri menargetkan dua sinagoga di Istanbul, menewaskan lebih dari 20 orang.
                </li>
                <li class="font_kecil">
                <strong>Bom di stasiun kereta</strong><br/>
                Sebuah bom di stasiun kereta api bawah tanah Saint-Michel, Paris, meledak dan menewaskan 8 orang, melukai 150 orang. Seranan dilakukan olej Armed Islamic Group (GIA) dari Aljazair.
                </li>
                <li class="font_kecil">
                <strong>Pengeboman Lockerbie</strong><br/>
                Sebuah bom meledak di pesawat Pan Am 103, 21 Desember 1988. Pesawar akan menuju New York dari London. Sebanyak 259 orang di pesawat dan 11 orang di lokasi tewas di kota Lockerbie, Skotlandia. Seorang intel Libya menjadi terdakwa dalam serangan.
                </li>
                </ol>
            </p>


        '
    ],    

    'institusiPendukung' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/negaraislamirakdansyamisis543ca797baf78','image'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150321_120850_harianterbit_isis_bendera.jpg','title'=>'ISIS'],
    ],
    

    'institusiPenentang' => [
        ['link'=>'#','image'=> 'http://www.sinarpaginews.com/bk27panel/modul/images_content/oki.jpg','title'=>'OKI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/badanintelijennegara51a2cdfd46386','image'=> 'http://www.indoberita.com/wp-content/uploads/2014/10/BIN-Jokowi-JK.jpeg','title'=>'BIN'],
        ['link'=>'http://www.bijaks.net/aktor/profile/kepolisiannegararipolri536756e743fd0','image'=> 'http://img08.deviantart.net/69dd/i/2013/095/1/c/logo_polri_by_crime1985-d60iljo.jpg','title'=>'POLRI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/nadhlatululama54c1fa023c629','image'=> 'http://kswmesir.org/wp-content/uploads/2015/07/51.jpg','title'=>'NU'],
        ['link'=>'http://www.bijaks.net/aktor/profile/ppmuhammadiyah5296b45eefcaa','image'=> 'http://www.rmol.co/images/berita/normal/419605_02542001072015_muhammadiyah.jpg','title'=>'MUHAMMADIYAH'],
        ['link'=>'http://www.bijaks.net/aktor/profile/kementerianagama51dd066da966d','image'=> 'http://www.muslimdaily.co/wp-content/uploads/2014/09/Kementerian-Agama.jpg','title'=>'KEMENAG'],
        ['link'=>'http://www.bijaks.net/aktor/profile/majelisulamaindonesia53097864039ce','image'=> 'http://cdn.ar.com/images/stories/2014/03/halal-mui.jpg','title'=>'MUI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdpr51da66b0a7ac4','image'=> 'http://hidayatullah.or.id/wp-content/uploads/2015/04/LOGO-DPR-RI.png','title'=>'DPR RI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/komisinasionalhakasasimanusia531e9843271ee','image'=> 'http://1.bp.blogspot.com/-qqaRNZbiWfY/U_PPWhFQyII/AAAAAAAAD6I/cmKY54JcBFs/s1600/Logo%2BKomnas%2BHAM.jpg','title'=>'KOMNAS HAM'],
    ],

    'negaraPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/amerikaserikat519c86ef28572','image'=> 'https://spektrumku.files.wordpress.com/2008/06/us-flag.jpg','title'=>'AS'],
        ['link'=>'http://www.bijaks.net/aktor/profile/arabsaudi538175cd1a906','image'=> 'http://media.viva.co.id/thumbs2/2008/09/23/54603_bendera_saudi_arabia_663_382.jpg','title'=>'Arab Saudi'],
        ['link'=>'#','image'=> 'https://upload.wikimedia.org/wikipedia/commons/thumb/archive/9/9f/20151013153513!Flag_of_Indonesia.svg/1200px-Flag_of_Indonesia.svg.png','title'=>'Indonesia'],
        ['link'=>'#','image'=> 'http://1.bp.blogspot.com/_Cc3gulUhlvs/TTuZjWhlcMI/AAAAAAAADDQ/w0MuLpBYkeY/s1600/bendera-Belgium.JPG','title'=>'Belgia'],
        ['link'=>'http://www.bijaks.net/aktor/profile/republikfederaljerman5371f1f61c897','image'=> 'https://upload.wikimedia.org/wikipedia/commons/b/ba/Flag_of_Germany.svg','title'=>'Jerman'],
        ['link'=>'#','image'=> 'http://www.bijaks.net/assets/images/flags/jordania.gif','title'=>'Yordania'],
        ['link'=>'#','image'=> 'http://fnoor.com/upload/userfiles/images/fn0374.jpg','title'=>'Iran'],
        ['link'=>'http://www.bijaks.net/aktor/profile/irak5371bcc81e12c','image'=> 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Flag_of_Iraq.svg/250px-Flag_of_Iraq.svg.png','title'=>'Irak'],
        ['link'=>'#','image'=> 'http://www.bijaks.net/assets/images/flags/uk.gif','title'=>'Inggris'],
        ['link'=>'http://www.bijaks.net/aktor/profile/turki537aebd4d6237','image'=> 'http://cdn-2.tstatic.net/banjarmasin/foto/bank/images/bendera-turki.jpg','title'=>'Turki'],
        ['link'=>'http://www.bijaks.net/aktor/profile/rusia5371c1920880f','image'=> 'http://images.gofreedownload.net/russia-grungy-flag-wallpaper-russia-world-171015.jpg','title'=>'Rusia'],
    ],


//    'partaiPendukung' => [
//        ['link'=>'','','title'=>''],
//    ],

//     'partaiPenentang' => [
//         ['link'=>'','image'=>'','title'=>''],
//     ],
    'quoteProtes'=>[
        ['from'=>'Bashar Al Assad','jabatan'=>'Presiden Suriah','img'=>'http://i.telegraph.co.uk/multimedia/archive/01925/Bashar-al-Assad-su_1925765b.jpg','url'=>'http://www.bijaks.net/aktor/profile/basharalassad539564f3b799b','content'=>'"Kami tidak bisa bicara tentang berbagi intelijen untuk melawa terorisme, ketika kebijakan pemerintah Prancis malah menyokong terorisme itu sendiri"'],
        ['from'=>'Anis Matta','jabatan'=>'mantan Presiden PKS','img'=>'http://statis.dakwatuna.com/wp-content/uploads/2013/02/muhammad-anis-matta.jpg','url'=>'http://www.bijaks.net/aktor/profile/hmanismattalc50f7ba1b1520d','content'=>'"Saat ini mari kita melihat jernih situasi. Tidak perlu membuat spekulasi. Saatnya bekerja sama untuk kemanusiaan"'],
        ['from'=>'Ust. Felix Siauw','jabatan'=>'-','img'=>'https://i.ytimg.com/vi/6CyQiYjP58U/hqdefault.jpg','url'=>'#','content'=>'"Kita berduka atas apa yang terjadi di Paris, dan lebih terluka saat membayangkan, bahwa di Suriah dan Gaza, itu terjadi tiap waktu"'],
        ['from'=>'Andi Arief','jabatan'=>'Mantan Staff Khusus Presiden Era SBY','img'=>'http://news.bijaks.net/uploads/2015/11/Andi-Arif-528x353.jpg','url'=>'http://www.bijaks.net/aktor/profile/alamandiarief53785b2fc8b16','content'=>'"Serangan pada masyarakat sipil di Prancis serangan "kampungan" tapi berdampak pada kepanikan sipil seluruh dunia"'],
    ],


    // 'quotePendukung'=>[
    //     ['from'=>'Zainal Abidin','jabatan'=>'Ketua Pengurus Besar Ikatan Dokter Indonesia','img'=>'http://img1.beritasatu.com/data/media/images/medium/1390391719.jpg','url'=>'#','content'=>'"Dalam etika kedokteran, dokter dibolehkan mendapat sponsorship berupa biaya transportasi, penginapan, dan makan untuk pendidikan berkelanjutan seperti seminar atau simposium."'],
    //     ['from'=>'Pieter Talaway','jabatan'=>'Pengacara PT. Interbat','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2013--04--38405-pieter-talaway-pn-surabaya-langgar-sema-no-1-tahun-2012.jpg','url'=>'#','content'=>'"Semua perusahaan itu cari untung, tapi harus ikut aturan. Kalau mengirimkan sales untuk menawarkan dan mempromosikan obat ke dokter, masak enggak boleh?""'],
    //     ['from'=>'Kelik Suhendri','jabatan'=>'Mantan Medical Representative Perusahaan Farmasi','img'=>'http://4.bp.blogspot.com/-pVIC0JMQWGk/VYpWWC_v9jI/AAAAAAAAA7o/SGnjmObfj6U/s200/dokter.jpg','url'=>'#','content'=>'"Setelah mereka selesai dihibur dan ‘pijit’, saya ditelepon dan disuruh membayar," ujar Kelik yang sekarang memilih berwiraswasta. "Tiap dokter," katanya, "menghabiskan uang sekitar Rp 8 juta."'],
    //     ['from'=>'Teddy Tjahjanto','jabatan'=>'Dokter Spesialis Penyakit Dalam yang mendapat suap dari PT. Interbat','img'=>'http://s3-eu-west-1.amazonaws.com/docplanner.co.id/doctor/19916c/19916ca4b4e6097c66abe59262c9c52f_large.jpg','url'=>'#','content'=>'"Semua farmasi begitu. Kalu enggak, apotik enggak bisa kasih murah sama orang."'],
    // ],

    'quotePenentang'=>[
        ['from'=>'Jokowi','jabatan'=>'Presiden Indonesia','img'=>'http://www.rmol.co/images/berita/normal/859494_12493403052015_Jokowi3.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'"Menyampaikan duka mendalam atas peristiwa yang menimpa Prancis. Kita mengutuk keras serangan teror di Prancis"'],
        ['from'=>'HM. Jusuf Kalla','jabatan'=>'Wakil Presiden RI','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYajJA7i3foDV7imodi2JD8JH2jY-DzK5T29X5aI6Yaig6Cpp-0w','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'"Seperti yang dikatakan presiden, Indonesia terkejut dan tentu mengutuk semua aksi seperti itu. Kita kan sudah mengalami bagaimana akibat dari pada teror seperti itu"'],
        ['from'=>'Barak Obama','jabatan'=>'Presiden AS','img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTJaLwuAy01s6YUxn88dD9FIXdC818G5TDZw0_vj_CPJxU8dK1G','url'=>'#','content'=>'"Penembakan dan peledakan di paris adalah serangan terhadap kemanusiaan dan nilai universal yang kita anut selama ini"'],
        ['from'=>'Hassan Rouhani','jabatan'=>'Presiden Iran','img'=>'http://thepeninsulaqatar.com/images/Hassan-Rouhani.jpg','url'=>'#','content'=>'"Atas kebesaran nama bangsa Iran yang menderita kejahatan terorisme, Saya mengutuk penyerangan Paris"'],
        ['from'=>'Haider al-Abadi','jabatan'=>'Perdana Menteri Irak','img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQAntF875EwHh_HD9taBF_yUaiI5TqYG6XqtQDZ07QdmTuESb-M','url'=>'#','content'=>'"Serangan mengerikan di Paris menandakan betapa besarnya ancaman terorisme global itu yang juga akan menemui untuk kita semua dan upaya internasional diperlukan untuk menghilangkannya"'],
        ['from'=>'Zulkifli Hasan','jabatan'=>'Ketua MPR','img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcS6bgNvMj3e9o-WHP3SudkUhuO9Y57R_PVtTtAT4414Z1v9-WQMTQ','url'=>'http://www.bijaks.net/aktor/profile/zulkiflihasan50ef907d0661c','content'=>'"Ini adalah tragedi kemanusiaan. Tidak ada agama apapun yang mendukung tindakan biadab seperti ini"'],
        ['from'=>'Lukman Hakim','jabatan'=>'Menteri Agama','img'=>'http://www.sayangi.com/media/k2/items/cache/a46a7dc19c2c87928527e1cfd49d9012_XL.jpg','url'=>'','content'=>'"Apapun alasan yang mendasarinya, tindak kekerasan yang terjadi di Paris yang mengakibatkan jatuhnya ratusan korban jiwa dan luka-luka, sama sekali tidak bisa dibenarkan. Semua kita harus menentang tindakan yang menista martabat kemanusiaan"'],
        ['from'=>'Moeldoko','jabatan'=>'Mantan Panglima TNI','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1XYA_YI1NI_svS2RjGdGAc579CjgAz9N_2bt4P_fzIZdy52YIGQ','url'=>'http://www.bijaks.net/aktor/profile/jenderaltnimoeldoko5227e0280b4dc','content'=>'"Jangan hanya mengandalkan TNI dan Polri. Semua masyarakat Indonesia harus waspada bahwa ancaman itu ada di sekitar kita"'],
        ['from'=>'Said Aqil','jabatan'=>'Ketua PB NU','img'=>'http://www.suara-islam.com/images/berita/said_aqil_20140913_115758.jpg','url'=>'http://www.bijaks.net/aktor/profile/saidaqilsiradj5343c56457ee7','content'=>'"Segala motif yang menjadi latar belakang serangan bersenjata dan ledakan bom di Paris tidak bisa dibenarkan, baik menurut agama maupun aturan Internasional"'],
        ['from'=>'Haedar Nashir','jabatan'=>'Ketua PP Muhammadiyah','img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcReQ5C57DDI_T79RZ6D4TbPfFo2JnVL6rvgFNc27nxUUUXUkb4Bcw','url'=>'http://www.bijaks.net/aktor/profile/hhaedarnashirdrmsi55c2cc801c322','content'=>'"Islam tidak mengajarkan kekerasan dan tindakan brutal. Semua pihak tidak boleh berasumsi stigma terhadap Islam dan umat Islam"'],
        ['from'=>'Yenny Wahid','jabatan'=>'Direktur Wahid Institute','img'=>'https://www.islamtoleran.com/wp-content/uploads/2014/11/yenny-wahid-_130416161758-936.jpg','url'=>'http://www.bijaks.net/aktor/profile/yennywahid50d27b7096177','content'=>'"Islam jelas-jelas mengharamkan aksi terorisme (hirabah)"'],
        ['from'=>'Fadli Zon','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://fajar.co.id/wp-content/uploads/2015/03/fadli-zon.jpg','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007','content'=>'"Tragedi Paris adalah duka dan keprihatinan bagi masyarakat dunia bukan hanya rakyat Prancis. Kita semua mengecam keras aksi teror tersebut"'],
        ['from'=>'KH Cholil Nafis','jabatan'=>'Ketua Komisi Dakwah MUI','img'=>'http://mui.or.id/wp-content/uploads/2014/03/Kyai-Cholil.jpg','url'=>'http://www.bijaks.net/aktor/profile/cholilnafis54e3007b1cfc2','content'=>'"Mengutuk setiap aksi kekerasan yang mengorbankan nyawa orang banyak. Bagaimana pun aksi itu tidak sesuai dengan ajaran agama apapun"'],
        ['from'=>'Budiman Sujatmiko','jabatan'=>'Politisi PDIP','img'=>'http://www.teropongsenayan.com/foto_berita/201506/12/medium_16Budiman%20Sudjatmiko%20001.jpg','url'=>'http://www.bijaks.net/aktor/profile/budimansudjatmikomscmphil50f8c130dca48','content'=>'"Yang mengkhawatirkan dari bom ini adalah ini akan munculkan sikap anti imigran"'],
        ['from'=>'Maneger Nasution','jabatan'=>'Komisioner Komnas HAM','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTMdmy0SWONkb-CWkZlYDvAsIzrargRPe90Tq3RctMT3ydWGvfU','url'=>'http://www.bijaks.net/aktor/profile/drsmanegernasution51a561e5b53af','content'=>'"Tak ada urgensi dan relevansinya, serta tak perlu sama sekali adanya aksi balasan dengan cara-cara kekerasan yang mengatasnamakan agama"'],
        ['from'=>'Ade Armando','jabatan'=>'Dosen Ilmu Politik UI','img'=>'http://statis.dakwatuna.com/wp-content/uploads/2015/08/ade-armando.jpg','url'=>'http://www.bijaks.net/aktor/profile/adearmando5562975cb7602','content'=>'"Pembantaian Paris mengajarkan kita apa yang akan terjadi kalau kita membiarkan radikalisme agama berkembang"'],
    ],

    'video'=>[
        ['id'=>'0o7fiSE4DvM'],
        ['id'=>'12-JsVy956s'],
        ['id'=>'SgX0OxVFi4s'],
        ['id'=>'igkEorotA5M'],
        ['id'=>'SobYd3AvseY'],
        ['id'=>'al7ZqWNDABA'],
        ['id'=>'fLc6KwnynKs'],
        ['id'=>'dGjkWq57J20'],
        ['id'=>'zMUZb_dVafo'],
        ['id'=>'iFR8j-Z2pK8'],
        ['id'=>'I3uUQb4fvy0'],
        ['id'=>'Tm69WZL-YIM'],
        ['id'=>'ogD3BDKBb9g'],     
    ],

    'foto'=>[
                ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/1054904/big/025110100_1447478829-2E6CC76C00000578-3317776-image-a-8_1447455928589.jpg'], 
                ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/1055029/big/080169100_1447488342-20151114-Lokasi_Bom-Paris.jpg'],
                ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/11/14/41/1061525/warga-paris-tawarkan-perlindungan-untuk-orang-asing-D4Q.jpg'],
                ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/1055023/big/091816500_1447488013-20151114-Teror_Bom-Paris.jpg'],
                ['img'=>'http://t2.gstatic.com/images?q=tbn:ANd9GcT2ZVa878VuhFm23uBnUs3lhcK4VzlPhv9M_oEOK2ZYa4HF_a2ZcA'],
                ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/1054735/big/069441100_1447456478-reuters-3.jpg'],
                ['img'=>'http://assets.juara.net/assets/new_uploaded/images/medium_paris.jpg'],
                ['img'=>'http://t1.gstatic.com/images?q=tbn:ANd9GcQUkXLOzDSlSADdB_Qo8fjyeI6WfFaUVpq6Q7InqPPJRRfWVpI'],
                ['img'=>'http://media.viva.co.id/thumbs2/2015/11/14/348107_salah-satu-korban-aksi-teror-di-paris--prancis-_663_382.jpg'],
                ['img'=>'http://img1.beritasatu.com/data/media/images/medium/291447462499.jpg'],
                ['img'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcRh5stb8PJ0_c1i1QYHyEW5_N6vyLMR6FeGlX3sW7hKmeGSM9NO'],
                ['img'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcRUl6CDfsFX_rQO7hM6408RJ6_v2ouE-1yKvwKYQhKM2ppatqo'],
                ['img'=>'http://www.dnaberita.com/foto_berita/67Ledakkan.jpg'],
                ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/11/14/450736/6vOvNe86zL.jpg?w=668'],
                // ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/11/14/623695/670x335/isis-resmi-mengaku-bertanggung-jawab-atas-teror-di-paris.jpg'],
                ['img'=>'http://kriminalitas.com/wp-content/uploads/2015/11/teror-paris-paris-paris.jpg'],
                ['img'=>'http://statik.tempo.co/?id=111204&width=620'],
                ['img'=>'http://assets.rappler.com/5FCC1F0A0232463D821E5DACC7F71632/img/C5F6E8E847C74D51BC62D31F07931905/paris-terrorist-attacks-epa-20151113-003.jpg'],
                ['img'=>'http://assets.rappler.com/5FCC1F0A0232463D821E5DACC7F71632/img/BB525955788E43D0A16B6ECF282E4BBC/paris-terrorist-attacks-epa-20151114-005.jpg'],
                ['img'=>'http://static.republika.co.id/uploads/images/inpicture_slide/serangan-tembakan-dan-bom-di-paris-prancis-jumat-malam-_151114091637-330.jpg'],
                ['img'=>'http://cdn-2.tstatic.net/kaltim/foto/bank/images/serangan-teror-di-paris_1_20151114_114340.jpg'],
                ['img'=>'http://assets2.jpnn.com/picture/normal/20151115_205232/205232_791791_teror_di_paris_3.jpg'],
                ['img'=>'http://cdn-2.tstatic.net/batam/foto/bank/images/prancis-diserang-teror_20151114_104932.jpg'],
                ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/11/14/41/1061627/paspor-suriah-ditemukan-di-tubuh-pelaku-teror-paris-h7s.jpg'],
                ['img'=>'http://cdn.rimanews.com/bank/low/paris1234.jpg'],
                ['img'=>'http://assets2.jpnn.com/picture/normal/20151115_193342/193342_171042_Paris_omar_mostefai_besar.jpg'],
                ['img'=>'http://cms.monitorday.com/statis/dinamis/detail/19015.JPG'],
                ['img'=>'http://sidomi.com/wp-content/uploads/2015/11/ISIS-Klaim-Sebagai-Pelaku-Serangan-Paris-Ancam-Masih-Banyak-Sasaran-Lagi-1-566x600.jpg'],
                ['img'=>'http://jakartagreater.com/wp-content/uploads/2015/11/teror-paris-2.jpg'],
                ['img'=>'http://cdn-2.tstatic.net/aceh/foto/bank/images/bom-paris_20151114_180626.jpg'],
                // ['img'=>'http://cdn.klimg.com/dream.co.id/resources/news/2015/11/15/22593/664xauto-paspor-wn-suriah-ditemukan-di-lokasi-bom-bunuh-diri-paris-151115h.jpg'],
                ['img'=>'http://static.gatra.id/images/gatracom/2015/dani/11-Nov/lokasi-bom-paris-2.jpg'],
                ['img'=>'http://www.sisidunia.com/wp-content/uploads/2015/11/134240_799299_teror_paris_afp.jpg'],
                ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/01/07/41/947467/paska-serangan-di-paris-hollande-gelar-pertemuan-darurat-9tT.JPG'],
                ['img'=>'http://ketemulagi.com/wp-content/uploads/2015/11/Pray-for-Paris.jpg'],
    ],

    'BERITA'=>[
        ['img'=>'','shortText'=>'','link'=>''],
    ],

   // 'kronologi'=>[
   //     'list'=>[
   //         ['date'=>'','content'=>'
   //         '],
   //         ['date'=>'','content'=>'
   //         '],
   //         ['date'=>'','content'=>'
   //         '],
   //     ]
   // ],

]



?>

<style>
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/terorisisdiparis/top.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/terorisisdiparis/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['block1']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block8']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block8']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>
        
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block2']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block2']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block3']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block3']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block4']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block4']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block5']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block5']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block6']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block6']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block7']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block7']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">NEGARA KORBAN</span></h5>
        <div id="accordion" class="panel-group row">
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                    <a href="http://www.bijaks.net/aktor/profile/perancis537ac112890fa">
                        <img src="http://2.bp.blogspot.com/-PuSAEeRPyOc/TWvn8xeNECI/AAAAAAAACIM/AugfqNciBwE/s1600/Gambar%2BBendera-Negara%2BPrancis.gif" style="width: 100%;margin-top: 5px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 1em;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;">
                        PRANCIS</b><br>
                    <div class="clear"></div>
                    </p>
                </div>
        </div>
        <div class="clear"></div>

        <?php if(!empty($data['negaraPenentang'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">NEGARA PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['negaraPenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                        <a href="<?php echo $val['url'];?>">
                            <img src="<?php echo $val['image'];?>" style="width: 100%;margin-top: 5px;margin-right: 5px;float: left;"/>
                        </a>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 1em;color: red;margin-left: 10px;margin-right: 10px;"><?php echo $val['title'];?></p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>

       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">NEGARA PROTES</span></h5>
        <div id="accordion" class="panel-group row">
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                    <a href="http://www.bijaks.net/aktor/profile/suriah5371b98429bfd">
                        <img src="http://1.bp.blogspot.com/-rZjXBqYvrsA/UBFQOkyOSmI/AAAAAAAAANI/T1OBu1ZcvgA/s1600/gambar-bendera-syria.JPG" style="width: 100%;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 1em;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;">
                        SURIAH</b><br>
                    <div class="clear"></div>
                    </p>
                </div>
        </div>
        <div class="clear"></div>

        <?php /*
        <?php if(!empty($data['institusiPendukung'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">INSTITUSI TERLIBAT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['institusiPendukung'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                    <a href="<?php echo $val['url'];?>"><img src="<?php echo $val['image'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>
        
       

       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">NEGARA PROTES</span></h5>
        <div id="accordion" class="panel-group row">
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                    <a href="http://www.bijaks.net/aktor/profile/suriah5371b98429bfd">
                        <img src="http://1.bp.blogspot.com/-rZjXBqYvrsA/UBFQOkyOSmI/AAAAAAAAANI/T1OBu1ZcvgA/s1600/gambar-bendera-syria.JPG" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;">
                        SURIAH</b><br>
                    <div class="clear"></div>
                    </p>
                </div>
        </div>
        <div class="clear"></div>
        */?>



        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block9']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block9']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <?php if(!empty($data['quotePendukung'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePendukung'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>

        <?php if(!empty($data['quoteProtes'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PROTES</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quoteProtes'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>">
                    <img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>



		<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['foto'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['video'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
<?php /*
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['berita'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>
*/ ?>
        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>
