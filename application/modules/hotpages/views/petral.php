<?php 
$data = [
    'NARASI'=>[
        'title'=>'GURITA PETRAL',
        'narasi'=>'<img src="http://cdn.sindonews.net/dyn/620/content/2015/05/14/34/1001100/petral-bubar-pemerintah-seharusnya-lihat-untung-rugi-mqQ.jpg" class="pic">Sejak Tanggal 13 mei 2015, pemerintah  secara resmi  membubarkan sekaligus melikuidasi  anak usaha PT Pertamina (Persero), PT Pertamina Energy Trading Limited (Petral). Perusahaan yang berkantor di Singapura selama ini menjadi trader yang melakukan aktivitas jual-beli minyak dari negara-negara produsen, yang kemudian dijual ke Pertamina. 
                    </p><p>Mekanisme seperti ini dinilai tidak efektif dan justru menjadi celah maraknya praktek korupsi yang dilakukan oleh mafia migas. Langkah pembubaran yang diambil oleh pemerintah ini dinilai tepat. Pertama untuk mengefisiensikan biaya pengadaan minyak. Dan kedua, untuk memutus mata rantai mafia-mafia migas yang selama ini menggerogoti tubuh Pertamina, terutama terkait pengadaan harga minyak dalam negeri. 
                    </p><p><img src="http://assets.kompas.com/data/photo/2014/11/19/114648620141119-103537780x390.JPG" class="pic2">Mafia-mafia migas yang menikmati “bisnis minyak haram” di Petral semakin menggurita sejak Susilo Bambang Yudhoyono berkuasa. Sehingga upaya pemberantasan mafia migas sulit dilakukan karena para pemainnya dekat dengan simpul-simpul kekuasaan. Rezim SBY diduga menjadi benteng bagi para mafia tersebut.
                    </p><p>Keterkaitan antara rezim SBY dengan mafia migas bukan kali ini saja mencuat kepermukaan, sudah sejak dulu ketika SBY masih menjadi presiden hal ini selalu dikaitkan dengan skandal korupsi yang ada di tubuh Petral. 
                    </p><p><img src="http://www.konfrontasi.com/sites/default/files/styles/article_big/public/article/2014/09/sby-dahlan130105c.jpg?itok=tGt2VXfg" class="pic">Rencana pembubaran yang pernah diwacanakan oleh Menteri BUMN, Dahlan Iskan mendapat reaksi keras dari pihak-pihak dari lingkungan penguasa. Pihak Pertamina dan Petral didukung penuh oleh pemerintah selalu berdalih, bahwa kewenangan impor minyak mereka dijalankan sesuai prosedur yang telah ada. Upaya tersebut selalu kandas karena adanya kekuatan besar yang menaungi dan mampu melindungi para pemain bisnis minyak tersebut. Seperti kata Dahlan Iskan kala itu.'
    ],
    'ANALISA'=>'Petral sebagai perantara untuk pasokan minyak dengan cara membeli minyak dari mana saja, lalu kemudian dijual kembali ke pertamina sudah menjalankan fungsinya sebagai badan usaha di bawah PT Pertamina. 
                </p><p>Namun aktivitas jual-beli minyak yang dilakukan oleh Petral justru menjadi celah terbuka bagi oknum-oknum Pertamina ataupun para mafia untuk mendulang untung sebesar-besarnya. 
                </p><p><img src="http://eksplorasi.co/wp-content/uploads/2014/09/mafia-migas-2.jpg" class="pic2">Keberadaan para oknum mafia  yang menjadi broker atau penyalur minyak tersebut sulit dihentikan karena. Pertama, Pertamina Energy Trading Limited (Petral) yang berdomisi di luar negeri tidak dapat dintervensi oleh hukum Indonesia karena perusahaan tersebut tunduk pada aturan hukum dimana ia berada. Sehingga untuk mengusut setiap dugaan kasus pelanggaran atau penyelewangan yang terjadi di dalamnya tidak dapat menggunakan aturan hukum dari Indonesia.
                </p><p>Kedua, para mafia migas tersebut adalah orang-orang yang memiliki koneksi kuat terhadap rezim. Besar dugaan mereka memberikan komisi terhadap oknum-oknum pemerintah untuk mengamankan bisnis mereka dari jeratan hukum. Memang, mafia migas tersebut tidak hanya ada di era kepemimpinan SBY saja. Di masa Soeharto mafia migas sudah ada, salah satu pemainnya diduga berasal dari keluarga Cendana. 
                </p><p><img src="http://assets.kompas.com/data/photo/2014/11/17/140932220141116ABN121780x390.JPG" class="pic">Kini, setelah Petral dibubarkan. Pemerintah akan mengusut dugaan penyelewengan pengadaan harga minyak yang merugikan negara tersebut dengan menjerat para pelakunya. Proses penyelidikan skandal ini diserahkan ke kepolisian dan Komisi Pemberantasan Korupsi (KPK) untuk membongkar tuntas para pemain bisnis minyak haram tersebut.',
    'PROFIL'=>[
        'narasi'=>'Petral berdiri pada 1969 dengan nama PT Petral Group. Sahamnya dipegang oleh Petra Oil Marketing  Corporation Limited perusahaan yang berkantor di Hong Kong dan Petra Oil Marketing Corporation yang berdomisili di California, Amerika Serikat (AS).
                    </p><p>Tahun 1978, berubah nama menjadi Petra Oil Marketing Limited setelah dua pemegang  saham tersebut melakukan marger. 
                    </p><p>Lalu, pada 1979-1992, perusahaan Zambesi Invesments Limited yang terdaftar di Hong Kong dan Pertamina Energy Services Pte Limited yang terdaftar di Singapura mengambil alih saham Petra  Oil Marketing Limited 
                    </p><p>Peruasahaan tersebut kemudian diakuisisi oleh PT Pertamina pada tahun 1998. Dan mengubah nama menjadi PT Pertamina Energy Trading Ltd (Petral) pada tahun 2001. Saham Petral dipegang oleh Pertamina, Zambesi Invesments Limited dan Pertamina Energy Services Pte Limited.
                    </p><p>Semua aktivitas Petral dilakukan di Singapura, dan tunduk pada hukum yang berlaku di Singapura, tempat perusahaan tersebut berdomisili. Tugas utamanya adalah sebagai trading minyak yakni membeli minyak dari Negara-negara industiri untuk kemudian dijual ke Pertamina.',
    ],
    'PROKONTRA'=>'Polemik pembubaran Pertamina Energy Trading Limited (Petral) tengah mendapat sorotan tajam. Perusahaan yang disebut-sebut sebagai sarang mafia migas tersebut mulai menyeret nama-nama yang disinyalir sebagai gembong mafia migas. 
                    </p><p><img src="http://cdn1-a.production.liputan6.static6.com/medias/701900/big/Foto.jpg" class="pic2">Menteri ESDM, Sudirman Said menyebut nama mantan presiden SBY sebagai pihak yang bertanggungjawab atas maraknya praktek korupsi minyak yang terjadi di Petral dalam 10 tahun belakangan ini. Pernyataan tersebut bukanlan tanpa dasar. Menurutnya, sejak SBY menjadi presiden pembubaran Petral karena dianggap sebagai sarang mafia migas selalu kandas di meja Presiden. 
                    </p><p>Meski tidak secara eksplisit menyebut nama dalang dari gembong mafia migas, namun pernyataan tersebut mengindikasikan bahwa SBY terkesan melindungi para mafia migas di Petral yang merajalela mengeruk untung dengan cara-cara illegal. Hal tersebut  lalu memicu polemik dan amarah dari kalangan orang-orang Cikeas. 
                    </p><p>SBY beserta krooni-kroninya bereaksi keras atas pernyataan tersebut. SBY marah besar dan merasa difitnah oleh pernyataan Sudirman Said. Dirinya merasa dideskreditkan dan  menolak tudingan bahwa dirinya tidak punya komitmen kuat dalam memberantas praktek-praktek bisnis minyak illegal di tubuh Petral yang berpotensi merugikan Negara.',
    'AKHIR'=>[
        'narasi'=>'<img src="http://cdn.sindonews.net/dyn/620/content/2015/04/22/34/992562/sudirman-serahkan-nasib-petral-ke-rini-Grt.jpg" class="pic2">Wacana membubarkan PT Pertamina Energy Trading Ltd atau Petral sudah menguat saat era kepemimpinan Presiden Susilo Bambang Yudhoyono pada 2006. Namun wacana tersebut tak kunjung terealisasikan karena kuatnya kekuatan yang membelenggu anak perusahaan PT Pertamina tersebut. 
                    </p><p class="font_kecil">Setelah wacana tersebut vakum beberpa tahun, baru pada era kepresiden Jokowi rencana tersebut benar-benar terlaksana. Tim Reformasi Tata Kelola Migas yang dibentuk oleh Pemerintah, yang bertugas untuk menyelidiki Petral (sarang mafia migas) memaparkan temuan-temuan dan memberikan rekomendasi ke pemerintah setelah melakukan audit dan sidak langsung ke kantor Petral.
                    </p><p class="font_kecil">Dan berikut beberapa alasan pemerintah akhirnya membubarkan Petral :',
        'list'=>[
            ['no'=>'Kinerja Petral yang selama ini banyak "kebocoran" disinyalir menjadi penyebab. Pembubaran merupakan langkah terbaik untuk Petral'],
            ['no'=>'Pertamina Energy Trading (Petral) Ltd banyak "bermain" di tataran ongkos angkut impor elpiji. Ini berdampak melejitnya harga elpiji dalam negeri. Tentu ini salah satu celah yang dimamfaatkan oleh para mafia untuk meraup untung.'],
            ['no'=>'Tim tata kelola dan reformasi migas menemukan indikasi kebocoran informasi spesifikasi produk dan owner estimate sebelum tender berlangsung. Klaim dari Petral yang selama mengakui sudah melakukan pengadaan minyak melalui  National Oil Companies (NOCs) dengan mengesankan mata rantai pengadaan minyak semakin pendek, ternyata tidak terbukti.'],
            ['no'=>'Tim ini juga menemukan cukup banyak indikasi adanya kekuatan tersembunyi yang terlibat dalam proses tender oleh Petral.'],
            ['no'=>'Banyak calo pengadaan minyak tumbuh subur di PT Pertamina Energy Trading (Petral).']
        ]
    ],
    'KINERJA'=>[
        'narasi'=>'Dari sisi pendapatan, kinerja Petral terus mengalami peningkatan dalam kurung waktu 5 tahun terakhir. Berikut kinerja keuangan Pertal sejak 2009 hingga 2013 :',
        'nlist1'=>'Tahun 2009 :',
        'list1'=>[
            ['no'=>'Pendapatan Usaha US$ 11,2 miliar.'],
            ['no'=>'Beban Pokok US$ 11,176 miliar.'],
            ['no'=>'Beban Usaha US$ 10 juta.'],
            ['no'=>'Laba Usaha US$ 29 juta.'],
            ['no'=>'Laba Bersih US$ 25 juta.'],
            ['no'=>'Total Aset US$ 1,306 miliar.'],
            ['no'=>'Total Kewajiban US$ 1,153 miliar.'],
            ['no'=>'Total Ekuitas US$ 153 juta.']
        ],
        'nlist2'=>'Tahun 2010 :',
        'list2'=>[
            ['no'=>'Pendapatan Usaha US$ 21,783 miliar.'],
            ['no'=>'Beban Pokok US$ 21,739 miliar.'],
            ['no'=>'Beban Usaha US$ 11 juta.'],
            ['no'=>'Laba Usaha US$ 33 juta.'],
            ['no'=>'Laba Bersih US$ 27 juta.'],
            ['no'=>'Total Aset US$ 2,315 miliar.'],
            ['no'=>'Total Kewajiban US$ 2,145 miliar.'],
            ['no'=>'Total Ekuitas US$ 170 juta.']
        ],
        'nlist3'=>'Tahun 2011 :',
        'list3'=>[
            ['no'=>'Pendapatan Usaha US$ 31,426 miliar.'],
            ['no'=>'Beban Pokok US$ 31,369 miliar.'],
            ['no'=>'Beban Usaha US$ 14 juta.'],
            ['no'=>'Laba Usaha US$ 43 juta.'],
            ['no'=>'Laba Bersih US$ 40 juta.'],
            ['no'=>'Total Aset US$ 2,745 miliar.'],
            ['no'=>'Total Kewajiban US$ 2,536 miliar.'],
            ['no'=>'Total Ekuitas US$ 209 juta.']
        ],
        'nlist4'=>'Tahun 2012 :',
        'list4'=>[
            ['no'=>'Pendapatan Usaha US$ 33,292 miliar.'],
            ['no'=>'Beban Pokok US$ 33,229 miliar.'],
            ['no'=>'Beban Usaha US$ 17 juta.'],
            ['no'=>'Laba Usaha US$ 46 juta.'],
            ['no'=>'Laba Bersih US$ 46 juta.'],
            ['no'=>'Total Aset US$ 3,557 miliar.'],
            ['no'=>'Total Kewajiban US$ 3,313 miliar.'],
            ['no'=>'Total Ekuitas US$ 244 juta.']
        ],
        'nlist5'=>'Tahun 2013 :',
        'list5'=>[
            ['no'=>'Pendapatan Usaha US$ 33,35 miliar.'],
            ['no'=>'Beban Pokok US$ 33,291 miliar.'],
            ['no'=>'Beban Usaha US$ 14 juta.'],
            ['no'=>'Laba Usaha US$ 45 juta.'],
            ['no'=>'Laba Bersih US$ 43 juta.'],
            ['no'=>'Total Aset US$ 3,418 miliar.'],
            ['no'=>'Total Kewajiban US$ 3,152 miliar.'],
            ['no'=>'Total Ekuitas US$ 266 juta.']
        ],
    ],
    'BISNIS'=>[
        'narasi'=>'<img src="http://i.ytimg.com/vi/54rokqvwGEk/0.jpg" class="pic">Tim Reformasi Tata Kelola Minyak dan Gas Bumi (Tim Antimafia Migas), memaparkan hasil dari investigasi yang baru-baru ini dilakukan seputar kepemilikan saham di Petral. Ditemukan nama-nama pemegang saham ketika Petral pertama kali dibentuk. Sejak tahun 1998 saat masih bernama Petra Oil Marketing Limited yang diakusisi oleh PT Pertamina (Persero) dan mengubah namanya menjadi PT Pertamina Energy Trading Ltd (Petral) pada tahun 2001. Saham Petral tidak seluruhnya dikuasai Pertamina. Perusahaan migas BUMN tersebut hanya memiliki saham sebesar 40 persen. Sisanya dimiliki Tommy Soeharto 20 persen, Bob Hasan 20 persen, dan sisanya Yayasan Karyawan Pertamina.
                    </p><p class="font_kecil">Salah satu kelompok broker minyak terbesar adalah kelompok Riza Cs, bahkan pengaruh Riza cs mampu membuat kelompok Cendana juga tunduk. Menurut sumber, Riza cs dekat dengan Purnomo Y dan Pramono Edhie Wibowo (adik Ny. Ani SBY) sejak Edhie masih di Kopassus. Purnomo yang Menteri ESDM & Edhie sebagai pintu masuk Riza cs ke Cikeas. Riza cs ini sering berkunjung ke Cikeas untuk mengamankan praktek mafia di impor minyak Pertamina. 
                    </p><p class="font_kecil"><img src="http://cdn1-a.production.liputan6.static6.com/medias/701900/big/Foto.jpg" class="pic2">Tentu saja tidak ada makan siang yang gratis. Selain di jajaran elit politik, Riza cs juga sangat dekat dengan Wakil Dirut Perusahaan hulu Migas dan Syamsu Alam yang General Managernya Purnomo Yusgiantoro sewaktu masih menjabat sebagai Menteri ESDM bertugas mengamankan kontrak-kontrak pembelian minyak impor dari mafia minyak ini. Dahlan Iskan yang meminta Pertamina membeli minyak secara langsung, justru ditantang oleh Direksi Pertamina, bahwa Pertamina harus membeli via broker. Bisnis migas keluarga Cendana dan keluarga Cikeas sudah bukan menjadi rahasia umum lagi, sebagai keluarga yang pernah jadi penguasa di pemerintahan, tentu mempunyai andil besar dalam bisnis ini.'
    ],
    'PENGINTAI'=>[
        'narasi'=>'<img src="http://energitoday.com/uploads//2015/05/Petral-Bubar-Pertamina-Hemat-Rp-250-Miliar-Per-Hari.jpg" class="pic">Setelah pemerintah mengumumkan pembubaran Petral (Pertamina Energy Trading Limited) pada Rabu (13/5), Pertamina berhasil menghemat Rp 250 miliar per hari. Menurut Menteri ESDM Sudirman Said, "Transaksi (impor minyak) yang beredar tiap hari sebesar US$ 150 juta atau setara Rp 1,7 triliun per hari. Setelah pembubaran (Petral), Pertamina menghemat US$ 22 juta (setara Rp 250 miliar). Hal tersebut juga senada dengan Direktur Utama Pertamina, Dwi Sutjipto, yang menargetkan US$ 400 juta atau sekitar Rp 5,2 triliun dapat dihemat karena pembubaran Petral.
                    </p><p class="font_kecil">Menariknya, pasca bubarnya Petral Presiden Jokowi telah menunjuk Sonangol EP sebagai pemasok sebagian kebutuhan minyak Indonesia, mengurangi peran Petral. Grup Sonangol adalah kongsi lama Surya Paloh. Tahun 2009, Surya Energi mendapat pinjaman modal dari China Sonangol International Holding Ltd. Anak usaha Sonangol EP tersebut menyuntikkan dana US$ 200 juta ke Surya Energi untuk menggarap Blok Cepu.
                    </p><p class="font_kecil"><img src="http://cdn.tmpo.co/data/2014/10/09/id_332459/332459_620.jpg" class="pic2">Sebagaimana yang diungkapkan oleh Fahmy Radhi, anggota Tim Reformasi Tata Kelola Minyak dan Gas Bumi (Migas), bahwa ada pihak lain yang ingin mengeruk keuntungan jika Petral dibubarkan, Fahmy menduga, perusahaan swasta yang dekat dengan Istana itu ingin menggantikan peran Petral atau menjadi pemasok BBM.
                    </p><p class="font_kecil">Banyak pihak yang merasa dirugikan atas pembubaran anak usaha Pertamina tersebut. Mafia migas selama ini dinilai sebagai biang kerok tidak efisiennya Petral sehingga negara banyak dirugikan hingga ratusan triliun rupiah. Harga bahan bakar juga semakin mahal.'
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'nasdem5119b72a0ea62']
        ],
        'institusi'=>[
            ['name'=>'BUMN','img'=>'http://us.images.detik.com/content/2014/12/16/4/bumn.jpg','url'=>'http://www.bijaks.net/aktor/profile/badanusahamiliknegarabumn51f86c5b60980'],
            ['name'=>'ESDM','img'=>'http://upload.wikimedia.org/wikipedia/id/f/fb/Logo_Kementerian_ESDM.gif','url'=>'http://www.bijaks.net/aktor/profile/kementerianesdm530d82b2c0ac2'],
            ['name'=>'Tim Reformasi Tata Niaga Minyak dan Gas','img'=>'http://1.bp.blogspot.com/-mNFRY7PWufY/VJdMcsgmNxI/AAAAAAAAB90/y__LSyat0rA/s1600/Tim%2BReformasi%2BTata%2BKelola%2BMigas.jpg','url'=>'http://www.bijaks.net/aktor/profile/timreformasitatakelolamigas554050155b047'],
            ['name'=>'Pertamina','img'=>'http://www.sindotrijaya.com/uploads/news/resize3/pertamina2.jpg','url'=>'http://www.bijaks.net/aktor/profile/pertamina52e5e96d31148'],
            ['name'=>'Komisi VI','img'=>'http://dpr.go.id/dokakd/foto/P_20150126_9590.JPG','url'=>'http://www.bijaks.net/aktor/profile/komisivi532bba9f4d3da'],
            ['name'=>'DPD RI','img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/DPD_Logo.jpg','url'=>'http://www.bijaks.net/aktor/profile/dewanperwakilandaerah531d2cecd99f7'],
            ['name'=>'Centre for Budget Analysis','img'=>'http://gdb.voanews.com/6440C6D0-961E-4D49-B324-F13287E2E03E_mw1024_s_n.jpg','url'=>''],
            ['name'=>'Energy Watch Indonesia','img'=>'http://www.energywatch.or.id/wp-content/uploads/2014/10/Logo-EWI-01-e1428499488189.jpg','url'=>'']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrat5119a5b44c7e4'],
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c']
        ],
        'institusi'=>[
            ['name'=>'Cikeas','img'=>'http://www.sesawi.net/wp-content/uploads/2011/10/sby4.jpg','url'=>''],
            ['name'=>'DPR RI','img'=>'http://batamtoday.com/media/news/logo_dpr_ri.png','url'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdpr51da66b0a7ac4'],
            ['name'=>'Komisi III','img'=>'http://www.teropongsenayan.com/img_galeri/688345BNN-Komisi%20III-ind.jpg','url'=>'http://www.bijaks.net/aktor/profile/komisiiiidprri54d30f40415f0'],
            ['name'=>'Komisi VII','img'=>'http://www.satyayudha.com/wp-content/uploads/2014/10/DSC_0238.jpg','url'=>'http://www.bijaks.net/aktor/profile/komisivii51d22e47ed22a']
        ]
    ],
    'PERKIRAAN'=>[
        'kontra'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']
        ],
        'pro'=>[
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'nasdem5119b72a0ea62']            
        ],
        'belum'=>[
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']           
        ],
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Dahlan Iskan','jabatan'=>'mantan Menteri BUMN','img'=>'http://stat.ks.kidsklik.com/statics/files/2012/07/13437127571409384459.jpg','url'=>'http://www.bijaks.net/aktor/profile/dahlaniskan503d887d648d6','content'=>'Ada yang berharap kalau Petral dibubarkan jual-beli minyak kembali dilakukan di Jakarta dan mungkin bisa menjadi obyekan baru'],
        ['from'=>'Sudirman Said','jabatan'=>'Menteri ESDM','img'=>'http://images.detik.com/content/2015/01/14/1034/sudirmanplnhuf2.jpg','url'=>'http://www.bijaks.net/aktor/profile/sudirmansaid544ca5d6974a8','content'=>'Sejak awal memang presiden memberi arahan Petral itu musti diseriusi karena memang di masa lalu reputasinya itu sarat dengan praktek yang tidak transparan begitu'],
        ['from'=>'Tanri Abeng','jabatan'=>'mantan Menteri BUMN','img'=>'http://www.indopos.co.id/wp-content/uploads/2014/12/Tanri-Abeng.jpg','url'=>'http://www.bijaks.net/aktor/profile/tanriabeng5046cd714027f','content'=>'Keputusan terakhir adalah dewan komisaris harus teken tersebut. Namun, kebiasaan saya tidak suka menunda keputusan. Jadi, pasti akan lebih cepat. Kalau ditutup atau tidak, Saya akan mempelajarinya'],
        ['from'=>'Gede Pasek','jabatan'=>'DPD RI','img'=>'http://www.teropongsenayan.com/foto_berita/201411/24/medium_87Achmad%20Hafisz%20009.jpg','url'=>'http://www.bijaks.net/aktor/profile/achmadhafisztohir51b686a4f291d','content'=>'Kasus mafia migas ini angkanya triliunan dan berlangsung sudah bertahun-tahun. Ini bisa menjadi megaskandal terbesar. Selain nilainya fantastis dan bisa terbesar sepanjang sejarah korupsi, juga akan menyeret begitu banyak gerbong. Saya yakin itu tidak mudah'],
        ['from'=>'Faisal Basri','jabatan'=>'Ketua Tim Reformasi Tata Niaga Minyak dan Gas','img'=>'http://static.republika.co.id/uploads/images/kanal_sub/faisal_basri_101126110132.jpg','url'=>'http://www.bijaks.net/aktor/profile/faisalbasri51b7f1e3d9a25','content'=>'Ingat Pak Dahlan Iskan (Mantan Menteri BUMN) mau bubarkan itu (Petral), tapi enggak bisa, dan malah dipanggil Presiden (SBY) 3 kali? Pemanggilan itu tidak membubarkan Petral'],
        ['from'=>'Fahmy Radhi','jabatan'=>'anggota Tim Reformasi Tata Kelola Minyak dan Gas Bumi (Migas)','img'=>'http://energitoday.com/uploads//2015/05/Fahmi-Radhi.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahmyradhi556453613e342','content'=>'Di awal pemerintah Pak Jokowi, kan, sudah kita ketahui, impor BBM dari Sonangol terhubung dengan perusahaan Surya Paloh (Surya Energi). Nah, soal pembubaran Petral ini, kita tunggu saja seperti apa'],
        ['from'=>'Dadan Kusdiana','jabatan'=>'Kepala Pusat Komunikasi Kementerian ESDM','img'=>'http://i.ytimg.com/vi/Ezo8LRmZbj0/sddefault.jpg','url'=>'http://www.bijaks.net/aktor/profile/dadankusdiana55644fa89909d','content'=>'Menteri ESDM (Sudirman Said) hanya berusaha menjalankan tugas dan tanggung jawabnya melakukan berbagai perbaikan dalam pengelolaan energi, termasuk sub sektor minyak dan gas'],
        ['from'=>'Wianda Pusponegoro','jabatan'=>'Vice President Corporate Communication Pertamina','img'=>'http://images.cnnindonesia.com/visual/2015/05/13/a43ad4c9-e5a2-4e2e-940c-00cd194e4e65.jpg?w=650','url'=>'http://www.bijaks.net/aktor/profile/wiandapusponegoro55644ba091454','content'=>'Saya sampaikan, dorongan (pembubaran Petral) sudah datang dari Menteri BUMN Dahlan Iskan. Tujuannya bagaimana kita melakukan fungsi pengadaan (impor BBM) secara efektif dengan tata kelola yang benar'],
        ['from'=>'Kurtubi','jabatan'=>'anggota Komisi VII DPR','img'=>'http://measiamagazine.net/wp-content/uploads/2013/10/Kurtubi-1.jpg','url'=>'http://www.bijaks.net/aktor/profile/hkurtubi5243b80d5cce5','content'=>'Sepanjang dia trader tetap kita harus bayar lebih mahal, beda dengan kita beli dari produsen langsung'],
        ['from'=>'Ahmad Hafidz Tohir','jabatan'=>'Ketua Komisi VI DPR-RI','img'=>'http://balipost.com/content/uploads/2014/01/gede-pasek-suardika1.jpg ','url'=>'http://www.bijaks.net/aktor/profile/gedepaseksuardikash50f8d7b497905','content'=>'Isunya bukan pada pembubaran Petral atau tidak. Konsentrasi kita adalah Pertamina harus melakukan efisiensi, juga transparansi dalam impor minyak tersebut'],
        ['from'=>'Jend (Purn) Wiranto','jabatan'=>'Ketum Partai Hanura','img'=>'http://static.skalanews.com/media/news/thumbs-396-263/wiranto-hanura.jpg','url'=>'http://www.bijaks.net/aktor/profile/hwirantosh50bdcb9a73d2f','content'=>'Tidak hanya Petral, semuanya yang merugikan negara dan masyarakat, ya dibubarkan saja. Untuk apa'],
        ['from'=>'Hendrawan Supratikno','jabatan'=>'Politisi PDI Perjuangan','img'=>'http://www.rmol.co/images/berita/normal/396385_03164208052015_Hendrawan-Supratikno.jpg','url'=>'http://www.bijaks.net/aktor/profile/profdrhendrawansupratikno5104f7c918e91','content'=>'Soal pembubaran Petral, PDI Perjuangan memberikan catatan kaki, apakah argumentasi pembubaran Petral sudah komprehensif karena kinerja Petral bagus'],
        ['from'=>'Said Didu','jabatan'=>'Staf Khusus Menteri Energi dan Sumber Daya Mineral (ESDM)','img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/04/24/391028/4UlOxGWJTk.jpg?w=668','url'=>'http://www.bijaks.net/aktor/profile/mohammadsaiddidu51cb9f6f9be4a','content'=>'Akhir-akhir ini kita mendengarkan niat membubarkan Petral dan kami harus memberikan apresiasi'],
        ['from'=>'Uchok Sky Khadafi','jabatan'=>'Direktur Centre for Budget Analysis','img'=>'http://media.forumkeadilan.com/2013/06/ucok-fitra-130619b-600x300.jpg','url'=>'http://www.bijaks.net/aktor/profile/uchokskykhada531fe1572d14a','content'=>'Enggak sepolos itu, dia (SBY) lebih tahu. Hanya saat ini sudah pensiun jadi presiden, mungkin sudah banyak lupanya. Coba lihat berita tertanggal 21 April 2012, ada pertemuan Dahlan Iskan dengan SBY bahas pembubaran Petral. Rakyat masih ingat, pak SBY sudah lupa kali']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Susilo Bambang Yudhiono (SBY)','jabatan'=>'Presiden RI ke- 6','img'=>'http://sandarannews.com/po-content/po-upload/presiden-sby.jpg','url'=>'http://www.bijaks.net/aktor/profile/susilobambangyudhoyono50ee672eb35c5','content'=>'Saya harap Pak Menteri ESDM melakukan klarifikasi apa yg dimaksud, karena justru saya ingin penyimpangan apapun diberantas'],
        ['from'=>'Ruhut Sitompul','jabatan'=>'Komisi III DPR RI','img'=>'http://www.jakpro.id/wp-content/uploads/2014/06/wpid1234-wpid-ruhut-sitompul.jpg','url'=>'http://www.bijaks.net/aktor/profile/ruhutpoltaksitompulsh50f91f7018b5c','content'=>'Jangan bangga membubarkan Petral, tapi nanti cuma ganticasing, cuma ganti nama. Karena kita tahu permainan di Petral. Gua heran dia (Sudirman, red) bisa terpilih jadi menteri. Kita tahu dia ini cuma bawahan dari direktur Pertamina, Pak Dwi'],
        ['from'=>'Agus Hermanto','jabatan'=>'Wakil Ketua DPR','img'=>'http://www.rmol.co/images/berita/normal/63147_02091117032015_agus-hermanto-_141114143409-850.jpg','url'=>'http://www.bijaks.net/aktor/profile/iragushermantomm5104ab0d64699','content'=>'Kita (pimpinan DPR) sudah tanda tangani untuk memanggil Sudirman Said ke DPR'],
        ['from'=>'Rachland Nashidik','jabatan'=>'Politisi Demokrat','img'=>'http://www.tubasmedia.com/wp-content/uploads/2011/03/090311-nasional3.gif','url'=>'http://www.bijaks.net/aktor/profile/rachlandnashidik5564472eddde3','content'=>'Dalam rangka pembentukan kebijakan, ia (Dahlan) juga tidak pernah menyampaikan proposal resmi kepada Presiden untuk membubarkan Petral'],
        ['from'=>'Karen Agustiawan','jabatan'=>'mantan Dirut PT Pertamina (Persero)','img'=>'http://assets.kompas.com/data/photo/2013/11/04/1202151karen-agustiawan780x390.JPG','url'=>'http://www.bijaks.net/aktor/profile/karenagustiawan514a791a769c0','content'=>'Saya tidak mau komentar apapun soal Pertamina'],
        ['from'=>'Didik Mukrianto','jabatan'=>'Sekretaris Fraksi Demokrat','img'=>'http://www.zonalima.com/images/view/1011-Didik%20Mukrianto.jpg','url'=>'http://www.bijaks.net/aktor/profile/didikmukrianto52afd982473d2','content'=>'Kita akan memohon pihak-pihak terkait untuk mengonfirmasi atas apa yang dikatakan Sudirman Said. Kita ungkap kebenaran, jangan sampai rakyat dibodohi dengan info-info yang sesat'],
        ['from'=>'Kardaya Warnika','jabatan'=>'Ketua Komisi VII DPR','img'=>'http://www.migasreview.com/upload/i/c%7Bca%7DXU6H0951%7Bca%7D2015-03-04%7Bca%7D05-43-21%7Bca%7D1418301839%7Bca%7Dh_thumb_b.JPG','url'=>'http://www.bijaks.net/aktor/profile/ferryjokoyuliantono52a02f60c71da','content'=>'Menteri ESDM sudah dua kali enggak datang. Pertama sebelum reses, kedua batal datang hari ini. Kalau tiga kali enggak datang kami bisa minta polisi jemput paksa'],
        ['from'=>'Satya W Yudha','jabatan'=>'Wakil Ketua Komisi VII DPR','img'=>'http://www.satyayudha.com/wp-content/uploads/2014/11/142003_303143_satya_golkar.jpg','url'=>'http://www.bijaks.net/aktor/profile/satyawirayuda534f67d842ca1','content'=>'Kami memanggil Menteri ESDM bukan karena pembubaran Petral. Ada pembahasan yang lebih besar lagi soal anggaran. Tapi tidak menutup kemungkinan ada anggota Komisi VII yang bertanya soal Petral'],
    ],
    'VIDEO'=>[
        ['id'=>'cFuqGuVgUU8'],
        ['id'=>'DrhbzapWZI8'],
        ['id'=>'_kzx6XGC4AM'],
        ['id'=>'6s9mIoODSXo'],
        ['id'=>'iE0_SoeoRfY'],
        ['id'=>'msld1cbnuZA'],
        ['id'=>'_EfKmkVLXww'],
        ['id'=>'9tFth7ZjCOo'],
        ['id'=>'z-5fxaI8ajY'],
        ['id'=>'lvyUZmfSmLs'],
        ['id'=>'SZnNzKT7ru0'],
        ['id'=>'C_AqWIQoId8'],
        ['id'=>'7zchnUTXB_I'],
        ['id'=>'g0-gNDqrWiY'],
        ['id'=>'Dp3Wah7U57Y'],
        ['id'=>'D_foF02lA90']
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }    
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/petral/top_kcl.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ANALISA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['ANALISA'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PRO-KONTRA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROKONTRA'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">AKHIR DARI KEJAYAAN PETRAL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['AKHIR']['narasi'];?></p>
                <ol style="list-style-type: square;margin-left: -30px !important;">
                    <?php
                    foreach ($data['AKHIR']['list'] as $key => $val) {
                        echo "<li style='margin-left: 20px;' class='font_kecil'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PROFIL PETRAL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://www.iberita.com/wp-content/uploads/2015/05/Petral.jpg" class="col-xs-12 col-sm-6" style="width: 100%;margin-bottom: 10px;">
                <p style="text-align: justify;"><?php echo $data['PROFIL']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KINERJA KEUANGAN PETRAL SEJAK 2009 HINGGA 2015</span></h5>
        <p style="text-align: justify;"><?php echo $data['KINERJA']['narasi'];?></p>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php 
                for($i=1;$i<=5;$i++){
                ?>
                <p style="text-align: justify;"><?php echo $data['KINERJA']['nlist'.$i];?></p>
                <ol style="list-style-type: square;margin-left: -30px !important;">
                    <?php
                    foreach ($data['KINERJA']['list'.$i] as $key => $val) {
                        echo "<li style='margin-left: 20px;' class='font_kecil'>".$val['no']."</li>";
                    }
                    ?>
                </ol>                
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BISNIS CENDANA DAN CIKEAS DI PETRAL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['BISNIS']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PENGINTAI DI BALIK BUBARNYA PETRAL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PENGINTAI']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 style="padding-top: 15px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>