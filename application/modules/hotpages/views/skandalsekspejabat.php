<?php
$data = [
    'block1'=>[
        'title'=>'Artis PSK Simpanan',
        'narasi'=>'
            <img src="http://i.ytimg.com/vi/TpgeJ5exKFA/hqdefault.jpg" class="pic">
            <p>Maraknya skandal seks pejabat dengan artis merupakan fakta riil dan sering terjadi di dalam lingkaran kehidupan para pejabat. Kasus-kasus yang selama ini terekspos media masih sebagian kecil yang nampak ke permukaan. Seperti fenomena gunung es, beragam moduspun dilakakukan. Mulai dari memelihara artis cantik sebagai istri simpanan hingga melakukan pelesiran ke tempat tertentu yang memberikan pelayanan esek-esek artis PSK atau memesan langsung melalui mucikari seperti kasus prostitusi online yang beberapa waktu lalu berhasil dibongkar.</p>
            <img src="http://cdn.tmpo.co/data/2011/04/11/id_71376/71376_620.jpg" class="pic2">
            <p>Skandal seks yang melibatkan para pejabat dengan artis lalu menjadi polemik. Para pejabat diduga sengaja memanfaatkan posisinya sebagai pejabat publik untuk menggaet artis, di lain sisi, para artis yang menjajajakan tubuhnya memang mengincar para pejabat sebagai pelanggangnya karena mereka umumnya berkantong tebal. </p>
        '
    ],

    'block2'=>[
        'title'=>'Artis Selingkuh dengan Pejabat',
        'narasi'=>'
            <img src="http://4.bp.blogspot.com/-tVO-CHEj9pc/VXeIQsVwD3I/AAAAAAAAC4o/sFFZmXcvtm0/s640/pelanggan%2Bpsk%2Bartis.jpg" class="pic">
            <p>Kehidupan gemerlap dan mewah yang melekat dari seorang artis membuat para artis mengejar popularitas dan ketenaran. Namun, tidak semua artis berhasil untuk menjadi terkenal dan mendapatkan gemerlap kemewahan. Beberapa dari artis yang tidak berhasil mendapatkannya mencari cara yang instant untuk mendapatkannya, salah satunya memanfaatkan kedekatan mereka dengan beberapa pejabat untuk mendapatkan kemewahan. </p>
            <p>Ada yang rela menjadi istri kedua dari seorang pejabat ataupun menikah secara sirih karena pejabat yang dinikahinya secara sirih tidak ingin orang banyak tahu mengenai pernikahan mereka khususnya dari keluarga dari pejabat tersebut. </p>
            <img src="http://pojoksatu.id/wp-content/uploads/2015/01/1-psk.jpg" class="pic2">
            <p>Tidak hanya itu, bahkan beberapa artis rela "menjual tubuhnya" yaitu menjadi seorang Pekerja seks komersial (PSK). Mereka mempromosikan tubuhnya dan popularitasnya melalui social media dan juga diperantarai oleh seorang murcikari. Artis prostitusi rela "menjual tubuhnya", bahkan dengan harga yang fantastis yaitu puluhan atau sampai ratusan juta rupiah.</p>
            <p>Semua itu dilakukan, hanya untuk mempertahankan lifestyle dan gaya hidup glamour seorang artis yang bahkan tidak sesuai dengan kapasitas keuangan mereka sehingga "lebih besar pasak daripada tiang".</p>
        '
    ],

    'block3'=>[
        'title'=>'Skandal Seks Pejabat Yang Menggemparkan Publik',
        'narasi'=>'
            <p>Ada beberapa kasus skandal seks pejabat yg menggemparkan publik:</p>
            <ol>
                <img src="http://eddymesakh.files.wordpress.com/2008/07/al-amin-azirwan2.gif" class="pic">
                <li>Video Porno Yahya Zaini dan Maria Skandal seks yang terjadi pada Desember 2006 lalu, yaitu anggota DPR dari Partai Golkar Yahya Zaini ketahuan berbuat mesum dengan pedangdut Maria Eva.</li>
                <li>Foto Mesum dan Pelecehan Seksual Max Moein dan Desi Firdiyanti. . Badan Kehormatan (BK) akhirnya memecat Max Moein dari keanggotaan di DPR atas alasan pelanggaran etika.</li>
                <li>Anggota DPR Al Amin Nasution menerima suap plus bonus wanita bayaran.</li>
                <img src="http://harianandalas.com/images/wanda/2015/oktober/Arzetti-Bilbina.jpg" class="pic2">
                <li>Isu skandal Seks bupati dan wakil bupati Pekalongan Siti Qomariyah dan Wahyudi Pontjo.</li>
                <li>Anggota DPRD M. Hasan Ahmad Alias Ihsan (44).dengan anak dibawah umur bayar 2,5 juta.</li>
                <li>Ahmad fathana dengan dengan mahasiswi cantik maharani.
                <li>Antasari Azhar dengan Rani Juliani.</li>
                <li>Anggota DPR  Arzeti  Bilbina selingkuh dengan Dandim Sidoarjo.</li>
            </ol>
        '
    ],

    'block4'=>[
        'title'=>'Pro Kontra',
        'narasi'=>'
            <img src="https://i.ytimg.com/vi/G1Wu4oACj4g/hqdefault.jpg" class="pic">
            <p>Selain dituntut untuk bersih dari tindakan korupsi, kolusi dan nepotisme, seorang pemimpin juga harus bersih dari pelanggaran dan harus mempertanggungjawabkan segala perilaku moralnya. Pasalnya, setiap tindakan atau perilaku seorang pejabat senantiasa dikontrol oleh masyarakat. Dan jika seorang pejabat tersandung kasus negatif, maka dengan cepat menjadi buah bibir di masyarakat.  Hal ini karena adanya sebuah tren positif di masyarakat dengan bersikap kritis terhadap keberadaan pejabat atau calon pejabat.</p>
            <img src="http://cdn-media.viva.id/vthumbs2/2015/06/05/50632_siapa-pejabat-penikmat-syahwat--bagian-5--terakhir_641_452.jpg" class="pic2">
            <p>Tetapi beberapa kalangan juga berpendapat bahwa mempersoalkan moralitas pejabat tinggi dianggap melanggar etika lantaran pejabat yang tesandung skandal seks  dengan perempuan lain merupakan masalah pribadi yang tidak perlu dipersoalkan dan sebaiknya tidak perlu diungkap di depan umum. Beberapa kasus skandal seks yang melibatkan pejabat  tidak hanya hanya persoalan moralitas tetapi kerap kali bersangkutpautan dengan persoalan hukum, yakni pejabat menggunakan uang hasil korupsi untuk bersenang-senang. Selingkuh dengan perempuan lain atau menyewa wanita bayaran menjadi salah satu motif pencucian uang</p>
       '
    ],

    'block5'=>[
        'title'=>'Anggota DPRD Jawa Timur Borong  Artis PSK',
        'narasi'=>'
            <img src="http://www.riaumerdeka.com/foto_berita/173%20artis.jpg" class="pic">
            <p>Pria bernama Budi Kusuma yang mengaku sebagai anggota DPRD mengaku bahwa dirinya pernah menyewa tiga artis Ibukota, yaitu Amel Alvi, Tyas Mirasih, dan Shinta Bachir.</p>
            <p>Fakta tersebut diungkapkan oleh Majelis Hakim Effendi Mukhtar saat membacakan fakta di persidangan di ruang sidang 7, Pengadilan Negeri Jakarta Selatan. Ketiga artis tersebut merupakan "anak asuh" dari Robby Abbas. Disebutkan bahwa mereka bertiga telah disewakan sebesar puluhan juta rupiah</p>
            <img src="http://images.solopos.com/2015/06/Foto-yang-diklaim-sebagai-Bella-Shofie-bersama-Robbie-Abbas-RA.-Instagram.jpg" class="pic2">
            <p>Disebutkan bahwa Amel Alvi telah melakukan tiga kali transaksi sebesar yaitu 15 juta di Surabaya dan Hotel Pan Pasifik sebesar Pan Pasific, Jakarta pada tahun 2014. Terakhir di Hotel Pasific Place dengan transaksi sebesar 20 Juta Rupiah.</p>
            <p>Begitupun dengan Tyas Mirasih yang pernah disewa sebesar 20 juta, sementara artis Shinta Bachir juga pernah disewa pejabat, namun tak menyebutkan berapa jumlah sewanya.</p>
        '
    ],

    'block6'=>[
        'title'=>'Anggota DPR Doyan Jajan Artis PSK',
        'narasi'=>'
            <img src="https://anekainfounik.files.wordpress.com/2015/09/pengacara-robbi-abbas-pieter-ell-tunjukkan-bap-ke-wartawan.jpg" class="pic">
            <p>Pieter Ell, pengacara dari Robby Abbas sang murcikari artis bertarif fantastis, pada selasa, 9 Juni 2016 melakukan wawancara pers. Kemudian dalam wawancara tersebut ia mengungkapkan sesuatu yang menghebohkan bahwa ia akan mengungkapkan semua anggota DPR yang pernah memesan artis kepada kliennya.</p>
            <img src="http://www.sebatasberita.com/wp-content/uploads/2015/10/Budi-Kusuma-Anggota-DPR-Borong-Artis-Amel-Alvi-Tyas-Mirasih-dan-Shinta-Bachir.jpg" class="pic2">
            <p>Tinggal cari saja, yang kebakaran jenggot berarti dia merasa. Kata Pieter saat dicerca pertanyaan mengenai siapa saja anggota DPR yang memesan artis dari kliennya Pieter dalam wawancaranya juga akan membuka semua nama artis yang ada dalam daftar kliennnya kemudian ia buka kembali kalangan parlemen.</p>
            <p>"Jangan dibuka sekarang. Kami akan buka semuanya di pengadilan baik artis maupun pelanggannya, biar dunia tahu," tegasnya.</p>
        '
    ],

    'block7'=>[
        'title'=>'Wakil Ketua DPR Tantang Buka Aib Anggota DPR',
        'narasi'=>'
            <img src="http://m.bijaks.net/assets/images/hotpages/skandalsekspejabat/fadli-zon.jpg" class="picprofil" style="width:100%;">
            <p class="rightcol">Wakil ketua DPR- RI, Fadli Zon menyatakan bahwa ia meminta RA dan pengacaranya untuk mengungkapkan siapa saja anggota DPR yang telah menggunakan jasanya. </p>
            <p class="rightcol">Sikap pengacara RA yang mengungkapkan bahwa ia akan membuka anggota-anggota DPR tanpa menyebutkan nama dalam konferensi persnya itu hanya menimbulkan spekulasi.</p>
            <p class="rightcol">"Tidak usah berspekulasi terkait masalah ini. Yang bersangkutan laporkan saja namanya ke MKD (Mahkamah Kehormatan Dewan)." Kata Fadli di Kompleks Parlemen, Senayan, Rabu, (3/6/2015)</p>
            <img src="http://www.bijaks.net/uploads/2015/08/psk-379x250.gif" class="picprofil" style="width:100%;">
            <p class="rightcol">Fadli menilai bahwa ketidakjelasan pengacara RA apakah yang dimaksudkan itu parlemen pusat atau daerah. Pengacara RA hanya menyebut parlemen tanpa menjelaskan lebih lanjut.</p>
        '
    ],

    'block8'=>[
        'title'=>'Pasal Pidana Bagi Pejabat Pelanggar Etika',
        'narasi'=>'
            <p class="rightcol">Dalam kasus perselingkuhan pejabat, belum ada standar hukuman  yang tegas. Selama ini kasus-kasus skandal selingkuh atau seks yang melibatkan pejabat hanya dinilai sebagai pelanggaran etika dan sumpah jabatan dan hanya diberikan sanksi skorsing ataupun dipecat. Sanksi tersebut belum bisa menjadi cara penghakiman yang efektif sehingga perilaku semacamnya masih terus terjadi. </p>
        '
    ],

    'institusiPendukung' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdpr51da66b0a7ac4','image'=>'http://uc.blogdetik.com/791/79197/files/2014/11/ed46b4cf066c6d1367af9a276daf661a_logo-dpr-ri-250x239.png','title'=>'DPR RI'],
        ['link'=>'#','image'=>'http://img.lensaindonesia.com/uploads/1/2014/09/89382-gedung-dprd-jatim-tambah-staf-ahli-bakal-ajukan-dana-rp-3-miliar.jpg','title'=>'DPRD JATIM'],
        ['link'=>'http://www.bijaks.net/aktor/profile/tentaranasionalindonesiatni531c8511eca9f','image'=>'http://www.sinarpaginews.com/bk27panel/modul/images_content/logo_tni_JPG-5.jpg','title'=>'TNI'],
    ],

//    'institusiPenentang' => [
//        ['link'=>'','','title'=>''],
//    ],

//    'partaiPendukung' => [
//        ['link'=>'','','title'=>''],
//    ],

     'partaiPenentang' => [
         ['link'=>'http://www.bijaks.net/aktor/profile/partaigerakanindonesiarayagerindra5119a4028d14c','image'=>'https://pbs.twimg.com/profile_images/3195156458/2db2524b01bb2a019d3ac621e063ff94.jpeg','title'=>'Gerindra'],
     ],

    'quotePendukung'=>[
        ['from'=>'Robby Abbas','jabatan'=>'Mucikari','img'=>'https://anekainfounik.files.wordpress.com/2015/05/robby-abbas-mucikari-yang-menawarkan-psk-artis-dan-gigolo.jpg','url'=>'http://www.bijaks.net/aktor/profile/robbyabbas563aa1e0031b1','content'=>'"Sesuai request klien, permintaan perempuan, itu tamu klien yang minta. Saya tak ada katalog foto, semua dilakukan melalui telepon."'],
        ['from'=>'Bella Sophie','jabatan'=>'Artis','img'=>'http://news.aditw.com/wp-content/uploads/2015/05/bella-sophie.jpg','url'=>'http://www.bijaks.net/aktor/profile/bellasofhie55545484ea9ad','content'=>'"Kalau dulu aku kan masih seorang diri, sekarang sudah berdua, beda pastinya. Aku merasa lebih baik dan lebih berkah dengan status baru ini."'],
        ['from'=>'Rhoma Irama','jabatan'=>'Artis','img'=>'http://sidomi.com/wp-content/uploads/2011/11/Rhoma-Irama.jpg','url'=>'http://www.bijaks.net/aktor/profile/radenomairama50e3b18fd1944','content'=>'"Saya memang sudah menikah secara siri dengan Angel Lelga. Kapan tepatnya, saya rasa tidak perlu diberitahukan. Sekaligus saya ingin mengumumkan saya sudah menceraikan wanita yang bernama asli Lely Anggraeni."'],
        ['from'=>'Sinta Bachir','jabatan'=>'Artis Simpanan','img'=>'http://sidomi.com/wp-content/uploads/2015/06/Shinta-Bachir6.jpg','url'=>'http://www.bijaks.net/aktor/profile/radenomairama50e3b18fd1944','content'=>'"Ya nggak munafik, saya pernah pacaran, pernah disimpan (pejabat kepolisian), dan menikah siri. Itu lebih baik, daripada jadi jajanan. (Menikah) siri halal kok, mendingan jadi simpanan daripada jajanan."'],
        ['from'=>'Pieter Ell','jabatan'=>'Kuasa Hukum Robby Abbas','img'=>'http://sidomi.com/wp-content/uploads/2015/06/Shinta-Bachir6.jpg','url'=>'#','content'=>'"Ini nama artis yang seharusnya dihadirkan oleh Jaksa dalam sidang Obie. Ini berasal dari Berkas Perkara dari pihak Kepolisian Republik Indonesia loh, sah."'],
    ],

    'quotePenentang'=>[
        ['from'=>'Lukman Hakim Saifuddin','jabatan'=>'Menteri Agama RI','img'=>'http://www.sayangi.com/media/k2/items/cache/a46a7dc19c2c87928527e1cfd49d9012_XL.jpg','url'=>'http://www.bijaks.net/aktor/profile/drshlukmanhakimsaifuddin511756084eb20','content'=>'"Nikah siri tidak dapat dipertanggungjawabkan, apalagi nikah siri secara online. Itu akan merugikan perempuan, Kalaupun pasangan nikah siri bercerai dan memperebutkan harta dari pernikahan maka negara tidak bisa bersikap. Karena (pernikahannya) tidak tercatat."'],
        ['from'=>'Effendi Muhktar','jabatan'=>'Hakim Pengadilan Negeri Jakarta Selatan','img'=>'http://pn-sambas.go.id/v2/images/Foto/Profil_Hakim/EFFENDI%20MUKHTAR.jpg','url'=>'#','content'=>'"Amelia Alviani telah mengaku tiga melakukan transaksi, satu kali di Surabaya pada 2014 dengan biaya transaksi Rp 15 juta, satu kali di Hotel Pan Pasific pada 2014 dengan biaya transaksi Rp 15 juta, dan terakhir pada 2015 di Pasific Place dengan biaya Rp 20 juta."'],
        ['from'=>'Fadli Zon','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://rmol.co/images/berita/normal/23875_10161712082015_ade.jpg','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007','content'=>'"Ngapain berspekulasi begitu, kalau punya fakta, sebut saja."'],
        ['from'=>'Taufik Kurniawan','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://www.rmol.co/images/berita/normal/216314_03100616092015_taufik_kurniawan.jpg','url'=>'http://www.bijaks.net/aktor/profile/irtaufikkurniawan50c7e1f8cb585','content'=>'"Kalau ada oknum anggota DPR RI terlibat, silakan diusut tuntas."'],
        ['from'=>'Sinta Nuriyah Wahid','jabatan'=>'Istri Gusdur','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_HT25_jlTiY-7U11eK0siD85_WDxmyhve7zEKgKbVwXAAasOytg','url'=>'http://www.bijaks.net/aktor/profile/sintanuriyahwahid5535efc286c80','content'=>'"Apabila undang-undang mengatakan ibu Halimah menang. Maka, perkawinan yang dilakukan sebelum-sebelumnya (antara Bambang dan Mayang) menjadi tidak sah."'],
        ['from'=>'Maia Estianty','jabatan'=>'Artis','img'=>'http://cdn1-a.production.liputan6.static6.com/medias/851492/big/b3ebdad423a3f7722f57a22bdd029cd8ti_1_-_Galih.jpg','url'=>'http://www.bijaks.net/aktor/profile/maiaestiantiy555acea4b73d2','content'=>'"Kasus ini mencoreng nama baik artis, padahal tidak semua artis punya kelakuan buruk. Tapi ini seakan-akan semua artis begitu, Kemungkinan karena gaya hidup tinggi, job lagi sepi, dan dia ingin jalan pintas."'],
        ['from'=>'Farhat Abbas','jabatan'=>'Pengacara Kontroversi','img'=>'http://www.wowkeren.com/images/photo/farhat_abbas.jpg','url'=>'http://www.bijaks.net/aktor/profile/farhatabbas51cce8399d1cd','content'=>'"Harusnya penangkapan itu diserahkan ke FPI saja, bersihkan indonesia dari segala bentuk maksiat!"'],
    ],

    'video'=>[
        ['id'=>'5TgM6_nDV-8'],
        ['id'=>'U7E2I62LoKI'],
        ['id'=>'jlE8mK1TRUM'],
        ['id'=>'YuKEyPvDLZA'],
        ['id'=>'rhxXy8y1yp8'],
        ['id'=>'D2fRXez0KgQ'],
        ['id'=>'DGj3-mSJ708'],
        ['id'=>'Gat5ayzrMug'],
        ['id'=>'F0kmlHW3GWc'],
        ['id'=>'J1UFI5nAtcc'],
        ['id'=>'1dv9onOfRZk'],
        ['id'=>'z3oE4wo9d3s'],
        ['id'=>'JjCck_W4Es4'],
        ['id'=>'0BFacHQ16rE'],
        ['id'=>'XYvQUcQ1xEU'],
        ['id'=>'uarRnZlCL3U'],
        ['id'=>'_qxfUFyKtbY'],
        ['id'=>'KK15ulQryjM'],
    ],

    'foto'=>[
        ['img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20141212_101217_nikita%20mirzani.jpg'],
        ['img'=>'https://upload.wikimedia.org/wikipedia/id/thumb/f/f2/Halimah_Agustina_Kamil.jpg/220px-Halimah_Agustina_Kamil.jpg'],
        ['img'=>'http://www.wowkeren.com/images/events/ori/2014/04/17/machica-mochtar-sidang-cerai-nia-daniati-05.jpg'],
        ['img'=>'http://static.inilah.com/data/berita/spotlite/2195467-1.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2013/04/21/id_178814/178814_620.jpg'],
        ['img'=>'http://3.bp.blogspot.com/-7E0giaa7xcg/U8B7w8WjoCI/AAAAAAAAALU/-cSZrKJjB4I/s1600/Tyas+Mirasih+1.png'],
        ['img'=>'http://www.21cineplex.com/data/gallery/pictures/140790194696932_430x625.jpg'],
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-gZKzL8fv7LjR5Q07uZuxkh9ZSnB72tQNAYrxwXUND_0WVXXw'],
        ['img'=>'http://cdn-2.tstatic.net/sumsel/foto/bank/images/shinta-bachir-kuning.jpg'],
        ['img'=>'http://www.wowkeren.com/images/photo/amel_alvi.jpg'],
        ['img'=>'http://www.wowkeren.com/images/events/thumb/2015/02/19/roro-fitria-press-screening-2014-02.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/makassar/foto/bank/images/roro-fitria_20150513_114021.jpg'],
        ['img'=>'http://www.emkbh.com/images/celebrities/Maia-Estianty.jpg'],
        ['img'=>'http://riaukepri.com/foto_berita/18Kartika%20Putri.jpg'],
        ['img'=>'http://4.bp.blogspot.com/-SI43xZL6mM4/Vdxwu7As-dI/AAAAAAAARxc/jbUsF5DfSSQ/s1600/toket%2Bmontok%2Bnikita%2Bmirzani.jpg'],
        ['img'=>'http://www.cumicumi.com/uploads/public/555/36b/cde/55536bcde6f75262638251.jpg'],
        ['img'=>'http://us.images.detik.com/content/2015/05/12/230/071936_amecov.jpg'],
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSBIaLZyZkdoLPtYE9YkbzJGAD8EJr4QVx91q7mHeFw7PNdxa4AMQ'],
        ['img'=>'http://ngokos.com/wp-content/uploads/2015/09/Scranshot-percakapan-Tiyas.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/05/10/124399/zJO2cy5Cdc.jpg?w=668'],
        ['img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQ83edNEIMOLIa7OMGArQFbGMzNo8t-B0SreF-eg2FEt8P4nT-UfA'],
        ['img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTTc1bnVVQcT6imnnu5lnHs6ZVVVpo4Ry3gByC-SfgXrKjXuIbQ'],
        ['img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQbJQWEtcJ4_mjAyGo9Tx-6jAseCtpy8xHzUYaPW0g8bzz9UooW'],
        ['img'=>'http://i.ytimg.com/vi/ZYBMd3yhtlw/hqdefault.jpg'],
        ['img'=>'http://www.rmoljakarta.com/images/berita/normal/599848_09543526102015_pengacara_robby_abbas.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/10/30/338/1240723/siapa-klien-pertama-robby-abbas-sang-mucikari-artis-GAtNz86aSU.jpg'],
    ],

//    'BERITA'=>[
//        ['img'=>'','shortText'=>'','link'=>''],
//    ],

    'kronologi'=>[
        'list'=>[
            ['date'=>'1 Oktober 2015','content'=>'
                <p>Artis Amel Alvi berhasil didatangkan dalam persidangan. Dalam persidangan Amel yang biasanya berpenampilan seksi, pada sidang kali ini memakai baju gamis hitam dengan muka ditutup oleh cadar. Dalam persidangan tersebut Amel Alvi dicecar banyak pertanyaan mengenai fakta di lapangan mengenai kebenaran atas warna coklat beserta uang sebesar 45 Juta Rupiah.</p>
            '],
            ['date'=>'25 Oct 2015','content'=>'
                <p>Arzeti dan Dandim Sidoharjo Digerebek di Kamar Hotel. Kepala Dinas Penerangan TNI Angkatan Darat Brigadir Jenderal M. Sabrar Fadhilah membenarkan penggerebekan terhadap Komandan Komando Distrik Militer Sidoarjo Letnan Kolonel Rizky Indra Wijaya dengan Arzetti Bilbina.</p>
            '],
            ['date'=>'28 Oct 2015','content'=>'
                <p>Tersiar kabar Penemuan Bercak Sperma saat Penggerebekan; dengan bukti adanya tisu dengan bercak sperma saat penggerebekan Anggota DPR Arzetti Bilbina bersama Komandan Komando Distrik Militer 0816 Sidoarjo Letkol Kav Risky di sebuah kamar hotel.</p>
            '],
        ]
    ],

]

?>

<style>
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/skandalsekspejabat/top.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/skandalsekspejabat/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['block1']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block2']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block2']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block3']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block3']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block4']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block4']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block5']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block5']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block6']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block6']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block7']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block7']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block8']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block8']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>

       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['foto'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['video'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
<?php /*
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['berita'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>
*/ ?>
        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>