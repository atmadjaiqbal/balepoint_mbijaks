<?php 
$data = [
    'NARASI'=>[
        'title'=>'PSSI DIAMBANG KEHANCURAN',
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2015/04/20/125929320150419AGS11780x390.JPG" class="pic">Organisasi induk sepakbola Indonesia, Persatuan Sepakbola Seluruh Indonesia (PSSI) resmi dibekukan oleh Kemenpora. Langkah tegas tersebut diambil setelah surat teguran , dengan SP 3 yang dilayangkan ke PSSI terkait  keikutsertaan Arema Cronus dan Persebaya dalam kompetisi liga Indonesia tidak digubris. Dua Klub asal jawa timur tersebut dinyatakaan tidak lolos persyaratan peserta kompetisi oleh Badan Olahraga Profesional Indonesia (BOPI) karena masalah legalitas dan dualisme kepemilikan klub.
                    </p><p><img src="http://m.bijaks.net/assets/images/hotpages/pssi/img1.jpg" class="pic2">Keputusan Kemenpora tersebut menyulut  protes dari  La Nyalla Mahmud Mattalitti yang terpilih menjadi Ketua PSSI hasil Kongres Luar Biasa Surabaya. PSSI dibawah kepemimpinannya  tak menggubris surat pembekuan yang dilayangkan Kemenpora. Ia mengatakan bahwa yang berhak membekukan PSSI adalah FIFA. Hal ini menjadi isyarat dari kubu PSSI untuk melawan dan menyatakan perang terbuka terhadap kebijakan Kemenpora.'
    ],
    'PROFIL'=>[
        'title'=>'PERSATUAN SEPAKBOLA SELURUH INDONESIA',
        'narasi'=>'<img src="http://m.bijaks.net/assets/images/hotpages/pssi/logo.png" class="pic" style="margin-bottom: 10px;">Persatuan Sepakbola Seluruh Indonesia (PSSI)adalah organisasi  induk yang bertugas mengatur kegiatan olahragasepak bola di Indonesia. PSSI berdiri pada tanggal 19 April 1930 dengan nama awal Persatuan Sepak Raga Seluruh Indonesia.PSSI bergabung dengan FIFA pada tahun 1952, kemudian dengan AFC pada tahun 1954. PSSI menggelar kompetisi Liga Indonesia setiap tahunnya, dan sejak tahun 2005, diadakan pula Piala Indonesia'
    ],
    'POLEMIK'=>[
        'narasi'=>'<img src="http://stat.ks.kidsklik.com/statics/files/2012/12/13543764041916856136.jpg" class="pic">Langkah Kemenpora membekukan PSSI akan berdampak sistemik, tidak hanya akan merugikan klub-klub secara finasial. Konsekuensi terburuk adalah apabila FIFA memberikan sanksi. Timnas terancam tidak bisa bermain di kompetisi naungan FIFA, sponsor dan dana dari FIFA juga akan dicabut, selain itu pemain Indonesia yang sedang meniti karir di luar negeri akan dipulangkan karena statusnya menjadi tidak jelas. 
                    </p><p class="font_kecil">Namun beberapa kalangan menganggap bahwa langkah pembekuan tersebut sebagai upaya untuk memperbaiki kualitas persepakbolaan di tanah air. Selama ini memang PSSI dianggap yang paling bertanggungjawab atas semakin merosotnya prestasi Timnas di kancah internasional. PSSI tidak sepenuh hati memperbaiki kualitas liga dan pembinaan pemain sehingga berdampak pada kualitas Timnas. Hal ini memicu pro dan kontar di masyarakat, khususnya para pecinta sepakbola
                    </p><p class="font_kecil"><img src="http://reportaseindonesia.com/foto_berita/55menpora.jpg" class="pic2">Kemenpora membentuk Tim Sembilan sebagai upaya agar kompetisi dan aktifitas sepakbola tidak vakum. Tim tersebut mengambil alih peran dan kewenangan PSSI untuk sementara waktu sambil menunggu hasil kongres baru yang kemungkinan akan digelar untuk membentuk kepengurusan baru PSSI. 
                    </p><p class="font_kecil">Namun, kebijakan tersebut dinilai oleh PSSI sebagai langkah yang liar dan ceroboh. Bagaimanapun kepengurusan PSSI hasil KLB Surabaya tetap sah karena sudah mendapat pengakuan dari FIFA dan AFC, kata La Nyalla Mahmud Mattalitti. Terkait adanya Tim 9 bentukan Kemenpora yang tidak mengakui semua kepengurusan PSSI hasil KLB Surabaya direspon dengan membentuk Tim Hukum yang akan melayangkan gugatan ke PTUN. Langkah hukum ditempuh untuk membentengi diri sekaligus mempertahankan status quo.'
    ],
    'ULTIMATUM'=>[
        'narasi'=>'Organisasi sepakbola dunia FIFA berekasi atas kisruh yang terjadi antara kemenpora dan PSSI. FIFA mengeluarkan ultimatum terhadap Kemenpora untuk tidak  ikut campur mengurusi persoalan yang terjadi di liga Indonesia.
                    </p><p class="font_kecil"><img src="http://cdn-media.viva.id/thumbs2/2015/04/13/307169_logo-di-markas-fifa_663_382.jpg" class="pic">Dalam surat yang dikirimkan ke Menteri Imam Nahrawi, FIFA mengultimatum bahwa PSSI harus mengelola urusan mereka secara independen dan tanpa pengaruh dari pihak ketiga, seperti diatur dalam Pasal 13 dan 17 Statuta FIFA.
                    </p><p class="font_kecil">FIFA juga mengingatkan bahwa hanya anggota FIFA  yang bisa memberi lisensi dan bertanggung jawab mengatur dan memaksakan kriteria yang harus dipenuhi klub yang berpartisipasi sesuai yang tercantum dalam poin 2 dan 3 di Peraturan Perizinan Klub FIFA.
                    </p><p class="font_kecil"><img src="https://aws-dist.brta.in/2012-07/e3025e377013c0fb87f166e45386e93e.jpg" class="pic2">Selanjutnya, FIFA menegaskan bahwa Pemerintah tidak memiliki kewenangan untuk mengintervensi badan sepokbola dengan memaksakan kriteria terhadap klub-klub yang akan mengikuti kompetisi liga domesti. FIFA mengancam akan memberikan sanksi yang berat berupa terhadap PSSI apabila persoalan tersebut tidak segera diselesaikan dan mengembalikan kewenangan PSSI dalam mengelola kompetisi sesuai dengan ketentuan yang berlaku.'
    ],
    'KONTROVERSI'=>[
        ['title'=>'Kasus korupsi Nurdin Halid','no'=>'Pada 13 Agustus 2007, Ketua Umum Nurdin Halid divonis dua tahun penjara akibat tindak pidana korupsi dalam pengadaan minyak goreng. Nurdin Halid kemudian dipenjara ats kasus tersebut, namun posisi sebagai ketua umum tetap ia pegang, dan tetap menjalankan kepemimpinan PSSI dari dalam penjara.'],
        ['title'=>'Reaksi atas LPI','no'=>'Pada Oktober 2010, Liga Primer Indonesia dideklarasikan di Semarang oleh Konsorsium dan 17 perwakilan klub. Namun LPI akhirnya mendapatkan izin dari pemerintah melalui Menteri Pemuda dan Olahraga Andi Mallarangeng'],
        ['title'=>'Kisruh dan Pembentukan komite Normalisasi','no'=>'Kisruh di PSSI semakin menjadi-jadi semenjak munculnya LPI. Ketua Umum Nurdin Halid melarang segala aktivitas yang dilakukan oleh LPI.  Pada 1 April 2011, Komite Darurat FIFA memutuskan untuk membentuk Komite Normalisasi yang akan mengambil alih kepemimpinan PSSI. FIFA juga menyatakan bahwa 4 orang calon Ketua Umum PSSI yaitu Nurdin Halid, Nirwan Bakrie, Arifin Panigoro, dan George Toisutta tidak dapat mencalonkan diri sebagai ketua umum sesuai dengan keputusan Komite Banding PSSI tanggal 28 Februari 2011. Selanjutnya, FIFA mengangkat Agum Gumelar sebagai Ketua Komite Normalisasi PSSI. Pada tanggal 9 Juli 2011 diadakan Kongres Luar Biasa di Solo, Djohar Arifin Husin terpilih sebagai Ketua Umum PSSI periode 2011-2015.'],
        ['title'=>'Pemecatan Alfred Riedl','no'=>'Pemecatan dan penunggakan gaji Alfred Riedl menimbulkan hal yang kontroversial karena pihak PSSI mengaku bahwa Alfred Riedl dikontrak oleh Nirwan Bakrie dan bukan oleh PSSI akan tetapi Alfred Riedl membantah hal tersebut dan membawa persoalan ini ke FIFA dan kasus ini belum terselesaikan.'],
        ['title'=>'Kisruh Indonesia Premier league','no'=>'Pergantian kepengurusan Ketua umum PSSI dari Nurdin Halid ke Djohar Arifin Husin dimulai era kompetisi baru. Dalam pembentukan IPL banyak masalah yang terjadi karena aturan-aturan yang ditetapkan oleh PSSI.Pembentukan IPL mendapat tekanan dari 12 klub sepak bola atau kelompok 14 karena kompetisi berjumlah 24 klub dan 6 klub diantaranya langsung menjadi klub IPL']
    ],
    'DAFTAR'=>[
        'narasi'=>'FIFA sebagai otoritas tertinggi sepakbola dunia cukup tegas dalam memberikan sanksi terhadap anggota-anggotanya yang dianggap tidak becus dalam mengelola sepakbola. Persolan yang kerap menimpa organisasi induk sepakbola ditiap negara-negara anggotanya kebanyakan karena adanya intervensi dari Pemerintah. Dan saat ini, PSSI juga dalam ancaman sanksi karena persoalan serupa.<br>Berikut Negara yang pernah mendapat sanksi dari FIFA :',
        'list'=>[
            ['title'=>'Yunani','link'=>'http://m.bijaks.net/aktor/profile/yunani553f01508d3c0','img'=>'http://m.bijaks.net/public/upload/image/politisi/yunani553f01508d3c0/badge/abbec18b160e0b54db93586c07c87f9b8442bd1f.png','no'=>'Federasi Sepakbola Yunani, HFF mendapat sanksi dari FIFA pada 3 Juli 2006, karena adanya campur tangan pemerintah dan politisasi dalam sepakbola. Hal itu dianggap melanggar statuta FIFA. Tapi HFF lalu meresponsnya dengan mengajukan perubahan. Alhasil, FIFA pun memberi tenggat waktu hingga 15 Juli 2006, sebelum Yunani benar-benar dinonaktifkan dari persepakbolaan internasional. Dan, pada 7 Juli 2006, HFF memiliki undang-undang olahraga baru, yang sejalan dengan FIFA. Sanksi pun dicabut.'],
            ['title'=>'Iran','link'=>'http://m.bijaks.net/aktor/profile/iran537ad62310221','img'=>'http://m.bijaks.net/public/upload/image/politisi/iran537ad62310221/badge/129b0fb2cbcaf494bddbb63d133a42541ee0a22a.png','no'=>'November 2006, atau lima bulan setelah Iran tampil di Piala Dunia 2006, FIFA memberi sanksi kepada Federasi Sepakbola Iran, IRIFF. Hal tersebut tak terlepas dari campur tangan pemerintah perihal terpilihnya kembali Mohammad Dadkan sebagai Presiden IRIFF. FIFA menilai tak ada independensi terkait pemilihan presiden baru IRIFF, sehingga dianggap telah melanggar pasal 17 Statuta FIFA. Namun, setelah dilakukan pemilihan ulang Presiden IRIFF, tepatnya Desember 2006, sanksi tersebut dicabut.'],
            ['title'=>'Kenya','link'=>'http://m.bijaks.net/aktor/profile/kenya5371bddb5d31f','img'=>'http://m.bijaks.net/public/upload/image/politisi/kenya5371bddb5d31f/badge/63723683ae33bbb122ba69b4c114bba05e54f9bd.png','no'=>'Pada November 2006, Federasi Sepakbola Kenya, KFF mendapat sanksi dari FIFA, setelah adanya campur tangan pemerintah dalam hal sepakbola. Namun pada 2007, FIFA akhirnya mencabut sanksi, setelah adanya perbaikan dalam tata kelola federasi. Pada 2008, pemegang otoritas tertinggi sepakbola Kenya sendiri berpindah ke FKL. Sebelum akhirnya pada 2011, hanya ada satu badan yang membawahi sepakbola, yaitu FKF.'],
            ['title'=>'Kuwait','link'=>'http://m.bijaks.net/aktor/profile/kuwait538170b2aecee','img'=>'http://m.bijaks.net/public/upload/image/politisi/kuwait538170b2aecee/badge/14e7226e1d4bcd94201fed5113e4782ca1512775.png','no'=>'Pada Oktober 2007, Federasi Sepakbola Kuwait (KFA) mendapat sanksi oleh FIFA, berupa larangan mengikuti segala kegiatan sepakbola di level internasional. Itu merupakan buntut dari adanya intervensi pemerintah dalam pemilihan ketua umum dan anggota.
                    <br>Sebulan kemudian, sanksi tersebut dicabut sementara oleh FIFA, setelah KFA menyetujui adanya beberapa perubahan. Tapi, lalu sanksi diberikan lagi dan baru dilepas secara kondisional pada kongres FIFA pada 2009. Situasi mereka pun terus dimonitor oleh FIFA.'],
            ['title'=>'Ethiopia','link'=>'http://m.bijaks.net/aktor/profile/ethiopia537d83176e078','img'=>'http://m.bijaks.net/public/upload/image/politisi/ethiopia537d83176e078/badge/716b2d61ee859a1602624d75f517181934f02102.png','no'=>'Majelis Umum Ethiopia memecat Presiden Federasi Sepakbola Ethiopia, EFF, Ashebir Woldegiorgis pada Januari 2008. Tapi, Ashebir menolaknya, sehingga terjadi kekisruhan. Alhasil, FIFA memberi sanksi, karena pemerintah dianggap telah mengintervensi.
                    <br>Ethiopia pun mesti absen dari laga-laga internasional, termasuk kualifikasi Piala Dunia 2010. Namun, setelah terpilih presiden EFF yang baru, Sahlu Gebrewold Gebremariam, FIFA kemudian mencabut sanksi tersebut.'],
            ['title'=>'Madagaskar','link'=>'http://m.bijaks.net/aktor/profile/madagaskar53797834186f1','img'=>'http://m.bijaks.net/public/upload/image/politisi/madagaskar53797834186f1/badge/7a6584e36adfb151ecc37b1549f7f6f88395a939.png','no'=>'Pada 19 Maret 2008 FIFA memberi sanksi kepada Federasi Sepakbola Madagaskar, FMF. Itu dilakukan setelah pemerintah membubarkan kepengurusan FMF dan meminta dilakukannya pembentukan ulang. FIFA menganggap ada intervensi dari pemerintah.
                    <br>Alhasil, Timnas Madagaskar pun dilarang berpartisipasi dalam segala kompetisi di level internasional. Namun, Mahkamah Agung setempat kemudian tak memuluskan upaya pemerintah untuk membubarkan FMF. Sanksi pun dicabut pada 19 Mei 2008.'],
            ['title'=>'Peru','link'=>'http://m.bijaks.net/aktor/profile/peru553efe775c056','img'=>'http://m.bijaks.net/public/upload/image/politisi/peru553efe775c056/badge/64ce7d33d0f85f56f32c6daba98b27e7995aadd4.png','no'=>'Federasi Sepakbola Peru, FPF mendapat sanksi dari FIFA pada November 2008. Hal tersebut dikarenakan adanya percekcokan antara federasi dan pemerintah. FPF pun kemudian diberi waktu sebulan untuk mengendalikan situasi.
                    <br>Setelah dilarang tampil di semua pertandingan level Internasional, FPF pun akhirnya mampu memenuhi apa yang disyaratkan FIFA. Pada Desember 2008, atau sebulan sejak sanksi diberikan, FIFA akhirnya mencabut hukuman tesebut.'],
            ['title'=>'Brunei Darussalam','link'=>'http://m.bijaks.net/aktor/profile/bruneidarussalam537311be844bf','img'=>'http://m.bijaks.net/public/upload/image/politisi/bruneidarussalam537311be844bf/badge/8c321b0581c42089e49ebd9edbf16497cc4663d7.jpg','no'=>'Brunei dinonaktifkan dari segala urusan sepakbola internasional oleh FIFA pada September 2009. Hal tersebut merupakan imbas dari tindakan pemerintah yang melenyapkan Federasi Sepakbola Brunei, BAFA, dan membentuk kepengurusan anyar, FABD pada 2008.
                    <br>Setelah dibentuk komite normalisasi, akhirnya terbentuk federasi baru dengan nama NFABD. FIFA pun kemudian menyetujui pembentukan federasi tersebut dan resmi mencabut sanksi untuk Brunei pada Mei 2011.'],
            ['title'=>'Irak','link'=>'http://m.bijaks.net/aktor/profile/irak5371bcc81e12c','img'=>'http://m.bijaks.net/public/upload/image/politisi/irak5371bcc81e12c/badge/b6ff0864f180b7b932af06838d7c4735094e86df.png','no'=>'Pada November 2009, Komite Olimpiade Irak membubarkan Federasi Sepakbola Irak, IFA. Pihak keamanan pun kemudian menguasai kantor federasi. Dan, FIFA lalu memberi tenggat waktu 72 jam untuk memulihkan situasi, namun tetap tak ada perubahan.
                    <br>Sanksi diberikan, sehingga Irak nonaktif dari segala aktivitas sepakbola internasional. Baru setelah empat bulan, atau tepatnya pada Maret 2010, segala permasalahan tersebut bisa diatasi dan FIFA pun mencabut sanksi untuk Irak.'],
            ['title'=>'Nigeria','link'=>'http://m.bijaks.net/aktor/profile/nigeria5371bf6fb065b','img'=>'http://m.bijaks.net/public/upload/image/politisi/nigeria5371bf6fb065b/badge/babf934bc6a57f6c15d4b4fab8632d31e6c65b22.png','no'=>'Pada 4 Oktober 2010, Federasi Sepakbola Nigeria, NFF mendapat sanksi dari FIFA. Hal itu dilakukan setelah adanya tindakan sepihak dari pemerintah yang mencampuri urusan sepakbola nasional. Namun, empat hari berselang, hukuman tersebut dicabut.
                    <br>Hal serupa kembali terulang pada 9 Juli 2014. Lagi-lagi, pemerintah melakukan intervensi dan kali ini perihal pengangkatan sepihak Lawrence Katiken sebagai pemimpin federasi. Namun, 10 hari kemudian, situasi kembali normal dan FIFA mencabut sanksinya.
                    <br>(Dari berbagi sumber)']
        ]
    ],
    'KETUA'=>[
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/8/8c/Soeratin.jpg/150px-Soeratin.jpg','url'=>'http://m.bijaks.net/aktor/profile/soeratinsosrosoegondo553f193ec3b53','thn'=>'(1930-1940)','name'=>'Soeratin Sosrosoegondo'],
        ['img'=>'http://m.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://m.bijaks.net/aktor/profile/Artono','thn'=>'(1941-1949)','name'=>'Artono Martosoewignyo'],
        ['img'=>'http://www.tokohindonesia.com/index2.php?option=com_resource&task=show_file&id=47638&type=thumbnail_article','url'=>'http://m.bijaks.net/aktor/profile/maladi517cb841aa32f','thn'=>'(1950-1959)','name'=>'Maladi'],
        ['img'=>'http://m.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://m.bijaks.net/aktor/profile/Abdul','thn'=>'(1960-1964)','name'=>'Abdul Wahab Djojohadi koesoemo'],
        ['img'=>'http://4.bp.blogspot.com/-ixwbJ-KBVtY/TfqMwRfEg8I/AAAAAAAAAM4/i4TKuWWgL_Y/s1600/maulwi.jpg','url'=>'http://m.bijaks.net/aktor/profile/maulwisaelan553f2bbd7b5c5','thn'=>'(1964-1967)','name'=>'Maulwi Saelan'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/b/b9/Kosasih_opurwanegara_ris.jpg','url'=>'http://m.bijaks.net/aktor/profile/kosasihpurwanegara553f1371b17e5','thn'=>'(1967-1974)','name'=>'Kosasih Poerwanegara'],
        ['img'=>'http://m.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://m.bijaks.net/aktor/profile/Bardosono','thn'=>'(1975-1977)','name'=>'Bardosono'],
        ['img'=>'http://m.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://m.bijaks.net/aktor/profile/Moehono','thn'=>'(1977-1977)','name'=>'Moehono'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/commons/f/fa/Ali_Sadikin_%281975%29.jpg','url'=>'http://m.bijaks.net/aktor/profile/letnanjenderalpurnkkoalhalisadikin518093c1a0915','thn'=>'(1977-1981)','name'=>'Ali Sadikin'],
        ['img'=>'http://m.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://m.bijaks.net/aktor/profile/sjarnoebisaid553f1f9fd78b5','thn'=>'(1982-1983)','name'=>'Sjarnoebi Said'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/7/74/Kardono-First-President-of-the-AFF.jpg/220px-Kardono-First-President-of-the-AFF.jpg','url'=>'http://m.bijaks.net/aktor/profile/kardono553f09a62683f','thn'=>'(1983-1991)','name'=>'Kardono'],
        ['img'=>'http://cdn-media.viva.id/thumbs2/2011/03/23/107496_azwar-anas_663_382.jpg','url'=>'http://m.bijaks.net/aktor/profile/irhazwaranas51872cb2788a2','thn'=>'(1991-1999)','name'=>'Azwar Anas'],
        ['img'=>'https://pacpdipciawiebang.files.wordpress.com/2008/02/agum.jpg','url'=>'http://m.bijaks.net/aktor/profile/agumgumelar518768633e53b','thn'=>'(1999-2003)','name'=>'Agum Gumelar'],
        ['img'=>'http://statik.tempo.co/data/2012/06/29/id_128356/128356_620.jpg','url'=>'http://m.bijaks.net/aktor/profile/nurdinhalid530eb1689eb22','thn'=>'(2003-2011)','name'=>'Nurdin Halid'],
        ['img'=>'http://kanalsatu.com/images/20150119-141948_32.jpg','url'=>'http://m.bijaks.net/aktor/profile/djohararifinhusin552770aa7c1db','thn'=>'(2011-2015)','name'=>'Djohar Arifin Husin'],
        ['img'=>'http://www.bolaskor.com/wp-content/uploads/2014/01/La-Nyalla-Mattalitti.jpg','url'=>'http://m.bijaks.net/aktor/profile/lanyallamattalitti553482e51ddbe','thn'=>'(18 April 2015)','name'=>'La Nyalla Mattalitti']
    ],
    'KRONOLOGI'=>[
        ['date'=>'9 April 2015','content'=>'Komite Eksekutif (Exco) PSSI telah menyutujui usulan tentang penghentian sementara kompetisi QNB League akibat polemik dengan Badan Olahraga Profesional Indonesia (BOPI)/Kemenpora tentang keikutsertaan Persebaya Surabaya dan Arema Cronus yang tak lolos syarat verifikasi yaitu asas legalitas.<br><br>Perwakilan 18 klub telah disampaikan opsi-opsi yang akan diambil dalam rapat Exco ketika dipanggil untuk berkumpul bersama PSSI dan PT Liga Indonesia di Jakarta pada sehari sebelumnya.'],
        ['date'=>'11 April 2015','content'=>'Beredar surat ancaman sanksi dari FIFA yang ditandatangani Sekretaris Jenderal Jerome Valcke.<br><br>Sehari sebelumnya, 10 April 2015. Dalam surat itu FIFA menilai BOPI telah menambah kriteria tentang keikutsertaan klub. FIFA meminta pemerintah membatasi urusan rumah tangga PSSI.'],
        ['date'=>'12 April 2015','content'=>'Seluruh pertandingan QNB League resmi dihentikan sementara oleh PT Liga Indonesia berdasarkan keputusan Exco.<br><br>Pada saat yang sama Kemenpora merespon surat FIFA dan menyatakan tak ada penambahan kriteria yang dilakukan BOPI. Semua kriteria itu tertera dalam PSSI Club Licensing Regulation yang disetujui Exco PSSI pada rapat 28 September 2013.'],
        ['date'=>'13 April 2015','content'=>'Penghentian liga mulai mendapatkan korban. Persija Jakarta menunggak gaji pemain selama tiga bulan terakhir. Alasan yang dilontarkan manajemen Persija adalah dana sponsor yang tak turun akibat kompetisi berhenti.'],
        ['date'=>'16 April 2015','content'=>'Kemenpora mengirimkan surat peringatan ketiga (SP3) terhadap PSSI tentang pengabaian teguran dalam dua surat peringatan sebelumnya. Dalam surat peringatan ketiga tersebut Kemenpora memberi tenggat waktu 1 X 24 Jam terhadap PSSI sejak surat itu diterima pada pukul 18.40 WIB.<br><br>Pada malam harinya terjadi penyerbuan dengan kekerasan oleh sekelompok orang ke diskusi tentang Persebaya Surabaya yang disiarkan langsung pada malam hari di stasiun televisi lokal, Gedung Graha Pena, Surabaya.'],
        ['date'=>'17 April 2015','content'=>'Bonek alias suporter sepak bola Persebaya 1927 menolak rencana KLB PSSI di kota tersebut. Mereka menuntut Persebaya dikembalikan seperti semula. Ribuan bonek pun menyatakan akan melakuakn long march untuk menuntut pendapat mereka.<br><br>Bonek memasuki kawasan Jalan Embong Malang untuk melakukan aksi proters kepada PSSI. Sabtu (18/4). (CNN Indonesia/M. Arby Putratama)'],
        ['date'=>'18 April 2015','content'=>'Sebanyak 1450 personel aparat keamanan gabungan mengamankan KLB PSSI yang berlangsung di Hotel JW Marriott, Surabaya. Sekitar 300 meter jalan Embong dari lokasi JW Marriott disterilkan polisi dengan barikade empat lapis hingga lokasi hotel.<br><br>Ribuan bonek telah memadati area sekitar jalan Embong sejak pagi hari.<br><br>Tengah hari, Kemenpora memublikasikan surat pembekuan PSSI yang ditandatangani Menpora Imam Nahrawi. Dalam surat tersebut dinyatakan persiapan timnas untuk ikut Sea Games 2015 akan diambil alih KONI dan KOI.<br><br>Beberapa saat setelahnya KLB PSSI menghasilkan La Nyalla Mahmud Mattalitti akan menjad ketua umum organisasi tersebut menggantikan Djohar Arifin.<br><br>Djohar dan Sekretaris Jenderal PSSI Joko Driyono mundur dari pencalonan mereka menjadi Ketua Umum organisasi itu.<br><br>La Nyalla terpilih dengan perolehan 92 suara dari total 106 pemilik hak suara']
    ],
    'PENYOKONG'=>[
        ['name'=>'BNPT','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'http://2.bp.blogspot.com/-k5jeVQ1svsA/U_grEHrkP-I/AAAAAAAAAT4/8K2k_WM_500/s1600/LOGO-BNPT.png' ],
        ['name'=>'POLRI','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'http://2.bp.blogspot.com/-GBErBcGBQlo/VIzWFgVVidI/AAAAAAAAAgw/VGDR74ZsPZ0/s1600/polisi-polri.png'],
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef']
        ],
        'institusi'=>[
            ['name'=>'Bonek','img'=>'http://2.bp.blogspot.com/__ed956rZIkE/TPJyiz2qZzI/AAAAAAAAADY/H4aC62ss4hQ/s1600/27180_105731152786509_100000488752405_153904_4840158_n.jpg','url'=>''],
            ['name'=>'Viking','img'=>'http://2.bp.blogspot.com/-z4LhYlN_T34/TtI1uDcdJQI/AAAAAAAAACk/QXqDhsxYSeI/s150/VIKing.jpg','url'=>''],
            ['name'=>'Kemenpora','img'=>'http://www.fdsinews.com/wp-content/uploads/wpid-wp-1425427806616.jpeg','url'=>''],
            ['name'=>'BOPI','img'=>'https://chairilzm.files.wordpress.com/2013/05/logo_kemenpora.jpg','url'=>''],
            ['name'=>'PT. Liga Indonesia','img'=>'http://www.hariandepok.com/wp-content/uploads/2014/09/ISL.jpg','url'=>'']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227']
        ],
        'institusi'=>[
            ['name'=>'KONI','img'=>'http://2.bp.blogspot.com/-xC_gsQA7uN4/UTtgudTALxI/AAAAAAAAAIc/adugfUIV5hY/s320/LOGO+KONI+NARU+CR.png','url'=>''],
            ['name'=>'PSSI','img'=>'http://images.solopos.com/2013/03/logo-pssi3.jpg','url'=>''],
            ['name'=>'Jak Mania','img'=>'https://lh5.googleusercontent.com/-yWWR3Cu3FFw/TXTYqDbkPsI/AAAAAAAAABo/oHc8yVnnt0U/s320/1_105840306l.jpg','url'=>''],
            ['name'=>'FIFA','img'=>'http://img4.wikia.nocookie.net/__cb20150211093302/logopedia/images/5/5d/FIFA_Logo.svg','url'=>''],
            ['name'=>'SSB Telaga Gorontalo','img'=>'http://1.bp.blogspot.com/-N2GQ-rnmKcU/T3fmkTg1mHI/AAAAAAAAAqU/VpVhs40cI80/s400/1.jpg','url'=>''],
            ['name'=>'Sekretaris Asosiasi Provinsi Aceh','img'=>'','url'=>''],
            ['name'=>'PSM Makassar','img'=>'http://spartacks.org/wp-content/uploads/2011/11/PSM-Makassar.png','url'=>''],
            ['name'=>'Komite Exco PSSI','img'=>'','url'=>'']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Eko','jabatan'=>'Pengamat Bola','img'=>'http://m.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Di mata hukum nasional, yang dilakukan Menpora memang sudah tepat dan sesuai aturan. Namun saya menyayangkan keputusan ini sedikit keluar dari jalur yang sebelumnya dibangun'],
        ['from'=>'HM Ridwan Hisjam','jabatan'=>'Wakil Ketua Komisi X DPR RI','img'=>'http://assets.kompas.com/data/photo/2015/04/19/1753594RidwanHisjam780x390.jpg','url'=>'','content'=>'Tidak ada, karena saya tahu betul siapa Imam Nahrawi. Kita sepakat, apa yang menjadi kebijakan pemerintahan harus didukung biar berjalan dengan baik. Mari kita selesaikan kisruh PSSI ini dengan baik'],
        ['from'=>'Roy Suryo','jabatan'=>'Mantan Kemenpora','img'=>'http://www.artefak.org/wp-content/uploads/2015/01/Roy-Suryo.jpg','url'=>'','content'=>'Saya percaya Menteri Pemuda dan Olahraga saat ini Imam Narhrawi punya pikiran sekelas menteri yang baik dan bisa menyelesaikan'],
        ['from'=>'Andie Pecie','jabatan'=>'Ketua Presidium Bonek 1927','img'=>'http://msports.net/id-content/id-upload/150416andiepeci-792813-idcms.jpg','url'=>'','content'=>'Jadi ini momentum penting. Dalam usia yang ke-85 tahun, PSSI memang harus dibenahi untuk perkembangan sepak bola Indonesia ke depan. Jadi kami dari bonek mengapresias langkah Kemenpora'],
        ['from'=>'Herru Joko','jabatan'=>'Viking Persib Club (VPC)','img'=>'http://static.inilah.com/data/berita/foto/2125810.jpg','url'=>'','content'=>'Saya kira sikap tegas Menpora dalam menyelesaikan kusutnya sepakbola di dalam negeri sudah tepat, dan layak mendapat apresiasi'],
        ['from'=>'Ari Junaedi','jabatan'=>'Pengamat sepakbola','img'=>'http://rmolsumsel.com/images/berita/normal/333759_08121921042014_arijunaedi.jpg','url'=>'','content'=>'Menurut saya pembekuan dilakukan tidak gegabah, kita bisa melihat permasalahan konflik, kepentingan, dan politik, pun bisnis yang akhirnya tidak sehat'],
        ['from'=>'Gatot S Dewa Broto','jabatan'=>'Deputi V Kemenpora','img'=>'http://us.images.detik.com/content/2013/07/01/328/gatotkominfo460.jpg','url'=>'','content'=>'Jadi, saat ini fokus kami untuk membentuk tim transisi, kalau mereka (PSSI) ajukan gugatan, silahkan saja kami terbuka untuk kemajuan sepakbola Indonesia'],
        ['from'=>'Herru Joko','jabatan'=>'Viking Persib Club (VPC)','img'=>'http://static.inilah.com/data/berita/foto/2125810.jpg','url'=>'','content'=>'Persib tidak akan hilang dari hati. Santai saja. Pemerintah sudah berusaha yang terbaik untuk mengurusi sepak bola Indonesia.']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Tono','jabatan'=>'Ketua Umum KONI','img'=>'http://www.sportanews.com/wp-content/uploads/a-ketua-koni-tono-suratman.jpg','url'=>'','content'=>'Yang jelas saya yang buka kongres, tentu saya mengakui. Kecuali saat membuka saya bilang ada masalah. Kan ada perwakilan dari AFC dan FIFA di sana'],
        ['from'=>'La Nyalla Mattalitti','jabatan'=>'Ketua Umum Terpilih PSSI 2015','img'=>'http://www.bolaskor.com/wp-content/uploads/2014/01/La-Nyalla-Mattalitti.jpg','url'=>'','content'=>'Yang jelas pengurus PSSI sudah dinyatakan sah oleh FIFA. Semua berlangsung demokratis dan transparan. Saya mendapat ucapan selamat dari FIFA karena FIFA tidak menganggap adanya pembekuan'],
        ['from'=>'David Noemi','jabatan'=>'FIFA','img'=>'http://m.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Kegagalan (pemerintah Indonesia) melakukan hal itu (tidak mencampuri urusan PSSI) akan membuat FIFA tidak punya pilihan selain menjatuhkan sanksi kepada PSSI'],
        ['from'=>'Richard Ahmad','jabatan'=>'Ketua Jak Mania','img'=>'http://www.indopos.co.id/wp-content/uploads/2015/02/2128.jpg','url'=>'','content'=>'Penyelesaian masalah ini bisa dibicarakan atau dikompromikan. Saya dengar PSSI akan menghadap Wakil Presiden lagi untuk menyelesaikan masalah ini. Dengan DPR juga. Tapi jika masih belum, maka presiden harus turun tangan'],
        ['from'=>'Reni Marlinawati','jabatan'=>'Anggota Komisi X DPR RI','img'=>'http://www.suara-islam.com/images/berita/reni_marlinawati-ppp_20130917_174620.jpg','url'=>'','content'=>'Langkah Menpora dengan menerbitkan keputusan sanksi administrasi terhadap PSSI merupakan langkah ekstrem'],
        ['from'=>'Rudi Moonti','jabatan'=>'Kepala Sekolah Sepak Bola (SSB) Gelora Telaga Gorontalo','img'=>'http://m.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Persoalan dua institusi itu bisa berdampak pada pembinaan olah raga hingga ke daerah-daerah'],
        ['from'=>'Teguh Juwarno','jabatan'=>'anggota komisi X DPR RI','img'=>'http://static.republika.co.id/uploads/images/kanal_sub/anggota-komisi-iii-dpr-ri-dari-fraksi-partai-amanat-_111128172620-118.jpg','url'=>'','content'=>'Kemenpora yang membekukan PSSI padahal persoalannya hanya verifikasi klub-klub yang berlaga di ISL, itu sama saja menangkap tikus dengan membakar lubung padi'],
        ['from'=>'Hinca Panjaitan','jabatan'=>'kata Wakil Ketua Umum PSSI','img'=>'http://rmol.co/images/berita/normal/44796_01060712112014_hinca_panjaitan1.jpg','url'=>'','content'=>'Ibu menteri mempelajarai data tentang hasil kongres dan permasalahan SK (surat keputusan) Menpora, dan berjanji akan memediasi PSSI dengan Menpora secepatnya'],
        ['from'=>'Nurdin Halid','jabatan'=>'mantan Ketua Umum PSSI','img'=>'http://statik.tempo.co/?id=29567&width=475','url'=>'','content'=>'Tindakan Menpora dalam rangka membenahi profesionalisme PSSI itu benar, tapi tindakan membekukan itu keliru besar karena berimplikasi pada FIFA menghukum PSSI'],
        ['from'=>'Sutan Adil Hendra','jabatan'=>'anggota Komisi X DPR dari Fraksi Partai Gerindra','img'=>'http://infojambi.com/images/tokoh_politik/suta-aidil-dpr.jpg','url'=>'','content'=>'PSSI sudah menjadi kebanggan nasional. “Siapapun yang suka sepak bola, termasuk saya tentu kecewa dengan kebijakan yang baru saja dibuat  Menpora'],
        ['from'=>'Djohar Arifin','jabatan'=>'mantan Ketua umum PSSI, dan Dewan Kehormatan PSSI','img'=>'http://img.lensaindonesia.com/uploads/1/2012/04/Rekonsiliasi-johar.jpg','url'=>'','content'=>'Ada miskomunikasi antara PSSI dan Menpora. Oleh karena itu, harus ada dialog untuk memecahkan masalah. Saya akan dorong pengurus baru'],
        ['from'=>'Tengku Mahmud Khaidir','jabatan'=>'Sekretaris Asosiasi Provinsi Aceh','img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/04/18/528868/540x270/pssi-dibekukan-persebaya-menolak-tunduk-ke-menpora-20150418161742.jpg','url'=>'','content'=>'Jadi kami harap, Menpora (Imam Nahrawi) segera mencabut keputusan pembekuan. Dahulukan kepentingan bangsa dan negara, jangan sepak bola malah menjadi korban kebuasan kekuasaan ini'],
        ['from'=>'Erwin Aksa','jabatan'=>'CEO Bosowa Corporation dan pemilik PSM Makassar','img'=>'http://www.bosowa.co.id/asset/image/_47K68251%281%291.jpg','url'=>'','content'=>'Saya pikir pemerintah dalam hal ini Menpora tidak berhak melakukan hal itu karena PSSI bukan organisasi terlarang'],
    ],
    'VIDEO'=>[
        ['id'=>'PiwNSxpeSNM'],
        ['id'=>'IB1J3kk_xTI'],
        ['id'=>'_qpHo6lhpCM'],
        ['id'=>'O77LzQ7pk5s'],
        ['id'=>'B270beaE9HM'],
        ['id'=>'FUyjpU-HWfc'],
        ['id'=>'oq-PvvOseF0'],
        ['id'=>'jwSWD9Xw02I'],
        ['id'=>'P5ZQLG94iA0']
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }    
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/pssi/img.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PROFIL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <p style="text-align: justify;"><?php echo $data['PROFIL']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">DAFTAR KETUA UMUM</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <ul style="margin: 0px 0px 0px -2px;">
                    <?php
                    foreach($data['KETUA'] as $key=>$val) {
                        ?>
                        <a href="<?php echo $val['url'];?>">
                            <li class="col-xs-6 dukung">
                                <img src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                                <p>Tahun<br><?php echo $val['thn']; ?></p>
                            </li>
                        </a>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">POLEMIK</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['POLEMIK']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ULTIMATUM FIFA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['ULTIMATUM']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">DAFTAR NEGARA YANG PERNAH DISANKSI FIFA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p><?php echo $data['DAFTAR']['narasi'];?></p>
                <?php
                $no=1;
                foreach($data['DAFTAR']['list'] as $key=>$val){
                    ?>
                    <div class="black boxdotted">
                        <?php echo "<span style='font-size: 16px;font-weight: bold;'><span id='bulet'>".$no."</span>".$val['title']."</span>";?>                        
                        <p style="font-size: 12px;color: black;margin-left: 10px;margin-right: 10px;margin-top: 15px;">
                            <a href="<?php echo $val['link'];?>"><img class="bendera" src="<?php echo $val['img'];?>" alt="<?php echo $val['no'];?>"  data-toggle="tooltip" data-original-title="<?php echo $val['title'];?>"/></a>
                            <?php echo $val['no'];?>
                        </p>                        
                    </div>
                    <?php
                    $no++;
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KONTROVERSI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <div class="col-xs-12" style="margin: 0px;padding: 0px;">
                    <ol style="list-style-type: square;margin-left: -20px;">
                        <?php
                        foreach ($data['KONTROVERSI'] as $key => $val) {
                            echo "<li style='font-weight: bold;'>".$val['title']."</li><p class='font_kecil'>".$val['no']."</p>";
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <p>PARPOL PENDUKUNG</p>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                    $photo = 'http://www.dpp.pkb.or.id/unlocked/unlock/lambangresmipkb.jpg';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom2" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KRONOLOGI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <ol style="list-style-type: circle;margin-left: -10px;">
                    <?php
                    foreach ($data['KRONOLOGI'] as $key => $val) {
                        echo "<li style='margin-right: 20px;font-weight: bold;'>".$val['date']."</li><p>".$val['content']."</p>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>



