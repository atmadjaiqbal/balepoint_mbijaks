<?php
$data = [
    'block1'=>[
        'title'=>'Skandal Suap Perusahaan Farmasi',
        'narasi'=>'
            <img src="http://cdn.tmpo.co/data/2011/03/03/id_66745/66745_620.jpg" class="pic">
            <p>Salah satu perusahaan Farmasi ternama di Indonesia diduga melakukan praktek suap terhadap ribuan dokter. Praktek tersebut berbentuk Kongkalikong antara perusahaan obat dan ribuan dokter. Tujuannya untuk meraup untung yang besar dari hasil penjualan obat-obatan.</p>
            <img src="http://www.tokohindonesia.com/lintas-berita/admin/uploads/bpom-medah-musnahkan-obat-dan-makanan-berbahaya-net5.jpg" class="pic2">
            <p>Modus yang dijalankan dengan cara meresepkan obat-obatan dari perusahaan farmasi tertentu kepada pasien dengan tidak mengindahkan sisi kualitas obat dan dengan harga yang mahal sehingga lebih banyak merugikan pasien.</p>
			<p>Kasus ini kemudian terbongkar dan menjadi polemik di masyarakat karena ada indikasi tidak hanya melibatkan ribuan dokter dan perusahaan farmasi, tetapi ratusan rumah sakit serta apotik dengan skala tempat yang cukup luas, tersebar di seluruh Indonesia terlibat dalam kartel haram tersebut.</p>
        '
    ],

    'block2'=>[
        'title'=>'Jejak Suap Obat Di Perusahaan Farmasi',
        'narasi'=>'
            <img src="http://beritacenter.com/wp-content/uploads/2015/11/Suap-obat.jpg" class="pic">
            <p>Kasus penyuapan terhadap dokter  di Indonesia selama ini memang kerap terjadi. Adanya praktek gelap antara pihak perusahaan farmasi dan dokter  ditengarai karena persaingan bisnis. Bisnis tersebut merupakan bisnis yang cukup menguntungkan karena melihat fakta bahwa obat-obatan adalah kebutuhan  yang cukup penting bagi masyarakat.  Kebutuhan tersebut membuat produksi obat-obatan tidak diteraklan sehingga bisnis farmasi ini bisa terus hidup. Tahun ini ratusan Perusahaan farmasi bersaing memperebutkan Rp 69 triliun pasar domestik.  Mereka bersaing dengan cara menyuap para dokter agar mempromosikan merk obat mereka ke pasien melalui peresepan obat dari dokter.  Akibat persaingan yang tidak sehat ini, banyak dokter dan rumah sakit menjadi mitra kerjasama perusahaan-perusahaan farmasi dalam melariskan produknya.</p>
            <p>Fakta terbaru seputar kasus suap yang melibatkan ribuan dokter dan ratusan rumah sakit yang tersebar di beberapa provinsi di Indonesia menambah daftar hitam dunia Farmasi di Indonesia. Sebelumnya beberapa kasus serupa juga terbongkar dengan memakai modus operandi yang sama. Yakni menyuap para dokter agar melakukan peresepan obat dari perusahaan farmasi tertentu. Akibatnya banyak dokter yang meresepkan obat secara asal-aslan kepada pasien  dengan harga yang relatif mahal. Prinsipnya, yang penting obat laku terjual. Dan tentu, hasilnya menguntungkan bagi dokter dan pihak perusahaan.</p>
            <img src="http://cdn.tmpo.co/data/2015/11/02/id_450409/450409_620.jpg" class="pic2">
            <p>Bahkan diduga mayoritas perusahaan Farmasi yang ada di Indonesia memakai pola pemasaran  tersebut. Obat-obatan generik yang seharusnya bisa diperoleh dengan harga yang relatif rumah, diberi merk tertentu sehingga nilai jualnya bisa lebih tinggi. Keuntungan yang didapat dari hasil manufulasi merk tersebut ditaksir mencapai Rp 11,37 Triliun dalam kurung waktu dua tahun terakhir ini.</p>
        '
    ],

    'block3'=>[
        'title'=>'Pro-Kontra: Gratifikasi atau Bukan?',
        'narasi'=>'
            <img src="http://gresik.co/wp-content/uploads/2015/11/dokter-di-suap-farmasi.jpg" class="pic">
            <p>Maraknya kasus suap yang melibatkan dokter dan perusahaan farmasi merupakan konsekuensi dari lemahnya hukum berlaku di Indonesia. Sejauh ini belum ada regulasi yang tegas yang mengatur pemberian hadiah atau komisi terhadap para dokter dari perusahaan Farmasi. Padahal kasusnya sudah sangat massif dan cukup meresahkan masyarakat. Kasus tersebut menjadi pro-kontra apakah pemberian-pemberian untuk memuluskan kongkalikong tersebut masuk dalam kategori gratifikasi sehingga bisa dijerat dengan pasal pidana korupsi atau bukan?</p>
			<p>KPK yang notabenanya adalah lembaga antirasuah  sejauh ini belum pernah menangani perkara korupsi terkait dengan kongkalikong antara farmasi dan dokter mengenai peresepan obat tersebut. KPK beralasan belum ada laporan yang masuk dari masyarakat. Padahal kasus tersebut bisa dikategorikan sebagai gratifikasi sesuai ketentuan yang diatur dalam aturan KPK. Namun hal berbeda diungkapkan oleh piha Indonesian Corruption Watch (ICW). Mereka  memiliki pandangan berbeda mengenai masalah kongkalikong antara perusahaan farmasi dengan dokter. ICW dalam keterangannya berpendapat bahwa harus memisahkan antara dokter yang termasuk ke dalam Pegawai Negeri Sipil (PNS) dengan swasta. Jika dokter itu merupakan dokter swasta maka tidak dapat dikategorikan ke dalam kasus suap.  Hukum di Indonesia tidak mengenal suap untuk pihak swasta tidak seperti di beberapa Negara lain. Dokter yang dapat terkena kasus suap haruslah dia yang dalam ruang kerjanya terdapat uang negara. </p>
			<p>Dari perspektif yang lain, yakni secara  kode etik kedokteran, seorang dokter dilarang menerima uang dari perusahaan farmasi yang bisa mempengaruhi independensinya dalam meresepkan obat.  Seorang dokter yang terbukti menerima gratifikasi akan dikenai sanksi. Sankinya bergam, dari teguran sampai pencabutan surat tanda registrasi (STR) dokter sehingga ia tak bisa berpraktek lagi. Menteri Kesehatan Nila F. Moeloek mengatakan Kementerian Kesehatan perlu mengatur lebih rinci apa saja yang boleh dan tidak diterima dokter. Seorang dokter boleh menerima hadiah dari perusahaan obat bila ditujukan untuk pengembangan kemampuan si dokter.</p>
            <img src="http://www.radarpekalongan.com/wp-content/uploads/2015/11/700x400xKPK-Siap-Tindak-Dokter-Nakal.jpg.pagespeed.ic.bEDQcW-AV8.webp" class="pic2">
			<p>Menteri Kesehatan Nila F. Moeloek mengatakan Kementerian Kesehatan perlu mengatur lebih rinci apa saja yang boleh dan tidak diterima dokter. Seorang dokter boleh menerima hadiah dari perusahaan obat bila ditujukan untuk pengembangan kemampuan si dokter.</p>
        '
    ],

    'block4'=>[
        'title'=>'Modus Operandi',
        'narasi'=>'
            <img src="http://healindonesia.com/wp-content/uploads/2011/02/timthumb.jpg" class="pic">
            <p>Permainan kolusi antara perusahaan obat dan dokter merupakan bentuk kejahatan yang sudah lama ada dan terus berlangsung sampai sekarang. Mafia obat ini merupakan kejahatan yang teroganisir secara rapih antara entitas pabrik farmasi dan rumah sakit untuk merampok uang pasien.  Modus operandi praktek kongkalikan dikemas dalam bentuk kerjasama yang seolah-olah legal sehingga sulit untuk dilacak.</p>
            <p>Para mafia yang berperan di dalamnya melibatkan  pihak perusahaan dan medrep perusahaan farmasi yang berperan sebagai perencana dan  inisiator yang menjadi ujung tombak perusahaan dalam melakukan pendekatan kepada para dokter . Sedangkan para dokter spesialis baik yang perseorangan ataupun yang bekerja di rumah sakit merupakan aktor pelaku yang mendukung kejahatan obat yang sangat merugikan pasien. Kedua pihak merencanakan dan melakukan kejahatan terhadap pasien secara massif.</p>
			<p>Pihak perusahaan melalui medreb menawarkan produk dari perusahaannya ke pada sejumlah dokter. Penawaran tersebut berupa kerjasama untuk meresepkan obat ke pasien. Imbalan yang di dapat oleh dokter beragam, mulai dari pemberian langsung hadiah barang atau uang, namun ada juga mendapat imbalan dari hasil penjualan obat, bagi hasil. Umumnya dokter mendapat komisi 40-50 % dari total penjualan obat.</p>
            <img src="http://cdn.tmpo.co/data/2011/06/08/id_78797/78797_620.jpg" class="pic2">
			<p>Model pembiayaan pendidikan spesilis kepada sejumlah dokter ke luar negeri juga merupakan modus operandi kongkalikong perusahaan farmasi. Sejumlah dokter sengaja dibiayai dengan imbalan dapat diikat dalam bentuk kerjasama, yakni para dokter diwajibakan uuntuk merekomendasikan obat dari perusahaan farmasi yang membiayainya. Modus operandi tersebut sudah sulit untuk dilacak karena modus yang dipakai sangat rapih dan teroganisir karena proses perjanjian dalam kondisi terselubung dan dalam jangka waktu yang panjang.</p>
       '
    ],

    'block5'=>[
        'title'=>'Kasus PT Interbat',
        'narasi'=>'
            <img src="http://komprominews.com/wp-content/uploads/2015/11/INTERBAT.jpg" class="pic">
            <p>PT interbat menjadi salah perusahaan yang terindikasi melakukan kongkalikong dengan para dokter.  Perusahaan tersebut disinyalir menggelontorkan dana  fantastis yaitu sebesar 131 M rupiah untuk diberikan kepada dokter-dokter dalam periode waktu 2013 sampai 2015. Hal itu dilakukan tidak lain agar para dokter tersebut meresepkan obat-obat dari perusahaan mereka . Setidaknya 2.125 dokter dan 151 rumah sakit yang tersebar di lima provinsi. Yaitu Jakarta, Banten, Jawa Barat, Jawa Timur, dan Sulawesi Selatan terindikasi menerima uang suap dari perusahaan Farmasi yang berlokasi di Jawa Timur tersebut.</p>
            <p>Jejak suap yang dilakukan oleh PT interbat sudah terjadi sejak lama dengan modus operandi memberikan komisi kepada para dokter berupa sejumlah uang, fasilitas mewah, ataupun biaya untuk sekolah profesi ke luar negeri.  Imbalannya, para dokter harus meresepkan obat dari perusahan tersebut kepada pasien.  Cara ini cukup efektif dalam meraup untung yang besar dan tentu aman dari jangkauan pihak berwajib karena dikemas dengan modus terselubung yang teroganisir.</p>
            <p>Praktek haram tersebut tentu berdampak pada tingginya harga obat. Dan yang paling memprihatinkan adalah harga mahal tersebut tidak dibarengi dengan kualitas obat yang sepadan sehingga masyarakat menjadi korban utama dari praktek tersebut. Obat-obatan yang selama ini beredar di Indonesia  menurut Ketua Pengurus Besar Ikatan Dokter Indonesia,  Zaenal Abidin  adalah generik. Tak ada obat paten yang dimiliki perusahaan farmasi Indonesia. Meski kelas generik, kata dia, perusahaan farmasi sudah mengemasnya sedemikian rupa, lalu diberi merek tertentu. Zainal menyebut obat tersebut, generik bermerek.</p>
            <img src="http://img1.beritasatu.com/data/media/images/medium/1378176429.jpg" class="pic2">
            <p>"Setelah obat paten menjadi obat generik, lalu diproduksi oleh siapa saja. Tapi diakal-akali bahwa ada yang bermerek. Itu yang membuat nilai bertambah, khasiat sama. Sehingga obat ini menjadi pilihan baru bagi dokter," kata Zaenal, awal Oktober lalu. Kasus tersebut mendapat sorotan yang massif dari publik. Masyarakat mengharapakan pemrintah turun tangan untuk memberantas mafia-mafia obat yang cukup meresahkan dan merugikan masyrakay dari segi ekonomi dan kesehatan.</p>
        '
    ],

    'block6'=>[
        'title'=>'Pembelaan Interbat',
        'narasi'=>'
            <img src="http://1.bp.blogspot.com/-Hd20UVXSQSo/VXfYknVyAqI/AAAAAAAAA2w/yewypfyE98w/s1600/Pieter%2BTalaway.jpg" class="pic">
            <p>Dokumen yang terungkap mengenai penyuapan yang dilakukan oleh PT menyulut respon dari masyarakat luas. Kasus ini jelas menyudutkan pihak PT Interbat, namun perusahaan tersebut mengklarikasi bahwa tuduhan hanyalah tuduhan fitnah.</p>
            <p>Melalui pengacara perusahaan, Pieter Talaway dalam wawancara dengan Tempo. Pieter membantah bahwa perusahaannya sedang difitnah oleh para karyawan perusahaan yang sedang bersengketa. Hal itu terjadi menurutnya karena para karyawan yang bersengketa tersebut menuntut adanya pesangon sebesar milyaran rupiah di Pengadilan Hubungan Industrial. Oleh karena itu, para karyawan tersebut mengancam dengan cara membuka kebobrokan perusahaan yang menurutnya ini adalah tindak pemerasan.</p>
            <p>Selain itu, Pieter juga menyebutkan bahwa perusahaan kliennya tersebut memang memberikan potongan 50 % bagi apotek yang menjual barang mereka dan bukan kepada dokter. Karena itu, menurutnya perusahaan telah melakukan cara-cara yang sesuai dengan peraturan.</p>
            <img src="http://1.bp.blogspot.com/-Ay6YSrHiv4o/UBLzUJc940I/AAAAAAAAAuY/q9nKxaCBOg4/s1600/Linda+Sitanggang-pharma+community.jpg" class="pic2">
			<p>Tidak hanya itu, ia juga membantah adanya suap berupa mobil dan uang kepada dokter. Jika ada bukti yang menyebutkan hal itu terjadi, menurutnya itu hanyalah akal-akalan dari karyawan pemasaran yang memakai trik tidak jujur untuk menaikan omzet penjualan sehingga tidak ada hubungannya dengan perusahaan.</p>
        '
    ],

    'block7'=>[
        'title'=>'Memutus Mata Rantai Konspirasi',
        'narasi'=>'
            <img src="http://img.scoop.it/a_0avmN8z97aKNzFTWl3RTl72eJkfbmt4t8yenImKBVvK0kTmF0xjctABnaLJIm9" class="picprofil" style="width:100%;>
            <p class="rightcol">Potensi kerugian yang luar biasa dari praktek yang dilakukan mafia obat memang sangat besar. Pemerintah melalui instrumen penegak hukumnya seperti KPK dan Polisi harus bisa menanggulangi guna memutus mata rantai "kartel obat" yang sudah sangat meresahkan  masyarakat dengan melakukan penyidikan terhadap mafia obat dan menjatuhkan hukuman yang setimpal bagi mereka yang terbukti bersalah melakukan mafia obat yang merugikan pasien dalam jumlah dana yang sangat besar.</p>
            <p class="rightcol">Selain itu, Asosiasi profesional kesehatan seperti IDI, PDGI, ISFI, dan PERSI harus lebih berperan untuk mencegah dan memberikan sanksi kepada semua anggota yang menjadi pelaku mafia obat. Semua instrumen yang dimiliki asosiasi dapat digunakan untuk menyelamatkan konsumen kesehatan. BPOM dapat melakukan sanksi dengan mencabut nomor register obat dan melarang obat beredar dan diperdagangkan di Indonesia.</p>
            <p class="rightcol">Kemenkes dan Konsil Kedokteran Indonesia harus melakukan sanksi administratif kepada para dokter, direksi dan pemilik rumah sakit dengan mencabut ijin praktik dokter yang terlbat kongkalikong dengan perusahaan Farmasi.</p>
            <img src="http://cdn.metrotvnews.com/images/library/images/ekonomi/adam/Pekerja%20menyelesaikan%20pengemasan%20obat%20PT%20Indofarma%20Tbk%20(INAF)%20di%20Kawasan%20Industri%20Cibitung,%20Jawa%20Barat,%20foto%20yudi%20mahatma.jpg" class="picprofil" style="width:100%;>
        '
    ],

    'block8'=>[
        'title'=>'Aturan Tentang Gratifikasi',
        'narasi'=>'
            <img src="http://2.bp.blogspot.com/-ZrwaHOtzamc/Tbf7XjkKqPI/AAAAAAAAAOk/ynmuDJHEKpk/s1600/Palu+dan+timbangan.jpg" class="picprofil" style="width:100%;>
            <p class="rightcol">Beberapa aturan yang mengatur tentang gratifikasi tertuang di dalam Undang-Undang Nomor 31 Tahun 2001 tentang Tindak Pidana Korupsi sebagai berikut:</p>
			<p class="rightcol">Pasal 12 B</p>
			<ol class="rightcol">
				<li class="rightcol">Setiap gratifikasi kepada pegawai negeri atau penyelenggara negara dianggap pemberian suap, apabila berhubungan dengan jabatannya dan yang berlawanan dengan kewajiban atau tugasnya, dengan ketentuan sebagai berikut:</li>
				<ol type="a">
					<li class="rightcol">Yang nilainya Rp 10 juta atau lebih, pembuktian bahwa gratifikasi tersebut bukan merupakan suap dilakukan oleh penerima gratifikasi.</li>
					<li class="rightcol">Yang nilainya kurang dari Rp 10 juta, pembuktian bahwa gratifikasi tersebut suap dilakukan oleh penuntut umum.</li>
				</ol>
				<li class="rightcol">Pidana bagi pegawai negeri atau penyelenggara negara sebagaimana dimaksud dalam ayat (1) adalah pidana penjara seumur hidup atau pidana penjara paling singkat empat tahun dan paling lama 20 tahun, dan pidana denda paling sedikit Rp 200 juta dan paling banyak Rp 1 miliar.</li>
			</ol>
			<p class="rightcol">Pasal 12 </p>
			<ol class="rightcol">
				<li class="rightcol">Ketentuan sebagaimana dimaksud dalam Pasal 12 B ayat (1) tidak berlaku, jika penerima melaporkan gratifikasi yang diterimanya kepada KPK.</li>
				<li class="rightcol">Penyampaian laporan sebagaimana dimaksud dalam ayat (1) wajib dilakukan oleh penerima gratifikasi paling lambat 30 hari kerja terhitung sejak tanggal gratifikasi tersebut diterima.</li>
				<li class="rightcol">KPK dalam waktu paling lambat 30 hari kerja sejak tanggal menerima laporan wajib menetapkan gratifikasi dapat menjadi milik penerima atau milik negara.</li>
			</ol>
        '
    ],

    'institusiPendukung' => [
        ['link'=>'#','image'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcR4lW14qsYCZ7NxeXOuIVh9z-HESZ266HjwTSc_oWNSv3VpPiZnFw','title'=>'PT. Interbat'],
        ['link'=>'#','image'=>'http://pusatpengobatan.com/wp-content/uploads/2015/07/rumah-sakit-cipto-mangunkusumo-jakarta.jpg','title'=>'RS. Ciptomangunkusumo'],
    ],
	

    'institusiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/komisipemberantasankorupsi5192fb408b219','image'=> 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQQ77g4SbKSlQvVpWVWbfFd0iHFN8fPJt1IDFGdGyIInopfhnrIEw','title'=>'KPK'],
        ['link'=>'http://www.bijaks.net/aktor/profile/kementeriankesehatan52f9bdfc6e729','image'=> 'http://4.bp.blogspot.com/-IOoEQnAFcz0/VILOxKm7HII/AAAAAAAAA9M/loE2qxr5JhA/s1600/logo%2BCek%2BPengumuman%2BPendaftaran%2BHasil%2BTes%2BTKD%2BTKB%2BCPNS%2BDEPKES%2BKEMENKES%2B(%2BKementerian%2BKesehatan%2B)%2BTahun%2B2015.gif','title'=>'Kementrian Kesehatan'],
    ],

//    'partaiPendukung' => [
//        ['link'=>'','','title'=>''],
//    ],

//     'partaiPenentang' => [
//         ['link'=>'','image'=>'','title'=>''],
//     ],

    'quotePendukung'=>[
        ['from'=>'Zainal Abidin','jabatan'=>'Ketua Pengurus Besar Ikatan Dokter Indonesia','img'=>'http://img1.beritasatu.com/data/media/images/medium/1390391719.jpg','url'=>'#','content'=>'"Dalam etika kedokteran, dokter dibolehkan mendapat sponsorship berupa biaya transportasi, penginapan, dan makan untuk pendidikan berkelanjutan seperti seminar atau simposium."'],
        ['from'=>'Pieter Talaway','jabatan'=>'Pengacara PT. Interbat','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2013--04--38405-pieter-talaway-pn-surabaya-langgar-sema-no-1-tahun-2012.jpg','url'=>'#','content'=>'"Semua perusahaan itu cari untung, tapi harus ikut aturan. Kalau mengirimkan sales untuk menawarkan dan mempromosikan obat ke dokter, masak enggak boleh?""'],
        ['from'=>'Kelik Suhendri','jabatan'=>'Mantan Medical Representative Perusahaan Farmasi','img'=>'http://4.bp.blogspot.com/-pVIC0JMQWGk/VYpWWC_v9jI/AAAAAAAAA7o/SGnjmObfj6U/s200/dokter.jpg','url'=>'#','content'=>'"Setelah mereka selesai dihibur dan `pijit`, saya ditelepon dan disuruh membayar," ujar Kelik yang sekarang memilih berwiraswasta. "Tiap dokter," katanya, "menghabiskan uang sekitar Rp 8 juta."'],
        ['from'=>'Teddy Tjahjanto','jabatan'=>'Dokter Spesialis Penyakit Dalam yang mendapat suap dari PT. Interbat','img'=>'http://s3-eu-west-1.amazonaws.com/docplanner.co.id/doctor/19916c/19916ca4b4e6097c66abe59262c9c52f_large.jpg','url'=>'#','content'=>'"Semua farmasi begitu. Kalu enggak, apotik enggak bisa kasih murah sama orang."'],
    ],

    'quotePenentang'=>[
        ['from'=>'Nila F. Moeloek','jabatan'=>'Menteri Kesehatan','img'=>'https://img.okezone.com//content/2014/10/26/481/1057152/nila-f-moeloek-menteri-kesehatan-pilihan-jokowi-PXCgKtglOJ.jpg','url'=>'http://www.bijaks.net/aktor/profile/niladjuwitaanfasamoeloek544ccde20164c','content'=>'"Saya kira waktunya ini diperbaiki, termasuk hal-hal yang dikaitkan dengan dunia kesehatan."'],
        ['from'=>'Johan Budi','jabatan'=>'Plt KPK','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQUh4MXwUvKtO1l4BXKseV0rBVPzPICkx3LxcMOnQ2Sy3IQgH0Y','url'=>'http://www.bijaks.net/aktor/profile/johanbudisp51b9205b1dd91','content'=>'"Sebab, berdasarkan UU tindak pidana korupsi harus ada unsur sebagai penyelenggara negara. Bila swasta tentu hal itu tidak bisa."'],
        ['from'=>'Giri Suprapdiono','jabatan'=>'Direktur Gratifikasi, Komisi Pemberantasan Korupsi (KPK)','img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/06/26/407966/JOPFcUhmA6.jpg?w=668','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007','content'=>'"Pemberian fee baik berupa discount khusus maupun fasilitas lain seperti jalan-jalan ke luar negeri, biaya dan akomodasi seminar merupakan bentuk gratifikasi dan dapat dikategorikan tindakan korupsi. Pasalnya, gratifikasi perusahaan farmasi kepada dokter baik secara langsung maupun tidak langsung akan mempengaruhi dokter untuk memberikan resep atau alat kesehatan ke perusahaan tertentu yang telah menjalin kerjasama dengan dokter."'],
        ['from'=>'Prof. dr. Iwan Dwiprahasto','jabatan'=>'Guru Besar Ilmu Farmakologi, Universitas Gajah Mada','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSc6TQbRvL1TGH8nZDYGtw_TRH2NrY87kS9CfU77MD1n8yjuBDO','url'=>'http://www.bijaks.net/aktor/profile/irtaufikkurniawan50c7e1f8cb585','content'=>'"Obat jadi mahal karena harus membiayai dokter jalan-jalan ke luar negeri, main golf, atau beli mobil."'],
        ['from'=>'Zaenal Abidin','jabatan'=>'Majelis Kehormatan Etika Kedokteran (MKEK)','img'=>'http://cdn.tmpo.co/data/2015/11/02/id_450412/450412_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/sintanuriyahwahid5535efc286c80','content'=>'"Laporan yang masuk lebih banyak berkaitan dengan perilaku dokter kepada pasien. Berkaitan dengan obat ada di urutan bawah, Jika terbukti, akan dikenai sanksi."'],
        ['from'=>'Tama Satrya Langkun','jabatan'=>'Peneliti Divisi Investigasi dan Publikasi ICW','img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSX5po_7OWZzu4MjwlL9rIcDfYbIdOzxVDX-fzjRvNP3UJ2gkuR','url'=>'http://www.bijaks.net/aktor/profile/maiaestiantiy555acea4b73d2','content'=>'"Kalau dia hanya dokter swasta dan betul-betul nggak ada uang negara di situ, yah tidak bisa dikatakan suap."'],
        ['from'=>'C.H. Soejono','jabatan'=>'Direktur Umum Rumah Sakit Cipto Mangunkusumo','img'=>'https://liputan6-media-production.s3-ap-southeast-1.amazonaws.com/medias/731341/big/058556300_1409649449-Dr-Soejono-4-20140902-Johan.jpg','url'=>'http://www.bijaks.net/aktor/profile/farhatabbas51cce8399d1cd','content'=>'"Setelah akhirnya paham, saya langsung marah dan membentak dia."'],
    ],

    'video'=>[
        ['id'=>'vqXowr1Okwc'],
        ['id'=>'4-vdFde4qIY'],
        ['id'=>'EQWOknHG2ek'],
        ['id'=>'qEdMzGTRNU8'],
        ['id'=>'q8XQYYD14kY'],
        ['id'=>'l7PZ3dXZ8do'],
        ['id'=>'b_Ei0u7SLpo'],
        ['id'=>'F9rwQi-Wma8'],
        ['id'=>'cLQ7pdOQEe0'],
        ['id'=>'dLXZKvRjhb4'],
        ['id'=>'PhlmagVFEaU'],
        ['id'=>'HrvzX52T5tQ'],
        ['id'=>'7B_wYets8Kg'],
        ['id'=>'hGQ-_k0vUM0'],
    ],

    'foto'=>[
        ['img'=>'http://img.redaksi.co.id/2015/11/Mafia-Obat-dan-Suap-Dokter--Menteri-Kesehatan-Gandeng-KPK.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2015/11/02/id_450521/450521_620.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/kanal_slide/obat-obatan-_141205100758-799.jpg'],
        ['img'=>'http://img.antaranews.com/new/2014/05/ori/2014050844.jpg'],
        ['img'=>'http://www.flowcreteasia.com/media/104471/hcptinterbat-header-1.jpg'],
        ['img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRz9sIum4uPFzcOhcV8WLYwK-7TUz0oLz46Sm77OHo_bQFoh01-'],
        ['img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSn7YU3qOSZ8GnDZ_ZscrQfdvLWBPMbHSYhwf7XGPEzyKiXm1FGhA'],
        ['img'=>'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQd_gLuennttrdHuH5pvvWkYKOGENBxDG4eD8nzRm4JcTAvOwtz4A'],
        ['img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSnnLUhebYvIMulAVl-A30wDE-ab3XRVGJGM980l2r09eLRfHbUmw'],
        ['img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSnS4clCjGEVSzMYWXty-42k6fYVgjisTvMHOYM4MmT4DCm7v1h'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/menteri-kesehatan-nila-f-moeloek_20151106_181508.jpg'],
        ['img'=>'http://indobillboard.co.id/wp-content/gallery/interbat/interbat.jpg'],
        ['img'=>'http://4.bp.blogspot.com/-lYpCU-R0xqo/VUohjSCEExI/AAAAAAAAGTY/9Z-ambhj8j0/s1600/PT%2BInterbat.png'],
        ['img'=>'http://1.bp.blogspot.com/-GmhsW8h-ICM/VS5cvVFatoI/AAAAAAAAC3I/GBm8lWWIy1A/s1600/1.jpg'],
        ['img'=>'http://sentraloker.net/wp-content/uploads/2014/09/Penerimaan-CPNS-2014-di-Kemenkes-Kementerian-Kesehatan.jpg'],
        ['img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQIDxGSkuiyHzdBsQ1OAFcaEkZyhTjUeeBshgDdbedEx-Pc7Naz'],
        ['img'=>'http://baranewsaceh.co/wp-content/uploads/2015/10/KORUPSI-OBAT.jpg'],
        ['img'=>'http://bintangpos.com/wp-content/uploads/2013/05/obat-300x166.jpg'],
        ['img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcR-L3N4xq1jC6MaqWoHShdxZB9YA8noAVzWF30FLd7eXTDgvPAQ'],
        ['img'=>'http://klouddata.com/wp-content/uploads/2014/09/SAP_mr_app1.png'],
        ['img'=>'http://www.seek.com.au/templates/6704334_3_logo.jpg'],
        ['img'=>'http://3.bp.blogspot.com/-Ah7tIMXe-80/VMZgb1o6XVI/AAAAAAAACuU/VGkeY7DuCGs/s1600/Tren%2BPositif%2Bindustri%2BMamin%2B2015_consumedia.jpg'],
        ['img'=>'http://4.bp.blogspot.com/-9698Cn9ICAM/VMDAhnZa1KI/AAAAAAAACoQ/UXYgZ2ZFCsA/s1600/Investasi%2BSektor%2BMakanan%2BTumbuh_consumedia.jpg'],
        ['img'=>'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRK_ZFWwa2dRgDq0IJTcwXlR2DwKQlLseVGzcTcGMXKgAtcF7fU'],
        ['img'=>'http://www.tokohindonesia.com/lintas-berita/admin/uploads/bpom-medah-musnahkan-obat-dan-makanan-berbahaya-net5.jpg'],
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuYY8AbtFl_6mfHsNszgCPRIQgKtIYDFxWXbI1TzqQXGLqU_g1'],
        ['img'=>'http://cdn.tmpo.co/data/2015/11/02/id_450411/450411_620.jpg'],
        ['img'=>'http://www.goriau.com/assets/imgbank/05112015/b8f14a270a7e4158a15c0133h-14161.jpg'],
    ],

//    'BERITA'=>[
//        ['img'=>'','shortText'=>'','link'=>''],
//    ],

//    'kronologi'=>[
//        'list'=>[
//            ['date'=>'','content'=>'
//            '],
//            ['date'=>'','content'=>'
//            '],
//            ['date'=>'','content'=>'
//            '],
//        ]
//    ],

]

?>

<style>
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/kartelobat/top.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/kartelobat/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['block1']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block2']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block2']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block3']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block3']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block4']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block4']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block5']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block5']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block6']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block6']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block7']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block7']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block8']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block8']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>

       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePendukung'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

		<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['foto'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['video'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
<?php /*
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['berita'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>
*/ ?>
        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>
