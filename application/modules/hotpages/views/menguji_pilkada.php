<?php 
$data = [
    'HASILCOUNT'=>[
        'title'=>'Hasil Real Count di 9 Daerah',
        'narasi'=>'<img src="http://www.bijaks.net/assets/images/hotpages/pilkada2015/INFOGRAPHIC.jpg"">'
    ],
    'PELAKSANAAN'=>[
        'title'=>'MENGUJI PELAKSANAAN PILKADA SERENTAK 2015',
        'narasi'=>'<img src="http://cdn1-a.production.liputan6.static6.com/medias/946422/big/095831500_1438749995-pilkada-serentak-4-yos-150805.jpg" class="pic"><p>Setidaknya ada sebanyak 9 Provinsi dan ratusan kabupaten/kota menggelar Pemilihan Kepala Daerah (Pilkada) serentak, pada Rabu 9 Desember 2015. Gelaran pilkada secara serentak dengan skala nasional ini untuk pertama kalinya digelar di Indonesia untuk memilih pemimpin (kepala) daerah.</p>
                   <img src="http://www.jppr.or.id/wp-content/uploads/2015/04/Jadwal-Pilkada-Serentak-2015.jpg" class="pic2"><p>Format baru dalam mekanisme pemilihan kepala daerah ini juga merupakan momentum untuk menguji sistem kepemiluan dalam menyaring pemimpin-pemimpin daerah yang berkualitas serta mengukur tingkat efektifitas dan efisiensi dari pelaksanaan pilkada serentak terkait dengan penghematan anggaran, kecurangan -kecurangan pemilu seperti politik uang dan penggelembungan surat suara yang selama ini massif terjadi pada setiap gelaran pemilihan kepala daerah.</p>'
    ],
    'PERTAMA'=>[
        'title'=> 'Pertama Kalinya',
        'narasi'=>'<img src="http://www.kemendagri.go.id/media/article/images/2015/06/04/p/i/pilkada_2015_serentak_1.jpg" class="pic"><p>Pemilihan kepala daerah yang digelar secara serentak dengan skala nasional merupakan format baru dalam sistem pemilihan dan untuk pertama kalinya dilaksanakan di Indonesia. Sebanyak 269  kepala daerah dipilih oleh masyarakat  itu berlangsung di 32 dari 34 provinsi, kecuali  dua provinsi yang tak ikut menyelenggarakan Pilkada serentak 2015, yakni DKI Jakarta dan Nangroe Aceh Darussalam. Keduanya adalah daerah khusus yang mendapat pengecualian untuk menggelar pilkada serentak gelombang pertama pada 9 Desember 2015 kemarin. Pilkada serentak gelombang pertama ini akan menjadi labolatorium uji coba menuju pemilu serentak yang dicanangkan oleh pemerintah yang bertujuan untuk efektifitas penyelenggaraan pemilihan kepala pemerintah baik di tingkat daerah maupun tingkat nasional dan efisiensi anggaran penyelenggaran pemilu nasioanal ataupun pemilukada.</p>
                   <p>Gelombang kedua pilkada serentak akan dilaksanakan Februari 2017, untuk kepala daerah yang masa dinasnya berakhir pada semester 2 tahun 2016 dan 2017.</p>
                   <p>Gelombang ketiga, Juni 2018. Ini untuk kepala daerah yang masa jabatan kepala berakhir pada 2018 dan 2019. Pada 2019, akan terjadi pelaksanaan Pemilu Nasional serentak. Pemilu Legislatif dan Pemilu Presiden dan Wakil Presiden rencananya dilaksanakan pada hari dan waktu yang sama. Karenanya, pada tahun itu tak ada pilkada.</p>
                   <img src="http://radaronline.co.id/wp-content/uploads/logistik-pilkada.jpg" class="pic2"><p>Tahun 2020, pilkada serentak gelombang keempat akan digulirkan. Hal Ini adalah kesinambungan Pilkada 2015. Lalu pada 2022 yang merupakan kesinambungan pilkada 2017. Berikutnya pada 2023 sebagai kesinambungan dari pelaksanaan pilkada 2018.</p>
                   <p>Pada 2027, pilkada serentak secara nasional akan benar-benar direalisasikan. Selanjutnya, setiap 5 tahun hanya dikenal dua pemilu nasional. Yaitu pileg yang bebarengan dengan pilpres, serta pilkada secara nasional.</p>
                   <p>Agenda pilkada serentak ini merupakan pengejawantahan dari UU No 8/2015 tentang Perubahan atas Undang-Undang Nomor 1 Tahun 2015 tentang Penetapan Perppu No 1/2014 tentang Pemilihan Gubernur, Bupati, dan Walikota menjadi Undang-undang.</p>'
    ],
    'TAHAPAN'=>[
        'title'=> 'Tahapan-Tahapan dan Mekanisme',
        'narasi'=>'<img src="http://www.rumahpemilu.com/spaw/uploads/images/article/thumb/20150804_040231_infografis%20tahapan%20pilkada%202015.png" class="pic"><p>Pada bulan April lalu KPU merilis peraturan KPU nomor 2 tahun 2015 yang telah disahkan oleh Kementerian Hukum dan HAM. Dalam peraturan tersebut dijelaskan mengenai tentang tahapan, program dan jadwal penyelenggaraan pemilihan gubernur dan wakil gubernur, bupati dan wakil bupati, walikota dan wakil walikota, Berikut tahapan penting Pilkada 2015 yang tercantum dalam Peraturan KPU Nomor 2 tahun 2015:</p>
                   <ol type="A">
                     <li>Penyerahan Syarat Dukungan Calon Perseorangan
                       <ul style="list-style-type:circle">
                          <li>Penyerahan syarat dukungan calon gubernur dan wakil gubernur kepada KPU Provinsi: 8-12 Juni?</li>
                          <li>Penyerahan syarat dukungan calon bupati atau wakil bupati, calon walikota atau wakil walikota kepada KPU Kabupaten/Kota: 11-15 Juni</li>
                          <li>Penelitian administratif dan faktual di tingkat desa/kelurahan: 23 Juni-6 Juli</li>
                          <li>Rekapitulasi di tingkat kecamatan: 7-13 Juli</li>
                          <li>Rekapitulasi di tingkat kabupaten/kota: 14-19 Juli</li>
                          <li>Rekapitulasi di tingkat provinsi: 22-24 Juli</li>li>
                       </ul>
                     </li>
                     <li>Pendaftaran Pasangan Calon
                       <ul style="list-style-type:circle">
                          <li>?Pendaftaran pasangan calon: 26-28 Juli 2015</li>
                          <li>Pemeriksaan kesehatan: 26 Juli-1 Agustus 2015</li>
                          <li>Penyampaian hasil pemeriksaan kesehatan: 1-2 Agustus 2015</li>
                          <li>Penelitian syarat pencalonan dan syarat calon: 28 Juli-3 Agustus 2015</li>
                          <li>Pemberitahuan hasil penelitian syarat pencalonan/calon: 3-4 Agustus 2015</li>
                          <li>Perbaikan syarat pencalonan/calon dari partai politik/gabungan partai politik/perseorangan: 4-7 Agustus 2015</li>
                          <li>Penetapan pasangan calon: 24 Agustus 2015</li>
                          <li>Pengundian dan pengumuman nomor urut pasangan calon: 25-26 Agustus 2015</li>
                       </ul>
                     </li>
                     <li>Kampanye
                       <ul style="list-style-type:circle">
                          <li>Masa kampanye: 27 Agustus-5 Desember 2015</li>
                          <li>Debat publik/terbuka antar pasangan calon: 27 Agustus-5 Desember 2015</li>
                          <li>Masa tenang dan pembersihan alat peraga: 6-8 Desember 2015</li>
                       </ul>
                     </li>
                     <li>Laporan dan Audit Dana Kampanye</li>
                       <ul style="list-style-type:circle">
                          <li>Penyerahan laporan awal dana kampanye; 26 Agustus 2015</li>
                          <li>Penyerahan laporan penerimaan sumbangan dana kampanye: 16 Otober 2015</li>
                          <li>Penyerahan Laporan Penerimaan dan Penggunaan Dana Kampanye (LPPDK): 6 Desember 2015</li>
                          <li>Audit LPPDK kepada Kantor Akuntan Publik: 7-22 Desember 2015</li>
                          <li>Pengumuman hasil audit dana kampanye: 24-26 Desember 2015</li>
                       </ul>
                     <li>Pemungutan dan Penghitungan Suara
                       <ul style="list-style-type:circle">
                          <li>Pemungutan dan penghitungan suara serentak di TPS: ?9 Desember 2015</li>
                          <li>Pengumuman hasil penghitungan suara di TPS: 9-15 Desember 2015</li>
                          <li>Rekapitulasi hasil penghitungan suara tingkat kecamatan: 10-16 Desember 2015</li>
                          <li>Rekapitulasi hasil penghitungan suara tingkat KPU Kab/kota: 16-18 Desember 2015</li>
                          <li>Rekapitulasi hasil penghitungan suara tingkat KPU provinsi: 19-27 Desember 2015</li>
                       </ul>
                     </li>
                     <li>Penetapan Calon Terpilih
                       <ul style="list-style-type:circle">
                          <li>Penetapan pasangan calon bupati/wakil bupati atau calon walikota/wakil walikota terpilih: 21-22 Desember</li>
                          <li>Penetapan pasangan calon gubernur/wakil gubernur terpilih: 22-23 Desember</li>
                       </ul>
                     </li>
                   </ol>'
    ],
    'CATATAN'=>[
        'title'=> 'Catatan-Catatan KPU',
        'narasi'=>'<img src="http://cdn0-a.production.liputan6.static6.com/medias/1082839/big/046137000_1449986540-twitter.jpg" class="pic"><p>Ketua KPU Husni Kamil Manik mengatakan lembaganya telah  mencatat setidaknya terdapat 12 permasalahan yang muncul dalam pilkada serentak 2015.  Permasalahan tersebut muncul mulai dari penyerahan syarat dukungan pasangan calon hingga penetapan pasangan calon.</p>
                   <p>Masalah pertama, terkait adanya temuan dokumen palsu di 8 daerah di antaranya Simalungun. Kedua, adanya dualisme kepengurusan partai politik di 18 daerah salah satunya di Sumba Timur. Ketiga, masalah persyaratan dukungan partai politik terjadi di 16 daerah, di antaranya di Belitung Timur dan Sorong Selatan. Keempat, masalah yang berkaitan dengan waktu pendaftaran seperti yang terjadi di satu daerah, yaitu Supiori.</p>
                   <p>Kelima, KPU menemukan adanya permasalahan yang berkaitan dengan pemenuhan dokumen dari instansi lain, seperti di Jambi dan Kotawaringin Timur.</p>
                   <p>Keenam, persyaratan mantan narapidana yang maju dalam Pilkada ada lima daerah, di antaranya Bengkulu Selatan dan Sidoarjo.</p>
                   <p>Ketujuh, yakni adanya masalah dalam status petahana yang kembali maju dalam Pilkada 2015. Ini terjadi di enam daerah, di antaranya Tanjung Jabung Timur dan Ogan Ilir. Untuk yang kedelapan, masalah dukungan terhadap calon perseorangan yang terjadi di 25 daerah.</p>
                   <img src="http://cdn-2.tstatic.net/banjarmasin/foto/bank/images/pilkada-serentak_20151104_191342.jpg" class="pic2"><p>Kesembilan, berkaitan dengan syarat kesehatan terdapat di 3 daerah, di antaranya Musi Rawas dan Musi Rawas Utara.</p>
                   <p>kesepuluh, masalah dalam perubahan dokumen pencalonan yang terjadi di 3 daerah. Sementara untuk yang kesebelas, adanya masalah terhadap calon kepala daerah yang bermasalah dengan status tersangkanya di satu daerah, yakni Bengkalis.</p>
                   <p>Keduabelas, pergantian calon diluar ketentuan, yaitu di Simalungun dan Sigi.</p>
                   <p>Selain ke-12 masalah tersebut, Badan Pengawas Pemilu (Bawaslu) telah kebanjiran sengketa dari pasangan calon kepala daerah di sejumlah daerah. Setidaknya ada  ratusan sengketa yang diajukan pasangan calon, yang tidak terima atas penetapan KPU di beberapa daerah.Hampir lebih dari 100, sengketa itu, dari tingkat provinsi dan kabupaten atau kota.</p>'
    ],
    'PARTISIPASI'=>[
        'title'=> 'Partisipasi Pemilih Tidak Sesuai Harapan',
        'narasi'=>'<img src="https://akirafirdhie.files.wordpress.com/2015/10/pilkada-serentak-2015.jpg" class="pic"><p>Tingkat partisipasi masyarakat dalam memilih cenderung menurun dibanding dengan gelaran pilkada sebelumnya. Penururunan animo masyarakat dalam memberikan hak suaranya disinyalir karena minimnya sosialisasi dari pihak penyelenggara pemilu sehingga masih banyak masyarakat yang belum paham akan pentingnya partisipasi dalam menentukan dan memilih calon pemimpin daerah.</p>
                   <img src="http://pashaol.com/wp-content/uploads/2015/06/Pilkada-Serentak-Mantapkan-Demokrasi-Indonesia.jpg" class="pic2"><p>Rilis hasil quick count yang dilakukan oleh beberapa lembaga survey, di beberapa wilayah seperti, Sumatera, Kalimantan, Jawa Timur, dan beberapa daerah lainnya memperlihat tingkat golput yang tinggi, yakni  menembus kisaran angka 50-60 persen atau tingkat partisipasi masyarakat berkisar di angka 60 persen. Fakta-fakta ini tentu mengkhawatirkan karena tidak sesuai dengan ekspektasi KPU sebagai lembaga fasilitator pemilu yang mematok angka 70 persen partisipasi masyarakat.</p>'
    ],
    'PLUSMINUS'=>[
        'title'=> 'Plus-Minus',
        'narasi'=>'<img src="http://cdn0-a.production.liputan6.static6.com/medias/1078940/big-portrait/020841900_1449570367-SI-KORA-Maskot-PEMILU-2014.jpg" class="pic">
                   <p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Gelaran pilkada serentak yang telah dilaksanakan masih menyisakan berbagai masalah. Namun tidak sampai mempengaruhi jalannya proses pemilihan secara signifikan.  Masalah-masalah yang muncul berkaitan dengan hal teknis pelaksanaan seperti penundaan pilkada di lima wilayah. Karena permasalahan hukum, pilkada yang seharusnya benar-benar serentak menjadi tidak berbarengan sepenuhnya.</p>
                   <p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Beberapa daerah yang harus mengalami pemungutan suara ulang. Pemungutan suara ulang harus dilakukan karena ada pelanggaran ketika pemungutan suara seperti pemilih mencoblos dua kali, pemilih yang bukan warga daerah tersebut memilih.</p>
                   <img src="http://www.tobasatu.com/wp-content/uploads/2015/05/pilkada-serentak-448x330.jpg" class="pic2"><p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Maraknya praktek politik uang dan  masih adanya tindakan anarkis dari para pendukung paslon di beberapa tempat yang tidak puas menerima hasil pemilihan. Sementara itu, kegaduhan terkait dengan sengketa hasil pemilu diprediksi akan meningkat setelah hasil rekapitulasi pemilihan rampung diselesaikan oleh KPU.</p>
                   <p class="font_kecil" style="margin-left: 20px;margin-right: 20px;">Dan yang paling memilukan adalah tingkat partisipasi masyarakat menurun drastis, yakni menembus kisaran angka 50 sampai 70 persen. Namun secara keseluruhan, perhelatan pilkada serentak diklaim relatif berlansung aman dan tertib</p>'
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Surya Paloh','jabatan'=>'Ketua Umum Partai NasDem','img'=>'http://rmol.co/images/berita/normal/698140_11164527102015_surya_paloh','url'=>'http://www.bijaks.net/aktor/profile/suryapaloh511b4aa507a50','content'=>'“Ini tidak terpengaruh ke Pilkada, buktinya di Sumatera itu kepala daerah (yang diusung NasDem) banyak menang,” (Surya Paloh, Ketua Umum Partai NasDem)'],
        ['from'=>'Zuhairi Misrawi','jabatan'=>'Politisi PDIP','img'=>'http://2.bp.blogspot.com/-JasDNwF7fjs/Uf1STweBd2I/AAAAAAAABww/nljdNBoklLk/s1600/zuhairi_f_619_f_297.jpg','url'=>'http://www.bijaks.net/aktor/profile/zuhairimisrawi52803d3047a74','content'=>'“Alhamdulillah Pilakada serentak berjalan lancar. Ini membutkikan bahwa kita sudah matang dalam berdemokrasi,” (Zuhairi Misrawi, Politisi PDIP)'],
        ['from'=>'Boediono','jabatan'=>'Mantan Wakil Presiden RI','img'=>'http://www.sindotrijaya.com/uploads/news/resize3/boediono_4.jpeg','url'=>'http://www.bijaks.net/aktor/profile/profdrhboedionomec5132cc6322ec2','content'=>'"Saya kira pemerintah sudah benar arahnya untuk efisiensi demokrasi. Saya dukung," (Boediono, Mantan Wakil Presiden RI) '],
        ['from'=>'Tjahjo Kumolo','jabatan'=>'Mendagri','img'=>'http://www.tjahjokumolo.com/wp-content/uploads/2010/10/Tjhajo.jpg','url'=>'http://www.bijaks.net/aktor/profile/tjahjokumolosh50f4fc41d9f04','content'=>'"Pemerintah optimistis kalau ini semua bisa berjalan , kecuali area rawan bencana, bisa saja ditunda," (Tjahjo Kumolo, Mendagri)'],
        ['from'=>'Irman Gusman','jabatan'=>'Ketua DPD RI','img'=>'http://gambar.radarpena.com/mei/images/Nasional/Irman_Gusman.jpg','url'=>'http://www.bijaks.net/aktor/profile/hirmangusmansemba510b3035b6e5a','content'=>'"Pilkada Serentak sebagai agenda kebangsaan harus menjadi prioritas tidak boleh dikalahkan dengan apapun, walaupun ada masalah internal (parpol) maka itu harus diselesaikan," (Irman Gusman, Ketua DPD RI)'],
        ['from'=>'Jusuf Kalla','jabatan'=>'Wapres RI','img'=>'http://ejurnalism.com/wp-content/uploads/2015/11/jusuf-kalla.jpg','url'=>'http://www.bijaks.net/aktor/profile/drshmuhammadjusufkalla50ee870b99cc9','content'=>'"Bagus pelaksanaannya, Aman. Tidak ada konflik-konflik yang dilaporkan. Hanya, ya insiden-insiden kecil ada lah tapi bukan pokok," (Jusuf Kalla, Wapres RI)'],
        ['from'=>'Daniel Johan','jabatan'=>'Wapres RI','img'=>'http://www.indochinatown.com/wp-content/uploads/2013/03/daniel-johan.jpg','url'=>'http://www.bijaks.net/aktor/profile/danieljohan52cd07f5a8327','content'=>'"Kami tetap posisi yang ada sekarang (menolak revisi), karena seluruh kabupaten sudah siap melakukan pilkada serentak, KPU juga sudah siap," (Daniel Johan, Anggota DPR asal Fraksi Partai Kebangkitan Bangsa (PKB))']
    ],

    'QUOTE_PENENTANG'=>[
        ['from'=>'Nico Harjanto','jabatan'=>'Direktur Populi Center','img'=>'http://cdn1-a.production.liputan6.static6.com/medias/832095/big/064697600_1426758259-Populi_Center_8.jpg ','url'=>'http://www.bijaks.net/aktor/profile/nicoharjanto54efe2d5d3010','content'=>'"Jadi, dari kalangan yang sudah ada di pemerintahan atau kalaupun yang dari luar bisa kita tebak. Kalau bukan incumbent, sebagian besar mereka wiraswasta. Tapi pengusaha daerah itu biasanya juga kroni incumbent," (Direktur Populi Center, Nico Harjanto)?'],
        ['from'=>'Sebastian Salang','jabatan'=>'Formappi','img'=>'http://cdn.rimanews.com/bank/Sebastian_Salang-2-MI.jpg','url'=>'http://www.bijaks.net/aktor/profile/sebastiansalang52a803abd4ff1','content'=>'"Kalau itu terjadi, maka siap-siap kepala daerah terpilih menjadi pesakitan. Berakhir di penjara karena mahar politik yang besar, biaya kampanye yang besar, ditambah lagi transaksi-transaksi untuk mendapatkan suara," (Koordinator Forum Masyarakat Peduli Parlemen Indonesia (Formappi), Sebastian Salang)?'],
        ['from'=>'Didik Supriyanto','jabatan'=>'Perludem','img'=>'http://www.rumahpemilu.com/spaw/uploads/images/article/thumb/20140714_081749_didik%20supriyanto%20pemilu.bmp','url'=>'http://www.bijaks.net/aktor/profile/didiksupriyanto5611f5ea32132','content'=>'"Publikasi dari pasangan calon ke masyarakat ini kan relatif terbatas dibandingkan kemarin-kemarin. Saya khawatir mereka hanya kenal incumbentsaja, tidak kenal yang lain," (Ketua Perkumpulan untuk Pemilu dan Demokrasi (Perludem), Didik Supriyanto)?'],
        ['from'=>'Masykurudin Hafidz','jabatan'=>'JPPR','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2013--08--43300-jppr-masykurudin-hafidz-bawaslu-dinilai-tak-bertaring-dalam-peranan-pengawasannya.jpg','url'=>'http://www.bijaks.net/aktor/profile/masykurudinhafidz5355d3e458569','content'=>'"Permintaan saham parpol itu terjadi pada saat pencalonan ditambah dengan mereka tidak mau cawe-cawe untuk mensukseskan pasangan calonnya. Terlihat dari besaran sumbangannya itu," (Koordinator Nasional Jaringan Pendidikan Pemilih untuk Rakyat (JPPR), Masykurudin Hafidz)'],
        ['from'=>'Fahri Hamzah','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://www.fahrihamzah.com/wp-content/uploads/2013/07/166077.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09','content'=>'"Pilkada hampir lewat karena banyak berita ‘papa minta saham’ berefek pada menurunnya pemilih dan antusiasme masyarakat terhadap pilkada. Ya terbukti golputnya masih besar," (Fahri Hamzah, Wakil Ketua DPR RI)'],
        ['from'=>'Yandri Susanto','jabatan'=>'Anggota Komisi II DPR RI','img'=>'http://lintaspos.com/wp-content/uploads/2015/06/Yandri.jpg','url'=>'http://www.bijaks.net/aktor/profile/yandrisusanto52e5d5a9a47a8','content'=>'"Kita akan panggil Bawaslu serta KPU untuk meminta laporan, apa yang sebenarnya terjadi. Misalnya di Serang hanya 51 persen yang hadir. Ini harus ada penjelasannya," (Yandri Susanto, Anggota Komisi II DPR RI)']
    ],

    'VIDEO'=>[
        ['id'=>'-y_jrLDQz3U'], ['id'=>'TFhZ07lv4S4'], ['id'=>'As8x4ukIKMs'], ['id'=>'ukBfcqpHFfE'], ['id'=>'E2NdxOfTli8'], ['id'=>'MM_-R7Uhij0'],
        ['id'=>'HuX8lm0Oyd4'], ['id'=>'-SNwzxcKp-0'], ['id'=>'9QKora0ZXxc'], ['id'=>'CgP-evJu-ww'], ['id'=>'YUk_bsRR5hY'], ['id'=>'1Ht7UfOkJRo'],
        ['id'=>'e3fE1TiaI5c'], ['id'=>'tZ9MHYQogXs'], ['id'=>'nnX83XazMa8'], ['id'=>'oqR1TQCnrIg'], ['id'=>'XAtg1_AqET4'], ['id'=>'YPOdnsFlAIg'],
        ['id'=>'KIS0DKEB-i8'], ['id'=>'1NjnQosZgLQ']
    ],
    'FOTO'=>[
        ['img'=>'http://oketimes.com/photo/dir012015/oketimes_KPU-Riau-Lakukan-Persiapan-Jelang-Pemilukada-Serentak.jpg'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/05/30/12/1007172/kpu-bakal-sukses-gelar-pilkada-serentak-bRV.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/07/08/411373/ALcwIu3zcg.jpg?w=668'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/kpu-resmikan-pelaksanaan-pilkada-serentak_20150417_174308.jpg'],
        ['img'=>'http://www.pemilu.com/wp-content/uploads/2015/05/Pilkada-Serentak-2015-Habiskan-Rp-67-Triliun-620x330.jpg'],
        ['img'=>'http://www.jpnn.com/picture/watermark/20150223_210040/210040_637301_pilkada_hitung_suara_dl.jpg'],
        ['img'=>'http://kpu-banjarnegarakab.go.id/file/upload/ok_.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/inpicture_slide/petugas-polisi-brimob-melakukan-aksi-pengamanan-saat-simulasi-pengamanan-_150811142508-840.jpg'],
        ['img'=>'http://elshinta.com/upload/article/_7307213787.jpg'],
        ['img'=>'http://www.indopolling.co/wp-content/uploads/2015/10/13102015.jpg'],
        ['img'=>'http://cdn.gresnews.com/showimg.php?size=view&imgname=2015420090639-pemilukadakpu.jpg'],
        ['img'=>'http://i.okezone.tv/photos/2015/04/07/19152/119675_medium.jpg'],
        ['img'=>'http://images1.rri.co.id/thumbs/berita_208833_800x600_PSX_20151013_130855-1600x1301.jpg'],
        ['img'=>'http://beritaintrik.com/sites/default/files/field/image/emil%20dardak.jpg'],
        ['img'=>'http://metropolitan.id/wp-content/uploads/2015/02/16-Lima-600x330.jpg?doing_wp_cron=1449716438.6900560855865478515625'],
        ['img'=>'http://beritahati.com/images/artikel/2650_1.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/07/27/326935_miing-daftar-calon-wakil-bupati-karawang_663_382.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2015/07/28/id_422685/422685_620.jpg'],
        ['img'=>'http://suarasukabumi.com/wp-content/uploads/2015/03/image9.jpg'],
        ['img'=>'https://i2.wp.com/sulbarpos.com/wp-content/uploads/2015/06/b3.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/pantomim_20150727_161546.jpg'],
        ['img'=>'https://i.ytimg.com/vi/3nMZZYLeqk8/maxresdefault.jpg'],
        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/952791/big/002802300_1439301427-20150811-_KPU_Tutup_Pendaftaran_Calon_Bupati-Jakarta-_Husni_Kamil-04.jpg'],
        ['img'=>'http://www.jpnn.com/picture/watermark/20150713_052544/052544_334496_cak_imin_dan_zumi_zola.jpg'],
        ['img'=>'http://detaksamarinda.com/sites/default/files/RITA%20WIDYASARI.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/surabaya/foto/bank/images/baliho-jember_20150511_175300.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/20130923_perhitungan-suara-pilbup-ciamis-2013_9251.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/07/28/326948_dimas-babai--calon-wali-kota-dan-wakil-wali-kota-depok_663_382.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/07/22/161109801-foto01-Copy27780x390.JPG'],
        ['img'=>'http://cdn-2.tstatic.net/wartakota/foto/bank/images/20150727pilkada-serentak_20150727_151504.jpg'],
        ['img'=>'http://www.okejoss.co/wp-content/uploads/2015/05/kpu-resmikan-pelaksanaan-pilkada-serentak_2015.jpg'],
        ['img'=>'http://img.antaranews.com/new/2015/05/ori/20150504Rakor-Pilkada-Serentak-040515-wpa-1.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/937816/big/065470200_1437984754-20150727-Pilkada_Serentak-Tangerang-Airin_Rachmi_Diany1.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/12/09/f2f3c6a1-5994-416a-bef5-799660c45361_169.jpg?w=650'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/12/09/8cd8bfbd-be76-4920-9429-4afe56993858_169.jpg?w=780&q=90'],
        ['img'=>'http://www.ehijrah.com/wp-content/uploads/2015/11/cawalkot-tangsel-2015-660x330.jpg'],
        ['img'=>'http://media.suara.com/thumbnail/650x365/images/2015/10/16/o_1a1nnbnsdeiu942ivb1qo4ro7a.JPG?watermark=true'],
        ['img'=>'http://i.okezone.tv/photos/2014/09/25/16469/102842_medium.jpg'],
        ['img'=>'http://img.antaranews.com/new/2015/11/ori/20151119antarafoto-kertas-suara-pilkada-serentak-181115-yu-1.jpg'],
        ['img'=>'http://i.okezone.tv/photos/2014/09/25/16469/102842_medium.jpg']
    ],
    'INSTITUSIPENDUKUNG'=>[
        ['url'=>'http://www.bijaks.net/aktor/profile/kementriandalamnegeri531c1cbb6af24','img'=>'https://pbs.twimg.com/profile_images/378800000756959754/622469540a4cc105e77cb7179b31c9e4_400x400.png','name'=>'Kementerian Dalam Negeri'],
        ['url'=>'http://www.bijaks.net/aktor/profile/komisipemilihanumumkpu519430c27915f','img'=>'http://kpu-jatengprov.go.id/wp-content/uploads/2015/08/LOGO-KPU-kecil.png','name'=>'Komisi Pemilihan Umum'],
        ['url'=>'http://www.bijaks.net/aktor/profile/dewanperwakilandaerah531d2cecd99f7','img'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Coat_of_arms_of_the_Regional_Representative_Council_of_Indonesia.svg/2000px-Coat_of_arms_of_the_Regional_Representative_Council_of_Indonesia.svg.png','name'=>'Dewan Perwakilan Daerah'],
        ['url'=>'http://www.bijaks.net/aktor/profile/kepolisiannegararipolri536756e743fd0','img'=>'http://2.bp.blogspot.com/-GBErBcGBQlo/VIzWFgVVidI/AAAAAAAAAgw/VGDR74ZsPZ0/s1600/polisi-polri.png','name'=>'Polri'],
        ['url'=>'http://www.bijaks.net/aktor/profile/kementerianhukumdanhakasasimanusia540e76759d3bb','img'=>'https://upload.wikimedia.org/wikipedia/id/8/82/Logo_Kemenkumham.jpg','name'=>'Kementerian Hukum dan HAM'],

    ],
    'PARTAIPESERTA'=>[
        ['url'=>'http://www.bijaks.net/aktor/profile/partaidemokrasiindonesiaperjuangan5119ac6bba0dd','img'=>'http://rmol.co/images/berita/normal/914268_11491028062015_pdip.jpg','name'=>'PDIP'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaigolongankarya5119aaf1dadef','img'=>'http://assets.kompas.com/data/photo/2013/01/14/1156005-logo-partai-golkar-780x390.jpg','name'=>'GOLKAR'],
        ['url'=>'http://www.bijaks.net/aktor/profile/nasdem5119b72a0ea62','img'=>'http://static.inilah.com/data/berita/foto/1679362.jpg','name'=>'NASDEM'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaikebangkitanbangsa5119b257621a4','img'=>'http://rakyatku.com/assets/uploads/2015/10/PKB.jpg','name'=>'PKB'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaikeadilansejahtera5119b06f84fef','img'=>'http://www.tobasatu.com/wp-content/uploads/2015/11/PKS.jpg','name'=>'PKS'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaiamanatnasional5119b55ab5fab','img'=>'http://www.sayangi.com/media/k2/items/cache/93b534e8083bb8d59b52d163f17f31e2_XL.jpg','name'=>'PAN'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaigerakanindonesiarayagerindra5119a4028d14c','img'=>'http://fajar.co.id/wp-content/uploads/2015/06/Partai-Gerindra.jpg','name'=>'GERINDRA'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaidemokrat5119a5b44c7e4','img'=>'http://gambar.radarpena.com/mei/images/Nasional/demokrat.jpg','name'=>'DEMOKRAT'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaihatinuranirakyathanura5119a1cb0fdc1','img'=>'https://upload.wikimedia.org/wikipedia/id/c/ce/HANURA.jpg','name'=>'HANURA'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaipersatuanpembangunan5189ad769b227','img'=>'http://dprd-tegalkab.go.id/wp-content/uploads/2015/04/601073_05390529102014_Logo-PPP.jpg','name'=>'PPP'],
        ['url'=>'http://www.bijaks.net/aktor/profile/partaibulanbintang52155330a9408','img'=>'http://bulan-bintang.org/wp-content/uploads/2015/03/Copy-of-LOGO-PBB1.jpg','name'=>'PBB'],

    ]    
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-left: 10px;
        float: right;
        border: 1px solid black;
        margin-top: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .clear {
        clear: both;
    }
</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['PELAKSANAAN']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/pilkada2015/pilkada-banner.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['PELAKSANAAN']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['PERTAMA']['title'];?> </span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PERTAMA']['narasi'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['TAHAPAN']['title'];?> </span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['TAHAPAN']['narasi'];?></p>
            </div>
        </div>        

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">Pilkada Serentak Dalam Angka</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                    <img src="http://kesbangpol.kemendagri.go.id/upload/Pilkada127.jpg" class="picprofil" style="width: 100%;"><br>
                    <ul style="margin-left: 5px;margin-right: 20px;">
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">9 Provinsi (pemilihan Gubernur)</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">30 Kota (pemilihan Wali Kota)</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">224 kabupaten (pemilihan Bupati).</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">21 pasangan calon Gubernur dan Wakil Gubernur.</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">714 pasangan calon Bupati dan Wakil Bupati.</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">117 pasangan Wali Kota dan Wakil Wali Kota.</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">246.243 buah Tempat Pemungutan Suara (TPS).</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">100.186.754 Pemilih .</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">50.160.885 laki-laki</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">50.025.491 perempuan.</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">378 orang yang tak menyebutkan jenis kelaminnya.</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Tingkat partisipasi masyarakat 50-60 %</li>
                    </ul>

            </div>
        </div>        



        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['CATATAN']['title'];?> </span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['CATATAN']['narasi'];?></p>
            </div>
        </div>   

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['PLUSMINUS']['title'];?> </span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PLUSMINUS']['narasi'];?></p>
            </div>
        </div>   

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['PARTISIPASI']['title'];?> </span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PARTISIPASI']['narasi'];?></p>
            </div>
        </div>   

        <h5 class="sub-kanal-title kanal-title-gray-soft black" style="margin-top: 30px;"><span id="perang_sebelumnya">INSTITUSI PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php
            foreach($data['INSTITUSIPENDUKUNG'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <div class="col-xs-4">
                        <img style="max-width: 100%;max-height: 70px;" src="<?php echo $val['img'];?>" class="lazy" alt="<?php echo $val['name'];?>">
                        <h4 style="font-size: .8em;"><?php echo "<span class='text-center black'>".$val['name']."</span>";?></h4>
                    </div>
                </a>
                <?php
            }
            ?>

            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black" style="margin-top: 30px;"><span id="perang_sebelumnya">PARTAI PESERTA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php
            foreach($data['PARTAIPESERTA'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <div class="col-xs-4">
                        <img style="max-width: 100%;max-height: 70px;" src="<?php echo $val['img'];?>" class="lazy" alt="<?php echo $val['name'];?>">
                        <h4 style="font-size: .8em;"><?php echo "<span class='text-center black'>".$val['name']."</span>";?></h4>
                    </div>
                </a>
                <?php
            }
            ?>

            </div>
        </div>
        <div class="clear"></div>




        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=0; $i < 3; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENDUKUNG']['$i']['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['content'];?></p>
            </div>
            <?php
            }
            ?>
        </div>        
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=4; $i < 6; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENDUKUNG']['$i']['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['content'];?></p>
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=0; $i < 3; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG']['$i']['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content'];?></p>
            </div>
            <?php
            }
            ?>
        </div>        
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=4; $i < 6; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG']['$i']['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content'];?></p>
            </div>
            <?php
            }
            ?>
        </div>        

        

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALLERY FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach ($data['FOTO'] as $key => $val) { ?>
                    <div class="col-xs-6 col-sm-3">
                        <a href="<?php echo $val['img'];?>" target="_blank">
                            <img src="<?php echo $val['img'];?>" class="img-responsive" style="height: 147px;width: 100%;margin-bottom: 10px;"/>
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>



