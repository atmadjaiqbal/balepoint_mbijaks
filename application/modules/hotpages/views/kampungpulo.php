<?php 
$data = [
    'NARASI'=>[
        'title'=>'KONTROVERSI TANAH KAMPUNG PULO',
        'narasi'=>'<img src="http://cdn.tmpo.co/data/2015/08/18/id_428624/428624_620.jpg" class="pic">Kamis 20/08/2015 pemrov DKI Jakarta melakukan pembongkaran bangunan di bantaran kali Ciliwung. Upaya Pemrov DKI Jakarta untuk merevitalisasi kali dengan menertibkan rumah dan bangunan milik warga mendapat respon keras dari warga. Warga kampung pulo menolak penggusuran, akhirnya bentrokanpun terjadi antara warga dan aparat. Bahkan Gubernur Ahok menurunkan ratusan polisi untuk mengamankan jalannya proses penggusuran tersebut. 
                    </p><p>Tindakan Gubernur Ahok dianggap terlalu berlebihan dan refresif. Beberapa kalangan menganggap, termasuk dari Komnas HAM menilai cara Pemrov DKI Jakarta menggusur warga melanggar HAM dan tidak sesuai prosedur. Polemik penggusuran tanah di Kampung Pulo menjadi kontroversi.
                    </p><p><img src="http://cdn-2.tstatic.net/tribunnews/foto/bank/images/penggusuran-kampung-pulo_20150820_215926.jpg" class="pic2">Gubernur Ahok berdalih bahwa penggusuran dilakukan untuk menanggulangi banjir yang kerap datang setiap tahun. Selain itu, penggusuran dilakukan karena tanah tersebut tanah Negara dan warga Kampung Pulo akan diberikan fasilitas rumah susun sebagai bentuk ganti rugi. 
                    </p><p>Namun, status kepemilikan tanah di Kampung Pulo menjadi perdebatan terkait ganti rugi yang akan diberikan oleh Pemerintah. Pemerintah DKI Jakarta berpegang pada ketentuan Undang-Undang Nomor 6 Tahun 1960 tentang Peraturan Dasar Pokok-pokok Agraria: bukti kepemilikan adalah sertifikat. Namun warga Kampung Pulo di Jakarta Timur itu merasa hal ini cukup ditunjukkan dengan surat perjanjian jual-beli dan tanda bayar Pajak Bumi dan Bangunan.'
    ],
    'ANALISA'=>[
        'narasi'=>'<img src="http://cdn1-a.production.liputan6.static6.com/medias/940788/big/024778900_1438225857-20150729-Stok-Photo-Ahok4.jpg" class="pic">Kampung Pulo. Kini Kampung Pulo rata dengan tanah akibat digusur oleh Pemrov DKI Jakarta. 
                    </p><p class="font_kecil">Alasan Gubernur Ahok yang meratakan bangunan-bangunan di sekitar kali Ciliwung untuk revitalisasi. 
                    </p><p class="font_kecil">Banjir yang kerap menggenangi Jakarta bersumber di Kampung Pulo. Kawasan tersebut sangat kumuh akibat tumpukan sampah dari warga dan banyaknya bangunan liar yang menghampat aliran sungai.',
        'list'=>[
            ['isi'=>'Menjadi sumber Banjir di Jakarta'],
            ['isi'=>'Tanah Negara dan resapan air.'],
            ['isi'=>'Sebelum keputusan penggusuran diambil, Gubernur Ahok telah berusaha memasyarakatkan proyek normalisasi itu: apa saja yang akan dilakukan dan apa akibatnya bagi sebagian warga di sekitar bantaran Sungai Ciliwung. Sosialisasi sudah dilaksanakan sejak tahun lalu.'],
            ['isi'=>'Pilhan untu menertibkan warga Kampung Pulo sudah tepat karena alasan kemanusiaan. Agar warga tidak lagi menderita ketika banjir datang.'],
            ['isi'=>'Langkah penggusuran diambil sesuai prosedur, yakni dengan melakukan negosiasi terlebih dahulu.'],
            ['isi'=>'Penggusuran tidak dilakukan secara mendadak. Sebelumnya sudah ada peringatan dari Pemrov DKI agar mengosongkan kawasan Kampung Pulo.'],
            ['isi'=>'Pemrov DKI berdalih bahwa daerah tempat tinggal warga merupakan tanah milik Negara. Bukan milik warga Kami.'],
            ['isi'=>'Setiap warga akan menerima penggantian ganti rugi sesuai dengan peraturan Pergub No 19 tahun 2004.'],
            ['isi'=>'Pemrov DKI akan menyediakan rumah susun untuk warga sebagai bentuk ganti rugi.']
        ]                    
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="http://cdn-2.tstatic.net/tribunnews/foto/bank/images/ormas_20150822_175105.jpg" class="pic2">Penggusuran yang dilakukan Pemprov DKI terhadap warga Kampung Pulo,Jatinegara, Jakarta Timur mendapat reaksi yang beragam dari masyarakat. Banyak dari masyarakat mengkritik kebijakan Pemrov DKI tersebut dengan mempertanyakan konsistensi Pemerintah DKI dalam menanggulangi masalah banjir di Jakarta. 
                    </p><p class="font_kecil">Warga Kampung Pulo juga merasa heran dengan klaim Pemrov DKI yang mengakui tanah yang mereka tempati sebagai tanah Negara, padahal warga memiliki sertifkat tanah yang sah. Selain Itu, penanganan bentrokan yang terjadi saat proses penggusura juga mendapat kritikan keras. Pelibatan polisi dan tentara dalam menghalau aksi massa dianggap cara yang sangat refresif. 
                    </p><p class="font_kecil"><img src="http://cdn.klimg.com/merdeka.com/i/w/news/2015/08/25/585860/670x335/video-perdebatan-sengit-ahok-dan-warga-kp-pulo-sebelum-digusur.png" class="pic">Seharusnya, Pemrov DKI melakukan pendekatan yang persuasif agar tidak terjadi korban jiwa. Di lain pihak, Gubernur Ahok sendiri mengaku bahwa relokasi ini masih menyisakan masalah, karena ada warga yang menolak dipindahkan, dan menuntut ganti rugi uang sekaligus rusun. Sedianya, warga akan dipindahkan ke rumah susun sewa Jatinegara Barat.Menurutnya tuntutan ganti rugi uang itu tidak relevan, karena status lahan yang merupakan milik negara. Ia pun menyinggung soal sumber anggaran, bila Pemprov DKI Jakarta harus memenuhi tuntutan ganti rugi uang.'
    ],
    'KRONOLOGI'=>[
        'list'=>[
            ['date'=>'8 Juni 2015','content'=>'pemerintah menyedorkan 786 pasal dalam Rancangan Undang Undang (RUU) Kitab Undang Undang Hukum Pidana (KUHP) ke DPR RI, Salah satu pasal adalah soal pasal penghinaan presiden dan wakil presiden RI.'],
            ['date'=>'3 Agustus 2015','content'=>'secara azas hukum yang berlaku segala Undang-Undang atau pasal yang telah dibatalkan oleh MK itu sudah tak bisa dibahas atau dihidupkan kembali dalam UU, ungkap Aziz Syamsuddin (ketua komisi III DPR RI)'],
            ['date'=>'8 Agustus 2015','content'=>'pasal penghinaan presiden bisa dibatalkan MK. Peluang pasal penghinaan Presiden dimuat dalam Kitab Undang-Undang Hukum Pidana (KUHP) sangat kecil. Demikian disampaikan Mantan Ketua Mahkamah Konstitusi Mahfud MD.'],
            ['date'=>'09 Agustus 2015','content'=>'SBY mengatakan pasal penghinaan presiden bersifat ngaret. SBY mengakui bahwa pasal pencemaran nama baik memang bersifat ngaret. Artinya, kata SBY, memang ada unsur subyektivitas.'],
            ['date'=>'10 Agustus 2015','content'=>'Ketua Mahkamah Konstitusi (MK) Arief Hidayat, mengatakan bahwa putusan MK itu final dan mengikat. Artinya, pasal tersebut takkan pernah lagi diterima sebagai delik aduan.']
        ]
    ],
    'INDIKASI'=>[
        'narasi'=>'<img src="http://usimages.detik.com/customthumb/2015/08/21/10/103225_ekokampungpulo.jpg?w=780&q=90" class="pic">Gubernur DKI Jakarta Basuki `Ahok` Tjahaja Purnama dilaporkan ke Komnas HAM terkait kekerasan yang terjadi saat penggusuran warga Kampung Pulo, Jakarta Timur. Gubernur Ahok dilaporkan oleh warga Kampung Pulo yang diwakili oleh Forum Warga Kota Jakarta (Fakta).
                    </p><p class="font_kecil"><img src="https://img.okezone.com/content/2015/08/20/338/1199829/oknum-satpol-diduga-pukuli-warga-saat-kerusahan-di-kampung-pulo-W8EzKTTzpz.jpg" class="pic2">Dalam investigasi  yang dilakukan oleh Fakta,ditemukan adanya pelanggaran HAM dalam proses penggusuran di Kampung Pulo. Bukti pelanggaran HAM mengacu pada cara-cara penangangan yang tidak sesuai prosedur. Pengerahan ribuan aparat polisi dan Tentara menjadi salah satu acuannya. Adapun soal tindakan kekerasan yang dilakukan oleh aparat menjadi bukti nyata adanya pelanggaran HAM. 
                    </p><p class="font_kecil">Warga dalam aduannya juga mengaku menderita akibat penggusuran tersebut. Gubernur Ahok dinilai semena-mena menggusur rumah dan bangunan yang ada di Kampung Pulo. Akibatnya ratusan kepala keluarga kehilangan tempat tinggal dan penghasilan ekonomi. Kini, warga Kampung Pulo tidak menentu nasibnya.'
    ],
    'LANGGANAN'=>[
        'narasi'=>'<img src="http://dekandidat.com/cms/wp-content/uploads/2015/02/banjir.jpg" class="pic">Kampung Pulo memang kerap menjadi langganan banjir tahunan yang terjadi di Jakarta. Setiap tahun, ketika musim hujan, Kampung Pulo digenani air banjir karena letaknya yang berada di bantaran kali Ciliwung. Banjir yang datang memaksa ratusan kepala keluarga diungsikan ke tempat yang aman. 
                    </p><p class="font_kecil"><img src="http://cms.monitorday.com/statis/dinamis/detail/12325.jpg" class="pic2">Pemrov DKI Jakarta beberapa kali mencari solusi agar banjir tidak terjadi lagi di Jakarta, terutama di Kampung Pulo dengan melakukan terobosan kanal timur. Kanal air tersebut dibuat untuk agar banjir kiriman yang kerap datang ke Jakarta melalui sungi Ciliwung tidak melewati tengah kota tapi pinggiran kota. 
                    </p><p class="font_kecil">Tapi cara tersebut sepertiya tidak efektif  dan tidak menjadi solusi penangan banjir. Pemrov DKI lalu mengambil langkah penggusuran, tetapi cara tersebut justru menimbulkan masalah baru. Warga tidak terima rumah dan bangunan mereka digusur dan bersikukuh untuk tetapa mendiami kawasana tersebut. Akhirnya bentrokan massal terjadi antara warga dan aparat keamananpun terjadi.'
    ],
    'SEJARAH'=>[
        'narasi'=>'<img src="http://rileksmedia.com/content/bataviatempodoler.jpg" class="pic">Kampung Pulo sudah ada sejak zaman Kolonial Belanda. Sejarah Kampung Pulo Jakarta dimulai tahun 1930. Kampung Pulo terletak  di bantaran sungai Ciliwung.dinamai Kampung Pulo karena kawasan tersebut mirip sebuah pulo. 
                    </p><p class="font_kecil">Secara gegrafis, Kampung Pulo berada di sisi utara dan timur Jakarta. Aliran sungai Ciliwung yang mengalir diantara sisi-sisi kampung Pulo berhulu di Bogor. Tidak mengherankan jika hamper setiap musim hujan tiba, kawasan tersebut banjir dan  memaksa warga setempat untuk mengungsi. 
                    </p><p class="font_kecil"><img src="https://fitriwardhono.files.wordpress.com/2012/04/banjir-04.jpg" class="pic2">Etnis Betawi merupakan mayoritas yang mendiami kawasan Kampung Pulo. Namun, sejak tahun 1970-an banyak pendatang yang datang dan menetap di kawasan tersebut. Mereka dari daerah Bogor dan sekitarnya. Sampai sekarang, beragam etnis seperti Tionghoa, Padang, Batak, Arab dan lainnya menjadi warga Kampung Pulo.
                    </p><p class="font_kecil">Pada masa kolonial Belanda, kampung tersebut merupakan bagian dari kawasan Meester Cornelis. Kampung seluas 8.575 hektar tersebut memiliki akar dan nilai sejarah antopologi kultural yang kuat.
                    </p><p class="font_kecil"><img src="http://1.bp.blogspot.com/-43uDyVWN3dE/Vd5wzWS5UQI/AAAAAAAADNI/1M3bGoeYO2E/s400/images%2B%25288%2529.jpg" class="pic">Selama empat abad, Meester Cornelis Jatinegara adalah salah satu pusat fungsional pertumbuhan Kota Jakarta. Fakta historis tersebut berhasil dihimpun Ivana Lee, pendamping warga dari LSM Ciliwung Merdeka yang pernah melakukan penelitian di wilayah tersebut.
                    </p><p class="font_kecil">Keberadaan sejumlah situs budaya religi dan tipologi arsitektur bangunan tempo dulu juga menjadi kekhasan Kampung Pulo.'
    ],
    'PROFIL'=>[
        'narasi'=>'Secara geografis wilayah kampung pulo terdiri dari dua rukun warga dan berada di sisi selatan, timur dan utara kali ciliwung yang berhulu dari kota bogor.',
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']
        ],
        'institusi'=>[
            ['page_id'=>'pemerintahprovinsipemprovdki54c1c6c8c3ef3'],
            ['page_id'=>'dprddki541a5b6fb78db'],
            ['page_id'=>'RMP']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c']
        ],
        'institusi'=>[
            ['page_id'=>'komnasham54c1eac938164'],
            ['page_id'=>'lbhjakarta55dde9e5dac01'],
            ['page_id'=>'frontpembelaislam5317d72f772e0'],
            ['page_id'=>'ciliwungmerdekacm55dded96d8085'],
            ['page_id'=>'gerakanlawanahok55ddf49baf66b'],
            ['page_id'=>'Fakta']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Basuki Tjahja Purnama','jabatan'=>'Gubernur DKI','img'=>'http://media.forumkeadilan.com/2013/07/Basuki-Tjahaja-Purnama-Ahok-metrotvnews.com_.jpg','url'=>'http://www.bijaks.net/aktor/profile/basukitjahajapurnama50f600df48ac5','content'=>'Itu jadinya bukan hanya seperti rusun loh ya, ini sudah seperti kayak apartemen. Kalau kamu lihat itu kalau dijual sekarang saya kira Rp400 juta orang merem juga mau beli'],
        ['from'=>'H.M Jusuf Kalla','jabatan'=>'Wakil Presiden RI','img'=>'http://metroonline.co/wp-content/uploads/2014/02/Jusuf-Kalla.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'Jadi sama sekali Pemerintah tidak bermaksud merugikan rakyat, tapi justru untuk memperbaiki kehidupan (rakyat), dan itu bukan hanya Kampung Pulo, di Tanjung Priok akan dibangun dulu. Nanti pindah semuanya'],
        ['from'=>'Ferry Mursyidan','jabatan'=>'Menteri Agraria','img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/05/29/400271/7zZI1rj5ti.jpg?w=668','url'=>'http://www.bijaks.net/aktor/profile/ferrymursyidanbaldan511b4ad3edaa1','content'=>'Di situ saja sudah masalah, maka ditata dalam penataan kawasan untuk membebaskan Jakarta dari bahaya banjir'],
        ['from'=>'Djarot Saiful Hidayat','jabatan'=>'Wakil Gubernur DKI Jakarta','img'=>'http://www.satuharapan.com/uploads/pics/news_39528_1434648302.jpg','url'=>'http://www.bijaks.net/aktor/profile/djarotsaifulhidayat5406bfc7406d7','content'=>'Kami sudah siapkan tempatnya, dan itu sangat layak. Kami punya program memberikan perumahan yang layak dan manusiawi mereka'],
        ['from'=>'Prasetyo Edi Marsudi','jabatan'=>'Ketua DPRD DKI','img'=>'http://cdn-2.tstatic.net/pontianak/foto/bank/images/prasetyo-edi-marsudi.jpg','url'=>'http://www.bijaks.net/aktor/profile/prasetyoedimarsudi51d223cb94d7e','content'=>'Rusunnya bagus dan nyaman, seharusnya tidak ada yang menolak. Mungkin karena perubahan pola hidup saja. Diajak hidup yang lebih manusiawi masa tidak mau?'],
        ['from'=>'Saefullah','jabatan'=>'Sekda DKI Jakarta','img'=>'http://assets.kompas.com/data/photo/2014/06/17/0958406saefullah1p.jpg','url'=>'http://www.bijaks.net/aktor/profile/saefullah54c09e806b22b','content'=>'Program ini harus terus, karena untuk kepentingan bersama, bukan untuk kepentingan siapa-siapa. Tujuan ini untuk angkat penyakit Jakarta, kalau tidak mulai dari sekarang kapan lagi terlebih situasi dengan bagus'],
        ['from'=>'Bambang Musyawardana','jabatan'=>'Walikota Jakarta Timur','img'=>'https://pbs.twimg.com/media/B7claewCQAQU71V.jpg','url'=>'http://www.bijaks.net/aktor/profile/bambangmusyawardana54c1b5fe35af6','content'=>'Kami apresiasi, molornya sudah terlalu panjang, karena itu atas perintah gubernur kita akan reloksi, kurang lebih ada 520 rumah yang akan kita relokasi'],
        ['from'=>'Maruar Sirait','jabatan'=>'Ketum RMP','img'=>'http://www.luwuraya.net/wp-content/uploads/2012/03/maruarar-sirait2.jpg','url'=>'http://www.bijaks.net/aktor/profile/Maruarar','content'=>'Kalau kebijakan bagus, kita harus jujur bilang bagus. Kita dukung Pak Ahok. Di saat yang sama, warga juga harus ikut berpartisipasi dalam mendukung kebijakan pemerintah yang benar-benar pro-rakyat.'],
        ['from'=>'Kombes Pol Umar Faroq','jabatan'=>'Kapolrestro Jakarta Timur','img'=>'http://www.rmoljakarta.com/images/berita/thumb/thumb_357794_07054009062015_kapolres_metro_jakarta_timur.jpg','url'=>'','content'=>'Makanya kita bertindak represif ya. Kami sebenarnya tidak menghendaki ini. Tapi ini sudah tugas dan sewajarnya kami lakukan']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Syarif','jabatan'=>'Sekretaris Komisi A‎ DPRD DKI Jakarta','img'=>'http://www.aktual.com/wp-content/uploads/2015/06/Syarief-Hasil-Angket-Akan-Selesai-Sebelum-25-Maret-Nanti.jpg','url'=>'','content'=>'Kami mengutuk aparat. Kenapa tidak bisa melakukan penggusuran dengan cara baik-bai‎k kepada warga Kampung Pulo?'],
        ['from'=>'Puan Maharani','jabatan'=>'Menko PMK','img'=>'http://www.seputarrakyat.com/wp-content/uploads/2014/10/Puan-Maharani.jpg','url'=>'http://www.bijaks.net/aktor/profile/puanmaharani5104bbe40ed12','content'=>'Itu bisa dilakukan dengan lebih persuasif dan mengajak lebih berbicara dengan lebik baik. Dan saya rasa gubernur akan melakukan hal itu karena saya dengar ke depannya insyaallah enggak ada lagi hal-hal seperti itu'],
        ['from'=>'JJ Rizal','jabatan'=>'Sejarawan Betawi','img'=>'http://cdnimage.terbitsport.com/image.php?width=650&image=http://cdnimage.terbitsport.com/imagebank/gallery/large/20150821_010534_harianterbit_jj_rizal-2.jpg','url'=>'http://www.bijaks.net/aktor/profile/jjrizal55de0182e033d','content'=>'Normalisasi yang terjadi bukan normalisasi tapi betonisasi'],
        ['from'=>'Sandyawan Sumardi','jabatan'=>'Direktur Ciliwung Merdeka','img'=>'http://www.satuharapan.com/uploads/pics/news_63_1400069596.JPG','url'=>'http://www.bijaks.net/aktor/profile/ignatiussandyawansumardi55ddfe2606236','content'=>'Karena warga dianggap tidak punya surat-surat tanah sama sekali sehingga tidak ada ganti rugi apapun'],
        ['from'=>'Muhammad Nur Khoirun','jabatan'=>'Komisioner Komnas HAM','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2014--08--75123-komisioner-komnas-ham-selidiki-pemukulan-pendukung-prabowo-datangi-polda.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadnurkhoiron51a55eeb05f62','content'=>'Kalau merampas hak kepemilikan seseorang secara paksa, itu sudah melanggar HAM, tapi kalau dari sisi masyarakat yang membakar alat berat, itu masuknya pada pelanggaran hukum'],
        ['from'=>'Tegar Putuhena','jabatan'=>'Ketua Lawan Ahok','img'=>'https://0.academia-photos.com/16029549/4348171/5049555/s200_tegar.putuhena.jpg_oh_dc9ed1cc3e5e9673c24d8209575edb7b_oe_54702ef5___gda___1417628574_45cdd677af5285287f96def887375232','url'=>'','content'=>'Gerakan Lawan Ahok kami buat setelah rapat maraton tadi malam dengan aktivis mahasiswa dan masyarakat. Terkait tindak kekerasan yang dilakukan Ahok terhadap Kampung Pulo'],
        ['from'=>'Nirwono','jabatan'=>'Pengamat Tata Kota','img'=>'http://gambar.radarpena.com/mei/images/nirwono%20yoga1.jpg','url'=>'','content'=>'Kalau memang berniat mengatasi banjir, jangan hanya rumah-rumah orang miskin saja yang ditertibkan. Tertibkan juga pengembang-pengembang nakal yang membangun properti di atas daerah resapan'],
        ['from'=>'Fadhli Nasution','jabatan'=>'Kuasa hukum Organisasi Masyarakat Lawan Ahok','img'=>'http://www.rmol.co/images/berita/normal/450623_01325810072015_fadli_nasution1.jpg','url'=>'','content'=>'Kami akan laporkan tindakan Ahok yang telah melanggar HAM ini ke Pengadilan internasional sebab, penggusuran Kampung Pulo telah mendunia'],
        ['from'=>'Azas Tigor Nainggolan','jabatan'=>'Ketua FAKTA','img'=>'http://beritatrans.com/cms/wp-content/uploads/2014/12/IMG_20141202_115833_edit.jpg','url'=>'http://www.bijaks.net/aktor/profile/azastigornainggolan52c61d2ab63a5','content'=>'Saya kira jumlah aparat yang diturunkan terlalu berlebihan karena sebetulnya warga tidak pernah menolak direlokasi, namun ada dialog yang belum tuntas'],
        ['from'=>'Habib Salim Alatas (Habib Selon)','jabatan'=>'Tokoh FPI','img'=>'http://cdn-2.tstatic.net/tribunnews/foto/images/preview/20120516_Foto_Habib_Salim_Alatas_FPI__1929.jpg','url'=>'http://www.bijaks.net/aktor/profile/habibsalimalatas51edeb2ba5db8','content'=>'Harusnya pemerintah tanya maunya warga apa, ya diomongin. Ngopi-ngopi bareng kan biar lebih enak. Biar Mufakat juga kayak Tanah Abang waktu itu'],
        ['from'=>'LBH Jakarta','jabatan'=>'Atika Yuanita Paraswaty','img'=>'http://assets.kompas.com/data/photo/2015/08/26/1250015image780x390.jpg','url'=>'','content'=>'Banyak pelanggaran prosedur yang terjadi dalam proses penggusuran. Itu karena mereka (Pemprov) tidak punya aturan yang jelas. Setidaknya harus ada SOP (standard operating procedure)'],
        ['from'=>'Tommy Soeharto','jabatan'=>'putra sulung Soeharto','img'=>'http://rmol.co/images/berita/normal/948857_05481002062015_tommy_soeharto','url'=>'http://www.bijaks.net/aktor/profile/hutomomandalaputra-tommy504d5dedbc8cd','content'=>'Alasan rakyat bertahan tidak perlu bertanya kenapa? Ada apa? Tentu saja mereka mempertahankan Hak hidup mereka, Mempertahankan sumber Penghidupan keluarga mereka, tempat tinggal mereka..Orang tidak sekolah juga tau..!!!']
    ],
    'VIDEO'=>[
        ['id'=>'2dwqpJ-I6jU'],
        ['id'=>'Ji1mbUvsdXw'],
        ['id'=>'ktBRpWVWi20'],
        ['id'=>'ktBRpWVWi20'],
        ['id'=>'SWCD00aSKBs'],
        ['id'=>'URiXU8bwGb4'],
        ['id'=>'43F75Vj1rL8'],
        ['id'=>'82i01tPZXFc'],
        ['id'=>'GT_buT3UO1Q'],
        ['id'=>'P3C4R8H0xws'],
        ['id'=>'0503f0spBl8'],
        ['id'=>'Au_F7QhIU5Q']
    ],
    'FOTO'=>[
        ['img'=>'http://cms.monitorday.com/statis/dinamis/detail/12620.jpg'],
        ['img'=>'http://www.indoberita.com/wp-content/uploads/2015/08/Kampung-Pulo.jpg'],
        ['img'=>'http://i.ytimg.com/vi/bujUmRIsmRs/hqdefault.jpg'],
        ['img'=>'http://2.bp.blogspot.com/-CI9uqWAnMC0/VdcBMxNd6yI/AAAAAAAAA50/VpAiVnUWMB8/s320/hari-ini-kampung-pulo-digusur-KUwT5KCKTw.jpg'],
        ['img'=>'https://i.ytimg.com/vi/a3u52RvOoaI/hqdefault.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2014/08/28/id_319449/319449_620.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/963798/big/068847500_1440307279-20150823-penggusruan_Kampung_Pulo-Jakarta.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/962859/big/062511500_1440161444-20150821-Kampung_Pulo-Jakarta.jpg'],
        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/961141/big/059002000_1440052261-20150820-Kampung-Pulo4.jpg'],
        ['img'=>'http://i.ytimg.com/vi/MGtRyyVep7k/hqdefault.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/08/20/422820/AQAWFUcZgk.jpg?w=668'],
        ['img'=>'http://i.ytimg.com/vi/SQbGQ831AzI/hqdefault.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/banjarmasin/foto/bank/images/gerakan-lawan-ahok_20150824_211012.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/08/24/1057531image780x390.jpg'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/08/25/170/1036908/si-pitung-sambangi-posko-gerakan-lawan-ahok-oiM.jpg'],
        ['img'=>'http://imgix.production.liputan6.static6.com/medias/921070/original/038528300_1436246314-ahok.jpg?w=673&h=373&fit=crop&crop=faces'],
        ['img'=>'http://assets.rappler.com/90194ED25D4346E7A603C9E01BAC06E4/img/2263B171BE9B486E8D4427A1B914619E/kampung_pulo_rplr6_2263B171BE9B486E8D4427A1B914619E.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/962716/big/007600500_1440155842-20180821-Kampung-Pulo-Jakarta.jpg'],
        ['img'=>'http://assets.rappler.com/612F469A6EA84F6BAE882D2B94A4B421/img/3CAF6203D96C418294132A7049B3AC41/img_9480_3CAF6203D96C418294132A7049B3AC41.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/08/20/160099/CVgB2fiDml.jpg?w=668'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/perumahan-di-kampung-pulo-jatinegara-yang-berada-di-bantaran-_150823165228-307.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/07/28/151317/WwEQBKHL7M.jpg?w=668'],
        ['img'=>'http://assets.tokohindonesia.com/berita/ti_1813469KampungPulo231440068981-preview780x390.jpg'],
        ['img'=>'http://www.infonitas.com/public/media/images/thumb/2015/08/25/2015-08-25-11-37-39_JJ-Rizal-dan-Ahok_thumb_634_350_c.jpg'],
        ['img'=>'http://smeaker.com/nasional/wp-content/uploads/2015/08/Tak-Gentar-JJ-Rizal-Terima-Tantangan-Debat-Ahok-Tommy-Ahok-Bukan-Tukang-%E2%80%9CGusur%E2%80%9D1.jpg'],
        ['img'=>'http://i.ytimg.com/vi/ItFf4GYRGzU/0.jpg'],
        ['img'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcSjfWFhZ201wn578PRuQnMiTfQVb8PEjg5aCVTQoMe3tjH6DqQ'],
        ['img'=>'https://img.okezone.com/content/2015/08/20/338/1199561/relokasi-warga-kampung-pulo-dki-siapkan-1-100-unit-rusun-eLPp8zBEUQ.jpg'],
        ['img'=>'http://assets.kompas.com/data/2012/todaysphoto/upload/photo/808/RusunKampungPulo221440243141_preview.jpg'],
        ['img'=>'http://www.infonitas.com/public/media/images/thumb/2015/08/20/2015-08-20-13-53-54_kampung%20pulo%20rusun_thumb_634_350_c.jpg']
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }    
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

    .gallery li {
        display: block;
        float: left;
        height: 50px;
        margin-bottom: 7px;
        margin-right: 0px;
        width: 25%;
        overflow: hidden;
    }
    .gallery li a {
        height: 100px;
        width: 100px;
    }
    .gallery li a img {
        max-width: 97%;
    }

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $(".gallery").lightGallery();
    $(".gallery2").lightGallery();
})
</script>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/kampungpulo/top_kcl.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">DALIH PENGGUSURAN</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['ANALISA']['narasi'];?></p>
                <ol style="list-style-type: circle;margin-left: -10px;">
                    <?php
                    foreach ($data['ANALISA']['list'] as $key => $val) {
                        echo "<li style='margin-right: 20px;'>".$val['isi']."</li>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PRO-KONTRA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROKONTRA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PROFIL KAMPUNG PULO</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://britabagus.com/wp-content/uploads/2015/08/resapanair3889p.jpg" style="width: 50%;margin: 0 auto;display: block;">
                    <p style="text-align: justify;"><?php echo $data['PROFIL']['narasi'];?></p>
                    <ul style="margin-right: 20px;">Selain itu, kampung pulo berbatasan dengan kawasan Meester Cornelis di sisi barat.
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Luas Wilayah: 8.575 hektar</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Jumlah Warga : 3.809 KK</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Mushollah Tertua bernama Mushollah Al-Awwabin</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Situs: makam Kyai Lukman nul, Hakim/Datuk (sebelum 1930), makam, Habib Said.</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Etnis Asli : Betawi</li>
                        <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Etnis Pendatang : Bugis, Tionghoa, Padang, Batak, Arab</li>
                    </ul>
            </div>
        </div>
        <div class="clear"></div>

        <!-- <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KRONOLOGI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <ol style="list-style-type: circle;margin-left: -10px;">
                    <?php
                    foreach ($data['KRONOLOGI']['list'] as $key => $val) {
                        //echo "<li style='margin-right: 20px;font-weight: bold;'>".$val['date']."</li><p>".$val['content']."</p>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div> -->

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">TERINDIKASI MELANGGAR HAM</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['INDIKASI']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">LANGGANAN BANJIR</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['LANGGANAN']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SEJARAH KAMPUNG PULO</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SEJARAH']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <h5 style="padding-top: 15px;">PARTAI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>
        <div class="black boxcustom2" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'RMP'){
                    $photo = 'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/dsc_0179jpg_20150825_163325.jpg';
                    $pageName = 'Relawan Merah Putih (RMP)';
                }
                elseif($val['page_id'] == 'dprddki541a5b6fb78db'){
                    $photo = 'http://www.jpnn.com/picture/normal/20150709_232117/232117_54378_dprd_dki.jpg';
                    $pageName = 'DPRD DKI';
                }
                elseif($val['page_id'] == 'pemerintahprovinsipemprovdki54c1c6c8c3ef3'){
                    $photo = 'http://www.aktualpost.com/wp-content/uploads/2015/05/LOGO-DKI.jpg';
                    $pageName = 'Pemrov DKI';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARTAI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'Fakta'){
                    $photo = 'http://cdn1-a.production.liputan6.static6.com/medias/862037/big/093319200_1430043292-Demo-Anti-Rokok.jpg';
                    $pageName = 'Fakta';
                }
                elseif($val['page_id'] == 'komnasham54c1eac938164'){
                    $photo = 'http://bantenpos.co/wp-content/uploads/2014/10/LOGO-KOMNAS-HAM.png';
                    $pageName = 'KOMNAS HAM';
                }
                elseif($val['page_id'] == 'lbhjakarta55dde9e5dac01'){
                    $photo = 'http://images.hukumonline.com/frontend/lt5499ec4a7fbb2/lt5499ed062fc47.jpg';
                    $pageName = 'LBH Jakarta';
                }
                elseif($val['page_id'] == 'ciliwungmerdekacm55dded96d8085'){
                    $photo = 'https://ciliwungmerdekajakarta.files.wordpress.com/2012/05/cm-y-copy.jpg?w=627&h=1672';
                    $pageName = 'Ciliwung Merdeka';
                }
                elseif($val['page_id'] == 'gerakanlawanahok55ddf49baf66b'){
                    $photo = 'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/ormas_20150822_175105.jpg';
                    $pageName = 'Gerakan Lawan Ahok';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['FOTO'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                        <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>            
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>