<?php 
$data = [
    'NARASI'=>[
        'title'=>'KONTROVERSI PASAL PENGHINAAN PRESIDEN',
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2014/08/28/0914114sby-jokowi-4780x390.jpg" class="pic">Presiden Joko Widodo (Jokowi) melalui pemerintahannya, menyedorkan 786 pasal dalam Rancangan Undang Undang (RUU) Kitab Undang Undang Hukum Pidana (KUHP) ke DPR RI. Dari ratusan pasal yang disedorkan, Jokowi memasukkan pasal mengenai pasal penghinaan presiden dan wakil presiden.
                    </p><p>Usulan pasal tesebut, terdapat pada pasal 263 ayat (1) RUU KUHP dan diperluas lagi dengan pasal 264 RUU KUHP. Pasal ini, sebenarnya sudah telah dicabut oleh Mahkamah Konstitusi (MK) pada pemerintahan Susilo Bambang Yudhiono (SBY) tahun 2006 silam. Pasal ini dianggap tidak mempunyai batasan yang jelas, sehingga menimbulkan ketidakpastian hukum. Saat itu pasal yang dihapus berbunyi “Penghinaan dengan sengaja terhadap Presiden dan Wakil Presiden dihukum dengan hukuman penjara selama-lamanya enam tahun atau denda sebanyak-banyaknya Rp4.500.”
                    </p><p><img src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTXISaerub_S4-bhR5AAKnBcrKgp8ybAc_tV09PBP_32_5IpJS5" class="pic2" style="height: 140px;">Pasal-pasal yang pernah disetip Mahkamah Konstitusi kembali menyelinap dalam rancangan undang-undang setebal 272 halaman yang dikirim pemerintah ke Dewan Perwakilan Rakyat pada 8 Juni lalu. Lewat RUU KUHP pemerintah tampaknya ingin menghidupkan lagi pasal penghinaan terhadap presiden dan wakilnya itu.
                    </p><p>Meski, masih berupa ajuan RUU tapi wacana ini terus bergulir. Pasal kontroversi ini, mempunyai sejarah panjang semenjak zaman kolonial sehingga disebut sebagai pasal karet. Pasal yang sudah menjadi kuburan dalam arsip MK, seolah-olah kembali ingin dihidupkan lagi menjadi "zombie". Pasal ini disebut akan menyalahi konstitusional, karena mengacu pada putusan MK yang sudah mengikat dan final, apalagi jelas dalam hal ini, MK selalu konsisten dengan setiap putusannya.'
    ],
    'ANALISA'=>[
        'narasi'=>'<img src="https://thephenomena.files.wordpress.com/2014/10/misteri_jokowi_dan_tukang_sate_membentuk_119.png" class="pic">Dengan alasan melindungi harkat dan martabat presiden dan wakil presiden, maka pasal yang pernah dibatalkan oleh Mahkamah Konstitusi (MK) perlu dibangkitkan kembali. Padahal dalam konsistensi putusan MK, setiap norma yang sudah diputuskan itu tak bisa diajukan kembali, dan akan dianggap perlawanan konstitusi.
                    </p><p class="font_kecil">Namun, untuk mencegah tafsir yang berlebihan, maka perlunya untuk mendudukkan pasal ini. "Ada tiga terminologi yang mesti dijelaskan", menurut pakar Hukum Pidana Asep Iwan Irawan. Pertama, ada kata presiden, kedua ada kata harkat martabat orang, dan ada kata penghinaan. Jika ini disebut delik aduan maka salah besar, karena pasal ini sudah mati dan selesai. Sementara kata penghinaan, terminologinya adalah "merendahkan" menyerang martabat orang lain.
                    </p><p class="font_kecil">Pasal ini, memiliki tafsir yang berbeda. Yang perlu disamakan persepsinya adalah mendefinisikan apa itu penghinaan dan apa itu kritik terhadap presiden. Persamaan hukum tidak memandang bulu, oleh karena itu siapapun yang berbuat salah harus dihukum. Sejumlah pihak pun menyayangkan, kalau pasal ini dihidupkan lagi, maka sangat berbahaya bagi demokrasi Indonesia. Rakyat sebagai agen of kontrol tidak akan berfungsi lagi, baik itu LSM, Pers dan sejumlah demonstrasi yang dianggap berbahaya akan dijeratkan dengan pasal ini.'
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRHoQzXpuy39z-ViBzFMzvqwKR6mm5H8YXRDC4tGYBfw8mVZ-e_" class="pic2">Pendukung pasal ini berdalih, seorang presiden wajib dilindungi harkat dan martabatnya oleh UU karena dalam kapisitasnya sebagai kepala negara. Seorang presiden adalah berkedudukan juga sebagai lambang negara. Hal ini diamini oleh politisi Demokrat, Ruhut Sitompul, dia menganggap pasal ini diperlukan untuk mencegah presiden sebagai kepala negara dipermalukan.
                    </p><p class="font_kecil">Di sisi lain, tanpa penjelasan dan batasan yang jelas tentang kategori penghinaan maka pasal ini bisa berpotensi menjadi pasal karet yang bisa memberangus demokrasi di Indonesia. Ahli Hukum dan juga mantan ketua MK Jimly Ashiqqie, menaruh khawatiran jika pasal penghinaan terhadap presiden dihidupkan lagi, maka budaya feodal yang ada di Indonesia bakal hidup kembali. Para penegak hukum bisa saja terlalu sensitif dalam bertindak, sehingga kalau kelewatan dapat merusak kebebasan berpendapat.
                    </p><p class="font_kecil"><img src="http://news.bijaks.net/uploads/2015/08/Mau-DORRR-Kepala-Penghina-Presiden-Hendropriyono-Dinilai-Jebak-Jokowi-AM-Hendropriyono-Bijaks-282x158.jpg" class="pic">Mendudukkan presiden sebagai lambang negara pun juga dibantah sebagai theory lama. MK menyatakan pasal penghinaan presiden  jelas meniadakan prinsip persamaan di depan hukum, mengurangi kebebasan berekspresi dan berpendapat, serta mengurangi kebebasan mendapat informasi dan prinsip kepastian hukum. Secara khusus, MK menegaskan perlunya pembaruan hukum sehingga RUU KUHP mendatang tidak boleh memuat nilai serupa seperti yang terdapat dalam Pasal 34, 136 bis, dan 137 yang telah dihapus tersebut.
                    </p><p class="font_kecil">Ketentuan ini sama dengan pasal 134 KUHP yang telah dinyatakan tidak mengikat oleh Mahkamah Konstitusi dalam putusannya No. 13-22/PUU-IV/2006. Dalam pertimbangannya Mahkamah Konstitusi menyatakan bahwa RKUHP harus tidak lagi memuat pasal-pasal yang isinya sama atau mirip dengan Pasal 134, Pasal 136 bis dan Pasal 137 KUHP. Dengan demikian maka Pasal 265 RKUHP ini menurut Mahkamah Konsituti bertentangan dengan konstitusi.'
    ],
    'KRONOLOGI'=>[
        'list'=>[
            ['date'=>'8 Juni 2015','content'=>'pemerintah menyedorkan 786 pasal dalam Rancangan Undang Undang (RUU) Kitab Undang Undang Hukum Pidana (KUHP) ke DPR RI, Salah satu pasal adalah soal pasal penghinaan presiden dan wakil presiden RI.'],
            ['date'=>'3 Agustus 2015','content'=>'secara azas hukum yang berlaku segala Undang-Undang atau pasal yang telah dibatalkan oleh MK itu sudah tak bisa dibahas atau dihidupkan kembali dalam UU, ungkap Aziz Syamsuddin (ketua komisi III DPR RI)'],
            ['date'=>'8 Agustus 2015','content'=>'pasal penghinaan presiden bisa dibatalkan MK. Peluang pasal penghinaan Presiden dimuat dalam Kitab Undang-Undang Hukum Pidana (KUHP) sangat kecil. Demikian disampaikan Mantan Ketua Mahkamah Konstitusi Mahfud MD.'],
            ['date'=>'09 Agustus 2015','content'=>'SBY mengatakan pasal penghinaan presiden bersifat ngaret. SBY mengakui bahwa pasal pencemaran nama baik memang bersifat ngaret. Artinya, kata SBY, memang ada unsur subyektivitas.'],
            ['date'=>'10 Agustus 2015','content'=>'Ketua Mahkamah Konstitusi (MK) Arief Hidayat, mengatakan bahwa putusan MK itu final dan mengikat. Artinya, pasal tersebut takkan pernah lagi diterima sebagai delik aduan.']
        ]
    ],
    'PASAL'=>[
        'narasi'=>'<img src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSlCEMUkVwkGUas1Q8vDxIAJOMmQwns0jVtssGgddXdvqQfUPRbaw" class="pic">Memberikan kebebasan atas kritik otokritik, demi terciptanya perbaikan sistem merupakan keniscayaan dalam demokrasi. Namun, ketika kebebasan berpendapat dikriminalisasi dengan ancaman dugaan pencemaran nama baik, hal itu dapat mengganggu demokrasi yang sudah mulai tumbuh sejak era reformasi bergulir.
                    </p><p class="font_kecil">Kebebasan berekspresi adalah hal yang patut dalam demokrasi, hal tersebut merupakan instrumen kontrol terhadap kekuasaan. Apabila pasal ini diberlakukan maka banyak yang akan dipenjarakan seperti pers, komedian, dan aktivis bila memberitakan atau menyiarkan sesuatu yang tidak sesuai dengan pemerintah.'
    ],
    'SEJARAH'=>[
        'narasi'=>'<img src="https://izzawa.files.wordpress.com/2012/03/gaba2.jpg" class="pic">"KUHP yang selama ini digunakan pemerintah Republik Indonesia merupakan warisan era kolonial Belanda, yaitu Wetboek van Strafrecht (WvS) 1915 Nomor 732. WvS Belanda tersebut diberlakukan di Indonesia berdasarkan UU Nomor 1 Tahun 1946 tentang Peraturan Hukum Pidana juncto UU Nomor 73 Tahun 1958 tentang Menyatakan Berlakunya UU Nomor 1 Tahun 1946 RI tentang Peraturan Hukum Pidana untuk Seluruh Wilayah Republik Indonesia dan Mengubah Undang-Undang Hukum Pidana.
                    </p><p class="font_kecil">Kala itu pasal penghinaan digunakan untuk melindungi pemerintah kolonial Hindia Belanda dari kritikan atau serangan para pejuang kemerdekaan RI. Pasca kolonialisme Belanda, pasal tersebut kerap digunakan pemerintah untuk membungkam para aktivis atau lawan politik yang melontarkan kritik lewat beragam media, baik lisan maupun tulisan. Cara itu –dengan memanfaatkan tangan polisi dan jaksa– terbilang efektif.
                    </p><p class="font_kecil">Pasal itu berjalan dengan logika bahwa presiden, wakil presiden, dan lembaga adalah simbol negara yang harus dijaga martabatnya. Posisi jabatan-jabatan itu dianggap tidak memungkinkan bertindak sebagai pengadu sehingga siapapun penghina presiden dapat langsung diproses penegak hukum.
                    </p><p class="font_kecil"><img src="http://cdn0-a.production.liputan6.static6.com/thumbnails/822879/big/015008000_1425600003-Kontroversi-Hukuman-Mati.jpg" class="pic2">Berlakunya pasal itu dinilai merugikan banyak orang, terutama pada Orde Baru. Aktivis Sri Bintang Pamungkas misalnya divonis 10 bulan penjara karena terlibat demo anti-Soeharto di Jerman, April 1995.
                    </p><p class="font_kecil">Ada pula Nanang dan Mundzakir yang didakwa satu tahun penjara karena menginjak foto Megawati saat berdemo di depan Istana Negara pada 2003, serta I Wayan Suardana yang dihukum enam bulan penjara karena membakar foto Susilo Bambang Yudhoyono dalam aksi unjuk rasa menolak kenaikan bahan bakar minyak tahun 2005.
                    </p><p class="font_kecil">Kasus-kasus itu berhenti saat Mahkamah Konstitusi pada 6 Desember 2006 memutuskan delik atau tindak pidana penghinaan terhadap kepala negara yaitu Pasal 134, 136 bis, dan 137 KUHP bertentangan dengan konstitusi sehingga tidak mempunyai kekuatan hukum mengikat.'
    ],
    'PROFIL'=>[
        'narasi'=>'Dalam Pasal 263 RUU KUHP ayat 1 yang disiapkan pemerintah disebutkan bahwa "Setiap orang yang di muka umum menghina presiden atau wakil presiden dipidana dengan pidana penjara paling lama 5 (lima) tahun atau pidana denda paling banyak Kategori IV".
                    </p><p class="font_kecil">Dalam ayat selanjutnya ditambahkan, "Tidak merupakan penghinaan jika perbuatan sebagaimana dimaksud pada ayat (1) jelas dilakukan untuk kepentingan umum atau pembelaan diri".
                    </p><p class="font_kecil">Aturan itu diperluas melalui Pasal 264 RUU KUHP yang berbunyi, "Setiap orang yang menyiarkan, mempertunjukkan, atau menempelkan tulisan atau gambar sehingga terlihat oleh umum, atau memperdengarkan rekaman sehingga terdengar oleh umum, yang berisi penghinaan terhadap Presiden atau Wakil Presiden dengan maksud agar isi penghinaan diketahui atau lebih diketahui umum, dipidana dengan pidana penjara paling lama lima tahun atau pidana denda paling banyak Kategori IV.”',
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaibulanbintang52155330a9408'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227']
        ],
        'institusi'=>[
            ['page_id'=>'sekretariatjenderalkemenkumham535dbf662045e'],
            ['page_id'=>'bin'],
            ['page_id'=>'soksi']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaidemokrat5119a5b44c7e4']
        ],
        'institusi'=>[
            ['page_id'=>'mahkamahkonstitusi51cf91379d144'],
            ['page_id'=>'ciia'],
            ['page_id'=>'unpar'],
            ['page_id'=>'universitasnusacendana54e5a8806f32c'],
            ['page_id'=>'kontras531c20d0d0f90'],
            ['page_id'=>'indonesianpolicewatch53df3ee0395d1'],
            ['page_id'=>'konfederasiserikatpekerjaindonesia55c9a179d59df']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Joko Widodo','jabatan'=>'Presiden RI','img'=>'http://rmol.co/images/berita/normal/471192_04185709072015_jokowi_smile_1','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'Namanya juga rancangan, terserah di Dewan dong. Itu rancangan saja kok ramai'],
        ['from'=>'Jusuf Kalla','jabatan'=>'wakil presiden RI','img'=>'https://upload.wikimedia.org/wikipedia/id/1/18/Jusuf_Kalla_PMI.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'Kalau (orang) memaki-makai atau menghina Presiden, tentu fungsi pemerintahan juga terkena. Jadi wajar saja (pasal itu dihidupkan)'],
        ['from'=>'Ruhut Sitompul','jabatan'=>'anggota komisi III DPR RI','img'=>'http://cdn.tmpo.co/data/2013/09/24/id_222734/222734_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/draevakusumasundarimamde50f91dd8e99b3','content'=>'Apapun presiden itu simbol negara di negara demokrasi saja tidak begitu'],
        ['from'=>'Eva Sundari','jabatan'=>'politisi senior PDIP','img'=>'http://www.rmol.co/images/berita/normal/874951_10592610042015_Eva_K_Sundari.jpg','url'=>'http://www.bijaks.net/aktor/profile/amhendropriyono51886cc4a0b72','content'=>'Ini (pasal penghinaan terhadap presiden) bukan untuk Jokowi ya karena beliau sih cuek'],
        ['from'=>'Patrice Rio Capela','jabatan'=>'Sekjend NasDemtan','img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQhFcPTR4IQILj9aNKOy09GRT93PZUKoQxA1KcdwgfF54aMGMczEA','url'=>'http://www.bijaks.net/aktor/profile/ruhutpoltaksitompulsh50f91f7018b5c','content'=>'Itu bukan berarti anti-kritik, seperti SBY diidentikkan dengan sapi atau kerbau, itu tidak boleh'],
        ['from'=>'Yasonna Laoly','jabatan'=>'menkunham','img'=>'http://www.globalindonesianvoices.com/wordpress/wp-content/uploads/2014/12/Yasona-Laoly.jpg','url'=>'http://www.bijaks.net/aktor/profile/dryassonnahlaolyshmsc50f8bfa462238','content'=>'Jangankan presiden, setiap warga masyarakat pun berhak menuntut untuk mendapatkan proses hukum yang sesuai, karena itu adalah hak setiap individu'],
        ['from'=>'Yusril Mahendra','jabatan'=>'ketua umum PBB','img'=>'http://www.rmol.co/images/berita/normal/463640_07552613042015_Yusril-PBB.jpg','url'=>'http://www.bijaks.net/aktor/profile/patriceriocapella511b5016a9c59','content'=>'Jangankan Presiden, kita orang biasa saja dihina bisa ditindak orang yang menghina itu. Jadi jangan disalahpahami apa yang dikatakan oleh Presiden seolah-olah mau menghidupkan pasal yang sesuai dengan pasal Ratu Belanda itu'],
        ['from'=>'Hendropiyono','jabatan'=>'mantan kepala BIN','img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150130_101027_harianterbit_hendropriyono.jpg','url'=>'http://www.bijaks.net/aktor/profile/adekomarudin5280ac60140b0','content'=>'Mengkritik itu misal bilang sodara salah, itu kritik. Itu boleh. Tapi kalau kamu bilang bangsat, itu menghina'],
        ['from'=>'Ade Komaruddin','jabatan'=>'ketua umum SOKSI','img'=>'http://static.inilah.com/data/berita/foto/2157732.jpg','url'=>'http://www.bijaks.net/aktor/profile/yusrilihsamahendra51675526bbc70','content'=>'Kami pun mendukung di dalam rancangan UU KUHP dimasukkan soal pasal penghinaan presiden']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Jimly Asshiddiqie','jabatan'=>'mantan Ketua MK','img'=>'http://img.lensaindonesia.com/uploads/1/2012/02/jimly5.jpg','url'=>'http://www.bijaks.net/aktor/profile/jimlyasshiddiqie51cb99e738a36','content'=>'Pasal ini sudah ketinggalan zaman dan tidak sesuai dengan peradaban UUD 45'],
        ['from'=>'Susilo Bambang Yudhoyono','jabatan'=>'Presiden RI 6','img'=>'http://indonesiaterkini.co/wp-content/uploads/2015/06/sby.jpg','url'=>'http://www.bijaks.net/aktor/profile/susilobambangyudhoyono50ee672eb35c5','content'=>'Pasal penghinaan, pencemaran nama baik dan tindakan tidak menyenangkan tetap ada "karetnya", artinya ada unsur subyektifitasnya'],
        ['from'=>'Nasir Djamil','jabatan'=>'anggota komisi III DPR R','img'=>'http://cdn.ar.com/images/stories/2015/07/nasir-djamil-.jpg','url'=>'http://www.bijaks.net/aktor/profile/hmuhammadnasirdjamil50f79983f3244','content'=>'Menjadi seorang pemimpin seperti Presiden harus siap dikritik dan dicaci'],
        ['from'=>'Aziz Syamsuddin','jabatan'=>'ketua komisi III DPR RI','img'=>'http://images.detik.com/content/2012/06/12/10/AzisSyamsuddin-DPR.jpg?w=650','url'=>'http://www.bijaks.net/aktor/profile/drazizsyamsuddin50f8e5a2bef6b','content'=>'Berdasarkan asas hukum yang berlaku, sesuatu yang dibatalkan di MK tidak bisa lagi dibahas atau dihidupkan kembali'],
        ['from'=>'Johanes Tuba Helan','jabatan'=>'pengamat hukum tata negara universitas cendana','img'=>'http://cdn-2.tstatic.net/kupang/foto/bank/images/yohanes-tuba-helan.jpg','url'=>'http://www.bijaks.net/aktor/profile/haritsabuulya55c9ae988d64e','content'=>'Kita juga kuatir kalau pasal ini bisa disalahtafsirkan oleh aparat penegak hukum dan itu akan berdampak pada kehidupan berdemokrasi bangsa ini'],
        ['from'=>'Amir Syamsuddin','jabatan'=>'mantan Menkunham','img'=>'http://a.okezone.com/photos/2012/02/02/4591/27414_large.jpg','url'=>'http://www.bijaks.net/aktor/profile/amirsyamsuddin51b7e62b21935','content'=>'Ini kelihatannya kalau sudah timbul kehebohan banyak orang mencoba melepaskan. Harusnya kaji dulu baik-baik. Tak perlu mencari popularitas'],
        ['from'=>'Arbi Sanit','jabatan'=>'pengamat politik','img'=>'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTGY0gWFNK2RRkj_YfKu47mijCUa94n-HHs2EMeyXR7ZjcpDn33','url'=>'http://www.bijaks.net/aktor/profile/profarbisanit55c9b3617b72b','content'=>'Jika pasal itu ada akan menghidupkan tradisi penguasa otoriter yang berawal dari masa kolonial dan orde baru'],
        ['from'=>'Harist Abu Ulya','jabatan'=>'Direktur The Community of Ideological Islamic Analyst (CIIA)','img'=>'http://cdn.ar.com/images/stories/2013/04/harits-abu-ulya1.jpg','url'=>'http://www.bijaks.net/aktor/profile/harisazhar528c203a4e71f','content'=>'Seolah menjadi hal urgent untuk menjaga wibawa dan wajah kekuasaan dibanding harus fokus bekerja yang bisa melahirkan kecintaan rakyat kepada pemimpinnya'],
        ['from'=>'Asep Warlan Yusuf','jabatan'=>'Pengamat Hukum Tata Negara Universitas Parahyangan (Unpar)','img'=>'http://www.rmoljakarta.com/images/berita/normal/32400_11521904072015_asep_warlan.jpg','url'=>'http://www.bijaks.net/aktor/profile/asepwarlanyusuf54ebd3dc4338d','content'=>'Saya khawatirnya nanti orang mengkritik dibilang menghina presiden. Jika ini terjadi, maka tak jauh beda dengan masa orde baru'],
        ['from'=>'Haris Azhar','jabatan'=>'koordinator kontras','img'=>'http://static.inilah.com/data/berita/foto/2162553.jpg','url'=>'http://www.bijaks.net/aktor/profile/netaspane51be6ee808eb6','content'=>'Saya tidak mengerti kenapa pemerintah masih mau mencantumkan pasal tersebut'],
        ['from'=>'Neta S Pane','jabatan'=>'Ketua Presidium Indonesia Police Watch (IPW)','img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRq44w0sYLo0PIIQviMdsV_dJuFOydW8pLX59_4cwvXIRQPcRsz6g','url'=>'http://www.bijaks.net/aktor/profile/johanestubahelan55c9bc33b4db1','content'=>'Polri bisa saja dituding sebagai alat presiden untuk mengkriminalisasi para pengeritik atau lawan-lawan politiknya']
    ],
    'VIDEO'=>[
        ['id'=>'yL1HYdWznJE'],
        ['id'=>'MKIpZNe_VKU'],
        ['id'=>'coiJd-BUdGc'],
        ['id'=>'2xj9jGVjHgc'],
        ['id'=>'iTWPSF1O_-U'],
        ['id'=>'AW6dWpEgpq8'],
        ['id'=>'oAefUmSfse4'],
        ['id'=>'sCySrXtox4Q'],
        ['id'=>'nXsR2jxb_B4'],
        ['id'=>'fZKzowG_sas'],
        ['id'=>'xabG8qFacCo'],
        ['id'=>'zHiK3OYMbPk']
    ],
    'FOTO'=>[
        ['img'=>'http://berita.suaramerdeka.com/konten/uploads/2015/08/KUHP.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/08/04/337/1190125/yasonna-keukeuh-masukan-pasal-penghinaan-presiden-ke-ruu-kuhp-9OK60jfFhr.jpg'],
        ['img'=>'http://images.hukumonline.com/frontend/lt55c42c9e5c57d/lt55c42f0c0780b.jpg'],
        ['img'=>'http://www.fiskal.co.id/img/news/1438844890news.jpg'],
        ['img'=>'http://usimages.detik.com/community/media/visual/2014/10/21/abcd4d00-8bbd-4bdb-b7af-b33a2473a68c_169.jpg?w=780&q=90'],
        ['img'=>'http://1.bp.blogspot.com/-ojxIT8JLjR8/Vcch3KsRpNI/AAAAAAABLtQ/aOuYTFbto4g/s1600/SBY_Jokowi_1.jpg'],
        ['img'=>'http://www.jpnn.com/picture/normal/20150810_213303/213303_484322_cuit_sby.jpg'],
        ['img'=>'http://3.bp.blogspot.com/-Q-_96kJRA54/VcFHI5X6agI/AAAAAAABLPE/fg6sjWwPCZo/s1600/pasal%2Bpenghinaan.jpg'],
        ['img'=>'http://3.bp.blogspot.com/-uWVsLYkene8/VcGFBnOmGMI/AAAAAAABLSA/Rz9mBrzEuaM/s1600/fahri16a.jpeg'],
        ['img'=>'http://2.bp.blogspot.com/-0a73vaH7OYo/VcLO1LB2NZI/AAAAAAABLWA/Oa-UB8ud7s0/s1600/11825212_911100005611292_6709418086430283634_n.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/150115164705-278.jpg'],
        ['img'=>'http://www.pks-kreatif.com/content/uploads//images/August2015/20150805215010.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/mantan-ketua-mahkamah-konstitusi-mk-jimly-asshiddiqie-_150515161156-115.jpg'],
        ['img'=>'http://3.bp.blogspot.com/-BZLF7t6pP_w/VcgBkRABvlI/AAAAAAABLvk/JjZXj7A2NDE/s1600/FPI_pasal.jpg'],
        ['img'=>'http://4.bp.blogspot.com/-4a2T2zVG7uo/VcBikqC3k1I/AAAAAAABLLc/YqcI3DBY5pw/s1600/fadli%2Bzon_pasal%2Bpenghinaan%2Bpresiden.jpg'],
        ['img'=>'http://image.slidesharecdn.com/pro-kontra-uuite-090611065905-phpapp01/95/pro-kontra-pasal-penghinaan-di-uu-ite-kuhp-2-728.jpg?cb=1244703566'],
        ['img'=>'http://image.slidesharecdn.com/presentasi-rpm-konten-140710003632-phpapp01/95/presentasi-kemkominfo-tentang-penanganan-internet-bermuatan-negatif-8-638.jpg?cb=1404958371'],
        ['img'=>'http://i.ytimg.com/vi/zHiK3OYMbPk/hqdefault.jpg'],
        ['img'=>'http://media.suara.com/thumbnail/650x365/images/2015/08/04/o_19rs603jpb8eftd1v983p14i1a.jpg?watermark=true'],
        ['img'=>'http://www.islamtoleran.com/wp-content/uploads/2015/08/11836869_10206098892724190_1741105027744859430_n.jpg'],
        ['img'=>'http://i.ytimg.com/vi/AW6dWpEgpq8/0.jpg'],
        ['img'=>'http://www.konfrontasi.com/sites/default/files/styles/article_big/public/article/2015/08/Screenshot_080415_051213_PM.jpg?itok=gGcs_WbU'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/presiden-dan-wakil-presiden-pimpin-rapat-terbatas_20150803_142740.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/06/22/72c2679f-14ab-46b0-a112-c9ccbceb6942_169.jpg?w=650']
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }    
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

    .gallery li {
        display: block;
        float: left;
        height: 50px;
        margin-bottom: 7px;
        margin-right: 0px;
        width: 25%;
        overflow: hidden;
    }
    .gallery li a {
        height: 100px;
        width: 100px;
    }
    .gallery li a img {
        max-width: 97%;
    }

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $(".gallery").lightGallery();
    $(".gallery2").lightGallery();
})
</script>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/pasalhantu/top_kcl.png")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ANALISA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['ANALISA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PRO-KONTRA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROKONTRA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BUNYI PASAL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://cdn.tmpo.co/data/2012/09/17/id_140334/140334_620.jpg" style="width: 50%;margin: 0 auto;display: block;">
                    <p style="text-align: justify;"><?php echo $data['PROFIL']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KRONOLOGI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <ol style="list-style-type: circle;margin-left: -10px;">
                    <?php
                    foreach ($data['KRONOLOGI']['list'] as $key => $val) {
                        echo "<li style='margin-right: 20px;font-weight: bold;'>".$val['date']."</li><p>".$val['content']."</p>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <h5 style="padding-top: 15px;">PARTAI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'komiteummatuntuktolikarakomat55bc370dbd56b'){
                    $photo = 'http://www.kemanusiaan.id/content_images/toliharadamai_thumbmedium.jpg';
                    $pageName = 'Komat (Komite Ummat Untuk Tolikara)';
                }
                elseif($val['page_id'] == 'komnasham54c1eac938164'){
                    $photo = 'https://upload.wikimedia.org/wikipedia/id/f/fa/Logo-Komnas-HAM.png';
                    $pageName = 'Komnas HAM';
                }
                elseif($val['page_id'] == 'gerejainjilidiindonesiagidi55a9bc4d6dfca'){
                    $photo = 'http://img.eramuslim.com/media/thumb/500x0/2015/07/logo-GIdI.jpg';
                    $pageName = 'GIDI';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>
        <div class="black boxcustom2" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'bin'){
                    $photo = 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/The_National_Intelligence_Agency_(Indonesia).svg/2000px-The_National_Intelligence_Agency_(Indonesia).svg.png';
                    $pageName = 'BIN';
                }
                elseif($val['page_id'] == 'soksi'){
                    $photo = 'http://downloaddesain.com/wp-content/uploads/2013/06/logo-soksi.jpg';
                    $pageName = 'SOKSI';
                }
                elseif($val['page_id'] == 'sekretariatjenderalkemenkumham535dbf662045e'){
                    $photo = 'http://www.kiblat.net/files/2015/04/Kemenkumham_Kementerian_Hukum_dan_Hak_Asasi_Manusia_Gedung4_Deni_Hardimansyah.jpg';
                    $pageName = 'MenkunHAM';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARTAI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'setarainstitute533a7f7211a39'){
                    $photo = 'http://setara-institute.org/wp-content/uploads/2014/11/si-logo-2.png';
                    $pageName = 'SETARA INSTITUTE';
                }
                elseif($val['page_id'] == 'jaringangusdurian55bc3b936972d'){
                    $photo = 'http://3.bp.blogspot.com/-jvrw0-Qbies/VbCouVTHwdI/AAAAAAAABIA/l4zHYhShdbU/s1600/1392106706844.jpg';
                    $pageName = 'Jaringan Gusdurian';
                }
                elseif($val['page_id'] == 'gmki55bc475d79ca6'){
                    $photo = 'https://upload.wikimedia.org/wikipedia/id/9/9c/Gmki_02.jpg';
                    $pageName = 'GMKI';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'ciia'){
                    $photo = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTF57zhW_FsWlm1ubwy6RXJyl9fJwt0hjS-3cCMxd7jfp5IqGCU';
                    $pageName = 'CIIA';
                }
                elseif($val['page_id'] == 'unpar'){
                    $photo = 'http://lpm.unpar.ac.id/weblpm/assets/uploads/slider/unpar.jpg';
                    $pageName = 'UNPAR';
                }
                elseif($val['page_id'] == 'universitasnusacendana54e5a8806f32c'){
                    $photo = 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRaGIcfVN1__CJqPPZjCOSqsXjvSiBD2ZUEVbzsX29Ys-SPw-Ne';
                    $pageName = 'UNDANA';
                }
                elseif($val['page_id'] == 'indonesianpolicewatch53df3ee0395d1'){
                    $photo = 'http://berita2bahasa.com/images/articles/201589indonesia%20police%20watch%20-%20istimewa.jpeg';
                    $pageName = 'IPW';
                }
                elseif($val['page_id'] == 'konfederasiserikatpekerjaindonesia55c9a179d59df'){
                    $photo = 'http://4.bp.blogspot.com/-nCGeC5q0hjk/U2XpVZ5y5eI/AAAAAAAAA0M/QYJjcpyGzgU/s1600/LOGO+KSPI.jpg';
                    $pageName = 'KSPI';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SEJARAH KUHP PASAL PENGHINAAN PRESIDEN</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SEJARAH']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PASAL KARET : ANCAMAN DEMOKRASI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PASAL']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['FOTO'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                        <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>            
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>