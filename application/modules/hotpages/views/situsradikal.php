<?php 
$data = [
    'NARASI'=>[
        'title'=>'KONTROVERSI SITUS ISLAM RADIKAL',
        'narasi'=>'Kementerian Komunikasi dan Informatika membredel situs  atau media islam  yang dinilai menyebarkan faham radikalisme dan seruan kebencian.   Setidaknya  ada 19 laman media Islam ditutup setelah Badan Nasional Penanggulangan Terorisme (BNPT)menganalisis potensi bahaya yang ditimbulkan dari isi laman-laman tersebut. Konten situs-situs tersebut  mengandung unsur provokasi, penanaman ideologi ekstrem, radikal, dan teroris. 
                    </p><p><img src="http://4.bp.blogspot.com/-l4TZY7H9NKE/VR4RHchXb_I/AAAAAAAAdWA/DVCs8loq6ak/s640/19-Situs-Islam-Indonesia-Diblokir-Pemerintah-Benarkah.jpg" class="pic">Pembredelan dilakukan sebagai upaya antisipatif terhadap penyebaran faham radikalisme dan terorisme dari luar, khususnya dari kelompok ekstrem ISIS, Al Qaeda, dan gerakan-gerakan terorisme yang lain. Sontak aksi tersebut mendapat penolakan dari berbagai kalangan karena, pertama, peraturan atau dasar hukumnya dinilai belum jelas. Kedua, pembredelan dilakukan secara serampangan sehingga situs-situs yang tidak memuat konten yang berbahaya ikut kena imbasnya. Ketiga, proses bredel  tidak transparan, karena tak ada upaya klarifikasi kapada pihak pengelola situs yang dicap radikal. Sikap demikian mencerminkan sikap otoriter pemerintah.'
    ],
    'PROFIL'=>[
        'sejarah'=>'<img src="http://www.kabarumat.com/assets/2015/03/situs-islam-diblokir.jpg" class="pic">Pasca peristiwa 65, Pemerintahan di bawah rezim orde baru menutup paksa  46 dari 163 koran yang ada.  Pemerintah berdalih bahwa pembredelan dilakukan untuk membersihkan sisa –sisa komunisme di Indonesia, yang memang kala itu menjadi agenda politik utama yang didengungkan oleh rezim Soeharto. Tida hanya menyisir media, ratusan wartawan yang dicurigai mempunyai afiliasi dengan komunis ditangkap dan dipecat dari keanggotaan di Persatuan Wartawan Indonesia(PWI).</p>
                    </p><p>Tahun 1994 tiga media, Tempo, Detik, dan Editor mengalami penutupan paksa yang dilakukan oleh Pemerintah. Ini adalah yang kedua kalinya dialami oleh Tempo setelah sebelumnya pada tahun 1982 dibredel oleh pemerintah melalui Departemen Penerangan.</p>
                    </p><p>Pengbungkaman pers besar-besaran oleh pemerintah dilakukan untuk menjaga stabilitas rezim. Soeharto sebagai pemegang kendali rezim Orde Baru tidak ingin media menyoroti pemerintahannya. Jika ada media yang berani mewartakan seputar pemerintahan yang tidak sesuai dengan selera rezim, maka konsekuensinya adalah penutupan paksa atau pembredelan. Ini adalah periode kelam bagi kebebasan pers di Indonesia.</p>
                    </p><p><img src="http://4.bp.blogspot.com/-YgQUuQ9AFAo/VRoyYWOe14I/AAAAAAAA_kI/haFW638Ahdk/s1600/22%2Bsitus%2Bislam%2Bdiblokir.jpg" class="pic2">Setelah reformasi bergulir, kebebasan pers mulai nampak seiring disahkannya Undang-Undang Nomor 40 Tahun 1999 tentang Pers.  Pers tidak lagi mengalami pengekangan dan ancama pembredela.. Kini pers mempunyai kebebasan dalam mencari, mengolah, dan menyampaikan informasi dengan menggunakan segala jenis saluran yang tersedia.</p>
                    </p><p>Namun hal tersebut tidak serta dapat membebaskan media dari ancaman penutupan paksa. Jika di Era Orde Baru kebijakan bredel diterapkam untuk melindungi privasi penguasa, di Era sekarang ancaman pembredelan masih mengintai. Kasus terakhir yang mencuat ke permukaan adalah yang di alami oleh media-media islam. Alasan pemerintah adalah karenakonten-konten media tersebut memuat ajaran tentang radikalisme dan terorisme yang berpotensi mengganggu ketertiban umum.</p>'
    ],
    'SARANA'=>[
        'narasi'=>'Pola rekrutmen yang diterapkan oleh kelompok-kelompok tidak lagi memakai pola yang tertutup, tetapi sekarang lebih terbuka. Jika dulu pendekatan melalui jaringan rumah ibadah, pengajian, atau memalui jalur dekat. Sekarang media menjadi sarana alternatif yang cukup efektif, penyebarannya lebih massif dan lebih terbuka.  Diduga beberapa kasus kekerasan yang bernuangsa SARA terjadi karena para pelaku memperoleh pemahaman keagamaan yang keliru dari media-media islam yang  menyebarkan ajaran untuk membenci kelompok lain. 
                    </p><p class="font_kecil">Radikalisasi melalui media menjadi pola rekrutmen yang banyak dipakai oleh kelompok-kelompok  garis keras tidak hanya dari dalam negeri tapi juga dari luar negeri. Al Qaeda sejak tahun 2005 menyebarkan ideologinya melalui media online. Gerakan radikal ISIS yang sekarang menjadi gerakan ekstrem di kawasan timur tengah juga memamfaatkan media online sebagai sarana rekrutmen. 
                    </p><p class="font_kecil">WNI yang berangkat ke Suriah dan  bergabung dalam jaringan kelompok Islamic State of Iraq and Syria (ISIS) diduga korban penyebaran paham radikalisme melalui jaringan internet.  "Media sosial membuka ruang tertutup menjadi terbuka. Tak heran jika beberapa remaja 18-25 tahun bergabung dengan ISIS karena pengaruh propaganda media sosial," demikian kata Mayjen TNI Agus Surya Bakti, Deputi Bidang Pencegahan, Perlindungan dan Deradikalisasi Badan Nasional Penanggulangan Terorisme (BNPT).'
    ],
    'KRITERIA'=>[
        'narasi'=>'Menurut BNPT, ada empat kriteria sebuah situs web media dapat dinilai radikal, antara lain :',
        'list'=>[
            ['no'=>'Ingin melakukan perubahan dengan cepat menggunakan kekerasan   dengan mengatasnamakan agama'],
            ['no'=>'Takfiri atau mengkafirkan orang lain'],
            ['no'=>'Mendukung, menyebarkan, dan mengajak bergabung dengan ISIS'],
            ['no'=>'Memaknai jihad secara terbatas']
        ],
        'bawah'=>'Empat kriteria tersebut menjadi dasar utama BNPT dalam mengambil langkah pemblokiran. BNPT juga menjawab tudingan bahwa yang mereka lakukan bukanlah tindakan yang sewenang-wenang.Penutupan terhadap  sebagian situs-situs yang bermuatan radikal sudah melalui pengkajian yang cukup panjang sejak tahun 2012 dengan berbagai pihak rekan BNPT'
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="http://cdn.ar.com/images/stories/2015/04/222.jpg" class="pic">Beragam tanggapan dan respon ditunjukkan oleh masyrakat dalam menyikapi aksi pembredelan situs islam oleh Kemeninfo atas masukan dari BNPT. Kelompok-kelompok islam menilai bahwa aksi demikian adalah bentuk ketdakmampuan pemerintah mengurai masalah yang ada. 
                    </p><p>Bahkan mereka menuding pemerintah terlalu otoriter karena tidak mengedepankan dialog terlebih dahulu, apalagi dalih pemerintah adalah situs-situs tersebut menyebarkan konten radikalisme padahal sejauh ini pemahaman tentang radikalisme masih menjadi perdebatan. Selain itu, menutup situs islam sama dengan menutup akses orang islam dalam berdakwah. 
                    </p><p><img src="http://i.ytimg.com/vi/rMZ_Vjs_cb8/hqdefault.jpg" class="pic2">Dari sudut pandang yang lain, langkah pemerintah memblokir situs media islam tidaklah tepat. Pemerintah tdaik bisa menutup situs tersebut tanpa adanya putusan dari pengadilan negeri.
                    </p><p>Namun tidak sedikit juga yang mengapresiasi langkah Kemeninfo tersebut. Upaya tersebut adalah langkah konkret yang dilakukan oleh pemerintah dalam menangkal setiap aksi yang berpeluang mengganggu keamanan nasional. Dewan Pers juga ikut merespon bahwa situs-situs yang diblokir oleh Kemeninfo tersebut tidak memenuhi standar kaidah jurnalisme karena menyampaikan pesan atau berita yang provokatif, fitnah, yang tidak didukung dengan data yang valid. Mereka bersekukuh mendukung langkah tersebut, sehingga situs-situs tersebut tidak dapat dilindungi sesuai payung hukum Undang-Undang Nomor 41 Tahun 1999 tentang Pers. Menurut mereka, situs-situs tersebut bukanlah produk jurnalistik.'
    ],
    'DAFTAR'=>[
        'narasi'=>'Saat ini terdapat 21 situs web media Islam yang diblokir akibat dinilai menyebarkan radikalisme. Mereka adalah :',
        'list'=>[
            ['no'=>'arrahmah.com'],
            ['no'=>'voa-islam.com'],
            ['no'=>'ghur4ba.blogspot.com'],
            ['no'=>'panjimas.com'],
            ['no'=>'thoriquna.com'],
            ['no'=>'dakwatuna.com'],
            ['no'=>'kafilahmujahid.com'],
            ['no'=>'an-najah.net'],
            ['no'=>'muslimdaily.net'],
            ['no'=>'hidayatullah.com'],
            ['no'=>'salam-online.com'],
            ['no'=>'aqlislamiccenter.com'],
            ['no'=>'kiblat.net'],
            ['no'=>'dakwahmedia.com'],
            ['no'=>'muqawamah.com'],
            ['no'=>'lasdipo.com'],
            ['no'=>'gemaislam.com'],
            ['no'=>'eramuslim.com'],
            ['no'=>'daulahislam.com'],
            ['no'=>'azzammedia.com'],
            ['no'=>'indonesiasupportislamicatate.blogspot.com']
        ]
    ],
    'NOLMALISASI'=>[
        'narasi'=>'Kominnfo mengupayakan normalisasi terhadap 19 situs yang telah diblokir. Langkah ini diambil setelah menerima beragam masukan dan atas intruksi Wakil Presiden, Yusuf Kalla. Selanjutnya dibentuk Tim Panel Terorisme, SARA, dan Kebencian dari Forum Penanganan Situs Internet Bermuatan Negatif (PSIBN). Setelah melakukan pertemuan dengan pemilik situs-situs yang telah diblokir. Akhirnya Kominfo merekomendasikan untuk membuka kembali situs-situs tersebut. <br>Dari 19 yang ditutup, baru ada 12 situs yang kembali dibuka :',
        'list'=>[
            ['no'=>'arrahmah.com'],
            ['no'=>'hidayatullah.com'],
            ['no'=>'salam-online.com'],
            ['no'=>'aqlislamiccenter.com'],
            ['no'=>'kiblat.net'],
            ['no'=>'gemaislam.com'],
            ['no'=>'panjimas.com'],
            ['no'=>'muslimdaily.net'],
            ['no'=>'voa-islam.com'],
            ['no'=>'dakwatuna.com'],
            ['no'=>'an-najah.net'],
            ['no'=>'eramuslim.com']
        ],
        'bawah'=>'Sementara, untuk sisanya masih tetap diblokir dan diberikan kesempatan untuk melakukan pertemuan dengan Kemkominfo.',
        'list2'=>[
            ['no'=>'ghur4ba.blogspot.com'],
            ['no'=>'thoriquna.com'],
            ['no'=>'kafilahmujahid.com'],
            ['no'=>'lasdipo.com'],
            ['no'=>'muqawamah.com'],
            ['no'=>'daulahislam.com'],
            ['no'=>'dakwahmedia.com']
        ]
    ],
    'PASAL'=>[
        'narasi'=>'Kominfo berdalih bahwa langkah pemblokiran yang dilakukan oleh pihaknya mengacu pada Peraturan yang sudah berlaku.',
        'list'=>[
            ['no'=>'Peraturan Menteri Kominfo Nomor 19 Tahun 2014 soal penanganan situs internet bermuatan negative'],
            ['no'=>'Undang-Undang Nomor 11 Tahun 2008 tentang Informasi dan Transaksi Elektronik (UU ITE)']
        ],
        'bawah'=>'Kominfo merujuk Pasal 1 Permen tersebut, pemblokiran situs adalah upaya yang dilakukan agar situs internet bermuatan negatif tidak dapat diakses. Ini tercantum dalam Bab III pasal 4 dari Peraturan Menteri tersebut.'
    ],
    'KRONOLOGI'=>[
        ['date'=>'27/3/2015','content'=>'Badan Nasional Penanggulangan Terorisme (BNPT) merekomendasikan pemblokiran  terhadap 22 situs islam yang dianggap menyebarkan faham radikalisme ke Kementerian Komunikasi dan Informatika.'],
        ['date'=>'30/03/2015','content'=>'Akhirnya Kemenkominfo melalui Menteri Rusdiantara kemudian meminta internet service provider (ISP) untuk  memblokir situs-situs yang dianggap menyebar paham radikalisme itu.'],
        ['date'=>'','content'=>'Protes dilayangkan oleh pemilik situs atas pemblokiran. Dari 19 situs yang telah diblokir, 10 pemilik situs sudah bertemu dengan Kominfo untuk meminta klarifikasi.'],
        ['date'=>'31/03/2015','content'=>'Wakil Ketua Majelis Permusyawaratan Rakyat, Hidayat Nur Wahid, mendesak Dewan Perwakilan Rakyat memanggil Menteri Komunikasi dan Informatika, Rudiantara.'],
        ['date'=>'07/04/2015','content'=>'Ditjen Aptika mengusulkan proses normalisasi kepada Panel Terorisme, SARA dan Kekerasan dari Forum Penanganan Situs Internet Bermuatan radikal.'],
        ['date'=>'10/04/2015','content'=>'Kominfo menggelar rapat dengan Tim panel untuk menormalisasi situs-situs yang diblokir.'],
        ['date'=>'','content'=>'Akhirnya Kominfo membuka kembali situs yang sebelumnya diblokir. 10  situs dibuka kembali dengan catatakan di bawah pengawasan Kominfo dan bersedia menghapus konten yang bernuangsa SARA.']
    ],
    'PENYOKONG'=>[
        ['name'=>'BNPT','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'http://2.bp.blogspot.com/-k5jeVQ1svsA/U_grEHrkP-I/AAAAAAAAAT4/8K2k_WM_500/s1600/LOGO-BNPT.png' ],
        ['name'=>'POLRI','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'http://2.bp.blogspot.com/-GBErBcGBQlo/VIzWFgVVidI/AAAAAAAAAgw/VGDR74ZsPZ0/s1600/polisi-polri.png'],
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1']
        ],
        'institusi'=>[
            ['name'=>'KEMENKO POLHUKAM','img'=>'http://upload.wikimedia.org/wikipedia/id/e/e2/Logo_Kemenkopolhukam.png','url'=>''],
            ['name'=>'IKATAN DAI INDONESIA (IKADI)','img'=>'http://4.bp.blogspot.com/_L64nMBrys-g/Sm6qHZyO6jI/AAAAAAAAAI8/x7Xk6jA1GTA/s400/IKADI.jpg','url'=>''],
            ['name'=>'INDONESIA CONFERENCE ON RELEGION AND FEACE (ICRP)','img'=>'http://ahlulbaitindonesia.org/berita/wp-content/uploads/2014/06/ICRP2-660x330.jpg','url'=>''],
            ['name'=>'PBNU','img'=>'','url'=>'']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef']
        ],
        'institusi'=>[
            ['name'=>'DPR RI','img'=>'http://beri.biz/wp-content/uploads/2012/11/DPR-RI-.jpg','url'=>''],
            ['name'=>'MUHAMMADIYAH','img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/4/44/Logo_Muhammadiyah.svg/200px-Logo_Muhammadiyah.svg.png','url'=>''],
            ['name'=>'PERSATUAN WARTAWAN INDONESIA (PWI)','img'=>'http://sp.beritasatu.com/media/images/original/20140906142159947.jpg','url'=>''],
            ['name'=>'JURNALIS ISLAM BERSATU (JITU)','img'=>'http://cdn.ar.com/images/stories/2013/04/jitu-1a.png','url'=>''],
            ['name'=>'ALIANSI JURNALIS INDEPENDEN (AJI)','img'=>'http://jilonews.com/images/gambar/aji1.png','url'=>''],
            ['name'=>'ASOSIASI PENYELENGGARA JASA INTERNET INDONESIA (APJII)','img'=>'http://sumut.apjii.or.id/wp-content/uploads/2012/08/APJII-copy.png','url'=>''],
            ['name'=>'MAJELIS ULAMA INDONESIA','img'=>'http://mui.or.id/wp-content/uploads/2014/03/051213114943_mui-2.jpg','url'=>''],
            ['name'=>'Perhimpunan Pemuda Hindu (Peradah)','img'=>'','url'=>'']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Abdul Kadir Karding','jabatan'=>'Anggota Komisi III DPR RI','img'=>'http://data.seruu.com/images/seruu/article/2014/08/10/abdul_kadir_karding,_s.pi,_m,si.jpg','url'=>'http://www.bijaks.net/aktor/profile/abdulkadirkarding5285dbba60e22','content'=>'Kalau bicara pencegahan dan antisipasi terjadinya aksi terorisme, tentu saya mendukung penuh langkah BNPT dalam mengusulkan pemblokiran situs-situs radikalisme atau situs bermuatan negatif.'],
        ['from'=>'I Putu Sudiartana','jabatan'=>'Anggota DPR RI','img'=>'http://www.dpr.go.id/images_pemberitaan/images/I%20Putu%20Sudiartana.jpg','url'=>'http://www.bijaks.net/aktor/profile/iputusudiartana52b3b288f3698','content'=>'Sebagai wakil dari masyarakat Bali, tentu kami harus mendukung upaya-upaya pemberantasan terorisme, termasuk dengan langkah yang dilakukan BNPT. Jangan sampai ada bom ketiga yang meledak di Bali'],
        ['from'=>'Komisaris Jenderal Saud Usman Nasution','jabatan'=>'Kepala BNPT','img'=>'http://www.indopos.co.id/wp-content/uploads/2014/10/Usman-e1427804408986.gif','url'=>'http://www.bijaks.net/aktor/profile/saudusmannasution5285761a70daf','content'=>'Kalau kita bicara jujur situs itu tidak mendidik, jadi salah besar BNPT menutup situs Islam. Saya sangat sedih, karena yang saya usulkan itu bernuansa negatif'],
        ['from'=>'Slamet Effendy Yusuf','jabatan'=>'Ketua Pengurus Besar Nahdatul Ulama (PBNU)','img'=>'http://img.eramuslim.com/media/2015/04/201407071421539061.jpg','url'=>'http://www.bijaks.net/aktor/profile/slameteffendyyusuf549784b0a8b60','content'=>'Saya kira karena memang negara berkewajiban hadir ketika ada pihak tertentu yang mendatangkan bahaya bagi persatuan dan kesatuan nasional dan ideologi nasional yaitu pancasila. Jadi oleh karena itu penertiban ini tepat, terutama untuk radikalisme'],
        ['from'=>'Brigadir jenderal Agus Rianto','jabatan'=>'Kepala Biro Penerangan Masyarakat','img'=>'http://panera.cdn.ansideng.com/dynamic/article/2015/02/26/9941/F2jCSPkmGK.jpg?w=630','url'=>'http://www.bijaks.net/aktor/profile/kombespoldrsagusrianto5530914a175db','content'=>'Sudah sejak jauh hari kami memperingatkan soal penyebaran informasi atau data melalui cara yang tidak menguntungkan, apalagi tidak sesuai dengan kepercayaan agama yang seharusnya. Karena yang ditampilkan kekerasan, sifatnya provokatif'],
        ['from'=>'Irjen (Purn) Ansyad Mbay','jabatan'=>'Mantan Kepala Badan Nasional Penanggulangan Terorisme','img'=>'http://statik.tempo.co/?id=74646&width=475','url'=>'http://www.bijaks.net/aktor/profile/ansyaadmbai532164d284534','content'=>'Kita harus apresiasi itu. Kebijakan itu penting dan luar biasa berdampak besar bagi upaya pencegahan paham kelompok radikalisme'],
        ['from'=>'KH Hasani Zubair','jabatan'=>'Juru bicara FUB','img'=>'http://www.bangsaonline.com/images/uploads/berita/700/d555e6ab6f2a50a1c487511d2955ff1a.jpg','url'=>'','content'=>'Kalau perlu, para pengelola situs-situs radikal ditangkap saja'],
        ['from'=>'HM Kholili','jabatan'=>'Pengamat Komunikasi Universitas Islam Negeri Yogyakarta','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Kalau media atau situs-situs itu berbahaya, harus disetop. Jangan sampai karena alasan kebebasan']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Hidayat Nur Wahid','jabatan'=>'Wakil Ketua MPR RI','img'=>'http://www.indonesia-2014.com/sites/default/files/tokoh/hidayat.jpg','url'=>'http://www.bijaks.net/aktor/profile/drhmhidayatnurwahidma50f7ae3aba4f8','content'=>'Seharusnya pemerintah memiliki landasan hukum yang kuat, sehingga ketika hendak melakukan pembredelan sudah ada landasan yang jelas. Kesalahannya (situs) itu apa, setelah jelas kesalahannya baru dijatuhkan sanksi, tidak langsung eksekusi'],
        ['from'=>'Fadli Zon','jabatan'=>'','img'=>'http://www.rmol.co/images/berita/normal/15516_07054810042015_1418724912fadli-zon.jpg','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007','content'=>'Rekom dari BNPT ke Kemenkominfo itu tidak dilakukan verifikasi, ditelan bulat-bulat. Dua lembaga ini kontribusinya sama dalam kesalahan. Seharusnya dikaji, bikin panel sehingga rekomendasi itu keluar sudah matang, bukan mentah. Bukan zamannya lagi, nutup situs, blokir, bredel'],
        ['from'=>'Agus Abdullah','jabatan'=>'Ketua Umum Jurnalis Islam Bersatu (JITU)','img'=>'http://img.salampos.com/uploads/2015/04/JITU-Ketua-Jurnalis-Islam-Bersatu-JITU-Agus-Abdullah-jpeg.image_.jpg','url'=>'','content'=>'Kami menunggu permintaan maaf dari Kemenkominfo atas kecerobohan ini. Kemenkominfo harus memberikan penjelasan kepada masyarakat. Sebab ini menyangkut nama baik yang telah dilabeli dengan sebutan radika'],
        ['from'=>'Onno W.Purbo','jabatan'=>'Fakar IT','img'=>'http://3.bp.blogspot.com/-eB1mlsOzkhs/UYCA0-KyrMI/AAAAAAAAAys/oClKg_-cCkw/s320/foto-onno-w-purbo.jpg','url'=>'','content'=>'Proses blokir situs sebetulnya merupakan proses penyadapan. Padahal di aturan yang ada, penyadapan harus berdasarkan perintah pengadilan'],
        ['from'=>'Saleh Partoana Daulay','jabatan'=>'Ketua Komisi VIII','img'=>'http://www.rmol.co/images/berita/normal/869467_02132504042015_saleh_partaonan_daulay.jpg','url'=>'http://www.bijaks.net/aktor/profile/salehpartaonandalay51bfce41ef1e4','content'=>'Kalau langsung ditutup kesannya pemerintah sangat otoriter. Tidak ada ruang diskusi dan klarifikasi. Yang sedikit berbeda, langsung dibungkam'],
        ['from'=>'Yunahar Ilyas','jabatan'=>'Ketua PP Muhammadiyah','img'=>'http://4.bp.blogspot.com/-3xk-DjoFRXU/VBee-Y0fLHI/AAAAAAAAGC0/cIFvw2Jp1xc/s1600/1.jpg ','url'=>'http://www.bijaks.net/aktor/profile/yunaharilyas54c5b6a8bae1f','content'=>'Yang diblokir agar dibuka. Cuma ke depannya, pemerintah tidak boleh main blokir lagi. Ini kontrol yang berlebihan terhadap media'],
        ['from'=>'Ilham Bintang','jabatan'=>'Ketua Dewan Kehormatan Persatuan Wartawan Indonesia (PWI)','img'=>'http://img.antaranews.com/stockphotos/tokoh/20101211214306-ilhambintag-2.jpg','url'=>'http://www.bijaks.net/aktor/profile/hilhambintang5530a0ca3b77f','content'=>'Rehabiitasi tidak mesti menutup jalan untuk menuntut. Jika mereka (pengelola situs) menganggap pemblokiran merugikan nama baik'],
        ['from'=>'Yhannu Setyawan','jabatan'=>'Komisioner Komisi Informasi Pusat (KIP)','img'=>'http://riaucitizen.com/wp-content/uploads/2015/04/Yhannu-Setyawan.jpg ','url'=>'http://www.bijaks.net/aktor/profile/yhannusetyawan5530a3cda326a','content'=>'Kita ini sedang gencar-gencarnya membangun demokrasi yang sehat, bukan malah menghidupkan lagi model pemerintahan yang otoriter'],
        ['from'=>'Din Syamsudin','jabatan'=>'','img'=>'http://cdn.ar.com/images/stories/2013/05/din-syamsudin.jpg','url'=>'http://www.bijaks.net/aktor/profile/dinsyamsuddin51c7bbdfbc558','content'=>'Memblokir situs hanya akan menumbuhkan radikalisme karena ada unsur kekecewaan. Bisa jadi tadinya cuma 22 diblokir kemudian tumbuh situs baru menjadi 22 ribu'],        
        ['from'=>'Tengku Zulkarnain','jabatan'=>'Wakil Sekjen Majelis Ulama Indonesia (MUI)','img'=>'http://cdn.ar.com/images/stories/2014/03/wakil-ketua-majelis-ulama-indonesia-mui-tengku-zulkarnaen-_130905141032-172.jpg','url'=>'','content'=>'Saya lihat gerakan membungkam Islam semakin kelihatan di Indonesia. Apalagi, ada upaya situs-situs Islam diberangus dengan alasan ada yang mendukung ISIS'],
        ['from'=>'Abdullah Gymnastiar (aagym)','jabatan'=>'','img'=>'http://tegarprajaksa.com/wp-content/uploads/2013/02/Aa-Gym1.jpg','url'=>'','content'=>'Harus ada penjelasan yg adil, agar tidak dianggap anti islam'],['from'=>'Firdaus Cahyadi','jabatan'=>'Direktur Eksekutif Yayasan SatuDunia','img'=>'https://daus1975.files.wordpress.com/2008/10/firdaus_011.jpg?w=470&h=311','url'=>'','content'=>'Kali ini situs-situs yang diduga menyebarkan ajaran intoleransi yang menjadi korban, ke depan bukan tidak mungkin situs-situs yang memiliki konten kritis terhadap kebijakan pemerintah yang menjadi korban pemblokiran'],               
        ['from'=>'Jusuf Kalla','jabatan'=>'Wakil Presiden','img'=>'http://assets.kompasiana.com/statics/files/1402277267842193257.jpg ','url'=>'http://www.bijaks.net/aktor/profile/drshmuhammadjusufkalla50ee870b99cc9','content'=>'Kalau hanya karena ada nama "Islam" lalu otomatis diblokir, tidak bisa begitu'],        
        ['from'=>'Senator Fachrul Razi','jabatan'=>'','img'=>'http://atjehpost.co/files/images/profile/aceh/Fachrul-Razi.jpg','url'=>'http://www.bijaks.net/aktor/profile/jenderalpurnfachrulrazi5397d24ba3d1e','content'=>'Kenapa anak negeri sendiri dicurigai dan diawasi begitu ketat']
    ],
    'VIDEO'=>[
        ['id'=>'C21TSDlp1Zw'],
        ['id'=>'AN4d-6q3riw'],
        ['id'=>'EQJrbEMKxLk'],
        ['id'=>'_s5yBLPl-bI'],
        ['id'=>'hewsyuVpr28'],
        ['id'=>'y2iUXGxTMsI'],
        ['id'=>'LcAjxZIEOg0'],
        ['id'=>'9rFyGJdsCFs'],
        ['id'=>'3hH0-_ARkrg']
    ],
    'FOTO'=>[
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img1.jpg'],
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img2.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/03/22/414462/dukungan-terhadap-ahok.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/02/27/id_375043/375043_620_tempoco.jpg'],
        ['img'=>'http://images.detik.com/customthumb/2015/03/23/10/171112_dprdkomahook4.jpg?w=460'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/03/01/31/970362/ahok-vs-dprd-dan-politik-adu-kuat-WkD.jpg'],
        ['img'=>'http://cdn-media.viva.co.id/thumbs2/2015/03/05/299841_mediasi-ahok-dprd-berujung-ricuh_663_382.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/03/03/id_376061/376061_620_tempoco.jpg'],
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-left: 10px;
        float: right;
        border: 1px solid black;
        margin-top: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/situsradikal/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">DAFTAR SITUS YANG DIBLOKIR</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
            <p style="text-align: justify;">Saat ini terdapat 21 situs web media Islam yang diblokir akibat dinilai menyebarkan radikalisme. Mereka adalah :</p>
                <div class="col-xs-12" style="margin-left: -15px;">
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['DAFTAR']['list'] as $key => $val) {
                            echo "<li>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PASAL PEMBLOKIRAN</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://www.mmtc.ac.id/images/Image/logo-kominfo-mirip-dikit-2.png" class="col-xs-6 col-sm-6" style="margin-left: -15px;">
                <div class="col-xs-6" style="padding-left: 0px;">
                    <p style="text-align: justify;"><?php echo $data['PASAL']['narasi'];?></p>
                </div>
                <div class="col-xs-12" style="margin-left: -15px;">
                    <ol style="list-style-type: square;margin-left: -20px;">
                        <?php
                        foreach ($data['PASAL']['list'] as $key => $val) {
                            echo "<li>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                    <p style="text-align: justify;"><?php echo $data['PASAL']['bawah'];?></p>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PRO KONTRA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROKONTRA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KRITERIA RADIKAL VERSI BNPT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
            <p style="text-align: justify;"><?php echo $data['KRITERIA']['narasi'];?></p>
                <div class="col-xs-12" style="margin-left: -15px;">
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['KRITERIA']['list'] as $key => $val) {
                            echo "<li>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div>
            <p style="text-align: justify;"><?php echo $data['KRITERIA']['bawah'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SEJARAH PEMBREDELAN DI INDONESIA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROFIL']['sejarah'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">NORMALISASI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
            <p style="text-align: justify;"><?php echo $data['NOLMALISASI']['narasi'];?></p>
                <div class="col-xs-12" style="margin-left: -15px;">
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['NOLMALISASI']['list'] as $key => $val) {
                            echo "<li>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div>
            <p style="text-align: justify;"><?php echo $data['NOLMALISASI']['bawah'];?></p>
                <div class="col-xs-12" style="margin-left: -15px;">
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['NOLMALISASI']['list2'] as $key => $val) {
                            echo "<li>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <p>PARPOL PENDUKUNG</p>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                    $photo = 'http://www.dpp.pkb.or.id/unlocked/unlock/lambangresmipkb.jpg';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom2" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KRONOLOGI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <ol style="list-style-type: circle;margin-left: -10px;">
                    <?php
                    foreach ($data['KRONOLOGI'] as $key => $val) {
                        echo "<li style='margin-right: 20px;font-weight: bold;'>".$val['date']."</li><p>".$val['content']."</p>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SARANA ALTERNATIF</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SARANA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>



