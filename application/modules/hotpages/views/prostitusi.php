<?php 
$data = [
    'NARASI'=>[
        'title'=>'BISNIS PROSTITUSI KELAS WAHID',
        'narasi'=>'<img src="http://static.republika.co.id/uploads/images/detailnews/sejumlah-tersangka-jaringan-prostitusi-dengan-sistem-online-yang-berhasil-_130212162808-515.jpg" class="pic">Menjamurnya prostitusi online jelas menambah daftar lokalisasi terselebung, seperti lokalisasi modus indekost, Penginapan dan Hotel atau Apartemen. Modus transaksinya lewat pesan Blackberri atau WA dengan mucikari, transfer uang muka lalu kencan dengan pelacur.  Hal ini kemudian membuat pemerintah gencar melakukan razia untuk memberantas praktek pelacuran modus online.
                    </p><p>Beberapa kasus yang diungkap oleh kepolisian, seperti penangkapan mucikari di Apartemen Kalibata City, yang menjajakan puluhan perempuan cantik. Pengungkapan kasus pembunuhan yang dialami Deuhdeuh, seorang PSK online yang dibunuh oleh pelanggangnya di kamar kostnya. Dan yang paling baru adalah terbongkarnya sindikat prostitusi online kelas wahid yang terdiri dari artis dan model, yang dijalankan oleh seorang mucikari berinisial “AR”.'
    ],
    'PROFIL'=>[
        'satu'=>'Kota Yogyakarta',
        'dua'=>'Islam, Kejawen',
        'tiga'=>'Jawa 1755-1950, Belanda 1755-1811; 1816-1942, Inggris 1811-1816,  Jepang 1942-1945, Indonesia 1945-1950',
        'empat'=>'Monarki (kesultanan) Sultan',
        'list'=>[
            ['no'=>'“Barang siapa yang pencahariannya dan kebiasaannya yaitu dengan sengaja mengadakan atau memudahkan perbuatan cabul dengan orang lain dihukum penjara selama-lamanya satu tahun empat bulan atau denda sebanyak-banyaknya Rp. 15.000,-.”'],
            ['no'=>'Sultan terakhir sebelum penurunan status negara (1940-1950; wafat 1988) - ISKS Hamengku Buwono IX'],
            ['no'=>'Sekarang (sejak 1989) - ISKS Hamengku Buwono X Pepatih Dalem (Menteri Pertama)'],
            ['no'=>'Pertama (1755-1799) - Danurejo I'],
            ['no'=>'Terakhir (1933-1945) - Danurejo VIII']
        ]
    ],
    'GRATIFIKASI'=>[
        'narasi'=>'<img src="http://www.aktualpost.com/wp-content/uploads/2015/05/Nama-17-Artis-Prostitusi-Online-yang-Diduga-Teman-AA.jpg" class="pic2">Pengakuan “AR” seputar bisnis prostitusi online yang digarapnya cukup menghebohkan. Beberapa inisial artis dan model disebut-sebut menjadi PSK dengan tarif yang cukup fantastis. Untuk dapat menikmati layanan jasa PSK artis, para pelanggangnya harus merogoh kocek yang dalam-dalam. Untuk sekali goyang saja, tarifnya berkisar antara 20 juta sampai 200 juta.
                    </p><p class="font_kecil">Bisnis syahwat dengan harga selangit ini kemudian menuai polemik dan pertanyaan dari beragam kalangan. Siapa pelanggan yang mau mengeluarkan uang sebanyak itu hanya untuk sekali kencan? 
                    </p><p class="font_kecil"><img src="http://www.rmoljakarta.com/images/berita/normal/910679_10261124042015_prostitusi_online.jpg" class="pic">Banyak yang berpendapat para pejabat dan pengusaha kaya yang paling bisa menikmati layanan esek-esek mahal tersebut. Hal ini kemudiaan dikaitkan dengan praktek gratifikasi seks yang kerap dilakukan oleh para pejabat yang korup. Jadi tidak menutup kemungkinan jasa seks artis ini kerap jadikan sebagai sarana pencucian uang dan gratifikasi seks oleh para pelaku korupsi. Apalagi "AR" sendiri mengakui bahwa salah satu pengguna prostitusi online datang dari kalangan pejabat dan pengusaha. Bahkan, Kepala Badan Reserse Kriminal Mabes Polri Komisaris Jenderal Budi Waseso tidak menampik jika kasus tersebut memiliki keterlibatan dengan sejumlah pejabat teras.'
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="https://i.ytimg.com/vi/IKW_28iIbuI/hqdefault.jpg" class="pic2">Terkait  maraknya praktek prostitusi yang berhasil diungkap oleh kepolisian menuai beragam tanggapan dari berbagai kalangan. Gubernur Ahok misalkan, mewacanakan akan menertibkan para PSK dengan cara melokalisirnya dengan cara menyediakan tempat khusus. Solusi yang ditawarkan ini untuk meminimalisir penyebaran praktek-praktek prostitusi yang semakin meresahkan.
                    </p><p class="font_kecil">Menurut Ahok, cara ini tentu tidak bisa seratus persen bisa menekan praktek pelacuran di Jakarta, karena menurutnya praktek akan terus ada sehingga untuk memberantas tentulah sangat sulit. Jadi pilihan realistisnya adalah melokalisir, agar tertib dan tidak berserakan di mana-mana. Cara ini juga diyakini efektif untuk menekan angka jumlah pengguna jasa syahwat tersebut.
                    </p><p class="font_kecil">Namun wacana ini mendapat reaksi penolakan dari banyak pihak. Beberapa kalangan menganggap solusi yang ditawarkan oleh Gubernur Ahok tidak akan memberikan hasil apa-apa. Justru tawaran baiknya adalah memberlakukan aturan yang tegas soal pelarangan praktek prostitusi dengan cara merevisi Undang-Undang tentang prostitusi. Solusi lainnya adalah razia pelacuran harus gencar dilakukan dengan melibatkan banyak kalangan termasuk masyarakat dan kepolisian.'
    ],
    'PSK'=>[
        'narasi'=>'<img src="http://i.ytimg.com/vi/STTe1N2AO_g/hqdefault.jpg" class="pic2">Bisnis seks artis yang dijalankan oleh "AR" terbilang bisnis kelas satu. Dari keterangan sang Mucikari, harga sekali kencan dengan artis binaannya dipatok  mulai dari 20 juta sampai 200 juta. Tentu harga yang cukup mahal.Tarif selangit yang harus dikeluarkan oleh penikmat jasa ini karena yang dijajakan adalah artis dan model. Status artis atau model memang menjadi faktor utama yang bisa menjelaskan mengapa bisnis syahwat ini sangatlah mahal.
                    </p><p class="font_kecil">Selama ini, banyak yang menilai bahwa artis atau model pastilah wanita-wanita cantik dan seksi. Sehingga bagi yang memiliki uang lebih akan rela mengeluarkan uangnya agar dapat menikmati layanan seks artis atau model tersebut. Selain kepuasan sekseual yang didapat, seorang akan merasa bangga karena bisa menikmati kemolekan tubuh para artis ataupun model tersebut.
                    </p><p class="font_kecil">Berikut inisial artis dan model beserta tarifnya :',
        'harga'=>[
            ['no'=>'TB','jml'=>'200'],
            ['no'=>'JD','jml'=>'150'],
            ['no'=>'BB','jml'=>'70'],
            ['no'=>'RF','jml'=>'60'],
            ['no'=>'RF','jml'=>'60'],
            ['no'=>'MT','jml'=>'55'],
            ['no'=>'KA','jml'=>'55'],
            ['no'=>'SB','jml'=>'55'],
            ['no'=>'CW','jml'=>'50'],
            ['no'=>'PUA','jml'=>'45'],
            ['no'=>'NM','jml'=>'40'],
            ['no'=>'CT','jml'=>'40'],
            ['no'=>'LM','jml'=>'35'],
            ['no'=>'DL','jml'=>'30'],
            ['no'=>'BS','jml'=>'30'],
            ['no'=>'AA','jml'=>'25'],
            ['no'=>'FNP','jml'=>'20']
        ]
    ],
    'MODUS'=>[
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2013/07/03/1912467Ilustrasi-Prostitusi780x390.jpg" class="pic">Prostitusi online merupakan modus baru dalam praktek pelacuran yang ada di Indonesia. Modus ini berkembang massif seiring dengan berkembangnya tekhnologi yang semakin canggih. Di kota-kota besar seperti Jakarta, prostitusi online mewabah dan tumbuh subur. Di samping faktor tekhnologi, prostitusi online adalah fenomena perpindahan dari praktek  yang dilokalisir dalam satu tempat khusus (lokalisasi) ke pola praktek yang lebIh fleksibel, yakni transaksi ini bisa dilakukan dimana saja tergantung kesepakan antara PSK dan pelangganya.
                    </p><p class="font_kecil">Fenomena perpindahan ini juga disebabkan alasan ekonomi dan keamanan. Prostitusi online lebih efisien dari segi biaya karena para pelaku, khususnya PSK bisa menjalankan bisnis tersebut dengan solo karir (tidak menggunakan mucikari) sehingga tidak perlu berbagi hasil, selain itu mereka tidak perlu mengeluarkan biaya sewa tempat karena biasanya sewa tempat ditangung oleh pelanggan. Soal keamanan juga menjadi faktor mengapa seks online menjamur. Para pelaku ini menganggap bahwa bisnis seks online lebih aman dijalankan karena transaksinya bersifat privat, yakni melalui pesan singkat atau via BBM.
                    </p><p class="font_kecil">Pelaku prostitusi biasanya bekerja dengan sangat teliti dan selektif sehingga aman dari jangkauan polisi.  Modus baru seputar pengelolaan bisnis prostitusi  bisa dipesan secara online dengan menggunakan media sosial seperti Twitter, SMS, BBM, dan lainnya.'
    ],
    'DAFTAR'=>[
        ['img'=>'http://cdn-2.tstatic.net/surabaya/foto/bank/images/2006dolly-peta.jpg','text'=>'Gang Dolly-Surabaya'],
        ['img'=>'http://www.nonstop-online.com/wp-content/uploads/2014/11/Ilustrasi-Gang-Kalijodo-.jpg','text'=>'Kramat Tunggak, Kali Jodoh-Jakarta'],
        ['img'=>'http://3.bp.blogspot.com/-90zeYNbGJL8/UKm6jo5xJpI/AAAAAAAAAzY/bL_Z5CnlKTE/s1600/unduhan.jpg','text'=>'Jl.Pajajaran-Malang'],
        ['img'=>'http://3.bp.blogspot.com/_G8TS4L9RjhM/TIACr0nly-I/AAAAAAAAAmU/CM12Lthm5DM/s320/jalan+pasar+kembang.jpg','text'=>'Sarkem-Yogyakarta'],
        ['img'=>'http://i.imgur.com/0JRzI.jpg','text'=>'Saritem-Bandung'],
        ['img'=>'http://jurnalpatrolinews.com/wp-content/uploads/2011/07/tempat-prostitusi.jpg','text'=>'Limusnunggal Cileungsi-Bogor'],
        ['img'=>'https://irs2.4sqi.net/img/general/width960/cg9Vt6fqunW7zz14Twr204QsJp-nOUnXsCZGRgfyjEE.jpg','text'=>'Sunan Kuning-Semarang'],
        ['img'=>'http://1.bp.blogspot.com/-bAJ0_udZJeQ/TZTC-PPWLEI/AAAAAAAAAFQ/B6JwD7VmjG4/s1600/Cewek_dugem.jpg','text'=>'Cileugeng Indah (CI)-Indramayu'],
        ['img'=>'http://cdn.tmpo.co/data/2010/08/04/id_43127/43127_620.jpg','text'=>'Lokalisasi Dadap-Tangerang'],
        ['img'=>'http://majuteruz.files.wordpress.com/2010/07/gg-sadar-banyumas.jpg','text'=>'Gang Sadar I & II-Baturraden,Purwokerto'],
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c']
        ],
        'institusi'=>[
            ['name'=>'Gubernur DKI','img'=>'http://statik.tempo.co/data/2012/10/24/id_146866/146866_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/pemerintahprovinsipemprovdki54c1c6c8c3ef3 '],
            ['name'=>'DPRD DKI JAKARTA','img'=>'http://jurnalpatrolinews.com/wp-content/uploads/2015/02/DPRD-DKI-.jpg','url'=>'http://www.bijaks.net/aktor/profile/dprddki541a5b6fb78db']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrat5119a5b44c7e4']
        ],
        'institusi'=>[
            ['name'=>'MUI','img'=>'http://upload.wikimedia.org/wikipedia/id/f/f5/Logo_MUI.png','url'=>'http://www.bijaks.net/aktor/profile/majelisulamaindonesia53097864039ce'],
            ['name'=>'NU','img'=>'http://www.realita.co/photos/bigs/20141112200922nu.jpg','url'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTC_zvkC5V5Vo6LkBdv5iQ0AIFRbhdmJXXs8CH5rTTgQL2CCf72'],
            ['name'=>'Muhammadiyah','img'=>'http://www.daririau.com/foto_berita/55Muhammadiyah.jpg','url'=>'http://www.bijaks.net/aktor/profile/ppmuhammadiyah5296b45eefcaa'],
            ['name'=>'IMWU','img'=>'http://www.satuharapan.com/uploads/pics/news_31280_1423907247.jpg','url'=>''],
            ['name'=>'Komisi III','img'=>'http://risamariska.com/wp-content/uploads/2015/04/risamariska.com-logo-dprri-box.jpg','url'=>'http://www.bijaks.net/aktor/profile/komisiiiidprri54d30e3f5bbd7'],
            ['name'=>'DPD RI','img'=>'http://fokusberita.co/wp-content/uploads/2015/04/DPD-RI.jpg','url'=>'http://www.bijaks.net/aktor/profile/dewanperwakilandaerah531d2cecd99f7'],
            ['name'=>'Forensik Polri','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTC_zvkC5V5Vo6LkBdv5iQ0AIFRbhdmJXXs8CH5rTTgQL2CCf72','url'=>'']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Abraham Lingguna (H. Lulung)','jabatan'=>'Anggota DPRD DKI Jakarta','img'=>'http://slazhpardede.net/wp-content/uploads/2015/03/Haji-Lulung-1.jpeg','url'=>'http://www.bijaks.net/aktor/profile/lulunglunggana5114dd9dd23b1','content'=>'Saya tidak mau komentarin Ahok lagi yah. Kalau soal pernyataan Ahok tanya yang lain, jangan gue. Tapi jangan lah, jangan menuduh orang sembarangan. Kalau belum cukup bukti'],
        ['from'=>'Moammar Emka','jabatan'=>'Budayawan','img'=>'http://www.indonesianfilmcenter.com/images/gallery/Moammar%20Emka4.jpg','url'=>'http://www.bijaks.net/aktor/profile/moammaremka555ac075d83d1','content'=>'Murni karena gaya hidup. Ada sebagian yang karena faktor ekonomi, tapi kalangan bawah. Motivasinya agar lebih bergaya. Kalau kalangan bawah, untuk hidup (faktor ekonomi). Kalau kalangan atas, motivasinya untuk bikin hidup lebih bergaya'],
        ['from'=>'Farhat Abbas','jabatan'=>'Pengacara','img'=>'http://www.beranda.co.id/wp-content/uploads/2015/04/farhat-abbas1.jpg','url'=>'http://www.bijaks.net/aktor/profile/farhatabbas51cce8399d1cd','content'=>'Harusnya semua artis yang baik-baik, berkumpul dan protes dengan kejadian iklan artis jadi pelacur, bersihkan nama profesi mereka, pasti polisi tak ada bukti'],
        ['from'=>'Basuki Djahaja Purnama (Ahok)','jabatan'=>'Gubernur DKI Jakarta','img'=>'http://fajar.co.id/wp-content/uploads/2015/03/ahok13.jpg','url'=>'http://www.bijaks.net/aktor/profile/basukitjahajapurnama50f600df48ac5','content'=>'Yang makainya juga banyak oknum pejabat kok. Bisa bayar mahal mana ada duit (kalau bukan didapat dari hasil) duit korup. (Uang hasil) korup baru bisa pakai tuh (sewa PSK papan atas)'],
        ['from'=>'Fadli Zon','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://images.solopos.com/2014/06/Fadli-Zon-twitter.jpeg','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007','content'=>'Ini kan dari zaman Nabi Adam sudah ada prostitusi'],
        ['from'=>'Vicky Shu','jabatan'=>'Artis','img'=>'http://bengkuluekspress.com/wp-content/uploads/2012/06/vickyshu.jpg','url'=>'http://www.bijaks.net/aktor/profile/vickyveranitayudhasokavickyshu555ac4c4c82d9','content'=>'Hal itu bukan lagi menjadi rahasia umum di kalangan salebritis tanah air'],
        ['from'=>'AH Bimo Suryono','jabatan'=>'Praktisi hukum','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Boleh terlokalisasi asalkan jangan ada PSK di bawah umur. Kalau enggak berikan hukuman seberat-seberatnya seperti di negara lain'],
        ['from'=>'Maia Estianty','jabatan'=>'Artis','img'=>'http://4.bp.blogspot.com/-RhCY6cdaGYs/VBwUoEdLcDI/AAAAAAAABJw/wVZGHESuuP0/s1600/Rahasia%2BDiet%2Bala%2BArtis%2BMaia%2BEstianty.jpg','url'=>'http://www.bijaks.net/aktor/profile/maiaestiantiy555acea4b73d2','content'=>'Kemungkinan karena gaya hidup tinggi, job lagi sepi, dan dia ingin jalan pintas'],
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Ruhut Sitompul','jabatan'=>'Komisi III DPR RI','img'=>'http://cdn.tmpo.co/data/2013/09/24/id_222734/222734_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/ruhutpoltaksitompulsh50f91f7018b5c','content'=>'Kita (DPR) maunya ada aturan itu, tapi banyak yang tidak mau hal tersebut ada. Kalau mau dicoba pasti akan mentah'],
        ['from'=>'Din Syamsuddin','jabatan'=>'Ketua Umum PP Muhammadiyah','img'=>'http://www.rmolsumsel.com/images/berita/normal/734512_04071423122014_din-syamsudin.jpg','url'=>'http://www.bijaks.net/aktor/profile/dinsyamsuddin51c7bbdfbc558','content'=>'Kami tersentak adanya prostitusi di kalangan high class yang melibatkan tokoh yang berpengaruh di masyarakat dan kerap dijadikan idola'],
        ['from'=>'Said Aqil Siradj','jabatan'=>'Ketua Umum Pengurus Besar Nahdlatul Ulama (PBNU)','img'=>'http://1.bp.blogspot.com/-MXgWJ6FEe1k/U-QW-f_QtkI/AAAAAAAADnU/9tE8HsiVl30/s1600/KH+Said+Aqil+Siradj.jpg','url'=>'http://www.bijaks.net/aktor/profile/saidaqilsiradj5343c56457ee7','content'=>'Itu mengerikan, menjijikan, memalukan, ya pemerintah harus segera mengambil sikap tegas. Minimal meminimalisir'],
        ['from'=>'Hasanudin AF','jabatan'=>'Ketua Komisi Fatwa MUI','img'=>'http://www.halalmui.org/newMUI/images/content/KF_Hasanuddin.jpg','url'=>'','content'=>'Yang jelas namanya zina di dalam syariah Islam itu haram. Prostitusi online, PSK atau WTS dan istilah aneh lainnya, sampai pada istilah pria tuna susila itu sebuah perkembangan dari zina pada umumnya'],
        ['from'=>'Fahira Idris','jabatan'=>'Anggota DPD RI','img'=>'http://www.rmol.co/images/berita/normal/352170_03135816042015_fahira_idris1.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahirafahmiidris54eed6b448f2b','content'=>'Saya rasa RUU ini sudah sangat mendesak untuk dirumuskan dan dibahas bersama. Makanya DPD segera mengusulkan RUU ini kepada DPR RI'],
        ['from'=>'Tuti Alawiyah','jabatan'=>'Ketua International Moslem Women Union (IMWU)','img'=>'http://www.suara-islam.com/images/berita/tuty_alawiyah_20131220_175147.JPG','url'=>'http://www.bijaks.net/aktor/profile/tutyalawiyah555af3a0cfea2','content'=>'Pemerintah, polisi harus menindak tegas. Tenaga pendidik, sekolah harus ikut berpartisipasi untuk mencegah'],
        ['from'=>'Maya Rumantir','jabatan'=>'Anggota DPD','img'=>'http://cdn1-a.production.liputan6.static6.com/medias/229731/big/hot300803maya_logo.jpg','url'=>'http://www.bijaks.net/aktor/profile/mayarumantir555afa3d10e0c','content'=>'Banyak saat ini artis-artis sudah merubah motivasi sebenarnya jika menjadi artis. Mereka hanya tergiur dengan uang tanpa adanya karya yang dihasilkan'],
        ['from'=>'Reza Indragiri Amriel','jabatan'=>'Psikologi forensik','img'=>'http://www.jpnn.com/uploads/berita/dir08012010/img08012010561311.jpg','url'=>'http://www.bijaks.net/aktor/profile/rezaiindragiriamriel555b04f90c1fc','content'=>'Tapi sayang, sanksi sosial kita tidak bekerja. Justru (lewat kasus) ini untuk melontarkan (popularitas) mereka lebih tinggi'],
    ],
    'VIDEO'=>[
        ['id'=>'DTHmWGpwrTs'],
        ['id'=>'8pRTZfkoJV8'],
        ['id'=>'99eOTUEwM_Q'],
        ['id'=>'vWJ6iXgLFz0'],
        ['id'=>'YTZvA_zfZGM'],
        ['id'=>'FPTS0Eb10ak'],
        ['id'=>'aneAYinZFuA'],
        ['id'=>'zKeg53aUjww'],
        ['id'=>'uMQ1E5cnY7I'],
        ['id'=>'015Tfo-tkJY'],
        ['id'=>'MCx0n_nDFVE'],
        ['id'=>'Az1aqkqIQws'],
        ['id'=>'volBTH-IoU4'],
        ['id'=>'-QKG-eWc8lo']
    ],
    'FOTO'=>[
        ['img'=>'http://1.bp.blogspot.com/-54Oc5dFwt8E/UTYCagOmgYI/AAAAAAAAA5Y/PirNKR-EEzg/s1600/Adegan+Lelang+Ciuman+Nikita+Disaksikan+Pejabat+Negri+ini.jpg'],
        ['img'=>'http://pojoksatu.id/wp-content/uploads/2015/01/1-psk.jpg'],
        ['img'=>'http://3.bp.blogspot.com/-pEPtYBq-Bi8/URyt4AIKETI/AAAAAAAAAiA/D48dvWf_DAs/s1600/razia+PSK+masih+telanjang+.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/kupang/foto/bank/images/prostitusi-psk-buka-bra-bh.jpg'],
        ['img'=>'http://assets.kompasiana.com/statics/files/14300197371137491623.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/prostitusi-online_20150416_223802.jpg'],
        ['img'=>'http://fajar.co.id/wp-content/uploads/2015/04/psk1.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/04/29/340/1142192/begini-modus-prostitusi-online-via-bbm-di-bandung-lGQZQxboUS.jpg'],
        ['img'=>'http://www.mediaindonesia.com/assets/upload/newcontent/955_newcontentisi_tata2.jpg'],
        ['img'=>'http://2.bp.blogspot.com/-idMLviHidWQ/VTAn1zzur7I/AAAAAAAAFtU/BxuKfT6imU4/s1600/Prostitusi%2BOnline.jpg'],
        ['img'=>'http://warta.sumedang.info/wp-content/uploads/2015/05/Ilustrasi-prostitusi-online.jpg'],
        ['img'=>'http://www.hizbut-tahrir.or.id/wp-content/uploads/2015/04/prostitusi-online.jpg'],
        ['img'=>'http://stat.ks.kidsklik.com/statics/files/2013/06/1371971641690395271.png'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/Situs-prostitusi-online.jpg'],
        ['img'=>'http://kabargue.com/wp-content/uploads/2015/05/Daftar-Nama-Artis-Terlibat-Prostitusi-Online-Milik-Mucikari-RA.jpg'],
        ['img'=>'http://www.rmol.co/images/berita/normal/579721_04162821042015_psk.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/04/24/338/1139577/jadi-member-prostitusi-online-bayar-rp500-ribu-JSGi1XgnKa.jpg'],
        ['img'=>'http://cms.jakartapress.com/files/news/201302/prostitusi%20online_pic.jpg'],
        ['img'=>'http://beforeitsnews.com/mediadrop/uploads/2013/38/155bbb1458ba1e93bcaeb888ad54f28c1881397e.jpg'],
        ['img'=>'http://www.skanaa.com/assets/images/news/20150424/553a71e2a81bb75d1a8b4567.jpg'],
        ['img'=>'http://www.tabloidnova.com/var/gramedia/storage/images/media/images/prostitusi/9250818-1-ind-ID/prostitusi_reference.jpg'],
        ['img'=>'http://www.aktualpost.com/wp-content/uploads/2014/10/FB.jpg'],
        ['img'=>'http://edisimedan.com/wp-content/uploads/2015/05/amel-alvi1.jpg']
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }    
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/prostitusi/top_kcl.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GRATIFIKASI SEKS PEJABAT ?</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['GRATIFIKASI']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PSK ARTIS KELAS ATAS</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PSK']['narasi'];?></p>
                <ol style="list-style-type: square;margin-left: -30px !important;">
                    <?php
                    foreach ($data['PSK']['harga'] as $key => $val) {
                        echo "<li style='margin-left: 20px;' class='font_kecil'><b>".$val['no']."</b> - Rp. ".$val['jml']." Juta</li>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ATURAN PENJERAT</span></h5>
        <div id="accordion" class="panel-group" style="height: 340px;">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://sp.beritasatu.com/media/images/original/20130326125607133.jpg" class="col-xs-12 col-sm-6" style="width: 100%;">
                <div class="col-xs-12" style="margin-left: -15px;">
                    <p style="text-align: justify;">Indonesia salah satu negara yang melarang praktek prostitusi. Larangan tersebut sudah diatur dalam Pasal 296 KUHP.</p>
                    <ol style="list-style-type: square;">                        
                        <li style='margin-left: 0px;margin-right: 20px;font-size: 12px !important;'>“Barang siapa yang pencahariannya dan kebiasaannya yaitu dengan sengaja mengadakan atau memudahkan perbuatan cabul dengan orang lain dihukum penjara selama-lamanya satu tahun empat bulan atau denda sebanyak-banyaknya Rp. 15.000,-.”</li>
                    </ol>
                    <p style="text-align: justify;">dan Pasal 506 KUHP :</p>
                    <ol style="list-style-type: square;">
                        <li style='margin-left: 0px;margin-right: 20px;font-size: 12px !important;'>“Barang siapa sebagai mucikari (souteneur) mengambil untung dari pelacuran perempuan, dihukum kurungan selama-lamanya tiga bulan.”</li>
                    </ol>
                    <p style="text-align: justify;">Pasal penjerat yang berlaku sekarang belum mampu memberikan efek jerah para pelaku prostitusi karena masih lemahnya aturan tersebut. Hingga saat ini, aturan tersebut hanya berlaku untuk menjerat mucikari, sedangkan untuk PSK dan pelanggannya belum ada aturan yang mengaturnya. Tidak mengherankan kalau praktek-praktek prostitusi masih mewabah dan sulit untuk dikendalikan. Terkait dengan hal tersebut, DPR kembali mewacakan untuk merevisi UU tentang prostitusi.</p>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PROKONTRA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROKONTRA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">DAFTAR TEMPAT LOKALISASI DI INDONESIA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;">Berikut ini daftar kota dengan Lokalisasinya :</p>
                <ul>
                    <?php
                    foreach ($data['DAFTAR'] as $key => $val) { ?>
                        <li class="font_kecil" style='text-align: left;'><img src="<?php echo $val['img'];?>" class='pic3'><br><?php echo $val['text'];?></li><br>
                        <div class="clear"></div>
                    <?php
                    }
                    ?>
                <div class="clear"></div>
                </ul>
                <p style="text-align: justify;">Dan masih ada ratusan tempat lokalisasi yang tersebar di seluruh Indonesia.</p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">MODUS BARU PRAKTEK PROSTITUSI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['MODUS']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 style="padding-top: 15px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>



