<?php 
$kabinet = array(

    0 => ( 
        array ( 
            'kementrian_id' => 'kementeriansekretariatnegara53213a4c0309b', 
            'page_id' => 'pratikno540d1bf323949', 
            'large_img' => '',
            'seleksi' => array (
                0 => 'Maruarar', 
                1 => 'irhpramonoanungwibowomm50fb45f79eaff',
                2 => 'yuddychrisnandy5119c37be6d9f', 
                3 => 'aniesbaswedan521c38cd331a1'
            )
        )
    ),
    1 => ( 
        array ( 
            'kementrian_id' => 'kementerianperencanaanpembnasional5327c12a80069',
            'page_id' => 'andrinofachaniago52aa74d77395b',
            'large_img' => '',
            'seleksi' => array (
                0 => 'aviliani516b780a967b3', 1 => 'faisalbasri51b7f1e3d9a25',
                2 => 'revrisondbaswir51495a7da0ea5', 3 => 'rhenaldkasali540d3b1b7b7cd')
        )
    ),
    2 => ( 
        array ( 
            'kementrian_id' => 'kementeriankemaritiman5450603e4dd4e', 
            'page_id' => 'indroyonosoesilo544c9c8852c65', 
            'large_img' => '',
            'seleksi' => array ()
        )
    ),
    3 => ( 
        array ( 
            'kementrian_id' => 'kementerianperhubungan52fb1971da37b', 
            'page_id' => 'ignasiusjonan51517941433fd', 'large_img' => '',
            'seleksi' => array (
                0 => 'marsekaltnipurnchappyhakim541021c7acb39', 1 => 'danangparikesit5409276a0c5d1',
                2 => 'letjentnipurnsutiyoso511c2b6deeb46')
        )
    ),
    4 => ( 
        array ( 
            'kementrian_id' => 'kementeriankelautandanperikanan5327b0eae5745', 'page_id' => 'susipudjiastuti544ca163ad2f8', 'large_img' => '',
            'seleksi' => array (0 => 'jamaluddinjompa54101cb58292d', 1 => 'drkadarusmanphd54101addaabb8',
                2 => 'profdrirrokhmindahurims51888b6c709d5', 3 => 'drfadelmuhammad51132604d3a90')
        )
    ),
    5 => ( 
        array ( 
            'kementrian_id' => 'kementrianpdanekonomikreatif53265e1c8ab6b', 
            'page_id' => 'ariefyahya514a7e6babb0c', 'large_img' => '',
            'seleksi' => array ()
        )
    ),
    6 => ( 
        array ( 
            'kementrian_id' => 'kementerianesdm530d82b2c0ac2', 'page_id' => 'sudirmansaid544ca5d6974a8', 'large_img' => '',
            'seleksi' => array (0 => 'Arif', 1 => 'luluksumiarso514a86c1d0ea7',
                2 => 'irtumiran5410267edbd9e', 3 => 'hkurtubi5243b80d5cce5'),
        )
    ),
    7 => ( 
        array ( 
            'kementrian_id' => 'departemenhukumdanham5313f91a94735', 'page_id' => 'laksamanatnipurntedjoedhie540925438ba50', 'large_img' => '',
            'seleksi' => array (
                0 => 'jendralpurnluhutbinsarpanjaitan5189af75dc288', 1 => 'jenderaltnimoeldoko5227e0280b4dc',
                2 => 'letjentnipurnsutiyoso511c2b6deeb46', 3 => 'letnanjenderaltnibudiman5227e9114ec01'
            )
        )
    ),
    8 => ( 
        array ( 
            'kementrian_id' => 'kementriandalamnegeri531c1cbb6af24', 'page_id' => 'tjahjokumolosh50f4fc41d9f04', 'large_img' => '',
            'seleksi' => array (0 => 'drabrahamsamadshmh5192f6e1b2ac4', 1 => 'agustinterasnarangsh50fdffc977dba',
                2 => 'pratikno540d1bf323949', 3 => 'basukitjahajapurnama50f600df48ac5')
        )
    ),
    9 => ( 
        array ( 
            'kementrian_id' => 'kementerianluarnegeriindonesia531be6259f9cc', 'page_id' => 'retnolestaripriansarimarsudi544cafc267cc8', 'large_img' => '',
            'seleksi' => array (0 => 'dinopattidjalaldjalal521c3eb12ca04', 1 => 'makmurkeliat541017c8844d9',
                2 => 'martynatalegawa50ef6d0b48132', 3 => 'donkmarut541018e5091e5')            
        )
    ),
    10 => ( 
        array ( 
            'kementrian_id' => 'kementerianpertahanan5305bb682e70f', 'page_id' => 'jenderalryamizardryacudu52d35477b3460', 'large_img' => '',
            'seleksi' => array (0 => 'andiwidjajanto529828f684199', 1 => 'tbhasanuddinsemm50ee39facac7f',
                2 => 'prabowosubiyantodjojohadikusumo50c1598f86d91')
        )
    ),
    11 => ( 
        array ( 
            'kementrian_id' => 'kementerianhukumdanhakasasimanusia540e76759d3bb', 'page_id' => 'dryassonnahlaolyshmsc50f8bfa462238', 'large_img' => '',
            'seleksi' => array (0 => 'artidjoalkostar52c50a4e25b27', 1 => 'saldiisra540d1e8cb1477',
                2 => 'zainalarifinmochtar53155ff075894', 3 => 'drabrahamsamadshmh5192f6e1b2ac4')
        )
    ),
    12 => ( 
        array ( 
            'kementrian_id' => 'kementeriankominfo531eec3b4f618', 'page_id' => 'rudiantara5312a61d28aa6', 'large_img' => '',
            'seleksi' => array (0 => 'ferrymursyidanbaldan511b4ad3edaa1', 1 => 'nezarpatria51c91f6fa079e',
                2 => 'onnowpurbo540feb2a39ae3', 3 => 'suryapaloh511b4aa507a50')
        )
    ),
    13 => (
        array ( 
            'kementrian_id' => 'kementerianpanreformasibirokrasi536c957889937', 'page_id' => 'yuddychrisnandy5119c37be6d9f', 'large_img' => '',
            'seleksi' => array (0 => 'ekoprasojo540fd9a4d8dde', 1 => 'trirismaharini52db45dd476f9',
                2 => 'agungadiprasetyo54103f5e8a3d4', 3 => 'basukitjahajapurnama50f600df48ac5')
        )
    ),
    14 => ( 
        array ( 
            'kementrian_id' => 'kementeriankoordbperekonomian531fdb30b8d14', 'page_id' => 'drsofyanadjalilshmamald518c7b1b2b183', 'large_img' => '',
            'seleksi' => array (0 => 'chairultanjung51db5b0c9061a', 1 => 'dahlaniskan503d887d648d6',
                2 => 'gitawirjawan50f5159e39ce6', 3 => 'srimulyaniindrawatiphd5189d71174e32')
        )
    ),
    15 => ( 
        array ( 
            'kementrian_id' => 'kementeriankeuangan530eab5a7267e', 'page_id' => 'bambangbrodjonegoro51a7f8ee9f40b', 'large_img' => '',
            'seleksi' => array (0 => 'agusdwmartowardojo50ef76e597642', 1 => 'radenpardede52155b9191ba5',
                2 => 'profdrhendrawansupratikno5104f7c918e91', 3 => 'srimulyaniindrawatiphd5189d71174e32')
        )
    ),
    16 => ( 
        array ( 
            'kementrian_id' => 'badanusahamiliknegarabumn51f86c5b60980', 'page_id' => 'rinimarianisoemarnosoewandi54069b71b999c', 'large_img' => '',
            'seleksi' => array (0 => 'hendrisaparini541033901b044', 1 => 'hkurtubi5243b80d5cce5',
                2 => 'emirsyahsatar51514de7b36f1', 3 => 'dahlaniskan503d887d648d6')
        )
    ),
    17 => ( 
        array ( 
            'kementrian_id' => 'kementeriankoperasiukm5327bc6c71bf4', 'page_id' => 'aanpuspayoga50fcc5a677632', 'large_img' => '',
            'seleksi' => array (0 => 'abdulkadirkarding5285dbba60e22', 1 => 'drakhofifahindarparawansa5189c941e1c45',
                2 => 'nusronwahid5109b917e9be0', 3 => 'rhenaldkasali540d3b1b7b7cd')
        )
    ),
    18 => ( 
        array ( 
            'kementrian_id' => 'kementerianperindustriankemenperin5326593d4a5c9', 'page_id' => 'salehhusinsemsi50fcac485a24d', 'large_img' => '',
            'seleksi' => array (0 => 'antonjoenoessupit541027343805b', 1 => 'poempidahidayatulloh526a0a87b1aa5',
                2 => 'triyogiyuwono54103bfbad7ae', 3 => 'nusronwahid5109b917e9be0')
        )
    ),
    19 => ( 
        array ( 
            'kementrian_id' => 'menteriperdaganganindonesia51d4bf0de10bf', 'page_id' => 'rahmatgobel544cc9228ca19', 'large_img' => '',
            'seleksi' => array (0 => 'drmarielkapangestu50ef8750d3096', 1 => 'soetrisnobachir53951be843c51',
                2 => 'sriadiningsih54102a1b1ed1b', 3 => 'gitawirjawan50f5159e39ce6')
        )
    ),
    20 => ( array ( 
        'kementrian_id' => 'kementrianpertanian531d2f81a4829', 'page_id' => 'amransulaiman544cc6fce57c6', 'large_img' => '',
        'seleksi' => array (0 => 'arifwibowo50f8c2f747be3', 1 => 'bustanilarifin5186083b8fe1e',
                2 => 'imansugema54102e88232e0', 3 => 'prabowosubiyantodjojohadikusumo50c1598f86d91')
        )
    ),
    21 => ( array ( 
        'kementrian_id' => 'kemenakertrans51e226ab3bd2c', 'page_id' => 'drsmuhhanifdhakiri512c7094f15d8', 'large_img' => '',
        'seleksi' => array (0 => 'riekediahpitaloka51090e9289dc8', 1 => 'rizalsukma540922da87922',
                2 => 'wahyususilo540ffc5ada001', 3 => 'muhaiminiskandar50ef9d0b6f4d5')
    )),
    22 => ( array ( 
        'kementrian_id' => 'kementerianpekerjaanumum531c1a8f4d5a3', 'page_id' => 'mbasukihadimuljono535e4c9155a29', 'large_img' => '',
        'seleksi' => array (0 => 'drbayukrisnamurthimsi50f5319d5e336', 1 => 'ilhamakbarhabibie540d573150b04',
                2 => 'trimumpuniwiyatno540ff5d75d0aa', 3 => 'irdjokokirmantodiplhe51a7053b57f68')
    )),
    23 => ( array ( 
        'kementrian_id' => 'kementerianlingkunganhidupri52e48dc2f0ae1', 'page_id' => 'sitinurbaya51e6043555e9b', 'large_img' => '',
        'seleksi' => array (0 => 'chalidmuhammad540fdc050b449', 1 => 'charlieheatubun5419366da2629',
                2 => 'widodosambodo540d3be87bbba', 3 => 'profdremilsalim5185e3157caf7')
    )),
    24 => ( array ( 
        'kementrian_id' => 'badanpertanahannasionalbpn5372f59c2c42c', 'page_id' => 'ferrymursyidanbaldan511b4ad3edaa1', 'large_img' => '',
        'seleksi' => array()
    )),
    25 => ( array ( 
        'kementrian_id' => 'kemenkopmanusiadankebudayaan5450637c52c78', 'page_id' => 'puanmaharani5104bbe40ed12', 'large_img' => '',
        'seleksi' => array()
    )),
    26 => ( array ( 
        'kementrian_id' => 'kementerianagama51dd066da966d', 'page_id' => 'drshlukmanhakimsaifuddin511756084eb20', 'large_img' => '',
        'seleksi' => array (0 => 'azyumardiazra540eb6c058c3c', 1 => 'musdahmulia540969ae97434', 2 => 'dralwiabdurrahmanshihab5170c3e79726b')
    )),
    27 => ( array ( 
        'kementrian_id' => 'kementeriankesehatan52f9bdfc6e729', 'page_id' => 'niladjuwitaanfasamoeloek544ccde20164c', 'large_img' => '',
        'seleksi' => array (0 => 'faslijalal540d58eb38418', 1 => 'drribkatjiptaning5108fe0ca2b10',
                2 => 'profdralighufrommuktimscphd50f53b79bf202', 3 => 'drdrsitifadilahsuparispjpk518c78b852b69')
    )),
    28 => ( array ( 
        'kementrian_id' => 'kementeriansosial530d8208a2c5a', 'page_id' => 'drakhofifahindarparawansa5189c941e1c45', 'large_img' => '',
        'seleksi' => array (0 => 'dadangjuliantara540ff02a2b10a', 1 => 'draevakusumasundarimamde50f91dd8e99b3',
                2 => 'hastokristiyanto5301bd1343234', 3 => 'nusronwahid5109b917e9be0')
    )),
    29 => ( array ( 
        'kementrian_id' => 'kementerianpemberdayaanpdanpanak5327af3e64264', 'page_id' => 'yohanayembise5260b1a944df9', 'large_img' => '',
        'seleksi' => array (0 => 'liesmarcoesnatsir540fde511450d', 1 => 'nanizulminarni540fdd54347cd',
                2 => 'puanmaharani5104bbe40ed12', 3 => 'drakhofifahindarparawansa5189c941e1c45')
        )),
    30 => ( array ( 
        'kementrian_id' => 'kementerianpendidikandankebudayaan530d82f25081c', 'page_id' => 'aniesbaswedan521c38cd331a1', 'large_img' => '',
        'seleksi' => array (0 => 'abdulmunirmulkhan51a569fe45387', 1 => 'hilmarfarid540ff167e2966',
                2 => 'yudilatif540d30ec826e9')
    )),
    31 => ( array ( 
        'kementrian_id' => 'kementerianrisetdanteknologi5327c284810b7', 'page_id' => 'muhammadnasir544dd3dae9b49', 'large_img' => '',
        'seleksi' => array (0 => 'gedewenten540fe573a302b', 1 => 'romisatriawahono540fe4d00bbfd',
                2 => 'yohannessurya540fe3677f517', 3 => 'ilhamakbarhabibie540d573150b04')
    )),
    32 => ( array ( 
        'kementrian_id' => 'kemenpora52f05885a7efd', 'page_id' => 'himamnahrawisag50fcad865ab41', 'large_img' => '',
        'seleksi' => array (0 => 'adhiems5410349908e89', 1 => 'aniesbaswedan521c38cd331a1',
                2 => 'herryzudianto5410359c07df0', 3 => 'drsututadianto5109157318565')
    )),
    33 => ( array ( 
        'kementrian_id' => 'kementerianpembangunandtertinggal5327b8371673e', 'page_id' => 'hmarwanjafarsesh50f8f52b84acd', 'large_img' => '',
        'seleksi' => array (0 => 'drsakbarfaizalmsi50f778b33337e', 1 => 'andrinofachirchaniago540d3faf44682',
                2 => 'indrajayapiliang5215b4986acc3', 3 => 'budimansudjatmikomscmphil50f8c130dca48')
    )),

);


?>
<!--<div class="row">
    <div class="col-xs-12">
        <img src="<?php echo base_url().'assets/images/spalsh-kabinet-2014.jpg';?>" class="lazy" style="width: 100%;">
    </div>
</div>-->
<?php 
foreach($kabinet as $key => $val):
    $institusi = $this->redis_slave->get('profile:detail:'.$val['kementrian_id']);
    $insarr = json_decode($institusi, true);
    $img_institusi = (count($insarr['profile_photo']) > 0) ? $insarr['profile_photo'][0]['badge_url'] : '';
    $about = strip_tags($insarr['about'], '<p>');
    
    $scandal_count = array(); $news_count = array(); $follower_count = array();$photo = array();$_color = array();$_backcolor = array();

    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
    $personarr = json_decode($person, true);
    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

    $where = array('page_id' => $personarr['page_id']);
    $scandal_count = count($personarr['scandal']);
    //$news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
    //$news_count = $news_terkait['cou'];
    //$foll = $this->aktor_lib->get_follower($personarr['page_id']);
    //$follower_count = $foll->num_rows();

    switch($personarr['partai_id'])
    {
        case 'nasdem5119b72a0ea62' : $_backcolor = "#210068"; $_color = "#000000"; break;
        case 'partaiamanatnasional5119b55ab5fab' : $_backcolor = "#310089"; $_color = "#000000"; break;
        case 'partaidemokrasiindonesiaperjuangan5119ac6bba0ddb' : $_backcolor = "#FF0000"; $_color = "#000000"; break;
        case 'partaikebangkitanbangsa5119b257621a4' : $_backcolor = "#00540D"; $_color = "#000000"; break;
        case 'partaikeadilansejahtera5119b06f84fef' : $_backcolor = "#000000"; $_color = "#ffffff"; break;
        case 'partaigerakanindonesiarayagerindra5119a4028d14c' : $_backcolor = "#993233"; $_color = "#000000"; break;
        case 'partaigolongankarya5119aaf1dadef' : $_backcolor = "#F1FF00"; $_color = "#000000"; break;
        case 'partaihatinuranirakyathanura5119a1cb0fdc1' : $_backcolor = "#FE7E00"; $_color = "#000000"; break;
        case 'partaikeadialndnpersatuanindonesia5119bd7483d2a' : $_backcolor = "#E76241"; $_color = "#000000"; break;
        case 'partaipersatuan518c5908a9e47' : $_backcolor = "#007100"; $_color = "#000000"; break;
        case 'partaibulanbintang52155330a9408' : $_backcolor = "#488774"; $_color = "#000000"; break;

        default :
            $_backcolor = "#cecece"; $_color = "#000000";break;
    }

    if($personarr['page_id'] == 'basukitjahajapurnama50f600df48ac5') { $_backcolor = "#cecece"; $_color = "#000000";}
?>    
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <h4 class="kanal-title kanal-title-gray">
                    <a href="<?php echo base_url('aktor/profile/') .'/'. $insarr['page_id'];?>"><?php echo strtoupper($insarr['page_name']);?></a>
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <?php echo character_limiter(nl2br($about), 150);?> [<a href="<?php echo base_url('aktor/profile/') .'/'. $insarr['page_id'];?>">selengkapnya</a>]
            </div>
            <div class="col-xs-4">
                <img style=" background-color: #bbbbbb;width: 100%;height: auto;" src="<?php echo $img_institusi;?>" class="lazy">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4">
                <img src="<?php echo (!empty($val['large_img']) ? $val['large_img'] : $photo);?>" style="width: 100%;border-radius: 6px 6px 6px 6px;" class="lazy">
            </div>
            <div class="col-xs-8">
                <h4>
                    <a style="color:<?php echo $_color;?> !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                </h4>
                <p><?php echo character_limiter(nl2br($personarr['about']), 200);?>  [<a href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>">selengkapnya</a>]</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <h4>Kandidat yang diprediksi:</h4>
            </div>
        </div>
        <div class="row">
        <?php 
        foreach($val['seleksi'] as $key => $row):
            $person = $this->redis_slave->get('profile:detail:'.$row);
            $personarr = json_decode($person, true);
            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

            $where = array('page_id' => $personarr['page_id']);
            $scandal_count = count($personarr['scandal']);
            //$news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
            //$news_count = $news_terkait['cou'];
            $foll = $this->aktor_lib->get_follower($personarr['page_id']);
            $follower_count = $foll->num_rows();
            switch($personarr['partai_id'])
            {
                case 'nasdem5119b72a0ea62' : $_backcolor = "#210068"; $_color = "#000000"; break;
                case 'partaiamanatnasional5119b55ab5fab' : $_backcolor = "#310089"; $_color = "#000000"; break;
                case 'partaidemokrasiindonesiaperjuangan5119ac6bba0ddb' : $_backcolor = "#FF0000"; $_color = "#000000"; break;
                case 'partaikebangkitanbangsa5119b257621a4' : $_backcolor = "#00540D"; $_color = "#000000"; break;
                case 'partaikeadilansejahtera5119b06f84fef' : $_backcolor = "#000000"; $_color = "#ffffff"; break;
                case 'partaigerakanindonesiarayagerindra5119a4028d14c' : $_backcolor = "#993233"; $_color = "#000000"; break;
                case 'partaigolongankarya5119aaf1dadef' : $_backcolor = "#F1FF00"; $_color = "#000000"; break;
                case 'partaihatinuranirakyathanura5119a1cb0fdc1' : $_backcolor = "#FE7E00"; $_color = "#000000"; break;
                case 'partaikeadialndnpersatuanindonesia5119bd7483d2a' : $_backcolor = "#E76241"; $_color = "#000000"; break;
                case 'partaipersatuan518c5908a9e47' : $_backcolor = "#007100"; $_color = "#000000"; break;
                case 'partaibulanbintang52155330a9408' : $_backcolor = "#488774"; $_color = "#000000"; break;

                default :
                    $_backcolor = "#cecece"; $_color = "#000000";break;
            }
            if($personarr['page_id'] == 'basukitjahajapurnama50f600df48ac5') { $_backcolor = "#cecece"; $_color = "#000000"; }
        ?>  
            <div class="col-xs-3">
                <div>
                    <a href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>">
                    <img style="background-color: #bbbbbb;border-radius: 6px 6px 6px 6px;width: 5em;" src="<?php echo $photo;?>" class="lazy">
                    </a>
                </div>
                <h4 style="font-size: .8em;">
                   <a href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>" style="color:<?php echo $_color;?> !important;"><?php echo strtoupper($personarr['page_name']);?></a>
                </h4>
            </div>
            <?php /*
            <div style="width: 177px;margin-bottom: 5px;text-align: left;">
                <a class="a-block" style="background-color: #cecece;width: 172px;" href="<?php echo base_url();?>aktor/scandals/<?php echo $personarr['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $scandal_count;?></span></a>
                <a class="a-block" style="width: 172px;" href="<?php echo base_url();?>aktor/news/<?php echo $personarr['page_id'];?>">BERITA <span class="clearfix"><?php echo $news_count;?></span></a>
                <a class="a-block" style="background-color: #cecece;width: 172px;"  href="<?php echo base_url();?>aktor/follow/<?php echo $personarr['page_id'];?>">FOLLOWER <span class="clearfix"><?php echo $follower_count;?></span></a>
            </div>
             */?>
<!--            <div style="float: left;width:177px;height: 320px; background-color: #EEEEF4;border-radius: 10px;margin-left:5px;border: solid 4px <?php echo $_backcolor; ?>;">
                <div style="width: 177px;height: 180px;text-align: center;margin-bottom: 5px;">
                    <img style=" background-color: #bbbbbb;width: 177px;height: 177px;border-radius: 6px 6px 0 0;" src="<?php echo $photo;?>">
                </div>

                <h4 style="line-height: 18px;height:55px;padding-left:5px;padding-right:5px;margin-bottom:5px;font-size: 14.5px;">
                   <a href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>" style="color:<?php echo $_color;?> !important;"><?php echo strtoupper($personarr['page_name']);?></a>
                </h4>

                <div style="width: 177px;margin-bottom: 5px;text-align: left;">
                   <a class="a-block" style="background-color: #cecece;width: 172px;" href="<?php echo base_url();?>aktor/scandals/<?php echo $personarr['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $scandal_count;?></span></a>
                   <a class="a-block" style="width: 172px;" href="<?php echo base_url();?>aktor/news/<?php echo $personarr['page_id'];?>">BERITA <span class="clearfix"><?php echo $news_count;?></span></a>
                   <a class="a-block" style="background-color: #cecece;width: 172px;"  href="<?php echo base_url();?>aktor/follow/<?php echo $personarr['page_id'];?>">FOLLOWER <span class="clearfix"><?php echo $follower_count;?></span></a>
                </div>
            </div>            -->
        <?php    
        endforeach;
        ?>
            
        </div>
        
    </div>
</div>
<?php
    endforeach;
?>

<?php /*
<?php

$kabinet = array(

    0 => ( array ( 'kementrian_id' => 'kementeriansekretariatnegara53213a4c0309b', 'page_id' => 'pratikno540d1bf323949', 'large_img' => '')),
    1 => ( array ( 'kementrian_id' => 'kementerianperencanaanpembnasional5327c12a80069', 'page_id' => 'andrinofachaniago52aa74d77395b', 'large_img' => '')),
    2 => ( array ( 'kementrian_id' => 'kementeriankemaritiman5450603e4dd4e', 'page_id' => 'indroyonosoesilo544c9c8852c65', 'large_img' => '')),
    3 => ( array ( 'kementrian_id' => 'kementerianperhubungan52fb1971da37b', 'page_id' => 'ignasiusjonan51517941433fd', 'large_img' => '')),
    4 => ( array ( 'kementrian_id' => 'kementeriankelautandanperikanan5327b0eae5745', 'page_id' => 'susipudjiastuti544ca163ad2f8', 'large_img' => '')),
    5 => ( array ( 'kementrian_id' => 'kementrianpdanekonomikreatif53265e1c8ab6b', 'page_id' => 'ariefyahya514a7e6babb0c', 'large_img' => '')),
    6 => ( array ( 'kementrian_id' => 'kementerianesdm530d82b2c0ac2', 'page_id' => 'sudirmansaid544ca5d6974a8', 'large_img' => '')),
    7 => ( array ( 'kementrian_id' => 'departemenhukumdanham5313f91a94735', 'page_id' => 'laksamanatnipurntedjoedhie540925438ba50', 'large_img' => '')),
    8 => ( array ( 'kementrian_id' => 'kementriandalamnegeri531c1cbb6af24', 'page_id' => 'tjahjokumolosh50f4fc41d9f04', 'large_img' => '')),
    9 => ( array ( 'kementrian_id' => 'kementerianluarnegeriindonesia531be6259f9cc', 'page_id' => 'retnolestaripriansarimarsudi544cafc267cc8', 'large_img' => '')),
    10 => ( array ( 'kementrian_id' => 'kementerianpertahanan5305bb682e70f', 'page_id' => 'jenderalryamizardryacudu52d35477b3460', 'large_img' => '')),
    11 => ( array ( 'kementrian_id' => 'kementerianhukumdanhakasasimanusia540e76759d3bb', 'page_id' => 'dryassonnahlaolyshmsc50f8bfa462238', 'large_img' => '')),
    12 => ( array ( 'kementrian_id' => 'kementeriankominfo531eec3b4f618', 'page_id' => 'rudiantara5312a61d28aa6', 'large_img' => '')),
    13 => ( array ( 'kementrian_id' => 'kementerianpanreformasibirokrasi536c957889937', 'page_id' => 'yuddychrisnandy5119c37be6d9f', 'large_img' => '')),
    14 => ( array ( 'kementrian_id' => 'kementeriankoordbperekonomian531fdb30b8d14', 'page_id' => 'drsofyanadjalilshmamald518c7b1b2b183', 'large_img' => '')),
    15 => ( array ( 'kementrian_id' => 'kementeriankeuangan530eab5a7267e', 'page_id' => 'bambangbrodjonegoro51a7f8ee9f40b', 'large_img' => '')),
    16 => ( array ( 'kementrian_id' => 'badanusahamiliknegarabumn51f86c5b60980', 'page_id' => 'rinimarianisoemarnosoewandi54069b71b999c', 'large_img' => '')),
    17 => ( array ( 'kementrian_id' => 'kementeriankoperasiukm5327bc6c71bf4', 'page_id' => 'aanpuspayoga50fcc5a677632', 'large_img' => '')),
    18 => ( array ( 'kementrian_id' => 'kementerianperindustriankemenperin5326593d4a5c9', 'page_id' => 'salehhusinsemsi50fcac485a24d', 'large_img' => '')),
    19 => ( array ( 'kementrian_id' => 'menteriperdaganganindonesia51d4bf0de10bf', 'page_id' => 'rahmatgobel544cc9228ca19', 'large_img' => '')),
    20 => ( array ( 'kementrian_id' => 'kementrianpertanian531d2f81a4829', 'page_id' => 'amransulaiman544cc6fce57c6', 'large_img' => '')),
    21 => ( array ( 'kementrian_id' => 'kemenakertrans51e226ab3bd2c', 'page_id' => 'drsmuhhanifdhakiri512c7094f15d8', 'large_img' => '')),
    22 => ( array ( 'kementrian_id' => 'kementerianpekerjaanumum531c1a8f4d5a3', 'page_id' => 'mbasukihadimuljono535e4c9155a29', 'large_img' => '')),
    23 => ( array ( 'kementrian_id' => 'kementerianlingkunganhidupri52e48dc2f0ae1', 'page_id' => 'sitinurbayabakar544dcebbe8b6f', 'large_img' => '')),
    24 => ( array ( 'kementrian_id' => 'badanpertanahannasionalbpn5372f59c2c42c', 'page_id' => 'ferrymursyidanbaldan511b4ad3edaa1', 'large_img' => '')),
    25 => ( array ( 'kementrian_id' => 'kemenkopmanusiadankebudayaan5450637c52c78', 'page_id' => 'puanmaharani5104bbe40ed12', 'large_img' => '')),
    26 => ( array ( 'kementrian_id' => 'kementerianagama51dd066da966d', 'page_id' => 'drshlukmanhakimsaifuddin511756084eb20', 'large_img' => '')),
    27 => ( array ( 'kementrian_id' => 'kementeriankesehatan52f9bdfc6e729', 'page_id' => 'niladjuwitaanfasamoeloek544ccde20164c', 'large_img' => '')),
    28 => ( array ( 'kementrian_id' => 'kementeriansosial530d8208a2c5a', 'page_id' => 'drakhofifahindarparawansa5189c941e1c45', 'large_img' => '')),
    29 => ( array ( 'kementrian_id' => 'kementerianpemberdayaanpdanpanak5327af3e64264', 'page_id' => 'yohanayembise5260b1a944df9', 'large_img' => '')),
    30 => ( array ( 'kementrian_id' => 'kementerianpendidikandankebudayaan530d82f25081c', 'page_id' => 'aniesbaswedan521c38cd331a1', 'large_img' => '')),
    31 => ( array ( 'kementrian_id' => 'kementerianrisetdanteknologi5327c284810b7', 'page_id' => 'muhammadnasir544dd3dae9b49', 'large_img' => '')),
    32 => ( array ( 'kementrian_id' => 'kemenpora52f05885a7efd', 'page_id' => 'himamnahrawisag50fcad865ab41', 'large_img' => '')),
    33 => ( array ( 'kementrian_id' => 'kementerianpembangunandtertinggal5327b8371673e', 'page_id' => 'hmarwanjafarsesh50f8f52b84acd', 'large_img' => '')),

);

?>


<br/>
<div class="container" style="margin-top:9px;">
<div class="sub-header-container">
    <div>
        <img src="<?php echo base_url().'assets/images/spalsh-kabinet-2014.jpg';?>">
    </div>
    <div style="margin-top:10px;/*background: transparent;* /">
        <?php
        foreach($kabinet as $key => $val)
        {
            $institusi = $this->redis_slave->get('profile:detail:'.$val['kementrian_id']);
            $insarr = json_decode($institusi, true);

            $img_institusi = (count($insarr['profile_photo']) > 0) ? $insarr['profile_photo'][0]['badge_url'] : '';
            ?>
            <div class="row-fluid" style="background-color: #ffffff;background: transparent;padding-bottom:20px;border-bottom: solid 2px #BBBBBB;margin-bottom: 10px;">
                <div style="width:960px;height: 200px;;background: transparent;">

                    <div style="float:left; width: 745px;margin-left:10px;min-height: 200px;height: 235px;overflow: hidden;">
                        <h3 style="margin: 0px;line-height: 30px;">
                            <a href="<?php echo base_url('aktor/profile/') .'/'. $insarr['page_id'];?>"><?php echo strtoupper($insarr['page_name']);?></a>
                        </h3>
                        <div class="clearfix"></div>
                        <?php $about = strip_tags($insarr['about'], '<p>'); ?>
                        <p style="padding-right: 10px;"><?php echo character_limiter(nl2br($about), 400);?> [<a href="<?php echo base_url('aktor/profile/') .'/'. $insarr['page_id'];?>">selengkapnya</a>]</p>

                    </div>

                    <div style="float:left; width: 170px;min-height: 200px;height: auto;background: transparent;">
                        <img style=" background-color: #bbbbbb;width: 170px;height: auto;" src="<?php echo $img_institusi;?>">
                    </div>

                </div>
<?php
                    $scandal_count = array(); $news_count = array(); $follower_count = array();$photo = array();$_color = array();$_backcolor = array();

                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';

                    $where = array('page_id' => $personarr['page_id']);
                    $scandal_count = count($personarr['scandal']);
                    $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                    $news_count = $news_terkait['cou'];
                    $foll = $this->aktor_lib->get_follower($personarr['page_id']);
                    $follower_count = $foll->num_rows();

                    switch($personarr['partai_id'])
                    {
                        case 'nasdem5119b72a0ea62' : $_backcolor = "#210068"; $_color = "#000000"; break;
                        case 'partaiamanatnasional5119b55ab5fab' : $_backcolor = "#310089"; $_color = "#000000"; break;
                        case 'partaidemokrasiindonesiaperjuangan5119ac6bba0ddb' : $_backcolor = "#FF0000"; $_color = "#000000"; break;
                        case 'partaikebangkitanbangsa5119b257621a4' : $_backcolor = "#00540D"; $_color = "#000000"; break;
                        case 'partaikeadilansejahtera5119b06f84fef' : $_backcolor = "#000000"; $_color = "#ffffff"; break;
                        case 'partaigerakanindonesiarayagerindra5119a4028d14c' : $_backcolor = "#993233"; $_color = "#000000"; break;
                        case 'partaigolongankarya5119aaf1dadef' : $_backcolor = "#F1FF00"; $_color = "#000000"; break;
                        case 'partaihatinuranirakyathanura5119a1cb0fdc1' : $_backcolor = "#FE7E00"; $_color = "#000000"; break;
                        case 'partaikeadialndnpersatuanindonesia5119bd7483d2a' : $_backcolor = "#E76241"; $_color = "#000000"; break;
                        case 'partaipersatuan518c5908a9e47' : $_backcolor = "#007100"; $_color = "#000000"; break;
                        case 'partaibulanbintang52155330a9408' : $_backcolor = "#488774"; $_color = "#000000"; break;

                        default :
                            $_backcolor = "#cecece"; $_color = "#000000";break;
                    }

                    if($personarr['page_id'] == 'basukitjahajapurnama50f600df48ac5') { $_backcolor = "#cecece"; $_color = "#000000";}

?>
                    <div class="row-fluid" style="height: auto;background: url('<?php echo (!empty($val['large_img']) ? $val['large_img'] : $photo);?>') no-repeat;">
                        <div style="float:left;width: 350px;height: 470px;">
                            <!-- img style="width: 300px;height: auto" src="< ?php echo $val['large_img'];?>"-->
                            <div style="width: 350px;height: 400px;"></div>
                            <div style="background-color: #000000;margin-top: 0px;width: 300px;margin-left:30px;opacity:0.6">
                                <a class="a-block a-block-1" style="width: 300px;background: transparent;color: #ffffff;" href="<?php echo base_url();?>aktor/scandals/<?php echo $personarr['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $scandal_count;?></span></a>
                                <a class="a-block" style="width: 300px;background: transparent;color: #ffffff;" href="<?php echo base_url();?>aktor/news/<?php echo $personarr['page_id'];?>">BERITA <span class="clearfix"><?php echo $news_count;?></span></a>
                                <a class="a-block a-block-1" style="width: 300px;background: transparent;color: #ffffff;" href="<?php echo base_url();?>aktor/follow/<?php echo $personarr['page_id'];?>">FOLLOWER <span class="clearfix"><?php echo $follower_count;?></span></a>
                                <div style="background-color: <?php echo $_backcolor;?>;width: 300px;height: 10px;"></div>
                            </div>
                        </div>

                        <div style="float:left;width: 590px;margin-top:-50px;">
                            <h4 style="line-height: 18px;font-size: 14.5px;margin-bottom: 5px;">
                                <a style="color:<?php echo $_color;?> !important;" href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>"><?php echo strtoupper($personarr['page_name']);?></a>
                            </h4>
                            <p><?php echo character_limiter(nl2br($personarr['about']), 2000);?>  [<a href="<?php echo base_url('aktor/profile/') .'/'. $personarr['page_id'];?>">selengkapnya</a>]</p>

                        </div>

                    </div>
            </div>

        <?php
        }


        ?>

    </div>
</div>
</div>
 */?>