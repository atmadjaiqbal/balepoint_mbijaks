<?php 
$data = [
    'NARASI'=>[
        'title'=>'MEMBONGKAR PEMBEGAL DANA SILUMAN',
        'narasi'=>'<img src="http://www.fastnewsindonesia.com/sites/default/files/styles/article_image/public/field/image/278585_basuki-tjahaja-purnama.jpeg?itok=XyAN8uMJ" class="pic">Keterangan Gubernur DKI Jakarta, Basuki Tjaja Purnama (Ahok) terkait adanya penggelembungan dana ilegal  RAPBD DKI yang berlangsung sejak tahun 2012 hingga 2015 menuai polemik yang semakin meluas. Adanyana siluman yang dilontarkan Gubernur Ahok spontan mendapat reaksi dari DPRD DKI Jakarta. Mereka mempermasalahkan pernyataan mantan politisi partai Gerindera tersebut. Kisruh berawal ketika Ahok dan DPRD bersitegang lantaran ditemukannya  dana siluman dalam rancangan APBD 2015 yang diselundupkan oleh Dewan. Ahok menyebutkan nilai dana siluman tersebut sebesar Rp 12,1 triliun. Selanjutnya, Anggaran pendapatan belanja daerah (APBD) DKI 2015 tidak kunjung cair.
                </p><p style="text-align: justify;"><img src="http://cdn0-a.production.liputan6.static6.com/medias/793755/big/072011800_1421056562-Ahok_Sidang_Paripurna3.jpg" class="pic2">Ketegangan semakin memanas ketika terjadi ketidaksepakatan antara pihak Pemprov DKI dan DPRD DKI dalam penyerahan dokumen Rancangan Anggaran Pendapatan Belanja Daerah (RAPBD). Masing-masing pihak mengklaim memiliki dokumen sah dan sesuai aturan yang berlaku. Sehingga Kementerian Dalam Negeri (Kemendagri) turun tangan untuk menyelesaikan persoalan tersebut melalui jalan mediasi, namun hasilnya nihil dan berakhir ricuh.'
    ],
    'PROFIL'=>[
        'nama'=>'Ir. Basuki Tjahaja Purnama MM',
        'agama'=>'Kristen',
        'tmp'=>'Manggar, Bangka Belitung',
        'tgl'=>'Rabu, 29 Juni 1966',
        'jejak'=>[
            ['no'=>'Anggota Komisi II DPR RI, 2009 - 2014.'],
            ['no'=>'Direktur Eksekutif Center for Democracy and Transparency (CDT.3.1).'],
            ['no'=>'Bupati Belitung Timur, 2005 - 2006.'],
            ['no'=>'Anggota DPRD Belitung Timur bidang Komisi Anggaran, 2005 - 2006.'],
            ['no'=>'Asisten Presiden Direktur bidang analisa biaya dan keuangan PT. Simaxindo Primadaya, Jakarta, 1994 - 1995.'],
            ['no'=>'Direktur PT. Nurindra Ekapersada, Belitung Timur, 1992 - 2005.'],
            ['no'=>'Wakil Gubernur DKI Jakarta (2012).'],
            ['no'=>'Gubernur DKI Jakarta (2014).']
        ]
    ],
    'PROKONTRA'=>[
        'narasi'=>'Polemik berkepanjang di pemerintahan DKI Jakarta secara otomatis menuai pro dan kontra di masyarakat khususnya di Jakarta. Dukungan ataupun bentuk simpatik mengalir dari beragam elemen masyarakat. Ada yang mendukung langkah yang ditempuh oleh Gubernur merupakan langkah yang sudah tepat. Namun adapula yang memberikan dukungan terhadap DPRD.',
        'dukungan'=>'Berbagai dukungan mengalir untuk Gubernur Ahok, dukungan ini bukan tanpa alasan. Dukungan tersebut  muncul dari masyarakat umum, LSM, dan kalangan akademisi. Mereka menilai Gubernur Ahok harus didukung sebagai bentuk komitmen bersama dalam memberantas dan  membersihkan pemerintah dari praktek-praktek korupsi yang merugikan masyarakat'
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'nasdem5119b72a0ea62'],
        ],
        'institusi'=>[
            ['name'=>'Menteri Pemberdayaan Aparatur Negara dan Reformasi Birokrasi','img'=>'http://www.dutakarya-pu.com/wp-content/uploads/2015/02/LKAN.png','url'=>''],
            ['name'=>'Dukungan terhadap Gubernur DKI Jakarta Basuki Tjahaja Purnama atau Ahok terkait kisruh APBD 2015 semakin meningkat. Dalam petisi online www. change.org,   lebih    dari 50 ribu orang mendukung penerapan e-budgeting yang dilakukan Ahok dan mendesak rakyat Jakarta mencabut mandat DPRD.','img'=>'http://blogs-images.forbes.com/erikkain/files/2014/12/Change.org_.gif','url'=>''],
            ['name'=>'Selain dukungan lewat dunia maya, para pendukung Ahok juga akan turun ke jalan besok pagi di tengah Car Free Day di sekitar Bundaran HI. Ratusan orang yang mengatasnamakan "teman Ahok" siap menggalang dukungan masyarakat','img'=>'https://pbs.twimg.com/profile_images/574022055969992704/Y1ULIbCB.jpeg','url'=>'']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']
        ],
        'institusi'=>[
            ['name'=>'DPRD','img'=>'http://pks-jakarta.or.id/wp-content/uploads/2014/12/logo-dprd.jpg','url'=>''],
            ['name'=>'Demo Korupsi','img'=>'http://bimg.antaranews.com/bali/2010/08/ori/demokorupsi020810.jpg','url'=>'']
        ]
    ],
    'PERKIRAAN'=>[
        'kontra'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']
        ],
        'pro'=>[
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'nasdem5119b72a0ea62']            
        ],
        'belum'=>[
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd']           
        ],
    ],
    'ANALISA'=>'<img src="http://www.hariandepok.com/wp-content/uploads/2015/03/Serangan-Balik-Ahok-Kepada-DPRD-KI-Jakarta-637x353.jpg" class="pic">Pasca pelimpahkan kasus dana siluman ke ranah hukum, pihak DPRD DKI berbalik menyerang sebagai upaya antisipasi untuk mempertahankan diri, Gunernur Ahok dituduh menyalahi aturan terkait dengan pengajuan APBD 2015 tanpa sepengetahuan DPRD. Selain menabrak aturan, Ahok kerap melanggar etika sebagai pejabat publik. Dewan menganggap omongan Ahok tak pantas diucapkan sebagai pejabat publik. Usulan penggunaan Hak Angket merupakan opsi yang ditempuh  oleh DPRD dalam memperkarakan Gubernur Ahok. 
                </p><p style="text-align: justify;">Mayoritas anggota DPR menandatangi pengajuan hak angket kepada Gubernur Ahok perihal penyampaian RAPERDA APBD DKI 2015 ke Kementerian Dalam Negeri (Kemendagri) sesaat setelah panitia hak angket dibentuk dan disahkan oleh DPRD. Berdasarkan Peraturan Pemerintah Nomor 16 Tahun 2010 tentang Pedoman Penyusunan Tata Tertib DPRD dan mengacu pada Tata Tertib DPRD, hak angket bisa digulirkan atas usulan minimal dua fraksi atau 15 anggota Dewan. Dewan berhak memanggil pejabat daerah yang terkait dengan subyek penyelidikan. Dalam kasus APBD DKI, DPRD berhak memanggil Ahok serta tim anggaran pemerintah daerah.
                </p><p style="text-align: justify;"><img src="http://img.bisnis.com/posts/2015/02/14/402690/ahok-sofyan.jpg" class="pic2">Namun pengajuan hak angket dianggap terlalu berlebihan karena sangkaan pelanggaran yang dialamatkan pada Ahok bukanlah pelanggaran yang berpotensi besar merugikan pemerintahan. Selain itu, hal tersebut masih sangat premature karena mengesampingkan prosedur yang semestinya dijalankan yakni pengajuan hak interplasi (hak untuk bertanya)  terlebih dahulu sebelum prosedur hak untuk menyelidiki (hak angket) diajukan.
                </p><p style="text-align: justify;"><img src="http://cdn-media.viva.co.id/thumbs2/2015/03/05/299841_mediasi-ahok-dprd-berujung-ricuh_663_382.jpg" class="pic">Dalih Anggota Dewan merupakan bentuk ketakutan semata terhadap ancaman Gubernur Ahok dalam  membongkar mafia dibalik dana siluman tersebut. Hal ini mengindikasikan adanya upaya penggulingan atau pemakzulan terhadap Gubernur Ahok.  Penggulingan bertujuan untuk “mendiamkan” dan  menyerang langsung Gubernur Ahok tanpa menanyakan terlebih dahulu perihal sengketa yang sedang terjadi. 
                </p><p style="text-align: justify;">Meski berada dalam tekanan dan intervensi saling silang menyerang, Gubernur Ahok tetap pada pendiriannya untuk membawa sengketa tersebut ke ranah hukum.',
    'STRATEGI'=>[
        ['img'=>'http://statik.tempo.co/data/2015/03/05/id_376741/376741_620_tempoco.jpg','no'=>'DPRD DKI  tengah menyiapkan senjata untuk serangan baru yang ditujukan untuk menggoyang bahkan juga bisa melengserkan Gubernur DKI Jakarta, Basuki Tjahaja Purnama alias Ahok dari jabatannya.'],
        ['img'=>'http://pribuminews.com/wp-content/uploads/2015/02/DPRDAngket2Chris.jpg','no'=>'Dari sembilan fraksi di DPRD DKI Jakarta, Fraksi Partai Kebangkitan Bangsa dan NasDem telah menarik diri dari hak angket. Artinya peluang DPRD cukup besar untuk menempuh upaya hak angket karena masih ada tujuh Fraksi yang mendukung upaya tersebut.'],
        ['img'=>'https://img.okezone.com/content/2015/02/27/338/1111690/dprd-tak-ciut-dengan-laporan-ahok-ke-kpk-9oi40qToiN.jpg','no'=>'Selanjutnya, mayoritas anggota dewan bersepakat menentang keterangan Ahok terkait dana siluman 12 Triliun lebih  yang ada dalam APBD DKI. Setidaknya sudah ada 95 dari 106 jumlah total anggota Dewan yang menandatangi usulan hak angket tersebut.'],
        ['img'=>'http://statik.tempo.co/?width=450&id=374728','no'=>'Wacana hak angket yang digulirkan merupakan siasat politik dalam menjatuhkan Gubernur Ahok. Lewat hak angket, DPRD akan mencari kesalahan Gubernur Ahok. Setelah menyelidik dan menemukan pelanggaran berat terhadap  UU, maka Ahok akan dilengserkan.'],
        ['img'=>'https://img.okezone.com/content/2015/03/16/338/1119019/drama-ahok-vs-dprd-dki-harus-berakhir-WPkyohKryC.jpg','no'=>'Jika Ahok lengser, PDIP memiliki hak untuk mengajukan calon gubernur sebagai partai pemenang pemilu di DKI.']
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Patrice Rio CapellaPatrice','jabatan'=>'Sekretaris Jenderal (Sekjen) Partai NasDem','img'=>'http://assets.kompas.com/data/photo/2013/08/22/13245220000-a-aario-capella780x390.jpg','url'=>'','content'=>'"Kami mengintruksikan seluruh anggota Fraksi NasDem untuk mencabut hak angket, dan meminta aparat penegak hukum melakukan tindakan"'],
        ['from'=>'Bowo','jabatan'=>'Koordinator Aksi @temanAhok','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Konsepnya yaitu mendesak penegakan hukum untuk pembegal APBD"'],
        ['from'=>'Aliansi Masyarakat Resah Dewan Perwakilan Rakyat (AMAR DPR)','jabatan'=>'','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Selamatkan APBD 12 triliun itu kan pajak dari rakyat, justru dana itu yang bisa membangun kota dan masyarakat Jakarta, ulah parpol terkait angket itu telah menghambat proses pembangunan kota Jakarta yang jelas merugikan masyarakat sebagai pembayar pajak"'],
        ['from'=>'Sebastian Salang','jabatan'=>'Koordinator PORMAPPI','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'"Upaya yang dilakukan oleh Ahok adalah upaya untuk menjaga agar uang negara tidak ada yang dikorup, dan di sisi yang lain menyelamatkan agar Anggota DPRD tidak ada yang masuk penjara"']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Selamat Nurdin','jabatan'=>'Ketua Fraksi Partai Keadilan Sejahtera','img'=>'http://4.bp.blogspot.com/-6o6VaLYla1Q/VPeZ2ev3r_I/AAAAAAAA9Vc/AJfh0vXyONo/s1600/Selamat%2BNurdin_PKS.jpg','url'=>'','content'=>'"Kami pengin Ahok mengakui telah melakukan hal keliru . Kami ngomong soal administrasi negara, dan dia melanggarnya. Apa yang dikirim tidak ada satu pun dari Dewan"'],
        ['from'=>'Abraham Lunggana','jabatan'=>'Wakil Ketua DPRD DKI','img'=>'http://wartatujuh.com/wp-content/uploads/2015/03/lunggana-620x330.jpg','url'=>'','content'=>'"Ahok yang berusaha men"deadlock"an kisruh APBD 2015 ini"']
    ],
    'VIDEO'=>[
        ['id'=>'QFy2IEcpcGI'],
        ['id'=>'Welz79lQoZE'],
        ['id'=>'RIbRoaY0jsM'],
        ['id'=>'zd4ngJdnUPU'],
        ['id'=>'6CKMPUltJtQ'],
        ['id'=>'Qm0fl1WMr-U']
    ],
    'FOTO'=>[
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img1.jpg'],
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img2.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/03/22/414462/dukungan-terhadap-ahok.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/02/27/id_375043/375043_620_tempoco.jpg'],
        ['img'=>'http://images.detik.com/customthumb/2015/03/23/10/171112_dprdkomahook4.jpg?w=460'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/03/01/31/970362/ahok-vs-dprd-dan-politik-adu-kuat-WkD.jpg'],
        ['img'=>'http://cdn-media.viva.co.id/thumbs2/2015/03/05/299841_mediasi-ahok-dprd-berujung-ricuh_663_382.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/03/03/id_376061/376061_620_tempoco.jpg'],
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-left: 10px;
        float: right;
        border: 1px solid black;
        margin-top: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .clear {
        clear: both;
    }
</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/ahokvsdprd/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ANALISA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['ANALISA'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PROFILE</span></h5>
        <div id="accordion" class="panel-group" style="height: 340px;">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://indonesiana.tempo.co/uploads/foto/2015/03/20/pitung.jpg" class="col-xs-6 col-sm-6" style="margin-left: -15px;">
                <div class="col-xs-6" style="padding-left: 0px;">
                    <span style="font-size: 1.2em;font-weight: bold;"><?php echo $data['PROFIL']['nama'];?></span><br>
                    Agama : <?php echo $data['PROFIL']['agama'];?><br>
                    Tempat Lahir : <?php echo $data['PROFIL']['tmp'];?><br>
                    Tgl. Lahir : <?php echo $data['PROFIL']['tgl'];?>
                </div>
                <div class="col-xs-12" style="margin-left: -15px;">
                    <span style="font-size: 1.2em;font-weight: bold;">Jenjang Karir :</span><br>
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['PROFIL']['jejak'] as $key => $val) {
                            echo "<li>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">STRATEGI MENGGULINGKAN AHOK</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;margin-bottom: 20px;">
                <ul>
                    <?php
                    foreach ($data['STRATEGI'] as $key => $val) { ?>
                        <li class="font_kecil" style='margin-left: 5px;text-align: justify;'><img src="<?php echo $val['img'];?>" class='pic3'><?php echo $val['no'];?></li><br>
                        <div class="clear"></div>
                    <?php
                    }
                    ?>
                <div class="clear"></div>
                </ul>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black" style="margin-top: 30px;"><span id="perang_sebelumnya">PRO KONTRA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="font-size: 12px;"><?php echo $data['PROKONTRA']['narasi'];?></p>
                <h5>Dukungan Terhadap Gubernur Ahok</h5>
                <p style="font-size: 12px;"><?php echo $data['PROKONTRA']['dukungan'];?></p>
            </div>
        </div>


        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;margin-top:10px;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <p>PARPOL PENDUKUNG</p>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                    $photo = 'http://www.dpp.pkb.or.id/unlocked/unlock/lambangresmipkb.jpg';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div><br>

        <div class="black boxcustom2 col-xs-12" style="padding-left: 20px;">
            <h5 style="padding-top: 10px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <div class="col-xs-4">
                        <img style="max-width: 100%;max-height: 70px;" src="<?php echo $val['img'];?>" class="lazy" alt="<?php echo $val['name'];?>">
                        <h4 style="font-size: .8em;"><?php echo "<span class='text-center black'>".$val['name']."</span>";?></h4>
                    </div>
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;pading">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="max-width: 100px;" src="<?php echo $val['img'];?>" class="lazy" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PERKIRAAN KEKUATAN DUKUNGAN DI PARLEMEN</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="font-weight: bold;font-size: 12px;margin-bottom: 10px;" class="text-center">KONTRA AHOK (Jumlah Suara 64)</p>
                <?php
                foreach($data['PERKIRAAN']['kontra'] as $key=>$val) {
                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $pageName = strtoupper($personarr['page_name']);
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                    ?>
                    <div class='col-xs-3'>
                        <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                            <img src="<?php echo $photo;?>" class="img-responsive" alt="<?php echo $pageName;?>" style="max-height: 77px;"/> 
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
            <div class="clear"></div>

            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="font-weight: bold;font-size: 12px;margin-bottom: 10px;" class="text-center">PRO AHOK (Jumlah Suara 23)</p>
                <?php
                foreach($data['PERKIRAAN']['pro'] as $key=>$val) {
                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $pageName = strtoupper($personarr['page_name']);
                    if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                        $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                    }
                    else {
                        $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                    }
                    ?>
                    <div class='col-xs-3'>
                        <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                            <img src="<?php echo $photo;?>" class="img-responsive" alt="<?php echo $pageName;?>" style="max-height: 77px;"/> 
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
            <div class="clear"></div>

            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="font-weight: bold;font-size: 12px;margin-bottom: 10px;" class="text-center">BELUM JELAS (Jumlah Suara 19)</p>
                <?php
                foreach($data['PERKIRAAN']['belum'] as $key=>$val) {
                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $pageName = strtoupper($personarr['page_name']);
                    if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                        $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                    }
                    else {
                        $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                    }
                    ?>
                    <div class='col-xs-3'>
                        <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                            <img src="<?php echo $photo;?>" class="img-responsive" alt="<?php echo $pageName;?>" style="max-height: 77px;"/> 
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=0; $i < 2; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENDUKUNG']['$i']['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['content'];?></p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>        
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=2; $i < 4; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENDUKUNG']['$i']['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['content'];?></p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][0]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][0]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 12px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG'][0]['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][0]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][0]['content'];?></p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
            </div>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][1]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][1]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 12px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG'][1]['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][1]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][1]['content'];?></p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALLERY FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach ($data['FOTO'] as $key => $val) { ?>
                    <div class="col-xs-6 col-sm-3">
                        <a href="<?php echo $val['img'];?>" target="_blank">
                            <img src="<?php echo $val['img'];?>" class="img-responsive" style="height: 147px;width: 100%;margin-bottom: 10px;"/>
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>



