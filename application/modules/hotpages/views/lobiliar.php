<?php
$data = [
    'block1'=>[
        'title'=>'PERANG ANTAR GENK : SETYA NOVANTO VS SUDIRMAN SAID',
        'narasi'=>'
            <p>Tudingan Menteri ESDM, Sudirman Said perihal adanya pejabat tinggi yang melakukan lobi liar untuk meminta jatah saham ke PT Freeport  memicu kegaduhan politik di dalam negeri.  Nama ketua DPR RI, Setya Novanto diduga menjual nama presiden Jokowi dan Wakil presiden yusuf Kalla untuk memuluskan lobi liar tersebut terkait perpanjangan kontrak kerja perusahaan tambang asal Amerika itu di Indonesia.</p>
            <img src="https://img.okezone.com/content/2015/11/18/337/1251754/pencatutan-nama-presiden-jokowi-diduga-setting-an-freeport-Lrv4SbzPlU.jpg" class="pic">
            <p>Tindakan komandan DPR tersebut dianggap menyalahi kode etik sebagai pejabat negara dan termasuk tindakan kriminal yang melanggar hukum. Kini nasib Setya Novanto berada diujung tanduk seiring massifnya desakan publik untuk memecat politisi Golkar itu dari posisinya sebagai Ketua DPR.</p>
        '
    ],

    'block7'=>[
        'title'=>'Aturan Main MKD',
        'narasi'=>'
            <img src="http://images1.rri.co.id/thumbs/berita_213954_800x600_IMG01828-20151029-1540.jpg" class="pic">

            <p >MKD bisa merekomendasikan pemberhentian tetap seorang anggota dewan yang melalui proses pemeriksaan, terbukti melanggar kode etik seperti yang tertuang dalam Peraturan DPR RI No. 2 tahun 2015 tentang Tata Beracara MKD DPR RI. Namun untuk sampai ke situ, MKD harus terlebih dahulu membentuk sebuah panel independen yang terdiri dari tujuh anggota, yang terdiri dari tiga unsur MKD dan empat unsur masyarakat.Panel inilah yang berhak memproses kasus pelanggaran kode etik berat yang berpotensi berujung pada pemberhentian anggota.</p>
            <p >Jika panel sepakat bahwa sanksi pemberhentian tetap memang harus dijatuhkan, maka hasil putusan panel ini harus dibawa ke sidang paripurna DPR RI untuk disepakati di sana.<p>
            <p >Anggota panel ini dipilih oleh para anggota MKD melalui musyawarah mufakat. Jika proses mencapai mufakat mengalami kebuntuan, pemungutan suara menjadi jalan keluar. Nasib Setya akan sangat bergantung pada dinamika yang terjadi di tubuh MKD sendiri.</p>
            <p >Namun sejauh ini MKD belum mau memutuskan untuk menyidangkan Ketua DPR Setya Novanto terkait kasus yang terngah menderanya. MKD berpendapat status Menteri ESDM Sudirman Said sebagai pelapor bermasalah. Padahal, di aturan Tata Beracara MKD diatur kasus yang sudah jadi perhatian publik bisa disidangkan. Aturan itu termaktub dalam pasal 4 dan 5 Tata Beracara MKD DPR </p> 
        '
    ],

    'block3'=>[
        'title'=>'Memburu Rente di Freeport',
        'narasi'=>'
            <p>Eksistensi perusahaan tambang asal Amerika tersebut,  sejauh ini banyak mendapat penolakan dari warga lokal, aktivis lingkungan hidup dan penggiat HAM. Hal ini tidak lepas dari efek destruktif yang ditimbulkan. Kerusakan lingkungan yang massif dan kesenjangan sosial yang begitu mengangah akibat kebijkan yang tidak adil terhadap penduduk lokal  kerap menjadi faktor yang memicu konflik berdarah di sekitaran lokasi tambang.  Selain itu, keberadaannya juga kerap dianggap merugikan Indonesia karena pembagian royalti yang begitu kecil.  Pemerintah Indonesia hanya mendapat 1 persen tiap tahunnya.</p>  
            <img src="http://kertaskecil.net/wp-content/uploads/2015/11/Setya-freeport.jpg" class="pic">
            <p>Namun faktanya , PT Freeport  terus eksis selama puluhan dalam mengeruk kekayaan alam di bumi Cendrawasih. Ada dugaan bahwa, selama ini kebedaraannya dibekingi oleh tangan-tangan kuat yang punya pengaruh kuat di pemerintah. Sehingga eksistensinya bisa terus berlangsung.  Para pejabat tinggi di tingkat pusat menggunakan posisinya untuk mengkondisikan regulasi  yang mengatur keberadaan PT tersebut di Papua.  Renegoisasi perpanjangan kontrak karya menjadi celah yang sering diekploitasi  untuk memburu rente bagi kepentingan pribadi. </p>
            <p>Perpanjangan kontrak Freeport  sedianya baru akan digulirkan dua tahun jelang masa kontrak lama berakhir. Kontrak PT Freeport akan berakhir  tahun 2019 nanti, namun Setya Novanto bersama seorang pengusaha mengambil inisiatif  dengan melakukan lobi liar dengan bos Perusahaaan tersebut sebanyak tiga kali. Dalam pertemuan itu, Setnov menawarkan kerjasama berupa  janji tentang kelancaran proses perpanjangan  kontrak karya PT Freeport Indonesia dan meminta imbalan jatah saham sebesar 11 persen  untuk dirinya  dan  9 persen untuk presiden dan wapres.</p>
            <img src="http://cdn.sindonews.net/dyn/620/content/2015/11/16/34/1061962/kadin-pencatutan-jokowi-di-freeport-tak-ganggu-investasi-YTP.jpg" class="pic2">
            <p>Pencatutan nama Presiden dan Wapres  dalam pertemuan itu disinyalir demi memuluskan lobi  renegosiasi perpanjang kontrak  perusahaan tersebut.  Nama Menteri  Koordinator politik, Hukum dan Keamanan luhut Binsar Panjaitan juga ikut dicantumkan. Tidak hanya itu,  Setnov juga meminta divestasi saham sebesar 49 persen ke PT Freeport dalam pembangunan proyek listrik di Timika, Papua. </p>


        '
    ],

    'block4'=>[
        'title'=>'JEJAK - JEJAK KASUS ',
        'narasi'=>'
            <img src="http://katadata.co.id/public/media/images/thumb/2015/11/18/2015_11_18-10_34_12_25ba396615290c52f7b77c607ebd1581_620x413_thumb.jpg" class="pic">
            <!--<img src="http://beritadunia.net/images/14478947850wlmjcgcmmku-brm3lj4ric57itxeo5l_0kithbvabm.jpg" class="pic2">-->
            
            <p>Kasus pencatutan nama Presiden  Joko Widodo dan Wapres Jusuf Kalla dalam melobi PT Freeport Indonesia hanya salah satu aksi kontroverial yang pernah dilakukan  oleh Setya Novanto.  Selain kasus tersebut yang mencuatkan namanya di berbagai media tanah air dalam sebulan terakhir ini. Politikus Golkar itu juga disebut melakukan  lobi-lobi liar ke  Perdana Menteri Jepang Shinzo Abe di pembelian pesawat amfibi untuk Indonesia dan menyurati Direktur Utama PT Pertamina untuk bayar biaya sewa ke PT Orbit Terminal Merak. Namun Setya Novanto selalu mampu meloloskan diri dari setiap kasus yang menderanya. </p>
            <p>Berikut Kasus-Kasus yang pernah menimpanya. </p>
            <p>
                <div style="float: left;width: 50px;font-weight: bold;">
                1999
                </div>
                <div style="float: left;font-weight: bold;">
                Pengalihan hak tagih Bank Bali
                </div>
                <div class="clear"></div>
                <div style="float: left;margin-left: 50px;">
                PT Era Giat Prima, perusahaan milik Setya bersama Djoko Tjandra dan Cahyadi Kumala, mendapat mandat menagih utang Bank Bali kepada Bank Dagang Nasional Indonesia. Imbalannya Rp500 miliar. Pembayaran ini merugikan negara, tapi pengusutannya dihentikan pada 2003.
                </div>
                <div class="clear"></div>
            

                <div style="float: left;width: 50px;font-weight: bold;">
                2003
                </div>
                <div style="float: left;font-weight: bold;">
                Penyelundupan beras
                </div>
                <div class="clear"></div>
                <div style="float: left;margin-left: 50px;">
                Setya diperiksa Kejaksaan Agung pada 27 Juli 2006 karena memindahkan 60 ribu ton beras ke gudang non-pabean yang belum membayar bea masuk sebesar Rp 122,5 miliar.
                </div>
                <div class="clear"></div>

                <div style="float: left;width: 50px;font-weight: bold;">
                2006
                </div>
                <div style="float: left;font-weight: bold;">
                Penyelundupan limbah
                </div>
                <div class="clear"></div>
                <div style="float: left;margin-left: 50px;">
                Perusahaan Setya, PT Asia Pasific Eco Lestari, mengimpor limbah beracun lewat Pulau Galang di Batam dari Singapura.
                </div>
                <div class="clear"></div>

                <div style="float: left;width: 50px;font-weight: bold;">
                2012
                </div>
                <div style="float: left;font-weight: bold;">
                Proyek PON 2012
                </div>
                <div class="clear"></div>
                <div style="float: left;margin-left: 50px;">
                Ia bersaksi untuk tersangka Gubernur Riau Rusli Zainal karena membahas anggaran yang diduga dikorupsi.
                </div>
                <div class="clear"></div>

                <div style="float: left;width: 50px;font-weight: bold;">
                2013
                </div>
                <div style="float: left;font-weight: bold;">
                E-KTP
                </div>
                <div class="clear"></div>
                <div style="float: left;margin-left: 50px;">
                Mantan Bendahara Umum Partai Demokrat Muhammad Nazaruddin menuding Setya membagikan fee proyek Kementerian Dalam Negeri ke sejumlah legislator. 
                </div>
                <div class="clear"></div>

                <div style="float: left;width: 50px;font-weight: bold;">
                2015
                </div>
                <div style="float: left;font-weight: bold;">
                Penggeledahan PT Victoria Securities Indonesia
                </div>
                <div class="clear"></div>
                <div style="float: left;margin-left: 50px;">
                Ia diduga mengintervensi Jaksa Agung Prasetyo yang menggeledah kantor Victoria Securities International Corporation karena dugaan pelanggaran pembelian aset PT Adyaesta Ciptatama kepada Bank BTN.
                </div>
                <div class="clear"></div>

                <div style="float: left;width: 50px;font-weight: bold;">
                2015
                </div>
                <div style="float: left;font-weight: bold;">
                Menghadiri Kampanye Donald Trump
                </div>
                <div class="clear"></div>
                <div style="float: left;margin-left: 50px;">
                Kunjungannya ke Amerika Serikat beberapa waktu lalu memicu polemik di tanah air. Pertemuannya  dengan kandidat Presiden Donald Trump menjadi sorotan tajam oleh awak media. Dia dinilai menyalahi kode etik seorang pejabat negara karena tindakannya tersebut. Ketua DPR itu hanya mendapat terguran tertulis dari MKD
                </div>
                <div class="clear"></div>
                
            </p>

       '
    ],

    'block5'=>[
        'title'=>'Transkrip Rekaman',
        'narasi'=>'
            <img src="http://www.konfrontasi.com/sites/default/files/styles/article_big/public/article/2015/11/SETYA%20REZA.jpg?itok=sSKO8qlL" class="pic">
            <p>Dalam transkrip rekaman yang beredar di kalangan wartawan, pihak yang diduga Setnov mengatakan kepada seseorang yang disebut sebagai Presiden Direktur PT Freeport Indonesia Maroef Sjamsuddin jika presiden melalui Luhut Panjaitan yang saat itu menjabat kepala staf presiden, disebut menginginkan 11 persen saham Freeport, sedangkan wapres akan diberi jatah 9 persen saham.</p>
            <p>Berikut Isi Transkrip</p>
            <p>Sn: Waktu pak Luhut di Solo...Pal Luhut lagi disibukkan habis Jumat itu. Kalau bisa tuntas, minggu depan sudah bisa diharapkan. Itu yang sekarang sudah bekerja.</p>
            <p>Ms: Coba ditinjau lagi fisibilitiesnya pak. Kalau ngga salah Freeport itu off taker.</p>
            <p>R: Saran saya jangan off taker dulu, kalau off taker itu akan.....</p>
            <p>Ms: Keterkaitan off taker itu darimana pak?</p>
            <p>R:..... (suara tidak jelas)</p>
            <p>Ms: Bapak juga nanti baru bisa bangun setelah kita kasih purchasing garanty lho pak. Purchasing garanty-nya dari kita lho pak.</p>
            <p>R: PLTA-nya</p>
            <p>Ms: Artinya patungan? Artinya investasi patungan 49-51 persen. Investasi patungan off taker kita juga? double dong pak? modalnya dari kita, off takernya dari kita juga.</p>
            <p>R: Kalau off taker itu.....<br/>Oke deh Kalau Freeport ngga usah ikut
            <p>Ms: Ini yang Pak R pernah sampaikan ke Dharmawangsa itu?</p>
            <p>R:....(tidak jelas)</p>
            <p>Ms: Oh kalau komitmen, Freeport selalu komitmen. Untuk smelter desember kita akan taruh 700 ribu dollar. Tanpa kepastian lho pak. Karena kalau kita ngga tahu, kita ngga komit. Sorry 700 juta dollar.</p>
            <p>Sn: Presiden Jokowi itu dia sudah setuju di sana di Gresik tapi pada pada ujung-ujungnya di Papua. Waktu saya ngadep itu, saya langsung tahu ceritanya ini waktu rapat itu terjadi sama Darmo...Presiden itu ada yang mohon maaf ya, ada yang dipikirkan ke depan, ada tiga....(kurang jelas)
            Tapi kalau itu pengalaman-pengalaman kita, pengalaman-pengalaman presiden itu, rata-rata 99 persen gol semua.
            Ada keputusan-keputusan lain yang digarap, bermain kita
            Makanya itu, Reza tahu Darmo, dimainkan habis-habisan, selain belok
            <p>Ms: delobies...
            Repot kalau meleset komitmen...30 persen. 9,36 yang pegang BUMN</p>
            <p>Sn: Kalau ngga salah, Pak Luhut itu bicara dengan Jimbok. Pak Luhut itu sudah ada yang mau diomong.</p>
            <p>R: Gua udah ngomong dengan Pak Luhut, ambilah 11, kasihlah Pak JK 9, harus adil kalau ngga ribut.</p>
            <p>Sn: Jadi kalau pembicaraan Pak Luhut dan Jim di Santiago, 4 tahun yang lampau itu, dari 30 persen itu 10 persen dibayar pakai deviden. Ini menjadi perdebatan sehingga mengganggu konstalasi. Ini begitu masalah cawe-cawe itu presiden ngga suka, Pak Luhut dikerjain kan begitu kan...Nah sekarang kita tahu kondisinya...Saya yakin juga karena presiden kasih kode begitu berkali-kali segala urusan yang kita titipkan ke presiden selalu kita bertiga, saya, pak Luhut, dan Presiden setuju sudah.
            <p>Saya ketemu presiden cocok. Artinya dilindungi keberhasilan semua ya. Tapi belum tentu kita dikuasai menteri-menteri Pak yang begini-begini.</p>
            <p>R: Freeport jalan, bapak itu happy, kita ikut happy. Kumpul-kumpul/kita golf, kita beli private jet yang bagus dan representatif</p>
            <p>Ms: Tapi saya yakin Pak Freeport pasti jalan.</p>
            <p>Sn: Jadi kita harus banyak akal. Kita harus jeli, kuncinya ada pada Pak Luhut dan saya.</p>
            <p>Ms: Terima kasih waktunya pak</p>
            <p>R: Jadi follow up gimana? Nanti saya bicara Pak Luhut jadi kapan. Terus Oke lalu kita ketemu. Iya kan?</p>
            <p>Sn: Kalau mau cari Pak Luhut harus cepet, kasih tanggung jawab enggak. Gimana sukses, kita cari akal.</p>

        '
    ],

    'block6'=>[
        'title'=>'Reaksi Istana',
        'narasi'=>'
        <img src="http://www.radarsolo.co.id/images/Pict/2015/november/18/jokowisetya.jpg" class="pic">
        <p>Pencatutan nama presiden dan Wapres dalam perkara Freeport yang menyerat nama Ketua DPR Setya Novanto membuat istana bereaksi keras.  Presiden Joko Widodo mengecam tindakan tersebut sebagai tindakan yang tidak terpuji. Selain melanggar tugas dan tanggung jawab ketua DPR yang mengintervensi tugas eksekutif, tindakaan tersebut beraroma konflik kepentingan yang menjurus aksi kriminal.</p>
        <p>Presiden menyerahkan kasus sepenuhnya ke pihak MKD untuk menelusuri pelanggaran kode etik tersebut. Presiden secara tegas meminta pihak-pihak yang terlibat di dalamnya agar diproses sesuai dengan ketentuan yang berlaku.  </p>
        <img src="http://img1.beritasatu.com/data/media/images/medium/1416237081.jpg" class="pic2">
        <p>Senada dengan presiden, Wapres Yusuf Kalla juga mendukung penuh pengusutan kasus tersebut yang sudah merendahkan martabat Pemerintah. Bahkan, Yusuf Kalla berencana membawa kasus tersebut ke ranah hukum</p>
        '
    ],

    'right1'=>[
        'title'=>'Jenjang Karir Politik',
        'narasi'=>'
            <img src="http://rmol.co/images/berita/normal/421755_12080520112015_setya_novanto" class="picprofil" style="width: 100%;">
            
            
                <table style="margin-left: 15px;margin-right: 15px;">
                    <tr>
                        <td style="width:90px;">Nama</td>
                        <td style="width:15px;">:</td>
                        <td>Setya Novanto</td>
                    </tr>
                    <tr>
                        <td>Agama</td>
                        <td>:</td>
                        <td>Islam</td>
                    </tr>
                    <tr>
                        <td>Tempat Lahir</td>
                        <td>:</td>
                        <td>Bandung</td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir</td>
                        <td>:</td>
                        <td>Jumat, 12 November 1954</td>
                    </tr>
                    <tr>
                        <td>Warga Negara</td>
                        <td>:</td>
                        <td>Indonesia</td>
                    </tr>
                    <tr>
                        <td>Partai </td>
                        <td>:</td>
                        <td>GOLKAR</td>
                    </tr>                    
                    
                    <tr>
                        <td colspan="3" align="left">
                            <ol>
                                <li style="font-size:12px !important;">Anggota DPR-RI dari (1999 - 2004, 2004 - 2009, 2009 - 2014, 2014 - 2019)</li>
                                <li style="font-size:12px !important;">Badan Anggaran DPR-RI </li>
                                <li style="font-size:12px !important;">Ketua Fraksi Partai Golkar (2009 - 2014) </li>
                                <li style="font-size:12px !important;">Ketua DPR-RI (2014 - 2019)</li>
                            </ol>
                        </td>
                    </tr>

                </table>
        '
    ],

    'block9'=>[
        'title'=>'SETNOV MENGELAK',
        'narasi'=>'
            <img src="http://cdn.tmpo.co/data/2015/09/30/id_441632/441632_620.jpg" class="picprofil" style="width: 100%;">
            <p class="rightcol font_kecil" style="margin-right: 10px;">Kasus pencatutan nama Presiden dan Wakil Presiden dalam dugaan permintaan saham PT Freeport makin terang benderang. Namun, Ketua DPR Setya Novanto yang disebutkan dalam aduan Menteri ESDM Sudirman Said tetap membantah tudingan-tudingan kepada dirinya</p>
            <p class="rightcol font_kecil" style="margin-right: 10px;">Setya Novanto juga mengaku pimpinan DPR bersih dari kasus yang dilaporkan Sudirman Said.  Ia membantah isi transkrip itu dengan menyatakan tidak pernah ada perbincanga terkat permintaan saham, apalagi membawa nama-nama Presiden dan Wapres. Meski mengakui ada pertemuan dengan pimpinan PT Freeport Maroef Sjamsoeddin dan pertemuannya itu bukan terkait urusan pribadi. Apa yang disampaikan menurut dia berkaitan dengan kepentingan rakyat. Wakil Ketua Umum Partai Golkar itu juga  mengelak kronologi tanggal, tempat, dan waktu pertemuan seperti yang disampaikan Sudirman, termasuk keberadaan pengusaha berinisial RC dalam pertemuan itu. </p>
            <img src="http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2015--11--88305-setya-novanto-catut-jokowi-freeport-jk-sore-ini-temui-bahas-pencatutan-nama.jpg" class="picprofil" style="width: 100%;">


        '
    ],    

    'institusiPendukung' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdpr51da66b0a7ac4','image'=>'http://www.gorontaloprov.go.id/images/news/DPR-RI.jpg','title'=>'DPR-RI'],
    ],
   'partaiPendukung' => [
       ['link'=>'http://www.bijaks.net/aktor/profile/partaigolongankarya5119aaf1dadef ','image'=>'http://fajar.co.id/wp-content/uploads/2015/08/golkar324.jpg','title'=>'GOLKAR'],
   ],
    

    'institusiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/badanpenelitiandanpengembanganesdm535f26d49ad74','image'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTsVaal8tLvbYBOvqJB0L79B6tGSZ4yhJssLZIf3qxL4-UHJKtC','title'=>'Kementerian Energi dan Sumber Daya Mineral'],
        ['link'=>'#','image'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Coat_of_arms_of_the_People%27s_Representative_Council_of_Indonesia.svg/250px-Coat_of_arms_of_the_People%27s_Representative_Council_of_Indonesia.svg.png','title'=>'Mahkamah Kehormatan Dewan DPR RI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','image'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Indonesian_Presidential_Emblem_gold.svg/2000px-Indonesian_Presidential_Emblem_gold.svg.png','title'=>'Presiden Republik Indonesia'],
    ],

    'partaiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/partaidemokrasiindonesiaperjuangan5119ac6bba0dd','image'=>'http://www.faktapost.com/foto_berita/61PDIP.png','title'=>'PDI-P'],
        ['link'=>'#','image'=>'http://www.mpr.go.id/uploads/fraksi/hanura.jpg','title'=>'HANURA'],
        ['link'=>'http://www.bijaks.net/aktor/profile/nasdem5119b72a0ea62','image'=>'http://2.bp.blogspot.com/-6WMmJUYkwHs/URTRolnNByI/AAAAAAAAS8E/1RaV9_ROfkA/s1600/nasdem_biru.png','title'=>'NASDEM'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaikebangkitanbangsa5119b257621a4','image'=>'http://1.bp.blogspot.com/-d9_P-ijkhgY/UTFOvvHOxsI/AAAAAAAABSM/ECen89I0mdc/s1600/PKB+Logo+Vektor+Partai+Kebangkitan+Bangsa.jpg','title'=>'PKB'],
    ],

    // 'negaraPenentang' => [
    //     ['link'=>'','image'=> 'https://spektrumku.files.wordpress.com/2008/06/us-flag.jpg','title'=>'AS'],
    // ],

    'quotePendukung'=>[
        ['from'=>'Setya Novanto','jabatan'=>'Ketua DPR','img'=>'https://aws-dist.brta.in/2015-09/original_700/0_0_1000_666_cba9a4692a0a77a27bf5d64d329d4aabe7cd9f82.jpg','url'=>'http://www.bijaks.net/aktor/profile/drssetyanovanto50f8fd3c666bc','content'=>'"Ya, yang jelas saya selaku pimpinan DPR tidak pernah kita membawa nama presiden dan mencatut nama presiden"'],
        ['from'=>'Setya Novanto','jabatan'=>'Ketua DPR','img'=>'https://aws-dist.brta.in/2015-09/original_700/0_0_1000_666_cba9a4692a0a77a27bf5d64d329d4aabe7cd9f82.jpg','url'=>'http://www.bijaks.net/aktor/profile/drssetyanovanto50f8fd3c666bc','content'=>'"Saya membenarkan ada transkrip yang beredar yang tentu tidak utuh"'],
        ['from'=>'Aryo Djojohadikusumo','jabatan'=>'anggota Komisi VII DPR Fraksi Gerindra','img'=>'http://rmol.co/images/berita/normal/247273_10364003092015_aryo_djojohadikusumo','url'=>'http://www.bijaks.net/aktor/profile/aryopsdjojohadikusumo52857f318de33','content'=>'"Menurut saya Pak menteri perlu menyampaikan bukti yang lebih kuat dan kredibel, karena kalau buktinya seperti ini beliau bisa digugat melakukan pencemaran nama baik,"'],
        ['from'=>'Miryam S Haryani','jabatan'=>'Ketua DPP Hanura','img'=>'http://www.bijaks.net/public/upload/image/politisi/miryamsharyanisemsi50f8daa2dd6c6/badge/24b4b2d36c012c7079cc5fe5e85835caef737c94.jpg','url'=>'http://www.bijaks.net/aktor/profile/miryamsharyanisemsi50f8daa2dd6c6','content'=>'"Ini kan sebuah tindakan yqng sangat tidak negarawan karena sebetulnya dia sudah tahu apa yang harus dia lakukan tanpa harus membuat geger Republik ini,"'],
        ['from'=>'Junimar Girsang','jabatan'=>'Wakil Ketua Mahkamah Kehormatan Dewan (MKD) DPR','img'=>'http://pojoksatu.id/wp-content/uploads/2015/01/xJunimart_Girsang.jpg.pagespeed.ic.MjLX1CBEWp.jpg','url'=>'http://www.bijaks.net/aktor/profile/junimartgirsang5265d43570ec6','content'=>'"Saya banyak dapat telepon, SMS. Menyatakan jangan banyak bicara lah, begitu"'],
        ['from'=>'Sufmi Dasco Ahmad','jabatan'=>'Wakil Ketua Mahkamah Kehormatan Dewan (MKD) DPR','img'=>'http://www.rmol.co/images/berita/thumb/thumb_259185_08441420072015_Sufmi-Dasco-Ahmad.jpg','url'=>'http://www.bijaks.net/aktor/profile/sufmidascoahmad52f1debb0ddec','content'=>'"Tidak perlu dan tidak usah. Tidak ada alasan yang mendasar untuk melakukan itu (pelaporan ke Bareskrim). Dimana tindak pidananya dan tindak kejahatannya?"'],
        ['from'=>'Nurdin Tampubolon','jabatan'=>'Ketua Fraksi Hanura','img'=>'http://www.goldbank.co.id/pic/profilnt_content_226.jpg','url'=>'http://www.bijaks.net/aktor/profile/irnurdintampubolon50f63c8d13552','content'=>'"Kecuali ada keputusan MKD bahwa yang bersangkutan melakukan pelanggaran, barulah bisa mundur. Tapi kalau sekarang, tak ada dasar hukumnya. Ada mekanisme yang harus diikuti"'],
        ['from'=>'Fadli Zon','jabatan'=>'Wakil Ketua DPR-RI','img'=>'http://www.jkt.life/wp-content/uploads/2015/09/fadli-zon.jpeg','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007','content'=>'"Ya mungkin saja nanti dia anggap berhasil bongkar ini jadi tidak direshuffle. Menurut survei terburuk. Kalau menurut saya pribadi sangat jelek sekali"'],
        ['from'=>'Effendi Simbolon','jabatan'=>'Politikus PDI Perjuangan','img'=>'http://rmol.co/images/berita/normal/69899_03212601042015_effendi_simbolon_1.jpg','url'=>'http://www.bijaks.net/aktor/profile/effendisimbolon511b3badc4d92','content'=>'"Sudirman Said sedang berakting dengan gayanya yang sok bersih. Niat pembuburan Petral tidak sepenuhnya tanpa kepentingan, ini lebih kepada mengambil kembali, merebut kembali kekuasaan dan kepentingan yang dulu mereka genggam"'],
        ['from'=>'Jimmly Asshidiqie','jabatan'=>'Mantan Ketua Mahkamah Konstitusi','img'=>'http://www.bijaks.net/public/upload/image/politisi/jimlyasshiddiqie51cb99e738a36/badge/0670931aed7f5658b894614263594debfc5692e8.jpeg','url'=>'http://www.bijaks.net/aktor/profile/jimlyasshiddiqie51cb99e738a36','content'=>'"Itu kan udah ada prosesnya. Itu seharusnya tidak dibuka, karena bisa diselesaikan secara internal dahulu. Sesama pejabat seharusnya jangan saling melempar batu"'],
        ['from'=>'Rizal Ramli','jabatan'=>'Menteri Koordinator Bidang Maritim Dan Sumber Daya','img'=>'http://static.inilah.com/data/berita/foto/2231775.jpg','url'=>'http://www.bijaks.net/aktor/profile/rizalramli503c4831b53ff','content'=>'"Anggap saja sedang melihat sinetron antar geng"'],
        ['from'=>'Fachri Hamzah','jabatan'=>'Wakil Ketua DPR-RI','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvRZIE6JGrFoksQK6ameUBGcSg3OsUyt5CL8hpQ4AnmIDo5TDy','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09','content'=>'"Ini yang merekam siapa sebetulnya? Yang betul-betul ikhtiar merekam, bawa rekaman gitu, kemudian rekaman itu disampaikan kepada menteri"'],
    ],

    'quotePenentang'=>[
        ['from'=>'Adian Napitupulu','jabatan'=>'Anggota Komisi VII DPR Fraksi PDI-P','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_7pl-qZvDO2f5G3dQek6JhiJb-D28-e0cUMYbANfQuGBRqHspUw','url'=>'http://www.bijaks.net/aktor/profile/adianyunusyusaknapitupulu526db97f68e58','content'=>'"Setya Novanto harus dinonaktifkan dulu dari jabatan sebagai anggota DPR untuk menjamin tidak ada lagi intervensi seperti itu,"'],
        ['from'=>'Ronald Rofiandri','jabatan'=>'Direktur Advokasi Pusat Studi Hukum dan Kebijakan Indonesia (PSHK)','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwC6Vqpmv0mQpwbJ42ftiA8utNlPTutYguTprnoawthT5fsyRxOw','url'=>'#','content'=>'"Kami mendesak terlapor, Setya Novanto untuk secara sementara mengundurkan diri dari jabatan Ketua DPR sampai ada putusan tetap dari MKD,"'],
        ['from'=>'Desmon J Mahesa','jabatan'=>'Ketua Komisi III DPR Fraksi Gerindra','img'=>'http://berita360.com/uploads/content/medium_13desmon%20mahesa_10.JPG','url'=>'http://www.bijaks.net/aktor/profile/desmondjunaidimahesa50f91e722d81d','content'=>'"Tidak ada pilihan bagi kami anggota DPR, kalau perlu gentlemen dia mundur karena mempermalukan DPR"'],
        ['from'=>'Sudirman Said','jabatan'=>'Menteri ESDM','img'=>'http://liputanislam.com/wp-content/uploads/2014/11/sudirman-said.jpg','url'=>'http://www.bijaks.net/aktor/profile/sudirmansaid544ca5d6974a8','content'=>'"Iya, itu ada kop surat Kementerian ESDM, kemudian ada paraf saya. Saya kira ini laporan yang saya sampaikan ke MKD"'],
        ['from'=>'Joko Widodo','jabatan'=>'Presiden','img'=>'http://img.antaranews.com/new/2014/10/ori/20141020Jokowi-Presiden-Pelantikan-001.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'"Sekarang paling ramai apa? Papa minta pulsa, diganti papa minta saham"'],
        ['from'=>'KH. Hasyim Muzadi','jabatan'=>'Anggota Dewan Pertimbangan Presiden','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR97COEreeQ5nT5-t1wkEWNR7Ce6dSjdx4nJA4NYHuQiThMuk8ZuA','url'=>'http://www.bijaks.net/aktor/profile/khahmadhasyimmuzadi52d23543c7793','content'=>'"Sebetulnya, kalau yang dicatut orang biasa, itu pencemaran nama baik. Namun, kalau kepala negara, itu kriminal kenegaraan"'],
        ['from'=>'Elkana Goro Leba','jabatan'=>'seorang mahasiswa S-2 asal NTT','img'=>'http://1.bp.blogspot.com/-5CPfdoz39Ec/T0jHzP-KnPI/AAAAAAAAACw/W7op-6n-bms/s1600/bes%2Bcopy.jpg','url'=>'#','content'=>'"Saya sebagai masyarakat NTT merasa menyesal dan malu dengan perbuatan Setya Novanto yang sudah berulang kali membuat malu"'],
        ['from'=>'Hendardi','jabatan'=>'Ketua Setara Institute','img'=>'http://cdn.ar.com/images/stories/2012/12/hendardi_1.jpg','url'=>'http://www.bijaks.net/aktor/profile/hendardi54f6811b30c7e','content'=>'"Tindakan Setya Novanto juga bisa dikualifikasi sebagai penipuan dan atau pemerasan"'],
        ['from'=>'Ruhut Sitompul','jabatan'=>'Anggota DPR-RI Fraksi Demokrat','img'=>'http://www.rmol.co/images/berita/normal/867448_05501904072015_ruhut.jpg','url'=>'http://www.bijaks.net/aktor/profile/ruhutpoltaksitompulsh50f91f7018b5c','content'=>'"Kalian tahu komandan kami siapalah, kan kalian tahu"'],
        ['from'=>'Ferdinand Hutahean','jabatan'=>'Direktur Eksekutif Energy Watch Indonesia (EWI)','img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20151119_114816_harianterbit_ferdinan_hutahean.jpg','url'=>'#','content'=>'"Mengapa ini masuk kategori pengkhianatan? jawabannya adalah karena saham yang diminta SN itu adalah seharusnya milik negara dalam bentuk divestasi saham. Artinya, SN sedang berupaya merampok atau mengambil hak negara secara ilegal"'],

    ],

    'video'=>[
        ['id'=>'aUEBMlZcQK4'],
        ['id'=>'lJXIUMGAiQM'],
        ['id'=>'ufbxBjH3QCk'],
        ['id'=>'WqE6zoHnP30'],
        ['id'=>'7g5gCZHk-yY'],
        ['id'=>'sbr5q9E5QF0'],
        ['id'=>'YBtruZpmRVE'],
        ['id'=>'xZ_SvQb7Zbc'],
        ['id'=>'u_eT0HuU17A'],
        ['id'=>'uKH3v12W-g8'],
        ['id'=>'pUUkr0V-hJs'],
        ['id'=>'UnvN1bpGwnw'],
        ['id'=>'ggQ8sIdHEdM'],
        ['id'=>'PALctjlLZVQ'],
        ['id'=>'sD-0vAKI6HU'],
        ['id'=>'Gyg2ARLn-qM'],
        ['id'=>'IjYUBMKIBi4'],
        ['id'=>'NsNk0tdgLsg'],
        ['id'=>'Ey-uNSa350s'],
        ['id'=>'tesmLgjSc3A'],
        ['id'=>'kUwhAsOolIk'],
        //['id'=>'mHqedkRuFnk'],     
    ],

    'foto'=>[
        ['img'=>'http://www.bijaks.net/uploads/2015/11/Petisi-Mahasiswa-NTT-Desak-Pemeecatan-Setya-Novanto-Jaring-605-Tandatangan-Setya-Novanto-Bijaks-627x261.jpg'], 
        ['img'=>'http://www.bijaks.net/uploads/2015/11/Petisi-Online-Setya-Novanto-Layak-Dipecat-Tembus-32912-Pendukung-Bijaks-627x261.jpg'], 
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTp_9d-DOtQaZtTTwVglY0MM1HtRn9aTNfAlcOlZLZHOtwNt6TBQ'], 
        ['img'=>'https://ferdfound.files.wordpress.com/2015/11/111715-fahri-tuding-sudirman-said-yang-ingin-percepat-perpanjangan-kontrak-freeport.jpg?w=545'], 
        ['img'=>'http://www.bijaks.net/uploads/2015/11/Jimly-Sayangkan-Sudirman-Said-Bocorkan-Transkrip-Percakapan-Setnov-dengan-Bos-Freeport-Bijaks-627x261.jpg'], 
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTcgyNqo0S6oKsGEAsFtH8LSiSV2YGvWtzoUrOn_jBgqtw_chI8'], 
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/10/12/34/1052452/rizal-ramli-heran-sudirman-said-ngotot-bela-freeport-lM5.jpg'], 
        ['img'=>'http://assets2.jpnn.com/picture/normal/20151117_195351/195351_305257_sudirman_said_ngakak.JPG'], 
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/916927/big/081714000_1435842088-Menteri-ESDM-dan-Bos-Freeport.jpg'], 
        ['img'=>'http://ahok.org/wp-content/uploads/2013/06/Jokowi-11Jun131.jpg'], 
        ['img'=>'http://www.jokowinomics.com/media/static/images/2015/09/Sudirman-Said-Lapor-Jokowi-Sebelum-Terbang-ke-Freeport-640x360.jpg'], 
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQP8aCqvE-SesvJCZHbxLE9tp2Bl14UeH4QgtxgHlck_gT0JpzB9A'], 
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSB6Q4B4KXGsLmZpHUDQtMfDF_3SQcdBg5t_YTCrkCcr_Qi6E5r'], 
        ['img'=>'http://www.aktual.com/wp-content/uploads/2015/11/Sudirman-Said-13-11-2015-42-681x402.jpg'], 
        ['img'=>'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRNdnFeAFgEMDSvpGPW6cuxGFRTFnTFh8glHBnflN_hZ9cp74VhMQ'], 
        ['img'=>'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQl7mndH9rr2yuibnK0x1tGriizpElySLPQz6mfqw4qQePNZ6DV'], 
        ['img'=>'http://cdn.tmpo.co/data/2011/02/25/id_66032/66032_620.jpg'], 
        ['img'=>'http://cdn.tmpo.co/data/2015/11/17/id_455314/455314_620.jpg'], 
        ['img'=>'http://merahputih.com/news/_timthumb-project-code.php?src=http://server8.merahpoetih.com/gallery/public/2015/04/03/U5zcyOmf6b1428044432.jpg&w=462&h=306&a=c,t'], 
        ['img'=>'http://assets.kompas.com/data/photo/2015/11/16/18120372194-363788181010659881-20151116-171618780x390.JPG'], 
        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/1038953/big/009536800_1446212952-20151030-Paripurna-DPR-ke-9-3.jpg'], 
        ['img'=>'http://media.viva.co.id/thumbs2/2015/10/02/340199_ketua-dpr-setya-novanto-wakil-ketua-drp-fadli-zon_663_382.jpg'], 
        ['img'=>'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcS7jOMQGwe4jaetuKfGym0RLLHcYwNcBMyR2932GkmwI2BnBcXbJA'], 
        ['img'=>'http://politik.beritaprima.com/wp-content/uploads/sites/6/2015/04/wapresJK2.jpg'], 
        ['img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2015--11--49691-adian-napitupulu-setya-novanto-harus-dinonaktifkan.jpg'], 
        ['img'=>'http://images.detik.com/customthumb/2015/09/14/159/20150914_majalahdetik_198_page_073.jpg?w=780&q=90'], 
        ['img'=>'http://www.bijaks.net/uploads/2015/09/Adian-Napitupulu-Sikap-Setya-Novanto-dan-Fadli-Zon-Kotori-DPR-627x261.jpg'], 
        ['img'=>'http://static.gatra.id/images/gatracom/2015/rizki/09-Sep/FadliZon_SetyaNovanto_GATRAnews_ErvanBayu2.jpg'], 
        ['img'=>'http://poskotanews.com/cms/wp-content/uploads/2015/09/anggota-dpr-laporkan-setnov-3.jpg'], 
        ['img'=>'http://hariansib.co/photo/dir092015/hariansib_Bisa-Berujung-Pelengseran-Ketua-DPR-Setya-Novanto---MKD-akan-Panggil.jpg'], 
        ['img'=>'https://i.ytimg.com/vi/xUXJcmyUmd0/hqdefault.jpg'], 
        ['img'=>'http://www.rmolsumsel.com/images/berita/normal/727541_11374620112015_maroef.jpg'], 
        ['img'=>'https://pbs.twimg.com/media/CUAq-h1WUAE1C3W.jpg:large'], 
        ['img'=>'http://www.teropongsenayan.com/foto_berita/201511/18/medium_55meme-setya-novanto-minta-saham_20151117_160610.jpg'], 
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/844496/big/075605600_1428313478-Jokowi4.jpg'], 
        ['img'=>'http://lampost.co/images/w750_h400px__1447861448.jpg'], 
        ['img'=>'https://i.ytimg.com/vi/9ioBjoz-x3w/hqdefault.jpg'], 
        ['img'=>'http://sbsinews.com/wp-content/uploads/ktz/presdir-freeport-bekas-wakil-kabin-wajar-curiga-dan-merekam-30bhntloprbk6tcy1khybu.jpg'], 
        ['img'=>'http://t0.gstatic.com/images?q=tbn:ANd9GcSh3gFfWRjtwK1tPBPdThSoYosNt9KmI4_kvTFN7AYmHl1odMZF'], 
        ['img'=>'http://energitoday.com/uploads//2015/11/Menteri-ESDM-dan-Presdir-Freeport-viva.co_.id_.jpg'], 
        ['img'=>'http://baranews.co/system/application/views/main-web/foto_news/ori/836754248-067277200_1425389884-menteri_esdm_sudirman_said_2.jpg'], 
        ['img'=>'http://assets.kompas.com/data/photo/2015/08/10/1724579sudirman-said780x390.jpg'], 
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/1058048/big/044770400_1447752501-20151117--Ketua-DPR-Setya-Novanto-Jakarta-Johan-Tallo-03.jpg'], 
        ['img'=>'http://img.bisnis.com/posts/2015/11/23/494728/set8.jpg'], 
        ['img'=>'http://statis.dakwatuna.com/wp-content/uploads/2015/11/setya-novanto.jpg'], 
        ['img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRp1JNNbC2Qjga4uBN8Pw2kMMSV9UlItVSyolIgZUGAesxVX9nG3A'], 
        ['img'=>'http://www.beritaempat.com/wp-content/uploads/P_20150826_150842-588x330.jpg'], 
        ['img'=>'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRDoxymObWKhJuINukDyy-lKie4q2bdsZs5PswOwGGEOp2RtmOq'], 
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFAWLO_PbplFJtXoFkPV9K8vmBgrlRCmWH6tX_ITvOkUkGmEsF'], 
        ['img'=>'http://majalahkartini.co.id/sites/default/files/kunjungan_budi_gunawan_ke_bali.jpg'], 

    ],

   'kronologi'=>[
       'list'=>[
           ['date'=>'8 Juni 2015','content'=>'Anggota DPR berinisial SN, bersama dengan seorang pengusaha, telah beberapa kali memanggil serta bertemu dengan pimpinan PT Freeport Indonesia. Hari Senin, 8 Juni, adalah pertemuan ketiga. 
Tepatnya pada pukul 14:00-16:00 WIB di sebuah hotel di kawasan SCBD, Jakarta Pusat, anggota DPR itu menjanjikan cara penyelesaian tentang kelanjutan kontrak PT FI dan meminta PT FI memberikan saham pada Jokowi dan Kalla.
Anggota DPR ini menjanjikan sebuah cara penyelesaian kepada pihak yang sedang bernegosiasi dengan RI, sembari meminta saham perusahaan dan saham proyek pembangkit listrik'],
           ['date'=>'16 November 2015','content'=>'Sudirman melaporkan anggota DPR berinisial SN tersebut — yang diduga sebagai Ketua DPR Setya Novanto — ke MKD hari ini. Namun Sudirman menolak menyebut nama Setya Novanto secara terang-terangan.
Sore harinya, Jusuf Kalla dijadwalkan menerima Ketua DPR RI Setya Novanto di Kantor Wakil Presiden, Jalan Medan Merdeka Utara, Jakarta Pusat. 
Berdasarkan informasi dari situs resmi Sekretariat Wakil Presiden pada Senin, Setya Novanto menemui Kalla pada pukul 15:00 WIB. Namun Setya membantah tudingan tersebut.'],
           ['date'=>'17 November 2015','content'=>'Direktur Advokasi Pusat Studi Hukum dan Kebijakan Indonesia (PSHK) Ronald Rofiandri mendesak Setya Novanto mengundurkan diri sementara sebagai Ketua DPR RI untuk mempermudah proses pemeriksaan oleh MKD.
Sementara itu, usai bertemu Setya di kantornya kemarin, Kalla menyatakan akan menunggu proses lebih lanjut di DPR.'],
           ['date'=>'17 November 2015','content'=>'Nama Menteri Koordinator Politik, Hukum, dan Keamanan Luhut Binsar Panjaitan disebut mengetahui saham yang akan diberikan untuk Presiden Jokowi dan Wakil Presiden Jusuf Kalla.
Dalam transkrip rekaman tersebut, pengusaha bernama Reza yang hadir bersama Setya Novanto menyebutkan keterlibatan Luhut dalam besaran saham untuk Pembangkit Listrik Tenaga Air (PLTA).
Rencananya, mereka mencari referensi yang dapat bekerjasama dengan PT. Freeport Indonesia. Dalam skenario ini Freeport hanya akan memiliki saham sebesar 51 persen.'],
           ['date'=>'19 November 2015','content'=>'Luhut juga membantah ada pertemuan antara dia dengan pihak Freeport.  Selain Luhut, nama Deputi I Kepala Staf Kepresidenan Darmawan Prasodjo juga disebut. Saat itu ia adalah anak buah Luhut, saat ia menjabat sebagai Kepala Staf Kepresidenan'],
           ['date'=>'23 November 2015','content'=>'Rapat kasus dugaan pelanggaran kode etik Setya dijadwalkan akan digelar internal oleh Mahkamah Kehormatan Dewan (MKD). 
Namun, anggota dan pimpinan MKD justru mempermasalahkan keabsahan Menteri Energi dan Sumber Daya Mineral Sudirman Said sebagai pelapor. Ketua Mahkamah Surahman Hidayat menuturkan, keabsahan pelapor itu tercantum dalam pasal 5 bab IV Peraturan Tata Beracara Mahkamah. 
MKD selanjutnya akan mengadakan rapat lagi pada tanggal 24 November dan memanggil ahli hukum untuk mendalami peraturan tersebut. 
MKD juga mempermasalah keabsahan barang bukti. Wakil Ketua Mahkamah Hardisoesilo menyebut laporan dan barang bukti berbeda. Durasi rekaman tertulis 120 menit di laporan tapi durasi barang bukti hanya 11 menit 38 detik
'],
       ]
   ],

]


?>


<style>
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/lobiliar/top.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/lobiliar/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['block1']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block3']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block3']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['right1']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['right1']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block4']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block4']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block5']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block5']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block6']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block6']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block7']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block7']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <?php /*
        <?php if(!empty($data['institusiPendukung'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">INSTITUSI TERLIBAT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['institusiPendukung'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                    <a href="<?php echo $val['url'];?>"><img src="<?php echo $val['image'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>
        
       

       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">NEGARA PROTES</span></h5>
        <div id="accordion" class="panel-group row">
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                    <a href="http://www.bijaks.net/aktor/profile/suriah5371b98429bfd">
                        <img src="http://1.bp.blogspot.com/-rZjXBqYvrsA/UBFQOkyOSmI/AAAAAAAAANI/T1OBu1ZcvgA/s1600/gambar-bendera-syria.JPG" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;">
                        SURIAH</b><br>
                    <div class="clear"></div>
                    </p>
                </div>
        </div>
        <div class="clear"></div>
        */?>



        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block9']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block9']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <?php if(!empty($data['quotePendukung'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePendukung'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>

        <?php if(!empty($data['quoteProtes'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PROTES</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quoteProtes'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>">
                    <img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>



		<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['foto'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['video'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
<?php /*
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['berita'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>
*/ ?>
        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>
