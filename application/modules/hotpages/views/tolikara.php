<?php 
$data = [
    'NARASI'=>[
        'title'=>'DAMAI DI TOLIKARA',
        'narasi'=>'<img src="http://www.klikpositif.com/media/images/news/insiden-tolikara-kedua-tersangka-akui-turut-lempar-jemaah_20150727145008.jpg" class="pic">Kerusuhan berdarah terjadi disalah satu distrik di kabupaten Tolikara, Papua, menyusul protes yang dilakukan ratusan anggota jemaat Gereja Injil  Indonesia (GIDI) terhadap penyelenggaraan Salat Idul Fitri di lapangan Markas Komando (MAKO) rayon Militer pada Jumat, (17/07/2015).
                    </p><p>Ratusan massa mendatangi lapangan tempat diselenggarakannya Salat Ied. Mereka meminta agar umat Islam tidak menyelenggarakan Salat Ied di lapangan, tapi di dalam tempat ibadah.
                    </p><p><img src="http://statis.dakwatuna.com/wp-content/uploads/2015/07/insiden-tolikar.jpg" class="pic2" style="height: 140px;">Polisi yang saat itu mengamankan jalannya Salat idul Fitri tersebut langsung membubarkan ratusan massa tersebut dengan menembakkan senjata.  Jemaat GIDI membalasnya dengan melakukan tindakan anarkis. Akibatnya, kericuhan pun pecah.
                    </p><p>Massa lalu membakar dan menghanguskan puluhan rumah dan toko serta mushallah. Dalam kerusuhan tersebut, puluhan orang mengalami luka serius dan seorang meninggal akibat terkena peluru.
                    </p><p><img src="http://www.jurnalitas.com/go/file/uploads/2015/07/masjid-di-tolikara-mojok.jpg" class="pic">Pihak kepolisian mengklaim tindakan penembakan tersebut sebagai upaya untuk mengamankan situasi, karena ratusan jemaat GIDI berpotensi melakukan aksi penyerangan terhadap umat muslim yang sedang melakukan Salat, namun dari pihak jemaat GIDI berdalih bahwa protes mereka akan dilukakan dengan negosiasi. Tetapi kericuhan justru pecah akibat ulah kepolisian yang bertindak sewenang-wenang.
                    </p><p>Pasca pecahnya konflik berdarah tersebut menyulut beragam tanggapan dari banyak kalangan. Banyak pihak yang menyayangkan pecahnya konflik tersebut. Akibat kelalaian pihak kepolisian diduga sebagai pemicu terjadinya kerusuhan yang melibatkan kepolisian dan pihak jemaat GIDI. Puluhan jemaat dan seorang lainnya tewas terkena timah panas aparat kepolisian.'
    ],
    'ANALISA'=>[
        'narasi'=>'<img src="http://www.intiwarta.com/wp-content/uploads/2015/07/papua-6.jpg" class="pic2">Peristiwa Tolikara menambah daftar panjang kasus intoleransi antar umat beragama di Indonesia. Kekerasan yang bermotif agama memang sering terjadi di tanah air. Kasus kekerasan terhadap Ahmadiyah, kasus Sampang Madura, Ambon dan Poso merupakan peristiwa yang mencoreng kerukunan antar umat beragama di Indonesia.
                    </p><p class="font_kecil">Kerukunan antar agama dan perbedaan keyakinan di Indonesia masih menjadi polemik yang berpotensi melahirkan konflik bernuangsa SARA. Sikap abai masyarakat dan pemerintah terhadap keberadaan kelompok minoritas menjadi salah satu pemicu meningkatnya kasus diskriminasi.
                    </p><p class="font_kecil">Umumnya, kasus semacam ini dilakukan oleh kelompok yang lebih mayoritas. Di beberapa tempat, kasus kekerasan terhadap kelompok minoritas sering diabaikan oleh pemerintah sehingga memicu konflik dengan skala yang lebih besar.
                    </p><p class="font_kecil"><img src="http://www.szaktudas.com/wp-content/uploads/2015/07/Aksi-Damai-muslim-surakarta-untuk-papua.jpg" class="pic">Pemerintah seakan tidak hadir untuk melindungi hak warganegaranya dalam beragama dan berkeyakinan. Kerusuhan di Tolikara akibat kegagalan pemerintah dalam membangun toleransi di daerah. Masyarakat yang berbeda keyakinan dengan mudah melakukan tindakan anarkis terhadap kelompok lain karena tidak adanya tatanan kerukunan dan toleransi  yang dibangun sebelumnya oleh pemerintah.
                    </p><p class="font_kecil">Selain itu, penanganan yang dilakukan oleh pemerintah terhadap konflik tersebut masih menggunakan cara-cara yang justru dapat menyulut emosi pihak yang bertikai sehingga menimbulkan konflik yang lebih besar.'
    ],
    'SOLUSI'=>[
        'narasi'=>'<img src="http://elshinta.com/upload/article/mef-voaindonesia3566634924.jpg" class="pic">Respon pemerintah dalam hal ini Presiden Jokowi bersama menteri terkait, terbilang tangkas dan cepat. Seminggu setelah insiden tolikara terjadi, Presiden langsung menginstruksikan penanganan komprehensif atas insiden tersebut. Hal ini ditegaskan Presiden Joko Widodo dalam rapat kabinet terbatas di Istana Negara, Jakarta, Rabu tanggal 22 Juli 2015. 
                    </p><p class="font_kecil">Penanganan itu meliputi penindakan hukum terhadap pelaku, rehabilitasi bangunan rusak, dan menjalin dialog dengan tokoh-tokoh di Papua.
                    </p><p class="font_kecil">Sehari setelah itu, presiden juga memanggil seluruh pemuka agama ke Istana membahas soal kerukunan umat beragama menyusul kerusuhan di Tolikara, Papua. Pertemuan itu dihadiri oleh tokoh-tokoh tersebut antara lain Azyumardi Azra, Ketua Pengurus Besar Nahdlatul Ulama (PBNU) Said Aqil Siradj, dan Ustad Yusuf Mansyur, hadir juga perwakilan dari PWI, PGI, pada Kamis, 23/072015.
                    </p><p class="font_kecil"><img src="http://brightcove01.brightcove.com/23/4077388032001/201507/2544/4077388032001_4369476713001_150723-TEMPO-CHANNEL-TEMPO-KINI-BEGINI-CARA-MUSLIM-DAN-GIGI-BERDAMAI-DI-TOLIKARA.jpg?pubId=4077388032001" class="pic2">Berikut intruksi presiden mengenai Insiden Tolikara. Pertama, Jokowi meminta penegakan hukum diselesaikan. Kedua, Presiden memerintahkan segera dilakukan kembali pembangunan fasilitas yang rusak di Tolikara. Ketiga, Presiden akan dialog dengan tokoh agama dan tokoh masyarakat Papua untuk sama sama menenangkan situasi di sana dan nasional.
                    </p><p class="font_kecil">Komite Umat untuk Tolikara (Komat Tolikara) menyatakan bantuan dari masyarakat umum untuk korban peristiwa di Karubaga, Kabupaten Tolikara, Papua, mencapai Rp 1,3 miliar.
                    </p><p class="font_kecil">Diketahui ada enam rumah, sebelas kios, dan satu musala ludes terbakar. Sebanyak 11 warga terluka dan 1 anak tewas. Ketum Persekutuan Gereja-Gereja dan Lembaga-Lembaga Injili Indonesia (PGLII), Ronny Mandang, menjelaskan bahwa semua warga Tolikara yang jadi korban dalam kerusuhan di Tolikara, Jumat 17 Juli 2015 adalah warga jemaat Gereja Injili Di Indonesia (GIDI).'
    ],
    'BERUJUNG'=>[
        'narasi'=>'<img src="http://cdn.sindonews.net/dyn/620/content/2015/07/30/14/1027703/kapolda-papua-yotje-mende-digantikan-kapolda-papua-barat-alR.jpg" class="pic">Pasca-insiden yang terjadi di Tolikara, Papua, Kapolri Jenderal Badrodin Haiti melakukan mutasi terhadap Kapolda Papua Irjen Yotje Mende. Posisi Yotje digantikan Kapolda Papua Barat Brigjen Paulus Waterpaw.
                    </p><p class="font_kecil">Hal ini tertuang dalam Surat Telegram Kapolri Nomor ST/195//VII/2015 tertanggal Kamis 30 Juli 2015, posisi Waterpaw akan diisi oleh Brigjen Royke Lumowa yang sebelumnya bertugas di Kementerian Koordinator Politik, Hukum, dan Keamanan sebagai Asisten Deputi Koordinasi Penanganan Daerah Konflik dan Kontijensi. 
                    </p><p class="font_kecil">Seperti diketahui, di akhir masa pensiun, Yotje dihadapkan dengan insiden penyerangan umat Islam di Distrik Karubaga, Tolikara, Papua, saat hari raya Idul Fitri 1436 Hijriah, Jumat 17 Juli 2015. Selain itu Yotje juga disibukkan dengan  keikutsertaannya di seleksi calon pimpinan Komisi Pemberantasan Korupsi (KPK).'
    ],
    'PROFIL'=>[
        'narasi'=>'<img src="https://upload.wikimedia.org/wikipedia/id/e/e1/Karubaga_Tolikara_1.JPG" class="pic2">Kabupaten Tolikara memiliki luas wilayah 5.234 km2 yang terbagi menjadi 4 kecamatan dengan Karubaga sebagai ibukota kabupaten. Kabupaten ini memiliki penduduk sebanyak 54.821 jiwa (2003). Wilayahnya berbatasan dengan Kabupaten Sarmi di sebelah utara, Kabupaten Jayawijaya di sebelah Selatan, Kabupaten Puncak Jaya di sebelah barat dan Kabupaten Jawawijaya di sebelah Timur. Dari keempat distrik yang ada (Karubaga, Kanggime, Kembu dan Bokondini), hanya distrik Karubaga dan Kanggime yang dapat dijangkau melalui udara dan jalan darat. Melalui udara, Distrik Karubaga atau Kanggimi dapat dicapai dari Wamena dalam waktu sekitar 20 menit.
                    </p><h5>Perekonomian</h5><p class="font_kecil">Kontribusi utama perekonomian daerah ini datang dari pertanian. Di daerah pedalaman yang merupakan ulayat mereka secara turun temurun, kegiatan pertanian dilakukan secara tradisional. Lahan tanaman bahan pangan sebagian besar ditanami ubi jalar. Tanaman rambat ini memang merupakan makanan pokok penduduk kabupaten ini. Sentra penghasil ubi jalar berada di Distrik Karubaga. Sama seperti daerah lain di Papua, babi merupakan ternak utama masyarakat. Karubaga dan Kanggime merupakan distrik yang terbanyak memelihara ternak ini. Tolikara juga merupakan daerah penghasil batu gamping yang digunakan sebagai bahan baku pengolahan semen. Potensi batu gamping mencapai jutaan ton kubik menyebar dari Tolikara sampai Yahukimo dan Jayawijaya.
                    </p><h5>Peta Kabupaten Tolikara</h5><p class="font_kecil">Kabupaten ini akan dimekarkan dari Kabupaten Tolikara dan Kabupaten Lanny Jaya. Distrik yang mungkin bergabung ke dalam kabupaten ini meliputi :<br>Dari Kabupaten Tolikara : Yuneri, Tagineri, Tagime, Yaleka, Danime, Poganeri<br>Dari Kabupaten Lanny Jaya: Maki, Poga, Dimaba, Gamelia
                    </p><h5>Kota Karubaga</h5><p class="font_kecil">Rencana pemerintah untuk pemekaran daerah otonomi Kota Karubaga yang  akan pisah dengan Kota induknya yakni Kabupaten Tolikara masih menunggu  proses lebih lanjut. Rencananya Kota Karubaga ada 9 kecamatan yang nantinya masuk kedalam kota Karubaga.'
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="http://cdn.rimanews.com/bank/masjid-di-bakar.jpg" class="pic2">Kekerasan yang berlatar belakang agama yang terjadi di Papua memicu polemik dari beragam kalangan. Pasca insiden tersebut, pemerintah cenderung melihat konflik tersebut disebabkan karena kurangnya komunikasi di antara komunitas Gereja Injili di Indonesia (GIDI) dan Muslim lokal.
                    </p><p class="font_kecil">Kekerasan tersebut merupakan ekspresi ketidakpuasan masyarakat yang emosional. Bahkan kasus tersebut hanya tindakan kriminal biasa, bukan konflik agama.
                    </p><p class="font_kecil"><img src="http://hizbut-tahrir.or.id/wp-content/uploads/2015/07/IMG_9003.jpg" class="pic">Sikap berbeda justru datang dari para aktivis penggiat HAM. Mereka menilai konflik tersebut merupakan pelanggaran HAM, yakni kebebasan beragama dan berkeyakinan.
                    </p><p class="font_kecil">Untuk itu, pemerintah dihimbau agar lebih peka terhadap keberadaan kelompok minoritas yang rentang terhadap tindakan diskriminasi. Pemerintah harus dapat memberikan jaminan perlindungan hukum dan menjamin kekerasan atas nama agama tidak terulang lagi.'
    ],
    'KRONOLOGI'=>[
        'list'=>[
            ['date'=>'','content'=>'Tanggal 11 Juli 2015, beredar selebaran yang yang berisi larangan terhadap agama lain untuk melaksanakan kegiatan keagamaan. Selebaran ditandangani oleh Pendeta Mathem Jingga, S.Th, MA dan Pendeta Nayus Wendas, S.Th.'],
            ['date'=>'','content'=>'Pukul 07.00 WIT saat jamaah muslim akan memulai kegiatan Salat Ied di lapangan Makoramil 1702-11, Karubaga. Ratusan Jemaat GIDI mendatangi lapangan tempat berlangsungnya kegiatan salat Idul Fitri. Massa beroperasi menghimbau kepada Jemaah Salat Ied untuk tidak melakasanakan ibadah Salat Ied di lapangan.'],
            ['date'=>'','content'=>'Selang beberapa menit kemudian, massa yang dikoordinir oleh pendeta Marthen Jingga mulai melempari jamaah salat ied dengan batu. Mereka memaksa kegiatan salat Ied agar segera dibubarkan.'],
            ['date'=>'','content'=>'Selanjutnya, kericuhan semakin memanas saat aparat kepolisian melepaskan tembakan peringatan. Namun massa jemaat GIDI tidak mengindahkan peringatan tersebut.'],
            ['date'=>'','content'=>'Pukul 08.00, massa justru melempari polisi dengan batu. Kericuhan terjadi antara aparat polisi dengan massa Jemaat GIDI'],
            ['date'=>'','content'=>'Api mulai membakar puluhan rumah, kios dan sebuah mushallah'],
            ['date'=>'','content'=>'Pukul 09.10 WT, massa dari dari pendeta Marthen Jingga berkumpul di ujung bandara karubaga untuk bersiaga.']
        ]
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'komiteummatuntuktolikarakomat55bc370dbd56b'],
            ['page_id'=>'majelisulamaindonesia53097864039ce'],
            ['page_id'=>'komnasham54c1eac938164'],
            ['page_id'=>'nahdlatululamanu53310bea6f11d'],
            ['page_id'=>'ppmuhammadiyah5296b45eefcaa'],
            ['page_id'=>'gerejainjilidiindonesiagidi55a9bc4d6dfca']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'setarainstitute533a7f7211a39'],
            ['page_id'=>'jaringangusdurian55bc3b936972d'],
            ['page_id'=>'frontpembelaislam5317d72f772e0'],
            ['page_id'=>'gmki55bc475d79ca6']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Tedjo Edhy','jabatan'=>'Menkopulhakam RI','img'=>'http://cdn.sindonews.net/dyn/620/content/2014/10/27/12/915782/tedjo-edhy-eks-ksal-banting-setir-jadi-politikus-tA4.jpg','url'=>'http://www.bijaks.net/aktor/profile/laksamanatnipurntedjoedhie540925438ba50','content'=>'Itu kami redam. Itu hanya solidaritas sempit'],
        ['from'=>'Ir. H. Joko Widodo','jabatan'=>'Presiden RI','img'=>'https://yt3.ggpht.com/-p0NW5C1v22g/AAAAAAAAAAI/AAAAAAAAAAA/_Bm12bemzeo/s900-c-k-no/photo.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'Saya kira tidak ada kata terlambat sehingga tidak ada gesekan kecil dan lebih baik aparat tegas menyelesaikan masalah (Tolikara) ini'],
        ['from'=>'Maruar Sirait','jabatan'=>'Politisi PDIP','img'=>'http://assets.jaringnews.com//3/2013/05/16/d8668197f46f255baa5fecda33f2b039_1.jpg','url'=>'http://www.bijaks.net/aktor/profile/Maruarar','content'=>'Kejadian ini telah mengusik kita semua. Para pelakuanya harus mendapatkan sanksi hukum dan negara tidak boleh melakukan pembiaran atas kasus ini'],
        ['from'=>'Dorman Wandikmbo','jabatan'=>'Presiden Gereja Injil di Indonesia (GIDI)','img'=>'https://pbs.twimg.com/media/CKcinELUYAILgB3.jpg:medium','url'=>'','content'=>'Kami menyetujui dan menyepakati bahwa yang pertama adalah masalah Tolikara akan kami selesaikan secara adat dengan damai, aman, untuk sekarang dan seterusnya'],
        ['from'=>'Luhut Panjaitan','jabatan'=>'Kepala Staff Kepresidenan','img'=>'http://cdn.tmpo.co/data/2014/10/22/id_336533/336533_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/jendralpurnluhutbinsarpanjaitan5189af75dc288','content'=>'Selama ini kita suka tidak menuntaskan. Tapi saya lihat sekarang Presiden minta menuntaskan semua. Tapi langkah-langkah awal, sudah diambil. Saya kira sudah bagus'],
        ['from'=>'Ustaz Ali Muchtar','jabatan'=>'Tokoh Agama Islam di Tolikara','img'=>'http://www.suara-islam.com/images/berita/tolikara-ali-mukhtar_20150723_172725.jpg','url'=>'','content'=>'Kami mewakili umat Muslim dan selaku tokoh agama Islam yang ada di Tolikara menyampaikan bahwa insiden yang terjadi di Tolikara diselesaikan secara damai dan kami setuju bahwa insiden tersebut bukan masalah SARA atau agama'],
        ['from'=>'Romo Benny Susetyo','jabatan'=>'Sekretaris Dewan Nasional Setara','img'=>'http://www.hidupkatolik.com/foto/bank/images/sajut-a-benny-susetyo-pr-hidup-katolik.jpg','url'=>'http://www.bijaks.net/aktor/profile/romobennysusetyo51cba6402881a','content'=>'Masalah belum jelas kita tunggu tim independen untuk menjelaskan duduk masalah, lebih baik media memberitakan yang damai karena penting untuk menjaga persatuan bangsa diatas segalanya'],
        ['from'=>'Jusuf Kalla','jabatan'=>'Wapres RI','img'=>'http://www.thestage.luwuraya.net/wp-content/uploads/2012/10/jusuf-kalla.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'Sekarang sudah membaik. Di Papuanya sudah baik, sudah damai. Presiden sudah memerintahkan untuk membangun kembali. Pemdanya, Mensos di situ juga membantu, semua ikut bantu, jadi secara umum di Papua sudah aman'],
        ['from'=>'KH Tengku Zulkarnain','jabatan'=>'Wakil Sekretaris Jenderal Majelis Ulama Indonesia (Wasekjen MUI)','img'=>'http://cdn.ar.com/images/stories/2014/03/wakil-ketua-majelis-ulama-indonesia-mui-tengku-zulkarnaen-_130905141032-172.jpg','url'=>'http://www.bijaks.net/aktor/profile/tengkuzulkarnain54a618f7d273d','content'=>'Dengan bersatunya tokoh-tokoh agama dan telah diselesaikannya masalah Tolikara secara adat itu memang tujuan kita agar tercapai kedamaian sampai hari kiamat'],
        ['from'=>'Lukman Hakim Syarifuddin','jabatan'=>'Menteri Agama RI','img'=>'https://upload.wikimedia.org/wikipedia/id/4/4c/Lukman_Hakim_Saifuddin_MPR.jpg','url'=>'http://www.bijaks.net/aktor/profile/drshlukmanhakimsaifuddin511756084eb20','content'=>'Substansinya harus dipahami dulu, apa maksud dari wacana perda itu, bagaimana isinya, dan hak-hak umat beragama. Jadi itu harus didalami dulu. Maknanya harus dikaji lebih mendalam menurut saya supaya kita tidak beda persepsi'],
        ['from'=>'Teten Masduki','jabatan'=>'Anggota Tim Komunikasi Presiden','img'=>'http://radarsukabumi.com/wp-content/uploads/2012/10/Teten-Masduki.jpg','url'=>'http://www.bijaks.net/aktor/profile/tetenmasduki51a78cb913c9c','content'=>'Bukan khusus Tolikara, tetapi pada dasarnya mempertahankan NKRI dengan keragaman suku agama'],
        ['from'=>'Sutiyoso','jabatan'=>'KaBIN RI','img'=>'https://upload.wikimedia.org/wikipedia/id/a/a3/Sutiyosodki.jpg','url'=>'http://www.bijaks.net/aktor/profile/letjentnipurnsutiyoso511c2b6deeb46','content'=>'Kemajemukan itu juga jadi titik lemah kita. Tapi saya berharap justru kemajemukan itulah yang menjadi persatuan bangsa kita']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Ayub Manuel','jabatan'=>'Ketua Umum GMKI','img'=>'http://cdn.ar.com/images/stories/2012/06/shobri-lubis.jpg','url'=>'','content'=>'GMKI menilai ada kejanggalan dari sejumlah informasi yang beredar'],
        ['from'=>'Hendardi','jabatan'=>'Ketua SETARA Institute','img'=>'http://www.satuharapan.com/uploads/pics/news_36101_1430131190.jpg','url'=>'','content'=>'meminta polisi untuk segera melakukan penyelidikan yang adil terkait kerusuhan di Tolikara, Papua'],
        ['from'=>'Alissa Wahid','jabatan'=>'Jaringan Gusdurian','img'=>'http://img.lensaindonesia.com/uploads/1/2015/05/31373-hendardi-setara-percuma-reshuffle-kalau-kepemimpinan-jokowi-tak-berubah.jpg','url'=>'http://www.bijaks.net/aktor/profile/hendardi54f6811b30c7e','content'=>'Kami juga menyayangkan kegagalan aparat penegak hukum dalam melindungi kebebasan beribadah dan mencegah terjadinya konflik sosial sebagaimana diamanatkan konstitusi dan undang-undang'],
        ['from'=>'Ahmad Shabri Lubis','jabatan'=>'Ketua Dewan Pimpinan Pusat Front Pembela Islam (FPI)','img'=>'http://www.fastrack-funschool.com/images/upload/images/alisa.jpg','url'=>'http://www.bijaks.net/aktor/profile/meilanyalissa51c29f5ac5a47','content'=>'FPI mendorong Kepolisian untuk menegakkan hukum yang berkeadilan, karena kami lihat ada pergeseran yang seakan-akan menganggap ini sebuah permasalahan kecil, tidak tahu dampak ke depannya']
    ],
    'VIDEO'=>[
        ['id'=>'mN-ap5cdNOs'],
        ['id'=>'NqQ9alT5xHo'],
        ['id'=>'gwT_sHF3cEA'],
        ['id'=>'_JWlbIpyc9Q'],
        ['id'=>'7d6C3B7MOBM'],
        ['id'=>'nsA5Vr5Gdxg'],
        ['id'=>'cing7RISNU0'],
        ['id'=>'3Ya1AHuBh4M'],
        ['id'=>'XAx7WGbdWFg'],
        ['id'=>'dwNNEtnXcyM'],
        ['id'=>'x6r6vnliRPQ'],
        ['id'=>'cVWbdLT3NRg'],
        ['id'=>'0S-9N2Xvr_o'],
        ['id'=>'XYnp4TBNChA']
    ],
    'FOTO'=>[
        ['img'=>'http://islamedia.id/wp-content/uploads/2015/07/papua_damai-600x330.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/07/23/414924/UeMkUratFO.jpg?w=668'],
        ['img'=>'http://www.piyunganonline.org/sites/default/files/field/image/19b18c61-de2f-412b-a1f0-02d476392a60_169.jpg'],
        ['img'=>'http://www.metrosulawesi.com/sites/default/files/styles/img_780_439/public/main/articles/foto-3_10.jpg?itok=x3JJcW_F'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/07/20/6eb00f49-af1a-4611-ac40-96482289aa8d_169.jpg?w=780&q=90'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/sejumlah-tokoh-agama-saling-bergandengan-tangan-saat-pembacaan-pernyataan-_150724143532-520.jpg'],
        ['img'=>'http://img1.beritasatu.com/data/media/images/medium/1437707476.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/07/31/337/1188418/jelang-pensiun-yotje-mende-bersyukur-tolikara-damai-AXlwF3N2uQ.jpg'],
        ['img'=>'http://kriminalitas.com/wp-content/uploads/2015/07/IMB_000224715.jpg'],
        ['img'=>'http://cdn-media.viva.id/thumbs2/2015/07/23/326368_forum-komunikasi-pimpinan-daerah-riau-_663_382.jpg'],
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/07/18/569210/670x335/5-butir-kesepakatan-damai-tokoh-agama-terkait-insiden-tolikara.jpg'],
        ['img'=>'http://images.detik.com/customthumb/2015/07/22/157/panglim1.jpg?w=780&q=90'],
        ['img'=>'http://img1.beritasatu.com/data/media/images/medium/871437117056.jpg'],
        ['img'=>'http://analisadaily.com/assets/image/news/big/2015/07/presiden-jokowi-ditemui-tokoh-masyarakat-tolikara-di-istana-merdeka-154075-1.jpg'],
        ['img'=>'http://www.jpnn.com/picture/watermark/20150723_170619/170619_105235_Ulama_Tolikara_ric_d.JPG'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/07/22/141832020150721-120602780x390.jpg'],
        ['img'=>'http://beritahati.com/images/artikel/1169.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/07/23/cb22d279-5715-445b-aa42-39a8ce119927_169.jpg?w=650'],
        ['img'=>'http://elshinta.com/upload/article/mef-voaindonesia3566634924.jpg'],
        ['img'=>'http://static.skalanews.com/media/news/thumbs-396-263/kerusuhan_tolikara_papua-pojoksatu.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/07/25/5303a0ff-46b2-42f0-9626-5992386ac4ba_169.jpg?w=650'],
        ['img'=>'http://img.antaranews.com/new/2015/07/ori/20150725antarafoto-presiden-tokoh-tolikara-240715-ym-1.jpg']
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }    
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

    .gallery li {
        display: block;
        float: left;
        height: 50px;
        margin-bottom: 7px;
        margin-right: 0px;
        width: 25%;
        overflow: hidden;
    }
    .gallery li a {
        height: 100px;
        width: 100px;
    }
    .gallery li a img {
        max-width: 97%;
    }

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $(".gallery").lightGallery();
    $(".gallery2").lightGallery();
})
</script>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/tolikara/top_kcl.png")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ANALISA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['ANALISA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PRO-KONTRA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROKONTRA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">TOLIKARA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://tolikarakab.go.id/wp-content/uploads/2015/04/LOGO-KABUPATEN-TOLIKARA.png" style="width: 50%;margin: 0 auto;display: block;">
                    <p style="margin-left: 20px;margin-right: 20px;">
                        <div class="col-xs-6">
                            <ul>Kabupaten Tolikara
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Semboyan : Nawi Arigi</li>
                            </ul>
                            <ul>Peta lokasi Kabupaten Tolikara
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Koordinat : Mien Kogoya</li>
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Provinsi :Papua</li>
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Ibu kota : Karubaga</li>
                            </ul>
                            <ul>Pemerintahan :
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Bupati : Usman Wanimbo</li>
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Wakil Bupati : Amos Jikwa</li>
                            </ul>
                        </div>
                        <div class="col-xs-6">
                            <ul>DAU : Rp. 507.270.132.000,- (2013)</ul>
                            <ul>Luas : 14.564 km2</ul>
                            <ul>Populasi :
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Total 147.750 jiwa (2000)</li>
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Kepadatan: 10,14 jiwa/km2</li>
                            </ul>
                            <ul>Demografi
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">Kode area telepon</li>
                                <li style="margin-left: 20px;list-style-type: square;font-size: 12px;">12250</li>
                            </ul>
                        </div>
                    </p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KRONOLOGI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">   
                <ol style="list-style-type: circle;margin-left: -10px;">
                    <?php
                    foreach ($data['KRONOLOGI']['list'] as $key => $val) {
                        echo "<li>".$val['content']."</li>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PROFIL TOLIKARA</span></h5>
        <img src="https://upload.wikimedia.org/wikipedia/id/b/b8/Peta_Infrastruktur_Kabupaten_Tolikara_%282012%29.gif" style="width: 100%;">
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROFIL']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <h5 style="padding-top: 15px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'komiteummatuntuktolikarakomat55bc370dbd56b'){
                    $photo = 'http://www.kemanusiaan.id/content_images/toliharadamai_thumbmedium.jpg';
                    $pageName = 'Komat (Komite Ummat Untuk Tolikara)';
                }
                elseif($val['page_id'] == 'komnasham54c1eac938164'){
                    $photo = 'https://upload.wikimedia.org/wikipedia/id/f/fa/Logo-Komnas-HAM.png';
                    $pageName = 'Komnas HAM';
                }
                elseif($val['page_id'] == 'gerejainjilidiindonesiagidi55a9bc4d6dfca'){
                    $photo = 'http://img.eramuslim.com/media/thumb/500x0/2015/07/logo-GIdI.jpg';
                    $pageName = 'GIDI';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'setarainstitute533a7f7211a39'){
                    $photo = 'http://setara-institute.org/wp-content/uploads/2014/11/si-logo-2.png';
                    $pageName = 'SETARA INSTITUTE';
                }
                elseif($val['page_id'] == 'jaringangusdurian55bc3b936972d'){
                    $photo = 'http://3.bp.blogspot.com/-jvrw0-Qbies/VbCouVTHwdI/AAAAAAAABIA/l4zHYhShdbU/s1600/1392106706844.jpg';
                    $pageName = 'Jaringan Gusdurian';
                }
                elseif($val['page_id'] == 'gmki55bc475d79ca6'){
                    $photo = 'https://upload.wikimedia.org/wikipedia/id/9/9c/Gmki_02.jpg';
                    $pageName = 'GMKI';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SOLUSI DAMAI DARI ISTANA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SOLUSI']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERUJUNG MUTASI KAPOLDA PAPUA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['BERUJUNG']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['FOTO'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                        <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>            
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>