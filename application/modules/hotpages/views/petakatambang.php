<?php
$data = [
    'block1'=>[
        'title'=>'Tragedi Tambang di Selok Awar-Awar',
        'narasi'=>'
        <img src="http://www.bangsaonline.com/images/uploads/berita/700/8d9037c33d1ec23b32c2cb2d149b554d.jpg" class="pic">
        <p>Aksi  penolakan penambangan pasir ilegal di Selok Awar-Awar, Kabupaten Lumajang, Jatim 
        berujung pada tewasnya seorang aktivis lingkungan dan mencederai seorang aktivis lainnya. 
        Dua aktivitis tersebut yakni Salim Kancil dan Tosan tergabung dalam Forum Komunikasi Masyarakat 
        Peduli Desa Selok Awar-Awar. Mereka dikenal cukup vokal dalam menolak aktivitas pertambangan 
        pasir yang merusak lingkungan.</p>
        <p>Pengeroyokan sadis yang berujung maut tersebut dilakukan oleh oknum Pemerintah Desa. Kepala Desa 
        Selok Awar-Awar, beserta puluhan anak buahnya menganiaya secara sadis dan brutal dua aktivis tersebut 
        hingga tewas. Aksi tersebut, dilakukan sebagai upaya untuk mengamankan bisnis tambang ilegal. Kuat 
        dugaan, banyak pihak yang mendapat untung dari tambang pasir ilegal tersebut. Tidak tanggung-tanggung, 
        oknum-oknum  pemerintah dari tingkat Desa, Kecamatan bahkan sampai pada oknum  DPRD ikut menikmati hasil 
        tambang pasir besi tersebut. Tidak hanya itu,  Kepolisian Lumajang  dan oknum TNI juga diduga menerima 
        suap untuk membekingi aktifitas penambangan ilegal di wilayah itu.</p>
        <img src="https://aws-dist.brta.in/2015-09/original_700/0_0_1000_660_17ff74b91e8d1936c6a90d058f8c6f8c957cee32.jpg" class="pic2">
        <p>Kasus penganiayaan serta pembunuhan sadis yang dialami dua aktivis lingkungan yang berjuang mempertahankan 
        tanah dan lingkungannya dari aktivitas pertambangan liar mendapat banyak kecaman. Kutukan keras juga 
        dilayangkan karena tragedi berdarah tersebut terjadi karena pihak kepolisian abai sehingga jatuhnya korban jiwa.</p>'

    ],

    'block2'=>[
        'title'=>'Menolak Tambang Nyawapun Tumbang',
        'narasi'=>'
        <img src="http://img.hello-pet.com/assets/uploads/2015/09/Salim-Kancil1.jpg" class="pic2">
        
        <p>"Tragedi berdarah di Lumajang, Jawa Timur dipicu oleh penolakan masyarakat terhadap aktivitas 
        pertambangan yang  merusak lingkungan dan lahan pertanian milik warga. Eksploitasi pasir emas di 
        pantai tersebut berdampak buruk pada lahan pertanian sehingga warga menjadi resah.</p>  
        <p>Diketahui, bahwa tambang pasir tersebut merupakan pertambangan ilegal. Selama ini pengelolaan 
        tambang dikendalikan oleh  aparat dari  desa dan kecamatan. Selain itu, banyak "orang kuat" membekingi 
        aktivitas pertambangan tersebut sehingga mampu beroperasi cukup lama. Kalangan TNI, Polisi dan Anggota 
        DPRD diduga ikut menikmati suap tambang ilegal tersebut.</p>
        <p>Beberapa kali warga melakukan protes namun upaya tersebut selalu gagal. Bahkan intensitas penggerukan 
        pasir semakin meningkat dengan skala kerusakan lingkungan  yang ditimbulkan semakin luas.</p>
        <img src="http://media.viva.co.id/thumbs2/2015/10/04/340504_foto-tosan_663_382.jpg" class="pic">
        <p>Para petani  kerap mendapat ancaman dan intimidasi dari anak buah kepala desa jika melakukan aksi 
        penolakan terhadap aktivitas penambangan pasir. Tewasnya Salim Kancil memperlihatkan betapa warga tidak 
        memiliki daya untuk menghentikan aktivitas pertambangan liar tersebut. Sebelum tewas, Salim Kancil disiksa 
        secara brutal dan keji oleh puluhan preman suruhan Kades Selok Awar-Awar."</p>'
    ],

    'block4'=>[
        'title'=>'QUOTE PEMBANTAI',
        'narasi'=>'
        <img src="http://assets2.jpnn.com/picture/normal/20150930_093908/093908_226420_salim_kades.jpg" class="pic">
         
        <p>"Itu saya lakukan untuk mengantisipasi adanya aksi demo, memang Salim cs berencana melakukan demo antitambang). 
        Saat itu dihadiri babinsa, babinkamtibmas, dan orang-orang yang terlibat pengeroyokan. Tapi, saya tak pernah 
        merencanakan untuk membunuh. Murni untuk kerja bakti.</p>(Hariyono, Kades Selok Awar-awar)'
    ],

    'block3'=>[
        'title'=>'Potensi Konflik di Tambang Pasir',
        'narasi'=>'
        <img src="http://fajar.co.id/wp-content/uploads/2015/10/Tambang-Pasir-Lumajang.jpg" class="picprofil" style="width: 100%;">
        
        <p class="rightcol">Maraknya lokasi tambang pasir yang berjejeran di sepanjang pantai selatan, khususnya di Kabupaten 
        Lumajang berpotensi memicu konflik antara warga pemilik lahan dan pengelola tambang. Khusus untuk 
        penambangan batu dan pasir, kabupaten itu menyatakan memiliki bahan galian pasir dan batu bangunan seluas 
        82,50 hektare. Areal pasir dan batu yang dieksploitasi baru 15 hektare dengan volume 239.065 m³ atau hanya 
        4% dari kapasitas yang tersedia.</p>
        <p class="rightcol">Fakta tersebut menegaskan bahwa ruang hidup dan lingkungan masyarakat semakin tergerus oleh aktivitas 
        tambang ilegal. Sehingga potensi konflik akibat adanya aktivitas tambang liar yang merusak lahan pertanian 
        milik warga akan meningkat.</p>  
        <img src="http://cdn.tmpo.co/data/2011/02/14/id_64643/64643_620.jpg" class="picprofil" style="width: 100%;">
        <p class="rightcol">Kasus di Selok Awar-Awar hanya salah satu kasus yang mencuat ke publik. Sebelumnya di  desa Wotgalih, 
        Kecamatan Yosowilangun menimbulkan konflik. Kasus serupa juga terjadi di desa Pandanarum dan Pandanwangi, 
        kecamatan Tempeh.</p>'
    ],

    'block8'=>[
        'title'=>'Otak Pembunuhan dan Peran Tim 12',
        'narasi'=>'
        <img src="http://pojokpitu.com/yp-gbr-news/15298_29-09-2015_21-05-43_.jpg" class="pic">
        <p>Desa Selok Awar-awar merupakan desa tambang pasir yang menyimpan banyak kekayaan alam. Tidak mengherankan 
        jika banyak oknum yang tergiur untuk mengoptimalkan potensi alam tersebut dengan cara membuka lahan tambang. 
        Hariyono, Kades Selok Awar-Awar salah satu pengelola tambang pasir yang ada di Kab. Lumajang.</p>   
        <p>Pria berkumis itu kemudian menjalankan bisnis ladang tambangnya dengan tangan besi.  Untuk menjaga 
        pungutan tetap langgeng, Hariyono menggunakan perdes sebagai dalih untuk mengekploitasi pasir di wilayahnya. 
        Dan ada dugaan bahwa ia melakukan penambangan pasir tanpa izin karena dekat  aparatur pemerintahan daerah Kab Lumajang, 
        bahkan oknum kepolisian dari Polres dan Polsek. Selain itu,  Ia juga menggunakan kelompok preman yang dinamai Tim 12.</p>   
        <p>Tim yang terdiri dari para preman itu ditugasi untuk menjaga portal-portal tempat truk keluar masuk mengangkut pasir 
        dari Watu Pecak. Ada tiga portal yang dijaga oleh tim 12. Dua di antaranya berada di luar Desa Selok Awar-awar. 
        Tarif yang dikenakan pun naik dari Rp 15 ribu menjadi Rp 35 ribu. Apabila dalam sehari saja ada 300 truk yang 
        melintasi portal, setidaknya Hariyono bisa mengeruk Rp 10 juta. Tidak sembarang orang bisa melewati portal yang dijaga 
        ketat tersebut, bahkan petugas Perum Perhutani.</p>
        <img src="http://www.jpnn.com/picture/normal/20151002_073546/073546_861616_Pembunuh_Tim_12_Gun_JP.jpg" class="pic2">

        <p>Usaha tambang pasir semakin lancar dengan keberadaan Desir sebagai pemimpin tim 12. Desir merupakan ketua LSM 
        di Desa tersebut. Dia punya wewenang kerjasama dengan Perhutani untuk mengelola hutan. Tim 12 pimpinan Desir inilah yang 
        kerap meneror dan mengintimidasi warga yang berani menolak aktivitas pertambangan di wilayah tersebut. Atas intruksi 
        Sang Kades, Tim 12 tersebut membunuh Salim kancil di depan banyak orang dan menganiaya Tosan sampai kritis.</p>'
    ],

    'block9'=>[
        'title'=>'Potensi Alam Terbaik se Indonesia',
        'narasi'=>'
          <img src="http://energitoday.com/uploads//2013/02/GUNUNG-SEMERU.jpg" class="pic2">
          
          <p>Daerah Lumajang, terkenal sebagai penghasil pasir terbaik di Indonesia yang disebabkan 
          pula keberadaan Gunung Semeru yang terletak di Kabupaten Lumajang. Adanya gunung tertinggi 
          di Pulau Jawa  itu telah membawa berkah dengan berlimpahnya bahan galian golongan C khususnya 
          jenis pasir, batu, coral dan sirtu yang tak pernah habis dan berhenti mengalir.</p>
          <p>Potensi bahan Galian golongan C jumlahnya akan bertambah terus sesuai dengan kegiatan rutin 
          Gunung Semeru yang mengeluarkan material kurang lebih 1 (satu) juta M3/tahun. Bukan saja 
          kuantitasnya yang sangat besar, namun kualitasnya juga sangat baik dan terbaik di Jawa Timur. 
          Berbagai penelitian menyimpulkan, unggulnya kualitas pasir Gunung Semeru karena kandungan tanah 
          (lumpur) sedikit, butiran pasirnya standar serta warna dan daya rekatnya yang baik.</p>
          <p>Lokasi  penambangan pasir dan batu cukup banyak, di antaranya di sepanjang Sungai/Kali 
          Rejali, Kali Regoyo, dan Kali Glidig. Tepatnya berada di Kecamatan Candipuro, Pasirian, Tempursari 
          dan Pronojiwo. Areal bahan tambang/galian pasir dan batu bangunan 82,50 ha dengan volume 5.976.625 m. 
          Areal pasir dan batu yang di eksploitasi baru 15 ha dengan volume 239.065 m atau hanya 4% dari 
          kapasitas yang tersedia.</p>
          <img src="http://lumajangsatu.com/foto_berita/60pasir-lumajang.jpg" class="pic">
          <p>Khusus mengenai areal tambang pasir besi yang dimiliki Lumajang mencapai 2.650 ha. Lokasinya 
          memanjang dalam satu deret di sepanjang pantai selatan. Tepatnya, di pantai selatan Kecamatan 
          Yosowilangun, Kecamatan Kunir, Kecamatan Tempeh dan Kecamatan Pasirian. Di samping pasir besi 
          Kabupaten Lumajang tersedia potensi tambang emas. Setidaknya ada 2 Desa yang dinyatakan memiliki 
          kandungan emas. Yaitu Desa Bulurejo, dan Desa Oro-Oro Ombo.  Keduanya berada di Kecamatan Tempursari, 
          60 Km dari jantung kota Lumajang.</p>
          <p>Potensi mineral pasir besi di wilayah itu sangat besar, Hasil survey geologi, potensi pasir 
          besi mencapai luasan sekitar 12.500 hektare.</p>'
    ],

    'block10'=>[
        'title'=>'Aliran Dana Gratifikasi Tambang ',
        'narasi'=>'
          <img src="http://bharatanews.com/foto_berita/46UANG.jpg" class="pic2">
          <p>Hasil tambang pasir disinyalir mengalir kebeberapa aparat pemerintah dan oknum polisi serta TNI.</p>   
          <p>1.&nbsp;Kepala Bagian Ekonomi Pemkab Lumajang, Ir. Ninis Ridyawati ikut diperiksa atas dugaan 
          penerimaan gratifikasi tambang illegal</p>
          <p>2.&nbsp;Anggota DPRD Lumajang, Sugianto dikabarkan menerima dana senilai Rp. 4 Juta</p> 
          <p>3.&nbsp;Kapolsek Pasirian, AKP Sudarminto dikabarkan menerima dana senilai Rp. 6 Juta</p> 
          <p>4.&nbsp;Kanit Reskrim Pasirian, Ipda Samsul Hadi dikabarkan menerima dana senilai Rp. 1,5 Juta</p>
          <p>5.&nbsp;Babinkamtibnas, Aipda Sigit Purnomo dikabarkan menerima dana senilai Rp. 3 Juta</p>
          <p>6.&nbsp;Danramil juga dikabarkan ikut menerima gratifikasi senilai Rp. 1 Juta</p>
          <p>7.&nbsp;Babinsa Selok Awar-awar menerima dana senilai Rp. 500 Ribu</p>
          <p>8.&nbsp;Camat Pasirian menerima dana rutin sebesar Rp. 1 Juta/Bulan</p>
          <p>9.&nbsp;Dua Pejabat Perhutani yang dikabarkan juga menerima dana rutin sebesar Rp. 500 Ribu/Bulan</p>
          <p>10.&nbsp;Oknum Wartawan dan LSM diantaranya Beni, Basori, Karminto, Wanto dan Karyaman yang dikabarkan 
          juga ikut menerima gratifikasi dari keuntungan yang diperoleh melalui aktifitas tambang illegal.</p>'
    ],

    'block11'=>[
        'title'=>'Bupati Terindikasi Turut Andil',
        'narasi'=>'
                  
            <p>Dinas Energi dan Sumber Daya Mineral (ESDM) Jawa Timur, mengendus izin tak wajar yang diduga dilakukan 
            pejabat Lumajang. Bahkan hasil penyelidikan Dinas ESDM mengerucut, bahwa Bupati Lumajang diduga memberi 
            izin kepada kepala Desa Selok Awar-Awar, Hariono melakukan aktivitas penambangan di area milik PT 
            Indo Modern Maining Sejahtera (IMMS).</p>
            <img src="http://lumajangsatu.com/foto_berita/2wabup-bupati%281%29.jpg" class="pic">

            <p>ESDM Jawa Timur menjelaskan, perusahaan tambang ini memiliki izin operasi yang dikeluarkan oleh Bupati 
            Lumajang Asa&#39;at Malik sejak 2012 hingga 2022. Namun sejak Januari 2014, sudah tidak beroperasi lagi 
            lantaran ada larangan ekspor pasir besi dalam bentuk mentah.</p>
            
            <p>Dari sini kemudian sebagian kelompok warga melakukan penambangan di lahan PT IMMS, tanpa sepengetahuan 
            perusahaan. Aktivitas ilegal ini berjalan sekian lama lantaran warga merasa mendapat perlindungan dari 
            pejabat setempat. Dalam tata ruang izin pertambangan, alur perizinan sebenarnya memang cukup sampai kabupaten, 
            dalam hal ini bupati atau wali kota. Tapi sejak Tahun 2015, ada undang-undang yang mengharuskan perizinan 
            aktivitas penambangan harus sampai ke tingkat provinsi.</p>
            <img src="http://jabar.pojoksatu.id/wp-content/uploads/2015/04/Tambang-Pasir.png" class="pic2">
            <p>Bagi perusahaan yang sudah memiliki kontrak sebelum 2015, maka menggunakan aturan izin di tingkat kabupaten 
            atau kota sampai masa akhir kontrak. Sementara itu sejak Januari 2015, PT IMMS yang mengajukan izin belum diproses. 
            Seluruh izin di Lumajang masih mengikuti kabupaten, ada 60 perizinan yang dikeluarkan bupati setempat.</p>'
    ],


    // 'institusiPendukung' => [
    //     ['link'=>'','image'=>'','title'=>''],
    // ],

    'tersangka' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/haryono561d263041a43','image'=>'http://assets2.jpnn.com/picture/normal/20150930_093908/093908_226420_salim_kades.jpg','title'=>'Kepala Desa Selok Awar-Awar'],
        ['link'=>'#','image'=>'http://cdn.img.print.kompas.com/getattachment/662172c1-62d1-42fe-81e8-64b332292ad2/249816?maxsidesize=315','title'=>'Oknum Polisi Polres Lumajang'],
        ['link'=>'#','image'=>'http://www.jpnn.com/picture/normal/20151002_073546/073546_861616_Pembunuh_Tim_12_Gun_JP.jpg','title'=>'Tim 12 Hariyono'],
    ],

    'institusiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/wahanalingkunganhidupindonesiawlhi5370cfc3cd6a3','image'=>'http://jowonews.com/wp-content/uploads/2014/11/201401044400.jpg','title'=>'Walhi'],
        ['link'=>'http://www.bijaks.net/aktor/profile/komisiiiidprri54d30f40415f0','image'=>'http://terkininews.com/files/DPR_RI_371677936.png&size=article_large','title'=>'Komisi III DPR'],
        ['link'=>'http://www.bijaks.net/aktor/profile/kompolnas530b08ed8a7cd','image'=>'http://cdn-2.tstatic.net/wartakota/foto/bank/images/20140603kompolnas-wajar-ada-kombes-kawal-capres.jpg','title'=>'Kompolnas'],
        ['link'=>'http://change.org','image'=>'http://www.nptechforgood.com/wp-content/uploads/2009/09/change.org_logo.png','title'=>'Change.org'],
        ['link'=>'#','image'=>'https://pbs.twimg.com/profile_images/1245332931/LOGO_GARUDA_400x400.jpg','title'=>'Komnas HAM'],
        ['link'=>'#','image'=>'http://www.jatam.org/wp-content/uploads/2015/02/logo-jatam01.jpg','title'=>'Jaringan Advokasi Tambang'],

    ],

    // 'partaiPendukung' => [
    //     ['link'=>'http://www.bijaks.net/aktor/profile/partaiamanatnasional5119b55ab5fab','image'=>'http://www.pemiluindonesia.com/wp-content/uploads/2013/04/PAN.jpg','title'=>'PAN'],
    // ],

    'partaiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/partaidemokrat5119a5b44c7e4','image'=>'http://4.bp.blogspot.com/-7VkISlK9gQE/Tu6xnxV646I/AAAAAAAAAQY/2pPGaGPwpo8/s1600/demokrat-2512.gif','title'=>'Demokrat'],
        ['link'=>'http://www.bijaks.net/politik','image'=>'http://dprd-tegalkab.go.id/wp-content/uploads/2015/04/logo_master.jpg','title'=>'PKB'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaidemokrasiindonesiaperjuangan5119ac6bba0dd','image'=>'http://1.bp.blogspot.com/-iHAUrzTJ82g/Uw79n0kJn-I/AAAAAAAAAlE/quCk5_1OEOY/s1600/Partai+Demokrasi+Indonesia+Perjuangan+%2528Logo%2529.jpeg','title'=>'PDI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/nasdem5119b72a0ea62','image'=>'http://www.kindo.hk/blog/images/2014-05/logo-partai-nasdem.jpg','title'=>'Nasdem'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaihatinuranirakyathanura5119a1cb0fdc1','image'=>'https://upload.wikimedia.org/wikipedia/id/c/ce/HANURA.jpg','title'=>'Hanura'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaipersatuanpembangunan5189ad769b227','image'=>'http://www.medanbagus.com/images/berita/normal/341256_10291211042013_PPP.jpg','title'=>'PPP'],
        ['link'=>'http://www.bijaks.net/aktor/profile/partaigerakanindonesiarayagerindra5119a4028d14c','image'=>'https://pbs.twimg.com/profile_images/1936137252/GERINDRA1.jpg','title'=>'Gerindra'],
    ],

    // 'quotePendukung'=>[
    //   ['from'=>'Ir. H. Jokowi','jabatan'=>'Presiden RI','img'=>'http://rmol.co/images/berita/normal/471192_04185709072015_jokowi_smile_1','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'"TNI juga bergerak, pemerintah daerah juga harus bergerak. Semua bergerak untuk memadamkan api dan membebaskan asap dengan target operasi yang jelas,"'],
    // ],

    'quotePenentang'=>[
        ['from'=>'Zulkifli Hasan','jabatan'=>'Ketua MPR RI','img'=>'http://img.lensaindonesia.com/uploads/1/2015/08/30540-main-zulkifli-hasan-ant-ketum-pan-ingatkan-jokowi-indonesia-di-ambang-krisis.gif','url'=>'http://www.bijaks.net/aktor/profile/zulkiflihasan50ef907d0661c','content'=>'"Memang biadab, usut tuntas dan tegakkan aturan agar ada yang terjerat jangan hilang begitu saja, apalagi kan ini pembela tambang, ada yang menambang tetapi merusak lingkungan"'],
        ['from'=>'Marwan Djafar','jabatan'=>'Menteri Desa dan Daerah Tertinggal','img'=>'http://www.rmol.co/images/berita/normal/847705_07334801092015_Marwan_Jafar.jpg','url'=>'http://www.bijaks.net/aktor/profile/hmarwanjafarsesh50f8f52b84acd','content'=>'"Jangan sampai ada pengelolaan SDA seperti pertambangan yang hanya menguntungkan kepala desa saja,"'],
        ['from'=>'Saifullah Yusuf','jabatan'=>'Wakil Gubernur Jawa Timur','img'=>'http://korannusantara.com/wp-content/uploads/2012/11/Saifullah-Yusuf.jpg','url'=>'http://www.bijaks.net/aktor/profile/drshsaifullahyusuf51402dc53a94e','content'=>'"Saya minta penyelidikannya dilakukan secara terbuka dan tidak ada yang kebal hukum,"'],
        ['from'=>'Akbar Faisal','jabatan'=>'Politisi Partai NasDem','img'=>'http://fajar.co.id/wp-content/uploads/2015/02/Akbar-Faisal1.jpg','url'=>'http://www.bijaks.net/aktor/profile/drsakbarfaizalmsi50f778b33337e','content'=>'"Kepada bupatinya, (Komisi III DPR) bertanya soal pembiaran. Kenapa lahan ini notabenenya sudah dianggap penambangan liar, tapi kok dibiarkan (beroperasi)"'],
        ['from'=>'Risa Mariska','jabatan'=>'Anggota Komisi III','img'=>'http://risamariska.com/wp-content/uploads/2014/02/risa-kpu-02-risamariska.com_.jpg','url'=>'http://www.bijaks.net/aktor/profile/risamariska526dcab9026d2','content'=>'"Saya sudah meminta Divkum, Propam dan Provost, turun menyelidiki kasus tambang pasir illegal,"'],
        ['from'=>'Benny Kabur Harman','jabatan'=>'Wakil Ketua Komisi III DPR','img'=>'http://sp.beritasatu.com/media/images/original/20141029174602970.jpg','url'=>'http://www.bijaks.net/aktor/profile/drbennykaburharmansh50f8dd6613925','content'=>'"Pembiaran ini terjadi karena diduga terdapat beberapa pihak yang terlibat dalam kasus tersebut"'],
        ['from'=>'Badrodin Haiti','jabatan'=>'Kapolri','img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150218_015120_harianterbit_badrodin.jpg','url'=>'http://www.bijaks.net/aktor/profile/badrodinhaiti531d430cb8e05','content'=>'"Apakah informasi yang selama ini berkembang negatif itu betul atau tidak, ada pembiaran, lambat dalam penanganan, itu nanti akan dijawab dari hasil penyelidikan Propam"'],
        ['from'=>'Ony Mahardika','jabatan'=>'Direktur WALHI JATIM','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2015--10--73182-ony-walhi-desak-pemprov-tinjau-rtrw-penataan-galian-tambang-jawa-timur.jpg','url'=>'http://www.bijaks.net/aktor/profile/onymahardika52cf55f1640c3','content'=>'"Jika ditangani sedari awal pasti tidak ada korban, soalnya penambangan pasir liar di Desa Selok Awar-awar, Kecamatan Pasirian, Kabupaten Lumajang sudah terjadi sejak Januari 2015 silam"'],
        ['from'=>'Natalius Pigay','jabatan'=>'Komisioner Komnas HAM','img'=>'http://rmolsumsel.com/images/berita/normal/485608_02143625102014_Natalius_Pigai.jpg','url'=>'http://www.bijaks.net/aktor/profile/nataliuspigai51a5607bce402','content'=>'"Jadi, pihak kepolisian bisa mengembangkan, apakah ada keterlibatan aparat pemerintah kabupaten atau ada keterlibatan korporasi"'],
        ['from'=>'Arsul Sani','jabatan'=>'Anggota Komisi III DPR RI','img'=>'http://www.rmol.co/images/berita/thumb/thumb_652735_04355920072015_arsul_sani.JPG','url'=>'http://www.bijaks.net/aktor/profile/arsulsani52c4f8ac84d4b','content'=>'"Kami minta Polda menelusurinya sesuai dengan penyidikan dalam kasus TPPU (Tindak Pidana Pencucian Uang) karena pembiaran yang terjadi itu menimbulkan dugaan adanya aliran uang"'],
        ['from'=>'Yuni Satia Rahayu','jabatan'=>'Mantan Wakil Bupati Sleman dan Politisi PDIP','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQH250jEwZYUex_BpmbZf4y8ZI74yQFMSvsAxZfCeGo6Y-MI4z2','url'=>'http://www.bijaks.net/aktor/profile/yunisatiarahayu561d34aed7054','content'=>'"Kalau boleh kritis, negara harus hadir agar masyarakat jangan saling melukai. Kasus Salim Kancil jangan sampai terjadi di Sleman,"'],
    ],

    'video'=>[
        ['id'=>'Ajm7JyCuD_Y'],
        ['id'=>'-4kQd0hpGlk'],
        ['id'=>'zz7dxrjxeM4'],
        ['id'=>'M699qB9g9zI'],
        ['id'=>'GVbKlYzRvZY'],
        ['id'=>'uaPTeTk8KuU'],
        ['id'=>'PSuZDa5-zqo'],
        //['id'=>'bAmci0OlmRY'],
        ['id'=>'_9MZZ7has5Y'],
        ['id'=>'VoJ9qI_ZwVE'],
        ['id'=>'ZxKnY2J0tPI'],
        ['id'=>'9zrQtuenWX8'],
        ['id'=>'6wGA-N00hRs'],
        ['id'=>'g-uXRH35FUk'],
        ['id'=>'Ti_7VWsmgVY'],
        ['id'=>'jly6Q7MtosU'],
        ['id'=>'IuTMSSvzI3U'],
        ['id'=>'BzlvJxCB96o'],
        ['id'=>'USiCd4bf1dY'],
        ['id'=>'mlURHlyx0qs'],
        ['id'=>'SDJsC5OFY8c'],
        ['id'=>'s0OaFSnIlEM'],
    ],

    'foto'=>[
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/10/12/607894/670x335/duit-tambang-berdarah-lumajang-dimakan-aparat-dan-pejabat.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2011/07/03/id_82329/82329_620.jpg'],
        ['img'=>'http://media.galamedianews.com/news/151002211732-tragedi-tambang-berdarah-hamida-bupati-lumajang-harus-diperiksa.jpg'],
        ['img'=>'http://www.topik9.com/wp-content/uploads/2015/10/penambang-pasir.jpg'],
        ['img'=>'http://images1.rri.co.id/thumbs/berita_203683_800x600_korban_new.jpg'],
        ['img'=>'http://www.suarasurabaya.net/_watermark/createimage_small.php?d=kk&c=berita&b=201510&a=160099&angka=10'],
        ['img'=>'http://www.jemberpost.com/wp-content/uploads/2015/09/339229_aksi-solidaritas-terhadap-salim-kancil_663_382.jpg'],
        ['img'=>'http://www.dpp.pkb.or.id/unlocked/styles/slide-show-dpp-pkb-headline/public/unlocked/940dab37768af683ee55b3578b86d1aa960675b681ab89972ac5bba7865b6bd6271c99a6abd3f0cb8ef8c9463dd4c409beritaplusfoto/Tambang%20Pasir%20Lumajang.jpg?itok=rpvsdsjR'],
        ['img'=>'http://www.suarasurabaya.net/_watermark/createimage_small.php?d=kk&c=berita&b=201510&a=160453&angka=10'],
        ['img'=>'http://www.suarasurabaya.net/_watermark/createimage_medium.php?d=kk&c=berita&b=201510&a=160160&angka=2'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/10/02/9b1bc7c9-1663-4018-84d0-5228d6eb304d_169.jpg?w=780&q=90'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/10/05/997dc2c7-d255-49cb-b8a3-07e8ce5c1955_169.jpg?w=300&q='],
        ['img'=>'http://www.suarasurabaya.net/_watermark/createimage_small.php?d=&c=potret&b=201510&a=16067&angka=3'],
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/09/30/602287/670x335/beri-izin-tambang-akankah-bupati-lumajang-terseret-kasus-salim.jpg'],
        ['img'=>'http://lumajangsatu.com/foto_berita/20walhi-jatim-selok-awar-awar.jpg'],
        ['img'=>'http://lacak.id/wp-content/uploads/2015/10/7a9e025b-30ef-41cb-afab-0831c9191095_169.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2012/05/29/id_122319/122319_620.jpg'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/09/27/3d3bf59b-177d-4106-9bdf-5d979b1b84f2_169.jpg?w=780&q=90'],
        ['img'=>'http://www.jurnal3.com/wp-content/uploads/2015/10/lumajang-kasus-600x370.jpg'],
        ['img'=>'https://ytimg.googleusercontent.com/vi/79NzPnMIrvM/mqdefault.jpg'],
        ['img'=>'http://surabayapost.net/foto_berita/12tambang.jpg'],
        ['img'=>'http://ytimg.googleusercontent.com/vi/9MeaBCxNEkk/mqdefault.jpg'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/09/27/ce36f5a8-41e7-4a0c-b4ce-d86ebb58fc01_169.jpg?w=780&q=90'],
        ['img'=>'http://halloapakabar.com/wp-content/uploads/2015/10/kasus-salim-kancil.jpg'],
        ['img'=>'https://images.detik.com/community/media/visual/2015/09/30/c52c3098-3eeb-4238-99d9-e29cf7a62c19_169.jpg?w=620&mark=undefined&image_body_visual_id=143137'],
        ['img'=>'http://lumajangsatu.com/foto_berita/62Bupati-Lumajang-Jengguk-Tosan.jpg'],
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/10/13/608369/670x335/komnas-ham-sebut-izin-tambang-ilegal-modus-cari-modal-menang-pilkada.jpg'],
        ['img'=>'https://singindo.files.wordpress.com/2015/10/wpid-screenshot_2015-10-06-20-37-03.png'],
        ['img'=>'http://berita.beritajatim.com/brt604406527.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/09/28/339303_demo-usut-pembunuhan-petani-lumajang_663_498.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/1017785/big/097903900_1444632865-20151012-_Kasus_Salim_Kancil-Sidang_Etik.jpg'],
        ['img'=>'http://geotimes.co.id/wp-content/uploads/2015/10/Aksi-Dukungan-Salim-Kancil-300915-Prad-1.jpg'],

    ],

    'berita'=>[
        ['img'=>'','shortText'=>'Peristiwa_berdarah_di_kabupaten_lumajang','link'=>'http://www.bijaks.net/scandal/index/18624-peristiwa_berdarah_di_kabupaten_lumajang'],
        ['img'=>'','shortText'=>'usulkan-salim-kancil-dapat-penghargaan','link'=>'http://www.bijaks.net/news/article/0-218175/komisi-iii-bakal-usulkan-salim-kancil-dapat-penghargaan'],
        ['img'=>'','shortText'=>'kapolri-klaim-pilih-orang-orang-terbaik','link'=>'http://www.bijaks.net/news/article/0-218086/kapolri-klaim-pilih-orang-orang-terbaik-selesaikan-kasus-salim-kancil'],
        ['img'=>'','shortText'=>'pembunuhan-salim-kancil-komnas-ham-minta-oknum','link'=>'http://www.bijaks.net/news/article/0-216407/pembunuhan-salim-kancil-komnas-ham-minta-oknum-pemkab-lumajang-dan-korporasi-diusut'],
        ['img'=>'','shortText'=>'saksikan-pembunuhan-salim-kancil-siswa-tk','link'=>'http://www.bijaks.net/news/article/0-216205/saksikan-pembunuhan-salim-kancil-siswa-tk-selok-awar-awar-diberikan-trauma-healling/'],
        ['img'=>'','shortText'=>'bukti-tata-kelola-pertambangan-buruk','link'=>'http://www.bijaks.net/news/article/0-215776/marwan-jafar-kasus-pembunuhan-salim-kancil-bukti-tata-kelola-pertambangan-buruk'],
        ['img'=>'','shortText'=>'pemda-dan-polisi-ugal-ugalan','link'=>'http://www.bijaks.net/news/article/0-215754/terungkap-pemda-dan-polisi-ugal-ugalan-tangani-pembunuhan-salim-kancil'],
        ['img'=>'','shortText'=>'jadi-otak-pembantaian-salim-kancil','link'=>'http://www.bijaks.net/news/article/0-215068/jadi-otak-pembantaian-salim-kancil-kades-terancam-20-tahun-penjara'],
        ['img'=>'','shortText'=>'pemerintah-didesak-lakukan-reformasi-agraria','link'=>'http://www.bijaks.net/news/article/0-215023/salim-kancil-tewas-pemerintah-didesak-lakukan-reformasi-agraria'],
        ['img'=>'','shortText'=>'tak-hanya-untungkan-kades','link'=>'http://www.bijaks.net/news/article/0-214858/terkait-salim-kancil-marwan-jafar-minta-tata-kelola-sda-tak-hanya-untungkan-kades'],
        ['img'=>'','shortText'=>'adanya-dugaan-pencucian-uang','link'=>'http://www.bijaks.net/news/article/0-214663/kasus-salim-kancil-dpr-cium-adanya-dugaan-pencucian-uang'],
        ['img'=>'','shortText'=>'salim-kancil-jadi-korban-praktik-politik','link'=>'http://www.bijaks.net/news/article/0-214634/komisi-iii-nilai-salim-kancil-jadi-korban-praktik-politik-dan-hukum-yang-korup'],
        ['img'=>'','shortText'=>'tak-cukup-diselesaikan-secara-hukum','link'=>'http://www.bijaks.net/news/article/0-214151/menteri-agraria-sebut-pembunuhan-salim-kancil-tak-cukup-diselesaikan-secara-hukum'],
        ['img'=>'','shortText'=>'begini-gurihnya-bisnis-tambang','link'=>'http://www.bijaks.net/news/article/0-214064/begini-gurihnya-bisnis-tambang-yang-renggut-nyawa-salim-kancil'],
        ['img'=>'','shortText'=>'kepala-bpn-hentikan-usaha-pertambangan','link'=>'http://www.bijaks.net/news/article/0-214055/kasus-salim-kancil-kepala-bpn-hentikan-usaha-pertambangan'],
        ['img'=>'','shortText'=>'minta-diisikan-ilmu-kebal','link'=>'http://www.bijaks.net/news/article/0-213805/masinton-para-pembunuh-salim-kancil-minta-diisikan-ilmu-kebal'],
        ['img'=>'','shortText'=>'unggah-foto-tersangka-pembunuhan','link'=>'http://www.bijaks.net/news/article/0-213660/unggah-foto-tersangka-pembunuhan-salim-kancil-soekarwo-ini-bukti-keseriusan-saya'],
        ['img'=>'','shortText'=>'polda-jatim-larang-keluarga','link'=>'http://www.bijaks.net/news/article/0-213604/polda-jatim-larang-keluarga-temui-tersangka-pembunuh-salim-kancil'],
        ['img'=>'','shortText'=>'pcnu-lumajang-desak','link'=>'http://www.bijaks.net/news/article/0-213326/pcnu-lumajang-desak-aparat-objektif-usut-kasus-salim-kancil'],
        ['img'=>'','shortText'=>'keluarga-tersangka-salim-kancil-dilarang-membesuk','link'=>'http://www.bijaks.net/news/article/0-213191/keluarga-tersangka-salim-kancil-dilarang-membesuk'],
        ['img'=>'','shortText'=>'lebih-kejam-dari-pki','link'=>'http://www.bijaks.net/news/article/0-212648/politikus-pan-pembunuhan-salim-kancil-lebih-kejam-dari-pki'],
        ['img'=>'','shortText'=>'usut-kasus-pembunuhan-salim-kancil','link'=>'http://www.bijaks.net/news/article/0-212176/komisi-iii-bentuk-tim-usut-kasus-pembunuhan-salim-kancil'],
        ['img'=>'','shortText'=>'petani-bukan-anak-tiri','link'=>'http://www.bijaks.net/news/article/0-212008/soal-salim-kancil-gerindra-petani-bukan-anak-tiri-yang-diperlakukan-tidak-adil-dan-minim-perhatian'],
        ['img'=>'','shortText'=>'eksistensi-penegakan-ham','link'=>'http://www.bijaks.net/news/article/0-211875/soal-salim-kancil-eksistensi-penegakan-ham-di-indonesia-dipertanyakan'],
        ['img'=>'','shortText'=>'ini-adalah-persoalan-nasional','link'=>'http://www.bijaks.net/news/article/0-211782/pembunuhan-salim-kancil-aktivis-98-ini-adalah-persoalan-nasional'],
        ['img'=>'','shortText'=>'tersembunyi-neoliberalisme-ilegal','link'=>'http://www.bijaks.net/news/article/0-211766/dibalik-kasus-pembunuhan-salim-kancil-tersembunyi-neoliberalisme-ilegal'],
        ['img'=>'','shortText'=>'dia-hanya-buruh-tani','link'=>'http://www.bijaks.net/news/article/0-211739/istri-salim-kancil-dia-hanya-buruh-tani'],
        ['img'=>'','shortText'=>'salim-kancil-dibunuh-kepala-desa','link'=>'http://www.bijaks.net/news/article/0-211623/salim-kancil-dibunuh-kepala-desa-jadi-tersangka'],
        ['img'=>'','shortText'=>'tangkap-otak-pembunuhan-salim-kancil','link'=>'http://www.bijaks.net/news/article/0-211602/ketua-mpr-tangkap-otak-pembunuhan-salim-kancil'],
        ['img'=>'','shortText'=>'pembunuh-salim-kancil-tertawa','link'=>'http://www.bijaks.net/news/article/0-211551/sadis-pembunuh-salim-kancil-tertawa-dan-bercanda-saat-ditahan'],


    ],

    'kronologi'=>[
        'list'=>[
            ['date'=>'Januari 2015','content'=>'Masyarakat Desa Selok Awar-Awar melakukan aksi penolakan tambang pasir berupa 
            penyampaian pernyataan sikap dari Forum Komunikasi Masyarakat Peduli Desa Selok Awar-Awar, Lumajang di mana Tosan 
            dan Salim Kancil menjadi bagian dan anggota dari Forum'],
            ['date'=>'Juni 2015','content'=>'Forum warga menyurati Bupati Lumajang untuk meminta audiensi tentang penolakan tambang pasir. Surat tersebut tidak direspons oleh Bupati Lumajang'],
            ['date'=>'9 September 2015','content'=>'Forum warga melakukan aksi damai penghentian aktivitas penambangan pasir dan truk muatan pasir di Balai Desa Selok Awar-Awar'],
            ['date'=>'10 September 2015','content'=>'Muncul ancaman pembunuhan yang diduga dilakukan oleh sekelompok preman yang dibentuk oleh Kepala Desa Selok Awar-Awar kepada Tosan. Kelompok preman tersebut diketuai oleh Desir'],
            ['date'=>'11 September 2015','content'=>'Forum melaporkan tindak pidana pengancaman ke Polres Lumajang yang diterima langsung oleh Kasat Reskrim Lumajang, Heri. Saat itu Kasat menjamin akan merespons pengaduan tersebut'],
            ['date'=>'19 September 2015','content'=>'Forum menerima surat pemberitahuan dari Polres Lumajang terkait nama-nama penyidik Polres yang menangani kasus pengancaman tersebut'],
            ['date'=>'21 September 2015','content'=>'Forum mengirim surat pengaduan terkait penambangan ilegal yang dilakukan oleh oknum aparat Desa Selok Awar-Awar di daerah hutan lindung Perhutani'],
            ['date'=>'25 September 2015','content'=>'Forum mengadakan koordinasi dan konsolidasi dengan masyarakat luas tentang rencana aksi penolakan tambang pasir dikarenakan aktivitas penambangan tetap berlangsung. Aksi ini rencananya digelar 26 September 2015 pukul 07.30 WIB.'],
            ['date'=>'26 September 2015','content'=>'Sekitar pukul 08.00 WIB, terjadi penjemputan paksa dan penganiayaan terhadap dua orang anggota forum yaitu Tosan dan Salim Kancil'],
            ['date'=>'26 September 2015','content'=>'Masih dihari yang sama, Salim Kancil ditemukan tewas dengan kondisi yang sangat memprihatinkan. Menurut beberapa saksi, Salim Kancil disiksa secara keji dan sadis sampai ia meregang nyawa. Sementara Tosan mengalami luka yang cukup serius dan dibawa ke Rumah Sakit'],
        ]
    ],

]


?>

<style>
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/tambang/salim-kancil.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/tambang/salim-kancil.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['block1']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block2']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block2']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block8']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block8']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block9']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block9']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block10']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block10']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block11']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block11']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>


        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block4']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block4']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block3']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block3']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>

       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['foto'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['video'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['berita'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>