<?php 
$data = [
    'NARASI'=>[
        'title'=>'Hukuman Mati Rezim Jokowi',
        'narasi'=>'Kehadiran pemerintahan baru di bawah Joko Widodo tidak mengubah banyak praktik hukuman mati di Indonesia. Di awal kepemimpinannya, Jokowi bahkan telah mengeksekusi 6 terpidana mati kasus narkoba di Nusa Kambangan dan Boyolali. </p><p style="text-align: justify;">Banyak kalangan menilai--terutama dari kalangan aktivis HAM—Jokowi tetap menerapkan hukum yang tidak manusiawi. Padahal ada alternatif lain yang lebih berat selain hukuman mati. Seperti hukuman 300 tahun.
                   </p><p style="text-align: justify;">Namun tak sedikit masyarakat yang juga ikut mendukung kebijakan hukuman mati Jokowi tersebut, mengingat narkoba telah meracuni anak bangsa Indonesia. Presiden Jokowi sendiri menyatakan Indonesia kini menghadapi “darurat narkoba.”  Sekalipun banyak menuai protes, Jokowi akan tetap menjalankan hukuman mati bagi pelaku kejahatan luar biasa, termasuk bandar dan pengedar narkoba serta pelaku tindak terorisme.'
    ],
    'PASAL_MEMBUNUH'=>[
        'narasi'=>'Presiden Joko Widodo selalu mengatakan kepada publik, bahwa bukan dirinya yang memerintahkan hukuman mati, tetapi hukum Indonesia. Hukuman mati di Indonesia merujuk kepada',
        'isi'=>[
            ['no'=>'Kitab Undang-Undang Hukum Pidana (KUHP), ketentuan Pasal 10 tegas menyatakan bahwa hukuman mati merupakan salah satu dari hukuman pokok.
                    Sementara pelaksanaan eksekusi hukuman mati diatur dalam Undang-Undang No.2/PNPS/1964 tentang Tata Cara Pelaksanaan Pidana Mati yang Dijatuhkan oleh Pengadilan di Lingkungan Peradilan Umum dan Militer dan tata pelaksanaannya diatur dalam Peraturan Kapolri No.12 Tahun 2010 tentang Tata Cara Pelaksanaan Pidana Mati.'],
            ['no'=>'Indonesia telah terikat dengan konvensi internasional narkotika dan psikotropika yang telah diratifikasi menjadi hukum nasional dalam UU Nomor 22 Tahun 1997 tentang Narkotika.'],
            ['no'=>'Putusan Mahkamah Konstitusi (MK) tertanggal 30 Oktober 2007 yang menolak uji materi hukuman mati dalam UU Narkotika. MK menegaskan bahwa hukuman mati dalam UU Narkotika tidak bertentangan dengan hak hidup yang dijamin UUD 1945 lantaran jaminan hak asasi manusia dalam UUD 1945 tidak menganut asas kemutlakan.']
        ]
    ],
    'PENDUKUNG'=>[
        'partaiPolitik'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaidemokrat5119a5b44c7e4'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab'],
            ['page_id'=>'partaikeadialndnpersatuanindonesia5119bd7483d2a'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4']
        ],
        'institusi'=>[
            ['name'=>'Mahkamah Konstitusi','img'=>'http://www.bijaks.net/public/upload/image/politisi/mahkamahkonstitusi51cf91379d144/badge/ac88aaa62f71902ffbcb42cdeb7a046d7b461320.jpg','url'=>'http://www.bijaks.net/aktor/profile/mahkamahkonstitusi51cf91379d144'],
            ['name'=>'Jaksa Agung','img'=>'http://www.bijaks.net/public/upload/image/politisi/kejaksaanagung530d6083297e1/badge/abda99f8e850c0bca085b2a6fafc5a76831e9c2d.jpg','url'=>'http://www.bijaks.net/aktor/profile/kejaksaanagung530d6083297e1'],
            ['name'=>'Polisi Republik Indonesia','img'=>'http://www.bijaks.net/public/upload/image/politisi/kepolisiannegararipolri536756e743fd0/badge/33009cc90cd7ab2779e8f44446da80fce94e496f.png','url'=>'http://www.bijaks.net/aktor/profile/kepolisiannegararipolri536756e743fd0'],
            ['name'=>'Badan Narkotika Nasional','img'=>'http://www.bijaks.net/public/upload/image/politisi/badannarkotikanasionalbnn5192fb6e2ffa5/badge/badannarkotikanasionalbnn5192fb6e2ffa5_20130515_031610.jpg','url'=>'http://www.bijaks.net/aktor/profile/badannarkotikanasionalbnn5192fb6e2ffa5'],
            ['name'=>'Mayoritas DPR RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/dewanperwakilanrakyatdpr51da66b0a7ac4/badge/be53b09d1120ec01cb37eb34c0417babe9b93f37.jpg','url'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdpr51da66b0a7ac4'],
            ['name'=>'KEMENKO POLHUKAM','img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/e/e2/Logo_Kemenkopolhukam.png/100px-Logo_Kemenkopolhukam.png','url'=>'http://id.wikipedia.org/wiki/Kementerian_Koordinator_Bidang_Politik,_Hukum,_dan_Keamanan_Indonesia']
        ],
        'tokoh'=>[
            ['name'=>'Joko Widodo','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/2c95a588c6879fc7358c019ccbf6ff744042dea7.jpg','jabatan'=>'Presiden RI','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19'],
            ['name'=>'Jusup Kalla','img'=>'http://www.bijaks.net/public/upload/image/politisi/drshmuhammadjusufkalla50ee870b99cc9/badge/e47e03b37d572a59b005aaa84751a6f05b002e9f.jpg','jabatan'=>'Wakil Presiden RI','url'=>'http://www.bijaks.net/aktor/profile/drshmuhammadjusufkalla50ee870b99cc9'],
            ['name'=>'Setyo Novanto','img'=>'http://www.bijaks.net/public/upload/image/politisi/drssetyanovanto50f8fd3c666bc/badge/49cb048fd43bcfc141df78187bf0df8ccf8b0034.jpg','jabatan'=>'Ketua DPR RI','url'=>'http://www.bijaks.net/aktor/profile/drssetyanovanto50f8fd3c666bc'],
            ['name'=>'Fadli Zon','img'=>'http://www.bijaks.net/public/upload/image/politisi/fadlizon5119d8091e007/badge/11bfa198ef6e5eabc035f5e8e858487df7e7a676.jpg','jabatan'=>'Wakil Ketua DPR RI','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007'],
            ['name'=>'Fahri Hamzah','img'=>'http://www.bijaks.net/public/upload/image/politisi/fahrihamzahse5105e57490d09/badge/3319f60b624e03209d687d0af9b1c4aa2661218e.jpg','jabatan'=>'Wakil Ketua DPR RI','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09'],
            ['name'=>'Muhammad Prasetyo','img'=>'http://www.bijaks.net/public/upload/image/politisi/muhammadprasetyo54d46b2010b68/badge/956df1c81c9bd7793bde99b52c6aa938cfa9fe96.jpg','jabatan'=>'Jaksa Agung','url'=>'http://www.bijaks.net/aktor/profile/muhammadprasetyo54d46b2010b68'],
            ['name'=>'Surya Paloh','img'=>'http://www.bijaks.net/public/upload/image/politisi/suryapaloh511b4aa507a50/badge/5b8adf5757dbdc45407f3c499223705c120d8629.jpg','jabatan'=>'Pendiri NasDem','url'=>'http://www.bijaks.net/aktor/profile/suryapaloh511b4aa507a50'],
            ['name'=>'Yusril Ihza Mahendra','img'=>'http://www.bijaks.net/public/upload/image/politisi/yusrilihsamahendra51675526bbc70/badge/030eee299b8489acd2955ff3328a160310712c35.jpg','jabatan'=>'Pakar Hukum','url'=>'http://www.bijaks.net/aktor/profile/yusrilihsamahendra51675526bbc70'],
            ['name'=>'Mahfud MD','img'=>'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/badge/profdrmohammadmahfudmdshsu512c527ac591b_20130226_082408.jpg','jabatan'=>'Mantan Ketua MK','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b'],
        ],
    ],
    'PENENTANG'=>[
        'tokoh'=>[
            ['name'=>'Syafii Maarif','img'=>'http://www.bijaks.net/public/upload/image/politisi/buyaahmadsyafiimaarif543f625a98a10/badge/90e1d21718e6139f03f240f89a80be5582905b21.jpg','jabatan'=>'Tokoh Bangsa','url'=>'http://www.bijaks.net/aktor/profile/buyaahmadsyafiimaarif543f625a98a10'],
            ['name'=>'Todung Mulya Lubis','img'=>'http://www.bijaks.net/public/upload/image/politisi/todungmulyalubis533a790b3fe41/badge/4dbe1a0e9ee9325980bb723dafc1b09a85d04860.jpg','jabatan'=>'Pakar Hukum','url'=>'http://www.bijaks.net/aktor/profile/todungmulyalubis533a790b3fe41'],
            ['name'=>'Imam Prasojo','img'=>'http://www.bijaks.net/public/upload/image/politisi/imambprasodjo54d07a740feb9/badge/4c4f1c96acd8dc5671daf276bbea6e6059d35832.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/imambprasodjo54d07a740feb9'],
            ['name'=>'Erry Riyana Hardjapamekas','img'=>'http://www.bijaks.net/public/upload/image/politisi/erryriyanahardjapamekas51a2b3bdb7874/badge/6460dba04e6dd657b5132f9721d90a2e197b6ab4.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/erryriyanahardjapamekas51a2b3bdb7874'],
            ['name'=>'Tumpak Hatorangan Panggabean','img'=>'http://www.bijaks.net/public/upload/image/politisi/tumpakhatoranganpanggabean5193023cd7c5e/badge/1ead704c13a2634564cbb187f194d8e05ad2fc8e.jpg','jabatan'=>'','url'=>'http://www.bijaks.net/aktor/profile/tumpakhatoranganpanggabean5193023cd7c5e'],
            ['name'=>'Usman Hamid','img'=>'http://www.bijaks.net/public/upload/image/politisi/usmanhamid54d41a069aa7a/badge/620b5a25d4fd36497c8e05c3eaaca075dd9b5593.jpg','jabatan'=>'Pendiri Kontras','url'=>'http://www.bijaks.net/aktor/profile/usmanhamid54d41a069aa7a'],
            ['name'=>'Bonar Tigor Naipospos','img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/08/27/283626/MolJ7snx4V.jpg?w=700','jabatan'=>'Setara Institute','url'=>'http://en.wikipedia.org/wiki/Setara_Institute'],
            ['name'=>'Mgr Ignasius Suharyo Pr','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Mgr._Ign._Suharyo.JPG/120px-Mgr._Ign._Suharyo.JPG','jabatan'=>'Uskup Agung','url'=>'http://id.wikipedia.org/wiki/Ignatius_Suharyo'],
            ['name'=>'Natalius Pigai','img'=>'http://upload.wikimedia.org/wikipedia/id/9/92/Natalius_Pigai.jpg','jabatan'=>'Komisioner Komnas HAM','url'=>'http://id.wikipedia.org/wiki/Natalius_Pigai'],
            ['name'=>'Andreas Harsono','img'=>'http://www.internetfreedomfellows.com/wp-content/uploads/2013/02/Harsano21.jpg','jabatan'=>'Peneliti dan Aktivis','url'=>'http://www.andreasharsono.net/'],
        ],
        'institusi'=>[
            ['name'=>'Komnas HAM','img'=>'http://www.bijaks.net/public/upload/image/politisi/komnasham54c1eac938164/badge/e559bfdeda6bc708d46fcb9a29afb001bb2360ec.jpg','url'=>'http://www.bijaks.net/aktor/profile/komnasham54c1eac938164'],
            ['name'=>'Amnesty International','img'=>'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAkFBMVEX88SoAAAD/9Sv/9yv//iz//CxkYxH/+Svw5ib/8yr28irj2SXe2CV7dhQEAAFZVQ6NiRUsKQbAuB9fWw7XzyNIRAv17CkcGgTn3ibRyiPKwSBvahElIwWFfxWVjhiimho3NQmqohtBPgoNCwAxLgdRTQ21rh0gHQMXFAN2cBPAviG4th/++a788kKsqh6YlhqwMc81AAAJ5ElEQVR4nOWcC5OiOhOGyQWTjHBAROSiKDiiON/R///vTgKEi4szzArGqq+rzikXa4dnk87bnU5nNO3/yZBqgAfmeFA1Qo9Bfx19MNUUfxiDEQARUY1xb0y3AbfszcCQlwNwtMDGeKuZ1P0lH6ww4TOJVbM0hrxEzOHKC/n/g3dRC4jDo8ACO+Ju+IC9iVYQY1ZQgSOB/l4M2DuAEbg7lVhnFzGagrfwMOJ9bUBlIdEYXYlPVPGSZHowk1Qg0fmDYrxAolTDGJpHNVWpp6V/gUglFvHsc4P1pReoxkL8Ye8rm0iGnEVDtQjLmUNh+WdlK5JQE7SwAr18DLPK1xQ5GHI3LazUkAqPtuWTra4EC2ctzwIrvx4dUs2tpUTB0LVFBawmR4VG9eyoggt3sJat1Blv5UMFXChoY0WwkYRKvbjZr/evStJ7sDSUyMfO61Md3BaImdYW0I+T1I3X6yoyWljHDpa+k8+3rx8u2Bquvd+WdeZ/yi/8l8s9M1rKFXZUHdXE9utXY+Pa/PVdLFc+TxWkX3jZzCLtzKK3qhejguCIm2ncdTQK1U5vqoiNuJlGrz1bzTJdeSpyL71R1I4WQEs+d5XsHhuurP3+ZjlcFWU4h1okWl7PqHysQFFLLrkeN26Li8j9x5IoSuyJXHanFpfM6kH6eqGvjNXqeWnNWFmdAGdDXcXEkxvZxu9JplBQawhHymozNlVSr7ZSiKucdFXvgSrSWG0BE7mVJsjyW5X5qFKI2lBccs2q8fGKAeymiEoMV65fJvGMFgqhJCp2jXllMDwWAwbnoCjJqabiBmEpWEUVonD7i5qoeG/IL/ZqG6GjQr3ydxgtYaisV4pcS4/B+W0q43zEiqk0dcb3k/s3OnqBelGLWPpkf5dRKzaGArFfTKMDcNVrRNuQ3GC/gXZ1jRTp86enmqMwVlrxmWzfZLyYDitjuq5jXByFxhhjXSfFA2E6eXXaWgSdHvuMtnYcb7emGeWmud2FLx7CR1x/2PbjPbleXCkfzgVeGgN+wfXSPPEXXErmcZ9cszAM3blPqed5Gv/Po7RdQM9fWjOUXEvCDSEEWTNbDHotrteeqEmuWe9o0AbrxSej33KxFtfLMkX2K67pOwJKHoTd33CllE2tEj4nY1qYlycr0B/EFVFKNdIcHTE4Nibc5RDRHBQVJIawM2weP1efUeJqmDBGdMzmLh1bzsjt6F9Azlc9gjSoCl0/+72w88ZKqJ/Zq836vF6OnGHw/c4i4iqJcWif6lcO4uradqyACbmDCB1P+A89mNtV+yU/cVm2tS9OIPb1WdYhHEc6WHizzeXxs33wDxaHYVwJxjRI4t0X9euWlJSOM2JOnB/3BQf/N5tGlgW+9xEO49oRvkgQRIhBmKX7fZryn5GGo3SRIoKxJkKx7V/5APB4yDQ0UFfbO12CeWDX6I3Pa7obSTBIII6wIck25YuG6n333F2wMOKKMuxylLSM0cXtIkpbxLbR33OVRuaisJiP4WTQdMTs8Q+erz3LpSFjDUZq3aF3a5sNi0Mg7pV3XRT6N/MJQqd89U9cQf+7veXdwdKLua49cwV5yEBCzK6CC467XRrGteiZRTa3lnYuvjWRWJyZAq5bn3d5YVyeEaYh8W7bUbEGcvV35yCilQdc5+PxNnJCNowrexChZVa5MsZOs4dxmQ9ei0qudTB6jWDgeuxVAgKrPur9+EWoJ3RCy1Yyr0zKIPJ6rj7Hp3kchNcym4uykQ/Ch3Gt+/2HIFL0a+ZukIQquB7uaskXEDEd8pxxVKyBXNvutzxnLd2JfXBlXU1RGLvjQtVskC5XJ5uAun+5uJqu66TI9SdpGujmX8yZF/PhJd4jLqhTOwXgZJm3OC8OnCepVN/lhbNUcJGsJKm56qwQkjAHXXsUC8bkQrci+9SXpTC09B4WRyKo03RefjVNue5uHr0NCHW+yKwPSHRIZY/MInEC13UD2bazkJvbxVQngrItboYR4mqEE55+otXawNSxo+O+NTDn9br8sEpcwzfCeJYuE3+qap3sXFoluyTbxdmlGIl0Bh7Y8Uox4ZMKoTg6GrUJi9Q/kPGPX3cvPvTiSNtSfaL6HKLXT3CKfT5rmmGv7jA+q7pTGlmWtTSll6+jz/U+tcxgsmomKRsSwMJ2s9m6BWTbO8dxDP8EYks0JGvU8wiqvt0Rw/V9Siar/ZKwfV+iZUtMCISQ+72JcfIPWBc9th/VtwFkcMoCq141Qy93ThCESasCNitvC9EN4LtBEuzLXXTNNWnRF5ILn7k0nnO/R3xwCJ7PZTWr0tUMLMU5I98aFkFPck2xc5VGMLW5l5+o3ryEs5ltLs1zOy3akmu6m6xMy0qHv+tTkr3scmfR3W5NzyXvJ93ullTN1RtPJudCxTE/OMT3AjSI65+puKrEYPbnPk8pF4QmOGwdryecSa7++0HTchFxEzvvD7IKuZBQrUeuq46LUYE1e/CtOq7ylw9cHoRcdVxY9IAuHn2rkEtE54fXBCVXf5fLpPMolP7hRl6hfola9qdXthhr951wMH+S6++J0ZwnXUcXQ42hbXLn/4O4+kWmqIzr/l9zlZ3/h9wnHzGI7zKpv+ci2fmDsfiZ7XZ5t+S0mwHrvpjw91x4lSK8zfEzVRNSZTnifiMUXTjPclGGXBB4+cZ1kie4GCtPAGJMsP/Ft6/NReiKq3/BPsijGT35eHvYcWVcP3cLBHrFxjBNHHPDg1JrNjv5/UAujUQreWXmycuRyG9qDU47sxjEdb8fQl79Kw4s+tyeBNU7x5MLmzXkLX/LxRDz5jf5s+Knq3JEtpbwqOTKEWPUGsIVlr08fPdGdP+6rQcrff78BQWtnfYhp1rRDvDhH4dyIc01l4ljNb+kYj1KP3y6ti/Z+ZDIBZhcd/ZyL+smP3KhoBnvcqy+3G3qPzteyAEGQlz5fXIFPfYDF6mvrJW22ZxDHSFntaNPcoWFzOh7A+LsT6wfuBKaSA9IrSiKv6i7MKDoVAjHaYlHEZdu5C4O4HBaHBc/ccl+zFMhMqckDH3qaR5CegYuQh/YSJWnD1v8+xByw4DqGIbx5luudp/V4gvqdQSDCchGraVUwy4agApAWt1NSx0+Cn8c1rW4VkYn3EBnyhqPKLHWbz7unDnr7jJb/ZjefWFj4jbb5p40EOVVM/MhFG2/Ha6j/+rLfazdaix9Lf4KfIi57kqu3ct/nwLr7yc/pVaeBPV6nOYM6FtDvVyVydCl4LebkGzzHZkyLlF3dTM7St+NSxwLE+JRt0hl+02Bf9VwDGHshbf82MN1VXwdkiHCqOHY+/cZr4aNIR453Z25XLwVV2mIIM8Ikuis0O8fWelybrK13uyepjDucvp7XIf8zt5v3AqD//v313/nP5XNlzPrVW+zAAAAAElFTkSuQmCC','url'=>'http://www.amnesty.org/'],
            ['name'=>'Institut Titian Perdamaian','img'=>'http://www.titiandamai.or.id/file/profil/logo.png','url'=>'http://www.titiandamai.or.id/'],
            ['name'=>'Kontras','img'=>'http://sp.beritasatu.com/media/images/original/20120104130115874.jpg','url'=>'http://www.kontras.org/'],
            ['name'=>'Human Rights Watch','img'=>'http://www.hrw.org/sites/all/themes/hrw/images/logo.png','url'=>'http://www.hrw.org/'],
            ['name'=>'Yayasan Lembaga Bantuan Hukum Indonesia','img'=>'http://infid.org/images/1373488207.jpg','url'=>'http://www.ylbhi.or.id/'],
            ['name'=>'Setara Institute','img'=>'http://setara-institute.org/wp-content/uploads/2014/11/si-logo-2.png','url'=>'http://setara-institute.org/'],
            ['name'=>'The Wahid Institute','img'=>'http://2.bp.blogspot.com/-EXBACyHGda0/VNBtshWA2_I/AAAAAAAAFTE/E2gdTVC63j8/s1600/The%2BWahid%2BInstitute.jpg','url'=>'http://www.wahidinstitute.org'],
            ['name'=>'Komnas Perempuan','img'=>'https://rangselbudi.files.wordpress.com/2012/03/komnasperempuan.jpg','url'=>'http://www.komnasperempuan.or.id/']
        ],
        'negara'=>[
            ['name'=>'Belanda','img'=>'http://www.bijaks.net/public/upload/image/politisi/belanda5373275a1e68e/badge/247e500c1d5bff90bdad74244e02c13064ec05d2.png','url'=>'http://www.bijaks.net/aktor/profile/belanda5373275a1e68e'],
            ['name'=>'Jerman','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/125px-Flag_of_Germany.svg.png','url'=>'http://www.bijaks.net/aktor/profile/republikfederaljerman5371f1f61c897'],
            ['name'=>'Uni Eropa','img'=>'http://www.bijaks.net/public/upload/image/politisi/unieropa537eb21c45dff/badge/a0dee47380d10daa6118d48f2cbac4d90ab34069.png','url'=>'http://www.bijaks.net/aktor/profile/unieropa537eb21c45dff'],
            ['name'=>'Inggris','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/125px-Flag_of_the_United_Kingdom.svg.png','url'=>'http://id.wikipedia.org/wiki/Britania_Raya'],
            ['name'=>'Australia','img'=>'http://upload.wikimedia.org/wikipedia/en/thumb/b/b9/Flag_of_Australia.svg/640px-Flag_of_Australia.svg.png','url'=>'http://www.bijaks.net/aktor/profile/australia5372f806e167b'],
        ]
    ],
    'TERPIDANA'=>[
        'narasi'=>'Minggu, 18 Januari 2015 Jokowi telah mengeksekusi mati enam terpidana kasus narkoba di Nusa Kambangan dan Boyolali. Lima diantaranya merupakan warga negara asing, selebihnya warga negara Indonesia. Berikut nama-namanya :',
        'isi'=>[
            ['no'=>'Namaona Denis (48), warga negara Malawi, diputus PN pada tahun 2001. Grasi ditolak pada 20 Desember 2014.'],
            ['no'=>'Marco Archer Cardoso Moreira (53), warga negara Brasil, diputus PN pada 2004.'],
            ['no'=>'Daniel Enemuo alias Diarrassouba Mamadou (38), warga negara Nigeria, diputus PN pada 2004 dan grasi ditolak 30 Desember 2014.'],
            ['no'=>'Ang Kiem Soei alias Kim Ho alias Ance Tahir alias Tommi Wijaya (52), warga negara Belanda. Grasinya ditolak 30 Desember 2014.'],
            ['no'=>'Tran Thi Bich Hanh (37), warga negara Vietnam, tidak mengajukan kasasi dan permohonan gransinya ditolak pada 30 Desember 2014.'],
            ['no'=>'Rani Andriani alias Melisa Aprilia, WNI asal Cianjur, Jawa Barat. Diputus PN pada tahun 2000. Grasi ditolak 30 Desember 2014.']
        ],
        'bawah'=>'Sejak 2000 hingga 2015 ini sudah ada 27 terpidana mati yang telah dieksekusi.'
    ],
    'DAFTAR'=>[
        'narasi'=>'Data Kejaksaan Agung mencatat sebanyak 136 terpidana mati yang masuk daftar tunggu eksekusi mati.<br>Dengan rincian :',
        'isi'=>[
            ['no'=>'64 terjerat kasus narkoba (versi BNN 66 orang)'],
            ['no'=>'72 terpidana dari kasus non-narkoba'],
            ['no'=>'2 orang lainnya terbukti melakukan tindak terorisme']
        ],
        'bawah'=>'Jumlah tersebut sudah termasuk dua terpidana mati dari kelompok <b>"Bali Nine"</b>'
    ],
    'BALI_NINE'=>[
        'narasi'=>'<b>Kelompok Bali Nine</b> merupakan sebutan untuk sebilan warga negara Australia yang ditangkap di Bali, 17 April 2005 dalam upaya menyelundupkan heroin seberat 8,2 kilogram dari Australia. 
                   <br>Kesembilan orang itu adalah :',
        'isi'=>[
            ['no'=>'Andrew Chan'],
            ['no'=>'Myuran Sukumaran'],
            ['no'=>'Si Yi Chen'],
            ['no'=>'Micel Czugaj'],
            ['no'=>'Renae Lawrence '],
            ['no'=>'Tach Duc Thanh Nguyen '],
            ['no'=>'Mattew Norma '],
            ['no'=>'Scott Rush '],
            ['no'=>'Martin Stephens']
        ],
        'bawah'=>'Pengadilan Negeri Denpasar telah memvonis hukuman mati kepada Myuran Sukumaran dan Andrew Chan. Sementara Lawrence, Czugaj, Stephens, dan Rush divonis dengan hukuman seumur hidup.'
    ],
    'MENUNGGU'=>[
        'narasi'=>'Kejaksaan Agung, M. Prasetyo memastikan eksekusi mati gelombang kedua tetap dilakukan. 
                   Eksekusi mati akan dilakukan di Nusakambangan, Cilacap, Jawa Tengah pada bulan Februari.
                   Melalui Kedutaan Besar negara asal terpidana yang akan dieksekusi, pemerintah telah memberitahukan ke pihak keluarga masing-masing.
                   Berbagai persiapan telah dilakukan, termasuk regu tembak. <br>Berikut nama delapan orang terpidana mati yang akan dieksekusi bulan Februari ini : <br>Duo Bali Nine',
        'isi'=>[
            ['no'=>'Andrew Chan ( Warga Negara Australia )'],
            ['no'=>'Myuran Sukumaran ( Warga Negara Australia )'],
            ['no'=>'Mary Jane Fiesta Veloso ( Warga Negara Filipina )'],
            ['no'=>'Serge Areski Atlaoui ( Warga Negara Prancis )'],
            ['no'=>'Martin Anderson alias Belo ( Warga Negara Ghana )'],
            ['no'=>'Zainal Abidin ( Warga Negara Indonesia )'],
            ['no'=>'Raheem Agbaje Salami ( Warga Negara Spanyol )'],
            ['no'=>'Rodrigo Gularte ( Warga Negara Brazil )']
        ]
    ],
    'ANALISA'=>'Indonesia memang memiliki dasar hukum untuk memberlakukan hukuman mati. Persoalannya adalah pada dampaknya. Hukuman mati tidak berdiri sendiri. Ada dampak yang akan mempengaruhi pada situasi politik, ekonomi dan kelangsungan hidup masyarakat Indonesia di negara lain. Setidaknya ada tiga persoalan yang membayangi Indonesia : </p><p style="text-align: justify;">Pertama, hukuman mati berpotensi dipolitisir dalam sistem hukum yang tidak transparan. Hukum di Indonesia masih mengandung kerumitan macam itu. Dalam kasus narkoba, hukuman mati masih belum menyentuh pelaku utama. Bagaiamana dengan oknum TNI dan Polri yang berperan sebagai pengedar bahkan melindungi gembong narkoba? </p><p style="text-align: justify;">Kedua, dampak hubungan luar negeri. Semakin Indonesia keras kepala dalam memberlakukan hukuman mati, tidak menutup kemungkinan akan mempengaruhi hubungan ekonomi dan politik Indonesia dengan negara sahabat. </p><p style="text-align: justify;">Ketiga, tenaga kerja kita di luar negeri banyak yang bermasalah. Badan Nasional Penempatan dan Perlindungan Tenaga Kerja Indonesia (BNP2TKI) telah mencatat, di tahun 2015 akan ada lima sampai tujuh kasus terkait hukuman gantung dan hukuman mati untuk TKI. Ada di Arab Saudi, Malaysia, Emirat Arab dan Hongkong. Semantara Indonesia mati-matian membebaskan warga negaranya yang terancam hukuman mati. Pada prinsipnya, Indonesia merasa ngeri dengan hukuman mati tersebut.',
    'DIGEMPUR'=>'Kepala Badan Narkotika Nasional (BNN) Anang Iskandar menegaskan Indonesia saat ini telah menjadi sasaran jaringan narkotika internasional. Kurang lebih 40-50 jaringan internasional dan nasional. Kebanyakan berasal dari Afrika, China, Iran, Belanda dan Australia. Mereka tidak main sendirian, tetapi membentuk jejaring internasional yang sangat rapi. Lemahnya penegakan hukum di Indonesia menjadi salah satu faktor meluasnya narkotika dan kasus kejahatan luar lainnya di Indonesia.',
    'BERITA'=>[
        ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/90d6bf2588f6c43832088a3036f77ce8bc229e1c.jpg','shortText'=>'Eksekuti Mati Tertunda di Tangan Jaksa Parsetyo','link'=>'http://www.bijaks.net/scandal/index/15637-eksekuti_mati_tertunda_di_tangan_jaksa_parsetyo'],
        ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/9f56af2ea29c3c05bf5b6953f2929fc91f10b3c7.jpg','shortText'=>'Brasil dan Belanda Melawan Hukuman Mati','link'=>'http://www.bijaks.net/scandal/index/15675-brasil_dan_belanda_melawan_hukuman_mati'],
        ['img'=>'http://www.bijaks.net/public/upload/image/skandal/large/54e9c2b93beffc6a59ba7161b680a8676f38953e.jpg','shortText'=>'Sindikat Narkoba Lurah Mamasa','link'=>'http://www.bijaks.net/scandal/index/15781-sindikat_narkoba_lurah_mamasa'],
        ['img'=>'http://news.bijaks.net/uploads/2015/02/Inggris-Kecam-Hukuman-Mati-di-Indonesia-627x261.jpg','shortText'=>'Inggris Kecam Hukuman Mati di Indonesia','link'=>'http://www.bijaks.net/news/article/0-91498//inggris-kecam-hukuman-mati-di-indonesia'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/setya-novanto2.jpg','shortText'=>'Setya Novanto Minta Hukuman Mati Tidak Dilakukan Massal','link'=>'http://www.bijaks.net/news/article/0-86934/setya-novanto-minta-hukuman-mati-tidak-dilakukan-massal'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/unduhan-33.jpg','shortText'=>'Hukuman Mati Pengedar Narkoba, BNN Contoh Singapura','link'=>'http://www.bijaks.net/news/article/0-86841/hukuman-mati-pengedar-narkoba-bnn-contoh-singapura'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/surya-paloh.jpg','shortText'=>'Surya Paloh Dukung Penuh Hukuman Mati Bagi Para Koruptor','link'=>'http://www.bijaks.net/news/article/0-86560/surya-paloh-dukung-penuh-hukuman-mati-bagi-para-koruptor'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/download-52.jpg','shortText'=>'Pemerintah Brazil: Hukuman Mati Bisa Rusak Hubungan Kedua Negara','link'=>'http://www.bijaks.net/news/article/0-85616/pemerintah-brazil-hukuman-mati-bisa-rusak-hubungan-kedua-negara'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/u100.jpg','shortText'=>'Dunia Desak RI Batalkan Hukuman Mati','link'=>'http://www.bijaks.net/news/article/0-85245//dunia-desak-ri-batalkan-hukuman-mati'],
        ['img'=>'http://news.bijaks.net/uploads/2015/01/sajut.jpg','shortText'=>'DPR Anggap Hukuman Mati Tak Sesuai dengan Karakter Bangsa','link'=>'http://www.bijaks.net/news/article/0-84880/dpr-anggap-hukuman-mati-tak-sesuai-dengan-karakter-bangsa'],
    ],
    'GELOMBANG'=>[
        'narasi'=>'Kebijakan hukum mati yang dikeluarkan Presiden Joko Widodo mendapatkan kecaman keras dari berbagai penjuru, baik dari dalam dan luar negeri :',
        'isi'=>[
            ['title'=>'Dari dalam negeri','img'=>'assets/images/hotpages/hukumanmati/pic3.jpg','no'=>'Protes datang dari peneliti aktivis Hak Asasi Manusia. Kontras, Komnas HAM, Setara Institute, Institut Titian Perdamaian. Lembaga Bantuan Hukum seleuruh Indonesia juga mengecam hukuman mati Jokowi. Mereka menempatkan kebijakan hukuman mati Jokowi sebagai indikator RAPOR MERAH 100 Hari kerja Jokowi-JK.'],
            ['title'=>'Pasca eksekusi mati warga negaranya','img'=>'assets/images/hotpages/hukumanmati/pic5.jpg','no'=>'Pemerintah Brazil dan Kerajaan Belanda menarik duta besarnya dari Indonesia. Tindakan tersebut sebagai protes keras terhadap kebijakan Jokowi yang dianggap tidak manusiawi.'],
            ['title'=>'Protes juga datang dari negara Inggris','img'=>'','no'=>'Kepada Wakil Presiden Jusuf Kalla dan Menteri Luar Negeri Retno, Menteri Luar Negeri Inggris, Philip Hammond mengecam keras Indonesia yang masih menggunakan hukuman mati.'],
            ['title'=>'Dari negera Australia','img'=>'assets/images/hotpages/hukumanmati/pic4.jpg','no'=>'Australia bereaksi keras terhadap rencana pemerintah Indonesia yang tetap akan mengeksekusi dua warga negaranya, Andrew Chan dan Myuran Sukumaran. Australia mengancam akan memboikot pariwisata Indonesia. Menteri Luar Negeri Australia, Julie Bishop mengatakan “Saya pikir orang-orang Australia akan menunjukkan ketidaksetujuan mereka yang dalam terkait aksi itu, termasuk dengan membuat keputusan tentang ke mana mereka ingin melakukan liburan.“']
        ]
    ],
    'PERINGKAT'=>[
        ['name'=>'Cina','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/125px-Flag_of_the_People%27s_Republic_of_China.svg.png','url'=>'','isi'=>'Cina termasuk yang paling getol menjalankan eksekusi mati. Tahun 2013 saja tercatat sebanyak 2400 tahanan menemui ajal di tangan algojo. Hukuman mati dipertontonkan di depan publik. Mayoritas penduduk mendukung hukuman mati. <br><br><b>Metode Hukuman Mati </b><br>Pada awalnya hukuman mati dilakukan dengan cara ditembak dibagian belakang kepala dengan satu peluru dari jarak dekat.  Karena banyak diprotes warga, Pemerintah Cina mengganti dengan "Bus Eksekusi". Yaitu eksekusi dengan suntikan mematikan dengan tangan terikat diatas tempat tidur dalam Bus Eksekusi.'],
        ['name'=>'Iran','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Flag_of_Iran.svg/125px-Flag_of_Iran.svg.png','url'=>'','isi'=>'Lebih dari 370 tahanan tewas lewat eksekusi mati tahun 2013 silam. Termasuk didalamnya jurnalis, aktivis HAM dan individu dengan dakwaan ringan.<br><br><b>Metode Hukuman Mati </b><br>Iran memiliki tiga metode eksekusi, yakni tembak mati, hukuman gantung atau rajam. Sama seperti di Cina, hukum di Iran mewajibkan pelaksanaan hukuman mati di depan publik.'],
        ['name'=>'Irak','img'=>'http://www.bijaks.net/public/upload/image/politisi/irak5371bcc81e12c/badge/b6ff0864f180b7b932af06838d7c4735094e86df.png','url'=>'http://www.bijaks.net/aktor/profile/irak5371bcc81e12c','isi'=>'Hukuman mati di Irak terutama marak digunakan sebagai instrumen kekuasaan pada masa diktator Sadam Husein. Tahun 2013 Irak mengeksekusi 177 tahanan yang sebagian besar tersangka teroris. Sementara 1.724 lainnya masih mendekam di penjara dan menunggu regu penembak beraksi.<br><br><b>Metode Hukuman Mati </b><br>Metode paling sering digunakan dengan pemenggalan kepala dan ditembak.'],
        ['name'=>'Arab Saudi','img'=>'http://www.bijaks.net/public/upload/image/politisi/arabsaudi538175cd1a906/badge/cd0509f5929a33f696c75d244811d89546bba4b2.png','url'=>'http://www.bijaks.net/aktor/profile/arabsaudi538175cd1a906','isi'=>'Lebih dari 80 tahanan tewas di tangan algojo di Arab Saudi 2013 lalu, termasuk di antaranya tiga remaja yang berusia di bawah 18 tahun. Kasus berkisar antara pembunuhan, penyeludupan hingga praktik dukun.<br><br><b>Metode Hukuman Mati </b><br>Metode paling sering digunakan dengan pemenggalan kepala.'],
        ['name'=>'Amerika Serikat','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flag_of_the_United_States.svg/125px-Flag_of_the_United_States.svg.png','url'=>'http://www.bijaks.net/aktor/profile/amerikaserikat519c86ef28572','isi'=>'Sedikitnya 80 vonis hukuman mati dijatuhkan tahun 2013 di Amerika Serikat. Saat yang bersamaan 39 tahanan dieksekusi.<br><b>Metode Hukuman Mati </b><br><br>Pilihan hukuman di AS dengan menggunakan dengan menggunakan suntikan racun. Pernah ada seorang tahanan sekarat selama 39 menit setelah mendapat suntikan racun.'],
        ['name'=>'Indonesia','img'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/125px-Flag_of_Indonesia.svg.png','url'=>'','isi'=>'Sebaliknya orang nomer satu di Istana Negara itu berjanji akan segera melaksanakan sejumlah eksekusi yang tertunda. 2013 lalu Indonesia menghukum mati lima tahanan, kebanyakan tersangkut kasus penyeludupan obat-obatan terlarang.']
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Joko Widodo','content'=>'"Vonis hukuman mati itu dari pengadilan, bukan dari Presiden. Polri, Kejaksaan Agung, dan instansi lain harus menegakkan hukum betul dengan tegas, termasuk narkoba. Kalau mereka minta pengampunan, tak ada pengampunan."','jabatan'=>'Presiden RI','img'=>'http://www.bijaks.net/public/upload/image/politisi/irjokowidodo50ee1dee5bf19/badge/433f3937d4dc1aeb66e1293a2503102a8e6e827f.jpg','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19'],
        ['from'=>'Mahfud MD','content'=>'"Coba dalam waktu dekat ini ada koruptor yang dihukum mati." ','jabatan'=>'Mantan Ketua MK','img'=>'http://www.bijaks.net/public/upload/image/politisi/profdrmohammadmahfudmdshsu512c527ac591b/thumb/profdrmohammadmahfudmdshsu512c527ac591b_20130226_061354.jpg','url'=>'http://www.bijaks.net/aktor/profile/profdrmohammadmahfudmdshsu512c527ac591b'],
        ['from'=>'Yusril Ihza Mahendra','content'=>'"Ancaman narkotika bagi bangsa dan negara ini sangat serius, karena bisa menghancurkan masa depan generasi muda, dan ini terkait kejahatan terorganisir internasional. Karena itu saya setuju hukuman mati bagi mereka yang mengedarkan, tapi tidak bagi para pemakai."','jabatan'=>'Menteri Hukum Indonesia tahun 1999 hingga 2004','img'=>'http://www.bijaks.net/public/upload/image/politisi/yusrilihsamahendra51675526bbc70/badge/030eee299b8489acd2955ff3328a160310712c35.jpg','url'=>'http://www.bijaks.net/aktor/profile/yusrilihsamahendra51675526bbc70'],
        ['from'=>'Ahmad Heryawan','content'=>'"Kita menghargai komitmen Pak Presiden Jokowi untuk menjaga anak bangsa ini dari bahaya narkoba. Kita apresiasi ketegasan beliau terhadap para pengedar, cukong narkoba."','jabatan'=>'Gubernur Jawa Barat','img'=>'http://www.bijaks.net/public/upload/image/politisi/ahmadheryawanheryawan51c91b31197b8/badge/f9eaef3b06035d43e036fab74738deaadcbf417f.jpg','url'=>'http://www.bijaks.net/aktor/profile/ahmadheryawanheryawan51c91b31197b8'],
        ['from'=>'Muhammad Prasetyo','content'=>'"Mudah-mudahan ini akan memiliki efek jera."','jabatan'=>'Jaksa Agung','img'=>'http://www.bijaks.net/public/upload/image/politisi/muhammadprasetyo54d46b2010b68/badge/956df1c81c9bd7793bde99b52c6aa938cfa9fe96.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadprasetyo54d46b2010b68'],
        ['from'=>'Aidil Chandra Salim','content'=>'"Hukuman matinya sudah benar. Tinggal kita harus mempercepat eksekusi mereka begitu peninjauan kembali di Mahkamah Agung ditolak."','jabatan'=>'Deputi Hukum dan Kerja Sama Badan Narkotika Nasional','img'=>'http://upload.wikimedia.org/wikipedia/id/b/b1/Aidil_Chandra_Salim.jpg','url'=>'http://id.wikipedia.org/wiki/Aidil_Chandra_Salim'],
        ['from'=>'Surya Paloh','content'=>'"Setuju sekali (koruptor dihukum mati). Karena memang ada hukumnya untuk itu."','jabatan'=>'Pendiri Partai NasDem','img'=>'http://www.bijaks.net/public/upload/image/politisi/suryapaloh511b4aa507a50/badge/3bbe78f966747a478c9416e6361a5bc28202f63d.jpg','url'=>'http://www.bijaks.net/aktor/profile/suryapaloh511b4aa507a50'],
        ['from'=>'Kombespol Sumirat Dwiyanto','content'=>'"Siapapun yang memasukan narkoba ke sana termasuk kemarin ada warga Australia yang memasukan narkoba ke sana pun dieksekusi mati oleh Singapura, akhirnya apa, peredaran narkotika di Singapura itu, jarang sekali." ','jabatan'=>'BNN','img'=>'http://www.bijaks.net/public/upload/image/politisi/sumiratdwiyanto54cb16fc7db98/badge/8acaf9dc218afee75aa66954d77fd2e202bef68e.jpg','url'=>'http://www.bijaks.net/aktor/profile/sumiratdwiyanto54cb16fc7db98'],
        ['from'=>'Setyo Novanto','content'=>'"Namun perlu dipertimbangkan berbagai aspek misalnya pelaksanaan hukuman mati tidak dilakukan secara massal,” ujarnya dalam seminar." ','jabatan'=>'Ketua DPR RI','img'=>'http://news.bijaks.net/uploads/2015/01/setya-novanto2.jpg','url'=>'http://id.wikipedia.org/wiki/Setya_Novanto']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Usman Hamid','content'=>'"Karena pada hakikatnya, hukuman mati mengambil hak hidup yang melekat pada setiap manusia. Alasan lainnya, ada banyak dalil para pendukung hukuman mati yang tidak terbukti."','jabatan'=>'Pendiri Kontras','img'=>'http://www.bijaks.net/public/upload/image/politisi/usmanhamid54d41a069aa7a/badge/620b5a25d4fd36497c8e05c3eaaca075dd9b5593.jpg','url'=>'http://www.bijaks.net/aktor/profile/usmanhamid54d41a069aa7a'],
        ['from'=>'Bonar Tigor Naipospos','content'=>'"Jokowi enggak paham HAM. Sayangnya Menko Polhukam dan Kejagung juga enggak peka HAM. Jokowi harus moratorium hukuman mati. Pemerintah-DPR (harus) bahas draft KUHAP dan hapuskan hukuman mati dari pidana kita." ','jabatan'=>'Setara Institute','img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/08/27/283626/MolJ7snx4V.jpg?w=700','url'=>'http://en.wikipedia.org/wiki/Setara_Institute'],
        ['from'=>'Georg Witschel','content'=>'"Jerman, seperti seluruh anggota Uni Eropa lainnya, menentang diberlakukannya hukuman mati. Hal itu tidak efektif dalam menurunkan angka kejahatan. Terutama dalam kasus pelanggaran narkotik." ','jabatan'=>'Duta Besar Jerman untuk Indonesia','img'=>'http://whatsnewjakarta.com/quotesfile/germanygeorgwitschel2.jpg','url'=>'http://de.wikipedia.org/wiki/Georg_Witschel'],
        ['from'=>'Mgr Ignasius Suharyo Pr','content'=>'"Tak ada seorang pun yang berhak atas hidup orang lain. Kejahatan di sana (Cina dan Amerika Serikat) tetap saja tinggi dan teori hukuman mati memberikan efek jera itu sangat tidak terbukti."','jabatan'=>'Uskup Agung Jakarta','img'=>'http://parokiarnoldus.net/wp-content/uploads/2014/04/sajut-mgr-ign-suharyo-pr-hidup-katolik.jpg','url'=>'http://id.wikipedia.org/wiki/Ignatius_Suharyo'],
        ['from'=>'Natalius Pigai','content'=>'"Kebijakan eksekusi mati harus dihapus di Indonesia. Efek jera itu, kan, untuk orang yang bersalah, bukan untuk orang lain."','jabatan'=>'Komisioner Komisi Nasional Hak Asasi Manusia (Komnas HAM)','img'=>'http://upload.wikimedia.org/wikipedia/id/9/92/Natalius_Pigai.jpg','url'=>'http://www.bijaks.net/aktor/profile/nataliuspigai51a5607bce402'],
        ['from'=>'Andreas Harsono','content'=>'"Pemerintah Joko Widodo harus memahami bahwa eksekusi mati ialah hukuman yang sangat barbar dan tak mengurangi kejahatan."','jabatan'=>'Peneliti dan Aktivis HAM','img'=>'http://www.internetfreedomfellows.com/wp-content/uploads/2013/02/Harsano21.jpg','url'=>'http://www.andreasharsono.net/'],
        ['from'=>'Bert Koenders','content'=>'"Ini adalah hukuman kejam dan tidak manusiawi. Hukuman ini tidak dapat diterima oleh martabat dan integritas kemanusiaan."','jabatan'=>'Menteri Luar Negeri Belanda','img'=>'http://cdn.oneworld.nl/sites/oneworld.nl/files/bert_koenders_0.jpg','url'=>'http://en.wikipedia.org/wiki/Bert_Koenders'],
        ['from'=>'Human Rights Watch','content'=>'"Pemerintah Indonesia yang mengejar grasi bagi Ahmad di Arab Saudi sementara terus memberlakukan hukuman mati adalah kemunafikan terhadap hak untuk hidup." ','jabatan'=>'','img'=>'http://www.hrw.org/sites/all/themes/hrw/images/logo.png','url'=>'http://www.hrw.org/'],
        ['from'=>'Inang Winarso','content'=>'"Mereka bukan bandar utama, tapi kurir."','jabatan'=>'Direktur Eksekutif Perkumpulan Keluarga Berencana Indonesia (PKBI)','img'=>'http://jjk.co.id/wp-content/uploads/2015/01/closer-look-inang-winarso-1024x974.jpg','url'=>''],
        ['from'=>'Rupert Abbott','content'=>'"Ini akan menjadi kemunduran besar jika pemerintah meneruskan rencana mengeksekusi 20 orang tersebut tahun ini." ','jabatan'=>'Direktur Riset Asia Tenggara dan Pasifik Amnesty International','img'=>'http://www.rfa.org/vietnamese/in_depth/desp-imp-r-con-rem-vn-05232013065612.html/Rupert-new-170-rfa.jpg/image','url'=>'http://uk.linkedin.com/pub/rupert-abbott/10/b28/2a7'],
        ['from'=>'Federica Mogherini','content'=>'"Pengumuman akan dilaksanakannya eksekusi mati terhadap enam terpidana narkoba di Indonesia, termasuk seorang warga Negara Belanda, sangat disesalkan." ','jabatan'=>'Perwakilan Tinggi Uni Eropa Urusan Luar Negeri dan Kebijakan Keamanan','img'=>'https://pbs.twimg.com/profile_images/528268962505568258/_1M6gJaT_400x400.jpeg','url'=>'http://en.wikipedia.org/wiki/Federica_Mogherini'],
        ['from'=>'Philip Hammond','content'=>'"Kami menegaskan sikap Inggris dan Uni Eropa pada umumnya yang menentang hukuman mati." ','jabatan'=>'Menteri Luar Negeri Inggris','img'=>'http://i2.mirror.co.uk/incoming/article282447.ece/alternates/s615/philip-hammond-pic-pa-550274315.jpg','url'=>'http://en.wikipedia.org/wiki/Philip_Hammond']
    ],
    'VIDEO'=>[
        ['url'=>'https://www.youtube.com/watch?v=FXt-6s5KvWo'],
        ['url'=>'https://www.youtube.com/watch?v=jjmDYeI6ytU'],
        ['url'=>'http://www.bijaks.net/aktor/profile/usmanhamid54d41a069aa7a'],
        ['url'=>'https://www.youtube.com/watch?v=1ScHuyqZtiY']
    ]
]
?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-left: 10px;
        float: right;
        border: 1px solid black;
        margin-top: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 200px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .clear {
        clear: both;
    }
</style>

<h4 class="kanal-title kanal-title-gray">HUKUMAN MATI REZIM JOKOWI</h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/hukumanmati/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ANALISA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><img src="<?php echo base_url('assets/images/hotpages/hukumanmati/pic.jpg')?>" class="pic"><?php echo $data['ANALISA'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">TERPIDANA YANG TELAH DIEKSEKUSI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><img src="<?php echo base_url('assets/images/hotpages/hukumanmati/pic6.jpg')?>" class="pic2"><?php echo $data['TERPIDANA']['narasi'];?></p>
                <ol>
                    <?php
                    foreach ($data['TERPIDANA']['isi'] as $key => $val) {
                        echo "<li style='margin-left: 10px;text-align: justify;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
                <p><?php echo $data['TERPIDANA']['bawah'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">EKSEKUSI MATI GELOMBANG KEDUA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['MENUNGGU']['narasi'];?></p>
                <ol>
                    <?php
                    foreach ($data['MENUNGGU']['isi'] as $key => $val) {
                        echo "<li style='margin-left: 10px;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PASAL MEMBUNUH</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PASAL_MEMBUNUH']['narasi'];?></p>
                <ol>
                    <?php
                    foreach ($data['PASAL_MEMBUNUH']['isi'] as $key => $val) {
                        echo "<li style='margin-left: 10px;text-align: justify;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
            </div>
        </div>

        <div class="boxcustom">
            <p class="list"><b>DIGEMPUR JARINGAN NARKOBA DUNIA</b></p>
            <p style="text-align: justify;"><?php echo $data['DIGEMPUR'];?></p>

            <hr class="line-mini">

            <p class="list"><b>DAFTAR TUNGGU EKSEKUSI MATI</b></p>
            <p style="text-align: justify;"><?php echo $data['DAFTAR']['narasi'];?></p>
            <ol style="list-style-type: square;">
                <?php
                foreach ($data['DAFTAR']['isi'] as $key => $val) {
                    echo "<li style='text-align: justify;'>".$val['no']."</li>";
                }
                ?>
            </ol>
            <p style="text-align: justify;"><?php echo $data['DAFTAR']['bawah'];?></p>

            <hr class="line-mini">

            <p class="list"><b>"BALI NINE"</b></p>
            <p style="text-align: justify;"><img src="<?php echo base_url('assets/images/hotpages/hukumanmati/pic2.jpg')?>" class="pic2"><?php echo $data['BALI_NINE']['narasi'];?></p>
            <ol>
                <?php
                foreach ($data['BALI_NINE']['isi'] as $key => $val) {
                    echo "<li style='text-align: justify;'>".$val['no']."</li>";
                }
                ?>
            </ol>
            <p style="text-align: justify;"><?php echo $data['BALI_NINE']['bawah'];?></p>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PERINGKAT NEGARA DENGAN HUKUMAN MATI TERBANYAK</span></h5>
        <?php
        $no=1;
        foreach($data['PERINGKAT'] as $key=>$val){
            ?>
            <div class="black boxdotted">                
                <p style="font-size: 12px;color: black;margin-left: 10px;margin-right: 10px;text-align: justify;">
                    <a href="<?php echo $val['url'];?>"><img class="bendera" src="<?php echo $val['img'];?>" alt="<?php echo $val['isi'];?>" /></a>
                    <?php echo "<span style='font-size: 14px;font-weight: bold;'><span id='bulet'>".$no."</span>".$val['name']."</span><br><br>";?>
                    <?php echo $val['isi'];?>
                </p>       
            </div>
            <?php
            $no++;
        }
        ?>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;pading">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <p>PARPOL PENDUKUNG</p>
            <?php
            foreach($data['PENDUKUNG']['partaiPolitik'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                    $photo = 'http://www.dpp.pkb.or.id/unlocked/unlock/lambangresmipkb.jpg';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom2" style="padding-left: 20px;height: 320px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <div class="col-xs-4">
                        <div>
                            <img style="border-radius: 6px 6px 6px 6px;max-height: 70px;" src="<?php echo $val['img'];?>" class="lazy" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>">
                        </div>
                        <h4 style="font-size: .8em;"><?php echo "<span class='text-center black'>".$val['name']."</span>";?></h4>
                    </div>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="boxcustom2" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">TOKOH PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['tokoh'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;pading">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">TOKOH PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['tokoh'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="max-width: 70px;max-height: 70px;" src="<?php echo $val['img'];?>" class="lazy" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">NEGARA YANG MENGANCAM</h5>
            <?php
            foreach($data['PENENTANG']['negara'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GELOMBANG PROTES</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['GELOMBANG']['narasi'];?></p>
                <ul>
                    <?php
                    foreach ($data['GELOMBANG']['isi'] as $key => $val) { ?>
                        <li style='font-weight: bold;margin-top: 5px;'><img src="<?php echo base_url('assets/images/hotpages/cakapolri/pointles.jpg')?>" style="margin-right: 5px;"><?php echo $val['title'];?></li>
                        <p style="text-align: justify;"><?php
                        if ($val['img'] != "") { ?>
                            <img src="<?php echo base_url().$val['img'];?>" class='pic3'>
                        <?php } ?>
                        <?php echo $val['no'];?></p>
                        <div class="clear"></div>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <div class="panel-collapse collapse in col-xs-6">
                <?php 
                for($i=0;$i<=3;$i++){
                ?>
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from'];?></b><br>
                        <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['content'];?></p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                <?php    
                }
                ?>
            </div>
            <div class="panel-collapse collapse in col-xs-6">
                <?php 
                for($i=4;$i<=8;$i++){
                ?>
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENDUKUNG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENDUKUNG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['from'];?></b><br>
                        <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENDUKUNG'][$i]['content'];?></p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                <?php    
                }
                ?>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <div class="panel-collapse collapse in col-xs-6">
                <?php 
                for($i=0;$i<=3;$i++){
                ?>
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 12px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from'];?></b><br>
                        <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content'];?></p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                <?php    
                }
                ?>
            </div>
            <div class="panel-collapse collapse in col-xs-6">
                <?php 
                for($i=4;$i<=8;$i++){
                ?>
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 12px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from'];?></b><br>
                        <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content'];?></p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                <?php    
                }
                ?>
            </div>   
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/FXt-6s5KvWo" frameborder="0" allowfullscreen></iframe></p>
            <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/89ehMVZ8YPQ" frameborder="0" allowfullscreen></iframe></p>
            <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/jjmDYeI6ytU" frameborder="0" allowfullscreen></iframe></p>
            <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/1ScHuyqZtiY" frameborder="0" allowfullscreen></iframe></p> 
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['BERITA'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                    <?php
                }
                ?>
            </div>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>



