<?php 
$data = [
    'NARASI'=>[
        'title'=>'Polemik Dana Aspirasi DPR RI',
        'narasi'=>'Dewan Perwakilan Rakyat (DPR) mengesahkan Peraturan DPR tentang tata cara pengusulan pembangunan daerah pemilihan (UP2DP) atau dana aspirasi dalam rapat paripurna. Total dana tersebut sebesar Rp 11,2 triliun atau 20 miliar bagi setiap anggota DPR RI.
                    </p><p><img src="http://www.jawapos.com/jppic/19564_17246_19150_16829_18937_16620_18599_16296_sidang%20paripurna.jpg" class="pic">Dana yang akan dikucurkan melalui APBN 2016 merupakan usulan dari DPR RI untuk menjadi platform perealisasian pembangunan daerah pemilihan. Meski mendapat penolakan dari presiden Jokowi, DPR tetap memaksakan peraturan tersebut untuk diloloskan sehingga memicu pro dan kontra. 
                    </p><p>Polemikpun bergulir, ada indikasi bahwa dana aspirasi merupakan  “dana politik” yang hanya akan menguntungkan parpol bukan rakyat. Selain itu, potensi penyalahgunaan dana tersebut mengintai.'
    ],
    'ANALISA'=>[
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2014/10/02/015656420141002-012646780x390.jpg" class="pic2">Pasca disahkannya peraturan tersebut, Presiden Jokowi langsung bereaksi tegas dengan mengutus bawahannnya, Menteri Keuangan Bambang Brodjonegoro ke DPR untuk menyampaikan sikap penolakannya. Sebelumnya, partai pendukung pemerintah di DPR juga memberikan intrupsi penolakan saat dilangsungkan rapat paripurna DPR. 
                    </p><p class="font_kecil">Sikap pemerintah tersebut membuat berang pimpinan DPR dan mayoritas anggota dewan karena tanpa persetujuan presiden dana aspirasi tidak akan pernah bisa terealisasikan. DPR pun mendesak dan mencoba melobi presiden agar mau memasukkan dana aspirasi ke dalam RAPBN 2016. 
                    </p><p class="font_kecil"><img src="http://cdn-media.viva.id/thumbs2/2014/10/02/271512_pimpinan-dpr-periode-2014-2019_663_382.jpg" class="pic">Sementara itu, sejumlah aktivis dan lembaga swadaya masyarakat menolak dana aspirasi karena menganggapnya sebagai bentuk pemborosan anggaran dan rawan diselewengkan sebagai dana kampanye.  
                    </p><p class="font_kecil">Sampai saat ini kinerja anggota dewan masih sangat mengecewakan. Dana aspirasi yang disahkan DPR  yang didengungkan akan dialokasikan untuk kepentingan di dapilnya masing-masing belum melalui hasil kajian mendalam. 
                    </p><p class="font_kecil">Usulan peraturan tersebut terlalu dipaksakan karena belum  ada keperluan yang begitu mendesak sehingga DPR tidak layak mendapat suntikan dana aspirasi tersebut.
                    </p><p class="font_kecil"><img src="http://cdn.metrotvnews.com/dynamic/content/2015/06/15/404743/kUsoWP7zNM.jpg?w=668" class="pic2">Apalagi, program dana aspirasi tersebut disinyalir sebagai upaya anggota DPR untuk mengamankan konsituen di dapil mereka masing-masing. Dengan dana aspirasi sebesar 20 miliar per dapil, agenda-agenda politik parpol akan berjalan, dan tentunya akan menguntungkan partai bukan rakyat. 
                    </p><p class="font_kecil">Dana tersebut juga merupakan bentuk pemborosan anggaran, sebab dana yang sudah ada di dalam APBN/APBD sudah cukup untuk memenuhi semua kebutuhan konsituen di daerah manapun. '
    ],
    'PROKONTRA'=>[
        'narasi'=>'<img src="http://img1.beritasatu.com/data/media/images/medium/241434432594.jpg" class="pic">Sebelumnya Presiden Jokowi memberikan sinyal ketidaksetujuannnya atas usulan  DPR tentang dana aspirasi. Alasan Jokowi mengacu pada kondisi ekonomi Indonesia yang sedang lesu. Alasan lainnya,  anggaran tersebut bertentangan dengan visi misi pemerintah yang tertuang dalam program nawa cita. 
                    </p><p class="font_kecil">Presiden juga beranggapan bahwa dana aspirasi itu akan berbenturan dengan program pembangunan yang telah ditetapkan pemerintah. Penolakan tersebut juga karena tidak sesuai dengan kewenangan DPR dalam penentuan anggaran. Usulan DPR melabrak kewenangan fungsi eksekutif.
                    </p><p class="font_kecil"><img src="http://nawaberita.com/wp-content/uploads/2015/06/dana-aspirasi-620x330.jpg" class="pic2">Di sisi lain, anggota berdalih dan tersebut ialah perwujudan pasal 80 huruf J Undang-Undang No. 17 Tahun 2014 tentang MPR, DPR, DPD, dan DPRD yang berbunyi anggota DPR berhak mengusulkan dan memperjuangkan program pembangunan daerah pemilihan.
                    </p><p class="font_kecil">Atas dasar demikian, DPR  bersikeras agar dana aspirasi dimasukkan dalam alokasi RAPBN 2016. Namun, pemerintah tetap akan mempertimbangkan usulan tersebut. Jika hal tersebut tumpang tindih dengan hasil Musyawarah Perencanaan Pembangunan Nasional 2016 atau rencana kerja pemerintah 2016, usulan tersebut tidak akan dimasukkan dalam RAPBN 2016.'
    ],
    'PROFIL'=>[
        'narasi'=>'Pasal 80 huruf J Undang-Undang Nomor 17 Tahun 2014 tentang MPR, DPR, DPD, dan DPRD untuk mengusulkan peningkatan dana aspirasi bagi setiap anggota Dewan.
                    </p><p class="font_kecil">Pasal 80 huruf J pada UU MD3 menyebutkan bahwa anggota DPR berhak mengusulkan dan memperjuangkan program pembangunan daerah pemilihan. Pasal tersebut tidak mengatur besarnya dana aspirasi.',
    ],
    'MENABRAK'=>[
        'narasi'=>'UP2DP yang rencananya dimasukkan melalui program transfer daerah Dana Alokasi Khusus (DAK) sangat berpotensi menabrak UU Nomor 23 Tahun 2014 tentang Pemerintah Daerah. Selain UU No. 23 Tahun 2014, usulan dana aspirasi ‎juga berpotensi menabrak UU Nomor 25 Tahun 2004 tentang Perencanaan Pembangunan Nasional dan UU Nomor 17 Tahun 2003 tentang Tata Kelola Keuangan Negara.
                    </p><p class="font_kecil"><img src="http://assets.kompas.com/data/photo/2014/05/28/161926520140528-172737-1780x390.jpg" class="pic">Sementara, sebagaimana amanat Undang-undang, yakni disalurkan melalui Pemda di dalam Musrenbang (Musyawarah Rencana Pembangunan) 2016, mulai dari Musrenbang Desa, Kecamatan, Kabupaten, Provinsi, dan Nasional untuk persiapan APBN 2017.
                    </p><p class="font_kecil">Andrianof Chaniago, Menteri Bappenas menegaskan bahwa dana aspirasi bisa bertabrakan dengan visi misi Presiden. Sebab, didalam undang-undang program pembangunan yang direncanakan itu harus diambil dari visi misi Presiden.
                    </p><p class="font_kecil">Dana aspirasi rawan penyelewengan dan efeknya pun bisa besar apalagi besarannya mencapai Rp20 miliar per anggota dewan maka bisa Rp11 triliun efeknya bagi pembangunan.'
    ],
    'KRONOLOGI'=>[
        'narasi'=>'Setiap DPR mengajukan dana atau anggaran tambahan selalu memicu polemik dan prokontra. Dana-dana yang diminta dinilai tidak relevan. Selama tahun 2015 saja DPR kerap mengajukan tambahan anggaran atas nama kepentingan rakyat dan atas yang memicu kontroversi. Berikut daftar dana-dana yang diajukan oleh DPR :',
        'list'=>[
            ['date'=>'Januari  2015 – Tambahan Tenaga Ahli','content'=>'Anggaran sebesar Rp 1.635 triliun yang berasal dari APBNP 2015 dikucurkan untuk menambah tenaga ahli dan untuk kebutuhan kesekretariatan DPR. Sebelumnya setiap anggota DPR dibantu dua tenaga ahli dan sekretaris. Dengan adanya usulan tersebut, nantinya setiap anggota DPR akan mendapat dua tambahan tenaga ahli.'],
            ['date'=>'Februari 2015 – Dana Rumah Asipirasi','content'=>'Usulan dana rumah aspirasi memicu kontroversi. DPR mengajukan usulan tersebut secara tiba-tiba tanpa ada alasan yang kuat. Dengan dana rumah aspirasi, nantinya setiap anggota DPR akan mendapat suntikan dana Rp 150 juta per tahun, setara dengan Rp 12,5 juta per bulan untuk dana rumah aspirasi. Dana rumah aspirasi itu dialokasikan untuk biaya sewa rumah dan keperluan operasional rumah aspirasi.'],
            ['date'=>'April 2015 – Polisi Parlemen','content'=>'Diam-diam DPR mengusulkan untuk membentuk polisi parlemen. Nantinya, seorang brigadir jenderal polisi akan memimpin 1.194 polisi dan PNS. Saat ini MPR diperkuat 29 anggota pengamanan dalam (pamdal), DPR dijaga 489 anggota pamdal, dan DPD oleh 50 anggota pamdal. Namun rencana yang akan menghabiskan dana miliaran itu tenggelam karena memperoleh penolakan publik termasuk polisi sendiri.'],
            ['date'=>'Mei 2015 - Alun-Alun Demokrasi','content'=>'Pembangunan Alun-alun Demokrasi di komplek Parlemen-Senayan diresmikan pada hari Kamis (21/5/2015). Dengan dalih dibangunnya alun-alun demokrasi, menurut para anggota dewan alun-alun berfungsi untuk ruang publik bagi rakyat, dan memfasiltasi ruang bagi pendemo. Pembangunan alun-alun demokrasi ini menarik perhatian dari mana sumber anggaran pembangunan tersebut.'],
            ['date'=>'Juni 2015 – Dana Aspirasi 20 Miliar','content'=>'Dana sebesar Rp 20 miliar untuk setiap anggota dewan dengan total Rp 11,2 triliun  sudah disahkan oleh DPR. Melalui rapat paripurna, mayoritas anggota DPR menyetujui program dana asprasi. Dana tersebut akan diajukan untuk dimasukkan dalam RAPBN 2016. Namun, sejauh ini usulan tersebut masih mendapat penolakan dari pemerintah dan publik.']
        ]
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaidemokrat5119a5b44c7e4'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaikebangkitanbangsa5119b257621a4'],
            ['page_id'=>'partaigolongankarya5119aaf1dadef'],
            ['page_id'=>'partaiamanatnasional5119b55ab5fab']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1']
        ],
        'institusi'=>[
            ['name'=>'KPK','img'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/KPK_Logo.svg/915px-KPK_Logo.svg.png','url'=>'http://www.bijaks.net/aktor/profile/komisipemberantasankorupsi5192fb408b219'],
            ['name'=>'Indonesian Corruption Watch (ICW)','img'=>'http://ethixbase.com/wp-content/uploads/2014/08/ICW-x-250x1801.png','url'=>'http://www.bijaks.net/aktor/profile/icwindonesiancorruptionwatch534e45d49c5d9'],
            ['name'=>'Lingkar Madani untuk Indonesia (LIMA)','img'=>'http://www.zonalima.com/images/view/-Ray%20Rangkuti.jpg','url'=>''],
            ['name'=>'Forum Indonesia untuk Transparansi Anggaran (FITRA)','img'=>'http://nttonlinenow.com/images/stories/2011/april-mei2012/fitra.jpg','url'=>''],
            ['name'=>'Forum Masyarakat Peduli Parlemen Indonesia (Formapi)','img'=>'http://www.luwuraya.net/wp-content/uploads/2011/10/frmappi.jpg','url'=>''],
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Fahri Hamzah','jabatan'=>'wakil ketua DPR RI','img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150520_065925_harianterbit_Fahri_Hamzah.jpg','url'=>'http://www.bijaks.net/aktor/profile/fahrihamzahse5105e57490d09','content'=>'Jadi jangan takut dana dicuri karena penggunaannya dilaporkan. Yang dicuri itu yang lalu-lalu'],
        ['from'=>'Aburizal Bakrie','jabatan'=>'ketua umum Partai Golkar','img'=>'https://vensca81.files.wordpress.com/2014/03/bakrie.jpg','url'=>'http://www.bijaks.net/aktor/profile/aburizalbakrie514662d3c6827','content'=>'Ini cara agar DPR itu bekerja, bekerja membela daerahnya. Jika tidak kerja maka mereka bukan anggota dewan yang baik'],
        ['from'=>'Desmond J Mahesa','jabatan'=>'Fraksi Gerindra','img'=>'http://kulitinta.com/wp-content/uploads/2015/04/Desmond-J-Mahesa-Pembangunan-Gedung-Baru-DPR.jpg','url'=>'http://www.bijaks.net/aktor/profile/desmondjunaidimahesa50f91e722d81d','content'=>'da aspirasi yang belum diwadahi, disitu kami harus diberi semacam nama dan dicari wadahnya'],
        ['from'=>'Ahmadi Noor Supit','jabatan'=>'ketua badan anggaran DPR RI','img'=>'http://cms.monitorday.com/statis/dinamis/detail/7665.jpg','url'=>'http://www.bijaks.net/aktor/profile/Ahmadi','content'=>'Logika menolaknya apa? Barangnya saja belum ada. Saya yakin presiden mendapat informasi yang tidak benar kalau ada kata-kata menolak'],
        ['from'=>'M. Qodari','jabatan'=>'Pengamat politik dari Indobarometer','img'=>'http://rmol.co/images/berita/normal/82645_06545909102014_M_Qodari.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadqodari55957f2f47214','content'=>'Saya kira pemerintah harus legowo dan melihat kepentingan yang lebih luas. Mau tidak mau pemerintah harus meloloskan dana aspirasi'],
        ['from'=>'Edhie Baskoro Yudhiono','jabatan'=>'Fraksi Partai Demokrat','img'=>'http://www.ceritamu.com/uploads/posts/HBC/files/C1157B4F-24C6-48C4-9148-557EA942AE1C.jpg','url'=>'http://www.bijaks.net/aktor/profile/edhiebaskoroyudhoyonobcommsc50f54a548f1ed','content'=>'Masyarakat harus melihat ini secara utuh. Jangan sampai melihat anggota DPR memegang dana langsung, itu salah besar'],
        ['from'=>'Bambang Soesatyo','jabatan'=>'anggota banggar dari fraksi Golkar','img'=>'http://voiceofjakarta.co.id/wp-content/uploads/2015/05/bambang-soesatyo1.jpg','url'=>'http://www.bijaks.net/aktor/profile/hbambangsoesatyosemba50f8fbabbfadf','content'=>'Realisasinya dalam bentuk program sesuai kebutuhan daerah masing-masing dan akan ada audit untuk itu'],
        ['from'=>'Yandri Susanto','jabatan'=>'sekretaris Fraksi PAN','img'=>'http://lintaspos.com/wp-content/uploads/2015/06/Yandri.jpg','url'=>'http://www.bijaks.net/aktor/profile/yandrisusanto52e5d5a9a47a8','content'=>'Rp 20 Miliar bentuk kami peduli pada terhadap dapil. Terkadang mereka tidak punya akses untuk itu'],
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Jhonni G Plate','jabatan'=>'Wakil ketua Fraksi partai NasDem','img'=>'http://images1.rri.co.id/thumbs/berita_156902_800x600_Jhonny.JPG','url'=>'http://www.bijaks.net/aktor/profile/johnnygplate5243d1a42a8c3','content'=>'Kami meminta pada Presiden Jokowi untuk tidak mengakomadasi in  ke dalam APBN'],
        ['from'=>'Pratikno','jabatan'=>'Menteri Sekretaris Negara (Mensesneg)','img'=>'http://www.rmol.co/images/berita/normal/276459_09470108042015_5-201502062024461.jpg','url'=>'http://www.bijaks.net/aktor/profile/pratikno540d1bf323949','content'=>'Presiden mengharapkan semua pihak untuk ikut prihatin dengan kondisi rakyat, berhati-hati dalam memanfaatkan anggaran semaksimal mungkin, seefektif mungkin'],
        ['from'=>'Donal Fariz','jabatan'=> 'Peneliti Indonesia Corruption Watch (ICW)','img'=>'http://img.jogjakartanews.com/20141123194025_baharuddin_kamba_2.jpg','url'=>'','content'=>'Desain-desain seperti ini sering dipergunakan sebelum pilkada, ada upaya mendapatkan feed back yang potensial diterima anggota DPR jika itu benar-benar direalisasikan'],
        ['from'=>'Indiarto Seno Adji Plt','jabatan'=>'Wakil Ketua KPK','img'=>'http://beritabuana.co/fileuploadmaster/950640seno%20adji.jpg','url'=>'http://www.bijaks.net/aktor/profile/indriyantosenoadji53d8ba6a501a7','content'=>'DPR sebaiknya menjelaskan secara transparan mengenai tujuan dana aspirasi itu. Jangan sampai dana aspirasi memiliki potensi dan celah terjadinya korupsi'],
        ['from'=>'Bambang P. Brodjonegoro','jabatan'=>'Menteri Keuangan RI','img'=>'http://katadata.co.id/sites/default/files/konten/2015/01/Bambang-Brodjonegoro-Menkeu-Katadata-Arief2.jpg','url'=>'http://www.bijaks.net/aktor/profile/bambangbrodjonegoro51a7f8ee9f40b','content'=>'Pembahasan anggaran harus sesuai ketentuan dan tidak ada penambahan anggaran yang baru. Jadi kalau mau dicoba pun harus mengikuti aturan yang ada'],
        ['from'=>'Basuki Tjahaja Purnama (Ahok)','jabatan'=>'Gubernur DKI Jakarta','img'=>'https://cgnpolitik.files.wordpress.com/2014/04/screenshot_2014-04-07-05-15-23-1.png?w=640','url'=>'http://www.bijaks.net/aktor/profile/basukitjahajapurnama50f600df48ac5','content'=>'Kalau kayak gitu, fungsi musrenbang jadi kacau-balau. Tetapi, kalau DPR sudah memutuskan, ya mau enggak mau, kita mesti ikut. Saya sih enggak setuju, aneh banget'],
        ['from'=>'Uchok Sky Khadafi','jabatan'=>'Direktur for Center Budget of Analysis ','img'=>'http://nawaberita.com/wp-content/uploads/2015/04/uchok.jpg','url'=>'http://www.bijaks.net/aktor/profile/uchokskykhada531fe1572d14a','content'=>'Kalau DPR itu betul serius ingin bantu rakyat, caranya bukan melalui dana aspirasi. Tapi dana desa tuh diperjuangkan sama, satu desa Rp 1 miliar lebih'],
        ['from'=>'Andrinof Chaniago','jabatan'=>'Menteri Perencanaan Pembangunan Nasional','img'=>'https://fokusterkinidotcom.files.wordpress.com/2014/10/menteri-perencanaan-pembangunan-negara-kepala-bappenas-andrinof-chaniago.jpg','url'=>'http://www.bijaks.net/aktor/profile/andrinofachaniago52aa74d77395b','content'=>'Kalau berdasarkan UU, perencanaan program pembangunan diambil dari visi misi Presiden. Jadi kalau pakai konsep dana aspirasi, bisa bertabrakan dengan visi misi Presiden'],
        ['from'=>'Ray Rangkuti','jabatan'=>'Direktur Eksekutif LIMA','img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/06/24/139802/NMCa0vQsjL.jpg?w=668','url'=>'http://www.bijaks.net/aktor/profile/donalfariz559574981daf0','content'=>'Ini namanya pengeluaran macam apa? Uang disediakan dulu programnya menyusul'],
        ['from'=>'Baharuddin Kamba','jabatan'=>'Jogja Corruption Watch ','img'=>'https://upload.wikimedia.org/wikipedia/id/archive/6/6d/20141128000621!Ray_Rangkuti.jpg','url'=>'http://www.bijaks.net/aktor/profile/rayrangkuti525f3c7e380f7','content'=>'Kami sangat khawatir, sampai daerah, dana itu jadi bancakan dan hanya untuk kelompok tertentu'],
        ['from'=>'Apung Widadi','jabatan'=>'Kordinator Advokasi dan Investigasi FITRA','img'=>'http://bolanasional.co/wp-content/uploads/2015/05/Apung-Widadi.jpg','url'=>'','content'=>'Untuk itu, kami dengan tegas  menolak Dana Aspirasi masuk dalam RAPBN 2016']
    ],
    'VIDEO'=>[
        ['id'=>'HadkxhZynSk'],
        ['id'=>'H-WrVOHVb-g'],
        ['id'=>'XJu2Tood6N4'],
        ['id'=>'3hb5MGzxJNI'],
        ['id'=>'sk0WkHUHZIo']
    ],
    'FOTO'=>[
        ['img'=>'http://www.fiskal.co.id/img/news/1435205238golkar-minta-dana-aspirasi-598x422.jpg'],
        ['img'=>'http://www.sabangnews.info/wp-content/uploads/2015/01/Dana-Aspirasi.jpg'],
        ['img'=>'http://beritakabar.com/wp-content/uploads/2015/06/dana_aspirasi_dpr.jpg'],
        ['img'=>'http://sp.beritasatu.com/media/images/original/20150616165805335.jpg'],
        ['img'=>'https://scontent.cdninstagram.com/hphotos-xfa1/t51.2885-15/s320x320/e15/11430224_876389189083380_1130609759_n.jpg'],
        ['img'=>'http://sp.beritasatu.com/media/images/original/20150618182251972.jpg'],
        ['img'=>'http://www.aktual.com/wp-content/uploads/2015/06/Aksi-Tolak-Dana-Aspirasi-1.jpg'],
        ['img'=>'http://radaronline.co.id/wp-content/uploads/HI-Dana-Aspirasi.jpg'],
        ['img'=>'http://www.aktualpost.com/wp-content/uploads/2015/06/demo-tolak-dana-aspirasi.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/05/08/591f1d52-570c-49a1-860d-430dec06d504_169.jpg?w=650'],
        ['img'=>'http://store.tempo.co/cover/medium/koran/2015/06/26/dana-aspirasi-tabrak-banyak-aturan.jpg'],
        ['img'=>'http://i.ytimg.com/vi/cGqcR3Y2jZw/0.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/06/16/405126/KoOj6AY4LA.jpg?w=668'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/06/24/1420235011-fot01.JPG20-780x390.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/demo-tolak-dana-aspirasi-anggota-dpr_20150619_121757.jpg'],
        ['img'=>'http://metroterkini.com/asset/foto_berita/09dec042b8c239c27718fd119a16a999.jpg'],
        ['img'=>'http://statik.tempo.co/?id=412995'],
        ['img'=>'http://cdnimage.terbitsport.com/image.php?width=650&image=http://cdnimage.terbitsport.com/imagebank/gallery/large/20150629_113841_harianterbit_Dana_Aspirasi.jpg'],
        ['img'=>'http://fajar.co.id/wp-content/uploads/2015/06/Ilustrasi-DPR-RI-Sultra-Dana-Aspirasi.jpg'],
        ['img'=>'http://analisadaily.com/assets/image/news/big/2015/06/dana-aspirasi-demi-kepentingan-wakil-rakyat-143735-1.jpg'],
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }    
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

    .gallery li {
        display: block;
        float: left;
        height: 50px;
        margin-bottom: 7px;
        margin-right: 0px;
        width: 25%;
        overflow: hidden;
    }
    .gallery li a {
        height: 100px;
        width: 100px;
    }
    .gallery li a img {
        max-width: 97%;
    }

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $(".gallery").lightGallery();
    $(".gallery2").lightGallery();
})
</script>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/danaaspirasi/top_kcl.png")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:-10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ANALISA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['ANALISA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PRO-KONTRA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROKONTRA']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">MENABRAK VISI PRESIDEN</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['MENABRAK']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PASAL 80 UU MD3</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://kebebasaninformasi.org/wp-content/uploads/2015/02/UU-MD3.jpg" class="col-xs-12 col-sm-6" style="width: 100%;margin-bottom: 10px;">
                <p style="text-align: justify;"><?php echo $data['PROFIL']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">DANA KONTROVERSI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['KRONOLOGI']['narasi'];?></p>
                <ol style="list-style-type: circle;margin-left: -10px;">
                    <?php
                    foreach ($data['KRONOLOGI']['list'] as $key => $val) {
                        echo "<li style='margin-right: 20px;font-weight: bold;'>".$val['date']."</li><p>".$val['content']."</p>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                    $photo = 'http://statis.dakwatuna.com/wp-content/uploads/2013/01/logo-PKB.jpg';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['FOTO'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                        <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>            
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>