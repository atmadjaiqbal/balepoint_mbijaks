<?php 
$left = '534px';
$right = '420px';
?>
<meta property="og:image" content="http://www.bijaks.net/assets/images/hotpages/cakapolri/peristiwa_polri.png" />

<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js" ></script>
<link rel="stylesheet" href="<?php echo base_url('assets/css/cakapolri.css'); ?>" type="text/css" media="screen">
<style type="text/css">

#main-container {
    width: 100%;
    /*min-height:600px;*/
    height: auto;
    margin-top:25px;
    margin-right:auto;
    margin-bottom:100px;
    margin-left:auto;
    /*background: url("<?php echo base_url('assets/images/hotpages/cakapolri/peristiwa_polri.png')?>") no-repeat scroll 0 0 rgba(0,0,0,0);*/

}

.nodes{
    width: 20px;
    height: 20px;
    float: left;
/*    background: url("*/<?php //echo base_url('assets/images/hotpages/cakapolri/point.png')?>/*");*/
}

#dotbar{
    float: left;
    border: none 1px red;
    width: 162px;
    height: 3px;
    margin-top: 122px;
    margin-left: 3px;
    border-radius: 10px 10px 10px 10px;
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/titik-penyokong.png');?>");

}


#node_pendukung{
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/pendukung.png')?>");
    width: 523px;
    height: 44px;
    margin-left: -5px;

}

#node_penentang{
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/penentang.png')?>");
    width: 523px;
    height: 41px;
    margin-left: -5px;

}

#node_kronologi{
    background: url("<?php echo base_url('assets/images/hotpages/cakapolri/penentang.png')?>");
    width: 523px;
    height: 44px;
    margin-left: -5px;

}


#kronologi_box{
    padding-left: 10px;
    padding-top: 10px;
    width: 430px;
    margin-left: 24px;
    height: auto;
    background: url('<?php echo base_url("assets/images/hotpages/cakapolri/back-kronologi.png");?>');
}

#titik{
    float: left;
    margin-left: 21px;
    margin-top: 24px;
    width: 11px;
    height: 751px;
    background: url('<?php echo base_url("assets/images/hotpages/cakapolri/kronologi.png");?>') no-repeat;
}

.bg-img{
    float: left;
    margin-left: 43px;
    margin-top: 10px;
}

.timg{
    width: 160px;
}

.texting1 {
    /*color: #000000;*/
    /*font-size: 14px;*/
    /*margin-left: 5px;*/
    /*text-align: justify;*/
    width: 440px !important;
    /*word-wrap: break-word;*/
}



/*.container-fluid {*/
    /*padding-left: 0 !important;*/
/*}*/

.node_text{
    float: left;
    text-align: center;
    font-size: 20px;
    font-weight: bold;
    margin-left: 6px;
    margin-top: 1px;
}

#container_container{
    margin-top: 400px;
    margin-left: -25px;
    position: absolute;
}

.content_box{
    margin-top: 10px;
    margin-bottom: 10px;
    border-radius: 5px 5px 5px 5px;
    border: solid 3px rgba(119,172,238,1.0);
    background: rgba(69,8,195,0.2);
    padding-top: 5px;
    padding-bottom: 5px;
    padding-left: 5px;
    padding-right: 5px;

}

#narration{
    width: 493px;
    margin-left: 28px;
    word-wrap: break-word;
    font-size: 14px;

}

#analisa{
    width: 493px;
    margin-left: 28px;
    word-wrap: break-word;
    font-size: 14px;

}



#penyokong1{
    float: left;
    border: none 1px red;
    width: 126px;
    height: 165px;
    margin-top: 20px;
    margin-left: 37px;
    border-radius: 10px 10px 10px 10px;
    background: rgba(225,225,225,1.0);
}
#penyokong2{
    float: left;
    border: none 1px red;
    width: 174px;
    height: 165px;
    margin-top: 20px;
    margin-left: 0;
    border-radius: 10px 10px 10px 10px;
    background: rgba(225,225,225,1.0);
}


#penyokong2_image{
    padding-top: 10px;
    padding-left: 38px;
    padding-right: 10px;
    width: 104px;
}

#penyokong2_logo{
    width: 24px;
    height: 15px;
    margin-left: 10px;
}

#penyokong2_name{
    margin-left: 10px;
    font-size: 11px;
    font-weight: bold;
}

#penyokong2_jabatan{
    font-weight: bold;
}
#node_pendukung_text{
    margin-left: 190px;
    color: rgba(255,255,255,1.0);
    padding-top: 10px;
    font-size: 20px;
    font-weight: bold;
}

#node_penentang_text{
    margin-left: 190px;
    color: rgba(255,255,255,1.0);
    padding-top: 10px;
    font-size: 20px;
    font-weight: bold;
}

#node_kronologi_text{
    margin-left: 190px;
    color: rgba(255,255,255,1.0);
    padding-top: 10px;
    font-size: 26px;
    font-weight: bold;
}

.green_box{
    background-color: #eaf7e3;
    width: 515px;
    margin-left: 5px;
    margin-top: -3px;
    height: auto;
    margin-bottom: 15px;
    padding-right: 0;
}

.red_box{
    background-color: #e95757;
    width: 515px;
    margin-left: 5px;
    margin-top: -3px;
    height: auto;
    margin-bottom: 15px;
    padding-right: 0;
}

#parpol_pendukung_box{
    margin-top: 1px;
    margin-left: 1px;
    height: 300px;
}

#parpol_penentang_box{
    margin-top: 1px;
    margin-left: 1px;
    height: 167px;
}


#institusi_pendukung_box{
    margin-top: 1px;
    margin-left: 1px;
}

#institusi_penentang_box{
    margin-top: 1px;
    margin-left: 1px;
    height: 415px;
}

#pk_penentang_box{
    margin-top: 1px;
    margin-left: 1px;
    height: 175px;
}

#tokoh_penentang_box{
    margin-top: 1px;
    margin-left: 1px;
    height: 534px;
}

#parpol_pendukung_title{
    margin-left: 10px;
    font-weight: bold;
    font-size: 15px;
    margin-top: 10px;
    margin-bottom: 10px;

}

#parpol_penentang_title{
    margin-left: 10px;
    font-weight: bold;
    font-size: 15px;
    margin-top: 10px;
    margin-bottom: 10px;

}


#institusi_pendukung_title{
    margin-left: 10px;
    font-weight: bold;
    font-size: 15px;
    padding-top: 10px;
    margin-bottom: 10px;

}


#institusi_penentang_title{
    margin-left: 10px;
    font-weight: bold;
    font-size: 15px;
    padding-top: 10px;
    margin-bottom: 10px;

}

#pk_penentang_title{
    margin-left: 10px;
    font-weight: bold;
    font-size: 15px;
    padding-top: 10px;
    margin-bottom: 10px;

}

#tokoh_penentang_title{
    margin-left: 10px;
    font-weight: bold;
    font-size: 15px;
    padding-top: 10px;
    margin-bottom: 10px;

}

.partai_images{
    margin-left: 10px;
    height: 110px;
    margin-top: 10px;
}

.institusi_pendukung_text{
    margin-left: 10px;
    font-size: 12px;

}

.institusi_penentang_text{
    margin-left: 10px;
    font-size: 12px;

}

.pk_penentang_text{
    margin-left: 10px;
    font-size: 12px;

}

.row-fluid .span3 {
    width: 22.404% !important;
}

.plain_title{
    margin-left: 1px;
    font-size: 20px;
    font-weight: bold;
    margin-top: 1px;
}

.plain_title2{
    margin-left: 1px;
    font-size: 20px;
    font-weight: normal;
    margin-top: 1px;
}
.gelombang{
    width: 518px;
    word-wrap: break-word;
    font-weight: normal;
    font-size: 14px;
    margin-top: 20px;
    margin-left: 1px;
    margin-bottom: 1px;
}

#berita_terkait{
    width: 513px;
}

.berita_terkait_class{
    margin-left: 10px;
    width: 148px;
    height: 75px;
}

.berita_text{
    margin-left: .5em;
    width: 56%;
    border-bottom: 1px solid #c4c4c4;
    font-size: 1em;
    text-align: justify;
}

.calon_tunggal{
    margin-top: 69px;
}

#calon_name{
    margin-left: 41px;
    margin-top: 22px;
    font-size: 20px;
    font-weight: bold;
    color: #ffffff;
}

#steps{
    float: left;
    width: 240px;
}
#kronologi_list{
    float: left;
    width: 368px;
    margin-left: 10px;
    margin-top: 10px;
}

.kronologi_date{
    font-size: 14px;
    font-weight: bold;
    color: #ac0808;
}

.kronologi_details{
    font-size: 1em;
}

#kronologi_0{
    margin-top: 9px;
}

#kronologi_1{
    margin-top: 1px;
}

#kronologi_2{
    margin-top: 1px;
}
#kronologi_3{
    margin-top: 11px;
}
#kronologi_4{
    margin-top: 8px;
}
#kronologi_5{
    margin-top: 1px;
}
#kronologi_6{
    margin-top: 7px;
}
#kronologi_7{
    margin-top: 1px;
}
#kronologi_8{
    margin-top: 7px;
}
#kronologi_9{
    margin-top: 7px;
    margin-bottom: 10px;
}

.quote_left_container{
    float: left;
    width: 187px;
    min-height: 300px;
    margin-right: 5px;
    background-color: #eeeef4;
}

.quote_right_container{
    float: left;
    width: 200px;
    min-height: 300px;
    background-color: #eeeef4;
}




#top_calon_tunggal{
    padding-top: 5px;
    padding-left: 5px;
    border-radius: 10px 0 0 10px;
    width: 427px;
    /*background: rgba(101,97,90,1.0);*/
    background-color: #877c65;
}


#foto_calon{
    margin-top: -216px;
    margin-right: 10px;
    float: right;
}

#narasibg{
    color: #e9f0ae;
    font-size: 14px;
    margin-left: 40px;
    margin-top: 10px;
    word-wrap: break-word;
}



#narasibiodata{
    margin-left: 40px;
}

.subtitling{
    color: #e9f0ae;
    font-weight: bold;
    margin-top: 10px;
    margin-bottom: 1px;
    margin-left: 40px;
    font-size: 14px;
}




.karir{
    color: #e9f0ae;
    font-size: 14px;
}

.redblock{
    height: 32px;
    font-size: 20px;
    color: #000000;
    text-align: center;
    background-color: #e08b8b ;
    /*width: 440px;*/
    margin-left: 4px;
    padding-top: 6px;;
    font-weight: bold;
}

.texting1{
    color: #000000;
    font-size: 14px;
    width: 430px;
    text-align: justify;
    word-wrap: break-word;
    margin-left: 5px;
}

.box_calon_baru{
    /*border: solid 1px red;*/
    /*border-radius: 10px 10px 10px 10px;*/
    /*border: dotted 3px #b1b1b1;*/
    /*width: 430px;*/
    /*margin-bottom: 10px;*/
    /*min-height: 100px;*/
    /*height: 200px;*/
}

.cal_bar_left{
    float: left;
    width: 100%;
}

.cal_bar_right{
    float: left;
    width: 100%;
    word-wrap: break-word;
}

.cal_bar_name{
    color: #000000;
    font-size: 16px;
    font-weight: bold;
    margin-left: 6px;
    margin-top: 4px;

}

.small_foto{
    float: left;
    width: 72px;
    margin-left: 10px;
    margin-bottom: 5px;
}


.small_info{
    float: left;
    width: 70%;
    margin-left: 2px;
    font-size: 1em;
    word-wrap: break-word;

}

#kronologi_box{
    width: 417px !important;
}

.one_quote{
    padding-left: 10px;
    padding-top: 10px;
;
}
.from_photo{
    float: left;
    width: 50px;
    height: 50px;
}

.from_detail{
    float: left;
    width: 75%;
}

.from_name{
    font-weight: bold;
    text-decoration: underline;
}

.from_jabatan{
    font-weight: normal;
    text-decoration: none;
    font-size: .9em;
}

.quote_pendukung_content{
    color: #0000FF;
    border-bottom: solid 2px #b1b1b1;
    font-size: 12px;
}

.quote_penentang_content{
    color: #ee0101;
    border-bottom: solid 2px #b1b1b1;
    font-size: 12px;
}










.container-fluid {
    /*margin-right: auto;*/
    /*margin-left: auto;*/
    padding-left: 0 !important;
    padding-right: 0 !important;
}

.top_image{
    margin-top: -33px;
    width: 100%;
    height: 100%;
    margin-bottom: 0;
}

.subtitling1{
    font-weight: bold;
    text-align: center;
    background: rgba(249,7,24,0.7);
    width: 100%;
}

#textwrap{
    float: right;
    margin: 5px;
}

.analisa_point{
    /*font-size: 14px;*/
    /*border-bottom: solid 1px #c4c4c4;*/
    margin-bottom: 10px;
}

.subnode1{
    font-weight: bold;
}

.biodata_title{
    float: left;
    /*width: 140px;;*/
    /*color: #e9f0ae;*/
    margin-left: 0;
    /*font-size: 14px;*/
}

.biodata_content{
    float: left;
    /*color: #e9f0ae;*/
    /*font-size: 14px;*/
}

.padded{
    margin-top: 1px;
}

.clear{
    clear: both;
}

.row{
    margin-bottom: 15px;
}

.biodata_content2{
    /*color: #e9f0ae;*/
    font-size: 10px;
    /*margin-left: 40px;*/
}

.pen_image{
    float: left;
    width: 50px;
}

.pen_text{
    float: left;
}

.pen_logo{
    width: 25px;
    height: 15px;
}

.pen_image_img{
     width: 45px;
     height: 60px;
}

.bottom_lined{
    border-bottom: solid 1px #c4c4c4;
}

.parpol_logo{
    float: left;
    width: 50px;
}

.parpol_logo_img {
    width: 45px;
    height: 35px;
}

.parpol_name{
    float: left;
    font-size: .9em;
    width: 80%;

}
</style>

<img src="<?php echo base_url('assets/images/hotpages/cakapolri/peristiwa_polri.png')?>" class="top_image"/>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12 subtitling1 padded">
            <?php echo $data['title'];?>
        </div>
        <div class="span12 padded">
            <?php echo $data['narration'];?>
        </div>
        
    </div>

    <div class="row-fluid">
        <div class="span12 subtitling1 padded">
            CALON TUNGGAL
        </div>
        <div class="span12 padded">
            <div class="subnode1"><?php echo $data['calonDiusung'][0]['name'];?></div>
        </div>
        <div class="span12 padded">
            <a href="<?php echo !empty($data['calonDiusung'][0]['page_id']) ? base_url('aktor/profile/'.$data['calonDiusung'][0]['page_id']) : '#';?>">
                <img src="<?php echo $data['calonDiusung'][0]['img']?>" style="width: 100px;height: 100px;" id="textwrap"/>
            </a>
            <?php echo $data['narasibg'];?>
            <div class="clear"></div>
        </div>
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                BIODATA
            </div>
            <div class="span12 padded">
                <div class="biodata_title">Nama</div>
                <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['name']?></div>
                <div class="clear"></div>
                <div class="biodata_title">Tempat/Tanggal Lahir</div>
                <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['ttl']?></div>
                <div class="clear"></div>
                <div class="biodata_title">Agama</div>
                <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['agama']?></div>
                <div class="clear"></div>
                <div class="biodata_title">Almamater</div>
                <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['almamater']?></div>
                <div class="clear"></div>
                <div class="biodata_title">Pangkat</div>
                <div class="biodata_content">:&nbsp;<?php echo $data['calonDiusung'][0]['pangkat']?></div>
                <div class="clear"></div>
            </div>
            
            <div class="span12 padded">
                <div class="subtitling1">KARIR</div>
                <div class="biodata_content2">
                    <?php
                    foreach($data['karir'] as $key=>$val){
                        $mulai = $val['mulai'];
                        $berakhir = !empty($val['berakhir']) ? ' - '.$val['berakhir'] : str_repeat('&nbsp;',12);
                        $sbg = $val['sbg'];
                    ?>
                        <div style="float: left;width: 95px;font-size: 12px;">
                            <?php echo $mulai.$berakhir;?>
                        </div>
                        <div style="float: left;font-size: 12px;word-wrap: break-word;width: 250px;">
                            <?php echo $sbg;?>
                        </div>
                        <div class="clear"></div>

                    <?php
                    }
                    ?>

                </div>
            </div>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span12 subtitling1 padded">
            ANALISA
        </div>
        <div class="span12 padded">
            <?php echo $data['analisa'];?>
        </div>
        
    </div>

    <div class="row-fluid">
        <div class="span12 subtitling1 padded">
            PENYOKONG
        </div>
        <div class="span12 padded">
            <div class="pen_image">
                <a href="<?php echo !empty($data['penyokong'][0]['page_id']) ? base_url('aktor/profile/'.$data['penyokong'][0]['page_id']) : '#';?>">
                    <img src="<?php echo $data['penyokong'][0]['img'];?>" class="pen_image_img"/>
                </a>
            </div>
            <div class="pen_text">
                <div class="pen_text_name"><?php echo strtoupper($data['penyokong'][0]['name']);?></div>
                <div class="pen_text_bottom">
                    <img class="pen_logo" src="<?php echo $data['penyokong'][0]['logo'];?>"/>&nbsp;
                    <?php echo $data['penyokong'][0]['jabatan'];?>
                </div>                
            </div>
        </div>
        <div class="clear bottom_lined"></div>
        <div class="span12 padded">
            <div class="pen_image">
                <a href="<?php echo !empty($data['penyokong'][1]['page_id']) ? base_url('aktor/profile/'.$data['penyokong'][1]['page_id']) : '#';?>">
                    <img src="<?php echo $data['penyokong'][1]['img'];?>" class="pen_image_img"/>
                </a>
            </div>
            <div class="pen_text">
                <div class="pen_text_name"><?php echo strtoupper($data['penyokong'][1]['name']);?></div>
                <div class="pen_text_bottom">
                    <img class="pen_logo" src="<?php echo $data['penyokong'][1]['logo'];?>"/>&nbsp;
                    <?php echo $data['penyokong'][1]['jabatan'];?>
                </div>                
            </div>
        </div>

        <div class="clear"></div>
    </div>

    <div class="row-fluid">
        <div class="span12 subtitling1 padded">
            PARPOL PENDUKUNG
        </div>
        <?php 
        foreach($data['partaiPolitikPendukung'] as $key=>$val){
            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
            $personarr = json_decode($person, true);
            $pageName = strtoupper($personarr['page_name']);
            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
            if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                $photo = 'http://www.dpp.pkb.or.id/unlocked/unlock/lambangresmipkb.jpg';
            }
        ?>
        <div class="span12 padded">
            <div class="parpol_logo">
                <a href="<?php echo base_url('aktor/profile/'.$val['page_id']);?>">
                    <img class="parpol_logo_img" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                </a>
            </div>
            <div class="parpol_name">
                <?php echo $pageName;?>            
            </div>
        </div>
        <div class="clear bottom_lined"></div>
        <?php    
        };?>
    </div>

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                INSTITUSI PENDUKUNG
            </div>
            <?php 
            foreach($data['institusiPendukung'] as $key=>$val){
            ?>
            <div class="span12 padded">
                <div class="parpol_logo">
                    <a href="<?php echo base_url('aktor/profile/'.$val['page_id']);?>">
                        <img class="parpol_logo_img" src="<?php echo $val['logo'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                    </a>
                </div>
                <div class="parpol_name">
                    <?php echo $val['name'];?>            
                </div>
            </div>
            <div class="clear bottom_lined"></div>
            <?php    
            };?>
        </div>

    </div>    

    <div class="row-fluid">
        <div class="span12 subtitling1 padded">
            PARPOL PENENTANG
        </div>
        <?php 
        foreach($data['partaiPolitikPenentang'] as $key=>$val){
            $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
            $personarr = json_decode($person, true);
            $pageName = strtoupper($personarr['page_name']);
            $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
        ?>
        <div class="span12 padded">
            <div class="parpol_logo">
                <a href="<?php echo base_url('aktor/profile/'.$val['page_id']);?>">
                    <img class="parpol_logo_img" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>"/>
                </a>
            </div>
            <div class="parpol_name">
                <?php echo $pageName;?>            
            </div>
        </div>
        <div class="clear bottom_lined"></div>
        <?php    
        };?>
    </div>

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                INSTITUSI PENENTANG
            </div>
            <?php 
            foreach($data['institusiPenentang'] as $key=>$val){
            ?>
            <div class="span12 padded">
                <div class="parpol_logo">
                    <a href="<?php echo base_url('aktor/profile/'.$val['page_id']);?>">
                        <img class="parpol_logo_img" src="<?php echo $val['logo'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                    </a>
                </div>
                <div class="parpol_name">
                    <?php echo $val['name'];?>            
                </div>
            </div>
            <div class="clear bottom_lined"></div>
            <?php    
            };?>
        </div>

    </div>    

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                TOKOH PENENTANG
            </div>
            <?php 
            foreach($data['tokohPenentang'] as $key=>$val){
            ?>
            <div class="span12 padded">
                <div class="parpol_logo">
                    <a href="<?php echo $val['url'];?>">
                        <img class="parpol_logo_img" src="<?php echo $val['logo'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                    </a>
                </div>
                <div class="parpol_name">
                    <?php echo $val['name'];?>            
                </div>
            </div>
            <div class="clear bottom_lined"></div>
            <?php    
            };?>
        </div>

    </div>    

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                PEJABAT KEPOLISIAN PENENTANG
            </div>
            <?php 
            foreach($data['pejabatKepolisianPenentang'] as $key=>$val){
            ?>
            <div class="span12 padded">
                <div class="parpol_logo">
                    <a href="<?php echo $val['url'];?>">
                        <img class="parpol_logo_img" src="<?php echo $val['logo'];?>" alt="<?php echo $val['name'];?>" data-toggle="tooltip" data-original-title="<?php echo $val['name'];?>"/>
                    </a>
                </div>
                <div class="parpol_name">
                    <?php echo $val['name'];?>            
                </div>
            </div>
            <div class="clear bottom_lined"></div>
            <?php    
            };?>
        </div>

    </div>    

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                GELOMBANG PENOLAKAN
            </div>
            <?php 
            foreach($data['gelombangPenolakan'] as $key=>$val){
            ?>
            <div class="span12 padded">
                <div style="font-size: 14px;padding: 1px;">
                    <img src="<?php echo base_url('assets/images/hotpages/cakapolri/pointles.jpg')?>"/>&nbsp;
                    <span style="font-weight: bold;font-size: 14px;"><?php echo $val['title'];?></span>
                </div>
                <div style="margin-left: 20px;">
                    <?php echo $val['aksi'];?>
                </div>
            </div>
            <div class="clear bottom_lined"></div>
            <?php    
            };?>
        </div>

    </div>    

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                QUOTE PENDUKUNG
            </div>
            <?php 
            for($ii=0;$ii<=8;$ii++){
            ?>
            <div class="span12 padded">
                <div style="font-size: 14px;padding: 1px;">
                    <div class="one_quote">
                        <div class="from_photo">
                            <a href="<?php echo !empty($data['quotePendukung'][$ii]['url']) ? $data['quotePendukung'][$ii]['url'] : '#';?>">
                            <img src="<?php echo $data['quotePendukung'][$ii]['img'];?>" style="width: 40px;"/>
                            </a>
                        </div>
                        <div class="from_detail">
                            <div class="from_name"><?php echo $data['quotePendukung'][$ii]['from'];?></div>
                            <div class="from_jabatan"><?php echo $data['quotePendukung'][$ii]['jabatan'];?></div>
                        </div>
                        <div class="clear"></div>
                        <div class="quote_pendukung_content">
                            <?php echo $data['quotePendukung'][$ii]['content'];?>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <?php    
            };?>
        </div>

    </div>    

<div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                QUOTE PENENTANG
            </div>
            <?php 
            for($ii=0;$ii<=8;$ii++){
            ?>
            <div class="span12 padded">
                <div style="font-size: 14px;padding: 1px;">
                    <div class="one_quote">
                        <div class="from_photo">
                            <a href="<?php echo !empty($data['quotePenentang'][$ii]['url']) ? $data['quotePenentang'][$ii]['url'] : '#';?>">
                            <img src="<?php echo $data['quotePenentang'][$ii]['img'];?>" style="width: 40px;"/>
                            </a>
                        </div>
                        <div class="from_detail">
                            <div class="from_name"><?php echo $data['quotePenentang'][$ii]['from'];?></div>
                            <div class="from_jabatan"><?php echo $data['quotePenentang'][$ii]['jabatan'];?></div>
                        </div>
                        <div class="clear"></div>
                        <div class="quote_pendukung_content">
                            <?php echo $data['quotePenentang'][$ii]['content'];?>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <?php    
            };?>
        </div>

    </div>    

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                KRONOLOGI
            </div>
            <div class="clear"></div>
                            
            <div id="kronologi_list_">
                <?php 
                foreach ($data['kronologi'] as $key => $value) {
                ?>    
                <div id="kronologi_<?php echo $key;?>">
                    <div class="kronologi_date">
                        <?php echo $value['date'];?>
                    </div>
                    <div class="kronologi_details">
                        <?php echo $value['content'];?>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>

    </div>    

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                SERANGAN BUAT BUDI GUNAWAN
            </div>
            <div>
                <?php echo $data['seranganBuatBudiGunawan'];?>      
            </div>
        </div>

    </div>    

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                POLRI SERANG BALIK KPK
            </div>
            <div>
                <?php echo $data['polriSerangBaliKpk']['narasi1']?>
                <?php
                foreach($data['polriSerangBaliKpk']['serangan'] as $key=>$val){
                ?>
                <div>
                    <div style="float: left;width: 15px;">
                    <img src="<?php echo base_url('assets/images/hotpages/cakapolri/pointles.jpg');?>"/>
                    </div>
                    <div style="float: left;word-wrap: break-word;width: 93%;text-align: justify;">
                    <?php echo $val['aksi'];?>
                    </div>
                </div>
                <div class="clear"></div>
                <?php
                }
                ?>
                <br/>
                <?php echo $data['polriSerangBaliKpk']['narasi2']?>      
            </div>
        </div>

    </div>  

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                CALON BARU KAPOLRI
            </div>
            <div><?php echo $data['narasiCalonBaru'];?></div>
            <?php
            foreach($data['calonBaru'] as $k=>$v){
                if($k == 0){
                    $hh = '163px';
                }elseif($k == 1){
                    $hh = '165px';
                }elseif($k == 2){
                    $hh = '170px';
                }elseif($k == 3){
                    $hh = '222px';
                }elseif($k == 4){
                    $hh = '147px';
                }elseif($k == 5){
                    $hh = '150px';
                }else{
                    $hh = '100px';
                };
            ?>
            <div class="box_calon_baru" style="height: <?php echo $hh;?>">
                <div class="cal_bar_left">
                    <div class="cal_bar_name"><?php echo $v['nama'];?></div>
                    <div>
                        <div class="small_foto">
                            <a href="<?php echo $v['url'];?>">
                            <img src="<?php echo $v['img'];?>" width="70px;height: 65px;"/>
                            </a>
                        </div>
                        <div class="small_info">
                            <div>Lahir:<?php echo $v['lahir'];?></div>
                            <div>Lulusan:<?php echo $v['lulusan'];?></div>
                            <div>Jabatan Terakhir:<?php echo $v['jabatanTerakhir'];?> </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="cal_bar_right">
                    <div style="font-weight: bold;margin-top: 1px;">KARIR</div>
                    <?php
                    foreach($v['karir'] as $key=>$val){
                    ?>
                    <div style="float: left;width: 3em;">
                        <?php echo $val['tahun'];?>
                    </div>
                    <div style="float: left;">
                        <?php echo $val['jabatan'];?>
                    </div>
                    <div class="clear"></div>
                    <?php
                    }
                    ?>
                </div>


            </div>
            <div class="clear"></div>
            <?php
            }
            ?>
        </div>

    </div>  

    <div class="row-fluid">    
        <div class="row-fluid">
            <div class="span12 subtitling1 padded">
                BERITA TERKAIT
            </div>
            <div>
                <div class="clear"></div>
                <div id="berita_terkait">
                    <div class='row-fluid'>
                        <?php 
                        foreach ($data['beritaTerkait'] as $key => $value) {
                            $addClass = $key == 0 ? 'small_gap' : '';
                        ?>    
                        <div class="span12" style="margin-left: -7px;margin-top: 10px;">
                            <div class="berita_text">
                                <a href="<?php echo $value['link']?>">
                                    <?php echo substr($value['shortText'],0,200).'...';?></a>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                    </div>

                </div>
                
            </div>
        </div>

    </div>  
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
})

</script>



