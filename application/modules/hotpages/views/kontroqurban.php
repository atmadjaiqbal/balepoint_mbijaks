<?php
$data = [
    'NARASI'=>[
        'title'=>'KONTROVERSI LARANGAN KURBAN DI JAKARTA',
        'narasi'=>'<img src="http://media.suara.com/thumbnail/650x365/images/2014/10/04/Idul-Adha.jpg?watermark=true" class="pic"><p>Gubernur DKI Jakarta Basuki Tjahaja Purnama (Ahok) mengakui dahulu Pemerintah Provinsi (Pemprov) DKI sempat mengizinkan penjualan serta pemotongan hewan kurban di sembarang tempat, termasuk pinggir jalan. Hal itu disebabkan karena belum dilakukan tes kesehatan sebelumnya pada hewan kurban.</p>
                   <p>Banyaknya penjual hewan kurban yang berjualan di trotoar menjelang Idul Adha membuat Gubernur Ahok mulai mengatur tempat berjualan mereka. Ahok juga mengatur tempat pemotongan hewan kurban.</p>
                   <p>Mantan politikus Partai Gerindra tersebut menjelaskan, kebijakan itu diinstruksikannya memiliki tujuan untuk menjaga kesehatan masyarakat. Sebab, darah hewan kurban yang dipotong sembarangan di tanah dapat menimbulkan spora yang berbahaya bagi kesehatan.</p>'
    ],

    'PROKONTRA'=>[
        'narasi'=>'<img src="http://assets.kompas.com/data/photo/2015/09/06/175346820150906-103738780x390.JPG" class="pic"><p>Sejak Gubernur DKI Jakarta Basuki Tjahaja Purnama mengeluarkan Ingub Nomor 168 Tahun 2015, pro dan kontra bermunculan. Misalnya, Anggota DPRD dari Fraksi PKS, Nasrullah, bahkan menuding Ahok telah kebablasan karena mengatur cara ibadah umat Islam. Pernyataan Nasrullah ini terkait Instruksi Gubernur yang mengatur tentang Pengendalian, Penampungan dan Pemotongan Hewan.</p>
                   <p>Menurut Nasrullah, kalau Gubernur sudah mengatur-atur masalah ibadah, maka ini akan terjadi konflik. Karena orang ibadah itu di mana saja dan kapan saja, yang penting baik lokasinya, kemudian dibersihkan, tempatnya mencukupi, dan tidak mengganggu warga lainnya.‎</p>
                   <p>Staf Hubungan Masyrakat (Humas) Masjid Istiqlal, Abu Hurairah Abdul Salam, mengingatkan Gubernur Ahok yang melarang penyembelihan hewan kuran di tempat sembarangan.</p>
                   <p>“Berdasarkan Pergub (Peraturan Gubernur), ketika Idul Adha terdapat pengecualian mengenai tempat penyembelihan hewan. Proses penyembelihan boleh dilakukan di luar Rumah Pemotongan Hewan (RPH),” katanya. Kamis (10/9/2015).</p>
                   <p>Tak dijelaskan Pergub mana yang dimaksud, namun lelaki yang akrab disapa Abuh itu menjelaskan, dalam Pergub itu ditetapkan bahwa selama tempat penyembelihan mempermudah dokter dari Suku Dinas Kesehatan untuk melakukan pemeriksaan, maka tempat tersebut boleh digunakan. Namun demikian ia menyarankan agar setiap masjid di Jakarta memiliki RPH sendiri guna mempermudah proses pemantauan hewan kurban dan proses penyembelihannya.</p>'
    ],

    'KESEHATAN'=>[
        'narasi'=>'<img src="http://cdn1-a.production.liputan6.static6.com/medias/743441/big/061998300_1411885177-periksa_hewankurban.jpg" class="pic"><p>Gubernur DKI Jakarta Basuki Tjahaja Purnama alias Ahok yang melarang penjualan sekaligus pemotongan hewan kurban yang digelar di atas trotoar. Beralasan, larangan ini dilakukan karena bisa berdampak buruk terhadap kesehatan masyarakatnya.</p>
                   <p>Untuk mendukung pelarangan tersebut, Ahok membandingkan dengan aturan pemotongan hewan kurban yang berlangsung di negara-negara Islam, terutama Arab Saudi. Dia meyakini, negara yang menjadi tempat tujuan umat Islam melaksanakan rukun Islam kelima itu tidak melaksanakan pemotongan hewan kurban di sembarang tempat.</p>
                   <p>"Itu dulu kenapa diizinkan potong hewan kurban semua di jalan? Dulu ilmu kesehatan belum tahu penyakit-penyakit berbahaya. Contoh kita enggak ngerti kenapa anak kecil habis main di tanah kok tiba-tiba pulang kena penyakit terus meninggal, sekarang kita teliti baru ngerti itu karena kena darah hewan, dia (hewan) punya spora itu baru bisa mati sekian bulan, jadi orang meninggal," kata Ahok di Jakarta, Selasa (7/9).</p>
                   <p>Untuk mengantisipasi hal itu, Ahok mengaku telah menginstruksikan bawahannya untuk menyiapkan Rumah Pemotongan Hewan (RPH) sebagai lokasi pemotongan resmi. Seluruh pedagang yang akan menjual hewan kurban juga di tempat-tempat yang sesuai dengan peruntukannya.</p>'
    ],

    'INTRUKSI'=>[
        'narasi'=>'<img src="http://mirajnews.com/id/wp-content/uploads/sites/3/2015/09/INGUB-DKI-168-660x330.jpg" class="pic"><p>Gubernur Ahok menerbitkan Instruksi Gubernur (Ingub) Nomor 168 Tahun 2015 tentang Pengendalian, Penampungan, dan Pemotongan Hewan. Kebijakan ini dalam rangka menyambut Idul Adha tahun 2015/1436 Hijriah.</p>
                   <img src="http://media.suara.com/thumbnail/650x365/images/2014/10/04/Idul-Adha.jpg?watermark=true" class="pic2"><p>Aturan itu mencantumkan pelarangan penjualan serta pemotongan hewan kurban di pinggir jalan. Selain itu, Pemprov DKI juga melarang pemotongan hewan kurban di sekolah-sekolah. Kemudian hewan-hewan yang akan dijual dan disembelih juga harus dites kesehatan terlebih dahulu.</p>
                   <p>Penjualan hewan kurban akan difokuskan pada lapangan tertentu. Sementara Pemprov DKI mendorong pemotongan hewan kurban di rumah potong hewan (RPH).</p>'
    ],

    'KECAMAN'=>[
        'narasi'=>'<img src="http://manjanik.com/wp-content/uploads/2015/09/Ahok-vs-FPI-Soal-Qurban.jpg" class="pic"><p class="font_kecil">Front Pembela Islam (FPI) menyatakan statement Gubernur DKI Jakarta Basuki Tjahja Purnama (Ahok) tentang sekolah dilarang menyembelih kurban itu sejatinya telah melecehkan umat Islam.</p>
                   <img src="http://cdn.tmpo.co/data/2013/09/29/id_224016/224016_620.jpg" class="pic2"><p class="font_kecil">>Wakil Ketua Umum Dewan Pimpinan Pusat Front Pembela Islam (FPI), KH Ja’far Sidiq mengatakan, sejatinya berqurban baik di masjid dan sekolah sudah menjadi tradisi warga Jakarta. Qurban di sekolah pun memberikan pelajaran terhadap anak-anak di sekolah.</p>
                   <p class="font_kecil">Menurut Kyai Ja’far, Ahok itu sudah tidak pantas lagi di anggap sebagai Gubernur DKI Jakarta lagi. Selain selalu bertutur kata buruk, Ahok juga selalu menyakiti hati rakyatnya. Dia pun tidak pernah sekali pun memberikan pengajaran baik terhadap generasi penerus di Jakarta.</p>'
    ],

    'LANGGAR'=>[
        'narasi'=>'<img src="http://www.hidayatullah.com/files/bfi_thumb/MUI-pus-2yiuqewbwcebnsdu0lr3ls.jpg" class="pic"><p class="font_kecil">“Dalam UUD 1945 disebutkan kalau setiap orang berhak memeluk agamanya masing-masing dan beribadah sesuai dengan agamanya. Kalau Ahok melarang pemotongan hewan kurban di depan masjid sebab masjid bukan RPH, Ahok melanggar undang-undang," kata Wasekjen Majelis Ulama Indonesia (MUI), Tengku Zulkarnain, Jumat (11/9/2015).</p>
                   <img src="http://static.republika.co.id/uploads/images/detailnews/daging-kurban-_141006141024-758.JPG" class="pic2"><p class="font_kecil">Lagi pula, ujar Tengku, selama ini umat Muslim tak pernah memotong hewan kurban di pinggir jalan. Pemotongan hanya dilakukan di halaman masjid, lapangan, ataupun halaman rumah.</p>
                   <p class="font_kecil">"Ahok tak berhak melarang umat Muslim memotong hewan kurban di halaman masjid. Saya tetap akan memotong hewan kurban di depan masjid, kalau berani melarang silakan datang ke masjid saya," kata Tengku.</p>'
    ],

    'TANGGAPAN'=>[
        'narasi'=>'<img src="http://www.rmol.co/images/berita/normal/191908_11121904062015_dprd_dki.jpg" class="pic"><p class="font_kecil">Wakil Ketua DPRD DKI Jakarta Triwisaksana (Sani) berharap Pemprov DKI tidak melakukan penertiban terkait adanya Intruksi Gubernur Nomor 168 Tahun 2015 tentang pemotongan hewan di Rumah Pemotongan Hewan (RPH).</p>
                   <img src="http://www.intelijen.co.id/wp-content/uploads/2014/09/hewan-kurban-sitnurulilmi-595x279.jpg" class="pic2"><p class="font_kecil">Menurut Sani, pelarangan pemotongan hewan ditempat umum seharusnya telebih dahulu dilakukan sosialisai agar warga ibu kota dapat memahami bahaya penyakit yang ditimbulkan dari pemotongan hewan tersebut.</p>
                   <p class="font_kecil">Politisi Partai Keadilan Sejahtera (PKS) ini mengimbau, Pemprov DKI selanjutnya melakukan edukasi terhadap dampak bahaya pemotongan hewan kurban di tempat umum. Ia menilai, edukasi jauh lebih baik dilakukan ketimbang pelarangan.</p>'
    ],

    'QUOTE_PENENTANG'=>[
        ['from'=>'Prof Dr KH Ali Mustafa Yakub MA','jabatan'=>'Imam Besar Masjid Istiqlal','img'=>'https://www.banksinarmas.com/id/syariah/images/aliImam.jpg','url'=>'#','content'=>'““Kalau memang dipotong di RPH (rumah pemotongan hewan) cukup gak menampung hewan yang sebanyak itu?” Prof Dr KH Ali Mustafa Yakub MA - Imam Besar Masjid Istiqlal'],
        ['from'=>'Amir Ma ruf','jabatan'=>'Direkur Lembaga Amil Zakat','img'=>'https://media.licdn.com/mpr/mpr/shrinknp_400_400/p/6/005/094/061/2c2c75f.jpg','url'=>'#','content'=>'"Padahal kurban di kawasan Sekolah Dasar bisa dijadikan media pembelajaran bagi anak usia dini," Amir Ma ruf - Direkur Lembaga Amil Zakat, Infaq dan Sodaqoh PBNU'],
        ['from'=>'Ismail Yusanto','jabatan'=>'Juru Bicara HTI','img'=>'http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2013--12--50688-ismail-yusanto-pks-tak-jauh-beda-dengan-partai-sekuler.jpg','url'=>'#','content'=>'“Gubernur ini tidak paham nilai islam akhirnya tidak bisa berpikir tepat,” Ismail Yusanto - Juru Bicara HTI'],
        ['from'=>'Harry Kurniawan','jabatan'=>'Aktifis Lembaga Bantuan Hukum','img'=>'http://statis.dakwatuna.com/wp-content/uploads/2014/09/Harry-Kurniawan-Skretaris-LBH-Adil-Sejahtera.jpg','url'=>'#','content'=>'“ "Apabila Instruksi ini tetap dilaksanakan akan menjauhkan anak didik dari nilai-nilai religius, khususnya di Sekolah Dasar. Pemotongan hewan kurban adalah rangkaian pelaksanaan ibadah bagi umat islam dalam rangka hari raya Idul Adha," Adil Sejahtera Harry Kurniawan  - Aktifis Lembaga Bantuan Hukum'],
        ['from'=>'Lulung Lunggana','jabatan'=>'Ketua DPW PPP Jakarta','img'=>'http://cdn0-a.production.liputan6.static6.com/medias/822604/big/053234400_1425549157-DPRD_Preskon_4.jpg','url'=>'#','content'=>'“"Kebijakan tersebut jelas merupakan bentuk penghapusan budaya yang sudah lama ada" Lulung Lunggana - Ketua DPW PPP Jakarta.'],
        ['from'=>'Saiful Rachmad Dasuki','jabatan'=>'Ketua Gerakan Pemuda (GP) Ansor','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'#','content'=>'"Kebijakan gubernur ini sangat aneh, pemotongan hewan kurban kan ritual keagamaan yang harus dihormati, bukan malah dilarang," Saiful Rachmad Dasuki - Ketua Gerakan Pemuda (GP) Ansor DKI Jakarta'],
        ['from'=>'Lucky P Sastrawirya','jabatan'=>'Anggota DPRD DKI Jakarta','img'=>'http://www.rmoljakarta.com/images/berita/thumb/thumb_956283_06475329072015_Lucky_P_S.jpg','url'=>'#','content'=>'"Instruksi ini ngawur. Prosesi pemotongan hewan kurban tidak bisa sembarangan, ada tata cara menurut ajaran Islam," Lucky P Sastrawirya - Anggota DPRD DKI Jakarta'],
        ['from'=>'Ardy Purnawan Sani','jabatan'=>'Wakil Ketua Dewan Kota Jakarta Pusat','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'#','content'=>'"Larangan itu sudah terjadi dua kali, sejak era Ahok masih jadi Wagub dan kini Gubernur DKI. Banyak penolakan dari masyarakat, tapi anehnya kok Ahok malah jalan terus?," Ardy Purnawan Sani - Wakil Ketua Dewan Kota Jakarta Pusat'],
        ['from'=>'Prof Dr Hj Tuty Alawiyah','jabatan'=>'Ketua Majelis Ulama Indonesia','img'=>'http://mirajnews.com/id/wp-content/uploads/sites/3/2015/03/Tuty-Alawiyah.jpg.jpg','url'=>'#','content'=>'"Pelaksanaan penyembelihan hewan kurban di dekat masyarakat dilakukan karena tujuan yang jelas, yaitu menjangkau lapisan masyarakat lebih dalam. Karena itu, pelaksanaan penyembelihan yang dilakukan di kalangan masyarakat, seperti masjid dan mushala, tidak perlu dilarang," Prof Dr Hj Tuty Alawiyah -  Ketua Majelis Ulama Indonesia'],
        ['from'=>'Tegar Putuhena','jabatan'=>'Ketua #lawanAhok','img'=>'http://mirajnews.com/id/wp-content/uploads/sites/3/2015/03/Tuty-Alawiyah.jpg.jpg','url'=>'#','content'=>'"kami mendesak Ahok untuk  meminta maaf  dan meralat pernyataannnya di hadapan media dalam waktu 3 x 24 Jam karena tidak sesuai dengan instruksi yang telah ditandatanganinya sendiri," Tegar Putuhena - Ketua #lawanAhok'],
        ['from'=>'Tengku Zulkarnain','jabatan'=>'Wasekjen MUI','img'=>'http://cdn.ar.com/images/stories/2014/03/wakil-ketua-majelis-ulama-indonesia-mui-tengku-zulkarnaen-_130905141032-172.jpg','url'=>'#','content'=>'"Selama ini kami memotong hewan kurban di halaman masjid. Darahnya dibuatkan lubang sendiri agar mengalir ke dalam tanah, tidak dibuang sembarangan," Tengku Zulkarnain - Wasekjen MUI'],
        ['from'=>'Ratna Sarumpaet','jabatan'=>'Aktivis','img'=>'http://img.lensaindonesia.com/uploads/1/2012/11/Ratna-Sarumpaet-Soal-Lady-Gaga.jpg','url'=>'#','content'=>'“Kualitas moral, kepemimpinanya dan cintanya ke Indonesia hangus. Seperti memotong kurban di jalan, dia bilang dia gubernur bersih tapi kemudian melarang potong kurban, dia sombong, nggak mau belajar dari kultur (budaya) orang Jakarta,” Ratna Sarumpaet - Aktivis'],
        ['from'=>'Rahmat HS','jabatan'=>'Tokoh Pemuda Tanah Abang','img'=>'http://www.teropongsenayan.com/foto_berita/201503/04/medium_51Rahmat%20HS%20(emka).jpg','url'=>'#','content'=>'"Ini sangat sensitif dan tidak semestinya Ahok masuk ke ranah yang tidak dipahaminya. Karena bisa menimbulkan keresahan umat," Rahmat HS - Tokoh Pemuda Tanah Abang'],
    ],

    'VIDEO'=>[
        ['id'=>'96tP2NeZqyI'],
        ['id'=>'IXxtvx8jXUA'],
        ['id'=>'zy2XjTRqDIo'],
        ['id'=>'t6sQbHlrpn0'],
        ['id'=>'OT0nY8AMqu0'],
        ['id'=>'_6a-HUI907g'],
        ['id'=>'Lk8WNULBPbc'],
        ['id'=>'8pvzrh6J4GY'],
        ['id'=>'hAdluFV9akI'],
        ['id'=>'RdXPRTtStAg'],
        ['id'=>'GnBkqkfK5h8'],
        ['id'=>'21YToVCOQYY'],
        ['id'=>'Ml1ogXblTy0'],
        ['id'=>'J2QpaFKsL0s'],
        ['id'=>'soNQGPipVIE'],
        ['id'=>'BWNou8_mUB4']
    ],

    'FOTO'=>[
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/larangan-berjualan-hewan-kurban-di-jalur-hijau-trotoar-taman-_150908133151-532.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/20141007_151720_pemotongan-hewan-kurban.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/09/08/105501520150902-133028780x390.JPG'],
        ['img'=>'http://www.suara-islam.com/images/berita/ahok_20130726_074823.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/09/25/296692/l4R3QSuEvy.jpg?w=668'],
        ['img'=>'http://2.bp.blogspot.com/-purpJq6J4R4/VCIIN_OVEQI/AAAAAAAAzxE/xmFKFVz9Lcw/s1600/hewan%2Bkurban.jpg'],
        ['img'=>'http://statis.dakwatuna.com/wp-content/uploads/2015/09/AHok-NTT-Sapi-660x371.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/daging-kurban-_141006141024-758.JPG'],
        ['img'=>'https://kabarislam.files.wordpress.com/2014/09/hewan-kurban.jpg?w=468'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/09/06/175346820150906-103738780x390.JPG'],
        ['img'=>'http://media.suara.com/thumbnail/650x365/images/2014/09/17/rs20140916012-e1410921683738.jpg?watermark=true'],
        ['img'=>'http://images.detik.com/customthumb/2014/11/12/10/105826_wawancaraahok5.jpg?w=780&q=90'],
        ['img'=>'http://cdn-2.tstatic.net/wartakota/foto/bank/images/20140930-wakil-wali-kota-bekasi-ahmad-syaikhu-dan-sapi.jpg'],
        ['img'=>'http://baranews.co/system/application/views/main-web/foto_news/ori/895540490-potong_kurban.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/sejumlah-sapi-kurban-dijajakan-di-sebuah-spbu-di-_131003184021-109.jpg'],
        ['img'=>'http://poskotanews.com/cms/wp-content/uploads/2014/09/ilusahok4.jpg'],
        ['img'=>'http://4.bp.blogspot.com/-tqO_2ao7Qwc/VCPeR3XRneI/AAAAAAAACjc/myoXh2YdRxM/s1600/domba%2Bkurban.jpg'],
        ['img'=>'http://cdn.ansideng.com/dynamic/article/2015/09/08/21012/83o85xVZnO.jpg?w=630'],
        ['img'=>'http://media.suara.com/thumbnail/650x365/images/2014/10/04/Idul-Adha.jpg?watermark=true'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/969826/big/025791700_1440751453-ahok_jaksel.jpg'],
        ['img'=>'http://www.voa-islam.com/photos3/abuvakha/jual_hewan_kurban_tanabang.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/detailnews/petugas-suku-dinas-pertanian-dan-peternakan-jakarta-pusat-melakukan-_150910164611-906.jpg'],
        ['img'=>'http://cdn.ar.com/images/_t/500x0/stories/2013/09/sembelih-kurban.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/09/29/298255/aIG5Byf8n5.jpg?w=668'],
        ['img'=>'http://cdn.ar.com/images/_t/500x0/stories/2013/10/potong-hewan-kurban-1.jpg'],
        ['img'=>'http://cdn.ar.com/images/_t/500x0/stories/2013/10/potong-hewan-kurban-1.jpg'],
        ['img'=>'http://1.bp.blogspot.com/-3mE8hxBMly4/VCIqav6HX7I/AAAAAAAABBM/l6KU7AalkL8/s1600/Hewan%2BKurban.jpg'],
        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/15542/big/hewan-kurban-131014b.jpg'],
        ['img'=>'http://media.suara.com/thumbnail/650x365/images/2015/09/09/o_19uojl90t18r95k91ded16961n58a.jpg?watermark=true'],
        ['img'=>'http://media.viva.co.id/thumbs2/2013/10/15/225935_pemotongan-hewan-kurban_663_382.jpg']
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/kontroqurban/top.jpg");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555;
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

    .gallery li {
        display: block;
        float: left;
        height: 50px;
        margin-bottom: 7px;
        margin-right: 0px;
        width: 25%;
        overflow: hidden;
    }
    .gallery li a {
        height: 100px;
        width: 100px;
    }
    .gallery li a img {
        max-width: 97%;
    }

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
<div class="col-xs-12 col-sm-12" id="gaza">
<img src="<?php echo base_url("assets/images/hotpages/kontroqurban/top.jpg")?>" style="width: 100%;height: auto;">
<div class="panel-collapse collapse in">
    <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PRO-KONTRA</span></h5>
<div id="accordion" class="panel-group">
    <div class="panel-collapse collapse in" style="margin-top:10px;">
        <p style="text-align: justify;"><?php echo $data['PROKONTRA']['narasi'];?></p>
    </div>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">ALASAN DEMI KESEHATAN</span></h5>
<div id="accordion" class="panel-group">
    <div class="panel-collapse collapse in" style="margin-top:10px;">
        <p style="text-align: justify;"><?php echo $data['KESEHATAN']['narasi'];?></p>
    </div>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">INTRUKSI GUBERNUR</span></h5>
<div id="accordion" class="panel-group">
    <div class="panel-collapse collapse in" style="margin-top:10px;">
        <p style="text-align: justify;"><?php echo $data['INTRUKSI']['narasi'];?></p>
    </div>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KECAMAN FPI</span></h5>
<div id="accordion" class="panel-group">
    <div class="panel-collapse collapse in" style="margin-top:10px;">
        <p style="text-align: justify;"><?php echo $data['KECAMAN']['narasi'];?></p>
    </div>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">MUI ANGGAP AHOK LANGGAR UUD 1945</span></h5>
<div id="accordion" class="panel-group">
    <div class="panel-collapse collapse in" style="margin-top:10px;">
        <p style="text-align: justify;"><?php echo $data['LANGGAR']['narasi'];?></p>
    </div>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">TANGGAPAN DPRD DKI JAKARTA</span></h5>
<div id="accordion" class="panel-group">
    <div class="panel-collapse collapse in" style="margin-top:10px;">
        <p style="text-align: justify;"><?php echo $data['TANGGAPAN']['narasi'];?></p>
    </div>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">TENTANG IDUL ADHA</span></h5>
<div id="accordion" class="panel-group">
    <div class="panel-collapse collapse in" style="margin-top: 10px;">
        <img src="https://arsiparmansyah.files.wordpress.com/2013/10/ilustrasi-nb-ibrahim-ismail.jpg" style="width: 50%;margin: 0 auto;display: block;">
        <p style="text-align: justify;">Idul Adha di Republik Indonesia, dikenal sebagai Hari Raya Haji, adalah sebuah hari raya Islam. Pada hari ini diperingati peristiwa kurban, yaitu ketika Nabi Ibrahim (Abraham), yang bersedia untuk mengorbankan putranya Ismail untuk Allah, akan mengorbankan putranya Ismail, kemudian digantikan oleh-Nya dengan domba.</p>
        <p style="text-align: justify;"><img src="http://cdn0-a.production.liputan6.static6.com/medias/664552/big/tawaf02--anri+syaiful.jpg" style="width: 50%;margin: 0 auto;display: block;">Pada hari raya ini, umat Islam berkumpul pada pagi hari dan melakukan salat Ied bersama-sama di tanah lapang atau di masjid, seperti ketika merayakan Idul Fitri. Setelah salat, dilakukan penyembelihan hewan kurban, untuk memperingati perintah Allah kepada Nabi Ibrahim yang menyembelih domba sebagai pengganti putranya.</p>
        <p style="text-align: justify;">Hari Raya Idul Adha jatuh pada tanggal 10 bulan Dzulhijjah, hari ini jatuh persis 70 hari setelah perayaan Idul Fitri. Hari ini juga beserta hari-hari Tasyrik diharamkan puasa bagi umat Islam.</p>
        <p style="text-align: justify;">Pusat perayaan Idul Adha adalah sebuah desa kecil di Arab Saudi yang bernama Mina, dekat Mekkah. Di sini ada tiga tiang batu yang melambangkan Iblis dan harus dilempari batu oleh umat Muslim yang sedang naik Haji.</p>
        <p style="text-align: justify;">Hari Idul Adha adalah puncaknya ibadah Haji yang dilaksanakan umat Muslim. Terkadang Idul Adha disebut pula sebagai Idul Qurban atau Lebaran Haji.</p>
    </div>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PENETAPAN IDUL ADHA</span></h5>
<div id="accordion" class="panel-group">
    <div class="panel-collapse collapse in" style="margin-top: 10px;">
        <img src="http://static.republika.co.id/uploads/images/detailnews/jamaah-haji-wukuf-di-padang-arafah-makkah-arab-saudi-_140912095033-724.jpg"  style="width: 50%;margin: 0 auto;display: block;"><br>
        <p style="text-align: justify;">Umat Islam meyakini, bahwa pilar dan inti dari ibadah Haji adalah wukuf di Arafah, sementara Hari Arafah itu sendiri adalah hari ketika jamaah haji di tanah suci sedang melakukan wukuf di Arafah, sebagaimana sabda Nabi saw. :</p>
        <p style="text-align: justify;">"Ibadah haji adalah (wukuf) di Arafah." HR At Tirmidzi, Ibnu Majah, Al Baihaqi, ad Daruquthni, Ahmad, dan al Hakim. Al Hakim berkomentar, “Hadits ini sahih, sekalipun dia berdua [Bukhari-Muslim] tidak mengeluarkannya”.</p>
        <p style="text-align: justify;">Dalam hadits yang dituturkan oleh Husain bin al-Harits al-Jadali berkata, bahwa amir Makkah pernah menyampaikan khutbah, kemudian berkata :</p>
        <p style="text-align: justify;">"Rasulullah saw. telah berpesan kepada kami agar kami menunaikan ibadah haji berdasarkan ru’yat (hilal Dzulhijjah). Jika kami tidak bisa menyaksikannya, kemudian ada dua saksi adil (yang menyaksikannya), maka kami harus mengerjakan manasik berdasarkan kesaksian mereka." HR Abu Dawud, al Baihaqi dan ad Daruquthni. Ad Daruquthni berkomentar, “Hadits ini isnadnya bersambung, dan sahih.”</p>
        <p style="text-align: justify;">Hadits ini menjelaskan: Pertama, bahwa pelaksanaan ibadah haji harus didasarkan pada hasil ru’yat hilal 1 Dzulhijjah, sehingga kapan wukuf dan Idul Adhanya bisa ditetapkan. Kedua, pesan Nabi kepada amir Makkah, sebagai penguasa wilayah, tempat di mana perhelatan haji dilaksanakan untuk melakukan ru’yat; jika tidak berhasil, maka ru’yat orang lain, yang menyatakan kesaksiannya kepada amir Makkah.</p>
        <img src="http://2.bp.blogspot.com/-qD54cPYYP3c/VGBGXBFkk2I/AAAAAAAABck/N9y8T4p-b6M/s1600/Kalender%2BSeptember%2B2015.png"  style="width: 50%;margin: 0 auto;display: block;"><br>
    </div>
</div>
<div class="clear"></div>

<div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
    <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://bptsp.jakarta.go.id/logo_dki.png" data-toggle="tooltip" data-original-title="PEMPROV DKI JAKARTA"/><p>PEMPROV DKI JAKARTA</p></a>
</div>
<div class="clear"></div>

<div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
    <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://4.bp.blogspot.com/-kVfvrWrUgsU/VRvZS2wb5gI/AAAAAAAAAEE/2WikG6tgBPI/s1600/logo%2Bmui.png" data-toggle="tooltip" data-original-title="MUI"/><p>MUI</p></a>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://1.bp.blogspot.com/-bE1R-B3go1E/UCYkigD6zWI/AAAAAAAABBY/xMe9GHn5pxk/s1600/FPI.png" data-toggle="tooltip" data-original-title="FPI"/><p>FPI</p></a>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="https://justnurman.files.wordpress.com/2010/04/logo-nu.jpg" data-toggle="tooltip" data-original-title="PBNU"/><p>PBNU</p></a>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://img1.eramuslim.com/fckfiles/image/info_umat/htilogo.jpg" data-toggle="tooltip" data-original-title="HTI (Hizbut Tahrir Indonesia)"/><p>HTI (Hizbut Tahrir Indonesia)</p></a>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://lbh-semarang.or.id/wp-content/uploads/2014/10/Logo-LBH-SMG-2007_1.jpg" data-toggle="tooltip" data-original-title="LBH (Lembaga Bantuan Hukum)"/><p>LBH (Lembaga Bantuan Hukum)</p></a>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://2.bp.blogspot.com/-KmFn3XbqR24/TZdX7b0DgHI/AAAAAAAAAA0/lYumr90kT0U/s760/logo%2Bfbr.jpeg" data-toggle="tooltip" data-original-title="FBR (Forum Betawi Rempug)"/><p>FBR (Forum Betawi Rempug)</p></a>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://lh5.googleusercontent.com/-Ftcze-QoIiI/AAAAAAAAAAI/AAAAAAAAAE4/vX5IC-SgeI0/photo.jpg" data-toggle="tooltip" data-original-title="GP Ansor Jakarta"/><p>GP Ansor Jakarta</p></a>
    <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://4.bp.blogspot.com/_L64nMBrys-g/Sm6qHZyO6jI/AAAAAAAAAI8/x7Xk6jA1GTA/s400/IKADI.jpg" data-toggle="tooltip" data-original-title="Ikatan Dai Indonesia (Ikatan Dai Indonesia)"/><p>Ikatan Dai Indonesia (Ikatan Dai Indonesia)</p></a>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
<div id="accordion" class="panel-group row">
    <?php
    foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
        <div class="panel-collapse collapse in col-xs-6">
            <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
            <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
            </p>
            <div class="clear"></div>
            <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
            <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
        </div>
    <?php
    }
    ?>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
<div id="accordion" class="panel-group">
    <ul id="light-gallery" class="gallery">
        <?php
        foreach($data['FOTO'] as $key=>$val){
            ?>
            <li data-src="<?php echo $val['img'];?>">
                <a href="#">
                    <img src="<?php echo $val['img'];?>" />
                </a>
            </li>
        <?php
        }
        ?>
    </ul>
</div>
<div class="clear"></div>

<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
<div id="accordion" class="panel-group row">
    <?php
    foreach($data['VIDEO'] as $key=>$val){
        ?>
        <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
    <?php
    }
    ?>
</div>

<div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>