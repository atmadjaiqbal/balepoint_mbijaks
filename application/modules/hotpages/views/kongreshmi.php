<style>
    .pic{
        width: 100% !important;
    }
    .pic2{
        width: 100% !important;
    }
    .picprofil{
        width: 100% !important;
    }
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/kongreshmi/top.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/kongreshmi/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['block1']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block3']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block3']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['right1']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['right1']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block4']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block4']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block5']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block5']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block6']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block6']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block7']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block7']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <?php /*
        <?php if(!empty($data['institusiPendukung'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">INSTITUSI TERLIBAT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['institusiPendukung'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                    <a href="<?php echo $val['url'];?>"><img src="<?php echo $val['image'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>
        
       

       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">NEGARA PROTES</span></h5>
        <div id="accordion" class="panel-group row">
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;">
                    <a href="http://www.bijaks.net/aktor/profile/suriah5371b98429bfd">
                        <img src="http://1.bp.blogspot.com/-rZjXBqYvrsA/UBFQOkyOSmI/AAAAAAAAANI/T1OBu1ZcvgA/s1600/gambar-bendera-syria.JPG" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;">
                        SURIAH</b><br>
                    <div class="clear"></div>
                    </p>
                </div>
        </div>
        <div class="clear"></div>
        */?>



        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block9']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block9']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block10']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block10']['narasi'];?>  
            </div>
        </div>

        <div class="clear"></div>

        <?php if(!empty($data['quotePendukung'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePendukung'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>

        <?php if(!empty($data['quoteProtes'])){?>
       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PROTES</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quoteProtes'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>">
                    <img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 15px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
        <?php };?>



		<h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['foto'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['video'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>
<?php /*
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['berita'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>
*/ ?>
        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>
