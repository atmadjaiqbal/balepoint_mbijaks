<?php 
$data = [
    'NARASI'=>[
        'title'=>'KONFERENSI ASIA AFRIKA',
        'narasi'=>'<img src="http://upload.wikimedia.org/wikipedia/commons/0/01/Gedung.Merdeka.jpg" class="pic">Konferensi Tingkat Tinggi Asia–Afrika (disingkat KTT Asia Afrika atau KAA, kadang juga disebut Konferensi Bandung) adalah sebuah konferensi antara negara-negara Asia dan Afrika, yang kebanyakan baru saja memperoleh kemerdekaan. KAA diselenggarakan oleh Indonesia, Myanmar (dahulu Burma), Sri Lanka (dahulu Ceylon), India dan Pakistan dan dikoordinasi oleh Menteri Luar Negeri Indonesia Sunario. Pertemuan ini berlangsung antara 18 April-24 April 1955, di Gedung Merdeka, Bandung, Indonesia dengan tujuan mempromosikan kerjasama ekonomi dan kebudayaan Asia-Afrika dan melawan kolonialisme atau neokolonialisme Amerika Serikat, Uni Soviet, atau negara imperialis lainnya.
                    </p><p style="text-align: justify;">Sebanyak 29 negara yang mewakili lebih dari setengah total penduduk dunia pada saat itu mengirimkan wakilnya. Konferensi ini merefleksikan apa yang mereka pandang sebagai ketidakinginan kekuatan-kekuatan Barat untuk mengkonsultasikan dengan mereka tentang keputusan-keputusan yang memengaruhi Asia pada masa Perang Dingin,<img src="http://citizenmagz.com/wp-content/uploads/2011/04/DSC02528.jpg" class="pic2"> kekhawatiran mereka mengenai ketegangan antara Republik Rakyat Tiongkok dan Amerika Serikat, keinginan mereka untuk membentangkan fondasi bagi hubungan yang damai antara Tiongkok dengan mereka dan pihak Barat, penentangan mereka terhadap kolonialisme, khususnya pengaruh Perancis di Afrika Utara dan kekuasaan kolonial perancis di Aljazair, dan keinginan Indonesia untuk mempromosikan hak mereka dalam pertentangan dengan Belanda mengenai Irian Barat.
                    </p><p style="text-align: justify;">Sepuluh poin hasil pertemuan ini kemudian tertuang dalam apa yang disebut Dasasila Bandung, yang berisi tentang "pernyataan mengenai dukungan bagi kerusuhan dan kerjasama dunia". Dasasila Bandung ini memasukkan prinsip-prinsip dalam Piagam PBB dan prinsip-prinsip Nehru.
                    </p><p style="text-align: justify;">Konferensi ini akhirnya membawa kepada terbentuknya Gerakan Non-Blok pada 1961.'
    ],
    'NARASI2'=>[
        'title'=>'Konferensi Asia Afrika 1955',
        'narasi'=>'<img src="http://bdg.co.id/wp-content/uploads/2014/07/MKAA-6.jpg" class="pic">Konferensi Asia Afrika tahun 1955 menjadi tonggak penting dalam sejarah bangsa-bangsa Asia dan Afrika. Para delegasi yang berasal dari 29 negara peserta konferensi berkumpul di Bandung, Indonesia untuk membahas perdamaian, keamanan, dan pembangunan ekonomi di tengah-tengah berbagai masalah yang muncul di berbagai belahan dunia.
                    </p><p style="text-align: justify;">Sebagian besar permasalahan tersebut disebabkan oleh dua blok yang memiliki kepentingan yang berbeda dan bertentangan ideologi. Kedua blok tersebut adalah Blok Barat dan Blok Timur. Masing-masing blok berusaha memperoleh dukungan dari negara-negara di Asia dan Afrika, yang dikenal dengan istilah "Perang Dingin". Situasi dunia, khususnya di benua Asia dan Afrika juga dipengaruhi oleh berbagai bentuk kolonialisme. Selain itu, beberapa negara Asia dan Afrika mengalami konflik yang muncul sebagai akibat dari kolonialisme dan politik divide et impera. Pada saat itu, PBB tidak mampu menangani permasalahan tersebut.
                    </p><p style="text-align: justify;"><img src="http://www.kemlu.go.id/Common/ImageHandler.ashx?fileurl=/OthersPictures/Museum%20Konferensi%20Asia%20Afrika/diorama%20room2.jpg&w=450&h=450" class="pic2">Hal-hal inilah yang menjadi alasan utama bagi pemerintah Burma (Myanmar), India, Indonesia, Pakistan, dan Sailan (Sri Lanka) untuk menyelenggarakan Konferensi Asia Afrika (KAA). Kelima negara ini mengajak negara-negara lain di Asia, Afrika, dan Timur Tengah untuk menciptakan etos baru hubungan antara bangsa-bangsa, yang disebut “Bandung Spirit”. Para pemimpin di KAA juga mendeklarasikan “Dasasila Bandung” yang mencerminkan komitmen bangsa-bangsa untuk mempraktekkan toleransi dan kedamaian hidup satu sama lain sebagai tetangga yang baik.',
        'title2'=>'Kemitraan Strategis Baru Asia Afrika 2005',
        'narasi2'=>'<img src="http://static.republika.co.id/uploads/images/detailnews/para-peserta-konferensi-asia-afrika-menuju-gedung-merdeka-jl-_130418081050-110.jpg" class="pic">Lima puluh tahun setelah konferensi, Indonesia berhasil menjadi tuan rumah Peringatan 50 tahun Konferensi Asia-Afrika. Bandung Spirit kembali dihidupkan dan berbagai rencana disusun untuk menjalin kerja sama antara dua benua. Seluruh peserta yang berkumpul pada tanggal 22 - 24 April 2005 di Jakarta dan Bandung meyakini bahwa Bandung Spirit senantiasa menjadi dasar yang kokoh untuk memelihara hubungan yang lebih baik di antara bangsa-bangsa Asia dan Afrika serta untuk menyelesaikan isu-isu global. Peringatan KAA tersebut mengarah pada penciptaan Kemitraan Strategis Baru Asia Afrika (NAASP).
                    </p><p style="text-align: justify;">Secara resmi ditandatangani oleh Indonesia dan Afrika Selatan—sebagai tuan rumah bersama KTT—Deklarasi NAASP berfungsi sebagai cetak biru bagi kolaborasi kedua benua dalam memerangi kemiskinan dan keterbelakangan yang dianggap sebagai masalah utama di Asia dan Afrika. Kesepakatan ini ditujukan untuk memperkuat multilateralisme, mencapai pertumbuhan ekonomi, meningkatkan perdamaian dan keamanan global, dan mengupayakan jalur pertumbuhan berkelanjutan antara kedua kawasan. Selain itu, para pemimpin yang hadir juga mengesahkan dokumen capaian mengenai pengentasan kemiskinan, terorisme, senjata pemusnah massal, dan pengembangan sistem peringatan dini tsunami.',
        'title3'=>'Peringatan ke-60 Konferensi Asia Afrika dan Peringatan ke-10 Kemitraan Strategis Baru Asia Afrika',
        'narasi3'=>'<img src="http://pojoksatu.id/wp-content/uploads/2015/04/kaa3.jpg" class="pic2">Dalam Peringatan ke-60 Konferensi Asia Afrika dan Peringatan ke-10 Kemitraan Strategis Baru Asia Afrika (NAASP), Pemerintah Republik Indonesia akan menjadi tuan rumah serangkaian acara tingkat tinggi dengan tema "Penguatan Kerja sama Selatan-Selatan dalam Rangka Meningkatkan Kesejahteraan dan Perdamaian Dunia" di Jakarta dan Bandung pada 19 - 24 April 2015. Sebanyak 109 negara Asia dan Afrika, 16 negara pengamat dan 25 organisasi internasional diundang untuk berpartisipasi dalam acara penting ini.
                    </p><p style="text-align: justify;">Forum ini bertujuan untuk menjembatani negara-negara Asia dan Afrika dalam mengejar kemitraan yang lebih kuat dan sarana berbagi pengalaman dalam meningkatkan pembangunan ekonomi kedua kawasan. Forum ini juga menjadi kesempatan untuk membahas solusi dan cara mengatasi tantangan bersama melalui penguatan kerja sama Selatan-Selatan.
                    </p><p style="text-align: justify;"><img src="http://blog.butikdukomsel.com/wp-content/uploads/antarafoto-KAA180455-3col.jpg" class="pic">Rangkaian pertemuan ini akan diawali dengan pertemuan tingkat pejabat tinggi (Senior Official Meeting) pada tanggal 19 April 2015, diikuti oleh pertemuan tingkat menteri (Ministerial Meeting) pada 20 April dan pertemuan tingkat kepala negara (Leaders Meeting) pada 22 - 23 April 2015. Selain itu, Asia-Africa Business Summit akan diselenggarakan pada 21 - 22 April di Jakarta sebagai acara pendamping. Peringatan KAA yang ke-60 akan diselenggarakan di Bandung pada tanggal 24 April 2015.
                    </p><p style="text-align: justify;">Konferensi Asia Afrika 2015 bertujuan untuk menyimpulkan tiga dokumen keluaran yaitu Bandung Message, dokumen tentang penghidupan kembali NAASP, dan deklarasi mengenai dukungan negara-negara Asia-Afrika untuk Palestina. (Siti Chodijah, Filmon Warouw)'
    ],
    'KILAS'=>[
        ['thn'=>'18 – 24 April 1955','text'=>'Konferensi Asia-Afrika berlangsung di Gedung Merdeka, Bandung. Persidangan ini diresmikan oleh Presiden Soekarno dan diketuai oleh PM Ali Sastroamidjojo. Hasil dari persidangan ini berupa persetujuan yang dikenal dengan Dasasila Bandung.' ],
        ['thn'=>'28 – 29 Desember 1954','text'=>'Untuk mematangkan gagasan masalah Persidangan Asia-Afrika, diadakan Persidangan Bogor. Dalam persidangan ini dirumuskan lebih rinci tentang tujuan persidangan, serta siapa saja yang akan diundang.' ],
        ['thn'=>'25 April – 2 Mei 1954','text'=>'Berlangsung Persidangan Kolombo di Sri Lanka. Hadir dalam pertemuan tersebut para pemimpin dari India, Pakistan, Burma (sekarang Myanmar), dan Indonesia. Dalam konferensi ini Indonesia memberikan usulan perlunya adanya Konferensi Asia-Afrika.' ],
        ['thn'=>'23 Agustus 1953','text'=>'Perdana Menteri Ali Sastroamidjojo (Indonesia) di Dewan Perwakilan Rakyat Sementara mengusulkan perlunya kerjasama antara negara-negara di Asia dan Afrika dalam perdamaian dunia.' ]
    ],
    'PELOPOR'=>[
        ['tokoh'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Ali_Sastroamidjojo.jpg/170px-Ali_Sastroamidjojo.jpg','bendera'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Flag_of_Indonesia.svg/800px-Flag_of_Indonesia.svg.png','text'=>'Ali Sastroamidjojo' ],
        ['tokoh'=>'http://www.bijaks.net/assets/images/hotpages/kaa/bogra.jpg','bendera'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Flag_of_Pakistan.svg/22px-Flag_of_Pakistan.svg.png','text'=>'Mohammad Ali Bogra' ],
        ['tokoh'=>'http://www.bijaks.net/assets/images/hotpages/kaa/Nehru.jpg','bendera'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_India.svg/22px-Flag_of_India.svg.png','text'=>'Jawaharlal Nehru' ],
        ['tokoh'=>'http://www.bijaks.net/assets/images/hotpages/kaa/Sir.jpg','bendera'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Flag_of_Sri_Lanka.svg/22px-Flag_of_Sri_Lanka.svg.png','text'=>'Sir John Kotelawala' ],
        ['tokoh'=>'http://www.bijaks.net/assets/images/hotpages/kaa/U_Nu.jpg','bendera'=>'http://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Flag_of_Myanmar.svg/22px-Flag_of_Myanmar.svg.png','text'=>'U Nu' ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef']
        ]
    ],
    'KRONOLOGI'=>[
        ['date'=>'24 November 2014','content'=>'Senin (24/11/2015) para pengurus partai menggelar rapat pleno dengan agenda persiapan  Munas IX digelar di Kantor DPP Partai Golkar, Slipi Jakarta.'],
        ['date'=>'25 November 2014','content'=>'Sehari setelahnya, yakni selasa (25/11/2014) dua kelompok massa yakni Angkatan Muda Partai Golkar (AMPG) yang dipimpin oleh Yorrys Raweyai bentrok dengan massa AMPG pimpinan Ahmad Doli yang mengklaim diri sebagai AMPG resmi di kantor DPP Partai Golkar, Jalan Anggrek Nelly Murni XI-A, Slipi, Jakarta.'],
        ['date'=>'30 November - 3 Desember 2014','content'=>'Munas IX Partai Golkar digelar di Nusa Dua, Bali. Hasil Munas menetapkan Aburizal Bakrie sebagai ketua umum terpilih.'],
        ['date'=>'04 Desember 2014','content'=>'Kamis (4/12/2014 Ketua Umum Aburizal Bakrie akhirnya mengumumkan susunan kabinet kepengurusan DPP Golkar periode 2014-2019 hasil Munas Bali.'],
        ['date'=>'06 Desember 2014','content'=>'Sabtu (6/12/2014) Musyawarah Nasional IX Partai Golkar tandingan yang digelar kubu Agung Laksono cs resmi dibuka,  di Ballroom Hotel Mercure, Ancol, Jakarta'],
        ['date'=>'03 Maret 2015','content'=>'Selasa (3/3/2015), Mahkamah Partai Golkar menyatakan kepengurusan hasil Munas Ancol Jakarta dinyatakan sah.'],
        ['date'=>'10 Maret 2015','content'=>'Menteri Hukum dan Ham Yasonna Laoly Yasonna secara resmi mengesahkan kepemimpinan Golkar versi Munas Ancol dengan Ketua Umum Agung Laksono.'],
        ['date'=>'23 Maret 2015','content'=>'Senin (23/3/2015) Siang, Yusril Ihza Mahendra selaku kuasa hukum partai Golkar mengajukan gugatan Surat Keputusan (SK) Menteri Hukum dan Ham (Menkumham) di Pengadilan Tata Usaha Negara Jakarta (PTUN).'],
        ['date'=>'25 Maret 2015','content'=>'Kubu Agung Laksono melayangkan surat peringatan pengosongan kantor ketua dan sekretaris Fraksi Golkar di komplek DPR RI.'],
        ['date'=>'27 Maret 2015','content'=>'Wakil Pimpinan Golkar dari kubu Agung Laksono, Yorrys Raweyai mendatangi kantor sekretariat Fraksi Golkar untuk mengambil alih kantor tersebut.'],
    ],
    'PESERTA'=>[
        ['negara'=>'Afganistan','img'=>'http://upload.wikimedia.org/wikipedia/commons/9/9a/Flag_of_Afghanistan.svg'],
        ['negara'=>'Arab Saudi','img'=>'http://upload.wikimedia.org/wikipedia/commons/0/0d/Flag_of_Saudi_Arabia.svg'],
        ['negara'=>'Burma','img'=>'http://upload.wikimedia.org/wikipedia/commons/8/8c/Flag_of_Myanmar.svg'],
        ['negara'=>'Ceylon','img'=>'http://upload.wikimedia.org/wikipedia/commons/1/11/Flag_of_Sri_Lanka.svg'],
        ['negara'=>'India','img'=>'http://upload.wikimedia.org/wikipedia/commons/4/41/Flag_of_India.svg'],
        ['negara'=>'Republik Rakyat Tiongkok','img'=>'http://upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_the_People%27s_Republic_of_China.svg'],
        ['negara'=>'Ethiopia','img'=>'http://upload.wikimedia.org/wikipedia/commons/7/71/Flag_of_Ethiopia.svg'],
        ['negara'=>'Indonesia','img'=>'http://upload.wikimedia.org/wikipedia/commons/9/9f/Flag_of_Indonesia.svg'],
        ['negara'=>'Irak','img'=>'http://upload.wikimedia.org/wikipedia/commons/f/f6/Flag_of_Iraq.svg'],
        ['negara'=>'Iran','img'=>'http://upload.wikimedia.org/wikipedia/commons/c/ca/Flag_of_Iran.svg'],
        ['negara'=>'Kamboja','img'=>'http://upload.wikimedia.org/wikipedia/commons/8/83/Flag_of_Cambodia.svg'],
        ['negara'=>'Jepang','img'=>'http://upload.wikimedia.org/wikipedia/commons/9/9e/Flag_of_Japan.svg'],
        ['negara'=>'Liberia','img'=>'http://upload.wikimedia.org/wikipedia/commons/b/b8/Flag_of_Liberia.svg'],
        ['negara'=>'Laos','img'=>'http://upload.wikimedia.org/wikipedia/commons/5/56/Flag_of_Laos.svg'],
        ['negara'=>'Lebanon','img'=>'http://upload.wikimedia.org/wikipedia/commons/5/59/Flag_of_Lebanon.svg'],
        ['negara'=>'Libya','img'=>'http://upload.wikimedia.org/wikipedia/commons/0/05/Flag_of_Libya.svg'],
        ['negara'=>'Mesir','img'=>'http://upload.wikimedia.org/wikipedia/commons/f/fe/Flag_of_Egypt.svg'],
        ['negara'=>'Nepal','img'=>'http://upload.wikimedia.org/wikipedia/commons/9/9b/Flag_of_Nepal.svg'],
        ['negara'=>'Filipina','img'=>'http://upload.wikimedia.org/wikipedia/commons/9/99/Flag_of_the_Philippines.svg'],
        ['negara'=>'Siprus','img'=>'http://upload.wikimedia.org/wikipedia/commons/d/d4/Flag_of_Cyprus.svg'],
        ['negara'=>'Pakistan','img'=>'http://upload.wikimedia.org/wikipedia/commons/3/32/Flag_of_Pakistan.svg'],
        ['negara'=>'Sudan','img'=>'http://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_Sudan.svg'],
        ['negara'=>'Suriah','img'=>'http://upload.wikimedia.org/wikipedia/commons/5/53/Flag_of_Syria.svg'],
        ['negara'=>'Thailand','img'=>'http://upload.wikimedia.org/wikipedia/commons/a/a9/Flag_of_Thailand.svg'],
        ['negara'=>'Turki','img'=>'http://upload.wikimedia.org/wikipedia/commons/b/b4/Flag_of_Turkey.svg'],
        ['negara'=>'Vietnam Utara','img'=>'http://upload.wikimedia.org/wikipedia/commons/f/fd/Flag_of_North_Vietnam_%281945-1955%29.svg'],
        ['negara'=>'Vietnam Selatan','img'=>'http://upload.wikimedia.org/wikipedia/commons/e/e9/Flag_of_South_Vietnam.svg'],
        ['negara'=>'Kerajaan Mutawakkilīyah Yaman','img'=>'http://upload.wikimedia.org/wikipedia/commons/8/89/Flag_of_Yemen.svg'],
        ['negara'=>'Yordania','img'=>'http://upload.wikimedia.org/wikipedia/commons/c/c0/Flag_of_Jordan.svg']
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef']
        ]
    ],
    'DUKUNGAN'=>[
        'narasi'=>'Dua kubu yang berseteru gencar menggalang dan mencari dukungan. Hal itu tidak hanya untuk memperkuat eksistensi kepengurusan partai, tetapi untuk memetakan kekuatan di Parlemen. Partai beringin di bawah kepengurusan Agung Laksono akan mengubah haluan untuk menjadi partai pendukung pemerintah. Kondisi ini akan berdampak pada berkurangnya kekuatan koalisi oposisi di DPR. Golkar saat ini menjadi pemegang kendali di Koalisi Merah Putih karena menjadi penyokong utama dari segi kuantitas suara di Parlemen.<br><br>Para Elit Pendukung Agung dari Golkar :',
        'satu'=>[
            ['page_id'=>'drshpriyobudisantoso51131c5b8da10'],
            ['page_id'=>'drsagusgumiwangkartasasmita50ee38bcdabca'],
            ['page_id'=>'yorrysraweyai52bbea81c3597'],
            ['page_id'=>'zainuddinamali5456ee91ad531'],
            ['page_id'=>'melchiasmarkusmekeng52a54c5052ca8'],
            ['page_id'=>'leonababan52675bf49f296'],
            ['page_id'=>'sariyuliati52a53db4ed03b']
        ],
        'narasi2'=>'Mereka menempati posisi-posisi strategis di dalam struktur kepengurusan Partai Golkar.<br>Selain dukungan dari elit internal Golkar, Agung Laksono bergerak cepat dengan melakukan safari politik ke elit-elit parpol lain. Setidaknya para ketua umum dan elit partai yang tergabung dalam Koalisi Indonesia Hebat seperti :',
        'dua'=>[
            ['page_id'=>'megawatisoekarnoputri50ee62bce591e'],
            ['page_id'=>'suryapaloh511b4aa507a50'],
            ['page_id'=>'drshmuhammadjusufkalla50ee870b99cc9'],
            ['page_id'=>'hwirantosh50bdcb9a73d2f'],
            ['page_id'=>'letjentnipurnsutiyoso511c2b6deeb46']
        ],
        'narasi3'=>'Mereka adalah barisan elit parpol yang sudah menyatakan komitmen untuk mendukung kepengurusan Agung Laksono sebagai ketua umum Golkar yang sah.<br>KMP Dukung ARB Aburizal Bakrie merapatkan barisan pendukungnya yang ada di KMP. Semua elit partai di KMP berada di belakang Aburizal dan terus memberi dukungan terhadapnya. Mereka adalah:',
        'tiga'=>[
            ['page_id'=>'hmanismattalc50f7ba1b1520d'],
            ['page_id'=>'fahrihamzahse5105e57490d09'],
            ['page_id'=>'fadlizon5119d8091e007'],
            ['page_id'=>'prabowosubiyantodjojohadikusumo50c1598f86d91']
        ],
        'narasi4'=>'Sedangkan dari dalam Golkar sendiri Ical didukung oleh :',
        'empat'=>[
            ['page_id'=>'akbartanjung503c2d29aaf6b'],
            ['page_id'=>'sitihediatihariyadi51b939bf7e379'],
            ['page_id'=>'Ahmadi'],
            ['page_id'=>'drfadelmuhammad51132604d3a90'],
            ['page_id'=>'adekomarudin5280ac60140b0'],
            ['page_id'=>'nurdinhalid530eb1689eb22'],
            ['page_id'=>'drssetyanovanto50f8fd3c666bc'],
            ['page_id'=>'drazizsyamsuddin50f8e5a2bef6b'],
            ['page_id'=>'drstheolsambuaga51886b9786e52'],
            ['page_id'=>'idrusmarham51132ad8423e1'],
            ['page_id'=>'sharifcicipsutarjo50ef9a0c31d50']
        ]
    ],
    'KRITIK'=>[
        'narasi'=>'Sejak terbitnya SK yang mengsahkan kubu Agung Laksono. Menteri Yasona mendapat kritikan yang tajam dari kubu Aburizal Bakrie. Yasono dianggap memihak dalam mengambil kebijakan yang tidak sesuai prosedur hukum yang berlaku. Seharusnya Surat Keputusan dikeluarkan oleh Kemenkumham setelah ada putusan dari PTUN. 
                Selain itu,keputusannya tidak didasarkan pada penilaian yang objektif, namun sarat politis. Keputusan tersebut merupakan keputusan yang ke dua kalinya dikeluarkan oleh Menteri Yasona sejak diangkat jadi menteri. Sebelumnya keputusan serupa dikeluarkan dalam menangani kasus sengketa di Partai Persatuan Pembangunan (PPP).
                Kubu Aburizal melawan keputusan pemerintah ini. Mereka mengugat putusan Menkumham ke pengadilan. Mereka juga melaporkan kubu Agung ke Bareskrim Polri dengan tuduhan pemalsuan dokumen. Selain itu, Koalisi Merah Putih juga akan mengajukan hak angket di DPR. Menteri Yasonna dianggap bekerja atas dasarpolitik.',
        'usulan'=>'Koalisi Merah Putih mengusulkan hak angket DPR untuk meminta penjelasan Menteri Hukum dan HAM yang dinilai tidak independen dalam menyikapi perselisihan di internal Partai Golkar dan Partai Persatuan Pembangunan.
                Usulan tersebut merupakan salah satu upaya perlawanan yang dilakukan oleh kubu Aburizal melalui koalisi pendukungnya di DPR yang tergabung dalam Koalisi Merah Putih (KMP). Setidaknya sudah ada 116 anggota yang menandatangi usulan hak angket. Artinya hak angket yang digulirkan ke Menkumham  itu sudah memenuhi  syarat untuk dibawa ke paripurna, karena sudah ditandatangani 116  anggota DPR.',
        'dalih'=>'Kemenkumham berdalih bahwa putusan yang dikeluarkannya sudah tepat, karena mengacu pada keputusan Mahkamah Partai Golkar yang mengabulkan untuk menerima kepengurusan DPP Partai Golkar hasil Munas Ancol, yakni berdasarkan ketentuan Pasal 32 ayat 5 UU Parpol Nomor 2/201, dinyatakan bahwa putusan MP (Mahkamah Partai Golkar) bersifat final dan mengikat secara internal.'
    ],
    'ANALISA'=>'<img src="http://dennyja-world.com/wp-content/uploads/2015/03/yasonna.jpg" class="pic2">Indikasi adanya intervensi politik yang dilakukan pemerintah terhadap polemik di internal Partai Golkar  menguat setelah Kemenkumham memutuskan sah kepengurusan Agung Laksono. Polemik tersebut merupakan babak tambahan dari kisruh Partai Golkar yang sudah berlangsung lama. Institusi Kementerian Hukum dan Hak Asasi Manusia (Kemenkumham) diduga dijadikan alat oleh oknum-oknum pemerintah dalam mengintervensi internal Golkar untuk memecah dan memperlemah kekuatan koalisi merah putih di parlemen. Seperti yang diketahui Partai Golkar adalah Oposisi di pemerintahan Jokowi-JK, yang tergabung dalam Koalisi Merah Putih (KMP). 
                </p><p>Surat Keputusan (SK) yang dikeluarkan oleh Kementerian Hukum dan Ham, yang mengesahkan kepengurusan Partai Golkar hasil Munas Jakarta merupakan hasil kesepakatan politik yang dilakukan oleh Agung Laksono, Jusuf Kalla, Surya Palloh, dan Megawati. SK tersebut adalah pesanan Megawati ke Menteri Yasona sesaat setelah kontrak politik disepakati, yakni Partai Golkar versi Munas Jakarta bersedia bergabung ke dalam Koalisi Indonesia Hebat.',
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Ari Junaedi','jabatan'=>'Pengamat politik Universitas Indonesia','img'=>'http://sp.beritasatu.com/media/images/original/20141208171806988.jpg','url'=>'','content'=>'"Jika punya cita-cita membesarkan partai maka rekonsiliasi yang dilakukan. Saya rasa ini kepentingan eli-elit yang tidak terakomodir dengan keputusan mahkamah partai."'],
        ['from'=>'Surya paloh','jabatan'=>'','img'=>'http://img2.bisnis.com/makasar/posts/2014/09/26/180862/Surya-Paloh.jpg','url'=>'','content'=>'"Pemimpin Golkar Tak Boleh Tidak Siap Kalah Ketika Bertanding."'],
        ['from'=>'Aziz Syamsuddin','jabatan'=>'','img'=>'http://teropongsenayan.com/foto_berita/201501/05/medium_27Aziz%20Syamsudin%20002.jpg','url'=>'','content'=>'"Sangat suprise ya, tanpa ada pertimbangan Dirjen, Menkumham mengelurkan surat itu."'],
        ['from'=>'Jusuf Kalla','jabatan'=>'','img'=>'http://assets.kompasiana.com/statics/files/1402277267842193257.jpg','url'=>'','content'=>'"Kita harus mentaati keputusan mahkamah partai yang kemudian disahkan oleh Menkumham."'],
        ['from'=>'Agus Gumiwang','jabatan'=>'','img'=>'http://cdn.partainasdemo250.org/uploads/images/1408447172_AGUS_GUMIWANG.jpg','url'=>'','content'=>'"Sudah jelas dan sudah tidak terbantahkan lagi bahwa DPP Partai Golkar yang sah itu adalah DPP pimpinan Pak Agung Laksono karena SK Kemenkumham itu sah."']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Abubakar Al-Habsyi','jabatan'=>'Ketua DPP Partai Keadilan Sejahtera','img'=>'http://www.wartabuana.com/pictures/Abubakar-Alhabsyi-201412181726271.jpg','url'=>'','content'=>'"Tugas pemerintah itu seharusnya hanya sebagai administrator, kalau yang sekarang sudah sampai ke intervensi."'],
        ['from'=>'Idrus Marham','jabatan'=>'Sekjen Golkar versi Munas Bali','img'=>'http://wartatujuh.com/wp-content/uploads/2015/03/drus-620x330.jpg','url'=>'','content'=>'"Negara ini hancur ketika kebijakan itu diambil berdasarkan penafsiran-penafsiran salah. Nah, itulah Menteri hukum ham kita sekarang ini"'],
        ['from'=>'Akbar Tandjung','jabatan'=>'','img'=>'http://www.solopos.com/dokumen/2010/06/akbar-tandjung-2.jpg','url'=>'','content'=>'"Keputusan Menkumham mengarah untuk memenangkan Agung"'],
        ['from'=>'Prabowo subianto','jabatan'=>'','img'=>'http://www.islamtoleran.com/wp-content/uploads/2014/07/prabowo-subianto-foto-f.jpg','url'=>'','content'=>'"Bagi saya dan bagi Gerindra yang saya pimpin, kami hanya mengakui bapak Aburizal Bakrie."'],
        ['from'=>'Fadli Zon','jabatan'=>'','img'=>'http://www.nonstop-online.com/wp-content/uploads/2015/03/fadli.jpg','url'=>'','content'=>'"Keputusan Menkumham ini sensitif, hak demokrasi parpol dipangkas dengan mudahnya melakukan intervensi politik, sehingga membuat dampak yang luas pada masyarakat. Golkar hanya salah satu korban saja, ini bisa terjadi pada yang lain, dan kalau dilakukan bisa jadi pengacau demokrasi."'],
        ['from'=>'Muntasir Hamid','jabatan'=>'Ketua Forum Silaturahmi DPD II Partai Golkar','img'=>'http://cdn-media.viva.co.id/thumbs2/2012/04/30/153002_muntasir-hamid_663_382.jpg','url'=>'','content'=>'"Dengan surat seperti ini bisa dicabut oleh Presiden. Karena diduga ada oknum di lingkaran Presiden Jokowi yang bermain di tengah kekisruhan ini."']
    ],
    'VIDEO'=>[
        ['id'=>'-3dG7wB9WcM'],
        ['id'=>'DRIch247vb8'],
        ['id'=>'xDhWM4uutZ4'],
        ['id'=>'LnUsdpVtXPM'],
        ['id'=>'zg4OGtlxu3Y'],
        ['id'=>'yBogtsLnlDE'],
        ['id'=>'B5vUhNdpQh0'],
        ['id'=>'u8Yq9o6Gw0g'],
        ['id'=>'sfxg40Lr26o'],
        ['id'=>'Rz25_O1RvKc'],
        ['id'=>'e-rX0qNPPwk'],
        ['id'=>'3PdpO_EPd-A']
    ],
    'FOTO'=>[
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img1.jpg'],
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img2.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/03/22/414462/dukungan-terhadap-ahok.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/02/27/id_375043/375043_620_tempoco.jpg'],
        ['img'=>'http://images.detik.com/customthumb/2015/03/23/10/171112_dprdkomahook4.jpg?w=460'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/03/01/31/970362/ahok-vs-dprd-dan-politik-adu-kuat-WkD.jpg'],
        ['img'=>'http://cdn-media.viva.co.id/thumbs2/2015/03/05/299841_mediasi-ahok-dprd-berujung-ricuh_663_382.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/03/03/id_376061/376061_620_tempoco.jpg'],
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-left: 10px;
        float: right;
        border: 1px solid black;
        margin-top: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        width: 20%;
        float: left;
    }
    .clear {
        clear: both;
    }
</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="http://www.bijaks.net/assets/images/hotpages/kaa/KAA.jpg" style="width: 100%;height: auto;">

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PELOPOR KAA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in row" style="margin-top:10px;">
                <div class="col-xs-12">
                    <?php
                    foreach ($data['PELOPOR'] as $key => $val) { ?>
                    <div class="ketua">
                        <img src="<?php echo $val['tokoh']; ?>" class="img-responsive">
                        <p class="text-center" style="font-size: 8px;"><?php echo $val['text']; ?></p>
                    </div>
                    <?php
                }
                ?>
                </div>
            </div>
        </div>

        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['NARASI2']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['NARASI2']['narasi'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['NARASI2']['title2'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['NARASI2']['narasi2'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['NARASI2']['title3'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['NARASI2']['narasi3'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KILAS BALIK</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <ol style="list-style-type: circle;margin-left: -20px;">
                    <?php
                    foreach ($data['KILAS'] as $key => $val) {
                        echo "<li style='margin-right: 20px;font-weight: bold;'>".$val['thn']."</li><p style='text-align: justify;'>".$val['text']."</p>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PESERTA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in row" style="margin-top:10px;">
                <?php
                foreach ($data['PESERTA'] as $key => $val) { ?>
                <div class="col-xs-4">
                    <img src="<?php echo $val['img']; ?>" class="img-responsive" style="width: 100%;">
                    <p class="text-center" style="font-size: 10px;"><?php echo $val['negara']; ?></p>
                </div> <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PERTEMUAN KEDUA (2005)</span></h5>
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;">Untuk memperingati lima puluh tahun sejak pertemuan bersejarah tersebut, para Kepala Negara negara-negara Asia dan Afrika telah diundang untuk mengikuti sebuah pertemuan baru di Bandung dan Jakarta antara 19-24 April 2005. Sebagian dari pertemuan itu dilaksanakan di Gedung Merdeka, lokasi pertemuan lama pada 50 tahun lalu. Sekjen PBB, Kofi Annan juga ikut hadir dalam pertemuan ini. KTT Asia–Afrika 2005 menghasilkan NAASP (New Asian-African Strategic Partnership, Kerjasama Strategis Asia-Afrika yang Baru), yang diharapkan akan membawa Asia dan Afrika menuju masa depan yang lebih baik berdasarkan ketergantungan-sendiri yang kolektif dan untuk memastikan adanya lingkungan internasional untuk kepentingan para rakyat Asia dan Afrika.</p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PERTEMUAN KETIGA (2015)</span></h5>
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <ol style="list-style-type: circle;margin-left: -20px;">
                <li style='margin-right: 20px;font-weight: bold;'>72 Negara Pastikan Hadiri Peringatan KAA ke-60</li><p style='text-align: justify;'>Terkait kepastian para kepala negara yang akan hadir dalam KAA, sampai saat ini sudah ada 72 kepala negara yang menyatakan kesiapan hadir dalam KAA. Kementerian Luar Negeri memastikan 72 negara telah mengonfirmasi kehadirannya. KAA ke-60 akan dilaksanakan di 2 kota yaitu Jakarta pada 19-23 April dan Bandung pada 24 April. Agenda KAA meliputi "Asia-Afrika Bussiness Summit" dan "Asia-Africa Carnival". Tema yang dibawa Indonesia dalam acara yang akan dihadiri 109 pemimpin negara dan 25 organisasi internasional tersebut adalah peningkatan kerja sama negara-negara di kawasan Selatan, kesejahteraan, serta perdamaian.</p>
                <li style='margin-right: 20px;font-weight: bold;'>Kim Jong-un Akan Hadir di KAA ke-60</li><p style='text-align: justify;'>Menurut informasi, dari 109 negara, 17 observer yang diundang, sampai beberapa hari lalu yang menyatakan partisipasi 85 negara. Kepala negara yang konfirmasi hadir itu sebanyak 24 kepala negara. Tapi, konten lebih lengkap ada di Kementerian Luar Negeri. Pemimpin Korea Utara Kim Jong-un sempat dikabarkan akan hadir dalam Konferensi Asia Afrika (KAA). Hal tersebut sebelumnya diberitakan di yonhap.kr.co, Minggu 25 Januari lalu. Jika kehadiran itu benar-benar terjadi, hal ini merupakan yang pertama bagi pemimpin Korea Utara itu menghadiri pertemuan internasional. Semenjak dia mengambil alih pemerintahan Korea Utara pada 2011, belum pernah ada laporan resmi mengenai perjalanan luar negeri Kim Jong-un. Tetapi sebelumnya dikabarkan, Dubes Republik Demokratik Rakyat Korea (DPRK/Korut) untuk Indonesia Ri Jong Ryul membantah informasi kedatangan 'Supreme Leader'. Dia mengatakan, Presiden Presidium Majelis Tertinggi Rakyat DPRK Kim Yong-nam yang bakal datang ke Tanah Air, bukan Kim Jong-un.[2] Apabila Kim Jong-un bisa hadir di KAA ke-60, maka ini merupakan sejarah baru.</p>
                <li style='margin-right: 20px;font-weight: bold;'>Indonesia Galang Deklarasi Dukungan Palestina Merdeka</li><p style='text-align: justify;'>Sebentar lagi acara skala internasional Konferensi Asia-Afrika (KAA) tahun ini akan digelar. Undangan untuk beberapa negara terkait pun telah dikirim. Penanggung jawab Panitia Nasional ‎Peringatan 60 Tahun Konferensi Asia Afrika (KAA) Luhut Pandjaitan mengatakan, dari 109 negara di Asia dan Afrika, tidak semua mendukung kemerdekaan Palestina. Karena itu, Pemerintah RI akan mendorong peserta KAA yang hadir, agar turut mendukung deklarasi tersebut. Dukungan pemerintah Indonesia terhadap Palestina sebagai negara merdeka, akan diwujudkan dalam pelaksaan Konferensi Asia Afrika (KAA). Indonesia akan menggalang deklarasi ‎dukungan penuh. Hingga saat ini draf dukungan Palestina merdeka masih dibahas perwakilan Indonesia di New York.‎ Luhut di Kantor Presiden, Jakarta, Selasa 31/3/2015 mengatakan, "Saya belum tahu perkembangan terakhir. Tapi itu menjadi usulan dari pemerintah Indonesia dan itu janji presiden. Kementerian Luar Negeri kita masih melobi itu. Mudah-mudahan bisa kita capai." Sebagai negara dengan mayoritas penduduk beragama Islam, Indonesia mempunyai arti penting bagi Palestina. Seperti komitmen Jokowi sejak awal menjadi presiden, pemerintah RI akan terus mendorong deklarasi ini, agar Palestina menjadi negara merdeka dan masuk anggota PBB. "Dan itu saya pikir, sangat penting untuk kita dorong mengenai kemerdekaan Palestina dan dukungan penuh Palestina masuk PBB," tegas Luhut.[3]) Hal ini, mendukung bagi kemerdekaan suatu bangsa, merupakan komitmen Indonesia sejak diproklamasikan sebagaimana tertuang di dalam pembukaan Undang-Undang Dasar 1945.</p>
                <li style='margin-right: 20px;font-weight: bold;'>Raja Yordania Akan Bahas Pemahaman Islam</li><p style='text-align: justify;'>Salah satu yang telah menerima undangan dan menyatakan ingin menghadiri acara yang akan dilaksanakan di Jakarta dan Bandung pada 22-24 April mendatang adalah Yordania. Namun kepastian kehadiran Raja Yordania, Abdullah II belum bisa dipastikan. Masih perlu menunggu konfirmasi dari pihak protokol kerajaan. Hal itu, disampaikan Raja Abdullah II kepada Utusan Khusus Presiden RI, Alwi Shihab, di Istana Hussainiya, Amman, Yordania, Rabu 18 Maret 2015. Pada pertemuan tersebut, Raja Yordania dan Utusan Khusus Presiden RI juga mendiskusikan berbagai isu penting di kawasan yang menjadi perhatian bersama. Salah satu isu yang mengemuka adalah mengenai pentingnya pengembangan pemikiran dan pemahaman Islam yang moderat di kalangan umat Islam. "Kedua pihak memandang bahwa langkah tersebut dapat mendorong berkembangnya pemikiran dan gerakan umat Islam yang membawa pesan damai dan manfaat bagi seluruh umat manusia," demikian dijelaskan pihak Kementerian Luar Negeri (Kemenlu) yang diterima Jumat, 20/3/2015.</p>
            </ol>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>