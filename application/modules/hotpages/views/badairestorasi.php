<?php
$data = [
    'block1'=>[
        'title'=>'Partai Restorasi Dalam Jerat Korupsi',
        'narasi'=>'
        <img src="http://4.bp.blogspot.com/-sRE5OdJZi5U/UQeXyjfFpiI/AAAAAAAAAIw/xjPnyyyZm-g/s320/84512_deklarasi_nasional_demokrat.jpg" class="pic">
        <p>Partai NasDem yang membawa jargon "gerakan perubahan",  tidak serta merta membuat  kader partai tersebut bersih dari praktek korupsi. Tubuh partai Nasdem tengah terkoyak  setelah  beberapa elitnya diciduk KPK karena terbukti melakukan praktek korupsi.</p> 
        <p>Hal ini menjadi ironi bagi etos restorasi yang tengah digembar-gemborkan oleh Partai  yang masih belia tersebut. Sejak awal didirikannya, NasDem memang diproyeksikan oleh pendirinya, Surya Paloh sebagai partai yang menjadi pilar pemberantasan korupsi di Indonesia. Namun pada tataran prakstisnya, cita-cita ideal tersebut tidak terealisasikan. Para pakar pun menilai, partai tersebut telah gagal memperjuangkan ideologi yang diusungnya. </p>
        <img src="http://img.bisnis.com/photos/2015/10/15/105692/kpk-1.jpg" class="pic2">
        <p>Bahkan Surya Paloh tengah dihadapkan pada kondisi yang dilematis, apakah Nasdem masih tetap akan dipertahankan atau dibubarkan sesuai dengan komitmen awal bahwa jika terbukti ada kader partai tersandung kasus hukum yang serius, terutama kasus korupsi, maka tak ada alasan untuk dipertahankan lagi. Komitmen restorasi inilah, yang kemudian menjadi jebakan persepsi bagi Partai berlogo rotasi biru jingga memeluk ini. </p>
    '
    ],

    'block2'=>[
        'title'=>'Kecurigaan NasDem',
        'narasi'=>'
        <img src="https://mamanfathurochman.files.wordpress.com/2014/01/cymera_20140129_085936.jpg" class="pic2">
        <p>Ketua Umum Partai Nasdem, Surya Paloh curiga adanya pihak-pihak yang sengaja mendesain agar partai besutannya itu dibubarkan. Sehingga, berujung pada kasus yang menimpa mantan Sekjen Partai, Patrice Rio Capella dengan dugaan suap korupsi Dana Bansos Pemprov Sumut.</p>
        <p>Menurut Surya, desain tersebut dilakukan dengan pendekatan opini yang sengaja dibangun agar persepsi masyarakat nantinya akan membenci Partai Nasdem. Bekas Politisi Partai Golkar ini menilai, kasus yang menimpa bekas anak buahnya itu sengaja di desain oleh pihak-pihak yang ingin menghancurkan karakter dari Partai Nasdem.</p>
        <img src="http://cdn-media.viva.id/thumbs2/2011/09/07/122391_surya-paloh_663_382.jpg" class="pic">
        <p>Meski begitu, Partai Nasdem akan tetap medukung lembaga antirasuah untuk melakukan pemberantasan korupsi di Indonesia. Sehingga, akan tetap bekerjasama kepada KPK dalam mengusut kasus yang menimpa bekas kadernya tersebut.</p>

        '
    ],

    'block3'=>[
        'title'=>'Sejarah Partai NasDem',
        'narasi'=>'
        <img src="http://data.seruu.com/images/seruu/article/2013/01/25/nasdem(5).jpg" class="picprofil" style="width:100%;">

        <p class="rightcol">Kongres I Partai NasDem yang digelar pada 25-26 Januari 2013 di Jakarta menjadi tonggak sejarah perjalanan Partai NasDem. Berbagai keputusan penting dikeluarkan dalam kongres ini. Satu di antaranya ialah memilih dan menetapkan Surya Paloh sebagai Ketua Umum Dewan Pimpinan Pusat (DPP) Partai NasDem periode 2013-2018. Ibarat perahu, layar telah berkembang, lengkap dengan nakhoda dan awak kapal.</p>
        <p class="rightcol">Kongres juga memberi mandat penuh kepada Surya Paloh untuk menyusun kepengurusan dan perangkat partai. Amanah kongres ini harus selesai selambat-lambatnya 14 hari sejak Surya Paloh terpilih secara aklamasi sebagai Ketua Umum. Bukan cuma itu, Kongres juga memberi mandat penuh kepada Dewan Pimpinan Pusat di bawah kepemimpinan Surya Paloh untuk menetapkan strategi dan kebijakan guna memenangi Pemilihan Umum Legislatif 2014.</p>
        <p class="rightcol">Mengapa Kongres memberikan mandat penting itu kepada Surya Paloh? Pasalnya, kala itu Pemilu 2014 sudah di depan mata. Oleh sebab itu, Kongres memandang, rekruitmen calon anggota legislatif (caleg) merupakan bagian penting dan strategis dalam upaya memenangi Pemilu 2014.</p>
        <p class="rightcol">Kongres I Partai NasDem saat itu diikuti 66 orang yang mewakili 33 DPW, 994 orang mewakili 497 DPD, 9 orang mewakili Majelis Tinggi, dan 2 orang anggota Dewan Pakar. Selain peserta yang memiliki hak suara, Kongres juga dihadiri 800 orang peninjau yang datang dari seluruh penjuru Indonesia.</p>
        <img src="http://4.bp.blogspot.com/-Vd9gfnzoyj0/UbCL8YCLH8I/AAAAAAAASoM/B5C5_DiTuwg/s1600/20130606-Pembekalan+Caleg+NasDem+1.jpg" class="picprofil"  style="width:100%;">
        <p class="rightcol">Surya Paloh Dalam pidatonya setelah terpilih menjadi ketua umum, antara lain menjelaskan, jabatan ketua umum bukanlah kredit poin. Pasalnya, Surya Paloh-lah yang mendirikan Partai NasDem dan sempat menjadi Ketua Majelis Tingggi Partai NasDem, sejak partai ini didaftarkan ke Kementerian Hukum dan HAM. Sebelum Surya Paloh terpilih secara resmi menjadi ketua umum Partai NasDem, adalah Patrice Rio Capella yang dipercaya sebagai ketua umum.</p>

        '
    ],

    'block4'=>[
        'title'=>'Kasus OC Kaligis',
        'narasi'=>'
        <img src="http://riaumandiri.co/assets/berita/28331039054-oc-kaligis-keluar-dari-gedung_kpk.jpg" class="pic">
        <p>Keterlibatan Gubernur Sumatera Utara, Gatot Pujo Nugroho, dan ‎pengacara kondang OC Kaligis yang juga menjabat sebagai Ketua Mahkamah Partai NasDem sejak 2013 ‎dalam kasus dugaan penerimaan dan pemberian suap kepada hakim PTUN Medan, membuat keduanya diperiksa sebagai saksi oleh penyidik. </p>
        <p>Kasus tersebut terungkap dari hasil Operasi Tangkap Tangan (OTT) yang dilakukan KPK di Sumatera Utara, pada Juli 2015. Pada operasi itu KPK menangkap tangan 5 orang, yakni Ketua PTUN Medan, Tripeni Irianto Putro bersama 2 koleganya sesama hakim PTUN, Amir Fauzi dan Dermawan Ginting,‎ panitera pengganti PTUN Syamsir Yusfan, serta seorang pengacara dari kantor OC Kaligis & Associates, M Yagari Bhastara alias Gerry. Kurang dari 24 jam kemudian, usai pemeriksaan secara intesif, KPK akhirnya resmi menetapkan kelimanya sebagai tersangka. Gerry diduga sebagai pemberi suap, sedangkan Tripeni, Amir, Dermawan, dan Syamsir ditengarai selaku penerima suap.</p>
        <img src="http://media.viva.co.id/thumbs2/2015/08/25/332500_gatot-diperiksa-penyidik-kejagung_663_382.jpg" class="pic2">
        <p>Uang sebanyak US$ 15 ribu dan SG$ 5 ribu turut diamankan dalam OTT itu, dan dijadikan sebagai barang bukti transaksi dugaan suap yang diberikan Gerry kepada keempat aparat penegak hukum di PTUN Medan tersebut. Pada perkembangannya, uang itu diberikan untuk memuluskan putusan gugatan Pemprov Sumut yang ditangani PTUN Medan.</p>
        <p>Gugatan ke PTUN itu sebelumnya dilayangkan oleh Kepala Biro Keuangan Pemprov Sumut, Ahmad Fuad Lubis yang notabene adalah anak buah Gubernur Gatot Pujo Nugroho. Pemprov Sumut kemudian menyewa jasa firma hukum OC Kaligis & Associates untuk menangani perkara gugatan tersebut. </p>
        
        '
    ],

    'block5'=>[
        'title'=>'Kasus Rio Capella',
        'narasi'=>'
        <img src="http://assets.kompas.com/data/photo/2015/10/16/131617620151016HER091780x390.JPG" class="pic">
        <p>Komisi Pemberantasan Korupsi (KPK), menetapkan politisi Nasional Demokrat Patrice Rio Capella sebagai tersangka kasus dugaan korupsi dana bantuan sosial (bansos) Sumatera Utara. </p>
        <p>Penyidik menyimpulkan dua bukti permulaan yang cukup disimpulkan terjadi dugaan tipikor (tindak pidana korupsi) yang diduga dilakukan GPN (Gatot Pujo Nugroho) selaku Gubernur Sumut. Dalam kasus yang sama penyidik juga telah menemukan dua bukti permulaan yang cukup menetapkan PRC (Patrice Rio Capella) sebagai tersangka selaku anggota DPR. </p>
        <p>Capela dijerat Pasal 12 huruf a dan b atau Pasal 11 Undang-undang nomor 31/1999 tentang Pemberantasan Tindak Pidana Korupsi. Sebelumnya, dalam pengembangan kasus Gatot, KPK menyelidiki adanya dugaan korupsi dalam pengajuan hak interpelasi DPRD Sumut dan pengadaan Anggaran Pendapatan dan Belanja Daerah Sumut tahun 2014.</p>
        <img src="http://cdn.klimg.com/merdeka.com/i/w/news/2015/10/19/610932/670x335/rio-capella-dibawakan-rp-200-juta-oleh-anak-magang-oc-kaligis.jpg" class="pic2">
        <p>Selain KPK, Kejaksaan juga sedang menggarap kasus bansos. Kejaksaan bahkan sudah memeriksa empat anak buah Gubernur Sumatera Utara Gatot Pujo Nugroho, keempatnya dianggap mengetahui dugaan korupsi dana bantuan sosial Provinsi Sumut tahun anggaran 2012-2013 yang berjumlah Rp 2 triliun. Antara lain, Kepala Biro Keuangan Ahmad Fuad Lubis, mantan Kepala Biro Keuangan Baharudin Siagiaan, Sekretaris Daerah Sumut Hasban Ritonga, dan Asisten I Pemerintahan Hasiholan Silaen.</p>

        '
    ],

    'block6'=>[
        'title'=>'Indikasi Terlibatnya SP',
        'narasi'=>'
        <img src="http://www.voa-islam.com/photos6/sasa/bIaK7Q4owJ.jpg" class="pic">
        <p>Pusaran suap hakim PTUN Medan yang menyeret nama mantan Ketua Mahkamah Partai Nasdem, OC Kaligis ternyata turut serta melibatkan petinggi partai Nasdem lainnya. Nama Ketua Umum Nasdem Surya Paloh sebanyak 15 kali disebut dalam BAP dua tersangka suap hakim. Di BAP istri Gubernur Sumut, Evy Susanti ada 1 kali nama Surya Paloh disebut. Sedangkan di BAP Yagari Bhastara Guntur alis Gery ada sebanyak 14 kali nama Surya Paloh disebut.</p>
        <p>Sangat sulit untuk tidak menghubungkan NasDem dalam dua perkara tersebut. Pasalnya, pihak-pihak terkait mulai dari Jaksa Agung M Prasetyo, Wakil Gubernur Sumut Tengku Erry Nuradi sampai tersangka kasus suap PTUN Medan, Otto Cornelis Kaligis, semua adalah kader NasDem. Belum lagi ada fakta persidangan yang menyebutkan bahwa Gubsu Gatot Pujo Nugroho, pernah menggelar pertemuan dengan para petinggi Partai NasDem membahas kasus dugaan korupsi dana bansos. Ketua Umum NasDem Surya Paloh termasuk salah satu yang hadir dalam pertemuan tersebut.</p>
        <img src="http://i2.wp.com/swaraindonesia.web.id/wp-content/uploads/2015/10/208674_surya-paloh-dan-rio-capella_663_382-e1444929247737.jpg?resize=619%2C344" class="pic2">
        <p>Saat ini, Partai NasDem berada dalam posisi yang sulit. Pasalnya, sejumlah petingginya terindikasi terlibat dalam kasus dugaan korupsi dana bantuan sosial Pemprov Sumut dan suap hakim PTUN Medan. Apalagi Sejauh ini beberapa Pihak menilai tidak adanya keberanian dari kader-kader NasDem sejak nama Surya Paloh  disebut-sebut, partai malah terlihat membela sehingga ada asumsi Partai NasDem justru terkesan melindungi koruptor.</p>
        '
    ],

    'block8'=>[
        'title'=>'Analisa',
        'narasi'=>'
        <img src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSJSfpg__BDp_GtaNwmJK4_EbQp1KowiOwT65aHLKZcXw2VHXGh" class="pic">
        <p>Kasus korupsi penanganan dana Bantuan Sosial, tunggakan dana bagi hasil, dan penyertaan modal badan usaha milik daerah Sumatera Utara dengan terdakwa Gubernur Sumatera Utara Gatot Puji Nugroho dan istrinya, Evy Susanti ikut menyerat beberapa elit partai NasDem. </p>
        <p>OC Kaligis yang merupakan ketua Mahkamah Partai Nasdem sudah diseret ke sel KPK karena terbukti menyuap hakim dan panitera Pengadilan Tata Usaha Negara Medan. Adapun Sekjen Nasdem Patrice juga telah ditetapkan menjadi tersangka oleh KPK. Gatot meminta bantuan ke Sekjen Nasdem, Patrice agar menghentikan kasusnya dengan cara membujuk Jaksa Agung Muhammad Prasetyo karena Jaksa Agung tersebut juga merupakan kader Nasdem.  Dua nama elit Nasdem yakni Ketua NasDem Sumatera Utara Tengku Erry Nuradi dan Ketua Umum NasDem Surya Paloh juga disebut-sebut terlibat dalam kasus suap tersebut.
        Diduga Gatot memanfaatkan para petinggi Nasdem sebagai sarana penghubung antara dirinya dan pihak Kejaksaan.</p>   
        <img src="https://img.okezone.com/content/2015/07/14/337/1182007/oc-kaligis-ditahan-pengacara-kami-tidak-terima-mLYhV5oQ7E.jpg" class="pic2">
        <p>Selain mencederai komitmen partai berlambang rotasi untuk menjadi partai yang bersih dan anti politik transaksional, kasus tersebut juga akan merusak citra partai tersebut di masyarakat dengan konsekuensi akan kehilangan kepercayaan dari para pendukungnya.  Nasdem tidak ubahnya seperti kebanyakan  partai lain, yang kadernya banyak terjerat praktek korupsi. Apalagi, kasus yang sekarang menerpanya tidak hanya melibatkan kadernya,  tidak tanggung-tanggung ketua umumnya juga diduga terlibat di dalamnya.</p> 
        <img src="http://images.detik.com/customthumb/2015/10/15/157/palohsurya1.jpg?w=780&q=90" class="pic">
        <p>Secara umum kondisi tersebut menjadi boomerang bagi partai Nasdem yang sebelumnya memang dikenal sebagai partai yang bersih dari korupsi, dan secara khusus hal ini membuat Surya Paloh menjadi pesakitan karena dianggap tidak mampu menanamkan semangat restorasi di setiap kadernya.  Semangat restorasi yang sering diagung-agungkan olehnya hanyalah sebatas jargon semata.</p> 
        <p>&nbsp;</p>        
        
        '
    ],

    'block11'=>[
        'title'=>'Menagih Janji Surya Paloh',
        'narasi'=>'
            <img src="http://static.republika.co.id/uploads/images/inline/Bubarkan1.jpg" class="pic2">
            <p>Ketua Umum Partai NasDem, Surya Paloh pernah mengatakan bahwa keberadaan partainya yang didirikan pada 2011 itu, tidak layak dipertahankan kalau ada kadernya tersandung kasus korupsi. Ikrar Paloh itu diingatkan lagi, salah satunya oleh Ketua DPR Setya Novanto. Dia mempersilakan Ketua Umum Partai Restorasi itu untuk menepati janjinya.</p>
            <p>Beban berat Paloh atas masa depan partainya kian bertumpuk, karena kasus yang membelit Gatot Pujo Nugroho, OC Kaligis, dan Rio Capella dihubung-hubungkan dengan Jaksa Agung Muhammad Prasetyo. Nama terakhir adalah kader Partai Nasdem sebelum menjabat Jaksa Agung.</p>
            <p>Desakan pembubaran partai Nasdem ini bukan hal yang mengada-ada. Sebab, Ketua Umum Partai Nasdem Surya Paloh sendiri yang telah berjanji partainya tidak akan cuci tangan bila ada kader yang tersangkut kasus pidana, </p>
            <img src="http://static.republika.co.id/uploads/images/detailnews/tagar-bubarkan-nasdem-puncaki-daftar-trending-topic-twitter-_151015214346-758.jpg" class="pic">
            <p>Berangkat dari peristiwa ini jugalah para penghuni lini masa pun menagih janji Ketua Umum NasDem Surya Paloh agar segera membubarkan partai yang masih seumur jagung. Bahkan, Tagar #BubarkanNasdem pun meramaikan jagat Twitter dan memuncaki daftar trending topic. tidak sedikit meme lucu yang mengingatkan janji Surya Paloh untuk segera melakukan pembubaran.</p>

            '
    ],


    'institusiPendukung' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdpr51da66b0a7ac4','image'=>'http://uc.blogdetik.com/791/79197/files/2014/11/ed46b4cf066c6d1367af9a276daf661a_logo-dpr-ri-250x239.png','title'=>'DPR RI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/komisiiiidprri54d30e3f5bbd7','image'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTXmc92ue__DQqCAqCLQfSPOqqXQhpVO-uRSRC-NdGbyERM1PKalA','title'=>'KOMISI III'],
    ],

    'institusiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/komisipemberantasankorupsi5192fb408b219','image'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/KPK_Logo.svg/2000px-KPK_Logo.svg.png','title'=>'KPK'],
        ['link'=>'http://www.bijaks.net/aktor/profile/icwindonesiancorruptionwatch534e45d49c5d9','image'=>'http://antikorupsi.org/sites/antikorupsi.org/files/ICW-x-250x180.png','title'=>'ICW'],
        ['link'=>'http://www.bijaks.net/aktor/profile/lembagailmupengetahuanindonesia52e855b288b29','image'=>'http://www.biologi.lipi.go.id/foto_berita/2015_06_01_10_20_50logolipikecil.png','title'=>'LIPI'],
        ['link'=>'#','image'=>'http://indopolitika.com/wp-content/uploads/2014/09/Said-Salahuddin-SIGMA.jpg','title'=>'SIGMA'],
        ['link'=>'#','image'=>'http://4.bp.blogspot.com/-r2hETPUShPo/TnVuO--zKUI/AAAAAAAAAGs/c_PiUZtPz4s/s1600/P1010643.JPG','title'=>'MAKI'],

    ],

    'partaiPendukung' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/nasdem5119b72a0ea62','image'=>'http://1.bp.blogspot.com/-IFxNQbIu91k/UQRlK64rouI/AAAAAAAASII/EXKZlL-j0Co/s1600/nasdem_biru.png','title'=>'Partai Nasdem'],
    ],

    // 'partaiPenentang' => [
    //     ['link'=>'','image'=>'','title'=>''],
    // ],

    'quotePendukung'=>[
      ['from'=>'Surya Paloh','jabatan'=>'Ketua Umum Partai Nasional Demokrat','img'=>'http://img2.bisnis.com/makasar/posts/2014/09/17/180643/Surya-Paloh.jpg','url'=>'http://www.bijaks.net/aktor/profile/suryapaloh511b4aa507a50','content'=>'"Kami tidak pernah bermain-main antara ucapan dengan perbuatan. NasDem tidak pernah bergeser dari komitmen penegakan hukum"'],
      ['from'=>'Jusuf Kalla','jabatan'=>'wapres RI','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAw4lGBgiFQSmNW9wkvBrQ30CDIcIHTuxTm9iSwCStsvYbbxoBJQ','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'"Harapan kita beliau dapat menjelaskan atau tentu saja dapat mempertanggungjawabkannya, ya kita tunggu saja prosesnya"'],
      ['from'=>'Lutfi Andy Mutty','jabatan'=>'Ketua DPP Partai Nasdem','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTI9jBZtZgbuSPDA5Wjpuxe95wmVBwDeRYE-96MpFKLe1u1hWILag','url'=>'#','content'=>'"Sudah jadi tradisi KPK menyebut berapa angkanya. Tapi, sejak kasus Patrice Rio Capella kemarin, tidak pernah saya dengar nilai yang disangkakan"'],
      ['from'=>'Taufiq Basari','jabatan'=>'Ketua DPP NasDem','img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQwBS9b38Virz18e-8k0T4BrvFj_VJj4nunNN6c3w0jozXZMH7o0w','url'=>'http://www.bijaks.net/aktor/profile/taufikbasari52326526994da','content'=>'"Dari awal kami konsisten kalau ada kader jadi tersangka akan diberhentikan atau mengundurkan diri, sudah kami lakukan saat ini. Tidak perlu khawatir kami tetap konsisten"'],
      ['from'=>'HM Prasetyo','jabatan'=>'Jaksa Agung','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVUhYKgnV6H2_QVcIYitLGPPvQbC1gG1UnrTLjiwURd0P2xjAn','url'=>'http://www.bijaks.net/aktor/profile/muhammadprasetyo54d46b2010b68','content'=>'"Kita tidak akan gentar dalam menghadapi isu seperti itu. Ini jaminan saya, isu apapun yang dikait-kaitkan dengan Kejaksaan dan masalah Rio atau apapun. Kita tidak akan pernah gentar"'],
      ['from'=>'Setia Novanto','jabatan'=>'Ketua DPR RI','img'=>'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRyQHuA46Vuv4CeqUW_XM4sEEEIeucf8BnoeGkzPnIxtcroR1a0','url'=>'http://www.bijaks.net/aktor/profile/drssetyanovanto50f8fd3c666bc','content'=>'"Kita selalu wanti-wanti kepada anggota DPR agar lebih hati-hati, jaga kredibilitas DPR . Mudah-mudahan dengan kejadian ini lebih hati-hati"'],
      ['from'=>'Pangi Syarwi Chaniago','jabatan'=>'Pengamat politik dari Universitas Islam Negeri (UIN) Syarif Hidayatullah Jakarta','img'=>'http://static.inilah.com/data/berita/foto/2118609.jpg','url'=>'http://www.bijaks.net/aktor/profile/pangisyarwichaniago534d013c5131b','content'=>'"Ini kultur politik yang mesti ditiru partai lain. Namun ini belum kiamat bagi NasDem. Selama ini NasDem membangun partai antikorupsi"'],
      ['from'=>'Azis Syamsuddin','jabatan'=>'Ketua Komisi III DPR RI','img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSB-Y16vUbXUIUSLI09J-iOieWX-YfTGuDBuYdW1HY26sRgkvuQAA','url'=>'http://www.bijaks.net/aktor/profile/azissyamsuddin52731d954a047','content'=>'"Kita hormati proses secara hukum"'],
      ['from'=>'Syarief Abdullah Alkadrie','jabatan'=>'Sekretaris Fraksi Partai NasDem DPR RI','img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRzKYmk8ELT7sUkDfrX5lSVozxBC7HltPkr2O2mXvZn8rwQvZnM','url'=>'#','content'=>'"Seperti kasus Bank Century, banyak kasus  BLBI. Jadi saya pikir kasus-kasus besar ini yng menjadi perhatian KPK jangan hanya persoalan-persoalan politik dikaitkan seperti ini"'],
      ['from'=>'Taufiqulhadi','jabatan'=>'Anggota Komisi III DPR dari Fraksi NasDem','img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcT6SYC1JM9aCdzYR3WQBqftGl5CNRRhFW3FLdk8pXK2lOontfer','url'=>'http://www.bijaks.net/aktor/profile/ttaufiqulhadi52415cefea55d','content'=>'"Dalam menghadapi plot ini, kami (Partai NasDem) bersikap tidak akan tinggal diam dan dalam waktu dekat kita siap membentuk tim investigasi untuk menelusuri terkait kasus hukum tersebut dan menelisik upaya pihak (plot) tersebut"'],
    ],

    'quotePenentang'=>[
        ['from'=>'Johan Budi','jabatan'=>'Plt Wakil Ketua KPK','img'=>'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSThlNgVEARxS08Xaurdc-jBFuVixfKSmUKfxh5heAfjyjVOoZc','url'=>'http://www.bijaks.net/aktor/profile/johanbudisp51b9205b1dd91','content'=>'"dalam kasus yang sama penyidik juga telah menemukan dua bukti permulaan yang cukup menetapkan PRC (Patrice Rio Capella) sebagai tersangka selaku anggota DPR"'],
        ['from'=>'Idil Akbar','jabatan'=>'Pengamat politik Universitas Pajajaran','img'=>'https://0.academia-photos.com/13216736/4057714/4733719/s200_idil.akbar.jpg','url'=>'#','content'=>'"harus diusut posisi perkaran itu dan menyangkut siapa saja. Harus sampai pada orang yang menyuruh Rio karena dialah aktor intelektual itu. Dialah pemain sebenarnya"'],
        ['from'=>'Siti Zuhro','jabatan'=>'Pengamat Politik LIPI','img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcT00adNXscc7--qxi8Z7jJFTCv7lBBEr1Ge_ak65tQdoFyD30dX','url'=>'http://www.bijaks.net/aktor/profile/sitizuhro53071802b0bee','content'=>'"Kalau partai baru saja tidak mampu menjaga itu, terus apa bedanya partai-partai yang ada sudah lama dengan yang baru di kancah politik"'],
        ['from'=>'Boyamin Saiman','jabatan'=>'Koordinator Masyarakat Anti-Korupsi Indonesia (MAKI)','img'=>'http://berita2bahasa.com/images/articles/2013613boyamin%20saiman%20(hukumonline%20com)%20b.jpg','url'=>'http://www.bijaks.net/aktor/profile/boyaminsaiman533cd211ad585','content'=>'"KPK tidak segera responsif ketika Surya Paloh disebut namanya, padahal ia sendiri bersedia jika dimintai keterangan."'],
        ['from'=>'Febri Hendri','jabatan'=>'Koordinator Divisi Investigasi Indonesian Corruption Watch (ICW)','img'=>'http://www.rmol.co/images/berita/thumb/thumb_624058_10045626012015_Febri-Hendri.jpg','url'=>'http://www.bijaks.net/aktor/profile/febrihendri5334d8bd694b5','content'=>'"Kami juga dukung KPK untuk menetapkan tokoh lebih tinggi dari Rio jika memang terdapat dua alat bukti yang mengindikasikan dia terlibat dalam kasus ini"'],
        ['from'=>'Said Salahuddin','jabatan'=>'Direktur Sinergi Masyarakat untuk Demokrasi Indonesia (Sigma)','img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5pybJ3qVfk1MTM2S7cU3fyBRKjfuy013rUU3i_Ks_b4R-kXUl','url'=>'http://www.bijaks.net/aktor/profile/saidsalahudin51b917bbab45f','content'=>'"Dalam setiap kasus gratifikasi suap, pihak pemberi suap tentulah hanya mau menjanjikan atau memberikan uang atau materi jika ia menerima janji atau manfaat dari pihak penerima suap. Atas kontribusi yang dijanjikan atau diberikan oleh penerima suap itulah pemberi suap menjanjikan atau memberikan uang atau materi"'],
    ],

    'video'=>[
        ['id'=>'YBi9nibUElI'],
        ['id'=>'8w7rCaDbDG4'],
        ['id'=>'wAT2FkAjl_s'],
        ['id'=>'q90A4gSG0ew'],
        ['id'=>'FsBgZuS0S30'],
        ['id'=>'Zl6XDHL2ZGU'],
        ['id'=>'SSyLzrFFhug'],
        ['id'=>'cyWrKOLdwPE'],
        ['id'=>'hlOGcVqH1g8'],
        ['id'=>'GBIt-KmXdjE'],
        ['id'=>'xktXN8y9FGg'],
        ['id'=>'ooFWEw8VR40'],
        ['id'=>'Vqw_Zbn6o4s'],
        ['id'=>'U7hGVwijbZ8'],
        ['id'=>'bL9KJWbJUtw'],
        ['id'=>'5-GWDteAZTE'],
        ['id'=>'XnQlJPEWePE'],
        ['id'=>'x-b0S0Yvwr4'],
        ['id'=>'_s7rBft2caw'],
        ['id'=>'7eovkkJ_uJo'],
        ['id'=>'_s7rBft2caw'],

    ],

    'foto'=>[
        ['img'=>'http://images.cnnindonesia.com/visual/2015/07/14/3134c828-a13d-4f23-87b7-dbd5862fe4cc_169.jpg?w=650'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/07/23/326311_oc-kaligis_663_382.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/10/16/337/1232591/rio-capella-tersangka-sekjen-perindo-semoga-jadi-pelajaran-ATxHlFeioi.jpg'],
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/10/15/609228/670x335/rio-capella-menduga-dirinya-ditetapkan-tersangka-besok.jpg'],
        ['img'=>'http://t2.gstatic.com/images?q=tbn:ANd9GcTmUJb7wOveW23PUctLHrLEbfPnp61Nt0hm_8v4cWCGHcT5JNi3'],
        ['img'=>'http://t1.gstatic.com/images?q=tbn:ANd9GcTKVPhw_I-BwG6pMYCavzBDaTclhyCexHQWoQi9O-cQuSxVsps'],
        ['img'=>'http://cdn.klimg.com/merdeka.com/i/w/news/2015/10/15/609364/670x335/ketua-dpp-nasdem-tegaskan-takkan-lindungi-kader-terlibat-korupsi.jpg'],
        ['img'=>'http://www.cenderawasihpos.com/uploads/berita/dir05082014/img0508201470851.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2014/08/05/20292401-200foto44780x390.JPG'],
        ['img'=>'http://a.okezone.com/photos/2015/03/12/18868/117793_large.jpg'],
        ['img'=>'http://t1.gstatic.com/images?q=tbn:ANd9GcQIMcRhu5NwJQjdTN6UYGPIWLOj5nIAixeiePn7kgIKkMhX98iU'],
        ['img'=>'http://hukrim.beritaprima.com/wp-content/uploads/sites/3/2015/07/kaligis-ditahan5.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2014/08/05/273741/kV8coWHmUG.jpg?w=668'],
        ['img'=>'http://rmol.co/images/berita/normal/122278_03260703042015_Barnabas_Suebu.jpg'],
        ['img'=>'http://bataranews.com/wp-content/uploads/2015/09/IMG_20150926_143853.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2010/12/09/101191_aktivis-icw-mengenakan-topeng-berwajah-koruptor_663_382.jpg'],
        ['img'=>'http://t3.gstatic.com/images?q=tbn:ANd9GcSLKqPbqsuIdfZRa034tiig6lcgCRLeCzguOvLYfQYntRMAu9arPw'],
        ['img'=>'http://pelitaonline.com/uploads/berita/medium/akibat-ada-pengurus-nasdem-yang-korupsi-desakan-bubarkan-partai-di-tagih-91206.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/surya-paloh_1_20150901_092030.jpg'],
        ['img'=>'http://medanseru.co/photo/dir062015/Korupsi-Walikota-Sibolga-Disebut-Dibekingi-Jaksa-Agung--amp--Partai-Nasdem.jpg'],
        ['img'=>'http://www.kabarhukum.com/wp-content/uploads/2015/10/surya-paloh1.jpg'],
        ['img'=>'https://www.islamtoleran.com/wp-content/uploads/2015/03/208674_surya-paloh-dan-rio-capella_663_382.jpg'],
        ['img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150714_073356_harianterbit_nasdem_oc_kaligis.JPG'],
        ['img'=>'http://photo.kontan.co.id/photo/2012/06/23/563725468p.jpg'],
        ['img'=>'http://img.antaranews.com/new/2014/08/ori/20140823Presiden-Terpilih-Jokowi-220814-wsj-4.jpg'],
        ['img'=>'http://images.cnnindonesia.com/visual/2015/01/21/232c515a-97f4-46be-992e-b11767dca1fc_169.jpg?w=650'],
        ['img'=>'http://www.kabel.co.id/wp-content/uploads/2015/07/oc-kaligis.png'],
        ['img'=>'http://medanseru.co/photo/dir032015/Diresmikan--Proyek-Rusunawa-Sarat-Korupsi-di-Sibolga.jpg'],
        ['img'=>'http://3.bp.blogspot.com/-xCayerk3oFY/Ua2GZs5iQCI/AAAAAAAAV-c/8XKJ-I10MQY/s1600/demo+format+tanggal+21+mei+2013..jpg'],
        ['img'=>'http://chaidirritonga.com/new/wp-content/uploads/2015/06/image62.jpg'],
        ['img'=>'http://www.palapapos.com/wp-content/uploads/2015/06/nas.jpg'],

    ],

    'berita'=>[
        ['img'=>'','shortText'=>'di Persidangan OC Kaligis ngeyel','link'=>'http://www.bijaks.net/news/article/0-221378/di-persidangan-oc-kaligis-ngeyel-tak-terkait-suap-menyuap'],
        ['img'=>'','shortText'=>'OC Kaligis Suap hakim PTUN Medan','link'=>'http://www.bijaks.net/news/article/0-216756/oc-kaligis-suap-hakim-ptun-medan-sebanyak-tiga-kali/'],
        ['img'=>'','shortText'=>'OC Kaligis terkesan sewenang-wenang','link'=>'http://www.bijaks.net/news/article/0-211971/pecat-anak-buah-politikus-pkb-kelakuan-oc-kaligis-terkesan-sewenang-wenang'],
        ['img'=>'','shortText'=>'Rio capella jadi tersangka','link'=>'http://www.bijaks.net/news/article/0-224542/rio-capella-jadi-tersangka-surya-paloh-malu'],
        ['img'=>'','shortText'=>'Mohon izin untuk doakan adnan buyung','link'=>'http://www.bijaks.net/news/article/0-210584/sebelum-sidang-oc-kaligis-mohon-izin-untuk-doakan-adnan-buyung/'],
        ['img'=>'','shortText'=>'DPR KPK periksa Rio Capella','link'=>'http://www.bijaks.net/news/article/0-224134//dugaan-korupsi-suap-dpr-kpk-periksa-rio-capella/'],
        ['img'=>'','shortText'=>'JK yakin jaksa agung tak terseret','link'=>'http://www.bijaks.net/news/article/0-224060/jk-yakin-jaksa-agung-tak-terseret-kasus-rio-capella/'],
        ['img'=>'','shortText'=>'Rio Capella tersangka nasdem layangkan surat pengunduran diri','link'=>'http://www.bijaks.net/news/article/0-223903/rio-capella-tersangka-nasdem-layangkan-surat-pengunduran-dirinya-ke-dpr/'],
        ['img'=>'','shortText'=>'Rio Capella harap segera dapat pengganti','link'=>'http://www.bijaks.net/news/article/0-223857//serahkan-surat-pengunduran-diri-rio-capella-nasdem-harap-segera-dapat-pengganti/'],
        ['img'=>'','shortText'=>'KPK tak tanggapi permintaan oc kaligis','link'=>'http://www.bijaks.net/news/article/0-212393/kpk-tak-tanggapi-permintaan-oc-kaligis-soal-rekening-diblokir'],
        ['img'=>'','shortText'=>'KPK pasti transparan','link'=>'http://www.bijaks.net/news/article/0-222689/kpk-pasti-transparan-usut-kasus-rio-capella'],
        ['img'=>'','shortText'=>'kini KPK incar si brewok','link'=>'http://www.bijaks.net/news/article/0-222507/habis-seret-rio-capella-di-pusaran-korupsi-kini-kpk-incar-si-brewok'],
        ['img'=>'','shortText'=>'Rio Capella hanya kambing hitam','link'=>'http://www.bijaks.net/news/article/0-222469/pengamat-patrice-rio-capella-hanya-kambing-hitam'],
        ['img'=>'','shortText'=>'Jokowi harus tindak tegas jika jaksa agung lakukan intervensi','link'=>'http://www.bijaks.net/news/article/0-222212/kasus-rio-capella-hmi-jokowi-harus-tindak-tegas-jika-jaksa-agung-lakukan-intervensi'],
        ['img'=>'','shortText'=>'Rio Capella irit bicara','link'=>'http://www.bijaks.net/news/article/0-222263/12-jam-diperiksa-rio-capella-irit-bicara/'],
        ['img'=>'','shortText'=>'Nasdem tunjuk sekjen baru','link'=>'http://www.bijaks.net/news/article/0-222160/nasdem-tunjuk-sekjen-baru-gantikan-rio-capella'],
        ['img'=>'','shortText'=>'Rio Capella hanya tumbal','link'=>'http://www.bijaks.net/news/article/0-223347/rio-capella-hanya-tumbal-untuk-selamatkan-surya-paloh'],
        ['img'=>'','shortText'=>'Ada yang ingin nasdem bubar','link'=>'http://www.bijaks.net/news/article/0-224407/paloh-ada-yang-ingin-nasdem-bubar'],
        ['img'=>'','shortText'=>'Bos nasdem mulai ukur Jokowi','link'=>'http://www.bijaks.net/news/article/0-224368/aihh-bos-nasdem-mulai-ukur-jokowi-tidak-spektakuler-poinnya-625'],
        ['img'=>'','shortText'=>'Ada pihak yang ingin partai nasdem lenyap','link'=>'http://www.bijaks.net/news/article/0-224010/surya-paloh-ada-pihak-yang-ingin-partai-nasdem-lenyap-dari-bumi-indonesia'],
        ['img'=>'','shortText'=>'Nasdem curiga ingin dilemahkan','link'=>'http://www.bijaks.net/news/article/0-223933/nasdem-curiga-ingin-dilemahkan'],
        ['img'=>'','shortText'=>'Disinyalir hanya kambing hitam','link'=>'http://www.bijaks.net/news/article/0-222165/tindakan-rio-capella-dalam-kasusnya-disinyalir-hanya-kambing-hitam'],
        ['img'=>'','shortText'=>'Garis politik nasdem telah ternoda','link'=>'http://www.bijaks.net/news/article/0-223572/pengamat-restorasi-sebagai-kitab-suci-garis-politik-nasdem-telah-ternoda'],
        ['img'=>'','shortText'=>'terjerat korupsi plt sekjen banyak hal yang sudah diperjuangkan','link'=>'http://www.bijaks.net/news/article/0-223107/terjerat-korupsi-plt-sekjen-banyak-hal-yang-sudah-diperjuangkan-nasdem-dalam-mewujudkan-restorasi'],
        ['img'=>'','shortText'=>'Nasdem goyah plt sekjen turun ke daerah','link'=>'http://www.bijaks.net/news/article/0-223111/nasdem-goyah-plt-sekjen-turun-ke-daerah-untuk-kembalikan-citra-partai'],
        ['img'=>'','shortText'=>'Jatah Nasdem dikabinet aman','link'=>'http://www.bijaks.net/news/article/0-223117/tersandung-korupsi-wapres-tegaskan-jatah-nasdem-di-kabinet-aman'],
        ['img'=>'','shortText'=>'Parpol masih jadi lahan rawan korupsi','link'=>'http://www.bijaks.net/news/article/0-222855/kasus-eks-sekjen-nasdem-jadi-bukti-parpol-masih-jadi-lahan-rawan-korupsi'],
        ['img'=>'','shortText'=>'Patrice Rio mundur dari sekjen Nasdem','link'=>'http://www.bijaks.net/news/article/0-222765/patrice-rio-mundur-dari-sekjen-nasdem-pembelajaran-politik-yang-bagus'],
        ['img'=>'','shortText'=>'Nasdem pasrahkan nasib prasetyo ke presiden','link'=>'http://www.bijaks.net/news/article/0-222697/nasdem-pasrahkan-nasib-prasetyo-ke-presiden'],
        ['img'=>'','shortText'=>'Rachmawati tak yakin nasdem dibubarkan','link'=>'http://www.bijaks.net/news/article/0-222496/tuduh-surya-paloh-tukang-bohong-rachmawati-tak-yakin-nasdem-dibubarkan'],
        ['img'=>'','shortText'=>'Kalau tidak berani bubarkan partai surya paloh bukan-pemimpin','link'=>'http://www.bijaks.net/news/article/0-222315/nasdem-oleng-ketua-umum-hmi-kalau-tidak-berani-bubarkan-partai-surya-paloh-bukan-pemimpin-tapi-pemimpi'],
        ['img'=>'','shortText'=>'Jokowi punya hak reshuffle kabinet','link'=>'http://www.bijaks.net/news/article/0-222220/ketua-dpp-nasdem-jokowi-punya-hak-reshuffle-kabinet'],
        ['img'=>'','shortText'=>'Rekannya terseret korupsi','link'=>'http://www.bijaks.net/news/article/0-222181/rekannya-terseret-korupsi-jaksa-agung-minta-nasdem-tak-disudutkan'],


    ],

    'kronologi'=>[
        'list'=>[
            ['date'=>'9 Juli 2015','content'=>'
              <p>KPK melakukan Operasi Tangkap Tangan (OTT) di PTUN Medan. KPK menangkap Ketua PTUN Medan, Tripeni Irianto Putro bersama 2 koleganya sesama hakim PTUN, Amir Fauzi dan Dermawan Ginting,‎ panitera pengganti PTUN Syamsir Yusfan, serta seorang pengacara dari kantor OC Kaligis & Associates, M Yagari Bhastara alias Gerry.</p>
              <p>Sehari setelahnya, KPK menetapkan kelimanya sebagai tersangka.</p>
              <p>Dalam pengembangan kasus tersebut, nama pengacara kondang OC Kaligis ikut terseret sebagai pihak pemberi suap</p>'],
            ['date'=>'14 Juli 2015','content'=>'OC Kaligis resmi jadi tersangka'],
            ['date'=>'22 Juli 2015','content'=>'Gubernur Sumatera Utara Gatot Pujo Nugroho diperiksa KPK sebagai saksi suap majelis hakim dan penitera PTUN Medan untuk tersangka M Yagari Bhastara alias Gerry dan OC Kaligis.'],
            ['date'=>'28 Juli 2015','content'=>'
            <p>Gatot Pujo Nugroho beserta istrinya Evi Susanti ditetapkan jadi tersangka</p>
            <p>Dalam persidangan kasus tersebut, beberapa elit Partai Nasdem disebut-sebut terlibat, mereka yang disebut adalah Sekjen Patrice,  Ketua NasDem Sumatera Utara Tengku Erry Nuradi dan Ketua Umum NasDem Surya Paloh.</p>
            '],
            ['date'=>'Rabu 23 September 2015','content'=>' 
            <p>Patrice diperiksa KPK sebagai saksi terkait pertemuan petinggi petinggi Partai NasDem dengan Gatot dan wakilnya Tengku Erry Nuradi.</p>
            <p>Pertemuan tersebut difasilitasi oleh pengacara Gatot, Otto Cornelis (OC) Kaligis.</p>'],
            ['date'=>'15 Oktober 2015','content'=>'Patrice akhirnya ditetapkan jadi tersangka. Sekjen Nasdem tersebut diduga menerima hadiah atas jasanya memfasilitasi penanganan perkara korupsi bantuan sosial saat diusut oleh Kejaksaan Tinggi Sumatera Utara. Patrice diduga melanggar Pasal 12 huruf a Undang-Undang Pemberantasan Korupsi.'],

        ]
    ],

]


?>

<style>
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/badairestorasi/top.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/badairestorasi/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['block1']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block2']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block2']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block8']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block8']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <!-- <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block9']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block9']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block10']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block10']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>
 -->
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block11']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block11']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block5']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block5']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block6']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['block6']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block4']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block4']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['block3']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['block3']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>

       <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['foto'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['video'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['berita'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>