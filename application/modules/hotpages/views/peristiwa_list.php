<h4 class="kanal-title kanal-title-cokelat">PERISTIWA</h4>
<?php
foreach($peristiwa as $key) { ?>
<div class="row">
    <div class="col-xs-4 col-sm-4 nopadding">
        <a class="" href="<?php echo $key['url']; ?>">
            <img class="img-media-list" src="<?php echo $key['thumbUrl'];?>" alt="<?php echo !empty($key['title']) ? $key['title'] : ''; ?>" >
        </a>
    </div>
    <div class="col-xs-8 col-sm-8">
        <a class="" href="<?php echo $key['url']; ?>">
            <h5 class="media-heading-nomargin"><?php echo $key['title']; ?></h5>
        </a>
        <small class="media-heading-nomargin"><?php echo substr($key['text'], 0, 60)." ..."; ?></small>
    </div>
</div>
<hr class="line-mini">
<?php } ?>