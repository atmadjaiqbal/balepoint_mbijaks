<?php
$data = [
    'NARASI'=>[
        'title'=>'KAMPUS BODONG WISUDA ABAL ABAL',
        'narasi'=>'<img src="http://smeaker.com/nasional/wp-content/uploads/2015/09/Diduga-Abal-Abal-Wisuda-Di-Tangsel-Digerebek-Kemenristek-Dikti-.jpg" class="pic">
                   <p>Dunia pendidikan di tanah air kembali geger dengan penonaktifan kampus-kampus yang dianggap bermasalah terkait dengan konflik internal, rasio jumlah mahasiswa dan dosen yang tidak seimbang serta dugaan adanya praktek jual beli ijazah palsu yang dilakukan oleh kampus.</p>
                   <p>Kasus teranyar yang paling menyita perhatian publik adalah terbongkarnya praktek jual beli ijazah palsu di Tangsel, Banten.  Praktek tersebut terkuak setelah kemenristek Dikti melakukan sidak pada penyelenggaraan wisuda illegal yang diselenggerakan oleh Yayasan Aldiana Nusantara (YAN) di Kampus Universitas Terbuka (UT).</p>
                   <img src="http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2015--09--86738-ribuah-mahasiswa-ikuti-wisuda-bodong-menristek-dikti-gerebek-tiga-kampus.jpg" class="pic2"><p>Dalam penggerebekan tersebut, ditemukan adanya beberapa kampus di bawah naungan Yayasan Aldiana Nusantara (YAN) menyelenggarakan wisuda tanpa ada koordinasi dan izin sebelumnya dari pihak Kopertis atau Kemenristek Dikti. Selain itu, pihak Kemenristek Dikti menduga adanya praktek jual beli ijazah palsu dengan mengacu pada proses perkuliahan dari kampus-kampus tersebut tidak memenuhi prosedur penyelenggara perguruan tinggi.</p>
                   <p>Kasus tersebut menambah daftar panjang tentang praktek jual beli ijazah palsu di Indonesia. Sebelumnya, beberapa kampus digerebek karena terindikasi melakukan praktek yang sama. Pihak kemenristek Dikti bergerak cepat dengan membentuk Tim Evalusi Akademik Perguruan Tinggi  untuk membongkar jaringan mafia ijazah palsu yang kerap merugikan masyarakat dan berupaya memberikan sanksi tegas terhadap pihak-pihak yang terlibat dalam praktek tersebut.  Modus praktek jual beli ijazah palsu  sudah berlangsung lama dibanyak wilayah dan kampus di Indonesia.</p>
                   <p>Tidak hanya kasus jual beli ijazah palsu yang berujung pada sanksi dinonaktifkan oleh pihak Dikti, beberapa kampus yang tersebar di Indonesia juga mengalami nasib penonaktifan karena diduga menyalahi peraturan perundang-undangan.</p>'
    ],

    'TEKANAN'=>[
        'title'=>'TEKANAN BUAT KEMENRISTEK DIKTI',
        'narasi'=>'<img src="http://kemahasiswaan.unej.ac.id/wp-content/uploads/2015/09/LOGO-RISTEK-DIKTI-1.gif" class="pic">
                   <p>Fenomena ijazah palsu yang semakin marak membuat Kemenristik Dikti mendapat banyak kritikan meski beberapa bulan terakhir Kemenristek Dikti berhasil membongkar beberapa kasus jual beli ijazah di beberapa kampus. Upaya tersebut dianggap masih belum total dalam memberantas praktik-prakek ilegal tersebut.</p>
                   <p>Kemenristik Dikti sebagai otoritas tertinggi pendidikan di Indonesia seakan tidak berdaya menekan pelanggaran tersebut padahal hukuman untuk pelanggar jual beli ijazah palsu  sangat berat, penjara selama sepuluh tahun atau denda Rp 1 miliar. Itu berdasar UU No 12 Tahun 2012 tentang Pendidikan Tinggi.</p>
                   <img src="http://www.mediasulbar.com/img/img_artikel/news_55bb232f21222.ijazahpalsu.jpg" class="pic2"><p>Praktek jual beli ijazah palsu masih sangat marak karena lemahnya kontrol dari pihak yang berwajib, terutama dari Kemenristek Dikti yang belum konsisten. Sejauh ini, belum ada program konkret dan langkah-langkah yang konsisten serta sistematis dalam menghapus praktek kejahatan akademik.</p>
                   <p>Untuk menekan praktek kotor tersebut, Kemenritek Dikti dihimbau untuk lebih ketat dalam mengontrol dan aktivitas perkuliahan di kampus-kampus yang terindikasi menyalahi peraturan perundang-undangan, dan menindak  tegas oknum-oknum yang terindikasi melakukan praktek jual beli ijazah palsu.</p>
                   <p>Selain meningkatkan pengawasan serta sanksi tegas terhadap pelaku kejahatan, pelibatan dari berbagai kalangan termasuk dari kepolisian dan masyarakat juga perlu ditingkatkan guna memberantas praktek kotor tersebut yang merugikan masyarakat.</p>'
    ],

    'BISNIS'=>[
        'title'=>'BISNIS AKADEMIK YANG MENGGIURKAN',
        'narasi'=>'<img src="http://cdn.tmpo.co/data/2015/05/30/id_404203/404203_620.jpg" class="pic">
                   <p>Maraknya praktek jual beli ijazah palsu diyakini sebagai ladang bisnis yang cukup menguntungkan. Praktek tersebut muncul karena adanya permintaan dan penyedia dengan beragam modus, kampus tersebut menerbitkan ijazah sarjana strata 1 (S1) kepada penerimanya yang tak menjalani perkuliahan maupun prosedur lain, seperti ujian, mengerjakan tugas akademik, dan persyaratan lain. Namun ada juga yang mengikuti kuliah hanya setahun atau dua tahun tapi memperoleh ijazah S1 dengan membayar sejumlah uang.</p>
                   <p>Peminat atau pengguna dari ijzah illegal tersebut berasal dari beragam kalangan yang memiliki latar belakang yang berbeda.  Diduga pengguna ijazah palsu  tersebut adalah dari pejabat daerah, pejabat pusat, politis, PNS, bahkan ada yang dari menteri.</p>
                   <img src="http://beritapendidikan.net/wp-content/uploads/2015/06/20150529_032620_harianterbit_karikatur_ijazah_palsu.jpg" class="pic2"><p>Bisnis tersebut marak terjadi karena banyaknya permintaan dari para penerima yang ingin memiliki ijazah tanpa harus mengikuti proses perkuliahan. Selain itu, caranya pun sangat mudah dengan harga yang bervariasi sesuai level kampus dan menyediakan segala jenis jurusan.</p>
                   <p>Beberapa motif penggunaan ijazah palsu di antaranya ialah untuk keperluan mencari pekerjaan, untuk kenaikan jabatan, dan ada juga untuk kebanggaan karena memiliki gelar sarjana. Jelas, praktek-praktek jasa menjadi ladang bisnis yang cukup menguntungkan. Baik para penerima atau penyedia jasa.</p>'
    ],

    'TEMUAN'=>[
        'title'=>'DAFTAR TEMUAN PELANGGARAN',
        'narasi'=>'<img src="http://kabartangsel.com/wp-content/uploads/2013/09/Aldiana-Nusantara-300x336.jpg" class="pic">
                   <p>Praktek wisuda abal-abal yang diselenggarakan Yayasan Aldiana Nusantara (YAN) bukan kasus pertama yang berhasil dibongkar oleh Kemenristek Dikti. Sebelumnya, beberapa kampus yang melakukan penyelewangan Ijazah telah disidak dan ditemukan pelanggaran seputar jual beli ijazah.</p>
                   <p>Pada Mei lalu Kementerian berhasil membongkar praktek jual beli ijazah palsu di Kampus Unirsity of Berkley Michigan, Amerika yang dikelola Lembaga Manajemen Internasional Indonesia di Jakarta. Setidaknya ditemukan lebih dari 150 Ijazah palsu. University of Berkley Michigan ternyata kampus yang tidak berizin.</p>
                   <img src="http://brightcove04.o.brightcove.com/4077388032001/4077388032001_4252556311001_150522-TEMPO-CHANNEL-REKTOR-UNIVERSITY-OF-BERKLEY-1.jpg?pubId=4077388032001" class="pic2"><p>Di Kalimantan Tengah, Kementerian berhasil menguak lebih dari 100 guru yang diduga menggunakan ijazah palsu untuk mendapatkan sertifikasi guru.</p>
                   <p>Di bulan Juni, Kementerian menonaktifkan kegiatan perkuliahan di Sekolah Tinggi Ilmu  Ekonomi (STIE) Adhy Niaga di Bekasi, Jawa Barat. Keputusan tersebut dikeluarkan karena disinyalir melakukan praktek jual beli ijazah palsu.</p>
                   <img src="http://www.bekasiurbancity.com/wp-content/uploads/2015/05/Menristek-dikti-akan-ttup-18-PT.jpg" class="pic"><p>Bulan Juli lalu, Kementerian mengancam akan menutup 18 perguruan tinggi di kawasan DKI Jakarta, Bogor, Tangerang, Bekasi, serta Kupang di Nusa Tenggara Timur. Kampus-kampus tersebut kedapatan melakukan jual-beli ijazah.</p>
                   <p>Tiga kampus di Jawa Timur yakni Universitas PGRI Jember, Universitas PGRI Ronggolawi Tuban, dan IKIP Budi Utomo di Malang. dibekukan oleh Kementerian karena  pelanggaran kode etik terkait dengan ijazah.</p>
                   <img src="https://img.okezone.com/content/2015/09/21/65/1218271/ini-profil-kampus-yang-menggelar-wisuda-bodong-kKr2O8NToX.jpg" class="pic2"><p>Sedangkan pada bulan ini, Sebanyak 12 kampus di Sulsel dinonaktifkan karena dianggap bermasalah.</p>
                   <p>Sabtu, 9 September pekan lalu, Kemenristek Dikti kembali menemukan dugaan praktik jual beli ijazah. Praktik itu diduga dilakukan oleh Yayasan Aldiana melalui tiga perguruan tinggi yakni Sekolah Tinggi Teknologi (STT) Telematika, Sekolah Tinggi Keguruan dan Ilmu Pendidikan (STKIP) Suluh Bangsa, dan Sekolah Tinggi Ilmu Tarbiyah (STIT).</p>'
    ],

    'SANKSI'=>[
        'title'=>'SANKSI BAGI KAMPUS NAKAL',
        'narasi'=>'<img src="http://sarjana.co.id/wp-content/uploads/2013/08/Kampus-Ilegal-e1377769037290.jpg" class="pic">
                   <p>Kemenristek Dikti akan menutup kampus-kampus yang terbukti melakukan praktek tersebut. Upaya tersebut dilakukan sebagai langkah untuk menekan dan memberantas praktek-praktek illegal jual beli ijazah.</p>
                   <p>Menteri Natsir mensinyalir ada puluhan kampus yang tersebar di seluruh Indonesia terindikasi terlibat atau melakukan praktek jual beli ijazah.  Umumnya kampus-kampus  yang melakukan praktek illegal tidak terdaftar sebagai kampus yang sah.</p>
                   <p>Sesuai aturan kementerian Riset dan Dikti menyebutkan, perguruan tinggi berstatus nonaktif, maka PT atau Kampus tersebut tidak boleh menerima mahasiswa baru untuk tahun akademik baru, tidak boleh melakukan wisuda.</p>
                   <img src="http://images.detik.com/content/2014/02/28/10/143156_ijazahaspal2.jpg" class="pic2"><p>Tak hanya itu, PT tersebut juga tidak memperoleh layanan Ditjen Dikti dalam bentuk beasiswa, akreditasi, pengurusan NIDN, sertifikasi dosen, hibah penelitian, partisipasi kegiatan Ditjen Kelembagaan IPTEKDIKTI lainnya, serta layanan kelembagaan dari Ditjen Kelembagaan IPTEKDIKTI, tidak memperoleh akses terhadap basis data Pangkalan Data Pendidikan Tinggi untuk pemutakhiran data (PT dan seluruh PRODI ).</p>
                   <p>Lebih lanjut, Kemenristek Dikti akan mengambil langkah hukum untuk membuat jerah para oknum-oknum yang terlibat dalam praktek tersebut.</p>
                   <p>Sanksi akan mengacu pada Undang Undang (UU) Nomor 12 Tahun 2012 tentang Pendidikan Tinggi. Para mafia ijazah palsu akan terkena hukuman pidana. Khususnya pada Pasal 44 ayat (4) adalah penjara selama 10 tahun atau denda Rp 1 miliar</p>'
    ],

    'QUOTE_PENDUKUNG'=>[
        ['from'=>'M. Nasir','jabatan'=>'Menristek RI','img'=>'https://img.okezone.com/content/2015/01/13/56/1091654/fokus-menristek-dikti-kembangkan-riset-teknologi-dna-Gk03TaLtwF.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadnasir544dd3dae9b49','content'=>'"Saya segera mencabut izin dan menutup perguruan tinggi (PT) yang melakukan transaksi jual-beli ijazah dan mengeluarkan ijazah palsu," (M. Nasir, Menristek RI)'],
        ['from'=>'Supriadi Rustad','jabatan'=>'Ketua Tim EKAPT Kemenristekdikti','img'=>'http://jowonews.com/wp-content/uploads/2015/01/supriadi.jpg','url'=>'http://www.bijaks.net/aktor/profile/supriadirustad55ffa681377f3','content'=>'"Saya mengusulkan kepada Dirjen dan Menristek untuk mencabut izin perguruan tinggi tersebut," (Supriadi Rustad, Ketua Tim Evaluasi Kinerja Akademik Perguruan Tinggi Kemenristekdikti)'],
        ['from'=>'Ceu Popong','jabatan'=>'Anggota DPR RI Fraksi Golkar','img'=>'http://static.republika.co.id/uploads/images/detailnews/ceu-popong-_141002012043-272.jpg','url'=>'http://www.bijaks.net/aktor/profile/popongdjunjunan','content'=>'"Masalahnya, permudahan izin itu tidak disertai kelanjutan pengawasan yang ketat sehingga tidak terkontrol, maka inilah yang terjadi," (Ceu Popong, Anggota DPR RI Fraksi Golkar)'],
        ['from'=>'Teuku Riefky Harsa','jabatan'=>'Ketua Komisi X DPR','img'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/0/06/Teuku_Riefky_Harsa.JPG/220px-Teuku_Riefky_Harsa.JPG','url'=>'http://www.bijaks.net/aktor/profile/hteukuriefkyharsa5105f3342ee57','content'=>'"Kami ingin tahu mana saja Perguruan Tinggi yang diindikasikan melakukan hal tersebut. Ini harus dibuka informasinya ke publik supaya masyarakat tidak jadi korban," (Teuku Riefky Harsa, Ketua Komisi X DPR)'],
        ['from'=>'Agus Hermanto','jabatan'=>'Wakil Ketua DPR RI','img'=>'https://upload.wikimedia.org/wikipedia/id/thumb/2/2a/Agus_Hermanto.jpg/200px-Agus_Hermanto.jpg','url'=>'http://www.bijaks.net/aktor/profile/iragushermantomm5104ab0d64699','content'=>'“Bagi universitas bodong harus betul-betul diberantas, ditutup! Jangan sampai kita melaksanakan pendidikan yang tidak betul dan tidak sesuai aturan. Bahkan keluarkan ijazah tidak sesuai,” (Agus Hermanto, Wakil Ketua DPR RI)'],
        ['from'=>'Dadang Rusdiana','jabatan'=>'Anggota DPR RI Komisi X','img'=>'http://www.teropongsenayan.com/foto_berita/201505/17/medium_5Dadang%20Rusdiana%20.jpg','url'=>'http://www.bijaks.net/aktor/profile/dadangrusdiana52f0aadd8fcfb','content'=>'"Praktik ini biasanya melibatkan kerja sama dengan dinas pendidikan setempat. Memang selalu ada oknum di daerah, inilah yang mesti ditertibkan. Kalau misalnya sanksi administrasi, kepala daerah yang harus menertibkan," (Dadang, Anggota DPR RI Komisi X)'],
        ['from'=>'Arrisetyanto','jabatan'=>'Rektor Mercu Buana','img'=>'http://kabaremagazine.com/wp-content/uploads/2012/10/1012-Pepanggihan-Arri.jpg','url'=>'#','content'=>'"Ya semua harus aware. Banyak sekarang perguruan tinggi yang mengeluarkan ijazah palsu. Makanya dulu cukup legalisir sekarang nggak," (Arrisetyanto, Rektor Mercu Buana)'],
        ['from'=>'Airin','jabatan'=>'Walikota Tangsel','img'=>'http://i0.wp.com/liputanbanten.com/wp-content/uploads/2015/07/Post-Image26.jpg?resize=640,320','url'=>'http://www.bijaks.net/aktor/profile/airinrachmidiani5049735804026','content'=>'“Yang pasti harus ada tindakan tegas. Sehingga tidak ada lagi masyarakat yang dirugikan,” (Airin Rachmi Diany, Walikota Tangsel)'],
        ['from'=>'Ridwan Kamil','jabatan'=>'Wali Kota Bandung','img'=>'http://www.uraikan.com/wp-content/uploads/2015/09/ridwan-kamil-diperiksa-kejati.jpg','url'=>'http://www.bijaks.net/aktor/profile/ridwankamil51c7d138bb02b','content'=>'“Bisnis mengelabui seperti itu memang pasarannya juga ada. Pasarnya orang-orang yang potong kompas yang tidak mau kerja keras. Orang yang tidak mau belajar, yang membohongi diri sendiri. Dan jumlah kelompok masyarakat yang gila gelar itu banyak sekali,” (Ridwan Kamil, Wali Kota Bandung)'],
        ['from'=>'Moestar Putrajaya','jabatan'=>'Pengamat Pendidikan','img'=>'http://dct.kpu.go.id/images/foto/DPD/31.%20DKI/17.%20MOESTAR.jpg','url'=>'#','content'=>'"Perguruan tinggi dimaksud berorientasi mengejar keuntungan semata, sementara para mahasiswanya hanya mengejar gelar, baik untuk kepentingan penyesuaian kenaikan jabatan di tempat kerjanya maupun sekedar untuk gengsi-gengsian," (Moestar Putrajaya, Pengamat pendidikan)'],
        ['from'=>'Djohan','jabatan'=>'Ketua LSM BADAR','img'=>'http://4.bp.blogspot.com/-T7QXGYmTfv0/UM0m45UtagI/AAAAAAAAACI/f1KAU_luKdQ/s1600/IMG00582-20121129-1029.jpg','url'=>'#','content'=>'“Tidak lama lagi musim mahasiswa baru akan tiba. LSM BADAR menghimbau kepada masyarakat untuk jeli memilih program studi dan perguruan tinggi. Jangan sampai menyesal, ijazahnya abal-abal karena mengikuti kuliah ilegal,”  (Djohan, Ketua LSM BADAR)'],
        ['from'=>'Ismansyah','jabatan'=>'Jubir FPKS DPRD Belitung Timur','img'=>'http://jariungu.com/images_caleg_2014/large/79940.png','url'=>'#','content'=>'“Berkenaan dengan pemberitaan di media media nasional yang menyebutkan penyelanggaraan wisuda di Kampus Yayasan Aldiana ilegal oleh Kementerian Riset dan Perguruan Tinggi, kami meminta penjelasan. Kami mohon Dinas terkait secepatnya berkoordinsi dan melakukan klarifikasi,” (Ismansyah, Juru Bicara Fraksi PKS DPRD Belitung Timur)'],
    ],

    'QUOTE_PENANTANG'=>[
        ['from'=>'Alimuddin Murthala','jabatan'=>'Ketua Yayasan Aldianan Nusantara','img'=>'http://www.luwuraya.net/wp-content/uploads/2015/09/alimudin-al-murtala.jpg','url'=>'http://www.bijaks.net/aktor/profile/alimuddinalmurthala5608f70545918','content'=>'“Sudah tiga kali kami wisuda, baru kali ini dipersoalkan. Justru saya bingung kenapa kemarin, yang dulu-dulu enggak,” (Alimuddin Al-Murthala, Ketua Yayasan Aldianan Nusantara)'],
        ['from'=>'Habibie Darussalam','jabatan'=>'Ketua Program Beasiswa YAN','img'=>'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpa1/v/t1.0-1/c12.42.155.155/12406_102134663296084_1826937975_n.jpg?oh=c463f2779bc3cf254907b51a43f8c2ca&oe=564107BD&__gda__=1446835818_5622c2ae3674b929a64702404107e2df','url'=>'#','content'=>'"Jadwalnya itu dibagi tiga waktu: pagi, siang, dan malam. Mahasiswa kuliah di sini. Tahun pertama kami wajibkan tinggal di asrama untuk mahasiswa berasal dari pemerintah daerah," (Habibie Darussalam, Ketua Program Beasiswa YAN)'],
        ['from'=>'Dinas Pendidikan Beltim','jabatan'=>'Dinas Pendidikan Belitung Timur','img'=>'http://dindikbelitung.web.id/theme/dindik_belitung/assets/img/splash_dikmen.png','url'=>'#','content'=>'"Kita dari Dinas Pendidikan dalam waktu dekat ini akan melakukan klarifikasi ke Kemenristek Dikti dan Kopertis III Jakarta. Kita akan mencoba mendapatkan keterangan tertulis tentang status Yayasan Aldiana Nusantara termasuk Perguruan Tinggi di lingkup yayasan tersebut," (Dinas Pendidikan Belitung Timur)'],
        ['from'=>'Nanang','jabatan'=>'Mahasiswa YAN','img'=>'http://2.bp.blogspot.com/-_wYkm66OHD8/VTjlnH7XowI/AAAAAAAAAKY/BWR5D_T82dM/s1600/_MG_9102.JPG','url'=>'#','content'=>'"Informasinya terlalu ditelan mentah-mentah sama media, tanpa observasi lebih mendalam. Jangan asal men-jugde, di sini kan ada proses pembelajaran. Padahal kan kami belajar dari semester satu,"  (Nanang, Mahasiswa YAN)'],
    ],

    'VIDEO'=>[
        ['id'=>'SPoIoKt20xk'],
        ['id'=>'9mIAdh0dibM'],
        ['id'=>'RjuzX713GxE'],
        ['id'=>'4RJRx2YYCVo'],
        ['id'=>'Wz7L-jQld6M'],
        ['id'=>'tFQYF8RoQWs'],
        ['id'=>'bvoMZKxYNQM'],
        ['id'=>'mOAv3g9Ujug'],
        ['id'=>'CP5tQj8FasY'],
        ['id'=>'Shwjyqp29iY'],
        ['id'=>'OkBQUAiri8g'],
        ['id'=>'ET2o3-MEN4Y'],
        ['id'=>'uRcrX3_QQxM'],
        ['id'=>'D7PJk-tlnAY'],
        ['id'=>'A-ZGGkrQQrE']
    ],

    'FOTO'=>[
        ['img'=>'http://www.indofakta.com/pictures/20150529004517.jpg'],
        ['img'=>'http://kpghost.filetemp.kaltimpost.co.id/webkp/file/berita/2015/09/21/antisipasi-terbitnya-ijazah-bodong-dari-kampus-perketat-regulasi-penomoran.jpg'],
        ['img'=>'http://images.jurnalasia.com/2015/05/Pengembangan-Kasus-Ijazah-Palsu280515-im-1.jpg'],
        ['img'=>'http://im0.olx.biz.id/images_olxid/27972018_1_644x461_perkuliahan-cepat-konversi-bandar-lampung-kota_rev004.jpg'],
        ['img'=>'http://4.bp.blogspot.com/-BaLJ0gZj3d0/Ur9YU_u3NrI/AAAAAAAAAHA/10qAWwgODGc/s1600/DSC_0265.JPG'],
        ['img'=>'http://i.ytimg.com/vi/kmfnfNFbIn8/hqdefault.jpg'],
        ['img'=>'http://www.radarpekalongan.com/wp-content/uploads/2015/09/Wisuda-Bodong-di-Gerebek-Regulasi-Nomor-Ijasah-Diperketat.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/09/21/337871_kampus-gelar-wisuda-abal-abal_663_382.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2015/05/22/315113_sidak-praktik-jual-beli-ijazah-palsu-di-bekasi_663_382.jpg'],
        ['img'=>'http://rakyatsulsel.com/folder-konten/themes/rakyatsulsel2015/tum.php?src=http://rakyatsulsel.com/folder-konten/uploads/2015/06/Box.jpg&w=668&h=439'],
        ['img'=>'http://i.ytimg.com/vi/o1zbg5ny_-o/hqdefault.jpg'],
        ['img'=>'http://images.detik.com/community/media/visual/2015/09/21/3db3c134-b493-4d64-b6e2-9e9cfdef5947_169.jpg?w=780&q=90'],
        ['img'=>'http://i.ytimg.com/vi/mmYYKHFwQwY/hqdefault.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/pontianak/foto/berita/2013/2/9/Ijazah_Palsu_ilustrasi.jpeg'],
        ['img'=>'http://i.ytimg.com/vi/4assGZ48KCU/hqdefault.jpg'],
        ['img'=>'http://www.pelitaonline.com/uploads/berita/original/ijazah-dan-masalah-struktural-pendidikan-86158.jpg'],
        ['img'=>'http://waspada.co.id/wp-content/uploads/2015/05/Ijazah-palsu1-660x330.jpg'],
        ['img'=>'http://waspada.co.id/wp-content/uploads/2015/06/Universitas-Of-Sumatera-660x330.jpg'],
        ['img'=>'http://www.posmetro-medan.com/wp-content/uploads/2015/03/25-3-RIADI-Mahasiswa-dari-Universitas-Setia-Budi-Mandiri-Dairi-saat-melakukan-unjukrasa-di-Jalan-Gatsu-Medan.jpg'],
        ['img'=>'http://i.ytimg.com/vi/IBntOwMc2yw/hqdefault.jpg'],
        ['img'=>'http://i.ytimg.com/vi/uRcrX3_QQxM/maxresdefault.jpg'],
        ['img'=>'http://infomedan.net/wp-content/uploads/plang-uos.jpg'],
        ['img'=>'http://img1.beritasatu.com/data/media/images/medium/1433385500.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2014/08/16/id_316108/316108_620.jpg'],
        ['img'=>'http://img-static.riaupos.co/menristek.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/07/28/456939/palsu2sah.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/06/10/2238033Ijazah-Palsu780x390.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/surabaya/foto/bank/images/mursyid-mudiantoro-dan-rektor-uts-rugaya-ijazah-palsu-asli_20150604_221836.jpg'],
        ['img'=>'http://i.ytimg.com/vi/D7TaJH6ft7Y/hqdefault.jpg'],
        ['img'=>'http://cdnimage.terbitsport.com/imagebank/gallery/large/20150526_022815_harianterbit_ijazah_palsu.jpg']
    ],

    'BERITA'=>[
        ['img'=>'http://news.bijaks.net/uploads/2015/05/Agus-Hermanto.jpg','shortText'=>'DPR Minta Tutup Kampus Bodong','link'=>'http://www.bijaks.net/news/article/0-139354/dpr-minta-tutup-kampus-bodong'],
        ['img'=>'http://www.bijaks.net/uploads/2015/07/Keluarkan-Ijazah-Palsu-Tiga-Kampus-di-Jatim-Dibekukan-Muhammad-Nasir-Bijaks-627x261.jpg','shortText'=>'Keluarkan Ijazah Palsu Tiga Kampus Di Jatim Dibekukan','link'=>'http://www.bijaks.net/news/article/0-165179/keluarkan-ijazah-palsu-tiga-kampus-di-jatim-dibekukan'],
        ['img'=>'http://news.bijaks.net/uploads/2015/07/Skandal-Seks-Mencuat-Kampus-IPDN-Riau-Dipindah-587x353.jpg','shortText'=>'Skandal Seks Mencuat Kampus IPDN Riau Dipindah','link'=>'http://www.bijaks.net/news/article/0-163509/skandal-seks-mencuat-kampus-ipdn-riau-dipindah/'],
        ['img'=>'http://news.bijaks.net/uploads/2015/06/44755IMG-20130121-00138-370x290.jpg','shortText'=>'Tak Ada Izin Operasi Kampus STIE Adhy Niaga Keberatan Atas Pembekuan Menristek','link'=>'http://www.bijaks.net/news/article/0-144223/tak-ada-izin-operasi-kampus-stie-adhy-niaga-keberatan-atas-pembekuan-menristek'],
        ['img'=>'http://news.bijaks.net/uploads/2015/05/ijazah-palsu1.jpg','shortText'=>'Kampus STIE Adhy Niaga Ancam Polisikan Menristek dan Dikti','link'=>'http://www.bijaks.net/news/article/0-138050/kampus-stie-adhy-niaga-ancam-laporkan-menristek-dan-dikti-ke-polisi'],
        ['img'=>'http://news.bijaks.net/uploads/2015/05/nasir1.jpg','shortText'=>'Menristek Serahkan Kampus Penerbit Ijazah Palsu ke Polisi','link'=>'http://www.bijaks.net/news/article/0-137118/menristek-serahkan-kampus-penerbit-ijazah-palsu-ke-polisi'],
        ['img'=>'http://news.bijaks.net/uploads/2015/05/ijazah-palsu1.jpg','shortText'=>'Kampus Penjual Ijazah Palsu Terancam Pidana','link'=>'http://www.bijaks.net/news/article/0-137529/kampus-penjual-ijazah-palsu-terancam-pidana'],
        ['img'=>'http://www.bijaks.net/uploads/2015/05/18-Kampus-Penjual-Ijazah-Terancam-DitutupBijaks.jpg','shortText'=>'18 Kampus Penjual Ijazah Terancam Ditutup','link'=>'http://www.bijaks.net/news/article/0-133849/18-kampus-penjual-ijazah-terancam-ditutup'],
        ['img'=>'http://news.bijaks.net/uploads/2015/07/palsu.jpg','shortText'=>'Cegah Izasah Palsu, KPU Gandeng Kemenristek Dikti','link'=>'http://www.bijaks.net/news/article/0-172020/cegah-izasah-palsu-kpu-gandeng-kemenristek-dikti'],
        ['img'=>'http://www.bijaks.net/uploads/2015/09/Formappi-Minta-Anggota-DPR-Berijazah-Bodong-Dibongkar-ke-Publik-627x261.jpg','shortText'=>'Formappi Minta Anggota DPR Berijazah Bodong Dibongkar ke Publik','link'=>'http://www.bijaks.net/news/article/0-208879/formappi-minta-anggota-dpr-berijazah-bodong-dibongkar-ke-publik'],
        ['img'=>'http://news.bijaks.net/uploads/2015/07/Ijazah-palsuu-627x353.jpg','shortText'=>'Jual Ijazah Bodong, 3 Universitas di Jawa Timur Dibekukan Menristekdikti','link'=>'http://www.bijaks.net/news/article/0-165203/jual-ijazah-bodong-3-universitas-di-jawa-timur-dibekukan-menristekdikti'],
        ['img'=>'http://news.bijaks.net/uploads/2015/09/23.3.jpg','shortText'=>'Lemah Pengawasan, Jual Beli Ijazah Ilegal Marak','link'=>'http://www.bijaks.net/news/article/0-207197/lemah-pengawasan-jual-beli-ijazah-ilegal-marak'],


    ]
]

?>

<style>
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/kampusabal2/top.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/kampusabal2/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['TEKANAN']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['TEKANAN']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['BISNIS']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['BISNIS']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['TEMUAN']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['TEMUAN']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KAMPUS DI NON-AKTIFKAN</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <p style="text-align: justify;">Berdasarkan data dari Pangkalan Data Perguruan Tinggi (PDPT) terdapat 235 perguruan tinggi yang dinonaktifkan.</p>
                <p style="text-align: justify;">Perguruan tinggi (PT) yang dinonaktifkan karena terindikasi ijazah palsu, rasio dosen dan mahasiswa tidak seimbang, konflik internal kampus, dan alasan menyalahi peraturan perundang-undangan tentang perguruan tinggi.</p>
                <ol><b>Sulawesi Selatan</b>
                    <li>Universitas Veteran Republik Indonesia (UVRI)</li>
                    <li>Universitas Indonesia Timur (UIT)</li>
                    <li>Institut Kesenian Makassar</li>
                    <li>STIH Al-Gazali</li>
                    <li>STIP Tamalate</li>
                    <li>STIK Yapika</li>
                    <li>Stikes Muhammadiyah Sidrap</li>
                    <li>Akademi Teknik Otomotif Makassar</li>
                    <li>Akper Pemda Sengkang</li>
                    <li>Akbid Graha Rabitha Anugrah</li>
                    <li>Poltek Internasional Indonesia Makassar</li>
                </ol><br/>
                <ol><b>Medan</b>
                    <li>Akademi Kebidanan Dewi Maya</li>
                    <li>Akademi Kebidanan Eunice Rajawali Binjai</li>
                    <li>Akademi Kebidanan Jaya Wijaya</li>
                    <li>Akademi Kesehatan Lingkungan Binalita Sudama</li>
                    <li>Akademi Keuangan Perbankan Swadaya Medan</li>
                    <li>Akademi Manajemen Gunung Leuser</li>
                    <li>Akademi Pertanian Gunung Sitoli</li>
                    <li>Akademi Sekretari Manajemen Lancang Kuning</li>
                    <li>Akademi Teknologi Lorena</li>
                    <li>AMIK Intelcom Global Indo Kisaran</li>
                    <li>AMIK Stiekom Sumatera Utara</li>
                    <li>Politeknik Profesional Mandiri</li>
                    <li>Politeknik Trijaya Krama</li>
                    <li>Politeknik Yanada</li>
                    <li>Politeknik Tugu 45 Medan</li>
                    <li>Sekolah Tinggi Bahasa Asing Swadaya Medan</li>
                    <li>Sekolah Tinggi Ilmu Ekonomi Riama</li>
                    <li>Sekolah Tinggi Ilmu Ekonomi Swadaya Medan</li>
                    <li>Sekolah Tinggi Ilmu Hukum Benteng Huraba</li>
                    <li>Sekolah Tinggi Kelautan Dan Perikanan Indonesia</li>
                    <li>Sekolah Tinggi Teknik Graha Kirana</li>
                    <li>Sekolah Tinggi Teknik Pelita Bangsa</li>
                    <li>STKIP Riama</li>
                    <li>Universitas Preston Indonesia</li>
                    <li>Universitas Setia Budi Mandiri</li>
                </ol>
                <ol><b>Jawa Timur</b>
                    <li>Universitas Kanjuruhan</li>
                    <li>Sekolah Tinggi Ilmu Ekonomi (STIE) Indonesia</li>
                    <li>Sekolah Tinggi Ilmu Hukum (STIH) Sunan Giri</li>
                    <li>Sekolah Tinggi Teknik Budi Utomo.</li>
                    <li>Universitas PGRI  Banyuwangi</li>
                    <li>IKIP  PGRI Jember</li>
                    <li>Universitas Bondowoso</li>
                    <li>IKIP Budi Utomo Malang</li>
                    <li>STIE Indonesia Malang</li>
                    <li>ISTP Malang</li>
                    <li>Undar Jombang</li>
                    <li>Universitas Nusantara PGRI Kediri</li>
                    <li>Universitas Teknologi Surabaya (UTS)</li>
                    <li>ITPS Surabaya</li>
                    <li>STIH Sunan Giri Malang</li>
                    <li>STIE ABI Surabaya.</li>
                    <li>Universitas Cakrawala Madiun</li>
                    <li>STIE Pariwisata Satya Widya Surabaya</li>
                    <li>STKIP Tri Buana Surabaya</li>
                    <li>AMIK Aji Jaya Baya Kediri</li>
                    <li>Akademi Tesktil IT Surabaya</li>
                    <li>ABA Webb Surabaya</li>
                    <li>STIT Widya Dharma  Surabaya</li>
                    <li>Akbar Bakti Wiyata Kediri</li>
                    <li>AKAD Peternakan Jember</li>
                    <li>ATN Sidoarjo.</li>
                </ol>

            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['SANKSI']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SANKSI']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">UU PELANGGARAN KAMPUS BODONG</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://bkp.pertanian.go.id/foto_berita/5039undang-undang.jpg" style="width: 50%;margin: 0 auto;display: block;"><br>
                <p style="text-align: justify;">Ada tiga jenis praktik penerbitan ijazah palsu. Pertama, penerbitan ijazah palsu dilakukan oleh Perguruan Tinggi yang tak punya izin dari Kementerian Riset Teknologi dan Pendidikan Tinggi. Kemudian, ada ijazah diperoleh dengan tidak kuliah. Misalnya, baru setahun kuliah sudah dapat ijazah. Selanjutnya, ada legalisasi ijazah yang dipalsukan.</p>
                <p style="text-align: justify;">Penerbitan dan penggunaan ijazah palsu masuk dalam kategori tindak pidana, Berikut undang-undang yang dilanggar terkait kasus tersebut :</p>
                <ol>
                    <li>Undang-undang Nomor 20 Tahun 2003 tentang Sistem Pendidikan Nasional</li>
                    <li>Undang-undang Nomor 12 Tahun 2012 tentang Perguruan Tinggi</li>
                    <li>Kitab Undang-undang Hukum Pidana (KUHP) Pasal 263 ayat 1 tentang Pembuatan Surat Palsu atau Memalsukan Surat</li>
                    <li>Kitab Undang-undang Hukum Pidana (KUHP) Pasal 378 tentang Penipuan</li>
                </ol><br/>
                <img src="http://cdn.sindonews.net/dyn/620/content/2013/04/11/13/736919/NIS2qAjUuP.jpg" style="width: 50%;margin: 0 auto;display: block;">
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KASUS YAN</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <img src="http://im0.olx.biz.id/images_olxid/27972018_1_644x461_perkuliahan-cepat-konversi-bandar-lampung-kota_rev004.jpg" style="width: 50%;margin: 0 auto;display: block;">
                <p style="text-align: justify;">Penggerebekan wisuda yang diselenggarakan oleh Yayasan Aldiana Nusantara di Pondok Cabe, Tangsel, Banten disinyalir sebagai kegiatan wisuda abal-abal karena menyalahi ketentuan perundang-undangan yang berlaku. Ribuan Mahasiswa yang mengikuti proses  wisuda berasal dari  perguruan tinggi yakni; Sekolah Tinggi Teknologi Telematika (STT), Sekolah Tinggi Keguruan dan Ilmu Pendidikan  Suluh Bangsa (STIKIP), Sekolah Tinggi Ilmu Tarbiyah (STIT), dan Sekolah Tinggi Ilmu Ekonomi (STIE) Ganesha,. Keempatnya berada di bawah satu yayasan yaitu Yayasan Aldiana Nusantara.</p>
                <p style="text-align: justify;">Kampus-kampus tersebut statusnya sudah nonaktif dan  tidak pernah melaksanakan proses perkuliahan yang wajar. Para mahasiswa hanya mengikuti kuliah 8-12 bulan namun ada juga yang sama sekali tidak pernah kuliah. Kemerintek Dikti menuding acara wisuda yang dilakukan oleh kampus-kampus tersebut tidak memliki izin karena tidak melaporkan ke pihak Kopertais dan Kementerian. Atas temuan tersebut, wisuda tersebut dicap sebagai wisuda yang berkedok penjualan ijazah palsu.</p>
                <p style="text-align: justify;">Jual-beli ijazah yang selama ini dilakukan oleh Yayasaan Aldiana Nusantara (YAN) sudah berlangsung sejak lama. Modusnya adalah dengan merekrut calon mahasiswa dari berbagai daerah dengan klaim memberikan beasiswa. Fakta yang ditemukan oleh Kementerian seputar pemberian beasiswa di yayasan tersebut tidak benar adanya. Calon mahasiswa masih dibebankan biaya kuliah, dan menyediakan paket ijazah tanpa harus kuliah dengan syarat calon penerima ijazah membayar sejumlah uang, antara 10-15 juta tergantung jurusan.</p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">MENOLAK DICAP ABAL ABAL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <img src="http://lapan6online.com/wp-content/uploads/2015/09/Wisuda-ABal2.jpg" style="width: 50%;margin: 0 auto;display: block;"><br>
                <p style="text-align: justify;">Pihak Yayasan yang menaungi empat kampus nonaktif tersebut berdalih tetap melakukan prosesi perkuliahan sesuai dengan standar nasional perguruan tinggi.  Pendiri Yayasan sekaligus Direktur Perguruan YAN Dr. H .Alimudin Al-Murtala, MM, M.Pd menyatakan bahwa selama ini wisuda yang diselenggarakan oleh kampusnya selalu dilaporkan ke pihak Kopertais dan Kementerian, jadi tidak benar apabila wisuda yang diselenggarakan disebut sebagai wisuda bodong.</p>
                <p style="text-align: justify;">Namun, setelah didesak untuk menunjukkan bukti-bukti terkait dengan perkuliahan yang diselenggarakan oleh Yayasaan tersebut, pihak yayasan tidak mampu menunjukkan data yang akurat. Tim Evalusi Akademik Perguruan Tinggi dari Kementerian yang melakukan sidak  menemukan ketidakcocokan data antara daftar peserta yang diterima dan jumlah peserta yang wisuda.</p>
                <p style="text-align: justify;">Setelah ditelusuri dan mendesak pihak yayasaan, akhirnya Ketua Yayasan Aldiana Nusantara, Alimuddin mengakui kesalahan yang telah dilakukan oleh yayasannya dengan menggelar wisuda ilegal. Ada 3 poin dalam berita acara pemeriksaan. Poin pertama adalah ijazah tidak diberikan ke lebih dari 1.000 peserta wisuda, yang kedua yaitu seluruh biaya wisudawan dikembalikan. Poin ketiga adalah tidak akan mengulangi pelanggaran yang dilakukan</p>
                <p style="text-align: justify;">Kini, nasib keempat kampus dan yayasan tersebut menunggu sanksi keras dari Kementerian. Dan ancaman penutupan dan penonaktifan membayangi yayasan beserta empat kampus di bawah naungannya</p>
                <img src="http://im0.olx.biz.id/images_olxid/27972018_1_644x461_perkuliahan-cepat-konversi-bandar-lampung-kota_rev004.jpg" style="width: 50%;margin: 0 auto;display: block;"><br>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://www.aktual.com/wp-content/uploads/2015/06/Kemenristek-Dikti-Beri-Penghargaan-ke-Peneliti-Berprestasi.jpg" data-toggle="tooltip" data-original-title="KEMENRISTEK"/><p>KEMENRISTEK</p></a>
            <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://hidayatullah.or.id/wp-content/uploads/2015/04/LOGO-DPR-RI.png" data-toggle="tooltip" data-original-title="DPR RI"/><p>DPR RI</p></a>
            <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="https://scontent-sin1-1.xx.fbcdn.net/hphotos-xfa1/v/t1.0-9/394063_201509109939023_1110736820_n.jpg?oh=663fbc2bb86205c44298ef0d425055fa&oe=56A61CD6" data-toggle="tooltip" data-original-title="LSM BADAR"/><p>LSM BADAR</p></a>
            <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Lambang_Kota_Tangerang_Selatan.svg/2000px-Lambang_Kota_Tangerang_Selatan.svg.png" data-toggle="tooltip" data-original-title="Kota Tangsel"/><p>Kota Tangsel</p></a>
            <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://2.bp.blogspot.com/-jt8t6lt0kck/UmTwtSffyhI/AAAAAAAADCU/hJEE7zZireY/s1600/Logo-Pemerintah-Kota-Bandung-transparent.png" data-toggle="tooltip" data-original-title="Kota Bandung"/><p>Kota Bandung</p></a>
            <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://www.riau-global.com/foto_berita/36Kantor%20dprd%20belitung%20timur.jpg" data-toggle="tooltip" data-original-title="DPRD Beltim"/><p>DPRD Beltim</p></a>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENANTANG</h5>
            <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="http://base.detik.com/static/a/4f065e41f726cc760d010bd3/mini/LOGO%20YAN.jpg" data-toggle="tooltip" data-original-title="YAN"/><p>YAN</p></a>
            <a><img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="https://sman11kotatangsel.files.wordpress.com/2015/06/logo-kota-tangerang-selatan.jpg" data-toggle="tooltip" data-original-title="DIKNAS TANGSEL"/><p>Dinas Pendidikan Kota Tangsel</p></a>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENANTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENANTANG'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['FOTO'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['BERITA'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>