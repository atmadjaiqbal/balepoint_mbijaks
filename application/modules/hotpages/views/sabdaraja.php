<?php 
$data = [
    'NARASI'=>[
        'title'=>'GONJANG GANJING SUKSESI SULTAN YOGYAKARTA',
        'narasi'=>'<img src="https://img.okezone.com/content/2015/03/23/340/1123092/pembahasan-suksesi-kraton-yogyakarta-belum-mendesak-0ONcYKLcQG.jpg" class="pic">Keraton Yogyakarta sedang memanas. Dua  buah Sabda raja yang dikeluarkan oleh Sri Sultan Hamenku Buwono X menuai polemik di internal kerajaan.  Sabda Pertama memuat  lima poin termasuk perubahan serta penghapusan  Kalifatullah yang selama ini menjadi gelar bagi raja yang memerintah keraton Yogyakarta.
                    </p><p>Sabda ke dua memuat soal calon pengganti dirinya di tahta kerajaan Yogyakarta.  Dalam sabda ke dua  itu, Sultan HB X mengganti nama baru bagi GKR Pembayun, putri sulung sultan  menjadi Gusti Kanjeng Ratu Mangkubumi Hamemayu Hayuning Bawono Langgeng ing Mataram. Gelar Mangkubumi yang disematkan pada Putri sulung Sultan menjadi pertanda bahwa sang putri akan menjadi suksesor penguasa di keraton Yogyakarta.
                    </p><p><img src="http://cdn-2.tstatic.net/tribunnews/foto/bank/images/pembayu-gkr_20150511_132341.jpg" class="pic2">Sabda raja yang menjadikan GKR Pembayun menjadi putri mahkota mendapat reaksi keras dari kerabat sultan, khususnya dari adik-adik sultan. Penolakan ini mengacu pada tradisi keraton yang selama ini dijalankan. Sri Sultan dianggap telah menabrak tradisi sakral dari leluhur raja-raja Yogyakarta.
                    </p><p>Dalam sejarah keraton, belum pernah ada putri mahkota, selama ini yang ada hanya putra makhkota dan yang berhak duduk disinggasana tertinggi kerajaan adalah seorang laki-laki. Jika Sultan tetap pada pendiriannya untuk mengorbitkan Putri sulungnya sebagai suksesornya, maka hal demikian akan menjadi tradisi baru di lingkungan  Keraton Ngayogyakarta Hadiningrat.'
    ],
    'PROFIL'=>[
        'satu'=>'Kota Yogyakarta',
        'dua'=>'Islam, Kejawen',
        'tiga'=>'Jawa 1755-1950, Belanda 1755-1811; 1816-1942, Inggris 1811-1816,  Jepang 1942-1945, Indonesia 1945-1950',
        'empat'=>'Monarki (kesultanan) Sultan',
        'list'=>[
            ['no'=>'Pertama (1755-1792) - ISKS Hamengku Buwono I'],
            ['no'=>'Sultan terakhir sebelum penurunan status negara (1940-1950; wafat 1988) - ISKS Hamengku Buwono IX'],
            ['no'=>'Sekarang (sejak 1989) - ISKS Hamengku Buwono X Pepatih Dalem (Menteri Pertama)'],
            ['no'=>'Pertama (1755-1799) - Danurejo I'],
            ['no'=>'Terakhir (1933-1945) - Danurejo VIII']
        ]
    ],
    'PROFIL2'=>[
        'narasi'=>'<img src="http://upload.wikimedia.org/wikipedia/id/thumb/9/92/Mataram_Baru_1830.png/250px-Mataram_Baru_1830.png" class="pic2">Kesultanan Ngayogyakarta Hadiningrat adalah negara dependen yang berbentuk kerajaan. Kedaulatan dan kekuasaan pemerintahan negara diatur dan dilaksanakan menurut perjanjian/kontrak politik yang dibuat oleh negara induk Kerajaan Belanda bersama-sama negara dependen Kesultanan Ngayogyakarta. Kontrak politik terakhir antara negara induk dengan kesultanan adalah Perjanjian Politik 1940.
                    </p><p class="font_kecil">Sebagai konsekuensi dari bentuk negara kesatuan yang dipilih oleh Republik Indonesia sebagai negara induk, maka pada tahun 1950 status negara dependen Kesultanan Ngayogyakarta Hadiningrat (bersama-sama dengan Kadipaten Pakualaman) diturunkan menjadi daerah istimewa setingkat provinsi dengan nama Daerah Istimewa Yogyakarta.
                    </p><p class="font_kecil"><img src="http://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Kraton_Yogyakarta_Pagelaran.jpg/260px-Kraton_Yogyakarta_Pagelaran.jpg" class="pic">Nama Yogyakarta adalah perubahan bentuk dari Yodyakarta. Yodyakarta berasal dari kata Ayodya dan Karta. Ayodya diambil dari nama kerajaan dalam kisah Ramayana, sementara karta berarti ramai.
                    </p><p class="font_kecil">Dengan ditandatanganinya Perjanjian Giyanti (13 Februari 1755) antara Pangeran Mangkubumi dan VOC di bawah Gubernur-Jendral Jacob Mossel, maka Kerajaan Mataram dibagi dua. Pangeran Mangkubumi diangkat sebagai Sultan dengan gelar Sultan Hamengkubuwana I dan berkuasa atas setengah daerah Kerajaan Mataram. Sementara itu Sunan Paku Buwono III tetap berkuasa atas setengah daerah lainnya dengan nama baru Kasunanan Surakarta dan daerah pesisir tetap dikuasai VOC.
                    </p><p class="font_kecil">Sultan Hamengkubuwana I kemudian segera membuat ibukota kerajaan beserta istananya yang baru dengan membuka daerah baru (jawa: babat alas) di Hutan Paberingan yang terletak antara aliran Sungai Winongo dan Sungai Code. Ibukota berikut istananya tersebut dinamakan Ngayogyakarta Hadiningrat dan landscape utama berhasil diselesaikan pada tanggal 7 Oktober 1756. Para penggantinya tetap mempertahankan gelar yang digunakan, Hamengku Buwono. Untuk membedakan antara sultan yang sedang bertahta dengan pendahulunya, secara umum, digunakan frasa " ingkang jumeneng kaping...ing Ngayogyakarto Hadiningrat " (bahasa Indonesia: "yang bertakhta ke .... di Yogyakarta"). Selain itu ada beberapa nama khusus antara lain Sultan Sepuh (Sultan yang Tua) untuk Hamengku Buwono II.',
        'status'=>[
            ['no'=>'De facto merdeka (1755-1830)'],
            ['no'=>'De jure negara dependen dari VOC (1755-1799)'],
            ['no'=>'De jure negara dependen dari Republik Bataav/Franco Nederland (1800-1811)'],
            ['no'=>'De jure negara dependen dari EIC (Inggris) (1811-1816)'],
            ['no'=>'De jure negara dependen dari Nederlands Indie (1816-1830)'],
            ['no'=>'Negara dependen dari Nederlands Indie (1830-1842)'],
            ['no'=>'Negara dependen dari Kekaisaran Jepang (1942-1945)'],
            ['no'=>'Negara dependen/daerah istimewa dari Republik Indonesia dengan bentuk monarki persatuan berparlemen (1945-1950)'],
            ['no'=>'Status negara diturunkan secara resmi menjadi status daerah istimewa setingkat dengan provinsi (1950)']
        ]
    ],
    'POLEMIK'=>[
        'narasi'=>'Keputusan Sultan HB X yang memberikan gelar Putri mahkota terhadap putri tertuanya yang tertuang dalam sabda raja, dan meniadakan gelar kafilatullah disinyalir sebagai kepentingan pribadi sang sultan yang menginginkan pucuk kekuasaan di keraton dipegang oleh keturunannya langsung.
                    </p><p class="font_kecil"><img src="https://img.okezone.com/content/2015/05/07/340/1146012/sultan-hb-x-akan-bertemu-adik-adiknya-sore-nanti-pScXOGc8g3.jpg" class="pic2">Pertama, Sultan HB X tidak memiliki anak laki-laki yang dapat diangkat sebagai putra mahkota (calon pengganti raja) sehingga keputusannya memberi gelar putri mahkota terkesan dipaksakan sebagai upaya untuk mendudukkan putrinya penerus tahta.  Kedua, penghapusan gelar Kalifatullah yang sarat dengan tradisi patrialkal merupakan upaya  antisipasi menghadapi kontroversi jika anak sulung Sultan, G.K.R. Mangkubumi, naik takhta. 
                    </p><p class="font_kecil">Sultan HB X menjelaskan bahwa perubahan nama itu merupakan dawuh atau perintah dari Allah SWT melalui leluluhurnya yang tidak bisa dibantah dan harus dijalankan. Penguasa Keraton Jogja ini menepis anggapan pergantian nama putri sulungnya, GKR Pembayun, menjadi GKR Mangkubumi sebagai bentuk pengangkatan putri mahkota penerus tahta kerajaan. Raja Keraton Yogyakarta ini.'
    ],
    'MELABRAK'=>[
        'narasi'=>'<img src="http://arsip.tembi.net/selft/2013/situs/20131121-2.jpg" class="pic">Polemik yang muncul terkait adanya sabda raja bukan soal diangkatnya putri mahkota yang merupakan hal baru dalam tradisi keraton, namun persolannya juga akan berdampak pada perubahan UUK terkait dengan pengisian jabatan Gubernur. Jabatan Gubernur DIY, mekanismenya tidak melalui pemilihan umum tetapi ditetapkan. Ketika menjadi raja maka secara otomatis akan mengisi jabatan sebagai Gubernur, dan yang menjadi persoalan adalah Keistimewaan Yogyakarta yang diatur dalam UU Nomor 13 Tahun 2012 tentang keistimewaan Yogyakarta disebutkan bahwa raja harus berjenis kelamin laki-laki.
                    </p><p class="font_kecil">Bahkan di masyarakat berkembang isu bahwa sabda raja merupakan perjuangan Sultan HB X untuk mendudukkan putri sulungnya meneruskan tahta. Sang Raja menggunakan berbagai upaya, seperti wacana penghilangan salah satu pasal yang ada dalam UUK DIY No 13/2012 tentang Keistimewaan. Yakni, menghilangkan kata istri bagi Gubernur DIY sehingga memberikan kesempatan pada perempuan untuk duduk di kursi kesultanan. Namun, upaya tersebut mendapat reaksi penolakan dari beberapa fraksi dan dari kerabat keraton.
                    </p><p class="font_kecil"><img src="http://cdn.sindonews.net/dyn/620/content/2015/05/07/151/998371/sultan-tak-diakui-lagi-sebagai-raja-kZz.jpg" class="pic2" style="max-width: 175px !important;">Hal tersebut diyakini oleh sebagian masyarakat dan kerabat keraton bahwa sabda raja tidak lebih dari sabda yang sarat kepentingan politik Sang Sultan, karena upaya jalur UU mentok sehingga Sultan HB X melakukan manuver dengan mengeluarkan sabda yang memiliki legitimasi mutlak dan tidak dapat ditolak karena pada prinsipnya sabda raja merupakan kewengan penuh dari Sultan.'
    ],
    'PEWARIS'=>[
        'narasi'=>'Sedikitnya, Sri Sultan Hamengku Buwono X ini mempunyai 5 ( lima ) orang saudara laki-laki yang mempunyai hak waris yang sama atas tahta dari Sri Sultan Hamengku  Buwono IX.',
        'list'=>[
            ['img'=>'http://statik.tempo.co/?id=119331&width=620','no'=>'KBPH Hadikusumo yang merupakan putra dari istri pertamanya Sri Sultan Hamengku Buwono IX, yaitu K.R.Ay. Pintokopurnomo.'],
            ['img'=>'http://images.solopos.com/2015/02/Gusti-Bendoro-Pangeran-Haryo-GBPH-Prabukusumo.jpg','no'=>'GBPH Prabukusumo yang merupakan putra dari istri ketiganya Sri Sultan Hamengku Buwono IX, yaitu KRAy Hastungkoro.'],
            ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/Gusti-Bendoro-Pangeran-Haryo-GBPH-Yudhaningrat.jpg','no'=>'GBPH Pakuningrat yang merupakan putra dari istri keempatnya Sri Sultan Hamengku Buwono IX, yaitu K.R.Ay. Ciptomurti.'],
            ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/871845/big/002181600_1431061613-jes_4_tempo_co.jpg','no'=>'GBPH Hadiwinoto yang merupakan putra dari istri keduanya Sri Sultan Hamengku Buwono IX, yaitu KRAy Windyaningrum.'],
            ['img'=>'http://assets.kompas.com/data/photo/2013/12/31/1845583joyokusumo780x390.jpg','no'=>'GBPH Joyokusumo yang merupakan putra dari istri keduanya Sri Sultan Hamengku Buwono IX, yaitu KRAy Windyaningrum.']
        ]
    ],
    'SABDA1'=>[
        'narasi'=>'<img src="http://us.images.detik.com/content/2015/05/08/10/140842_112559_sabda.jpg" class="pic">Gusti Allah, Gusti Agung, Kuoso Cipto paringono siro kabeh adiningsun, sederek dalem, sentono dalem lan abdi dalem nompo welinge dawuh Gusti Allah, Gusti Agung, Kuoso Cipto lan romo ningsun eyang-eyang ingsun, poro leluhur Mataram wiwit waktu iki ingsun nompo dawuh kanugrahan dawuh Gusti Allah, Gusti Agung, Kuoso Cipto asmo kelenggahan ingsun Ngarso Dalem Sampean Dalem Ingkang Sinuhun Sri Sultan Hamengku Bawono Ingkang Jumeneng Kasepuluh Suryaning Mataram, Senopati ing Kalogo Langenging Bawono Langgeng Langgenging Toto Panotogomo. Sabdo Rojo iki perlu dimangerteni diugemi lan ditindakake yo mengkono sabdo ingsun.
                    <br><span style="font-weight: bold;font-size: 14px;">Artinya :</span>
                    <br>Tuhan Allah, Tuhan Agung, Maha Pencipta, ketahuilah para adik-adik, saudara, keluarg di Keraton dan abdi dalem, saya menerima perintah dari Allah, ayah saya, nenek moyang saya dan para leluhur Mataram, mulai saat ini saya bernama Sampean Dalem Ingkang Sinuhun Sri Sultan Hamengku Bawono Ingkang Jumeneng Kasepuluh Surya ning Mataram, Senopati ing Kalogo, Langenging Bawono Langgeng, Langgeng ing Toto Panotogomo. Sabda Raja ini perlu dimengerti, dihayati dan dilaksanakan seperti itu sabda saya.'
    ],
    'SABDA2'=>[
        'narasi'=>'<img src="http://beritajogja.id/wp-content/uploads/2015/05/Sultan-HB-X.jpg" class="pic">Siro adi ingsun, seksenono ingsun Sampeyan Dalem Ingkang Sinuhun Sri Sultan Hamengku Bawono Ingkang Jumeneng Kasepuluh Suryaning Mataram, Senopati ing Ngalogo Langenging Bawono Langgeng, Langgenging Toto Panotogomo Kadawuhan netepake Putri Ingsun Gusti Kanjeng Ratu Pembayun tak tetepake Gusti Kanjeng Ratu Mangkubumi Hamemayu Hayuning Bawono Langgeng ing Mataram. Mangertenono yo mengkono dawuh ingsun.
                    <br><br><span style="font-weight: bold;font-size: 14px;">Artinya :</span>
                    <br>Saudara semua, saksikanlah saya Sampean Dalem Ingkang Sinuhun Sri Sultan Hamengku Bawono Ingkang Jumeneng Kasepuluh Surya ning Mataram, Senopati ing Kalogo, Langenging Bawono Langgeng, Langgeng ing Toto Panotogomo mendapat perintah untuk menetapkan Putri saya Gusti Kanjeng Ratu Pembayun menjadi Gusti Kanjeng Ratu Mangkubumi Hamemayu Hayuning Bawono Langgeng ing Mataram. Mengertilah, begitulah perintah saya.'
    ],
    'PERIODE'=>[
        ['img'=>'http://1.bp.blogspot.com/-QLJP1x5wfs4/Tq0RMvY3vWI/AAAAAAAAACY/Vu9yqxvNiic/s1600/1.jpg','text'=>'Sri Sultan Hamengkubuwono I <br>(13 Februari 1755 - 24 Maret 1792)'],
        ['img'=>'https://briodharmawan.files.wordpress.com/2013/09/hb2.jpg','text'=>'Sri Sultan Hamengkubuwono II <br>(2 April 1792 akhir 1810) - periode pertama'],
        ['img'=>'http://en.rodovid.org/images/thumb/0/01/HB_III.jpg/180px-HB_III.jpg','text'=>'Sri Sultan Hamengkubuwono III<br>(akhir 1810 akhir 1811) - periode pertama'],
        ['img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQclKQYi6grPk8tP3pf-8I__vEiTfdwlMOtFRtgF7P9NNA1nNSH','text'=>'Sri Sultan Hamengkubuwono IV<br>(9 November 1814 6 Desember 1823)'],
        ['img'=>'http://3.bp.blogspot.com/_QxFCU1D19cw/TRhyTZSloiI/AAAAAAAABD0/q_fBK7PSvzs/s1600/hb5.jpg','text'=>'Sri Sultan Hamengkubuwono V<br>(19 Desember 1823 17 Agustus 1826) -  periode    pertama'],
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRJBTw71HVcepZUTw3aTbVdH6eEWA4F8eulmMYrYELZIS5cas4U','text'=>'Sri Sultan Hamengkubuwono VI<br>(5 Juli 1855 20 Juli 1877)'],
        ['img'=>'http://4.bp.blogspot.com/_1UsyJ19irTY/TSTCN8yVzHI/AAAAAAAAAu4/xlt75OSJAg0/s400/hb7.jpg','text'=>'Sri Sultan Hamengkubuwono VII<br>(22 Desember 1877 29 Januari 1921)'],
        ['img'=>'http://4.bp.blogspot.com/_1UsyJ19irTY/TSTCkoN2rSI/AAAAAAAAAu8/FU--uhr8v4k/s400/hb8.jpg','text'=>'Sri Sultan Hamengkubuwono VIII<br>(8 Februari 1921 22 Oktober 1939)'],
        ['img'=>'http://hendricus-widi-y.blog.ugm.ac.id/files/2012/05/sri-sultan-hamengku-buwono.jpg','text'=>'Sri Sultan Hamengkubuwono IX<br>(18 Maret 1940 2 Oktober 1988)'],
        ['img'=>'http://yogyakarta.panduanwisata.id/files/2014/03/Sri-Sultan-Hamengku-Buwono-X.jpg','text'=>'Sri Sultan Hamengkubuwono X<br>(7 Maret 1989) - saat inilah']
    ],
    'KONTROVERSI'=>[
        ['title'=>'Kasus korupsi Nurdin Halid','no'=>'Pada 13 Agustus 2007, Ketua Umum Nurdin Halid divonis dua tahun penjara akibat tindak pidana korupsi dalam pengadaan minyak goreng. Nurdin Halid kemudian dipenjara ats kasus tersebut, namun posisi sebagai ketua umum tetap ia pegang, dan tetap menjalankan kepemimpinan PSSI dari dalam penjara.'],
        ['title'=>'Reaksi atas LPI','no'=>'Pada Oktober 2010, Liga Primer Indonesia dideklarasikan di Semarang oleh Konsorsium dan 17 perwakilan klub. Namun LPI akhirnya mendapatkan izin dari pemerintah melalui Menteri Pemuda dan Olahraga Andi Mallarangeng'],
        ['title'=>'Kisruh dan Pembentukan komite Normalisasi','no'=>'Kisruh di PSSI semakin menjadi-jadi semenjak munculnya LPI. Ketua Umum Nurdin Halid melarang segala aktivitas yang dilakukan oleh LPI.  Pada 1 April 2011, Komite Darurat FIFA memutuskan untuk membentuk Komite Normalisasi yang akan mengambil alih kepemimpinan PSSI. FIFA juga menyatakan bahwa 4 orang calon Ketua Umum PSSI yaitu Nurdin Halid, Nirwan Bakrie, Arifin Panigoro, dan George Toisutta tidak dapat mencalonkan diri sebagai ketua umum sesuai dengan keputusan Komite Banding PSSI tanggal 28 Februari 2011. Selanjutnya, FIFA mengangkat Agum Gumelar sebagai Ketua Komite Normalisasi PSSI. Pada tanggal 9 Juli 2011 diadakan Kongres Luar Biasa di Solo, Djohar Arifin Husin terpilih sebagai Ketua Umum PSSI periode 2011-2015.'],
        ['title'=>'Pemecatan Alfred Riedl','no'=>'Pemecatan dan penunggakan gaji Alfred Riedl menimbulkan hal yang kontroversial karena pihak PSSI mengaku bahwa Alfred Riedl dikontrak oleh Nirwan Bakrie dan bukan oleh PSSI akan tetapi Alfred Riedl membantah hal tersebut dan membawa persoalan ini ke FIFA dan kasus ini belum terselesaikan.'],
        ['title'=>'Kisruh Indonesia Premier league','no'=>'Pergantian kepengurusan Ketua umum PSSI dari Nurdin Halid ke Djohar Arifin Husin dimulai era kompetisi baru. Dalam pembentukan IPL banyak masalah yang terjadi karena aturan-aturan yang ditetapkan oleh PSSI.Pembentukan IPL mendapat tekanan dari 12 klub sepak bola atau kelompok 14 karena kompetisi berjumlah 24 klub dan 6 klub diantaranya langsung menjadi klub IPL']
    ],
    'KETUA'=>[
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/8/8c/Soeratin.jpg/150px-Soeratin.jpg','url'=>'http://www.bijaks.net/aktor/profile/soeratinsosrosoegondo553f193ec3b53','thn'=>'(1930-1940)','name'=>'Soeratin Sosrosoegondo'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/Artono','thn'=>'(1941-1949)','name'=>'Artono Martosoewignyo'],
        ['img'=>'http://www.tokohindonesia.com/index2.php?option=com_resource&task=show_file&id=47638&type=thumbnail_article','url'=>'http://www.bijaks.net/aktor/profile/maladi517cb841aa32f','thn'=>'(1950-1959)','name'=>'Maladi'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/Abdul','thn'=>'(1960-1964)','name'=>'Abdul Wahab Djojohadi koesoemo'],
        ['img'=>'http://4.bp.blogspot.com/-ixwbJ-KBVtY/TfqMwRfEg8I/AAAAAAAAAM4/i4TKuWWgL_Y/s1600/maulwi.jpg','url'=>'http://www.bijaks.net/aktor/profile/maulwisaelan553f2bbd7b5c5','thn'=>'(1964-1967)','name'=>'Maulwi Saelan'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/b/b9/Kosasih_opurwanegara_ris.jpg','url'=>'http://www.bijaks.net/aktor/profile/kosasihpurwanegara553f1371b17e5','thn'=>'(1967-1974)','name'=>'Kosasih Poerwanegara'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/Bardosono','thn'=>'(1975-1977)','name'=>'Bardosono'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/Moehono','thn'=>'(1977-1977)','name'=>'Moehono'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/commons/f/fa/Ali_Sadikin_%281975%29.jpg','url'=>'http://www.bijaks.net/aktor/profile/letnanjenderalpurnkkoalhalisadikin518093c1a0915','thn'=>'(1977-1981)','name'=>'Ali Sadikin'],
        ['img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'http://www.bijaks.net/aktor/profile/sjarnoebisaid553f1f9fd78b5','thn'=>'(1982-1983)','name'=>'Sjarnoebi Said'],
        ['img'=>'http://upload.wikimedia.org/wikipedia/id/thumb/7/74/Kardono-First-President-of-the-AFF.jpg/220px-Kardono-First-President-of-the-AFF.jpg','url'=>'http://www.bijaks.net/aktor/profile/kardono553f09a62683f','thn'=>'(1983-1991)','name'=>'Kardono'],
        ['img'=>'http://cdn-media.viva.id/thumbs2/2011/03/23/107496_azwar-anas_663_382.jpg','url'=>'http://www.bijaks.net/aktor/profile/irhazwaranas51872cb2788a2','thn'=>'(1991-1999)','name'=>'Azwar Anas'],
        ['img'=>'https://pacpdipciawiebang.files.wordpress.com/2008/02/agum.jpg','url'=>'http://www.bijaks.net/aktor/profile/agumgumelar518768633e53b','thn'=>'(1999-2003)','name'=>'Agum Gumelar'],
        ['img'=>'http://statik.tempo.co/data/2012/06/29/id_128356/128356_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/nurdinhalid530eb1689eb22','thn'=>'(2003-2011)','name'=>'Nurdin Halid'],
        ['img'=>'http://kanalsatu.com/images/20150119-141948_32.jpg','url'=>'http://www.bijaks.net/aktor/profile/djohararifinhusin552770aa7c1db','thn'=>'(2011-2015)','name'=>'Djohar Arifin Husin'],
        ['img'=>'http://www.bolaskor.com/wp-content/uploads/2014/01/La-Nyalla-Mattalitti.jpg','url'=>'http://www.bijaks.net/aktor/profile/lanyallamattalitti553482e51ddbe','thn'=>'(18 April 2015)','name'=>'La Nyalla Mattalitti']
    ],
    'PENYOKONG'=>[
        ['name'=>'Keraton Yogyakarta','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSJSZGTA0oYG6s4tO9Yd4HdovLqo-J1Cu8Y5-jiVI8dHboqTm2tmA' ],
        ['name'=>'Gubernur DIY','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'http://images.detik.com/content/2012/10/10/10/sultandilantikdalam.jpg']
    ],
    'PENANTANG'=>[
        ['name'=>'Para Rayi Dalem (adik-adik Sri Sultan HB X)','jabatan'=>'','page_id'=>'','logo'=>'','img'=>'http://krjogja.com/photos/eebbee8ee5e192e43849f888ebc7ff84.jpg']
    ],
    'PENDUKUNG'=>[
        'institusi'=>[
            ['name'=>'Keraton Yogyakarta','img'=>'http://4.bp.blogspot.com/-Rb53ZZbKtk8/UNaOKMoOX9I/AAAAAAAAACE/dHy53JYRaaY/s1600/jogja1.png','url'=>'http://www.bijaks.net/aktor/profile/kesultananngayogyakartahadiningrat554f1dd8ddaa3'],
            ['name'=>'Mendagri','img'=>'http://www.kemendagri.go.id/media/images/2011/09/23/d/e/depdagrilogo.png','url'=>'http://www.bijaks.net/aktor/profile/kementriandalamnegeri531c1cbb6af24'],
            ['name'=>'UGM','img'=>'http://1.bp.blogspot.com/-ffqU9F79rr8/VPjUfXmw1BI/AAAAAAAABws/KZZSW-P6bz0/s1600/logo-ugm.png','url'=>'http://www.bijaks.net/aktor/profile/nversitasgadjahmadaugm530d548c52047'],
            ['name'=>'UNDIP','img'=>'http://universitaspendidikan.com/wp-content/uploads/2014/01/UNDIP-Semarang.jpg','url'=>'http://www.bijaks.net/aktor/profile/universitasdiponegoro554f20900a3bb']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227'],
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c']
        ],
        'institusi'=>[
            ['name'=>'Himpunan Mahasiswa Yogyakarta','img'=>'https://teguhdwi05.files.wordpress.com/2013/10/1102578_10200317969026436_2007480736_o.jpg','url'=>''],
            ['name'=>'DPRD Kota Yogyakarta','img'=>'http://berita.suaramerdeka.com/konten/uploads/2014/08/yogya.jpg','url'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdaerah52f09395adcaf'],
            ['name'=>'Muhammadiyah','img'=>'http://upload.wikimedia.org/wikipedia/en/thumb/4/47/Muhammadiyah_Logo.svg/1021px-Muhammadiyah_Logo.svg.png','url'=>'http://www.bijaks.net/aktor/profile/ppmuhammadiyah5296b45eefcaa'],
            ['name'=>'NU','img'=>'http://www.realita.co/photos/bigs/20141112200922nu.jpg','url'=>'http://www.bijaks.net/aktor/profile/nahdlatululamanu53310bea6f11d'],
            ['name'=>'Kraton Surakarta','img'=>'http://farm9.staticflickr.com/8076/8259494589_e2f071446d_b.jpg','url'=>'http://www.bijaks.net/aktor/profile/keratonsurakartahadiningrat55503d7c12cef'],
            ['name'=>'DPR RI','img'=>'http://beri.biz/wp-content/uploads/2012/11/DPR-RI-.jpg','url'=>'http://www.bijaks.net/aktor/profile/badankehormatandprri539509594a6a1'],
            ['name'=>'Warga Jogja','img'=>'http://us.images.detik.com/customthumb/2015/05/07/10/123726_img2015050700527.jpg?w=460','url'=>''],
            ['name'=>'Kesultanan Banten','img'=>'http://bisniswisata.co/fileuploadmaster/31430kotagede_01.jpg','url'=>'http://www.bijaks.net/aktor/profile/kesultananbanten5550511e67f23']
        ]
    ],
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Taufik Kurniawan','jabatan'=>'Kepala Pusat Penerangan (Kapuspen) Kemendagri','img'=>'http://jurnalpatrolinews.com/wp-content/uploads/2011/04/Taufik-Kurniawan.jpg','url'=>'http://www.bijaks.net/aktor/profile/irtaufikkurniawan50c7e1f8cb585','content'=>'UU Keistimewaan DIY levelnya Presiden dan DPR. Yang nantinya UU itu akan menyesuaikan dengan sabda raja dan pasti yang kerja Presiden dan DPR. Tentu, Mendagri akan melakukan atas perintah Presiden'],
        ['from'=>'Doddy Riyadmadji','jabatan'=>'Wakil Ketua DPR RI, Fraksi PAN','img'=>'http://www.kemendagri.go.id/media/images/2014/12/17/d/o/dodi.jpg','url'=>'','content'=>'Jelas UU Keistimewaan Yogyakarta sudah diatur bahwasanya negara kita kan menganut negara demokrasi sungguhpun ada keistimewaan dalam kaitan dengan Yogjakarta tersebut'],
        ['from'=>'Dr Sri Margana','jabatan'=>'Sejarawan Universitas Gadjah Mada (UGM)','img'=>'http://4.bp.blogspot.com/-pNOl_nfKE_I/VQlR9hjU9MI/AAAAAAAAB3g/O8GlxGH3y6o/s1600/LECT_UGM_Dr%2BSri%2BMargana.jpg','url'=>'http://www.bijaks.net/aktor/profile/srimargana554f18492397d','content'=>'Sultan berhak memilih gelarnya sendiri seperti juga raja-raja sebelumnya'],
        ['from'=>'GKR Mangkubumi','jabatan'=>'Putri Sulung Sri Sultan','img'=>'http://sorotjogja.com/wp-content/uploads/2015/05/00074194.jpg','url'=>'http://www.bijaks.net/aktor/profile/gustikanjengratugkrmangkubumi554f0c9522dba','content'=>'Saya sih monggo kemawon (terserah saja). Saya ngundi dawuh (menerima perintah) dari Gusti Allah melalui Ngarso Dalem (Sri Sultan)'],
        ['from'=>'Tjahjo Kumolo','jabatan'=>'Mendagri RI','img'=>'http://www.jurnalparlemen.com/timthumb.php?src=http://www.jurnalparlemen.com/mod/news/images/normal/cfc0083436f9faa3927755ffc1a5ba1d.jpg&w=620&h=410&zc=1','url'=>'http://www.bijaks.net/aktor/profile/tjahjokumolosh50f4fc41d9f04','content'=>'Urusan resmi keraton kan ya dari Sultan, kan bukan ranah kami'],
        ['from'=>'Jusuf Kalla','jabatan'=>'Wapres RI','img'=>'http://upload.wikimedia.org/wikipedia/id/1/18/Jusuf_Kalla_PMI.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'Di Inggris itu perempuan jadi ratu. Masa abad 21 masih ada diskriminasi? Jadi tidak masalah'],
        ['from'=>'Sri Sultan HB  X','jabatan'=>'Gubernur DIY Yogyakarta','img'=>'http://images.solopos.com/2012/05/SULTAN-SUR1-206x310.jpg','url'=>'http://www.bijaks.net/aktor/profile/srisultanhamengkubuwonox519067cd71584','content'=>'Biar sekarang yang nggak setuju berkoar-koar semua dulu lah']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'GBPH Prabukusumo','jabatan'=>'','img'=>'https://bagusrosyid.files.wordpress.com/2010/12/prabu.jpg','url'=>'','content'=>'Iya, kami meminta maaf. Karena kedua pendiri kerajaan Mataram Islam, Ki Ageng Pemanahan, Ki Ageng Giring disebut-sebut Ngarso Dalem'],
        ['from'=>'GBPH Yudhoningrat','jabatan'=>'','img'=>'http://cdn-2.tstatic.net/jogja/foto/bank/images/gbph-yudhaningrat-gusti-yudha__20150506_181655.jpg','url'=>'','content'=>'Ini mimpi buruk. Kembali ke awal, masalah anaknya yang jadi putri mahkota itu tidak pernah kita kenal dan tidak kita harapkan. Itu berlawanan dengan aturan pokok kekhalifahan Keraton Ngayogyokarto'],
        ['from'=>'Tubagus (Tb) Abbas Waseh','jabatan'=>'salah satu keturunan Kesultanan Banten','img'=>'http://3.bp.blogspot.com/-HADqYAfWSr4/UEy-FA347SI/AAAAAAAAAhk/zF8pgt2H98c/s1600/576928_3655283903119_1449517623_n.jpg','url'=>'','content'=>'Sabda dan dawuh Sultan itu berlaku untuk internal keluarga, tidak untuk umum. Maka penyelesaiannya juga internal keluarga'],
        ['from'=>'Heni Astiyanto','jabatan'=>'Wakil Ketua Muhammadiyah Kota Yogyakarta','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Penghapusan gelar pemimpin agama itu praktis mengubah pakem Keraton Yogya yang selama ini beridentitas sebagai Kerajaan Mataram Islam'],
        ['from'=>'Jadul Maulana','jabatan'=>'Wakil Ketua Tanfidziyah Nahdlatul Ulama Yogyakarta','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Bukan untuk tujuan diskriminatif, tapi membimbing pemimpin agar bisa menjalankan perilaku sesuai ajaran Allah. Ini sifatnya universal'],
        ['from'=>'Gusti Kanjeng Ratu (GKR) Wandansari','jabatan'=>'anggota keluarga Kasunanan Kraton Surakarta','img'=>'https://soloraya.files.wordpress.com/2013/10/gusti-moeng3-778x1024.jpg','url'=>'','content'=>'Mestinya Solo juga terluka, karena membahas mengenai bumi Mataram tidak hanya berorientasi pada Yogyakarta saja, tetapi juga Surakarta'],
        ['from'=>'Antok','jabatan'=>'warga Jogja','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Ini warga Kauman nggak terima lafadz Islamnya dihilangi, padahal yang buat ini kan pendahulunya orang Islam semua'],
        ['from'=>'Fadli Zon','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://cdnimage.terbitsport.com/image.php?width=650&image=http://cdnimage.terbitsport.com/imagebank/gallery/large/20150406_122831_harianterbit_Fadli_Zon.jpg','url'=>'http://www.bijaks.net/aktor/profile/fadlizon5119d8091e007','content'=>'Bila ini terkait masalah pewarisan tahta maka sesuai undang-undang dikatakan penerus tahta adalah laki-laki. Kita berpegang dulu pada UU, dalam waktu dekat akan datang ke Yogjakarta'],
        ['from'=>'Susiyanto','jabatan'=>'pemerrhati sejarawan','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Saya sebenarnya prihatin, akhir-akhir ini memang ada beberapa hal yang bersifat negatif berkaitan dengan Islam dari keraton Yogyakarta, terutama pemikiran sultannnya yang agaknya mulai terpengaruh oleh pluralisme agama, dibadingkan untuk mengikuti leluhur- leluhurnya yang memiliki kedekatan dengan Islam'],
        ['from'=>'Dhanang Respati Puguh','jabatan'=>'Sejarawan UNDIP','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png','url'=>'','content'=>'Tentu hal ini berbeda dengan kebiasaan yang sudah dilakukan pada kerajaan Jawa sebelumnya'],
        ['from'=>'Djoko Suryo','jabatan'=>'Sejarahwan UGM','img'=>'http://us.images.detik.com/content/2010/12/01/158/djoko-suryo-2.jpg','url'=>'','content'=>'Makanya untuk adanya penghapusan kata Khalifatullah pada gelar Sultan dan juga ada pengangkatan Putri Raja menjadi Putri Mahkota serta terjadi polemik dirasakan oleh keluarga Keraton, semua itu kita sebagai masyarakat di luar Keraton belum mengetahui apa alasan tepatnya']
    ],
    'VIDEO'=>[
        ['id'=>'nqXxEmgOsOo'],
        ['id'=>'fvuoXh55k1Q'],
        ['id'=>'nQWs4_O35d8'],
        ['id'=>'eoGNfTeu9k4'],
        ['id'=>'2iwgXCmsDU4'],
        ['id'=>'d0lYVdHZHQU'],
        ['id'=>'EEBM2O1Nwd8'],
        ['id'=>'h_sCVdfgzO4'],
        ['id'=>'KWRC5lBJidE'],
        ['id'=>'R2HoBrRslsY'],
        ['id'=>'eqNeNqESzWA'],
        ['id'=>'I-P5PeSNHdE'],
        ['id'=>'0JOi7uE-BmQ'],
        ['id'=>'2E2j_TDPPGk']
    ],
    'FOTO'=>[
        ['img'=>'https://img.okezone.com/content/2015/05/05/340/1144743/sabda-raja-kembali-dikeluarkan-keraton-yogya-ditutup-Mrhjstplfx.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2015/03/06/1243187eka-49780x390.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/05/09/340/1147035/sabda-raja-irasional-VC5FXoP6aJ.jpg'],
        ['img'=>'http://cdn.jokowinomics.com/static/images/2015/05/Raja-Jogja-640x427.jpg'],
        ['img'=>'http://cdn.metrotvnews.com/dynamic/content/2015/05/08/123997/NIEl49DS1X.jpg?w=668'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/870537/big/078320000_1430918520-sri.jpg'],
        ['img'=>'http://cdn.img.print.kompas.com/getattachment/77d9f5d4-35d7-43bd-bf22-3b860d81f8b2/187858'],
        ['img'=>'http://www.radarjogja.co.id/wp-content/uploads/2015/03/GKR-Pembayun-KRT-Yudhahadiningrat-.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2012/05/10/id_119332/119332_620.jpg'],
        ['img'=>'http://t1.gstatic.com/images?q=tbn:ANd9GcT51Xe5WNA2ZPbgDjPDxMK6YhGqbRTointSgoaYDDZEVvNYkkab-A'],
        ['img'=>'http://cdn-media.viva.id/thumbs2/2010/12/18/101663_sri-sultan-hamengku-buwono-x-dan-para-pengawal-kraton-yogyakarta_663_382.jpg'],
        ['img'=>'http://us.images.detik.com/content/2015/05/08/10/131531_112559_sabda.jpg'],
        ['img'=>'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjqndw4aAJ0CB7tjuvpO61HD8eT8EGCSNAYTeLwdOUdrT_41knqw'],
        ['img'=>'http://img2.bisnis.com/bandung/posts/2015/05/08/533210/gkr-tempo.jpg'],
        ['img'=>'http://i.ytimg.com/vi/rpx4tB5MXjE/0.jpg'],
        ['img'=>'http://beritajogja.id/wp-content/uploads/2015/03/Pembacaan-Sabdotomo-dihadiri-kerabat-dalem-Kraton.jpeg'],
        ['img'=>'http://cdn0-a.production.liputan6.static6.com/medias/871791/big-portrait/043388600_1431058567-jog_3_yogyakarta_panduanwisata_id.png'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/05/05/189/997641/mengapa-adik-lelaki-sultan-tak-hadiri-sabda-raja-Be7.jpg'],
        ['img'=>'http://citraindonesia.com/wp-content/uploads/2015/05/abdi-dalem-keraton-yogya-daerah-foto.cahselo-670.jpg'],
        ['img'=>'http://static.republika.co.id/uploads/images/kanal_sub/raja-keraton-yogyakarta-sri-sultan-hamengkubuwono-x-_150510134531-100.jpg'],
        ['img'=>'http://t1.gstatic.com/images?q=tbn:ANd9GcTixFlfVxXv9ck5VSPJ2cg4XNe1Adfs6uJmhLRQoFbklc7GBUet'],
        ['img'=>'http://cahselo.com/wp-content/uploads/2014/04/8-agustus-grebeg-syawal-1434-h-kraton-yogyakarta-keluarkan-7-gunungan1.jpg'],
        ['img'=>'https://ikbalsiami.files.wordpress.com/2011/11/n6303c-01011.jpg'],
        ['img'=>'http://stat.ks.kidsklik.com/statics/files/2012/10/1349933741565722417.jpg']
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
        display: inline-block;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-right: 10px;
        margin-bottom: 10px;
        float: left;
        border: 1px solid black;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 120px;
        width: 100%;
    }
    .clear {
        clear: both;
    }
    p {
        text-align: justify;
    }    
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}

</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/sabdaraja/top_kcl.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo strtoupper("Kesultanan Ngayogyakarta Hadiningrat"); ?></span></h5>
        <div id="accordion" class="panel-group" style="height: 340px;">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <img src="http://upload.wikimedia.org/wikipedia/commons/thumb/2/28/Royal_Seal_of_the_Sultanate_of_Yogyakarta.svg/600px-Royal_Seal_of_the_Sultanate_of_Yogyakarta.svg.png" class="col-xs-6 col-sm-6" style="margin-left: -15px;">
                <div class="col-xs-6" style="padding-left: 0px;">
                    <span style="font-size: 1.2em;font-weight: bold;">Ibu Kota : <?php echo $data['PROFIL']['satu'];?></span><br>
                    Agama : <?php echo $data['PROFIL']['dua'];?><br>
                    Bahasa : <?php echo $data['PROFIL']['tiga'];?>
                </div>
                <div class="col-xs-12" style="margin-left: -15px;">
                    <span style="font-size: 1.2em;font-weight: bold;">Pemerintahan :</span><br>
                    <?php echo $data['PROFIL']['empat'];?>
                    <ol style="list-style-type: square;">
                        <?php
                        foreach ($data['PROFIL']['list'] as $key => $val) {
                            echo "<li>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">POLEMIK</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['POLEMIK']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">MELABRAK ATURAN, MENERUSKAN TRAH</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['MELABRAK']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SABDA RAJA I</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SABDA1']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SABDA RAJA II</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SABDA2']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PROFIL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['PROFIL2']['narasi'];?></p>                
                <p style="text-align: justify;">
                    <b style="font-size: 12px;">SEJARAH</b><br>
                    Pembentukan : Perjanjian Giyanti - 13 Februari 1755<br>
                    Penurunan status : Pengundangan UU No. 3 Tahun 1950 - 4 Maret 1950
                </p>            
                <p style="text-align: justify;">
                    <b style="font-size: 12px;">STATUS POLITIK</b><br>
                    <ol style="list-style-type: square;margin-left: -30px !important;">
                        <?php
                        foreach ($data['PROFIL2']['status'] as $key => $val) {
                            echo "<li style='margin-left: 20px;' class='font_kecil'>".$val['no']."</li>";
                        }
                        ?>
                    </ol>
                </p>
                <p style="text-align: justify;">
                    <b style="font-size: 12px;">LAIN-LAIN</b><br>
                    Hymne Sultan : Gendhing Monggang<br>
                    Sebagian wilayah didirikan Negara Kepangeranan Pakualaman pada 1813
                </p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo strtoupper("Periode Waktu Silsilah Raja Yogyakarta"); ?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <ul style="margin: 0px 0px 0px -2px;">
                    <?php
                    foreach($data['PERIODE'] as $key=>$val) {
                        ?>
                        <a href="<?php echo $val['url'];?>">
                            <li class="col-xs-12 dukung">
                                <img src="<?php echo $val['img'];?>"/>
                                <p><?php echo $val['text']; ?></p>
                            </li>
                        </a>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo strtoupper("Pewaris Tahta Kesultanan"); ?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <p style="text-align: justify;"><?php echo $data['PEWARIS']['narasi'];?></p> 
                <div class="col-xs-12" style="margin: 0px;padding: 0px;">
                    <ol style="margin-left: -20px;">
                        <?php
                        foreach ($data['PEWARIS']['list'] as $key => $val) {
                            echo "<li><img src='".$val['img']."' style='float: right;margin-left: 10px;max-width: 50px;margin-top: 5px;'>".$val['no']."</li><div class='clear'></div>";
                        }
                        ?>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo strtoupper("11 Adik HB X"); ?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <p style="text-align: justify;"><?php echo $data['PEWARIS']['narasi'];?></p> 
                <div class="col-xs-12" style="margin: 0px;padding: 0px;">
                    <ol style="list-style-type: square;margin-left: -20px;">
                        <li class="font_kecil">GBPH Prabukusumo</li>
                        <li class="font_kecil">‎GBPH Pakuningrat</li>
                        <li class="font_kecil">GBPH Cakraningrat</li>
                        <li class="font_kecil">GBPH Suryodiningrat</li>
                        <li class="font_kecil">GBPH Suryomataram</li>
                        <li class="font_kecil">GBPH Suryanegara</li>
                        <li class="font_kecil">GBPH Hadisuryo</li>
                        <li class="font_kecil">GBPH Hadinegoro</li>
                        <li class="font_kecil">GBPH Condrodiningrat</li>
                        <li class="font_kecil">KGPH Hadiwinoto</li>
                        <li class="font_kecil">GBPH Hadisuryo</li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PENYOKONG</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <ul style="margin: 0px 0px 0px -2px;">
                    <li class="col-xs-6 dukung">
                        <img src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSJSZGTA0oYG6s4tO9Yd4HdovLqo-J1Cu8Y5-jiVI8dHboqTm2tmA"/>
                        <p>Ketaron Yogyakarta</p>
                    </li>
                    <li class="col-xs-6 dukung">
                        <img src="http://images.detik.com/content/2012/10/10/10/sultandilantikdalam.jpg"/>
                        <p>Gubernur DIY</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PENANTANG</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <ul style="margin: 0px 0px 0px -2px;">
                    <li class="col-xs-12 dukung">
                        <img src="http://krjogja.com/photos/eebbee8ee5e192e43849f888ebc7ff84.jpg"/>
                        <p>Para Rayi Dalem (adik-adik Sri Sultan HB X)</p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">INSTITUSI PENDUKUNG</h5>
            <?php
            foreach($data['PENDUKUNG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://m.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>

        <div class="black boxcustom3" style="padding-left: 20px;">
            <h5 style="padding-top: 15px;">INSTITUSI PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['institusi'] as $key=>$val){
                ?>
                <a href="<?php echo $val['url'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $val['img'];?>" alt="<?php echo $val['name'];?>">
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENENTANG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div> 
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>



