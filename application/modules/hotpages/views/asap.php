<?php
$data = [
    'line1'=>[
        'title'=>'DARURAT ASAP INDONESIA',
        'narasi'=>'
                   <p>Setelah hampir setengah abad, bencana asap di Indonesia masih saja terjadi. Bahkan kian meluas di sejumlah 
                   wilayah Sumatera, Kalimantan, dan Jawa. Kita seakan tak pernah belajar, bahkan cenderung mengabaikannya.</p>
                   <p>Lahan yang terbakar pada 2015, berdasarkan data dari Kementerian Lingkungan Hidup dan Kehutanan, terdata 
                   ada di 12 provinsi titik rawan kebakaran hutan.</p>
                   <p>Lahan terbakar terluas berada di Riau, mencapai 2.025,42 hektar (ha). Provinsi dengan luas lahan terbakar 
                   signifikan lainnya ialah Kalimantan Barat (900,20 ha), Kalimantan Tengah (655,78 ha), Jawa Tengah (247,73 ha), 
                   Jawa Barat (231,85 ha), Kalimantan Selatan (185,70 ha), Sumatera Utara (146 ha), Sumatera Selatan (101,57), 
                   dan Jambi (92,50 ha).</p>
                   <img src="http://blog.act.id/wp-content/uploads/2015/08/fakta-kebakaran-hutan.jpg" class="pic2"/>
                   <p>Jumlah titik panas di Sumatera mencapai 944 titik dan di Kalimantan 222 titik. Kebakaran hutan dan lahan 
                   pun diperkirakan masih terus berlangsung, bahkan hingga ke taman nasional.</p>'
    ],

    'line2'=>[
        'title'=>'DAMPAK YANG DITIMBULKAN',
        'narasi'=>'<img src="http://dralf.net/wp-content/uploads/2015/08/kuda.jpg" class="pic">
                   <p>Kebakaran hutan menimbulkan banyak dampak merugikan baik dari segi ekologi hingga ekonomi. Beberapa dampak 
                   yang sangat merugikan dari kebakaran hutan dan lahan gambut. Diantaranya :</p>
                   <p>Pertama, Hilang dan rusaknya habitat satwa liar. Indonesia memiliki beragam satwa liar yang hidup didalamnya. 
                   Beberapa wilayah hutan di Indonesia juga merupakan kawasan Taman Nasional yang juga  merupakan habitat asli dan 
                   penting bagi sejumlah spesies yang dilindungi. Kebakaran hutan dan lahan gambut mengakibatkan dampak negatif 
                   langsung bagi satwa-satwa tersebut sehingga statusnya kini terancam punah. Hutan dan lahan gambut yang terbakar 
                   juga tidak akan bisa dipulihkan seperti sedia kala, karena butuh ratusan tahun untuk mendapatkan besar pohon 
                   serta keanekaragaman hayati yang alami di hutan tropis.</p>
                   <p>Kedua, Meningkatkan emisi gas rumah kaca penyebab perubahan iklim. Lahan gambut dan hutan yang secara alami 
                   merupakan tempat untuk menyerap oksigen bebas berlebih yang terdapat di atmosfer, memiliki peran penting dalam 
                   mengendalikan perubahan iklim. Apabila lahan gambut dan hutan terbakar maka justru akan melepaskan karbon dan 
                   emisi gas lainnya ke udara sehingga berkontribusi dalam pemanasan global yang kini terjadi di seluruh belahan dunia.</p>
                   <img src="http://img.lensaindonesia.com/thumb/350-630-1/uploads--1--2015--09--12546-kabut-asap-selimuti-bandara-kualanamu-medan-kebakaran-hutan-menebal-11-penerbangan.jpg" class="pic2">
                   <p>Ketiga, Mengganggu kesehatan manusia. Kebakaran hutan dan lahan gambut menyebabkan polusi udara dan berdampak 
                   langsung bagi masyarakat yang tinggal disekitar wilayah hutan baik yang dekat ataupun yang tinggal puluhan kilometer 
                   dari lokasi kebakaran. Asap yang ditimbulkan dapat tersebar lebih dari puluhan kilometer. Seperti kebakaran hutan riau 
                   lalu yang mengakibatkan meningkatnya jumlah korban akibat ISPA (Infeksi Saluran Pernapasan) dan total masyarakat yang 
                   terpapar partikel asap mencapai lebih dari 55 ribu jiwa hingga akhirnya puluhan sekolah terpaksa diliburkan sepekan lebih.</p>
                   <p>Keempat, Merugikan negara secara ekonomi. Akibat asap yang mengganggu wilayah sekitar lokasi hutan, banyak aktivitas manusia 
                   yang terganggu hingga terpaksa berhenti mulai dari sekolah hingga perdagangan. Oleh karena itu juga berdampak buruk pada 
                   perputaran ekonomi di wilayah sekitar, sehingga mengalami kerugian. Selain ekonomi, asap yang sampai ke wilayah negara 
                   tetangga juga dapat berakibat buruk bagi hubungan bilateral Indonesia.</p>'
    ],

    'line3'=>[
        'title'=>'PRESIDEN TINDAK TEGAS PEMBAKAR HUTAN',
        'narasi'=>'<img src="http://archives.portalsatu.com/wp-content/uploads/2015/09/Jokowi-tinjau-hutan-terbakar.jpg" class="pic">
                  <p class="rightcol">Tindakan membuka lahan dengan cara membakar oleh segelintir oknum telah membuat malu bangsa Indonesia di mata dunia akibat 
                  asap tebal yang ditimbulkan. Presiden Joko Widodo telah menerima nama para pembakar hutan dan memerintahkan untuk segera dihukum.</p>
                  <p class="rightcol">Kabarnya menteri kehutanan sudah melaporkan nama-nama pada Presiden, dan nama-nama itu sudah masuk ke tangan polisi, Nama-nama itu 
                  terdiri dari perseorangan dan perusahaan.</p> 
                  <p class="rightcol">Presiden Jokowi telah menyiapkan 3 hal untuk membuat jera oknum perusak hutan itu. Pertama adalah proses hukum setegas-tegasnya, 
                  kemudian pencabutan hak administratif serta izin perusahaan, dan selanjutnya adalah upaya pencegahan. Salah satu upaya pencegahan 
                  tersebut yakni menyiapkan disinsentif oleh Menko Perekonomian bagi pelaku pembakaran hutan.</p>
                  <img src="http://oketimes.com/photo/dir102014/oketimes_2-Pelaku-Pembakaran-Hutan-Ditangkap-Polres-Kampar.jpg" class="pic">
                  <p class="rightcol">Memang sudah banyak insentif ekonomi yang memungkinkan suburnya pembakaran lahan konsesi, sampai dengan saat ini pemerintah tetap 
                  serius menangani kebakaran hutan. Dari segi kesehatan, pembagian masker oleh Kementerian Kesehatan juga dilakukan.</p> 
'
    ],

    'line4'=>[
        'title'=>'KEKHAWATIRAN NASA',
        'narasi'=>'<img src="http://gfw.blog.s3.amazonaws.com/2015/09/fires3.png" class="pic">
                  <p class="rightcol">Badan antariksa Amerika Serikat NASA memberikan tanggapan terkait bencana kebakaran hutan Indonesia di tahun 2015, bencana kabut asap 
                  akibat kebakaran hutan di Indonesia akan mencatat rekor terburuk jika musim panas terus berlanjut.</p>  
                  <p class="rightcol">Jika ramalan musim panas yang lebih panjang terbukti, tahun 2015 akan masuk ranking salah satu kejadian paling parah. Sistem cuaca 
                  El Nino membuat kondisi di Indonesia lebih kering dari biasanya.</p>
                  <p class="rightcol">Lembaga Global Fire Emissions yang merupakan bagian dari NASA memperkirakan sekitar 600 juta ton gas rumah kaca telah dilepas 
                  akibat kebakaran hutan tahun ini, kurang lebih setara dengan output tahunan gas yang dilepas Jerman.</p>'
    ],

    'line5'=>[
        'title'=>'PENYEBAB KEBAKARAN HUTAN',
        'narasi'=>'<img src="http://img.bisnis.com/posts/2015/09/17/473413/titik-api.jpg" class="pic">
                  <p>Fenomena cuaca El Nino, yang menyebabkan hampir seluruh wilayah kepulauan Indonesia menjadi kering, bukanlah biang kebakaran hutan di Sumatera 
                  dan Kalimantan. Berdasarkan laporan sebuah lembaga riset, faktor manusia merupakan penyebab kebakaran hutan di sejumlah provinsi.</p>
                  <p>Lebih dari 90 persen kebakaran hutan disebabkan karena manusia, atau sengaja dibakar. Meskipun cuaca panas dan kering memperparah dan memperluas 
                  titik api di sejumlah provinsi seperti Riau, Jambi, dan Pontianak dan menyebabkan kabut asap pekat, pemantik apinya adalah manusia.</p>
                  <p>Pembakaran hutan merupakan cara yang paling murah untuk mengubah lahan hutan menjadi kebun kelapa sawit, sekaligus mendongkrak harga lahan. 
                  Riset CIFOR mencatat bahwa terjadi kenaikan harga lahan sekitar Rp 3 juta setelah pembakaran lahan. Sebelum terbakar, harga lahan berkisar 
                  Rp 8 juta, dan setelah terbakar menjadi Rp 11 juta per hektar. Setelah ditanami sawit, harganya berlipat lagi, sekitar Rp 50 juta, dan bisa 
                  mencapai Rp 100 juta per hektar apabila ditanami sawit bibit unggul.</p>
                  <img src="http://www.mongabay.co.id/wp-content/uploads/2012/12/rizal-Membuka-Lahan-Perkebunan-Sawit.jpg" class="pic2">
                  <p>Saat ini kelapa sawit menjadi "emas hijau" yang banyak diincar investor, dari mulai perusahaan raksasa hingga investor perorangan karena 
                  merupakan investasi paling menguntungkan. Karenanya, pembakaran hutan, menurut riset CIFOR, merupakan cara menghasilkan uang dengan mudah.</p>
                  <p>Berdasarkan pantauan Rappler di Riau dan Jambi beberapa waktu lalu, industri kelapa sawit masih merupakan primadona ekonomi. Tak hanya perusahaan 
                  besar yang memiliki jutaan hektar kebun, masyarakat kecil juga ikut bermain dalam bisnis ini dari mulai puluhan hingga ratusan hektar.</p>'
    ],

    'line6'=>[
        'title'=>'KABUT ASAP MASUKI NEGARA TETANGGA',
        'narasi'=>'<img src="http://cdn.tmpo.co/data/2013/06/21/id_195304/195304_620.jpg" class="pic">
                  
                  <p class="rightcol">Badan Nasional Penanggulangan Bencana (BNPB)  menyatakan angin membawa asap akibat kebakaran hutan dan lahan di Sumatera 
                  dan Kalimantan masuk ke Singapura dan sebagian wilayah Serawak, Malaysia. Angin yang mengarah ke Timur Laut menyebabkan asap 
                  dari Riau, Jambi dan Sumatera Selatan masuk ke wilayah Singapura.</p> 
                  <p class="rightcol">Adapun asap yang berasal dari Kalimantan Barat terbawa angin ke Timur Laut sampai ke bagian barat Serawak dan membuat 
                  wilayah itu berkabut asap.</p>
                  <p class="rightcol">Ada 665 titik panas terdeteksi di wilayah Sumatera, termasuk di Sumatera Selatan (475), Bengkulu (10), Jambi (83), 
                  Bangka Belitung (45), Lampung (25), Riau (12), Sumatera Barat (8), Kepulauan Riau (5), Sumatera Utara dan Aceh (1). 
                  Namun satelit tidak mendeteksi keberadaan titik panas di Kalimantan.</p>
                  <img src="http://www.dw.com/image/0,,18708982_303,00.jpg" class="pic">
                  <p class="rightcol">Asap juga menurunkan kualitas udara di Riau dan Jambi, menjadikannya berbahaya untuk kesehatan sehingga sekolah-sekolah 
                  diliburkan untuk menghindarkan anak dari dampak kabut asap. BNPB juga menyatakan bahwa hampir 80 persen wilayah Kalimantan 
                  tertutup asap dengan tingkat kepekatan sedang hingga tinggi.</p>
                  <p class="rightcol">Upaya pemadaman terus dilakukan di semua daerah yang hutan dan lahannya terbakar untuk mengatasi masalah kabut asap. Namun 
                  tampaknya pembakaran juga masih terus berlangsung.</p>'
    ],

    'line7'=>[
        'title'=>'PERUSAHAAN PEMBAKAR HUTAN',
        'narasi'=>'
                  <img src="http://cdn.tmpo.co/data/2013/07/01/id_198088/198088_620.jpg" class="pic">
                  <p class="rightcol">Berdasarkan data kejadian kebakaran hutan dan lahan pada 2015, sebanyak 27 korporasi sedang menjalani penyidikan oleh Polri. 
                  Jumlah tersangka korporasi pada tahun ini lebih banyak dibandingkan pada 2013 dan 2014 yang masing-masing hanya 4 korporasi dan 
                  1 korporasi. Empat perusahaan di Sumatera Selatan yang masuk tahap penyidikan di Mabes Polri, yakni PT Bumi Mekar Hijau, 
                  PT Waimusi Agro Indah, PT Tempirai Palm Resources, dan PT Rembang Agro Jaya. PT Bumi Mekar Hijau merupakan anak usaha Sinarmas Group 
                  yang berafiliasi dengan Asia Pulp and Paper (APP).</p>
                  <img src="http://potretterkini.com/asset/foto_berita/b882225ee0cfcdbaf690d542b1b6e92f.jpg" class="pic">
                  <p class="rightcol">Selain empat perusahaan tersebut, masih ada 14 perusahaan lain di Sumsel yang tengah diselidiki oleh Mabes Polri. Sementara itu, 
                  tiga korporasi yang ditetapkan sebagai tersangka pembakaran lahan di Kalimantan Tengah, yakni PT Makmur Bersama Asia di Kapuas, 
                  PT Gobalindo Alam Perkasa di Sampit, dan PT Antang Sawit Perdana di Pulang Pisau. Tersangka korporasi pembakaran lahan di Riau 
                  masih dalam tahap penyelidikan Mabes Polri. Oleh karena itu, hanya inisial yang dicantumkan dalam data, yakni PT HSL, PT MWR, 
                  dan PT RAPP.</p>'
    ],

    'line8'=>[
        'title'=>'KEBAKARAN SEMAKIN MELUAS',
        'narasi'=>'<img src="http://media.viva.co.id/thumbs2/2013/06/21/210990_sebaran-titik-api-dan-asap-di-sumatera--2013-_663_382.jpg" class="pic2">
                  
                  <p>Sejak 1960-an hingga saat ini, kebakaran terjadi berulang kali, bahkan mengalami peningkatan jumlah titik api.</p>
                  <p>Rekapitulasi luas kebakaran hutan per provinsi di Indonesia tahun 2010-2015 dalam situs Kementerian Lingkungan Hidup 
                  juga menunjukkan hal itu. Dibandingkan tahun 2010, luas lahan terbakar meningkat puluhan kali lipat. Di Jambi, contohnya, 
                  di tahun 2010, lahan terbakar hanya 2,5 ha. Tahun 2014 meningkat menjadi 3.470 ha.</p>
                  <img src="http://cdn.sindonews.net/dyn/620/content/2015/02/26/24/969135/kebakaran-hutan-di-riau-meluas-rapp-jatuhkan-bom-air-fxJ.jpg" class="pic">
                  <p>Sumber lain menyebutkan, kebakaran di Jambi dalam satu bulan terakhir telah menyebar ke areal seluas 40.000 ha. 
                  Sebanyak 33.000 ha di antaranya merupakan kebakaran gambut yang masih terus meluas.</p>
                  <p>Sementara itu, di Kalimantan Tengah, tahun ini, berdasarkan data pemadaman kebakaran BPBD kabupaten/kota se-Kalimantan 
                  Tengah, luas lahan terbakar sejak Januari hingga 10 September mencapai 940,9 ha. Pada 2014, kebakaran lahan menghanguskan 4.022 ha.</p>'
    ],

    'line9'=>[
        'title'=>'ANCAMAN NEGARA TETANGGA',
        'narasi'=>'
                  <img src="http://static.inilah.com/data/berita/foto/2240605.jpg" class="pic2">
                  <p>Kali ini Singapura menyatakan rasa marahnya karena kabut asap tebal menyelimuti negara-kota tersebut sehingga membuat sekolah-sekolah ditutup.</p>
                  <img src="http://static.theglobeandmail.ca/495/news/world/article12701409.ece/ALTERNATES/w620/Singapore+Asia+Haze4.JPG" class="pic">
                  <p>Singapura mengambil langkah pencegahan darurat dengan menutup semua SD dan SMP untuk pertama kalinya sejak kabut asap terjadi. 
                  Masker-masker gratis juga dibagikan di tempat-tempat layanan masyarakat bagi warga lanjut usia dan kelompok rentan.</p>'
    ],

    'line10'=>[
        'title'=>'PASAL PIDANA BAGI PEMBAKAR HUTAN',
        'narasi'=>'
                  <img src="http://cdn-2.tstatic.net/kaltim/foto/bank/images/ilustrasi-penjara_20150708_211538.jpg" class="pic">
                  <img src="http://jambi-independent.co.id/media/k2/items/cache/87a72099ffdf58f8548c1fad1f88bfe5_XL.jpg" class="pic2">
                  <p>Perusahaan yang bergerak di sektor perkebunan dan kehutanan itu akan dijerat oleh tiga undang-undang, yakni 
                  UU No. 41/1999 tentang Kehutanan, UU No. 18/2004 tentang Perkebunan, dan UU No. 32/2009 tentang Perlindungan dan Pengelolaan 
                  Lingkungan Hidup. Pasal yang terkait dengan aktivitas pembakaran lahan dan hutan, yakni Pasal 78 ayat 3 UU No. 41/1999 dengan 
                  ancaman hukuman maksimal 15 tahun penjara dan denda Rp5 miliar, serta Pasal 48 ayat 1 UU No. 18/2004, dan Pasal 108 UU 
                  No. 32/2009 dengan ancaman hukuman maksimal 10 tahun dan denda Rp10 miliar.</p>'
    ],

    'line11'=>[
        'title'=>'MENGAPA KEBAKARAN TERUS TERJADI',
        'narasi'=>'
                  <img src="http://3.bp.blogspot.com/-3cig48iiW2Y/VB7roAKt45I/AAAAAAAAEIE/vvT85SbvyQ8/s1600/xx.png" class="pic">
                  
                  <p>Meskipun kita sudah dapat menentukan ukuran kebakaran dan dimana lokasinya, masih banyak hal yang belum kita ketahui. 
                  Mengapa pemerintah Indonesia gagal untuk menerbitkan informasi dimana perusahaan sawit, kertas, dan kayu beroperasi, masih 
                  banyak kesenjangan informasi serta masalah seputar akurasi terkait hal ini.</p>
                  <p>Tersedianya peta batas konsesi serta kepemilikan lahan terbaru dapat memperbaiki koordinasi di antara institusi pemerintah 
                  yang berusaha menghentikan api, peningkatan penegakkan hukum di sekitar kawasan, serta tentu saja, akuntabilitas yang lebih 
                  baik untuk perusahaan maupun institutsi pemerintah terkait.</p>
                  <p>Lebih lanjut di lapangan menjadi prioritas yang mendesak, termasuk penelitian dan survei mendalam untuk dapat mengerti 
                  proporsi pembakaran yang dilakukan oleh perusahaan besar dibandingkan dengan operasi ukuran menengah maupun kecil. Pemerintah 
                  maupun organisasi peneliti independen, perlu secara cepat melakukan investasi lebih untuk mengerti akar masalah dari kebakaran 
                  ini serta menyusun program yang lebih baik untuk mencegah kebakaran.</p>
                  <img src="https://www.indoloka.com/blog/wp-content/uploads/2015/08/kebakaran-hutan.jpg" class="pic2">
                  <p>Terkait dengan hal ini, beberapa progres telah dibuat. Pemerintah Indonesia dan Singapura, serta kelompok ASEAN yang 
                  lebih besar, sedang melakukan usaha-usaha untuk menurunkan risiko kebakaran. Deteksi api dan usaha pemadaman telah ditingkatkan, 
                  serta penegakkan hukum Indonesia telah melakukan beberapa penangkapan yang signifikan. Pemerintah negara-negara ASEAN telah 
                  sepakat untuk bekerja sama dan membagi data mengenai titik api dan penggunaan lahan. Lebih lanjut, banyak perusahaan yang 
                  telah, sejak saat itu, mengumumkan secara public kebijakan tidak menggunakan pembakaran, serta melakukan investasi terhadap 
                  system pengawasan dan pengendalian api mereka.</p>
                  <p>Akan tetapi, seperti yang ditunjukkan oleh angka yang belum pernah terjadi sebelumnya ini, usaha-usaha tersebut belum 
                  menjawab pertanyaan apa yang diperlukan untuk menghentikan krisis ini. Nasib hutan, kualitas air, serta kesehatan masyarakat 
                  Indonesia serta orang-orang dan hewan liar yang hidup dari pada hutan ini bergantung pada penegakkan hukum, informasi yang transparan, 
                  koordinasi yang lebih baik antara institusi pemerintah, serta tanggung jawab perusahaan yang lebih baik lagi.</p>'
    ],

    'line12'=>[
        'title'=>'WAPRES BUAT SINGAPURA TERKEJUT ?',
        'narasi'=>'
                  <img src="http://media.galamedianews.com/news/150928062919-wapres-jk-soal-asap-singapura-jangan-hanya-bicara.jpg" class="pic">
                  
                  <p>Wakil Presiden Indonesia, Jusuf Kalla, telah mengulangi pernyataan kontroversial yang pertama kali dia sampaikan pada bulan 
                  Maret silam. Bahwa tetangga Indonesia harus bersyukur untuk kualitas udara yang baik dalam waktu 11 bulan karena diprediksi kabut 
                  asap yang diakibat dari kebakaran hutan masuk ke wilayah Singapura dan malaysia berlangsung selama kurang lebih 1 bulan.</p> 
                  <p>Mungkin hal inilah yang membuat Menteri Luar Negeri Singapura Shanmugam menjadi kaget, sebagaimana yang diungkapkan melalui akun 
                  Facebook-nya yang seolah bernada kaget dengan pernyataan seorang pejabat top di Indonesia. </p>
                  <img src="http://cdn.tmpo.co/data/2014/02/13/id_263322/263322_620.jpg" class="pic2">
                  <p>Dalam tulisan di akun itu, dirinya meluapkan kekesalannya pada salah satu pejabat senior di Indonesia yang tak disebutkan 
                  identitasnya. Dirinya menyesalkan bahwa pernyataan itu dikeluarkan oleh pejabat di tingkat senior dari Indonesia, yang terkesan 
                  mengabaikan kesehatan masyarakat di negaranya, dan masyarakat di Indonesia sendiri akibat dari kabut asap yang ditimbulkan dari 
                  kebakaran hutan yang tak kunjung padam. Untuk diketahui Tingkat PSI (Indeks Kualitas Udara) di wilayah Indonesia berada pada hampir 
                  di level 2.000.</p>'
    ],        

    'line13'=>[
        'title'=>'TANGGAPAN ISTANA',
        'narasi'=>'
                  <img src="http://www.redtophotel.com/d/redtop/media/istana_merdeka.jpg" class="pic">
                  
                  <p>Kepala Staf Kepresidenan Teten Masduki menegaskan Pemerintah Indonesia tidak tinggal diam dan terus bekerja keras menangani kebakaran 
                  hutan. Namun karena kebakaran hutan yang begitu luas, maka tidak mudah memadamkan api. Pemerintah Indonesia sedang mencari alternatif 
                  solusi untuk menghentikan kebakaran hutan.</p>
                  <p>Begitupun Juga Kementerian Koordinator Bidang Ekonomi tengah memikirkan disintensif ekonomi untuk membuat pelaku pembakaran hutan 
                  memiliki risiko ekonomi jika tidak menjaga konsesi lahan hutannya. Selain itu, salah satu upaya serius pemerintah menangani kebakaran 
                  hutan yaitu mengevaluasi lahan gambut.</p>
                  <img src="http://www.goriau.com/assets/imgbank/01022015/b7790f62ab0085863abc1b25u-28839.jpg" class="pic2">
                  <p>Sebelumnya, Presiden meminta tata kelola lahan gambut yang buruk segera diperbaiki. Hal ini mengatasi kebakaran di lahan gambut yang 
                  terjadi setiap tahun. Adapun Pemerintah Daerah mewajibkan perusahaan pemegang hak pengelolaan lahan gambut membangun embun yang bisa 
                  dimanfaatkan untuk perendaman (rewetting) tanah gambut.</p>
                  <p>Presiden Jokowi juga menyampaikan bahwa langkah konkrit mengatasi kebakaran lahan gambut dengan membangun embung air sudah dilakukan 
                  di Kalimantan Tengah. Selain itu, Presiden menegaskan komitmen untuk menindak tegas pembakar hutan yang sudah menyengsarakan harus didukung 
                  oleh semua pihak. Pemerintah pusat, pemerintah daerah, swasta, dan masyarakat harus bersatu padu, bekerja bersama untuk menuntaskan upaya 
                  penanggulangan kebakaran lahan ini.</p>'
    ],

    'institusiPendukung' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/kantorstaffpresidenri560d3c4dbb179','image'=>'https://aws-dist.brta.in/2015-09/original_700/0_0_1200_786_09848a5f9010a98ff848eedcc39cd09708678bed.jpg','title'=>'Staff Kepresidenan'],
        ['link'=>'http://www.bijaks.net/aktor/profile/majelispermusyawaratanrakyat51da5f26a4a63','image'=>'http://www.rmol.co/images/berita/normal/379078_09054322052015_mpr_ri11.jpg','title'=>'MPR RI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/kementerianlingkunganhidupri52e48dc2f0ae1','image'=>'http://bantenpos.co/wp-content/uploads/2015/09/kementerian-lhk.jpg','title'=>'Kementerian LHK'],
        ['link'=>'http://www.bijaks.net/aktor/profile/departemenhukumdanham5313f91a94735','image'=>'https://pbs.twimg.com/profile_images/1428328945/logopolhukamfb.jpg','title'=>'Kemenkopulhakam'],
        ['link'=>'http://www.bijaks.net/aktor/profile/kementeriankesehatan52f9bdfc6e729','image'=>'http://ioti.or.id/images/Depkes.gif','title'=>'Kemenkes'],
        ['link'=>'http://www.bijaks.net/aktor/profile/tentaranasionalindonesiatni531c8511eca9f','image'=>'http://4.bp.blogspot.com/-_lsC67QEjUk/U51ekIXP7JI/AAAAAAAARw8/Cc8DRvTiYv0/s1600/LOGO+BARU+TNI.png','title'=>'TNI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/kepolisiannegararipolri536756e743fd0','image'=>'http://3.bp.blogspot.com/-3BeLPC6nrQk/U_72RXnlfVI/AAAAAAAAAGw/fW03QMRCEgw/s1600/Logo%2BPOLRI%2Bfull%2Bcolor.jpg','title'=>'Polri'],
        ['link'=>'http://www.bijaks.net/aktor/profile/badanintelijennegara51a2cdfd46386','image'=>'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/The_National_Intelligence_Agency_(Indonesia).svg/2000px-The_National_Intelligence_Agency_(Indonesia).svg.png','title'=>'BIN'],
        ['link'=>'http://www.bijaks.net/aktor/profile/badannasionalpenanggulanganbencana5328f43cd78de','image'=>'http://www.ngawikab.go.id/home/wp-content/uploads/BNPB-AKAN-BENTUK-UPT-BENCANA-DI-JAWA-TIMUR.jpg','title'=>'BNPB'],
        ['link'=>'http://www.bijaks.net/aktor/profile/pemprovsumateraselatan54e3fe3891cdc','image'=>'http://www.transformasinews.com/file/2014/06/Logo-Pemprov-Sumsel.jpg','title'=>'Pemda Sumsel'],

    ],

    'institusiPenentang' => [
        ['link'=>'http://www.bijaks.net/aktor/profile/dewanperwakilanrakyatdpr51da66b0a7ac4','image'=>'http://tjatursaptoedy.info/wp-content/uploads/2013/08/dpr-ri.jpg','title'=>'DPR RI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/dewanperwakilandaerah531d2cecd99f7','image'=>'http://1.bp.blogspot.com/-RbdMACDExhg/U_fdZqk_bXI/AAAAAAAAARo/MSZurQO9a9M/s1600/dpd-ri.png','title'=>'DPD RI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/wahanalingkunganhidupindonesiawlhi5370cfc3cd6a3','image'=>'http://img.antaranews.com/new/2011/01/ori/20110112014233walhi.jpg','title'=>'WALHI'],
        ['link'=>'http://www.bijaks.net/aktor/profile/greenpeace531532e1a7358','image'=>'https://goodpitch.org/uploads/cache/org_image/max_600_400_greenpeace.jpg','title'=>'Green Peace'],
        ['link'=>'#','image'=>'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpf1/v/t1.0-1/c88.26.323.323/s160x160/76555_104779199590604_2586015_n.jpg?oh=2ac50410d61a7cab5e799a55bfcc570e&oe=56A0FEC4&__gda__=1452864290_a8569c60dd10ff1a55e0d46579381195','title'=>'LSM Amanat Penderitaan Rakyat (Ampera)'],
        ['link'=>'#','image'=>'http://nexlogistic.com/images/Asperindo.jpg','title'=>'Asperindo'],
        ['link'=>'#','image'=>'http://sinarharapan.co/sh_img/15/09/10/l/150910091ilustrasikebakaranhutan.jpg','title'=>'Komunitas AWAS ASAP'],

    ],

    'SANKSI'=>[
        'title'=>'SANKSI BAGI KAMPUS NAKAL',
        'narasi'=>'<img src="http://sarjana.co.id/wp-content/uploads/2013/08/Kampus-Ilegal-e1377769037290.jpg" class="pic">
                   <p>Kemenristek Dikti akan menutup kampus-kampus yang terbukti melakukan praktek tersebut. Upaya tersebut dilakukan sebagai langkah untuk menekan dan memberantas praktek-praktek illegal jual beli ijazah.</p>
                   <p>Menteri Natsir mensinyalir ada puluhan kampus yang tersebar di seluruh Indonesia terindikasi terlibat atau melakukan praktek jual beli ijazah.  Umumnya kampus-kampus  yang melakukan praktek illegal tidak terdaftar sebagai kampus yang sah.</p>
                   <p>Sesuai aturan kementerian Riset dan Dikti menyebutkan, perguruan tinggi berstatus nonaktif, maka PT atau Kampus tersebut tidak boleh menerima mahasiswa baru untuk tahun akademik baru, tidak boleh melakukan wisuda.</p>
                   <img src="http://images.detik.com/content/2014/02/28/10/143156_ijazahaspal2.jpg" class="pic2"><p>Tak hanya itu, PT tersebut juga tidak memperoleh layanan Ditjen Dikti dalam bentuk beasiswa, akreditasi, pengurusan NIDN, sertifikasi dosen, hibah penelitian, partisipasi kegiatan Ditjen Kelembagaan IPTEKDIKTI lainnya, serta layanan kelembagaan dari Ditjen Kelembagaan IPTEKDIKTI, tidak memperoleh akses terhadap basis data Pangkalan Data Pendidikan Tinggi untuk pemutakhiran data (PT dan seluruh PRODI ).</p>
                   <p>Lebih lanjut, Kemenristek Dikti akan mengambil langkah hukum untuk membuat jerah para oknum-oknum yang terlibat dalam praktek tersebut.</p>
                   <p>Sanksi akan mengacu pada Undang Undang (UU) Nomor 12 Tahun 2012 tentang Pendidikan Tinggi. Para mafia ijazah palsu akan terkena hukuman pidana. Khususnya pada Pasal 44 ayat (4) adalah penjara selama 10 tahun atau denda Rp 1 miliar</p>'
    ],

    'quotePendukung'=>[
      ['from'=>'Ir. H. Jokowi','jabatan'=>'Presiden RI','img'=>'http://rmol.co/images/berita/normal/471192_04185709072015_jokowi_smile_1','url'=>'http://www.bijaks.net/aktor/profile/irjokowidodo50ee1dee5bf19','content'=>'"TNI juga bergerak, pemerintah daerah juga harus bergerak. Semua bergerak untuk memadamkan api dan membebaskan asap dengan target operasi yang jelas,"'],
      ['from'=>'H. M Jusuf Kalla','jabatan'=>'wakil presiden RI','img'=>'http://www.duajurai.com/wp-content/uploads/2014/10/Jusuf-Kalla-3.jpg','url'=>'http://www.bijaks.net/aktor/profile/muhammadjusufkalla54e1a16ff0b65','content'=>'"Saya bicara  agak keras (dalam pertemuan) agar jangan cuma bicara saja, ayo datang dan bantu (memadamkan). Persoalan asap bukan hanya soal menangani api tetapi juga hutan dan lingkungannya,"'],
      ['from'=>'Siti Nurbaya','jabatan'=>'Menteri LHK','img'=>'http://www.rmol.co/images/berita/normal/76298_01574208032015_Siti_Nurbaya1.jpg','url'=>'http://www.bijaks.net/aktor/profile/sitinurbaya51e6043555e9b','content'=>'"Mereka harus tahu, kita ini ‎kerja luar biasa. Saya enggak tahu presiden mana yang bisa bekerja seperti presiden Jokowi,"'],
      ['from'=>'Teten Masduki','jabatan'=>'Kepala Staff Kepresidenan','img'=>'http://cdn.tmpo.co/data/2013/02/06/id_165915/165915_620.jpg','url'=>'http://www.bijaks.net/aktor/profile/tetenmasduki51a78cb913c9c','content'=>'"Sebagian nama perusahaan yang melanggar pasti sudah di tangan polisi, dan arahan Presiden sudah jelas agar memberikan sanksi tegas bagi pelaku yang terbukti melakukan pembakaran hutan,"'],
      ['from'=>'Sutiyoso','jabatan'=>'Kepala BIN','img'=>'http://dekandidat.com/cms/wp-content/uploads/2013/11/bang_yos.jpg','url'=>'http://www.bijaks.net/aktor/profile/letjentnipurnsutiyoso511c2b6deeb46','content'=>'"Kami kan memang BIN di daerah bisa dimanfaatkan semua departemen, karena itu memang tugas kami. Tadi Bu Siti ngomong begitu. Ya tentu kami akan tindaklanjuti dengan informasi-informasi yang bisa kami berikan,"'],
      ['from'=>'Sutopo Purwo Nugroho','jabatan'=>'Kepala Pusat Data Informasi dan Humas BNPB','img'=>'http://niassatu.com/wp-content/uploads/2015/07/Sutopo-Purwo-Nugroho.jpg','url'=>'http://www.bijaks.net/aktor/profile/sutopopurwonugroho5289dea4efe94','content'=>'"Asap di Riau akumulasi dari asap dari Riau ditambah dari Jambi dan Sumsel,"'],
      ['from'=>'Syamsul Maarif','jabatan'=>'Kepala BNPB','img'=>'http://www.ciputranews.com/media/images/boni/syamsul_maarif.jpg','url'=>'http://www.bijaks.net/aktor/profile/syamsulmaarif5333b64a22350','content'=>'"Presiden meminta kepala daerah agar tidak ragu-ragu menyatakan darurat asap. Bencana ini bukan bencana kebakaran hutan, tetapi bencana darurat asap,"'],
      ['from'=>'Bambang Hendro','jabatan'=>'Sekjen Kementerian Lingkungan Hidup dan Kehutanan','img'=>'http://riaumandiri.co/assets/berita/87116799829-bambang-endoryono.jpg','url'=>'#','content'=>'"Konsentrasi kita ke sumber api. Kita bicara lahan dan hutan. Kita sepakat untuk sesegera mungkin memadamkan api dan menghilangkan asap,"'],
      ['from'=>'Sugarin','jabatan'=>'Kepala BMKG Riau','img'=>'http://www.bmkg.go.id/BMKG_Pusat/ImagesWeb/BMKG_Pusat_201422711359.jpg','url'=>'#','content'=>'"Konsentrasi partikulat PM10 mencapai hampir 200 mikro gram per meter kubik. Indeks standar pencemaran udara berada pada status tidak sehat,"'],
    ],

    'quotePenentang'=>[
        ['from'=>'Irman Gusman','jabatan'=>'Ketua DPD RI','img'=>'http://www.rmol.co/images/berita/normal/167423_10072518082015_irman_gusman.jpg','url'=>'','content'=>'"Menurut saya harus dilakukan penanganan yang tidak mungkin lagi level daerah. Jadi, memang menurut pandangan saya sudah dipikirkan darurat nasional. Itu kira-kira menurut pandangan saya,"'],
        ['from'=>'Agus Hermanto','jabatan'=>'Wakil Ketua DPR RI','img'=>'http://www.rmol.co/images/berita/normal/63147_02091117032015_agus-hermanto-_141114143409-850.jpg','url'=>'','content'=>'"Pemerintah harus lebih tegas, lebih trengginas (cekatan). Saya melihat pemerintah kurang serius menangani,"'],
        ['from'=>'E. Herman Khaeron','jabatan'=>'Wakil Ketua Komisi IV DPR','img'=>'http://www.rmol.co/images/berita/thumb/thumb_26816_02264422072015_herman_khaeron.jpg','url'=>'','content'=>'"Pemerintah harus bertindak cepat dan tidak menunggu waktu. Apalagi Komisi IV DPR sudah memberikan dukungan anggaran yang memadai untuk mengatasi kebakaran hutan dan lahan itu,"'],
        ['from'=>'Verna Gladies Merry Inkiriwang','jabatan'=>'Komisi IX DPR RI Fraksi P Demokrat','img'=>'http://www.metrosulawesi.com/sites/default/files/main/articles/1252671_20140116071653.jpg','url'=>'','content'=>'"Ini kategori berbahaya. Belum lagi data ISPU (Indeks Standar Pencemaran Udara) sudah pada level 2.314 yang fluktuatif, dan Penderita ISPA di Provinsi Kalteng mencapai 11.522 orang. Angka-angka ini mengerikan,"'],
        ['from'=>'Arief Poyuono','jabatan'=>'Wakil Ketua Umum Partai Gerindra','img'=>'http://img.eramuslim.com/media/2015/01/6e2feccf1476f3ee2b16761e8d2601f56e3ba5cc.jpg','url'=>'','content'=>'"Kehadiran Jokowi ditengah-tengah lahan bekas terbakar seperti hendak memberikan rasa nyaman kepada masyarakat yang disekitar tempat tinggalnya asap ‘tumbuh subur’, padahal faktanya tidak demikian,"'],
        ['from'=>'Nana Mulyana','jabatan'=>'Koordinator Wilayah I Sumatera Asosiasi Jasa Pengiriman Ekspres Pos dan Logistik Indonesia','img'=>'http://img.bisnis.com/posts/2013/09/29/166332/131001_nana-mulyana.jpg','url'=>'','content'=>'"Kabut asap menyelimuti Kota Pekanbaru, Riau, khususnya bandara setempat, berdampak kepada lumpuhnya transportasi udara. Ini membuat kami merugi,"'],
        ['from'=>'Intsiawati Ayus','jabatan'=>'anggota DPD RI','img'=>'http://www.goriau.com/assets/imgbank/25092013/a88998a9264514acdaa286ppd-10416.jpg','url'=>'','content'=>'"Harusnya dalam kondisi begini, semua pintu-pintu rumah sakit swasta terbuka lebar"'],
        ['from'=>'Abdul Wahid','jabatan'=>'ketua DPD PKB Prov Riau','img'=>'http://www.politikriau.com/foto_berita/79Abdul-Wahid.jpg','url'=>'','content'=>'"Tidak ada penanganan maksimal, jika status kebencanaan di Provinsi Riau belum ditingkatkan. Karena dengan peningkatan status tersebut semua pihak akan konsentrasi pada penanggulanan masalah ini"'],
        ['from'=>'Teguh Surya','jabatan'=>'Juru kampanye Greenpeace','img'=>'http://www.perspektifbaru.com/i/art/Teguh_Surya_3_1494_f_754.jpg','url'=>'','content'=>'"Pemerintah belum serius tangani karhutla"'],
        ['from'=>'Kusnadi','jabatan'=>'Koordinator Aliansi Sumatera Utara Anti-Asap (AWAS ASAP)','img'=>'http://cdn.tmpo.co/data/2015/09/06/id_434599/434599_620.jpg','url'=>'','content'=>'"Kami lihat pemerintah tidak pernah serius dalam persoalan kabut asap ini. Selama 18 tahun terakhir selalu berulang tanpa ada solusi yang memadai"'],
        ['from'=>'Abetnego Tarigan','jabatan'=>'Direktur Eksekutif WALHI','img'=>'http://www.mongabay.co.id/wp-content/uploads/2013/10/DSCF4107.jpg','url'=>'','content'=>'"Jika pemerintah benar-benar ingin serius menangani kabut asap, tidak hanya soal memadamkan tetapi juga membatasi proses izin untuk pembukaan lahan atau HTI"'],
        ['from'=>'Riko Kurniawan','jabatan'=>'Direktur Eksekutif Daerah Walhi Riau','img'=>'http://gagasanriau.com/wp-content/uploads/2013/08/1003778_10201911645214024_866629834_n.jpg','url'=>'','content'=>'"Provinsi yang rutin mengalami kebakaran lahan dan hutan yang berakibat pada kabut asap itu ada 7 Provinsi di Indonesia. Dan silahkan cek bahwa seluruh provinsi tersebut lahannya didominasi oleh lahan gambut yang diberikan izin konsesi pada perusahaan"'],
        ['from'=>'Kurniawan Sabar','jabatan'=>'Manajer Kampanye Eksekutif Nasional Walhi','img'=>'http://www.perspektifbaru.com/i/art/Kurniawan_Sabar_-1_1595_f_808.gif','url'=>'','content'=>'"Pernyataan Presiden Joko Widodo bahwa solusi mengatasi kebakaran adalah pembuatan kanal-kanal atau kanalisasi bisa kontraproduktif terhadap upaya penanganan kebakaran lahan dan hutan"'],
        ['from'=>'Uli Siagian','jabatan'=>'Koordinator Aksi "Pulihkan Sumatera dari Asap"','img'=>'http://cumakita.com/wp-content/uploads/2015/09/aksi-jadi-472x332.jpg','url'=>'','content'=>'"Asap bukan bencana, tetapi kelalaian pemerintah dalam menjaga kawasan hutan yang tersisa"'],
        ['from'=>'Feri Vandelis','jabatan'=>'Aktivis Gerakan Bengkulu Melawan Asap','img'=>'https://img.okezone.com/content/2015/09/18/340/1216591/demo-aktivis-gerakan-melawan-asap-kenakan-topeng-berdarah-9rdf5rMuyt.jpg','url'=>'','content'=>'"Topeng yang bercorak berdarah itu menandai wajah masyarakat Indonesia yang menangis akibat kabut asap"'],
    ],

    'video'=>[
        ['id'=>'C9WOl3FcerE'],
        ['id'=>'I620UWAPDUU'],
        ['id'=>'l9OfXbWhQ-0'],
        ['id'=>'OoQCfn7wv9Q'],
        ['id'=>'XMrajfwgYp0'],
        ['id'=>'5zxnxOuwvrA'],
        ['id'=>'7v4gdUuvWuQ'],
        ['id'=>'I620UWAPDUU'],
        ['id'=>'m23CpvppWeg'],
        ['id'=>'7YQ71ebx7Rk'],
        ['id'=>'PPhxGH3oLUA'],
        ['id'=>'jTmoG6LuUvU'],
        ['id'=>'THHsXtboFzg'],
        ['id'=>'dhZwUUwmI4w'],
        ['id'=>'lkxAXFLIHFw'],
        ['id'=>'seidto6Jz6w'],
    ],

    'foto'=>[
        ['img'=>'http://statik.tempo.co/?id=422459&width=620'],
        ['img'=>'http://www.mediaindonesia.com/assets/upload/gallery/6791_thumb_api3.jpg'],
        ['img'=>'http://beritatrans.com/cms/wp-content/uploads/2015/07/Karhutla.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2014/02/12/1324011Drone-Australia780x390.jpg'],
        ['img'=>'http://cdn1-a.production.liputan6.static6.com/medias/677756/big/2014-05-15T225238Z_59774469_GM1EA5G0J0D01_RTRMADP_3_USA-WILDFILES-CALIFORNIA.JPG'],
        ['img'=>'http://suratrakyat.com/wp-content/uploads/2015/02/Kebakaran-Hutan-riau.jpg'],
        ['img'=>'http://assets.kompas.com/data/photo/2013/06/25/1322515pemadaman1780x390.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/07/31/458097/hutan.jpg'],
        ['img'=>'http://klikriau.com/foto_news/56bakar.jpg'],
        ['img'=>'http://img2.bisnis.com/sumatra/posts/2015/02/18/55239/130910_kebakaran-hutan-2.jpg'],
        ['img'=>'http://static.gatra.id/images/gatracom/2015/dani/09-Sep/peta-kebakaran-sumatera-kalimantan.jpg'],        
        ['img'=>'http://siaksatu.com/assets/berita/52553917467-medium_40helikopter-bolco-pemadam-kebakaran-riau-ditambah.jpg'],
        ['img'=>'http://2.bp.blogspot.com/-wjUydBKfteM/U8cmRaZj-6I/AAAAAAAAUdM/W1Hc9cW7yo0/s1600/kebakaran+hutan.jpg'],
        ['img'=>'https://img.okezone.com/content/2015/09/04/337/1208440/jokowi-akan-pantau-langsung-kebakaran-hutan-riau-fwJKk4KwUF.jpg'],
        ['img'=>'http://beritadaerah.co.id/wp-content/uploads/2014/09/Wildfires-USA190914-620x330.jpg'],
        ['img'=>'http://www.metrosiantar.com/wp-content/uploads/2015/09/kebakaran-hutan.jpg'],
        ['img'=>'http://2.bp.blogspot.com/-bJefQmET61s/UAaRC_KZ-_I/AAAAAAAABjA/j0Dmyhm0bac/s1600/kebakara+n+hutan.jpg'],
        ['img'=>'http://www.kaltimprov.go.id/img_news/medium_32KEBAKARAN%20BUKIT%20SOEHARTO%20(15)%20(FILEminimizer).JPG'],
        ['img'=>'http://1.bp.blogspot.com/-fFnXVDm112w/VYjhYktfCNI/AAAAAAAACj8/nwcCgr50yUU/s1600/penyebab%2Bkebakaran%2Bhutan.JPG'],
        ['img'=>'https://herwin.files.wordpress.com/2009/02/forest-fire.jpg'],
        ['img'=>'http://depoktren.com/wp-content/uploads/2014/10/kebakaran-hutan.jpg'],
        ['img'=>'http://id.theasianparent.com/wp-content/uploads/2013/06/800px-Northwest_Crown_Fire_Experiment.png'],
        ['img'=>'http://www.mongabay.co.id/wp-content/uploads/2013/09/hutan-lindung-kebakaran-hutan-di-Rokan-Hulu-1157474_213150085517021_1347456503_n.jpg'],
        ['img'=>'http://www.radioaustralia.net.au/indonesian/sites/default/files/imagecache/ra_article_feature/images/2014/01/13/bakar_1312014.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2009/05/08/70378_suatu_helikopter_berupaya_padamkan_api_kebakaran_hutan_di_california_663_382.jpg'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2013/08/27/42/775966/W2dhmn4ENC.jpg'],
        ['img'=>'http://cahayareformasi.com/wp-content/uploads/2013/06/Kebakaran-hutan-Riau.jpg'],
        ['img'=>'http://www.kabarhukum.com/wp-content/uploads/2015/09/asap-kualanamu6.jpg'],
        ['img'=>'http://cdn.tmpo.co/data/2014/09/18/id_325933/325933_620.jpg'],
        ['img'=>'http://kliksulsel.com/wp-content/uploads/2015/09/hutan3.jpg'],
        ['img'=>'http://media.viva.co.id/thumbs2/2013/06/21/210990_sebaran-titik-api-dan-asap-di-sumatera--2013-_663_382.jpg'],
        ['img'=>'http://static.skalanews.com/media/news/thumbs-396-263/titik-api-sumatera(7).jpg'],
        ['img'=>'http://www.dw.com/image/0,,18712454_303,00.jpg'],
        ['img'=>'http://cdn-2.tstatic.net/tribunnews/foto/bank/images/jokowi-tinjau-lokasi-kebakaran-lahan-di-kalimantan_20150923_174716.jpg'],

    ],

    'berita'=>[
        ['img'=>'','shortText'=>'Pemprov Sumsel Akan Evakuasi Warga','link'=>'http://www.bijaks.net/news/article/0-212040//pemprov-sumsel-akan-evakuasi-warga-jika-kabut-asap-meningkat/'],
        ['img'=>'','shortText'=>'Kabut Asap Mereda Siswa Tanjungpinang Kembali Bersekolah','link'=>'http://www.bijaks.net/news/article/0-211995/kabut-asap-mereda-siswa-tanjungpinang-kembali-bersekolah'],
        ['img'=>'','shortText'=>'Kabut Asap Buat Mata Pengendara Perih','link'=>'http://www.bijaks.net/news/article/0-211924/kabut-asap-buat-mata-pengendara-perih'],
        ['img'=>'','shortText'=>'Kabut Asap Selimuti Pekanbaru','link'=>'http://www.bijaks.net/news/article/0-211842/kabut-asap-selimuti-pekanbaru-bandara-ssk-kembali-lumpuh'],
        ['img'=>'','shortText'=>'Warga Bengkalis Terserang ISPA','link'=>'http://www.bijaks.net/news/article/0-211843//dampak-asap-4-150-warga-bengkalis-terserang-penyakit-ispa/'],
        ['img'=>'','shortText'=>'Indonesia Perlu Waktu Atasi Asap','link'=>'http://www.bijaks.net/news/article/0-211692/joko-widodo-indonesia-perlu-waktu-atasi-kabut-asap'],
        ['img'=>'','shortText'=>'70 Penerbangan Batal','link'=>'http://www.bijaks.net/news/article/0-211670/akibat-kabut-asap-sebanyak-70-penerbangan-tujuan-pekanbaru-batal'],
        ['img'=>'','shortText'=>'Siswa SD Tetap Semangat Sekolah','link'=>'http://www.bijaks.net/news/article/0-211639/diselimuti-kabut-asap-siswa-sd-tetap-semangat-sekolah'],
        ['img'=>'','shortText'=>'Ribuan Siswa Kembali Diliburkan','link'=>'http://www.bijaks.net/news/article/0-211619/ribuan-siswa-di-pekanbaru-kembali-diliburkan-akibat-kabut-asap'],
        ['img'=>'','shortText'=>'Status Darurat Diperpanjang','link'=>'http://www.bijaks.net/news/article/0-211549/pemkab-sumbar-perpanjang-kembali-status-darurat-kabut-asap'],
        ['img'=>'','shortText'=>'Kabut Asap Membaik','link'=>'http://www.bijaks.net/news/article/0-211470//kabut-asap-di-muara-teweh-membaik/'],
        ['img'=>'','shortText'=>'Kabut Asap Menghilang','link'=>'http://www.bijaks.net/news/article/0-211463/kabut-asap-di-muara-teweh-menghilang'],
        ['img'=>'','shortText'=>'Komisi I Dukung Gugatan Singapura','link'=>'http://www.bijaks.net/news/article/0-211432/komisi-i-dukung-gugatan-singapura-terkait-kabut-asap'],
        ['img'=>'','shortText'=>'Verna Nilai Pemerintah Tak Sigap','link'=>'http://www.bijaks.net/news/article/0-211425/verna-nilai-pemerintah-tak-sigap-tangani-asap'],
        ['img'=>'','shortText'=>'Warga Riau Terjangkit ISPA','link'=>'http://www.bijaks.net/news/article/0-211340/36-184-warga-riau-terjangkit-ispa-akibat-paparan-asap'],
        ['img'=>'','shortText'=>'Dua Anggota Kostrad Tumbang','link'=>'http://www.bijaks.net/news/article/0-211334/dua-anggota-kostrad-tumbang-akibat-hirup-asap-akibat-kebakaran-lahan'],
        ['img'=>'','shortText'=>'Balita Alami Radang paru-paru','link'=>'http://www.bijaks.net/news/article/0-211104/akibat-kabut-asap-balita-pekanbaru-alami-radang-paru-paru'],
        ['img'=>'','shortText'=>'Pemkot Pekanbaru Evakuasi Bayi-bayi','link'=>'http://www.bijaks.net/news/article/0-211023/dampak-kabut-asap-pemkot-pekanbaru-evakuasi-bayi-bayi-di-bawah-umur'],
        ['img'=>'','shortText'=>'Ketua DPR Minta Pemerintah Lebih Trengginas','link'=>'http://www.bijaks.net/news/article/0-211008/ketua-dpr-minta-pemerintah-lebih-trengginas-tangani-bencana-kabut-asap'],
        ['img'=>'','shortText'=>'Balita 13 bulan Infeksi paru-akibat-kabut-asap','link'=>'http://www.bijaks.net/news/article/0-210986/di-riau-balita-13-bulan-infeksi-paru-akibat-kabut-asap'],
        ['img'=>'','shortText'=>'Ratusan Warga Kubu Terkena ISPA','link'=>'http://www.bijaks.net/news/article/0-210903/akibat-kabut-asap-ratusan-warga-kubu-terkena-penyakit-ispa-dan-diare'],
        ['img'=>'','shortText'=>'Seskab Belum Sepakat','link'=>'http://www.bijaks.net/news/article/0-210751/kabut-asap-seskab-belum-ada-kata-sepakat-soal-tawaran-singapura'],
        ['img'=>'','shortText'=>'Diguyur Hujan, Asap Tebal Hilang','link'=>'http://www.bijaks.net/news/article/0-210726/diguyur-hujan-semalam-kabut-asap-tebal-di-bengkulu-hilang'],
        ['img'=>'','shortText'=>'Pemerintah Diminta Membiayai','link'=>'http://www.bijaks.net/news/article/0-210723/pemerintah-diminta-membiayai-pengobatan-korban-kabut-asap'],
        ['img'=>'','shortText'=>'Pemerintah Tolak Bantuan Singapura','link'=>'http://www.bijaks.net/news/article/0-210585/pemerintah-tolak-bantuan-singapur-atasi-kabut-asap'],
        ['img'=>'','shortText'=>'Singapura Soal Kabut Asap','link'=>'http://www.bijaks.net/news/article/0-210411/singapura-soal-kabut-asap-wapres-jk-silahkan-kalau-mau-bantu-jangan-hanya-bicara'],
        ['img'=>'','shortText'=>'Sebulan Sekolah Diliburkan','link'=>'http://www.bijaks.net/news/article/0-210289/sebulan-sekolah-di-pekanbaru-diliburkan-akibat-kabut-asap'],
        ['img'=>'','shortText'=>'Tanggapan Istana Terkait Asap','link'=>'http://www.bijaks.net/news/article/0-210256/tanggapan-istana-terkait-asap-tebal-di-singapura'],
        ['img'=>'','shortText'=>'Warga Minta Pejabat Ikut Rasakan','link'=>'http://www.bijaks.net/news/article/0-210214/warga-minta-para-pejabat-jakarta-ikut-rasakan-penderitaan-rakyat-yang-terkena-asap'],
        ['img'=>'','shortText'=>'Ekspor Asap RI Meningkat','link'=>'http://www.bijaks.net/news/article/0-210163/ekspor-asap-ri-meningkat-2-926-sekolah-di-malaysia-terpaksa-ditutup'],
        ['img'=>'','shortText'=>'Korban Asap di Riau','link'=>'http://www.bijaks.net/news/article/0-210200/sebanyak-44-871-korban-asap-di-riau'],
        ['img'=>'','shortText'=>'Warga Jambi Hanya Pasrah','link'=>'http://www.bijaks.net/news/article/0-210058/asap-pekat-warga-jambi-hanya-bisa-pasrah'],


    ],



]

?>

<style>
    .boxcustom {background: url('<?php echo base_url("assets/images/hotpages/asap/top.jpg");?>');padding: 10px;}
    .boxblue {background-color: #63839c;padding: 10px;border-radius: 5px;color: white;margin-bottom: 10px;}
    .boxcustom2 {background-color: #eaf7e3;padding-bottom: 20px;}
    .boxcustom3 {background-color: #e95757;padding-bottom: 20px;}
    .boxdotted {border-radius: 10px;border: 2px dotted #bcbb36;width: 100%;height: auto;margin-bottom: 10px;padding-top: 5px;display: inline-block;}
    .black {color: black;}
    .white {color: white;}
    .green {color: #e9f0ae;}
    .list {background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;padding-left: 30px;}
    .list2 {list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');}
    .block_green {background-color: #00a651;}
    .block_red {background-color: #a60008;}
    #bulet {background-color: #555555;text-align: center;width: 35px;height: 15px;border-radius: 50px 50px 50px 50px;font-size: 12px;
        -webkit-border-radius: 50px 50px 50px 50px;-moz-border-radius: 50px 50px 50px 50px;color: white;padding: 4px 8px;margin-right: 5px;}
    .bendera {width: 150px;height: 75px;margin-right: 10px;margin-bottom: 10px;float: left;border: 1px solid black;}
    .pic {float: left;margin-right: 10px;max-width: 150px;margin-top: 5px;}
    .pic2 {float: right;margin-left: 10px;max-width: 150px;margin-top: 5px;}
    .pic3 {float: left;margin-right: 10px;max-width: 100px;margin-top: 5px;}
    .ketua {height: 120px;width: 100%;}
    .clear {clear: both;}
    p {text-align: justify;}
    li.dukung {float: left; padding:3px;vertical-align: top;border: 2px solid lightgray;color: black;margin-bottom: 5px;}
    li.dukung img {width: 55px; height: 55px;  padding: 0px !important;float: left;margin: 0 auto;}
    li.dukung p {height: auto;margin-top: 5px;text-align: center;font-size: 10px;}
    .gallery li {display: block;float: left;height: 50px;margin-bottom: 7px;margin-right: 0px;width: 25%;overflow: hidden;}
    .gallery li a {height: 100px;width: 100px;}
    .gallery li a img {max-width: 97%;}

</style>

<link rel="stylesheet"  href="<?php echo base_url(); ?>assets/css/lightGallery.css"/>
<script src="<?php echo base_url(); ?>assets/js/lightGallery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $(".gallery").lightGallery();
        $(".gallery2").lightGallery();
    })
</script>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['line1']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/asap/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in">
            <p style="text-align: justify;"><?php echo $data['line1']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line2']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['line2']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line5']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['line5']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line8']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['line8']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line9']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['line9']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line10']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['line10']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line11']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['line11']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line12']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['line12']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line13']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['line13']['narasi'];?></p>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line4']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['line4']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line7']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['line7']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line3']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['line3']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>
        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya"><?php echo $data['line6']['title'];?></span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
            <?php echo $data['line6']['narasi'];?>  
            </div>
        </div>
        <div class="clear"></div>


        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePendukung'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['quotePenentang'] as $key=>$val) { ?>
                <div class="panel-collapse collapse in col-xs-6">
                    <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                        <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                    </p>
                    <div class="clear"></div>
                    <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;">"<?php echo $val['content'];?>"</p>
                    <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">
                </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">GALERI FOTO</span></h5>
        <div id="accordion" class="panel-group">
            <ul id="light-gallery" class="gallery">
                <?php
                foreach($data['foto'] as $key=>$val){
                    ?>
                    <li data-src="<?php echo $val['img'];?>">
                        <a href="#">
                            <img src="<?php echo $val['img'];?>" />
                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['video'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">BERITA TERKAIT</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <?php
                foreach($data['berita'] as $key=>$val){
                    ?>
                    <a href="<?php echo $val['link'];?>"><?php echo $val['shortText'];?></a>
                    <br>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
</script>