<?php 
$data = [
    'NARASI'=>[
        'title'=>'RONDE TERAKHIR KISRUH GOLKAR',
        'narasi'=>'<img src="http://www.harjasaputra.com/media/k2/items/cache/72ca15047a78e7b4a322422f38badf4b_XL.jpg" class="pic">Partai Golkar di kancah perpolitikan nasional sedang berada pada kondisi labil. Lebih dari enam dekade terkahir sejak lahirnya Golkar, eksistensi Golkar relatif stabil sebagai partai besar, yang mampu menancapkan pengaruhnya di setiap rezim yang dilaluinya. 
                </p><p style="text-align: justify;">Namun di rezim pemerintahan Jokowi-JK, Golkar di bawah kepemimpinan Aburizal Bakrie diguncang kisruh internal yang berkepanjangan. Konflik internal partai berlambang beringin berimbas pada munculnya dualisme kepemimpinan, Aburizal Bakrie versi Munas Bali dan Agung Laksono versi Munas Jakarta. Tidak sampai di situ, polemik mengerucut ke perebutan kantor sekretariat fraksi di parlemen.
                </p><p style="text-align: justify;"><img src="http://us.images.detik.com/customthumb/2015/02/11/10/084358_dangdutmunas3.jpg?w=500" class="pic2">Surat Keputusan (SK) dari Kemenkumham mengindikasikan adanya intervensi politik yang dilakukan pemerintah terhadap polemik di internal Partai Golkar. SK tersebut merupakan pesanan Megawati ke Menteri Yasona. Megawati sebelumnya menerima masukan dari Jusuf Kalla dan surya Palloh agar mengitruksikan ke Menteri Yasona untuk mengeluarkan SK kepengerusan partai Golkar hasil Munas Jakarta. Dengan memberikan legalitas hukum pada kepengurusan Golkar munas Jakarta dan mendudukkan Agung Laksono sebagai ketua umumnya, maka Golkar akan lebih mudah digiring untuk masuk ke dalam gerbang partai koalisi pendukung pemerintah, Koalisi Indonesia hebat (KIH). Bergabungnya Golkar ke dalam koalisi indonesia hebat akan memperkokoh kekuatan partai pendukung pemerintah di parlemen.
                </p><p style="text-align: justify;">Eskalasi konflik semakin meluas. Putusan sela Pengadilan Negeri Jakarta Utara membuat kubu Agung Laksono meradang. Dalam putusan itu, PN Jakarta Utara mengabulkan permintaan dari penggugat, yakni Aburizal Bakrie dan Idrus Marham dan mengembalikan Golkar ke kepengurusan Munas Riau 2009. Golkar terancam tidak bisa mengikuti kontestasi politik, Pilkada serentak desember nanti karena tidak adanya kepengurusan yang sah. Akhirnya, jalan islah dirumuskan sebagai upaya penyelamatan Golkar.'
    ],
    'PROFIL'=>[
        'sejarah'=>[
            ['no'=>'Tahun 1964. Lahir sebagai Sekretariat Bersama Golkar (Sekber) diakhir pemerintahan Presiden Soekarno. '],
            ['no'=>'Didirikan oleh golongan militer, khususnya perwira Angkatan Darat. terpilih sebagai Ketua Pertama Sekber Golkar adalah Brigadir Jenderal (Brigjen) Djuhartono sebelum digantikan Mayor Jenderal (Mayjen) Suprapto Sukowati.'],
            ['no'=>'Tanggal 4 Februari 1970, Sekber Golkar menetapkan satu nama dan tanda gambar yaitu Golongan Karya (GOLKAR). Logo dan nama ini, sejak Pemilu 1971, tetap dipertahankan sampai sekarang.'],
            ['no'=>'Tahun 1971 Golkar ikut dalam pemilu dan menjadi pemeang dengan 34.348.673 suara atau 62,79 % dari total perolehan suara. Ini adalah pemilu pertama untuk Golkar.'],
            ['no'=>'Pada tanggal 17 Juli 1971 Sekber GOLKAR mengubah dirinya menjadi GOLKAR.'],
            ['no'=>'Selama puluhan tahun Orde Baru berkuasa, jabatan-jabatan dalam struktur eksekutif, legislatif dan yudikatif, hampir semuanya diduduki oleh kader-kader Golkar.'],
            ['no'=>'Kemenangan Golkar selalu diukir dalam pemilu di tahun 1977, 1982, 1987, 1992, dan 1997.'],
            ['no'=>'Tahun 1988 Soeharto lengser dari singgasana kekuasaannya, yang kemudian berimbas pada tuntutan pembubaran Golkar.'],
            ['no'=>'AkbarTandjung terpilih sebagai ketua umum dan Golkar berubah wujud menjadi PartaGolkar.'],
            ['no'=>'Di bawah kepemimpinan Akbar Tandjung, Golkar berhasil bertahan dari serangan eksternal dan krisis citra, inilah yang membuat Akbar menjadi ketua umum Golkar yang cukup legendaris.'],
            ['no'=>'Pada pemilu pertama di Era Reformasi ini Partai Golkar mengalami penurunan suara  di peringkat ke dua di bawah PDIP. Namun pada pemilu berikutnya Golkar kembali unggul.'],
            ['no'=>'Pemilu legislatif 2009 suara Partai Golkar kembali turun ke posisi dua. Tahun 2004 Golkar menjadi pemenang pemilu legislatif dengan 24.480.757 suara atau 21,58% suara sah. Namun pada pemilu 2014 Partai  Golkar kembali harus rela berada di posisi dua dengan perolehan suara 18.432.312 (14,75 persen)']
        ]
    ],
    'SALING'=>'<img src="http://inilahdepok.com/wp-content/uploads/2015/02/agung-ical-620x330.jpg" class="pic">Aburizal Bakrie yang terpilih jadi  ketua umum Golkar versi musyawarah nasional di Bali terus mendapat perlawanan dari kubu Agung Laksono. Gugutan yang dilayangkan ke PN Jakarta Utara ditolak. Putusan pengadilan menyatakan tidak memiliki wewenang mengadili sengketa partai. Kepengurusan diselesaikan melalui mekanisme internal partai, Mahkamah Partai. Jalur hukum yang dimenangkan kubu Agung menyudutkan posisi Aburizal Bakrie.
                </p><p style="text-align: justify;">Legalitas kepengurusan Agung Laksono semakin kuat setelah Kemenkumhan mengeluarkan SK sah untuk kepengurusan Agung Laksono. Namun, kubu Agung tidak tinggal diam. Melalui Pengacaranya, Yusril Ihza Mahendra melayangkan gugatan ke Pengadilan Tinggi Tata Usaha Negara Jakarta Utara, yang kemudian memenangkan kubu Ical. Putusan sela yang menetapkan penundaan pelaksanaan surat Kementerian, sehingga mengembalikan kepengurusan Golkar sesuai dengan Musyawarah Nasional Riau.
                </p><p style="text-align: justify;"><img src="http://img.bisnis.com/posts/2015/05/21/435866/golkar-idrus-marham.jpg" class="pic2">“Dari putusan penundaan ini, kepengurusan Pengurus Pusat Golkar Agung Laksono tak boleh mengambil tindakan administratif dan politik apa pun juga,” kata kuasa hukum Aburizal Bakrie, Yusril Ihza Mahendra.
                </p><p style="text-align: justify;">Keputusan PTUN itu langsung diiukuti langkah Wakil Ketua DPR Fadli Zon dengan memutuskan pimpinan Dewan Perwakilan Rakyat akan menyatakan status quo atas kepengurusan Fraksi Partai Golkar di lembaga legislatif. “Pimpinan tak bisa membacakan dua surat perombakan fraksi, jadi kami anggap status quo sampai inkracht,” kata politikus Partai Gerindra tersebut.
                </p><p style="text-align: justify;">Selain memenangkan putusan sela. Pada 18 Mei 2015 Pengadilan Tata Usaha Negara memenangkan kubu Aburizal Bakrie dalam putusan sengketa Partai Golkar. Majelis hakim memerintahkan Menteri Hukum dan HAM mencabut SK yang mengesahkan kepengurusan Agung Laksono. Sebaliknya Hakim mengesahkan SK Menkumham lama hasil Munas Riau 2009 yang mengakui kepengurusan Ical. Dengan demikian, konflik semakin memanas.',
    'SIASAT'=>'<img src="http://www.itoday.co.id/wp-content/uploads/2012/12/bakrieical.jpg" class="pic">Hasrat Aburizal Bakrie untuk kembali menduduki pucuk pimpinan partai berlambang pohon beringin mulai menguat ketika ada upaya untuk mempercepat Musyawarah Nasional IX yang sedianya akan dilaksanakan pada Januari 2015 menjadi tanggal 30 November 2014. Perubahan jadwal tersebut disinyalir sebagai siasat Ical untuk dapat dapat kembali memegang kendali kekuasaan di Golkar.
                </p><p style="text-align: justify;">Percepatan munas disebut-sebut bertujuan untuk mendorong terjadinya aklamasi pemilihan Aburizal Bakrie menjadi ketua umum kembali. Di sisi lain, percepatan munas akan membatasi waktu bagi calon ketua umum lain untuk melakukan konsolidasi.
                </p><p style="text-align: justify;"><img src="http://liputanislam.com/wp-content/uploads/2014/04/bakrie_aburizal.jpg" class="pic2">Agung Laksono mengakui, percepatan munas bakal mempersulit langkah kandidat lain untuk bersaing. Pasalnya, waktu untuk mengumpulkan dukungan plus mempersiapkan logistik menjadi sangat singkat.
                </p><p style="text-align: justify;">Percepatan Munas dari jadwal semula Januari 2015 dianggap merugikan kandidat yang mencalonkan diri. Informasi bahwa percepatan munas disengaja agar para caketum, kecuali Ical, tidak siap.',
    'MENJEGAL'=>'Tujuh calon yang sedianya akan ikut dalam bursa pemilihan ketua umum  bersatu dengan hanya mengusung satu nama untuk melawan Aburizal Bakrie ( Ical) dalam Munas IX. Agung Laksono adalah aktor dibalik manuver tersebut. Dengan begitu agenda regenerasi kepimimpinan Golkar akan terealisasi. Penolakan terhadap Aburizal Bakrie sudah jauh-jauh hari sudah terdengar.
                </p><p style="text-align: justify;"><img src="http://www.posmetro-medan.com/wp-content/uploads/2015/04/Agung-Laksono-yang-bersiteru-dengan-ARB.jpg" class="pic">Beberapa kader Golkar merasa kecewa dengan kinerja yang ditunjukkan Ical dalam mengawal partai Golkar di kancah perpolitikan nasional. Golkar mengalami penurunan popularitas sejak dipegang oleh Ical. Indikatornya adalah untuk pertama kalinya Golkar tidak memiliki calon dalam ajang Pilpres 2014.
                </p><p style="text-align: justify;">Dan ini menunjukkan Golkar gagal menjaga tradisi sebagai parpol yang selalu menyumbangkan kader terbaiknya untuk diusung sebagai calon presiden. Fakta inilah yang menajdi acuan harus dilakukannya regenerasi kepemimpinan di dalam tubuh Golkar. Demi gengsi dan nama besar Golkar.',
    'KETUA'=>[
        ['name'=>'Agung Laksono','thn'=>'(2015)','img'=>'http://berita2bahasa.com/images/articles/2015313agung%20laksono%20-%20tempo%20co%20-%20b.jpg' ],
        ['name'=>'Aburizal Bakrie','thn'=>'(2009 - 2015)','img'=>'http://upload.wikimedia.org/wikipedia/commons/4/45/Aburizal_Bakrie_-_March_2011.jpeg' ],
        ['name'=>'Jusuf Kalla','thn'=>'(2004–2009)','img'=>'http://upload.wikimedia.org/wikipedia/commons/7/71/Jusuf_Kalla.jpg' ],
        ['name'=>'Akbar Tandjung','thn'=>'(1998–2004)','img'=>'http://upload.wikimedia.org/wikipedia/id/f/f4/Akbar_Tanjung.jpg' ],
        ['name'=>'Harmoko','thn'=>'(1993–1998)','img'=>'http://upload.wikimedia.org/wikipedia/commons/7/75/Harmoko.jpg' ],
        ['name'=>'Wahono','thn'=>'(1988–1993)','img'=>'http://upload.wikimedia.org/wikipedia/id/9/98/Wahono.jpg' ],
        ['name'=>'Sudharmono','thn'=>'(1983–1988)','img'=>'http://upload.wikimedia.org/wikipedia/commons/0/02/Sudharmono2.jpg' ],
        ['name'=>'Amir Moertono','thn'=>'(1973–1983)','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png' ],
        ['name'=>'Suprapto Sukowati','thn'=>'(1969–1973)','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png' ],
        ['name'=>'Djuhartono','thn'=>'(1964-1969)','img'=>'http://www.bijaks.net/assets/images/no-image-politisi.png' ]
    ],
    'KRONOLOGI'=>[
        ['date'=>'24 November 2014','content'=>'Senin (24/11/2015) para pengurus partai menggelar rapat pleno dengan agenda persiapan  Munas IX digelar di Kantor DPP Partai Golkar, Slipi Jakarta.'],
        ['date'=>'25 November 2014','content'=>'Sehari setelahnya, yakni selasa (25/11/2014) dua kelompok massa yakni Angkatan Muda Partai Golkar (AMPG) yang dipimpin oleh Yorrys Raweyai bentrok dengan massa AMPG pimpinan Ahmad Doli yang mengklaim diri sebagai AMPG resmi di kantor DPP Partai Golkar, Jalan Anggrek Nelly Murni XI-A, Slipi, Jakarta.'],
        ['date'=>'30 November - 3 Desember 2014','content'=>'Munas IX Partai Golkar digelar di Nusa Dua, Bali. Hasil Munas menetapkan Aburizal Bakrie sebagai ketua umum terpilih.'],
        ['date'=>'04 Desember 2014','content'=>'Kamis (4/12/2014 Ketua Umum Aburizal Bakrie akhirnya mengumumkan susunan kabinet kepengurusan DPP Golkar periode 2014-2019 hasil Munas Bali.'],
        ['date'=>'06 Desember 2014','content'=>'Sabtu (6/12/2014) Musyawarah Nasional IX Partai Golkar tandingan yang digelar kubu Agung Laksono cs resmi dibuka,  di Ballroom Hotel Mercure, Ancol, Jakarta'],
        ['date'=>'03 Maret 2015','content'=>'Selasa (3/3/2015), Mahkamah Partai Golkar menyatakan kepengurusan hasil Munas Ancol Jakarta dinyatakan sah.'],
        ['date'=>'10 Maret 2015','content'=>'Menteri Hukum dan Ham Yasonna Laoly Yasonna secara resmi mengesahkan kepemimpinan Golkar versi Munas Ancol dengan Ketua Umum Agung Laksono.'],
        ['date'=>'23 Maret 2015','content'=>'Senin (23/3/2015) Siang, Yusril Ihza Mahendra selaku kuasa hukum partai Golkar mengajukan gugatan Surat Keputusan (SK) Menteri Hukum dan Ham (Menkumham) di Pengadilan Tata Usaha Negara Jakarta (PTUN).'],
        ['date'=>'25 Maret 2015','content'=>'Kubu Agung Laksono melayangkan surat peringatan pengosongan kantor ketua dan sekretaris Fraksi Golkar di komplek DPR RI.'],
        ['date'=>'27 Maret 2015','content'=>'Wakil Pimpinan Golkar dari kubu Agung Laksono, Yorrys Raweyai mendatangi kantor sekretariat Fraksi Golkar untuk mengambil alih kantor tersebut.'],
        ['date'=>'30 mei 2015','content'=>'Penandatanganan kesepakatan islah khusus dilakukan. Penandatanganan kesepakatan islah dilakukan oleh Ketua Umum DPP Partai Golkar hasil Munas Jakarta Agung Laksono, Ketua Umum DPP Partai Golkar hasil Munas Bali Aburizal Bakrie, dan Sekretaris Jenderal dari kedua kubu, Zainuddin Amali serta Idrus Marham di kediaman yusuf Kalla.'],
        ['date'=>'1 Juni 2015','content'=>'Pengadilan Negeri Jakarta Utara mengeluarkan putusan sela yang menguntungkan kepengurusan Golkar Aburizal Bakrie.']
    ],
    'PENDUKUNG'=>[
        'partai'=>[
            ['page_id'=>'partaidemokrasiindonesiaperjuangan5119ac6bba0dd'],
            ['page_id'=>'nasdem5119b72a0ea62'],
            ['page_id'=>'partaikeadialndnpersatuanindonesia5119bd7483d2a'],
            ['page_id'=>'partaihatinuranirakyathanura5119a1cb0fdc1'],
            ['page_id'=>'partaipersatuanpembangunan5189ad769b227']
        ]
    ],
    'PENENTANG'=>[
        'partai'=>[
            ['page_id'=>'partaigerakanindonesiarayagerindra5119a4028d14c'],
            ['page_id'=>'partaikeadilansejahtera5119b06f84fef']
        ]
    ],
    'DUKUNGAN'=>[
        'narasi'=>'Dua kubu yang berseteru gencar menggalang dan mencari dukungan. Hal itu tidak hanya untuk memperkuat eksistensi kepengurusan partai, tetapi untuk memetakan kekuatan di Parlemen. Partai beringin di bawah kepengurusan Agung Laksono akan mengubah haluan untuk menjadi partai pendukung pemerintah. Kondisi ini akan berdampak pada berkurangnya kekuatan koalisi oposisi di DPR. Golkar saat ini menjadi pemegang kendali di Koalisi Merah Putih karena menjadi penyokong utama dari segi kuantitas suara di Parlemen.<br><br>Para Elit Pendukung Agung dari Golkar :',
        'satu'=>[
            ['page_id'=>'drshpriyobudisantoso51131c5b8da10'],
            ['page_id'=>'drsagusgumiwangkartasasmita50ee38bcdabca'],
            ['page_id'=>'yorrysraweyai52bbea81c3597'],
            ['page_id'=>'zainuddinamali5456ee91ad531'],
            ['page_id'=>'melchiasmarkusmekeng52a54c5052ca8'],
            ['page_id'=>'leonababan52675bf49f296'],
            ['page_id'=>'sariyuliati52a53db4ed03b']
        ],
        'narasi2'=>'Mereka menempati posisi-posisi strategis di dalam struktur kepengurusan Partai Golkar.<br><br>Selain dukungan dari elit internal Golkar, Agung Laksono bergerak cepat dengan melakukan safari politik ke elit-elit parpol lain. Setidaknya para ketua umum dan elit partai yang tergabung dalam Koalisi Indonesia Hebat seperti :',
        'dua'=>[
            ['page_id'=>'megawatisoekarnoputri50ee62bce591e'],
            ['page_id'=>'suryapaloh511b4aa507a50'],
            ['page_id'=>'drshmuhammadjusufkalla50ee870b99cc9'],
            ['page_id'=>'hwirantosh50bdcb9a73d2f'],
            ['page_id'=>'letjentnipurnsutiyoso511c2b6deeb46']
        ],
        'narasi3'=>'Mereka adalah barisan elit parpol yang sudah menyatakan komitmen untuk mendukung kepengurusan Agung Laksono sebagai ketua umum Golkar yang sah.<br><br>KMP Dukung ARB<br>Aburizal Bakrie merapatkan barisan pendukungnya yang ada di KMP. Semua elit partai di KMP berada di belakang Aburizal dan terus memberi dukungan terhadapnya. Mereka adalah:',
        'tiga'=>[
            ['page_id'=>'hmanismattalc50f7ba1b1520d'],
            ['page_id'=>'fahrihamzahse5105e57490d09'],
            ['page_id'=>'fadlizon5119d8091e007'],
            ['page_id'=>'prabowosubiyantodjojohadikusumo50c1598f86d91']
        ],
        'narasi4'=>'Sedangkan dari dalam Golkar sendiri Ical didukung oleh :',
        'empat'=>[
            ['page_id'=>'akbartanjung503c2d29aaf6b'],
            ['page_id'=>'sitihediatihariyadi51b939bf7e379'],
            ['page_id'=>'Ahmadi'],
            ['page_id'=>'drfadelmuhammad51132604d3a90'],
            ['page_id'=>'adekomarudin5280ac60140b0'],
            ['page_id'=>'nurdinhalid530eb1689eb22'],
            ['page_id'=>'drssetyanovanto50f8fd3c666bc'],
            ['page_id'=>'drazizsyamsuddin50f8e5a2bef6b'],
            ['page_id'=>'drstheolsambuaga51886b9786e52'],
            ['page_id'=>'idrusmarham51132ad8423e1'],
            ['page_id'=>'sharifcicipsutarjo50ef9a0c31d50']
        ]
    ],
    'KRITIK'=>'<img src="http://dennyja-world.com/wp-content/uploads/2015/03/yasonna.jpg" class="pic2">Sejak terbitnya SK yang mengsahkan kubu Agung Laksono. Menteri Yasona mendapat kritikan yang tajam dari kubu Aburizal Bakrie. Yasono dianggap memihak dalam mengambil kebijakan yang tidak sesuai prosedur hukum yang berlaku. Seharusnya Surat Keputusan dikeluarkan oleh Kemenkumham setelah ada putusan dari PTUN.
                </p><p class="font_kecil" style="text-align: justify;">Selain itu,keputusannya tidak didasarkan pada penilaian yang objektif, namun sarat politis. Keputusan tersebut merupakan keputusan yang ke dua kalinya dikeluarkan oleh Menteri Yasona sejak diangkat jadi menteri. Sebelumnya keputusan serupa dikeluarkan dalam menangani kasus sengketa di Partai Persatuan Pembangunan (PPP).
                </p><p class="font_kecil" style="text-align: justify;"><img src="http://www.jurnal3.com/wp-content/uploads/2015/03/laoly-golkar-yusril.jpg" class="pic">Kemenkumham berdalih bahwa putusan yang dikeluarkannya sudah tepat, karena mengacu pada keputusan Mahkamah Partai Golkar yang mengabulkan untuk menerima kepengurusan DPP Partai Golkar hasil Munas Ancol, yakni berdasarkan ketentuan Pasal 32 ayat 5 UU Parpol Nomor 2/201, dinyatakan bahwa putusan MP (Mahkamah Partai Golkar) bersifat final dan mengikat secara internal.
                </p><p class="font_kecil" style="text-align: justify;">Kubu Aburizal melawan keputusan pemerintah ini. Mereka mengugat putusan Menkumham ke pengadilan. Mereka juga melaporkan kubu Agung ke Bareskrim Polri dengan tuduhan pemalsuan dokumen. Selain itu, Koalisi Merah Putih juga akan mengajukan hak angket di DPR. Menteri Yasonna dianggap bekerja atas dasarpolitik.',
    'USULAN'=>'<img src="http://www.sayangi.com/media/k2/items/cache/20a212aca23d09956e75057d32a25b01_XL.jpg" class="pic2">Koalisi Merah Putih mengusulkan hak angket DPR untuk meminta penjelasan Menteri Hukum dan HAM yang dinilai tidak independen dalam menyikapi perselisihan di internal Partai Golkar dan Partai Persatuan Pembangunan.
                </p><p class="font_kecil" style="text-align: justify;">Usulan tersebut merupakan salah satu upaya perlawanan yang dilakukan oleh kubu Aburizal melalui koalisi pendukungnya di DPR yang tergabung dalam Koalisi Merah Putih (KMP). Setidaknya sudah ada 116 anggota yang menandatangi usulan hak angket.
                </p><p class="font_kecil" style="text-align: justify;">Artinya hak angket yang digulirkan ke Menkumham  itu sudah memenuhi  syarat untuk dibawa ke paripurna, karena sudah ditandatangani 116  anggota DPR.',
    'ANALISA'=>'<img src="http://cdn1-a.production.liputan6.static6.com/medias/798664/big/045609700_1421832684-raker_1.jpg" class="pic2">Indikasi adanya intervensi politik yang dilakukan pemerintah terhadap polemik di internal Partai Golkar  menguat setelah Kemenkumham memutuskan sah kepengurusan Agung Laksono. Polemik tersebut merupakan babak tambahan dari kisruh Partai Golkar yang sudah berlangsung lama. Institusi Kementerian Hukum dan Hak Asasi Manusia (Kemenkumham) diduga dijadikan alat oleh oknum-oknum pemerintah dalam mengintervensi internal Golkar untuk memecah dan memperlemah kekuatan koalisi merah putih di parlemen. Seperti yang diketahui Partai Golkar adalah Oposisi di pemerintahan Jokowi-JK, yang tergabung dalam Koalisi Merah Putih (KMP). 
                </p><p style="text-align: justify;">Surat Keputusan (SK) yang dikeluarkan oleh Kementerian Hukum dan Ham, yang mengesahkan kepengurusan Partai Golkar hasil Munas Jakarta merupakan hasil kesepakatan politik yang dilakukan oleh Agung Laksono, Jusuf Kalla, Surya Palloh, dan Megawati. SK tersebut adalah pesanan Megawati ke Menteri Yasona sesaat setelah kontrak politik disepakati, yakni Partai Golkar versi Munas Jakarta bersedia bergabung ke dalam Koalisi Indonesia Hebat.',
    'QUOTE_PENDUKUNG'=>[
        ['from'=>'Ari Junaedi','jabatan'=>'Pengamat politik Universitas Indonesia','img'=>'http://sp.beritasatu.com/media/images/original/20141208171806988.jpg','url'=>'','content'=>'"Jika punya cita-cita membesarkan partai maka rekonsiliasi yang dilakukan. Saya rasa ini kepentingan eli-elit yang tidak terakomodir dengan keputusan mahkamah partai."'],
        ['from'=>'Surya paloh','jabatan'=>'','img'=>'http://img2.bisnis.com/makasar/posts/2014/09/26/180862/Surya-Paloh.jpg','url'=>'','content'=>'"Pemimpin Golkar Tak Boleh Tidak Siap Kalah Ketika Bertanding."'],
        ['from'=>'Aziz Syamsuddin','jabatan'=>'','img'=>'http://teropongsenayan.com/foto_berita/201501/05/medium_27Aziz%20Syamsudin%20002.jpg','url'=>'','content'=>'"Sangat suprise ya, tanpa ada pertimbangan Dirjen, Menkumham mengelurkan surat itu."'],
        ['from'=>'Jusuf Kalla','jabatan'=>'','img'=>'http://assets.kompasiana.com/statics/files/1402277267842193257.jpg','url'=>'','content'=>'"Kita harus mentaati keputusan mahkamah partai yang kemudian disahkan oleh Menkumham."'],
        ['from'=>'Agus Gumiwang','jabatan'=>'','img'=>'http://cdn.partainasdemo250.org/uploads/images/1408447172_AGUS_GUMIWANG.jpg','url'=>'','content'=>'"Sudah jelas dan sudah tidak terbantahkan lagi bahwa DPP Partai Golkar yang sah itu adalah DPP pimpinan Pak Agung Laksono karena SK Kemenkumham itu sah."']
    ],
    'QUOTE_PENENTANG'=>[
        ['from'=>'Abubakar Al-Habsyi','jabatan'=>'Ketua DPP Partai Keadilan Sejahtera','img'=>'http://www.wartabuana.com/pictures/Abubakar-Alhabsyi-201412181726271.jpg','url'=>'','content'=>'"Tugas pemerintah itu seharusnya hanya sebagai administrator, kalau yang sekarang sudah sampai ke intervensi."'],
        ['from'=>'Idrus Marham','jabatan'=>'Sekjen Golkar versi Munas Bali','img'=>'http://wartatujuh.com/wp-content/uploads/2015/03/drus-620x330.jpg','url'=>'','content'=>'"Negara ini hancur ketika kebijakan itu diambil berdasarkan penafsiran-penafsiran salah. Nah, itulah Menteri hukum ham kita sekarang ini"'],
        ['from'=>'Akbar Tandjung','jabatan'=>'','img'=>'http://www.solopos.com/dokumen/2010/06/akbar-tandjung-2.jpg','url'=>'','content'=>'"Keputusan Menkumham mengarah untuk memenangkan Agung"'],
        ['from'=>'Prabowo subianto','jabatan'=>'','img'=>'http://www.islamtoleran.com/wp-content/uploads/2014/07/prabowo-subianto-foto-f.jpg','url'=>'','content'=>'"Bagi saya dan bagi Gerindra yang saya pimpin, kami hanya mengakui bapak Aburizal Bakrie."'],
        ['from'=>'Fadli Zon','jabatan'=>'','img'=>'http://www.nonstop-online.com/wp-content/uploads/2015/03/fadli.jpg','url'=>'','content'=>'"Keputusan Menkumham ini sensitif, hak demokrasi parpol dipangkas dengan mudahnya melakukan intervensi politik, sehingga membuat dampak yang luas pada masyarakat. Golkar hanya salah satu korban saja, ini bisa terjadi pada yang lain, dan kalau dilakukan bisa jadi pengacau demokrasi."'],
        ['from'=>'Muntasir Hamid','jabatan'=>'Ketua Forum Silaturahmi DPD II Partai Golkar','img'=>'http://cdn-media.viva.co.id/thumbs2/2012/04/30/153002_muntasir-hamid_663_382.jpg','url'=>'','content'=>'"Dengan surat seperti ini bisa dicabut oleh Presiden. Karena diduga ada oknum di lingkaran Presiden Jokowi yang bermain di tengah kekisruhan ini."']
    ],
    'VIDEO'=>[
        ['id'=>'gycWAkep7j0'],
        ['id'=>'lp0uRAtlzo0'],
        ['id'=>'UXXvjPee7gk'],
        ['id'=>'blQg7nvA8GY'],
        ['id'=>'zl5ewqmv4P8'],
        ['id'=>'01FtCzymc5M']
    ],
    'FOTO'=>[
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img1.jpg'],
        ['img'=>'http://www.bijaks.net/assets/images/hotpages/ahokvsdprd/img2.jpg'],
        ['img'=>'http://img.bisnis.com/posts/2015/03/22/414462/dukungan-terhadap-ahok.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/02/27/id_375043/375043_620_tempoco.jpg'],
        ['img'=>'http://images.detik.com/customthumb/2015/03/23/10/171112_dprdkomahook4.jpg?w=460'],
        ['img'=>'http://cdn.sindonews.net/dyn/620/content/2015/03/01/31/970362/ahok-vs-dprd-dan-politik-adu-kuat-WkD.jpg'],
        ['img'=>'http://cdn-media.viva.co.id/thumbs2/2015/03/05/299841_mediasi-ahok-dprd-berujung-ricuh_663_382.jpg'],
        ['img'=>'http://statik.tempo.co/data/2015/03/03/id_376061/376061_620_tempoco.jpg'],
    ]
]

?>

<style>
    .boxcustom {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/back-kronologi.png");?>');
        padding: 10px;
    }
    .boxblue {
        background-color: #63839c;
        padding: 10px;
        border-radius: 5px;
        color: white;
        margin-bottom: 10px;
    }
    .boxcustom2 {
        background-color: #eaf7e3;
        padding-bottom: 20px;
    }
    .boxcustom3 {
        background-color: #e95757;
        padding-bottom: 20px;
    }
    .boxdotted {
        border-radius: 10px;
        border: 2px dotted #bcbb36;
        width: 100%;
        height: auto;
        margin-bottom: 10px;
        padding-top: 5px;
    }
    .black {
        color: black;
    }
    .white {
        color: white;
    }
    .green {
        color: #e9f0ae;
    }
    .list {
        background: url('<?php echo base_url("assets/images/hotpages/hukumanmati/point.png")?>') no-repeat 0px center;
        padding-left: 30px;
    }
    .list2 {
        list-style-image: url('<?php echo base_url("assets/images/hotpages/cakapolri/pointles.jpg")?>');
    }
    .block_green {
        background-color: #00a651;
    }
    .block_red {
        background-color: #a60008;
    }
    #bulet {
        background-color: #555555; 
        text-align: center;
        width: 35px;
        height: 15px;
        border-radius: 50px 50px 50px 50px;
        -webkit-border-radius: 50px 50px 50px 50px;
        -moz-border-radius: 50px 50px 50px 50px;
        color: white;
        padding: 4px 8px;
        margin-right: 5px;
        font-size: 12px;
    }
    .bendera {
        width: 150px;
        height: 75px;
        margin-left: 10px;
        float: right;
        border: 1px solid black;
        margin-top: 10px;
    }
    .pic {
        float: left;
        margin-right: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic2 {
        float: right;
        margin-left: 10px;
        max-width: 150px;
        margin-top: 5px;
    }
    .pic3 {
        float: left;
        margin-right: 10px;
        max-width: 100px;
        margin-top: 5px;
    }
    .ketua {
        height: 70px;
        width: 100%;
    }
    .ketua2 {
        height: 50px;
        width: 50px;
        margin-bottom: 5px;
    }
    .clear {
        clear: both;
    }
</style>

<h4 class="kanal-title kanal-title-gray"><?php echo $data['NARASI']['title'];?></h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="gaza">
        <img src="<?php echo base_url("assets/images/hotpages/kisruhgolkar/top.jpg")?>" style="width: 100%;height: auto;">
        <div class="panel-collapse collapse in" style="margin-top:10px;">
            <p style="text-align: justify;"><?php echo $data['NARASI']['narasi'];?></p>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SALING SIDANG</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SALING'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">PETA KEKUATAN</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['DUKUNGAN']['narasi'];?></p>
                <?php
                foreach($data['DUKUNGAN']['satu'] as $key=>$val) {
                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $pageName = strtoupper($personarr['page_name']);
                    if($val['page_id'] == 'zainuddinamali5456ee91ad531'){
                        $photo = 'http://cdn-2.tstatic.net/surabaya/foto/bank/images/zainuddin-amali-target-golkar-20-kursi-surabaya.jpg';
                    }
                    else {
                        $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                    }
                    ?>
                    <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                        <img src="<?php echo $photo;?>" class="ketua2" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>">
                    </a>
                <?php
                }
                ?>

                <p style="text-align: justify;"><?php echo $data['DUKUNGAN']['narasi2'];?></p>
                <?php
                foreach($data['DUKUNGAN']['dua'] as $key=>$val) {
                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $pageName = strtoupper($personarr['page_name']);
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                    ?>
                    <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                        <img src="<?php echo $photo;?>" class="ketua2" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>">
                    </a>
                <?php
                }
                ?>
                
                <p style="text-align: justify;"><?php echo $data['DUKUNGAN']['narasi3'];?></p>
                <?php
                foreach($data['DUKUNGAN']['tiga'] as $key=>$val) {
                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $pageName = strtoupper($personarr['page_name']);
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                    ?>
                    <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                        <img src="<?php echo $photo;?>" class="ketua2" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>">
                    </a>
                <?php
                }
                ?>
                
                <p style="text-align: justify;"><?php echo $data['DUKUNGAN']['narasi4'];?></p>
                <?php
                foreach($data['DUKUNGAN']['empat'] as $key=>$val) {
                    $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                    $personarr = json_decode($person, true);
                    $pageName = strtoupper($personarr['page_name']);
                    if($val['page_id'] == 'nurdinhalid530eb1689eb22'){
                        $photo = 'http://www.rmol.co/images/berita/thumb/thumb_261225_08030202112014_ade.JPG';
                    }
                    else {
                        $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                    }
                    ?>
                    <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                        <img src="<?php echo $photo;?>" class="ketua2" alt="<?php echo $pageName;?>" data-toggle="tooltip" data-original-title="<?php echo $pageName;?>">
                    </a>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SIASAT ICAL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['SIASAT'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">MENJEGAL ICAL</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['MENJEGAL'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KETUA UMUM GOLKAR DARI MASA KE MASA</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in row" style="margin-top:10px;">
                <?php
                foreach ($data['KETUA'] as $key => $val) { ?>
                <div class="col-xs-3">
                    <img src="<?php echo $val['img']; ?>" class="img-responsive ketua">
                    <p class="text-center" style="font-size: 8px;"><?php echo $val['name']; ?><br>
                    <span class="text-center"><?php echo $val['thn']; ?></span></p>
                </div>
                <?php
                }
                ?>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">SEJARAH SINGKAT</span></h5>
        <div id="accordion" class="panel-group" style="height: auto;">
            <div class="panel-collapse collapse in" style="margin-top: 10px;">
                <ol style="list-style-type: square;margin-left: -10px;">
                    <?php
                    foreach ($data['PROFIL']['sejarah'] as $key => $val) {
                        echo "<li style='margin-right: 20px;'>".$val['no']."</li>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom2" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_green text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENDUKUNG</h5>
            <p>PARPOL PENDUKUNG</p>
            <?php
            foreach($data['PENDUKUNG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                if($val['page_id'] == 'partaikebangkitanbangsa5119b257621a4'){
                    $photo = 'http://www.dpp.pkb.or.id/unlocked/unlock/lambangresmipkb.jpg';
                }
                else {
                    $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                }
                ?>
                <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <div class="black boxcustom3" style="padding-left: 20px;border-radius: 10px 10px 0 0;">
            <h5 class="white block_red text-center" style="margin-left: -20px;border-radius: 10px 10px 0 0;padding-top: 10px;padding-bottom: 10px;">PENENTANG</h5>
            <h5 style="padding-top: 15px;">PARPOL PENENTANG</h5>
            <?php
            foreach($data['PENENTANG']['partai'] as $key=>$val){
                $person = $this->redis_slave->get('profile:detail:'.$val['page_id']);
                $personarr = json_decode($person, true);
                $pageName = strtoupper($personarr['page_name']);
                $photo =  (count($personarr['profile_photo']) > 0) ? $personarr['profile_photo'][0]['badge_url'] : '';
                ?>
                <a href="http://www.bijaks.net/aktor/profile/<?php echo $val['page_id'];?>">
                    <img style="margin-right: 10px;margin-bottom: 10px;max-height: 70px;" src="<?php echo $photo;?>" alt="<?php echo $pageName;?>"/>
                </a>
                <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KRONOLOGI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <ol style="list-style-type: circle;margin-left: -10px;">
                    <?php
                    foreach ($data['KRONOLOGI'] as $key => $val) {
                        echo "<li style='margin-right: 20px;font-weight: bold;'>".$val['date']."</li><p>".$val['content']."</p>";
                    }
                    ?>
                </ol>
            </div>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">KRITIK TERHADAP PUTUSAN MENTERI</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['KRITIK'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">USULAN HAK ANGKET</span></h5>
        <div id="accordion" class="panel-group">
            <div class="panel-collapse collapse in" style="margin-top:10px;">
                <p style="text-align: justify;"><?php echo $data['USULAN'];?></p>
            </div>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENDUKUNG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            foreach($data['QUOTE_PENDUKUNG'] as $key=>$val) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $val['url'];?>"><img src="<?php echo $val['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $val['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $val['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: blue;margin-left: 10px;margin-right: 10px;"><?php echo $val['content'];?></p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">QUOTE PENENTANG</span></h5>
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=0; $i < 2; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content'];?></p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>        
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=2; $i < 4; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content'];?></p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>
        <div id="accordion" class="panel-group row">
            <?php
            for ($i=4; $i < 6; $i++) { ?>
            <div class="panel-collapse collapse in col-xs-6">
                <p style="font-size: 12px;text-align: left;margin-left: 10px;margin-right: 10px;"><a href="<?php echo $data['QUOTE_PENENTANG'][$i]['url'];?>"><img src="<?php echo $data['QUOTE_PENENTANG'][$i]['img'];?>" style="width: 50px;margin-top: 10px;margin-right: 5px;float: left;"/></a>
                    <b style="font-size: 14px;float: left;margin-top: 13px;margin-left: 10px;width: 80px;line-height: 15px;"><?php echo $data['QUOTE_PENENTANG'][$i]['from'];?></b><br>
                    <div class="clear"></div><span style="font-size: 12px;float: left;margin-left: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['jabatan'];?></span>
                </p>
                <div class="clear"></div>
                <p style="font-size: 12px;color: red;margin-left: 10px;margin-right: 10px;"><?php echo $data['QUOTE_PENENTANG'][$i]['content'];?></p>
                <hr style="width: 90%;border-color: black;margin-top: -3px;margin-bottom: -3px;margin-left: 5%;">                
            </div>
            <?php
            }
            ?>
        </div>
        <div class="clear"></div>

        <h5 class="sub-kanal-title kanal-title-gray-soft black"><span id="perang_sebelumnya">VIDEO TERKAIT</span></h5>
        <div id="accordion" class="panel-group row" style="margin-bottom: 0px !important;">
            <?php
            foreach($data['VIDEO'] as $key=>$val){
                ?>
                <p class="col-xs-12 col-sm-6"><iframe width="100%" height="265" src="https://www.youtube.com/embed/<?php echo $val['id'];?>" frameborder="0" allowfullscreen></iframe></p>
                <?php
            }
            ?>
        </div>

        <h5><a href="" id="backToTop">Back to Top</a></h5>

        <div><?php $dt['news'] = $news; $this->load->view('tpl_berita', $dt); ?></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
        $('#backToTop').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 2000);
            return false;
        });
    });
</script>



