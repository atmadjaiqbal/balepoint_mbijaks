<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scandal extends MX_Controller
{
	public $membership;
	public $uID;
	public $isLogin;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('core/bijak');

		$this->load->library('scandal/scandal_lib');
		$this->load->helper('text');
        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');
        $this->load->module('headline');
        $this->load->module('survey');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
		$this->current_url = '/'.$this->uri->uri_string();
		$this->session->set_flashdata('referrer', $this->current_url);

        $this->member = $this->session->userdata('member');

		//$this->checkMember();
		// $this->checkUID();
	}

	public function index($URL="")
	{
		$member         			= $this->session->userdata('member');
		$data['member'] 			= $this->membership;
		$data['sidebar']  		= $this->bijak->getSidebarSentimen();
		$data['template_path'] 	= 'template/';
		$data['show_header'] 	= TRUE;
		$data['title'] 			= 'Bijaks.Net';
        $data['scripts'] = array('bijaks.js', 'bijaks.spam.js');

        // Managing banners
        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
        $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
        # 1 = ios
        # 2 = android
        if( $iPod || $iPhone ){
            $type = "ios";
        }elseif($iPad){
            $type = "ios";
        }elseif($Android){
            $type = "a";
        }elseif($webOS){
            $type = "a";
        }else{
            $type = 'a';
        }        


        $ads_banner = [];
        $adbanners = $this->redis_slave->get("list:banners");
        $temp = '';
        if(!empty($adbanners)){
            $adbanners = json_decode($adbanners,true);
            foreach($adbanners as $adb){
                if($adb['is_image_shown'] == 1){
                    $ilink = base_url('home/ad?id='.$adb['id'].'-'.$type);
                    $temp .= '<a href="'.$ilink.'" target="_blank">';
                    if(substr($adb['image_url'], 0,7) != 'http://'){
                        $urlArray = explode('/', $adb['image_url']);
                        $tempNb = count($urlArray);
                        $fname = 'small_'.$urlArray[$tempNb-1];
                        $imageUrl = '';
                        for($i=0;$i<=$tempNb-2;$i++){
                            $imageUrl .= $urlArray[$i].'/';
                        }
                        $imageUrl .= $fname;
                        $temp .= '<img src="'.base_url($imageUrl).'" style="width: 100%;height: auto;"/></a>';
                    }else{
                        $urlArray = explode('/', $adb['image_url']);
                        $tempNb = count($urlArray);
                        $fname = 'small_'.$urlArray[$tempNb-1];
                        $imageUrl = '';
                        for($i=0;$i<=$tempNb-2;$i++){
                            $imageUrl .= $urlArray[$i].'/';
                        }
                        $imageUrl .= $fname;
                        $temp .= '<img src="'.$imageUrl.'" style="width: 100%;height: auto;"/></a>';
                    }
                }

                if($adb['is_text_shown'] == 1){
                    $tlink = base_url('home/ad?id='.$adb['id'].'-'.$type);
                    $temp .= '<a href="'.$tlink.'" target="_blank">';
                    $temp .= '<div class="text_adds">'.$adb['text_shown'];
                    $temp .= '<span style="margin-left: 40px;color:red;font-size: 14px;font-weight: bold;font-family : Courier New, Courier, monospace;">';
                    $temp .= '(download here)</span></div></a><br>';
                }
                $ads_banner[] = $temp;
                shuffle($ads_banner);
                $temp = '';
            }
        }
        $data['ads_banner'] = $ads_banner;
        // End Managing banners
//		if($member)
//		{
			if(!empty($URL))
			{
                $data['category'] = 'scandal';

				$arrUrl     = explode("-", $URL);
				$scandalID  = intval($arrUrl[0]);


//                $scandal_detail = $this->getscandalcache($scandalID, $limit=0, $offset=0, $result=2);
                $scandal_detail = $this->scandal_lib->getScandalDetail($scandalID);
                $data['category'] = 'scandal';
                $data['skandal'] = $scandal_detail;

//                $data['skandal_terkait'] = $this->getscandalrelationscache($scandal_detail['players'], $scandalID);
                $data['skandal_terkait'] = $this->getScandalRelations($scandal_detail['players'], $scandalID);

                $data['head_title'] = 'Scandal ' . $scandal_detail['title'];

                $html['html']['content'] = $this->load->view('mobile/scandal_detail', $data, true);
                $this->load->view('m_tpl/layout', $html);

			}
			else
			{

                
                $data['category'] = 'scandal';

                $limit = 19;
                $page = 1;
                $per_page = $this->input->get('per_page');
                if(!is_bool($per_page)){
                    if(!empty($per_page)){
                        $page = $per_page;
                    }
                }

                $offset = ($limit*intval($page)) - $limit;
                if($offset != 0){
                     $offset += (intval($page) - 1);
                }

                $qr = $_SERVER['QUERY_STRING'];

                $rkey = 'scandal:list:all';
                $user_count = $this->redis_slave->lLen($rkey);

                $scandal_list_m = array();
                $scandal_list = array();

                // get scandal healine
                if($page == 1){
                    $limit -= 1;
                    $user_count = intval($user_count) + 1;
                    $head_key = 'scandal:list:headline';
                    $hot_key = 'scandal:list:hot';

                    $head_id = $this->redis_slave->lRange($head_key, 0, 0);
                    $hot_id = $this->redis_slave->lRange($hot_key, 0, 0);

                    $head_detail = $this->getscandalcache($head_id[0], 0, 0, 0);
                    $scandal_list_m[0] =  $head_detail;
                    if($head_id != $hot_id){
                        $limit -= 1;
                        $user_count = intval($user_count) + 1;
                        $hot_detail = $this->getscandalcache($hot_id[0], 0, 0, 0);
                        $scandal_list_m[1] = $hot_detail;
                    }
                }

                $scandal_list_l = $this->getscandalcache($id=0, $limit, $offset, $result=1);

                $scandal_list = array_merge(array_values($scandal_list_m), array_values($scandal_list_l));
                
                $idCheck = array();
                foreach ($scandal_list as $key => $tag_name) {
                    if(in_array($tag_name['scandal_id'], $idCheck)){
                        unset($scandal_list[$key]);
                    }
                    $idCheck[] = $tag_name['scandal_id'];
                }
//                echo '<pre>';
//                var_dump($scandal_list);
//                echo '</pre>';

                $this->load->library('pagination');

                $config['base_url'] = base_url().'scandal/index/?'; //.$qr;
                $config['total_rows'] = $user_count;
                $config['per_page'] = $limit;
                $config['use_page_numbers'] = TRUE;
                $config['page_query_string'] = TRUE;
                $choice = $config["total_rows"] / $config["per_page"];
                $config["num_links"] = 2; // round($choice);
                $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
                $config['full_tag_close'] = '</ul>';
                $config['cur_tag_open'] = '<li class="disabled"><a>';
                $config['cur_tag_close'] = '</a></li>';
                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';
                $config['next_link'] = '...';
                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';
                $config['prev_link'] = '...';
                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';
                $config['first_link'] = FALSE;
                $config['last_link'] = FALSE;

                $this->pagination->initialize($config);

                $data['pagi'] =  $this->pagination->create_links();
                $data['skandal'] = $scandal_list;
                $data['offset'] = $offset;
                $data['total']	= $user_count;

                $data['head_title'] = 'Scandal';

                


                $html['html']['content'] = $this->load->view('mobile/scandal_index', $data, true);
                $this->load->view('m_tpl/layout', $html);

			}
//		}
//		else
//		{
//			redirect(base_url().'home/login/?redirect='.$this->current_url);
//		}
	}

    private function getscandalrelationscache($page_list = array(), $id)
    {


        $key = 'profile:skandal:';
        $skandal_list_terkait = array();
        foreach($page_list as $num => $row){
            $key .= $row['page_id'];
            $skandal_list_by_page_id = $this->redis_slave->get($key);
            $skandal_list_arr = json_decode($skandal_list_by_page_id, True);
            if(count($skandal_list_arr) > 0)
                $skandal_list_terkait = array_merge($skandal_list_arr, $skandal_list_terkait);
        }



        $scandal_id = array();
        foreach($skandal_list_terkait as $num => $row){
            if($id == $row['scandal_id'])
                continue;
            $scandal_id[$num] = $row['scandal_id'];
        }


        $max_scandal_list = 3;
        $start = 0;

        $no_max = true;
        $num_skandal = count($scandal_id);
        while ($num_skandal < 3){
            $offset = ($max_scandal_list - $num_skandal)+$start + 1;
//            echo '<pre>';
//            var_dump($offset .' - '.$start);
//            echo '</pre>';
            $skandal_lain = $this->redis_slave->lRange('scandal:list:all', $start, $offset);
            $scandal_id = array_merge($skandal_lain, $scandal_id);
            if(in_array($id, $skandal_lain)){
                $inde = array_search($id, $skandal_lain);
//                var_dump($inde);
                unset($scandal_id[$inde]);
            }
            $start = $offset;
            $num_skandal = count($scandal_id);
            if($start >= 30){
                break;
            }
        }
        $skandal_detail = array();
        foreach($scandal_id as $num=>$row){
            if($num == 4)
                break;
            $skandal = $this->getscandalcache($row, $limit=0, $offset=0, $result = 0);
            $skandal_detail[$num] = $skandal; // json_decode($skandal, true);
        }

        return $skandal_detail;

    }

    public function kronologies_list()
    {
        $scandal_id = $this->uri->segment(3);
        $page = $this->uri->segment(4);
        $limit = 20;
        $offset = ($limit*intval($page)) - $limit;
        $kronologies = $this->scandal_lib->GetScandalKronologies($scandal_id, $limit, $offset);
        $data['result'] = $kronologies;
        $data['count_result'] = count($kronologies);
//        var_dump($kronologies);
        $this->load->view('scandal_kronologies_list', $data);
    }

    private function getscandalcache($id=0, $limit=9, $offset=0, $result=1, $key_exist=0)
    {
        if($result === 1){

            $rkey = 'scandal:list:all';

            $rrange = $offset + $limit;

            $skandal_list = $this->redis_slave->lRange($rkey, $offset, $rrange);
            if($key_exist != 0){
                if(in_array($key_exist, $skandal_list) ){
                    $news = $this->redis_slave->lRange($rkey, $offset, intval($rrange) + 1);
                }
            }

            $result = array();
            $i = 0;
            if(count($skandal_list) > 0){
                foreach ($skandal_list as $key => $value) {

                    if($key_exist == intval($value))
                        continue;

                    $skandal_detail = $this->redis_slave->get('scandal:min:'.$value);
                    $skandal_array = @json_decode($skandal_detail, true);

                    $skandal_kronologi = $this->redis_slave->get('scandal:kronologi:'.$value);
                    $skandal_kronologi_array = @json_decode($skandal_kronologi, true);

                    $skandal_photo = $this->redis_slave->get('scandal:photo:'.$value);
                    $skandal_photo_array = @json_decode($skandal_photo, true);

                    $skandal_players = $this->redis_slave->get('scandal:players:'.$value);
                    $skandal_players_array = @json_decode($skandal_players, true);

                    $skandal_relations = $this->redis_slave->get('scandal:relations:'.$value);
                    $skandal_relations_array = @json_decode($skandal_relations, true);

                    $skandal_array['photos'] = array_reverse($skandal_photo_array, true);
                    $skandal_array['kronologi'] = $skandal_kronologi_array;
                    $skandal_array['players'] = $skandal_players_array;
                    $skandal_array['relations'] = $skandal_relations_array;


                    $result[$i] = $skandal_array;

                    $i++;
                }
            }
            return $result;
        }else{
            $skandal_detail = $this->redis_slave->get('scandal:min:'.$id);
            $skandal_array = @json_decode($skandal_detail, true);

            $skandal_kronologi = $this->redis_slave->get('scandal:kronologi:'.$id);
            $skandal_kronologi_array = @json_decode($skandal_kronologi, true);

            $skandal_photo = $this->redis_slave->get('scandal:photo:'.$id);
            $skandal_photo_array = @json_decode($skandal_photo, true);

            $skandal_players = $this->redis_slave->get('scandal:players:'.$id);
            $skandal_players_array = @json_decode($skandal_players, true);

            $skandal_relations = $this->redis_slave->get('scandal:relations:'.$id);
            $skandal_relations_array = @json_decode($skandal_relations, true);

            $skandal_array['photos'] = $skandal_photo_array; //array_reverse($skandal_photo_array, true);
            $skandal_array['kronologi'] = $skandal_kronologi_array;
            $skandal_array['players'] = $skandal_players_array;
            $skandal_array['relations'] = $skandal_relations_array;

            return $skandal_array;

        }
    }

	protected function getScandalList($page='0', $limit='10')
	{
		$data['total_rows']  = $this->scandal_lib->getScandalListCount();
		$data['scandals']    = $this->scandal_lib->getScandalList('',$page, $limit); //getScandalList_cache_120
		return $data;
	}

	public function load_more($page='1', $limit='10')
	{
		// first page use limit 10 so next offset is 11
		$offset           = $page*$limit;
		$data             = $this->getScandalList($offset, $limit);
		$data['offset']   = $offset;
		$totalPage        = ceil($data['total_rows']/$limit);
		$nextPage         = (int)$page+1;
		$load_more        = ($page < $totalPage) ? '1': '0';

		$html = $this->load->view('scandal/scandal_list', $data, true);

		// HTML Response, Flag Load More, Next Page
		echo $html .'||'. $load_more .'||'. $nextPage;
	}


	private function checkMember() {
		$this->membership = $this->session->userdata('member');
		if($this->membership) {
			$this->membership['current_city'] = $this->member->getUserCurrentCity($this->membership['account_id']);
		} else {
			redirect($this->data['base_url'].'home/login/?redirect='.$this->current_url);
		}
	}

	public function checkUID() {
		$uID = $this->input->post('uID');
		if(isset($_GET['uid']) && ! empty($_GET['uid'])) {
			$this->uID = $_GET['uid'];
			$this->isLogin = $this->isLogin($this->uID, $this->membership['account_id']);
		} else if(! empty($uID)) {
			$this->uID = $uID;
			$this->isLogin = $this->isLogin($this->uID, $this->membership['account_id']);
		} else {
			$this->uID  = $this->membership['account_id'];
			$this->isLogin = true;
		}
	}

	private function isLogin($str1, $str2) {
		if(! strcmp($str1, $str2)) {
			return true;
		} else {
			return false;
		}
	}

	function sesi($return='false')
	{
        $ScandalAllKey = 'scandal:list:headline';
        $skandal_list = $this->redis_slave->lRange($ScandalAllKey, 0, 0);
//        var_dump($skandal_list);
        $scandal = $this->getscandalcache($skandal_list[0], 0, 0, 0);
//		echo '<pre>';
//        var_dump($scandal);
//		echo '</pre>';
        $data['skandal'] = $scandal;

		if ($return == 'true')
			return $this->load->view('template/skandal/tpl_skandal_index', $data, true);

		$this->load->view('template/skandal/tpl_skandal_index', $data);		
	}

    /**
     * @param array $page_list
     * @param $id
     * @return array
     */
    private function getScandalRelations($page_list = array(), $id){
        # This Block is Using MySQL
        $skandal_list_terkait = array();
        foreach($page_list as $num => $row){
            $skandal_list_arr = $this->scandal_lib->getScandalByPlayer($row['page_id']);
            if(count($skandal_list_arr) > 0)
                $skandal_list_terkait = array_merge($skandal_list_arr, $skandal_list_terkait);
        }
        # /This Block is Using MySQL

        $scandal_id = array();
        foreach($skandal_list_terkait as $num => $row){
            if($id == $row['scandal_id'])
                continue;
            $scandal_id[$num] = $row['scandal_id'];
        }

        $max_scandal_list = 3;
        $start = 0;

        $no_max = true;
        $num_skandal = count($scandal_id);
        while ($num_skandal < 3){
            $offset = ($max_scandal_list - $num_skandal)+$start + 1;
            $skandal_lain = $this->scandal_lib->getScandalList('',$start,$offset); # MySQL way
            $scandal_id = array_merge($skandal_lain, $scandal_id);
            if(in_array($id, $skandal_lain)){
                $inde = array_search($id, $skandal_lain);
                unset($scandal_id[$inde]);
            }
            $start = $offset;
            $num_skandal = count($scandal_id);
            if($start >= 30){
                break;
            }
        }
        $skandal_detail = array();
        foreach($scandal_id as $num=>$row){
            if($num == 4)
                break;
            $skandal = $this->scandal_lib->getScandalDetail($row);  # MySQL way
            $skandal_detail[$num] = $skandal; // json_decode($skandal, true);
        }
        return $skandal_detail;
    }
}