<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scandal_Lib {

	protected  $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database('slave');
	}

	public function getScandalList($tipe='', $page='0', $limit = '10') {
		$this->CI->load->helper('bijak');
		$member 					= $this->CI->session->userdata('member');
		$member_account_id 	= (isset($member['account_id'])) ? $member['account_id'] : NULL;

	   $where 		= '';
	   $order_by 	= ' date DESC ';
	   if ($tipe == 'hotlist')  {
	   	$where 		= " AND ts.hotlist = '1' ";
	   	$order_by 	= ' ts.hot_date DESC ';
	   }

		if ($tipe == 'headline')  {
			$where 		= " AND ts.headline = '1' ";
			$order_by 	= ' ts.head_date DESC ';
		}


		$sql     = "SELECT ts.scandal_id,tc.content_id,  ts.title, ts.status, ts.date, ts.updated_date, ts.uang, ts.publish, ts.short_desc, left(ts.description,250) description,
								tc.count_comment, tc.count_like, tc.count_dislike,
								get_count_like_content_by_account('".$member_account_id."', tc.content_id) as is_like,
								get_count_dislike_content_by_account('".$member_account_id."', tc.content_id) as is_dislike
		            FROM tscandal ts
		            LEFT JOIN tcontent tc on ts.scandal_id = tc.location AND tc.content_group_type = '50'
		            WHERE main_scandal = '0' and publish = 1 ".$where."
		            ORDER BY date DESC LIMIT ".$page.", ".$limit;
		$result  = $this->CI->db->query($sql)->result_array();
		foreach ($result as $key => $row) {
		   $photos                 		= $this->getScandalPhoto($row['scandal_id']);
			$result[$key]['url']          = base_url() . "scandal/index/" . $row['scandal_id'] . "-" . urltitle($row['title']) . "/";
			$result[$key]['photo']        = (isset($photos[0])) ? $photos[0]['attachment_title'] : 0;
			$result[$key]['photos']       = (!empty($photos)) ? $photos : array() ;
			$result[$key]['players']      = $this->getScandalPlayer($row['scandal_id']);
			$contentDate 						= mysql_date("%d %M %Y - %h:%i", strtotime($row['updated_date']));
			$result[$key]['content_date']   =  $contentDate;
         //$result[$key]['content_count']  =  $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate);
         $result[$key]['content_count']  =  '';
		}
		return $result;
	}

public function getScandalListCount() {
		$sql     = "SELECT count(scandal_id) total_rows FROM tscandal WHERE main_scandal = '0' and publish = 1";
		$result  = $this->CI->db->query($sql)->row_array();
		$return  = '0';
		if (isset($result['total_rows'])) $return = $result['total_rows'];
		return $return;
	}

	public function getScandalDetail($scandalID) {
		$sql     = "SELECT ts.scandal_id, tc.content_id, ts.title, ts.uang, ts.status, ts.short_desc, ts.description, ts.updated_date,
							tc.count_comment, tc.count_like, tc.count_dislike
		            FROM tscandal ts
		            LEFT JOIN tcontent tc on ts.scandal_id = tc.location AND tc.content_group_type = '50'
		            WHERE ts.scandal_id = '$scandalID' ";
      $result  = $this->CI->db->query($sql)->row_array();
      if (!empty($result)) {
         $cm      = $this->CI->load->module('ajax');
		   $photos              = $this->getScandalPhoto($result['scandal_id']);
			$result['photo']     = (isset($photos[0])) ? $photos[0]['attachment_title'] : 0;
			$result['photos']    = (!empty($photos)) ? $photos : array() ;
         $result['players']   = $this->getScandalPlayer($result['scandal_id']);

         $contentDate         		= mysql_date("%d %M %Y - %h:%i", strtotime($result['updated_date']));
         $result['content_date']   	= $contentDate ;
         $result['content_count']   = $cm->count_content($result['content_id'], '2', '2', NULL, $contentDate);
         $result['kronologi'] = $this->getScandalKronologi($result['scandal_id']);
         $result['relations'] = $this->getScandalAliran($result['scandal_id']);

      }
		return $result;
	}

	public function getScandalPhoto($scandalID) {
		$sql     = "SELECT *  FROM tcontent_attachment
		            WHERE content_id in (SELECT content_id FROM tscandal_photo Where scandal_id = '$scandalID' ORDER BY created_date DESC)";
      $result  = $this->CI->db->query($sql)->result_array();
		return $result;
	}


	public function getScandalPlayer($scandalID, $offset = 0, $limit = 5) {
		$LIMIT = "LIMIT $offset, $limit ";
		if (!$offset)  $LIMIT = '';

		$sql = "SELECT tp.page_id as pg_id, tp.title, tp.description, top.page_id, top.profile_content_id, top.page_name , ta.attachment_title
					  FROM tscandal_player tp
					  LEFT JOIN tobject_page top ON top.page_id = tp.page_id
					  LEFT JOIN tcontent_attachment ta ON ta.content_id = top.profile_content_id
					  WHERE scandal_id = '$scandalID'
					  ORDER BY tp.nomor_urut, tp.player_id DESC    $LIMIT";
		$result = $this->CI->db->query($sql)->result_array();
		return $result;
	}

	public function getScandalKronologi($scandalID) {
		$sql     = "SELECT scandal_id, title, date, short_desc FROM tscandal WHERE main_scandal = '$scandalID' order by date ";
        $result  = $this->CI->db->query($sql)->result_array();
		return $result;
	}

    public function GetScandalKronologies($scandalID, $limit=0, $offset=0)
    {
        $lm = '';
        if (intval($limit) != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }

        $sql     = "SELECT scandal_id, title, date, short_desc FROM tscandal WHERE main_scandal = '$scandalID' order by date $lm";
        $result  = $this->CI->db->query($sql)->result_array();
        return $result;
    }

	public function getScandalAliran($scandalID) {
		$sql     = "SELECT tr.*, ta.attachment_title source_attachment,  tat.attachment_title target_attachment
						FROM trelations_map tr
					   LEFT JOIN tobject_page top ON top.page_id = tr.source_id
					   LEFT JOIN tcontent_attachment ta ON ta.content_id = top.profile_content_id

					   LEFT JOIN tobject_page topt ON topt.page_id = tr.target_id
					   LEFT JOIN tcontent_attachment tat ON tat.content_id = topt.profile_content_id
						WHERE tr.scandal_id = '$scandalID' order by tanggal ";
      $result  = $this->CI->db->query($sql)->result_array();
		return $result;
	}

    public function getScandalByPlayer($page){
        $sql = "SELECT tp.date, tp.`title` AS title_player, tp.`description` AS player_desc, tp.`short_desc`, tp.`sumber_data`, tp.`nomor_urut`, tp.`pengaruh`,
            ts.`scandal_id`, ts.`title` AS scandal_title, ts.`description` AS scandal_desc
            FROM tscandal_player tp
            LEFT JOIN tscandal ts ON ts.`scandal_id`=tp.`scandal_id`
            WHERE tp.page_id  = '".$page."'
            ORDER BY tp.date desc";
        $result  = $this->CI->db->query($sql)->result_array();
        return $result;
    }

}

/* End of file file.php */