<?php
$skandal_url = base_url() . 'scandal/index/'.$skandal['scandal_id'].'-'.urltitle($skandal['title']);
$skandal_photos = (count($skandal['photos']) > 0) ? $skandal['photos'][0]['large_url'] : base_url('assets/images/img-default-scandal.png');

$skandal_photos = base_url('assets/images/img-default-scandal.jpg');

$headx_photos = null; // (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
$headm_photos = null; //(count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';

foreach($skandal['photos'] as $kes => $item){
    if(key_exists('type', $item)){
        if(intval($item['type']) == 1){
            $headx_photos = $item['large_url'];
            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
                $headx_photos = $item['original_url'];
            }

            $headm_photos = $item['thumb_url'];
            break;
        }
    }
}

?>

<div class="container">
    <?php
//        $dt_sub_header['skandal_photos'] = $skandal_photos;
//        $dt_sub_header['status'] = $skandal['status'];
//        $dt_sub_header['dampak'] = $skandal['uang'];

    $skandal_photos = $skandal_photos;
    $status = ucwords($skandal['status']);
    $dampak = $skandal['uang'];

    ?>
    <?php $this->load->view('template/tpl_sub_header'); ?>
<!--    <?php $this->load->view('template/tpl_sub_header_small', $dt_sub_header); ?> -->
</div>
<br/>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- DETAIL -->
            <div class="span8">
                <div class="news-detail">
                    <h2><?php echo $skandal['title']; ?></h2>
<!--                    <h4>--><?php //echo mdate('%d %M %Y', strtotime($skandal['date'])); ?><!--</h4>-->


                    <div class="skandal-container-tab">
                        <ul id="myTab" class="nav nav-tabs">
                            <li class="active"><a  href="#detil" data-toggle="tab">DETIL</a></li>
                            <li><a href="#kronologis" data-toggle="tab">KRONOLOGIS</a></li>
                            <li><a href="#players" data-toggle="tab">PARA PEMAIN</a></li>
                            <li><a href="#aliran_dana" data-toggle="tab">KONEKSI & ALIRAN DANA</a></li>
                            <li><a href="#powermap" data-toggle="tab">SKANDAL POWERMAP</a></li>
                        </ul>
<!--                        <div class='skandal-container-bottom-tab' id="myTab">-->
<!--                          <div class="active skandal-tab skandal-tab-title">-->
<!--                              <a href="#detil" data-toggle="tab" class="">DETIL</a>-->
<!--                          </div>-->
<!--                          <div class="skandal-tab skandal-tab-divider"></div>-->
<!--                          <div class="skandal-tab skandal-tab-title">-->
<!--                             <a data-toggle="tab" href="#kronologis">KRONOLOGIS</a>-->
<!--                          </div>-->
<!--                          <div class="skandal-tab skandal-tab-divider"></div>-->
<!--                          <div class="skandal-tab skandal-tab-title">-->
<!--                             <a data-toggle="tab" href="#players">SKANDAL PLAYER</a>-->
<!--                          </div>-->
<!--                          <div class="skandal-tab skandal-tab-divider"></div>-->
<!--                          <div class="skandal-tab skandal-tab-title">-->
<!--                             <a data-toggle="tab" href="#aliran_dana">KONEKSI & ALIRAN DANA</a>-->
<!--                          </div>-->
<!--                          <div class="skandal-tab skandal-tab-divider"></div>-->
<!--                          <div class="skandal-tab skandal-tab-title">-->
<!--                             <a data-toggle="tab" href="#powermap">SKANDAL POWERMAP</a>-->
<!--                          </div>-->
<!---->
<!--                       </div>-->

                    </div>

                    <!-- tab content -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="detil">
                            <div id="myCarousel" data-interval="3000" data-pause="hover" class="carousel-head carousel slide">

                                <!-- Carousel items -->
                                <div class="carousel-inner">

                                    <?php if($headx_photos != null){ ?>
                                    <div class="active item skandal-carousel-image">
                                        <img src="<?=$headx_photos;?>">
                                    </div>
                                    <?php } ?>

                                    <?php
                                        $_x_active = '';
                                        $k= ($headx_photos != null) ? 1 : 0;
                                        foreach($skandal['photos'] as $kes => $item){

                                            if(isset($item['type']))
                                            {
                                                if($item['type'] == 1)
                                                {
                                                    continue;

                                                } else {
                                                    if($k == 0 && $headx_photos == null)
                                                    {
                                                        $_x_active = 'active';
                                                    } else {
                                                        $_x_active = '';
                                                    }
                                                }
                                            } else {
                                                if($k == 0 && $headx_photos == null) { $_x_active = 'active'; } else { $_x_active = ''; }
                                            }
                                    ?>
                                    <div class="<?php echo $_x_active; ?> item skandal-carousel-image">
                                        <?php
                                       // echo $item['original_url'];
                                        $img_uri_car = $item['large_url'];
                                        if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){

                                            $img_uri_car = $item['original_url'];
                                        }
                                        ?>
                                        <img src="<?=$img_uri_car;?>">
                                    </div>
                                    <?php $k++; } ?>

                                </div>
                                <div class="score-place score-place-overlay score" data-id="<?php echo $skandal['content_id']; ?>" ></div>
                                <div class="row-fluid skandal-carousel-control-container" style="">
                                    <!-- Carousel nav -->
<!--
                                    <div class="span1">
                                        <a class="skandal-carousel-control pull-left left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                                    </div>
-->
                                    <div class="skandal-carousel-control-thumbs">
                                        <!-- Carousel thumb -->
                                        <ol class="skandal-carousel-indicators">

                                            <?php if($headx_photos != null){ ?>
                                            <li data-target="#myCarousel" data-slide-to="0" class="active">
                                                <img class="img-polaroid img-polaroid-scandal" src="<?php echo $headm_photos ;?>" >
                                            </li>
                                            <?php } ?>

                                            <?php $set_active='';
                                            $i= ($headx_photos != null) ? '1' : 0; foreach($skandal['photos'] as $kem => $item){
                                            if(isset($item['type']))
                                            {
                                                if($item['type'] == 1) {
                                                    continue;
                                                } elseif($i == 0 && $headx_photos == null) {
                                                    $set_active = 'active';
                                                }
                                            } else {
                                                if($i == 0 && $headx_photos == null) {
                                                    $set_active = 'active';
                                                } else { $set_active = '';}
                                            }
                                            ?>
                                                <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $set_active; ?>">
                                                    <img class="img-polaroid img-polaroid-scandal" src="<?php echo $item['thumb_url'] ;?>">
                                                </li>
                                                <?php $i++; } ?>

                                            <?php

                                                   ?>
                                        </ol>
                                    </div>
<!--
                                    <div class="span1">
                                        <a class="skandal-carousel-control pull-right right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                                    </div>
-->
                                </div>
                                <div class="skandal-detail-under-slider">
                                <hr class="hr-white">
                                <span><strong>&nbsp; Politisi Terkait</strong></span>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <?php
                                        if (count($skandal['players']) > 0)
                                        {
                                            echo '<ul class="ul-img-hr">';
                                            foreach ($skandal['players'] as $key => $pro)

                                            {
                                                ?>
                                                <?php
                                                if( $pro['profile_icon_url'] != 'None'){
                                                    $img_politik = $pro['profile_icon_url'];
                                                    if(!file_get_contents($img_politik)){
                                                        $img_politik = base_url().'assets/images/icon/no-image-politisi.png';
                                                    }
                                                } else {
                                                    $img_politik = base_url().'assets/images/icon/no-image-politisi.png';
                                                }
                                                ?>
                                                <li class="">
                                                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                                        <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="" src="<?php echo $img_politik; ?>">
                                                    </a>
                                                </li>
                                                <?php
                                            }
                                            echo '</ul>';
                                        } else{
                                            ?>
                                            <span>Tidak ada politisi terkait</span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <hr class="hr-white">
                                <div class="row-fluid">
                                    <div class="span2 span2-pd-left">
                                        <span><strong>Status</strong></span>
                                    </div>
                                    <div class="span10 span10-pd-right">
                                        <span class=""><?php echo ucwords($status); ?></span>
                                    </div>
                                </div>
                                <hr class="hr-white">
                                <div class="row-fluid">
                                    <div class="span2 span2-pd-left">
                                        <span><strong>Kejadian</strong></span>
                                    </div>
                                    <div class="span10 span10-pd-right">
                                        <span class=""><?php echo mdate('%d %M %Y', strtotime($skandal['date'])); ?></span>
                                    </div>
                                </div>
                                <hr class="hr-white">
                                <div class="row-fluid">
                                    <div class="span2 span2-pd-left">
                                        <span class=""><strong>Dampak</strong></span>
                                    </div>
                                    <div class="span10 span10-pd-right">
                                        <span class=""><?php echo $dampak; ?></span>
                                    </div>

                                </div>
                            </div>
                            <br>
                            <div class="row-fluid">
                                <div class="span12 skandal-detail-desc">
                                    <p>
                                    <?php echo nl2br($skandal['description']); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!-- ###### KRONOLOGI ###### -->
                        <div class="tab-pane" id="kronologis">
                            <div class="skandal-kronologi skandal-slim">
<!--                                <div class="div-line-small"></div>-->
                                <?php $dt_sebelum = ''; $kronologi_id = ''; ?>
                                <?php foreach($skandal['kronologi'] as $indeks => $krono){ ?>
                                <?php
                                if($dt_sebelum != $krono['date']){
                                    $kronologi_id = $krono['scandal_id'];
                                }
                                ?>
                                <?php if($dt_sebelum == $krono['date']){ ?>
                                    <!--                                <div id="" class="--><?php //echo $kronologi_id; ?><!----><?php //echo ($dt_sebelum == $krono['date']) ? ' collapse' : '';?><!--">-->
                                    <?php } ?>
                                <div class="row-fluid ">
                                    <div class="span3">
                                        <?php
                                        if($dt_sebelum != $krono['date']){

                                            ?>
                                            <h4><?php echo mdate('%d %M %Y', strtotime($krono['date'])); ?> </h4>
                                            <?php } ?>
                                    </div>
                                    <div class="span9">
                                        <?php //if($dt_sebelum != $krono['date']){ ?>
                                        <i class="icon-plus pull-right mycolaps" data-toggle="collapse" data-target="#<?php echo $krono['scandal_id']; ?>"></i>
                                        <?php //} ?>
                                        <div class="">
                                            <p><strong><?php echo $krono['title']; ?></strong></p>
                                            <p id="<?php echo $krono['scandal_id']; ?>" class="collapse"><?php echo nl2br($krono['short_desc']); ?></p>
                                            <div class="div-line-small"></div>
                                        </div>


                                    </div>

                                </div>
                                <?php if($dt_sebelum == $krono['date']){ ?>
                                    <!--                                </div>-->
                                    <?php } ?>
                                <?php $dt_sebelum = $krono['date']; ?>
                                <?php } ?>
                            </div>
                        </div>

                        <!-- ###### PLAYERS ###### -->
                        <div class="tab-pane" id="players">
                            <div class='skandal-players '> <!-- skandal-slim -->

                                <?php if (count($skandal['players']) > 0){ ?>
                                <?php foreach ($skandal['players'] as $key => $pro) { ?>
                                    <?php if($key != 0){ ?>
                                    <div class="div-line-small"></div>
                                    <?php } ?>
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <a class="" href="<?php echo base_url().'aktor/profile/'.$pro['page_id']; ?>"><h3><?php echo $pro['page_name']; ?></h3></a>
                                            <p><?php echo nl2br($pro['description']); ?></p>
                                        </div>
                                        <div class="span3 skandal-player-detail">
                                            <?php
                                                if( $pro['profile_icon_url'] != 'None'){
                                                $img_politik = $pro['profile_thumb_url'];
                                                } else {
                                                $img_politik = base_url().'assets/images/badge/no-image-politisi.png';
                                                }
                                            ?>
                                            <img class="thumbnail" alt="<?php echo $pro['page_name']; ?>" src="<?php echo $img_politik; ?>">
                                        </div>

                                    </div>

                                    <?php } ?>

                                <?php }else{ ?>
                                <p class="alert alert-info">Data belum tersedia</p>
                                <?php } ?>
                            </div>
                        </div>

                        <!-- ###### aliran dana ###### -->
                        <div class="tab-pane" id="aliran_dana">
                            <div class='skandal-relations skandal-slim'>

                                <?php if (count($skandal['relations']) > 0){ ?>
                                <?php foreach ($skandal['relations'] as $key => $pro) { ?>
                                    <div class="div-line-small"></div>
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <a class="" href="<?php echo $pro['profile_source_url']; ?>"><h3><?php echo $pro['page_name_source']; ?></h3></a>
                                            <p><strong><?php echo $pro['keterangan'] . ' : ' .$pro['relasi'] . ' kepada' ; ?></strong></p>
                                            <a class="" href="<?php echo $pro['profile_target_url']; ?>"><h3><?php echo $pro['page_name_target']; ?></h3></a>
                                            <p><?php echo nl2br($pro['deskripsi']); ?></p>
                                        </div>
                                        <div class="span3 skandal-player-detail">
                                            <img title="<?php echo $pro['page_name_source']; ?>" alt="<?php echo $pro['page_name_source']; ?>" src="<?php echo $pro['profile_thumb_url_source']; ?>">
                                            <a class="" href="<?php echo $pro['profile_source_url']; ?>"><?php echo $pro['page_name_source']; ?></a>
                                        </div>
                                        <div class="span3 skandal-player-detail">
                                            <img title="<?php echo $pro['page_name_target']; ?>" alt="<?php echo $pro['page_name_target']; ?>" src="<?php echo $pro['profile_thumb_url_target']; ?>">
                                            <a class="" href="<?php echo $pro['profile_target_url']; ?>"><?php echo $pro['page_name_target']; ?></a>
                                        </div>

                                    </div>

                                    <?php } ?>

                                <?php }else{ ?>
                                <p class="alert alert-info">Data belum tersedia</p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="powermap">
                            <img src="<?=base_url();?>assets/images/powermap-under-b.gif">
                        </div>
                    </div>

                </div>
                <!-- KOMENTAR -->
                <div class="div-line"></div>
                <div class="row-fluid">

                    <h5>BERI KOMENTAR</h5>
                    <div class="row-fluid">
                        <div class="comment-side-left">
                            <div style="background:
                                    url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                            </div>
                        </div>

                        <div class="comment-side-right">
                            <div class="row-fluid">
                                <input id='comment_type' data-id="<?=$skandal['content_id'];?>" type="text" name="comment" class="media-input" >
                            </div>
                            <div class="row-fluid">
                                <div class="span12 text-right">
                                    <?php if(!is_array($this->member)){?>
    <span>Login untuk komentar</span>&nbsp;
    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
    <?php }elseif(is_array($this->member)){?>
                                    <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="comment" data-page="1" data-id="<?=$skandal['content_id'];?>" class="row-fluid comment-container">

                </div>
                <div id="load_more_place" class="row-fluid komu-follow-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a data-page="1" data-id="<?=$skandal['content_id'];?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>
                <div id="div_line_bottom" class="div-line"></div>
                <!-- END -->
            </div>
            <!-- TERKAIT -->
            <div class="span4" style="margin-top:5px;">
                <div class="home-title-section" style="background-color: #bbbbbb;;">
                    <span class="hitam" style="margin-left: 4px !important"><a href="#">SKANDAL LAINNYA</a></span>
                </div>
                <br>
                <?php foreach($skandal_terkait as $num=>$row){ ?>
                <?php
                    $dt['val'] = $row;
                    $dt['val']['tipe'] = 'small';
                ?>
                <div>
                <?php $this->load->view('template/skandal/tpl_skandal_index_terkait', $dt); ?>
                </div>
                <br>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>


<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>


            <!-- TERKINI -->
            <?php echo $skandal_mini; ?>


            <!-- HOT ISSUE -->
            <?php //echo $issue; ?>
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>
    <div class="container-bawah">

        <div class="container">
            <div class="sub-header-container">
                <div class="row-fluid">

                    <!-- EKONOMI -->
                    <?php echo $ekonomi; ?>


                    <!-- HUKUM -->
                    <?php echo $hukum; ?>


                    <!-- PARLEMEN -->
                    <?php echo $parlemen; ?>

                </div>
            </div>
        </div>
        <br>

        <div class="container">
            <div class="sub-header-container">
                <div class="row-fluid">

                    <!-- EKONOMI -->
                    <?php echo $nasional; ?>


                    <!-- HUKUM -->
                    <?php echo $daerah; ?>


                    <!-- PARLEMEN -->
                    <?php echo $internasional; ?>

                </div>
            </div>
        </div>
        <br>
    </div>


<script>
    $(function () {
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });

        $('.mycolaps').on('click', function(e){
            e.preventDefault();
            var dt_target = $(this).data('target');
            var cl = $(dt_target).attr('class');
            if(cl == 'collapse'){
                $(this).attr('class', 'icon-minus pull-right mycolaps collapsed');
            }else{
                $(this).attr('class', 'icon-plus pull-right mycolaps collapsed');
            }
        });

    })

    $(document).ready(function() {
      /*
      $('.carousel-control').live('click', function(e) {
          $(this).siblings('.carousel-inner').css( "overflow", "hidden" );
      });
      */

      $('.carousel-head').carousel({interval:false})

    });
</script>

<?php $this->load->view('template/modal/report_spam_modal'); ?>