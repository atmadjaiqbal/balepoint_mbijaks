<style>
.boxed{
    border: solid 1px #c4c4c4;
    padding: 3px 3px 3px 3px;
}

.socmedicon{
    width: 22px;
    height: 22px;
    margin-right: 3px;
}
</style>
<h4 class="kanal-title kanal-title-red">SKANDAL</h4>
<?php
//echo '<pre>';print_r($skandal);echo '</pre>';
$skandal_url = base_url() . 'scandal/index/'.$skandal['scandal_id'].'-'.urltitle($skandal['title']);
$skandal_photos = (count($skandal['photos']) > 0) ? $skandal['photos'][0]['large_url'] : base_url('assets/images/img-default-scandal.png');

$skandal_photos = base_url('assets/images/img-default-scandal.jpg');
$headx_photos = base_url('public/upload/image/skandal/large/'.$skandal['photos'][0]['attachment_title']);

//$headx_photos = null; // (count($val['photos']) > 0) ? $val['photos'][0]['large_url'] : '';
//$headm_photos = null; //(count($val['photos']) > 0) ? $val['photos'][0]['thumb_url'] : '';

//foreach($skandal['photos'] as $kes => $item){
//    if(key_exists('type', $item)){
//        if(intval($item['type']) == 1){
//            $headx_photos = $item['large_url'];
//            if( file_exists('public/upload/image/skandal/'.$item['attachment_title'])){
//                $headx_photos = $item['original_url'];
//            }
//
//            $headm_photos = $item['thumb_url'];
//            break;
//        }
//    }
//}

$status = ucwords($skandal['status']);
$dampak = $skandal['uang'];

?>

<div class="row">
	<div class="col-xs-12">
		<h4><?php echo $skandal['title']; ?></h4>
		<img class="img-media-list" src="<?php echo $headx_photos;?>" alt="<?php echo $skandal['title']; ?>" title="<?php echo $skandal['title']; ?>">
		<div class="score-place score-place-overlay score" data-id="<?php echo $skandal['content_id']; ?>" ></div>
		<p>Politisi Terkait : </p>
		<?php
            if (count($skandal['players']) > 0)
            {
                echo '<ul class="ul-img-hr">';
                foreach ($skandal['players'] as $key => $pro)
                {
                    ?>
                    <?php
                    /*
                    if( $pro['profile_icon_url'] != 'None'){
                        $img_politik = $pro['profile_icon_url'];
                        if(!file_get_contents($img_politik)){
                            $img_politik = base_url().'assets/images/icon/no-image-politisi.png';
                        }
                    } else {
                        if(!empty($pro['page_id'])){
                            $img_politik = base_url('public/upload/image/politisi/'.$pro['page_id'].'/thumb/'.$pro['attachment_title']);
                        }else{
                            $img_politik = base_url().'assets/images/icon/no-image-politisi.png';
                        }
                    }
                    */
                    $img_politik = base_url('public/upload/image/politisi/'.$pro['page_id'].'/thumb/'.$pro['attachment_title']);

                    ?>
                    <li class="">
                        <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                            <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="" src="<?php echo $img_politik; ?>">
						</a>
                    </li>
                    <?php
                }
                echo '</ul>';
            } else{
                ?>
                <p>Tidak ada politisi terkait</p>
                <?php
            }
            ?>
         <p class="clearfix"></p>   
		<span><strong>Status</strong> : <?php echo ucwords($status); ?>, </span>
		<span><strong>Kejadian</strong> : <?php echo mdate('%d %M %Y', strtotime($skandal['updated_date'])); ?> </span>
		<p><strong>Dampak</strong> : <?php echo $dampak; ?></p>
	</div>
	<div class="col-xs-12">
		<div class="panel-group" id="accordion">
			
			<div class="panel panel-default">
			    <div class="panel-heading">
			      <h6 class="panel-title panel-title-xs">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseDesc">
			          	DESKRIPSI
			        </a>
			      </h6>
			    </div>
			    <div id="collapseDesc" class="panel-collapse collapse ">
			      <div class="panel-body panel-body-xs">
			        	<p><?php echo nl2br($skandal['description']); ?></p>
			      </div>
			    </div>
		    </div>

		    <div class="panel panel-default">
			    <div class="panel-heading">
			      <h6 class="panel-title panel-title-xs">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseKro">
			          	KRONOLOGIS
			        </a>
			      </h6>
			    </div>
			    <div id="collapseKro" class="panel-collapse collapse ">
			      <div class="panel-body panel-body-xs">
			        	<?php $dt_sebelum = ''; $kronologi_id = ''; ?>
                        <?php foreach($skandal['kronologi'] as $indeks => $krono){ ?>
                        <?php
                        if($dt_sebelum != $krono['date']){
                            $kronologi_id = $krono['scandal_id'];
                        }
                        ?>
                        <?php if($dt_sebelum == $krono['date']){ ?>
                            <!--                                <div id="" class="--><?php //echo $kronologi_id; ?><!----><?php //echo ($dt_sebelum == $krono['date']) ? ' collapse' : '';?><!--">-->
                            <?php } ?>
                        
                                <?php
                                if($dt_sebelum != $krono['date']){

                                    ?>
                                    <h5><?php echo mdate('%d %M %Y', strtotime($krono['date'])); ?> </h5>
                                    <?php } ?>
                            
                                <?php //if($dt_sebelum != $krono['date']){ ?>
                                
                                <?php //} ?>
                                
                                <p><strong><?php echo $krono['title']; ?></strong></p>
                                <p><?php echo nl2br($krono['short_desc']); ?></p>
                                <hr class="line-mini">
                                
                        <?php if($dt_sebelum == $krono['date']){ ?>
                            <!--                                </div>-->
                            <?php } ?>
                        <?php $dt_sebelum = $krono['date']; ?>
                        <?php } ?>
			      </div>
			    </div>
		    </div>

		    <div class="panel panel-default">
			    <div class="panel-heading">
			      <h6 class="panel-title panel-title-xs">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseAkt">
			          	PARA PEMAIN
			        </a>
			      </h6>
			    </div>
			    <div id="collapseAkt" class="panel-collapse collapse">
			      <div class="panel-body panel-body-xs">
			        	<?php if (count($skandal['players']) > 0){ ?>
                        <?php foreach ($skandal['players'] as $key => $pro) { ?>
                            <?php if($key != 0){ ?>
                            <hr class="line-mini">
                            <?php } ?>
                            <a class="" href="<?php echo base_url().'aktor/profile/'.$pro['page_id']; ?>"><h5><?php echo $pro['page_name']; ?></h5></a>
                            <p><?php echo nl2br($pro['description']); ?></p>
                                
                            <?php } ?>

                        <?php }else{ ?>
                        <p class="alert alert-info">Data belum tersedia</p>
                        <?php } ?>
			      </div>
			    </div>
		    </div>

		    <div class="panel panel-default">
			    <div class="panel-heading">
			      <h6 class="panel-title panel-title-xs">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseKon">
			          	KONEKSI DAN ALIRAN DANA
			        </a>
			      </h6>
			    </div>
			    <div id="collapseKon" class="panel-collapse collapse">
			      <div class="panel-body panel-body-xs">
			        	<?php if (count($skandal['relations']) > 0){ ?>
                        <?php foreach ($skandal['relations'] as $key => $pro) { ?>
                            
                                <a class="" href="<?php echo $pro['profile_source_url']; ?>"><h3><?php echo $pro['page_name_source']; ?></h3></a>
                                <p><strong><?php echo $pro['keterangan'] . ' : ' .$pro['relasi'] . ' kepada' ; ?></strong></p>
                                <a class="" href="<?php echo $pro['profile_target_url']; ?>"><h3><?php echo $pro['page_name_target']; ?></h3></a>
                                <p><?php echo nl2br($pro['deskripsi']); ?></p>
                                
                            	<hr class="line-mini">

                            <?php } ?>

                        <?php }else{ ?>
                        <p class="alert alert-info">Data belum tersedia</p>
                        <?php } ?>
			      </div>
			    </div>
		    </div>

		    <div class="panel panel-default">
			    <div class="panel-heading">
			      <h6 class="panel-title panel-title-xs">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePw">
			          	SKANDAL POWERMAP
			        </a>
			      </h6>
			    </div>
			    <div id="collapsePw" class="panel-collapse collapse">
			      <div class="panel-body panel-body-xs">
			        	<img class="img-media-list" src="<?=base_url();?>assets/images/powermap-under-b.gif">
			      </div>
			    </div>
		    </div>



		</div>
	</div>
</div>
<hr class="line-mini"/>
<!-- KOMENTAR -->
<div class="div-line"></div>


<h5>BERI KOMENTAR</h5>
<div class="row-fluid">
<!--     <div class="comment-side-left">
        <div style="background:url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

        </div>
    </div> -->

    <div class="comment-side-right">
        <div class="row-fluid">
            <!-- input id='comment_type' data-id="<?=$news['content_id'];?>" type="text" name="comment" class="media-input" -->
            <textarea id='comment_type' data-id="<?php echo $skandal['content_id']; ?>" name="comment" class="media-input" rows="4"></textarea>
        </div>
        <div class="row-fluid">
            <div class="span12 text-left">
                <?php if(!is_array($this->member)){?>
                <span>Login untuk komentar</span>&nbsp;
                <a href="<?php echo base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
                <a href="<?php echo base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
                <?php }elseif(is_array($this->member)){?>
                <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>

<!-- komentar -->
<div style="clear: both;"></div>
<div>&nbsp;</div>
<hr class="line-mini"/>
<div id="comment" data-page="1" data-id="<?php echo $skandal['content_id']; ?>" class="row-fluid comment-container"></div>
<div id="load_more_place" class="row-fluid komu-follow-list komu-wall-list-bg">
    <div class="span12 text-center">
        <a data-page="1" data-id="<?php echo $skandal['content_id']; ?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
    </div>
</div>
<div id="div_line_bottom" class="div-line"></div>

<?php echo $ads_banner[0]; ?>

<h4 class="kanal-title kanal-title-red">SKANDAL LAINNYA</h4>
<?php 
	$sk_ls['scandal_more'] = $skandal_terkait;
	$this->load->view('scandal/mobile/scandal_list', $sk_ls);
?>



