<script type="text/javascript">
    $(document).ready(function() {

        var id = '#dialog';

        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();

        //Set heigth and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});

        //transition effect
        $('#mask').fadeIn(1000);
        $('#mask').fadeTo("slow",0.8);

        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();

        //Set the popup window to center
        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);

        //transition effect
        $(id).fadeIn(2000);

        //if close button is clicked
        $('.window .close').click(function (e) {
            //Cancel the link behavior
            e.preventDefault();

            $('#mask').hide();
            $('.window').hide();
        });

        //if mask is clicked
        $('#mask').click(function () {
            $(this).hide();
            $('.window').hide();
        });

    });

</script>

<style type="text/css">
    #mask {position:absolute;left:0;top:0;z-index:9000;background-color:#000;display:none;}
    #boxes .window {position:absolute;left:0;top:0;width:960px;height:700px;z-index:9999;padding:5px;}
    #boxes #dialog {width:960px;height:720px;padding:5px;background-color:#ffffff;}
    #boxes .close-overlay {color: #000000;font-family:helvetica, "Times New Roman", arial;position: absolute;top:0px;}
</style>


<div id="boxes">
    <div style="top: -20px; left: 172px;" id="dialog" class="window">
        <img src="<?php echo base_url('assets/images/launch-bijak-fullpage.jpg'); ?>" style="background: rgba(0, 0, 0, 0);" >
    </div>
</div>
