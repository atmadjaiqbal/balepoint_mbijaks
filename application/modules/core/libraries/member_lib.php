<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_Lib {

	private $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->db = $this->CI->load->database('slave', true);
	}
	
  public function GetUserAccount($where=null, $sortby=null, $limit=null)
	{
    $sql = "SELECT * FROM tusr_account ";
    $sql .= $where;
    $sql .= $sortby;
    $sql .= $limit;
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function GetUserAccountByAccountID($accountID)
	{
		$where = "WHERE account_id = '".$accountID."' ";
		$limit = "LIMIT 1 ";
		return $this->GetUserAccount($where, null, $limit);
	}
	
  public function GetFriendRequest($accountID)
  {
  	$sql = "SELECT * FROM tusr_friend_request WHERE (to_account_id = '".$accountID."' AND flag = 0) ";
  	$query = $this->db->query($sql);
  	return $query->result_array();
  }	
  	
	public function GetFriendRequestFrom($accountID)
	{
		$sql = "SELECT * FROM tusr_friend_request WHERE (account_id = '".$accountID."' AND flag = 0) ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
		
	public function GetUserDisplayName($accountID)
	{
		$sql = "select display_name from tusr_account where account_id = '$accountID'";
		$query = $this->db->query($sql);				
		return $query->num_rows() > 0? $query->row()->display_name : NULL;
	}
	
	public function GetUserCurrentCity($accountID)
	{
		$sql = "SELECT current_city FROM tusr_account WHERE account_id = '".$accountID."' LIMIT 1";
		$query = $this->db->query($sql);
		$row = $query->row();
		return $row->current_city;
	}
		
	public function GetMemberByAccountID($accountID)
	{
		$sql = "Select ta.*, p.profile_content_id, p.page_id, t.attachment_title from tusr_account ta left join
				tobject_page p on p.page_id = ta.user_id left join tcontent_attachment t on t.content_id = p.profile_content_id
				where ta.account_id = '".$accountID."' and ta.account_status = 1 ";
    $query = $this->db->query($sql);
    return $query->row_array();
	}

	public function GetMemberFollowsCountRev($accountID, $pageID) 
	{
    $sql = "
            SELECT COUNT(*) AS count
            FROM
            (
            SELECT
            follow_list.page_id
            FROM
            (
            	(
            	SELECT
            	tuf.page_id
            	FROM
            	tusr_follow tuf 
            	LEFT JOIN tusr_account tua ON tua.user_id = tuf.page_id
            	WHERE tuf.account_id = '".$accountID."' AND tua.account_status = 1 and tuf.flag = 1
            	)
            
            	UNION ALL
            	(
            		SELECT
            		tua.user_id as page_id
            		FROM
            		tusr_follow tuf 
            		LEFT JOIN tusr_account tua ON tua.account_id = tuf.account_id
            		WHERE tuf.page_id = '".$pageID."' AND tua.account_status = 1 and tuf.flag = 1
            	)
            ) AS follow_list
            
            GROUP BY follow_list.page_id
            ) as x
            ";
     $query = $this->db->query($sql);    
	   return $query->result_array();
	}

	public function GetUserAffiliasiText($userID) 
	{
		if(! empty($userID)) {
			$rs = $this->GetUserAffiliasi($userID);
			$str = array();
			foreach($rs as $row) {
				if($row['page_name'] != $row['page_id']) {
					$str[] = "<a href='" . base_url() . "politisi/index/" . $row['page_id'] . "' title='" . $row['page_name'] . "'>" . $row['page_name'] . "</a>";
				} else {
					$str[] = "<a title='" . $row['page_name'] . "'>" . $row['page_name'] . "</a>";
				}
			}
			return implode(", ", $str);
		} else {
			return "";
		}
	}

	public function GetUserAffiliasi($userID) 
	{
		if(!empty($userID)) {
			// TO GET LIST OF POLITISI & PARTAI FROM AFFILIASI USER
			$sql = "SELECT politic_id as page_id FROM `tuser_affiliasi` WHERE user_id = '".$userID."' ";
			$query = $this->db->query($sql);
			$rsAffi = $query->result_array();
			foreach($rsAffi as &$row) 
			{
				$sql = "SELECT page_name, profile_content_id FROM `tobject_page` WHERE page_id = '".$userID."' ";
				$query = $this->db->query($sql);
				$rs = $query->result_array();
				if(!empty($rs)) 
				{
					$row['page_name'] = $rs[0]['page_name'];
					$row['profile_content_id'] = $rs[0]['profile_content_id'];
				} else {
					$row['page_name'] = $row['page_id'];
					$row['profile_content_id'] = "";
				}
			}
			return $rsAffi;
		} else {
			return array();
		}
	}

	public function isAFriend($accountID, $guestID)
	{
		$sql = "Select friend_id from tusr_friend 
		        where (account_id = '".$accountID."' and friend_account_id = '".$guestID."') or 
		        ((account_id = '".$guestID."' and friend_account_id = '".$accountID."')) ";

		$query = $this->db->query($sql);
		return $query->num_rows() > 0;
	}

	public function isFolowing($accountID, $guestID)
	{
		$sql = "select t1.page_id from tusr_follow t1 join tobject_page t2 on t1.page_id = t2.page_id
				    where t1.account_id = '".$accountID."' and t2.entry_by = '".$guestID."' ";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0;
	}

	public function isFriendRequested($accountID, $guestID)
	{
		$sql = "select account_id from tusr_friend_request
				    where (account_id = '".$accountID."' and to_account_id = '".$guestID."' and flag = 0)";
		$query = $this->db->query($sql);
		return $query->num_rows() > 0;
	}

	public function GetMemberFollowAnFollowerList($accountID, $pageID) 
	{
		$sql = "SELECT
            follow_list.type,
            follow_list.user_type,
            follow_list.page_id,
            follow_list.account_id,
            IFNULL(top.page_name, follow_list.display_name) AS page_name ,
            tca.attachment_title
            FROM
            (
            	(
            	SELECT
            	'follow' AS type,
            	IF(IFNULL(tua.account_type,1) = 1, 'politisi', 'user') AS user_type,
            	tuf.page_id,
            	tua.account_id,
            	tua.display_name
            	FROM
            	tusr_follow tuf 
            	LEFT JOIN tusr_account tua ON tua.user_id = tuf.page_id
            	WHERE tuf.account_id = '".$accountID."' AND tua.account_status = 1
            	)
            
            	UNION ALL
            	(
            		SELECT
            		'follower' AS type,
            		IF(IFNULL(tua.account_type,1) = 1, 'politisi', 'user') AS user_type,
            		tua.user_id as page_id,
            		tuf.account_id,
            		tua.display_name
            		FROM
            		tusr_follow tuf 
            		LEFT JOIN tusr_account tua ON tua.account_id = tuf.account_id
            		WHERE tuf.page_id = '".$pageID."' AND tua.account_status = 1
            	)
            ) AS follow_list
            
            LEFT JOIN tobject_page top ON top.page_id = follow_list.page_id
            LEFT JOIN tcontent_attachment tca ON tca.content_id = top.profile_content_id
            
            ORDER BY follow_list.user_type, follow_list.type 
        
        ";
		$query = $this->db->query($sql);
	  return $query->result_array();
	}
	
	public function CountUserLike($accountID)
	{
        $sql = "SELECT COUNT(*) AS 'like_account'
            FROM tcontent c INNER JOIN tcontent_like cl ON c.`content_id` = cl.content_id
            INNER JOIN tusr_account ua ON cl.`account_id` = ua.`account_id`
            WHERE c.account_id = '".$accountID."' AND cl.flag = '1' ";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function CountUserUnLike($accountID)
	{
        $sql = "SELECT COUNT(*) AS 'dislike_account'
            FROM tcontent c INNER JOIN tcontent_dislike cl ON c.`content_id` = cl.content_id
            INNER JOIN tusr_account ua ON cl.`account_id` = ua.`account_id`
            WHERE c.account_id = '".$accountID."' AND cl.flag = '1' ";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	
	public function CountUserComment($accountID)
	{
        $sql = "SELECT COUNT(*) AS 'comment_count' FROM tcontent c
            INNER JOIN tcontent_comment cc ON c.`content_id` = cc.`content_id`
            INNER JOIN tusr_account ua ON cc.`account_id` = ua.`account_id`
            WHERE c.account_id = '".$accountID."' AND cc.flag = '1' ";
        $query = $this->db->query($sql);
		return $query->row_array();

	}
	
	public function GetUserLikeMember($accountID, $offset=0, $limit=20)
	{		
    $sql = "SELECT c.*, ua.user_id AS 'user_userid', ua.profile_content_id AS 'user_profileid',
            ua.account_id AS 'user_accountid', ua.`display_name`, cl.entry_date AS 'date_like', ca.`attachment_title` AS 'guest_photo'
            FROM tcontent c INNER JOIN tcontent_like cl ON c.`content_id` = cl.content_id
            INNER JOIN tusr_account ua ON cl.`account_id` = ua.`account_id`
            INNER JOIN tobject_page p ON p.page_id = ua.user_id
            INNER JOIN tcontent_attachment ca ON p.`profile_content_id` = ca.`content_id`
            WHERE c.account_id = '".$accountID."' AND cl.flag = '1' 
            ORDER BY cl.entry_date DESC LIMIT ".$offset." , ".$limit;                        
    $query = $this->db->query($sql);
    return $query->result_array();
	}
	
	public function NewCountUserLikeMember($accountID)
	{
    $sql = "SELECT COUNT(*) AS 'total_like' 
            FROM tcontent c INNER JOIN tcontent_like cl ON c.`content_id` = cl.content_id
            INNER JOIN tusr_account ua ON cl.`account_id` = ua.`account_id`
            WHERE c.account_id = '".$accountID."' AND cl.flag = '1' ";
    $query = $this->db->query($sql);
    $resCount = $query->row_array();
    return $resCount['total_like'];				
	}

	public function GetUserDislikeMember($accountID, $offset=0, $limit=20)
	{
    $sql = "SELECT c.*, ua.user_id AS 'user_userid', ua.profile_content_id AS 'user_profileid', 
            ua.account_id AS 'user_accountid', ua.`display_name`, cl.entry_date AS 'date_like', ca.`attachment_title` AS 'guest_photo'
            FROM tcontent c INNER JOIN tcontent_dislike cl ON c.`content_id` = cl.content_id
            INNER JOIN tusr_account ua ON cl.`account_id` = ua.`account_id`
            INNER JOIN tobject_page p ON p.page_id = ua.user_id
            INNER JOIN tcontent_attachment ca ON p.`profile_content_id` = ca.`content_id`
            WHERE c.account_id = '".$accountID."' AND cl.flag = '1'
            ORDER BY cl.entry_date DESC LIMIT ".$offset." , ".$limit;                        
    $query = $this->db->query($sql);
    return $query->result_array();		
	}

	public function NewCountUserDislikeMember($accountID)
	{
    $sql = "SELECT COUNT(*) AS 'total_unlike' 
            FROM tcontent c INNER JOIN tcontent_dislike cl ON c.`content_id` = cl.content_id
            INNER JOIN tusr_account ua ON cl.`account_id` = ua.`account_id`
            WHERE c.account_id = '".$accountID."' AND cl.flag = '1' ";
    $query = $this->db->query($sql);
    $resCount = $query->row_array();
    return $resCount['total_unlike'];				
	}

  public function GetUserCommentMember($accountID, $offset=0, $limit=20)
  {
    $arr_comment = array();
  	$sql = "SELECT c.*, ua.`display_name`, ua.`account_id` AS 'user_accountid', ua.`user_id` AS 'user_userid', cc.`text_comment`,
  	        cc.`entry_date` AS 'comment_date' FROM tcontent c
            INNER JOIN tcontent_comment cc ON c.`content_id` = cc.`content_id`
            INNER JOIN tusr_account ua ON cc.`account_id` = ua.`account_id`
            WHERE c.account_id = '".$accountID."' AND cc.flag = '1'
            ORDER BY cc.entry_date DESC LIMIT ".$offset." , ".$limit;                        
    $query = $this->db->query($sql);
    $rsComment = $query->result_array();
    foreach($rsComment as $rwComment)
    {
        $sqlc = "SELECT ca.* FROM tcontent_attachment ca INNER JOIN tobject_page p ON ca.content_id = p.profile_content_id
                 WHERE p.page_id = '".$rwComment['user_userid']."' ";
        $queryc = $this->db->query($sqlc);
        $rsPhoto = $queryc->row_array();
        $arr_comment[] = array(
            'content_id' => $rwComment['content_id'], 'account_id' => $rwComment['account_id'], 'user_accountid' => $rwComment['user_accountid'],
            'content_group_type' => $rwComment['content_group_type'], 'group_content_id' => $rwComment['group_content_id'],
            'page_id' => $rwComment['page_id'], 'title' => $rwComment['title'], 'description' => $rwComment['description'],
            'location' => $rwComment['location'], 'content_type' => $rwComment['content_type'],
            'privacy' => $rwComment['privacy'], 'last_update_date' => $rwComment['last_update_date'],
            'entry_date' => $rwComment['entry_date'], 'status' => $rwComment['status'], 'count_comment' => $rwComment['count_comment'],
            'count_like' => $rwComment['count_like'], 'count_dislike' => $rwComment['count_dislike'],
            'display_name' => $rwComment['display_name'], 'user_userid' => $rwComment['user_userid'],
            'text_comment' => $rwComment['text_comment'], 'comment_date' => $rwComment['comment_date'],
            'row_photo' => $rsPhoto,

        );
    }
    return $arr_comment;

  }
  
  public function NewCountUserCommentMember($accountID)
  {
  	$sql = "SELECT COUNT(*) AS 'total_comment' FROM tcontent c
            INNER JOIN tcontent_comment cc ON c.`content_id` = cc.`content_id`
            INNER JOIN tusr_account ua ON cc.`account_id` = ua.`account_id`
            WHERE c.account_id = '".$accountID."' AND cc.flag = '1' "; 
    $query = $this->db->query($sql);
    $resCount = $query->row_array();
    return $resCount['total_comment'];
  }

  public function ViewUserCommentOnMember($contentID=null, $account_id=null )
  {
      $arrView = array();
      $sql = "SELECT ct.*, cgt.`content_group_name` AS 'content_group_name', ua.`display_name`, ua.`account_id` AS 'user_accountid',
             ua.`user_id` AS 'user_userid' FROM tcontent ct
             INNER JOIN tm_content_group_type cgt ON ct.`content_group_type` = cgt.`content_group_type`
             INNER JOIN tusr_account ua ON ct.`account_id` = ua.`account_id`
             WHERE ct.content_id = '".$contentID."' AND ct.account_id = '".$account_id."' ";
      $query = $this->db->query($sql);

      $data['content'] = $query->row_array();

      $sql = "SELECT ca.* FROM tcontent_attachment ca INNER JOIN tobject_page p ON ca.content_id = p.profile_content_id
              WHERE p.page_id = '".$data['content']['user_userid']."' ";
      $query = $this->db->query($sql);
      $data['content_photo'] = $query->row_array();

      $sql = "SELECT c.*, ua.`display_name`, ua.`account_id` AS 'user_accountid', ua.`user_id` AS 'user_userid', cc.`text_comment`,
  	         cc.`entry_date` AS 'comment_date' FROM tcontent c
  	         INNER JOIN tcontent_comment cc ON c.`content_id` = cc.`content_id`
             INNER JOIN tusr_account ua ON cc.`account_id` = ua.`account_id`
             WHERE c.account_id = '".$account_id."'
             AND c.content_id = '".$contentID."' AND cc.flag = '1' ORDER BY cc.entry_date DESC ";
      $query = $this->db->query($sql);
      $rsComment = $query->result_array();

      foreach($rsComment as $rwComment)
      {
          $sqlc = "SELECT ca.* FROM tcontent_attachment ca INNER JOIN tobject_page p ON ca.content_id = p.profile_content_id
                 WHERE p.page_id = '".$rwComment['user_userid']."' ";
          $queryc = $this->db->query($sqlc);
          $rsPhoto = $queryc->row_array();
          $data['arr_comment'][] = array(
              'content_id' => $rwComment['content_id'], 'account_id' => $rwComment['account_id'], 'user_accountid' => $rwComment['user_accountid'],
              'content_group_type' => $rwComment['content_group_type'], 'group_content_id' => $rwComment['group_content_id'],
              'page_id' => $rwComment['page_id'], 'title' => $rwComment['title'], 'description' => $rwComment['description'],
              'location' => $rwComment['location'], 'content_type' => $rwComment['content_type'],
              'privacy' => $rwComment['privacy'], 'last_update_date' => $rwComment['last_update_date'],
              'entry_date' => $rwComment['entry_date'], 'status' => $rwComment['status'], 'count_comment' => $rwComment['count_comment'],
              'count_like' => $rwComment['count_like'], 'count_dislike' => $rwComment['count_dislike'],
              'display_name' => $rwComment['display_name'], 'user_userid' => $rwComment['user_userid'],
              'text_comment' => $rwComment['text_comment'], 'comment_date' => $rwComment['comment_date'],
              'row_photo' => $rsPhoto,

          );
      }
      return $data;
  }

  public function GetContentGroupTypeName($GroupID=null)
  {
      $sql = "SELECT * FROM tm_content_group_type WHERE content_group_type = '". $GroupID ."' ";
      $query = $this->db->query($sql);
      return $query->row_array();
  }

	public function GetObjectInfoAddressByAccountId($accountID)
	{
		$sql = "SELECT * FROM tobject_info_address t1 
		        WHERE t1.page_id in (SELECT t2.page_id FROM tobject_page t2 WHERE t2.entry_by = '".$accountID."')";
		$query = $this->db->query($sql);
		return $query->row_array();
	}
			
	public function CountMemberGroup($accountID)
	{
		$sql = "SELECT count(*) AS 'total_group' FROM tcontent_group g
		        INNER JOIN tusr_account ua ON g.account_id = ua.account_id
		        WHERE g.account_id = '".$accountID."' ";
		$query = $this->db->query($sql);
		return $query->row_array();         
	}
	
	public function dropfriendlist($accountID)
	{
		$sql = "SELECT ua.*, uf.`account_id` FROM tusr_account ua 
		        INNER JOIN tusr_friend uf ON ua.`account_id` = uf.`friend_account_id`
		        WHERE uf.`account_id` = '".$accountID."' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function ShowNoticeLike($accountID, $isShow = 'n')
	{
		$sql = "UPDATE tusr_account SET show_notice_like = '".$isShow."' 
		        WHERE account_id = '".$accountID."' ";
		$query = $this->db->query($sql);        
	}
	
	public function ShowNoticeUnlike($accountID, $isShow = 'n')
	{
		$sql = "UPDATE tusr_account SET show_notice_unlike = '".$isShow."' 
		        WHERE account_id = '".$accountID."' ";
		$query = $this->db->query($sql);        
	}
	
	public function ShowNoticeComment($accountID, $isShow = 'n')
	{
		$sql = "UPDATE tusr_account SET show_notice_comment = '".$isShow."' 
		        WHERE account_id = '".$accountID."' ";
		$query = $this->db->query($sql);        
	}
	
	public function likehasread($accountID, $entrydate)
	{
		$sql = "UPDATE tcontent_like SET flag = '0' WHERE account_id = '".$accountID."' AND entry_date = '".$entrydate."' ";
		$query = $this->db->query($sql);		
	}
	
	public function unlikehasread($accountID, $entrydate)
	{
		$sql = "UPDATE tcontent_dislike SET flag = '0' WHERE account_id = '".$accountID."' AND entry_date = '".$entrydate."' ";
		$query = $this->db->query($sql);		
	}
	
	public function commenthasread($accountID, $entrydate)
	{
		$sql = "UPDATE tcontent_comment SET flag = '0' WHERE account_id = '".$accountID."' AND entry_date = '".$entrydate."' ";
        $query = $this->db->query($sql);
	}

    public function followhasread($accountID, $pageName)
    {
        $sql = "UPDATE tusr_follow SET flag = '0' WHERE account_id = '".$accountID."' ";
        $query = $this->db->query($sql);
    }
	

}

?>
