<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//CONSTANSE CACHE NAME
define('CACHE_HOME_HEADLINE', 		'HOME'.DIRECTORY_SEPARATOR.'HEADLINE');
define('CACHE_HOME_HOTPROFILE', 		'HOME'.DIRECTORY_SEPARATOR.'HOT-PROFILE');
define('CACHE_HOME_NEWS_LATEST', 	'HOME'.DIRECTORY_SEPARATOR.'NEWS-LATEST');
define('CACHE_HOME_NEWS_HOT', 		'HOME'.DIRECTORY_SEPARATOR.'NEWS-HOT');
define('CACHE_HOME_NEWS_HUKUM', 		'HOME'.DIRECTORY_SEPARATOR.'NEWS-HUKUM');
define('CACHE_HOME_NEWS_PARLEMENT', 'HOME'.DIRECTORY_SEPARATOR.'NEWS-PARLEMENT');
define('CACHE_HOME_NEWS_NATIONAL', 	'HOME'.DIRECTORY_SEPARATOR.'NEWS-NATIONAL');
define('CACHE_HOME_NEWS_DAERAH', 	'HOME'.DIRECTORY_SEPARATOR.'NEWS-DAERAH');
define('CACHE_HOME_NEWS_INTERNATIONAL', 	'HOME'.DIRECTORY_SEPARATOR.'NEWS-INTERNATIONAL');
define('CACHE_HOME_NEWS_RESES', 		'HOME'.DIRECTORY_SEPARATOR.'NEWS-RESES');
define('CACHE_HOME_NEWS_EKONOMI', 	'HOME'.DIRECTORY_SEPARATOR.'NEWS-EKONOMI');
define('CACHE_HOME_SURVEY', 			'HOME'.DIRECTORY_SEPARATOR.'SURVEY');
define('CACHE_HOME_SUKSESI', 			'HOME'.DIRECTORY_SEPARATOR.'SUKSESI');
define('CACHE_HOME_SCANDAL', 			'HOME'.DIRECTORY_SEPARATOR.'SCANDAL');
define('CACHE_HOME_COMMENT_LATEST', 'HOME'.DIRECTORY_SEPARATOR.'COMMENT-LATEST');
define('CACHE_HOME_BLOG_LATEST', 	'HOME'.DIRECTORY_SEPARATOR.'BLOG-LATEST');
define('CACHE_HOME_BLOG_FAVORIT', 	'HOME'.DIRECTORY_SEPARATOR.'BLOG-FAVORIT');

define('CACHE_SIDEBAR_KOMUNITAS_POLITISI', 	'SIDEBAR'.DIRECTORY_SEPARATOR.'KOMUNITAS-POLITISI');


class Cache_lib {

	protected  $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('cache');
	}

	function getHeadline()
	{
		//$return = $this->CI->cache->get(CACHE_HOME_HEADLINE);
		//if (!$return) $return = $this->setHeadline();
        $return = $this->setHeadline();
        return $return;
	}

	function setHeadline()
	{
		$this->CI->load->library('news/news_lib');
		$this->CI->load->library('politik/politik_lib');
		$this->CI->load->library('scandal/scandal_lib');

		$member = $this->CI->session->userdata('member');

		$data['account_id'] 	= (isset($member['account_id'])) ? $member['account_id'] : NULL;
		$data['headline'] 	= $this->CI->news_lib->getHeadline(3, 3);

		// headline hot PROFILE
		$headlineProfile = $this->CI->politik_lib->getHotPolitisi(1, 2); // LIMIT, PUBLISH ID		//_cache_900_original
		if (!empty($headlineProfile)) {
			$cm = $this->CI->load->module('ajax');
			foreach($headlineProfile as $key => $row)
			{
				$headlineProfile[$key]['count_page'] 	= $cm->count_page($row['page_id'], 2);
				$headlineProfile[$key]['scandal']  	= $this->CI->politik_lib->getScandalByPolitisi($row['page_id'], 2);
				$headlineProfile[$key]['content_type']  	= 'HOT_PROFILE';
			}
			$data['headline'][] =  $headlineProfile[0];

		}

		$headlineScandal = $this->CI->scandal_lib->getScandalList('headline', 0,1);
		if (!empty($headlineScandal)) {
			$headlineScandal[0]['content_type']  	= 'SCANDAL';
			$data['headline'][] =  $headlineScandal[0];
		}

        $data['ttlcomment'] = $cm->CountAllComment();
        $data['ttllike'] = $cm->CountAllLike();
        $data['ttluser'] = $cm->CountAllUser();
        $data['ttlscandal'] = $cm->CountAllScandal();
        $data['ttlsuksesi'] = $cm->CountAllSuksesi();
        $data['ttltokoh'] = $cm->CountAllTokoh();

        $return = $this->CI->load->view('home/carousel_slider', $data, true);
		$this->CI->cache->write($return, CACHE_HOME_HEADLINE);
		return $return;
	}


	function getNews($category, $catName='Berita Terakhir', $limit=4)
	{
		$cacheName = $this->getNewsCacheName($category);
		$return = $this->CI->cache->get($cacheName);
		if (!$return) $return = $this->setNews($category, $catName, $limit);
		return $return;
	}


	function setNews($category, $catName='Berita Terakhir', $limit=4)
	{
		$this->CI->load->library('news/news_lib');
		$this->CI->load->helper('imagefull');
		$this->CI->load->helper('text');

		$member 		= $this->CI->session->userdata('member');
		$cacheName 	= $this->getNewsCacheName($category);
		$offset 		= 0;
		$posts		= $this->CI->news_lib->newshomepage($category, $limit, $offset);
		$i=0;
		foreach($posts as $key => $post) {
			$content_id = $post['content_id'];
			$posts[$key]['politisi_terkait'] = $this->CI->bijak->getPolitisiTerkait($content_id, 'cid', $limit);
		}

        switch($category)
        {
            case '8' :
                $data['last_comunity']  = $this->GetLastUpdateComunity('#bd8c74', $posts[0]['topic_id']);
                break;

            case '18' :
                $data['last_comunity']  = $this->GetLastUpdateComunity('#fa82ff', $posts[0]['topic_id']);
                break;

            default :
                $data['last_comunity']  = $this->GetLastUpdateComunity('#ca8869', $posts[0]['topic_id']);
                break;
        }

		$data['posts'] 	= $posts;
		$data['category'] = $category;
		$data['catName'] 	= $catName;
		$data['account_id'] = (isset($member['account_id'])) ? $member['account_id'] : NULL;

		$return = $this->CI->load->view('home/news', $data, true);

		$this->CI->cache->write($return, $cacheName);
		return $return;
	}

    function getNewsTOC($category, $catName='Berita Terakhir', $limit=4, $catlabelname)
    {
        $cacheName = $this->getNewsCacheName($category);
        $return = $this->CI->cache->get($cacheName);
        if (!$return) $return = $this->setNewsTOC($category, $catName, $limit, $catlabelname);
        return $return;
    }

    function setNewsTOC($category, $catName='Berita Terakhir', $limit=2, $catlabelname)
    {
        $this->CI->load->library('news/news_lib');
        $this->CI->load->helper('imagefull');
        $this->CI->load->helper('text');

        $member 	= $this->CI->session->userdata('member');
        $cacheName 	= $this->getNewsCacheName($category);
        $offset 	= 0;
        $posts		= $this->CI->news_lib->newshomepage($category, $limit, $offset);

        $i=0;
        foreach($posts as $key => $post) {
            $content_id = $post['content_id'];
            $posts[$key]['politisi_terkait'] = $this->CI->bijak->getPolitisiTerkait($content_id, 'cid', $limit);
        }

        $data['posts'] 	= $posts;
        $data['category'] = $category;
        $data['categories_name'] = $catlabelname;
        $data['catName'] = $catName;
        $data['account_id'] = (isset($member['account_id'])) ? $member['account_id'] : NULL;

        $return = $this->CI->load->view('home/news_toc', $data, true);

        $this->CI->cache->write($return, $cacheName);
        return $return;
    }

    function getNewsCacheName($category)
	{
		$cacheName = CACHE_HOME_NEWS_LATEST;
		if ($category == '8') $cacheName = CACHE_HOME_NEWS_HOT;
		if ($category == '1') $cacheName = CACHE_HOME_NEWS_HUKUM;
		if ($category == '7') $cacheName = CACHE_HOME_NEWS_PARLEMENT;
		if ($category == '9') $cacheName = CACHE_HOME_NEWS_NATIONAL;
		if ($category == '17') $cacheName = CACHE_HOME_NEWS_DAERAH;
		if ($category == '19') $cacheName = CACHE_HOME_NEWS_INTERNATIONAL;
		if ($category == '18') $cacheName = CACHE_HOME_NEWS_RESES;
		if ($category == '4') $cacheName = CACHE_HOME_NEWS_EKONOMI;
		return $cacheName;
	}

	function getHotProfile()
	{
		$return = $this->CI->cache->get(CACHE_HOME_HOTPROFILE);
		if (!$return) $return = $this->setHotProfile();
		return $return;
	}

	function setHotProfile()
	{
		$this->CI->load->library('politik/politik_lib');
		$this->CI->load->helper('imagefull');
		$this->CI->load->helper('seourl');

		$data['hot_profile'] = $this->CI->politik_lib->getHotPolitisi(15, 1); // LIMIT, PUBLISH ID		//_cache_900_original
		$cm = $this->CI->load->module('ajax');
		foreach($data['hot_profile'] as $key => $row)
		{
			$data['hot_profile'][$key]['count_page'] 	= $cm->count_page($row['page_id'], 2);
			$data['hot_profile'][$key]['scandal']  	= $this->CI->politik_lib->getScandalByPolitisi($row['page_id'], 3);
		}

		$return = $this->CI->load->view('home/hot_profile_list', $data, true);
		$this->CI->cache->write($return, CACHE_HOME_HOTPROFILE);
		return $return;
	}


	function getSurvey()
	{
		$return 		= $this->CI->cache->get(CACHE_HOME_SURVEY);
		if (!$return) $return = $this->setSurvey();
		return $return;
	}

	function setSurvey()
	{
		$this->CI->load->library('survey/survey_lib');
		$this->CI->load->helper('seourl');
		$this->CI->load->helper('text');

		$member             = $this->CI->session->userdata('member');
        $data['memberlogin'] = $this->CI->session->userdata('member');
		$data['account_id'] = (isset($member['account_id'])) ? $member['account_id'] : NULL;
		$data['survey']     = $this->CI->survey_lib->getSurveyList('hotlist',0,1);
		$return             = $this->CI->load->view('home/survey', $data, true);
		$this->CI->cache->write($return, CACHE_HOME_SURVEY);

		return $return;
	}


	function getScandal()
	{
		$return 		= $this->CI->cache->get(CACHE_HOME_SCANDAL);
		if (!$return) $return = $this->setScandal();
		return $return;
	}

	function setScandal()
	{
		$this->CI->load->library('scandal/scandal_lib');
		$this->CI->load->helper('imagefull');
		$this->CI->load->helper('seourl');

		$member               = $this->CI->session->userdata('member');
		$data['account_id']   = (isset($member['account_id'])) ? $member['account_id'] : NULL;
		$data['scandal']      = $this->CI->scandal_lib->getScandalList('hotlist', 0,1);
		$data['scandal_more'] = $this->CI->scandal_lib->getScandalList('', 0,10);
        $data['last_comunity']  = $this->GetLastUpdateComunityScandal('#fe3d3d', $data['scandal'][0]['content_id']);

		$return               = $this->CI->load->view('home/skandal', $data, true);
		$this->CI->cache->write($return, CACHE_HOME_SCANDAL);

		return $return;
	}


	function getSuksesi()
	{
		$return 		= $this->CI->cache->get(CACHE_HOME_SUKSESI);
		if (!$return) $return = $this->setSuksesi();
		return $return;
	}

	function setSuksesi()
	{
		$this->CI->load->library('suksesi/suksesi_lib');
		$this->CI->load->helper('imagefull');
		$this->CI->load->helper('seourl');

		$member               = $this->CI->session->userdata('member');
		$data['account_id']   = (isset($member['account_id'])) ? $member['account_id'] : NULL;
		$data['suksesi']      = $this->CI->suksesi_lib->getSuksesiData(0,2, array('hotlist' => '1'));
		$data['suksesi_more'] = $this->CI->suksesi_lib->getSuksesiData(2,10, array('hotlist' => '1'));
		$return               = $this->CI->load->view('home/suksesi', $data, true);
		$this->CI->cache->write($return, CACHE_HOME_SUKSESI);

		return $return;
	}


	function getBlog($type='latest', $section='opini')
	{
		$cacheName = CACHE_HOME_BLOG_LATEST;
		//if ($type == 'favorit') $cacheName = CACHE_HOME_BLOG_FAVORIT;
		//$return = $this->CI->cache->get($cacheName);
		//if (!$return) $return = $this->setBlog($type, $section);
        $return = $this->setBlog($type, $section);
        return $return;
	}

	function setBlog($type='latest', $section)
	{
		$this->CI->load->model('Content_model');
		$this->CI->load->helper('imagefull');
		$this->CI->load->helper('text');

		$cacheName = CACHE_HOME_BLOG_LATEST;
		if ($type == 'favorit') $cacheName = CACHE_HOME_BLOG_FAVORIT;

		$member             = $this->CI->session->userdata('member');
		$data['account_id'] = (isset($member['account_id'])) ? $member['account_id'] : NULL;
		$data['blogs'] 		= $this->CI->Content_model->getBlogList($type, 45);
        $data['section']    = $section;
		$return        		= $this->CI->load->view('home/blog', $data, true);
		$this->CI->cache->write($return, $cacheName);

		return $return;
	}


	function getComment($type='latest')
	{
		//$return = $this->CI->cache->get(CACHE_HOME_COMMENT_LATEST);
		/*if (!$return) */
        $return = $this->setComment($type);
		return $return;
	}


	function setComment($type='latest')
	{
		$this->CI->load->model('Content_model');
		$this->CI->load->helper('imagefull');
		$this->CI->load->helper('text');

		$data['comments'] = $this->CI->Content_model->getCommentList($type, '100');

		$return = $this->CI->load->view('home/comment', $data, true);
		$this->CI->cache->write($return, CACHE_HOME_COMMENT_LATEST);

		return $return;
	}


	function getSidebarKomunitasPolitisi()
	{
		$return 		= $this->CI->cache->get(CACHE_SIDEBAR_KOMUNITAS_POLITISI);
		if (!$return) $return = $this->setSidebarKomunitasPolitisi();

		return $return;
	}

	function setSidebarKomunitasPolitisi()
	{
		$this->CI->load->library('template/template_lib');

		$return = $this->CI->template_lib->getSidebarKomunitasPolitisi();
		$this->CI->cache->write($return, CACHE_SIDEBAR_KOMUNITAS_POLITISI);

		return $return;
	}


	function clearCache($directory='')
	{
		return $this->CI->cache->delete_all($directory);
	}

    public function GetLastUpdateComunity($BackColor=null, $topicID)
    {
        $comunity = $this->CI->load->module('ajax');
        $data['rsSocial'] = $comunity->GetLastSocialInfoByTopicID($topicID);
        $data['BackColor'] = $BackColor;
        return $this->CI->load->view('home/home_comunity_update', $data, true);
    }

    public function GetLastUpdateComunityScandal($BackColor=null, $topicID)
    {
        $comunity = $this->CI->load->module('ajax');

        $data['rsSocial'] = $comunity->GetLastSocialInfoByContentID($topicID, '50');
        if(empty($data['rsSocial']))
        {
            $data['rsSocial'] = $comunity->GetLastSocialInfo();
        }
        $data['BackColor'] = $BackColor;
        $data['width'] = "330px;";
        return $this->CI->load->view('home/home_comunity_update', $data, true);
    }

    function getKomunitas()
    {
        $return 		= $this->CI->cache->get(CACHE_HOME_SUKSESI);
        if (!$return) $return = $this->setKomunitas();
        return $return;
    }

    function setKomunitas()
    {
        $this->CI->load->library('suksesi/suksesi_lib');
        $this->CI->load->helper('imagefull');
        $this->CI->load->helper('seourl');

        $member               = $this->CI->session->userdata('member');
        $data['account_id']   = (isset($member['account_id'])) ? $member['account_id'] : NULL;
        $data['suksesi']      = $this->CI->suksesi_lib->getSuksesiData(0,2, array('hotlist' => '1'));
        $data['suksesi_more'] = $this->CI->suksesi_lib->getSuksesiData(2,10, array('hotlist' => '1'));
        $return               = $this->CI->load->view('home/suksesi', $data, true);
        $this->CI->cache->write($return, CACHE_HOME_KOMUNITAS);

        return $return;
    }

}

/* End of file file.php */