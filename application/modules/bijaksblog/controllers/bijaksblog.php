<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bijaksblog extends Application {


    public function __construct()
    {
        parent::__construct();

        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);

        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->module('timeline/timeline');
        $this->load->module('news/news');

        $this->load->model('Content_model');
        $this->load->helper('imagefull');
        $this->load->helper('text');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
    }


    public function index()
    {
        $this->load->helper('imagefull');
        $data				 = $this->data;

        $data['scripts']  = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');

        $data['blogs'] = $this->displayblog();
        $data['terkini']  = $this->headline->terkini_list();
        $data['reses']    = $this->headline->reses_list();
        $data['headline'] = $this->headline->headline_list(); //$this->news(8, 'hot-issues');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 9, 'true');

        $html['html']['content']  = $this->load->view('bijaksblog/bijaksblog_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html, false);
    }


    public function displayblog()
    {
        $data = $this->Content_model->getBlogList('latest', 200);
        return $data;
    }

}

?>