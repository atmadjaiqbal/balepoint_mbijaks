<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aktor extends Application
{
    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->helper('m_date');

        $this->load->helper('text');
        $this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');        
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');

        $this->load->module('survey');
        $this->load->module('headline');

        $this->load->library('aktor/aktor_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->library('suksesi/suksesi_lib');
        
    }

    function _remap($method, $params=array())
    {
    	if($method == 'index'){
            redirect(base_url());
        }
        if(method_exists($this, $method))
        {
            $this->isBijaksProfile($params[0]);
            $this->$method($params);
        }else{

            $this->isBijaksProfile($method);
            $this->index($method);
        }

    }

    function isBijaksProfile($user_id)
    {

        $users = $this->redis_slave->get('profile:detail:'.$user_id);
        $user_array = json_decode($users, true);

        if(count($user_array) > 0)
        {
            $this->user = $user_array;
        }else{
            show_404();
        }
    }
    
    public function profile($id)
    {
//        $this->isLogin();
        $user_id = $id[0];
        
        $data['topic_last_activity'] = 'bijak';

        $this->isBijaksProfile($user_id);
        $user = $this->user;

       
        //check if follow
        $data['is_follow'] = false;
        if($id != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = array('account_id' => $this->member['account_id'], 'page_id' => $user['page_id']);
            $dt = $this->aktor_lib->is_follow($select, $where)->row_array();
            if($dt['total'] > 0) $data['is_follow'] = true;
        }

        $data['follow'] = $this->aktor_lib->get_follower($user['page_id'])->result_array();
        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }


        $where = array('page_id' => $user['page_id']);
        $info_address = $this->aktor_lib->get_info_address('*', $where);
        $data['address'] = array();
        if($info_address->num_rows() > 0){
            $data['address'] = $info_address->row_array(0);
        }

        
        $data['user'] = $user;
        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list = array();
        if($arr_profile['album_photo'])
        {
            foreach($arr_profile['album_photo'] as $rwPhoto)
            {
                $_photo_list = $rwPhoto['photo'];
            }
        }

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;

        $data['head_title'] = 'Profile ' . $user['page_name'];
        $data['polterkait'] = $user['page_name'];

        $html['html']['content'] = $this->load->view('mobile/profile_view', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    private function follow($id)
    {
        //$this->isLogin();

        $user_id = $id[0];
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js',  'bijaks.aktor.follow.js');
        $data['category'] = 'follow';
        $data['topic_last_activity'] = 'bijak';

        $this->isBijaksProfile($user_id);
        $user = $this->user;

        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }

        $data['user'] = $user;

        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list = array();
        if($arr_profile['album_photo'])
        {
          foreach($arr_profile['album_photo'] as $rwPhoto)
          {
             $_photo_list = $rwPhoto['photo'];
          }
        }

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;

        //$data['list_photo'] = $this->aktor_lib->get_foto_profile($user['page_id'], 4, 0)->result_array();
        $data['follow'] = $this->aktor_lib->get_follower($user['page_id'])->result_array();

        $data['hot_profile'] = $this->profile->hot('true');
        $data['skandal'] = $this->scandal->sesi('true');
        $data['survey'] = $this->survey->mini_survey();
        $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');
        $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 9, 'true');

        $html['html']['content']  = $this->load->view('follow_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);

    }

    function photo($dt)
    {
//        $this->isLogin();
//        if(count($dt) == 1){

            $user_id = $dt[0];

            $this->isBijaksProfile($user_id);
            $data['detailProf'] = $this->user;
            $user = $this->user;

            $count_post = $this->aktor_lib->get_count_post(null, $user['page_id']);
            foreach($count_post->result_array() as $val){
                $user['count_'.$val['tipe']] = $val['total'];
            }

            $rowProf = $this->redis_slave->get('profile:detail:'.$dt[0]);
            $arr_profile = @json_decode($rowProf, true);
            $_photo_list = array();
            if($arr_profile['album_photo']) {
               foreach($arr_profile['album_photo'] as $rwPhoto)
               {
                 $_photo_list = $rwPhoto['photo'];
               }
            }

            $data['user'] = $user;
            $data['list_photo'] = $_photo_list;
            
            $html['html']['content'] = $this->load->view('mobile/profile_photo', $data, true);
            $this->load->view('m_tpl/layout', $html);


    }

    private function photo_album_list()
    {
        $tipe = $this->uri->segment(5);
        $page_id = $this->uri->segment(3);
        $page = $this->uri->segment(4);
        $user = $this->user;
        $limit = 12;
        $offset = ($limit*intval($page)) - $limit;
        $list_album = $this->aktor_lib->get_foto_profile_album($tipe, $page_id, $limit, $offset);
        $data['user'] = $user;
        $data['result'] = $list_album;
        $data['count_result'] = count($list_album);
        $this->load->view('photo_album_list', $data);
    }


    function news($url='')
    {
    	$page_id = $this->uri->segment(3);
    	

        $data = $this->data;
        
        $user = $this->user;
        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $data['user'] = $user;

        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list = array();
        if($arr_profile['album_photo'])
        {
            foreach($arr_profile['album_photo'] as $rwPhoto)
            {
                $_photo_list = $rwPhoto['photo'];
            }
        }

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;
        $data['follow'] = $this->aktor_lib->get_follower($page_id)->result_array();
        
        $user_count = $user['count_news'];

        $limit = 10;

        $page = 1;
        $per_page = $this->input->get('per_page');
        if(!is_bool($per_page)){
            if(!empty($per_page)){
                $page = $per_page;
            }
        }

        $offset = ($limit*intval($page)) - $limit;

        $news_list = $this->aktor_lib->get_news_by_aktor($page_id, $limit, $offset);
        $rkey_min = 'news:detail:';
        $result = array();

        $have_redis_empty = false;

        foreach($news_list->result_array() as $row=>$val)
        {
            $news = $this->redis_slave->get($rkey_min.$val['news_id']);
            $news_json = json_decode($news, true);
            if(empty($news_json)){
                $have_redis_empty = true;
                continue;
            }
            $news_json['entry_date'] = $val['entry_date'];
            $result[$row] = $news_json;

        }


        $this->load->library('pagination');

        $config['base_url'] = base_url().'aktor/news/'.$page_id.'?'; //.$qr;
        $config['total_rows'] = $user_count;
        $config['per_page'] = $limit;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2; // round($choice);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="disabled"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '...';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '...';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;

        $this->pagination->initialize($config);

        $data['pagi'] =  $this->pagination->create_links();
        $data['news'] = $result;
        $data['offset'] = $offset;
        $data['total']  = $user_count;

        $data['head_title'] = 'Berita ' . $user['page_name'];
        $data['polterkait'] = $user['page_name'];


        $html['html']['content'] = $this->load->view('mobile/profile_news', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    
    function scandals($url=null)
    {
    	$page_id = $this->uri->segment(3);

        $data = $this->data;
        
        $user = $this->user;
        $count_post = $this->aktor_lib->get_count_post($user['account_id'], $user['page_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }

        $data['user'] = $user;

        $rowProf = $this->redis_slave->get('profile:detail:'.$user['page_id']);
        $arr_profile = @json_decode($rowProf, true);
        $_photo_list  = array();
        if($arr_profile['album_photo'])
        {
            foreach($arr_profile['album_photo'] as $rwPhoto)
            {
                $_photo_list = $rwPhoto['photo'];
            }
        }

        $data['user'] = $user;
        $data['list_photo'] = $_photo_list;
        //$data['list_photo'] = $this->aktor_lib->get_foto_profile($user['page_id'], 4, 0)->result_array();

        $data['follow'] = $this->aktor_lib->get_follower($page_id)->result_array();
        
        $page = 1;
        $per_page = $this->input->get('per_page');
        if(!is_bool($per_page)){
            if(!empty($per_page)){
                $page = $per_page;
            }
        }

        $limit = 10;

        $offset = ($limit*intval($page)) - $limit;

        $user_count = $user['count_scandal'];

        $scandal_list = $this->aktor_lib->get_scandal_by_aktor($page_id, $limit, $offset);
        $rkey_min = 'scandal:min:';
        $rkey_player = 'scandal:players:';
        $rkey_photo_headline = 'scandal:photo:headline:';
        $rkey_photo = 'scandal:photo:';
        $result = array();

        foreach($scandal_list->result_array() as $row=>$val)
        {
            $skandal_min = $this->redis_slave->get($rkey_min.$val['scandal_id']);
            $skandal_player = $this->redis_slave->get($rkey_player.$val['scandal_id']);
            $skandal_photo_headline = $this->redis_slave->get($rkey_photo_headline.$val['scandal_id']);
            $skandal_photo = $this->redis_slave->get($rkey_photo.$val['scandal_id']);

            $skandal_json = json_decode($skandal_min, true);
            $skandal_json['player'] = json_decode($skandal_player, true);
            $skandal_json['photo'] = json_decode($skandal_photo, true);
            $skandal_json['photo_headline'] = json_decode($skandal_photo_headline, true);

            $result[$row] = $skandal_json;

        }

        $this->load->library('pagination');

        $config['base_url'] = base_url().'aktor/scandals/'.$page_id.'?'; //.$qr;
        $config['total_rows'] = $user_count;
        $config['per_page'] = $limit;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2; // round($choice);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="disabled"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '...';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '...';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;

        $this->pagination->initialize($config);

        $data['pagi'] =  $this->pagination->create_links();
        $data['skandal'] = $result;
        $data['offset'] = $offset;
        $data['total']  = $user_count;

        $data['head_title'] = 'Skandal ' . $user['page_name'];
        $data['polterkait'] = 'Skandal, ' . $user['page_name'];


        $html['html']['content'] = $this->load->view('mobile/profile_scandal', $data, true);
        $this->load->view('m_tpl/layout', $html);
    	
    }

    
    private function isLogin($return=false)
    {
        $cr = current_url();
        $this->member = $this->session->userdata('member');
        if(!$this->member['logged_in']){
            if($this->input->is_ajax_request()){
                return false;
            }else{
                if($return){
                    return false;
                }else{
                    redirect(base_url().'home/login?frm='.urlencode($cr));
                }
            }

        }

        return true;
        //exit('please');
    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }


}
