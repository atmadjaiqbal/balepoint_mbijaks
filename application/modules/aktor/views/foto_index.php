<div class="container">
    <?php $this->load->view('template/tpl_header_profile'); ?>
</div>
<br>
<div class="container" id="profile-detail">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span8"><h3><?=ucwords($album['title']);?></h3></div>
                    <div class="span4 text-right">
                        <?php if($user['page_id'] == $this->member['user_id']){ ?>
                        <a id="tambah_foto" href="#" class="btn-flat btn-flat-large btn-flat-dark">TAMBAH FOTO</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if($user['page_id'] == $this->member['user_id']){ ?>
                <div id="form_foto" class="row-fluid hide">
                    <div class="span12 komu-blog-post">
                        <form id="addalbumphotoform" name="addalbumphotoform" method="post" enctype="multipart/form-data" action="<?php echo base_url().'timeline/post_photo_album';?>" target="albumphotouploadiframe">
                            <div class="textarea_holder">
                                <input name="gcid" type="hidden" value ="<?=$album['content_id'];?>" />
                                <input class="input-flat input-flat-large" type="text" placeholder="photo's title..." name="title" value=""><br>
                                <textarea class="input-flat input-flat-large" id="description" name="description" autocomplete="off" cols="30" rows="3" placeholder="Say something about this..."></textarea>
                                <input type="file" name="userfile" id="userfile">
                                <div class="pull-right">
                                    <input type="reset" class="btn-flat btn-flat-dark" id="batal_post" value="Batal">
                                    <input type="submit" class="btn-flat btn-flat-dark" id="wallphotobtnpost" value="Kirim">

                                </div>
                            </div>
                        </form>
                    <iframe name="albumphotouploadiframe" id="albumphotouploadiframe" style="display:none;"></iframe>
                    </div>
                </div>
                <?php } ?>
                <br>
                <div id="foto_container" data-cid="<?=$album_id;?>" data-user="<?=$user['user_id'];?>" data-tipe="1" data-page='1'>

                </div>
                <br>
                <div class="row-fluid komu-wall-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a id="foto_load_more" class="komu-wall-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>

                <!-- KOMENTAR -->
                <div class="div-line"></div>
                <div class="row-fluid">

                    <h5>BERI KOMENTAR</h5>
                    <div class="media media-comment">
                        <div class="pull-left media-side-left">
                            <div style="background:
                                    url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                            </div>
                        </div>

                        <div class="media-body">
                            <div class="row-fluid">
                                <input id='comment_type' data-id="<?=$album_id;?>" type="text" name="comment" class="media-input" >
                            </div>
                            <div class="row-fluid">
                                <div class="span12 text-right">
                                    <span>Login untuk komentar</span>&nbsp;
                                    <a class="btn-flat btn-flat-dark">Register</a>
                                    <a class="btn-flat btn-flat-dark">Login</a>
                                    <a id="send_coment" class="btn-flat btn-flat-gray">Send</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="comment" data-page="1" data-id="<?=$album_id;?>" class="row-fluid comment-container">

                </div>
                <div class="row-fluid komu-follow-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a data-page="1" data-id="<?=$album_id;?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>
                <div class="div-line"></div>
                <!-- END KOMENTAR -->
                <br>
            </div>
        </div>
    </div>
</div>

<div id="photo_modal" class="modal hide fade">
    <div class="modal-container">
        <div class="modal-sub-container hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3></h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span7">
                        <img class="img-modal" src="">
                        <p class="desc-modal"></p>
                    </div>
                    <div class="span5">

                        <!-- KOMENTAR -->
                        <div class="div-line"></div>
                        <div class="row-fluid">

                            <h5>BERI KOMENTAR</h5>
                            <div class="media media-comment">
                                <div class="pull-left media-side-left">
                                    <div style="background:
                                            url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                                    </div>
                                </div>

                                <div class="media-body">
                                    <div class="row-fluid">
                                        <input id='comment_modal_type' data-id="" type="text" name="comment" class="media-input span12" >

                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12 text-right">
<!--                                            <span>Login untuk komentar</span>&nbsp;-->
                                            <a class="btn-flat btn-flat-dark">Register</a>
                                            <a class="btn-flat btn-flat-dark">Login</a>
                                            <a id="send_coment_modal" class="btn-flat btn-flat-gray">Send</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="comment_modal" data-page="1" data-id="" class="row-fluid comment-container">

                        </div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a id="comment_modal_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                        <div class="div-line"></div>
                        <!-- END KOMENTAR -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    <div class="modal-footer">-->
<!--        <a href="#" class="btn">Close</a>-->
<!--        <a href="#" class="btn btn-primary">Save changes</a>-->
<!--    </div>-->
</div>
