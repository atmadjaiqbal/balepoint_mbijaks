<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>

<!-- BLOG -->
<script type="text/javascript" src="<?=base_url();?>assets/js/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
        selector: "#blog_area",
        menubar:false,
        statusbar: false,
        height: 250,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste jbimages"
        ],
        toolbar: "insertfile undo redo | paste styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages"

    });
</script>

<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span12"><h3>Buat Blog</h3></div>
                </div>
                <br>

                <div class="row-fluid">
                    <div class="span12 komu-blog-post">
                        <form id="add_blog_form" name="add_blog_form" method="post" action="<?php echo base_url().'timeline/post_blog';?>">
                            <input name="cid" type="hidden" value ="<?=(!empty($blog['content_id'])) ? $blog['content_id'] : '0';?>" />
                            <input name="user" type="hidden" value ="<?php echo $user['user_id'];?>" />
                            <input name="id" type="hidden" value ="<?php echo $user['account_id'];?>" />
                            <input class="input-flat input-flat-large" type="text" placeholder="Blog title" name="title" value="<?=(!empty($blog['title'])) ? $blog['title'] : '';?>"><br>
                            <textarea id="blog_area" name="content_blog" style="width:100%"><?=(!empty($blog['content_blog'])) ? $blog['content_blog'] : '';?></textarea>
                            <div class="pull-right">
                                <input type="submit" class="btn-flat btn-flat-dark" id="wall_post_blog" value="Kirim">
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>