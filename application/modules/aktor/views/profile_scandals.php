<div class="container">
    <!--?php $this->load->view('template/tpl_header_profile'); ?-->
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<div class="container" id="profile-detail">
    <div class="sub-header-container">
        <div class="badge-info">    
         <?php echo $this->load->view('template/aktor/tpl_aktor_side'); ?>
        </div>   
        <div class="profile-info">
            <div class="profile-name">
                <p>Skandal <?php echo $user['page_name']; ?></p>
            </div>
            <div id="profile_scandal_container" data-user="<?=$user['page_id'];?>" data-tipe="1" data-page='1'>

            </div>
            <br>
            <div id="load_more_place" class="row-fluid komu-wall-list komu-wall-list-bg">
                <div class="span12 text-center">
                    <a id="profile_scandal_load_more" class="komu-wall-list-more" href="#">LOAD MORE</a>
                </div>
            </div>

        </div>
    </div>
</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>


            <!-- SKANDAL -->
            <?php echo $skandal; ?>


            <!-- HOT ISSUE -->
            <?php //echo $issue; ?>
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>

<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>