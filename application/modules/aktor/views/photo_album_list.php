<div class="row-fluid" id='aktor-album'>
    <ul>
        <?php
        $y = 0;
        foreach ($result as $rwAlbum)
        {
            $_imgalbum = 'http://www.bijaks.net/public/upload/image/politisi/'.$rwAlbum['page_id'].'/thumb/'.$rwAlbum['attachment_title'];
            ?>
            <li>
                <div class="img-frame">
                    <img class="lazy img-polaroid" src='<?php echo $_imgalbum; ?>'
                         data-src='<?php echo $_imgalbum; ?>'
                         data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $rwAlbum['title']; ?>"
                         alt=''/>
                    <div class="score-place score-place-overlay score" data-tipe="1" data-id="<?php echo $rwAlbum['content_id']; ?>"></div>
                    <!--a data-id='<?=$rwAlbum['content_id'];?>' data-user="<?=$user['page_id'];?>" href="#"><h4 class="album-title"><?=ucwords($rwAlbum['title']);?></h4></a-->
                    <p class="aktor-date" style="float:none !important;"><?=mdate('%M %d, %Y - %h:%s', strtotime($rwAlbum['entry_date']));?></p>
                </div>
            </li>
            <?php
            $y++;
        }
        ?>
    </ul>
</div>
