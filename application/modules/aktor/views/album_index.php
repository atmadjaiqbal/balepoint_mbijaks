<div class="container">
    <!--?php $this->load->view('template/tpl_header_profile'); ?-->
    <?php $this->load->view('template/tpl_sub_header'); ?>

</div>
<br>
<div class="container" id="profile-detail">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/aktor/tpl_aktor_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <div class="row-fluid komu-wall-title">
                    <div class="span8"><h3>Default Album</h3></div>
                    <div class="span4 text-right">
                    </div>
                </div>

                <div id="default_album_container" data-user="<?=$user['page_id'];?>" data-tipe="1" data-page='1' data-album='40'></div>
                <br>
                <div id="load_more_place" class="row-fluid komu-wall-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a id="default_album_load_more" class="komu-wall-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>

                <div class="row-fluid komu-wall-title">
                    <div class="span8"><h3>Profile</h3></div>
                    <div class="span4 text-right">
                    </div>
                </div>

                <div id="profile_album_container" data-user="<?=$user['page_id'];?>" data-tipe="1" data-page='1' data-album='22'></div>
                <br>
                <div id="load_more_place" class="row-fluid komu-wall-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a id="profile_album_load_more" class="komu-wall-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

    <!-- modal -->
<div id="photo_modal" class="modal hide fade">
    <div class="modal-container">
        <div class="modal-sub-container hide">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3></h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="span7">
                        <img class="img-modal" src="">
                        <p class="desc-modal"></p>
                    </div>
                    <div class="span5">

                        <!-- KOMENTAR -->
                        <div class="div-line"></div>
                        <div class="row-fluid">

                            <h5>BERI KOMENTAR</h5>
                            <div class="media media-comment">
                                <div class="pull-left media-side-left">
                                    <div style="background:
                                            url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                                    </div>
                                </div>

                                <div class="media-body">
                                    <div class="row-fluid">
                                        <input id='comment_modal_type' data-id="" type="text" name="comment" class="media-input span12" >

                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12 text-right">
                                            <!--                                            <span>Login untuk komentar</span>&nbsp;-->
                                            <a class="btn-flat btn-flat-dark">Register</a>
                                            <a class="btn-flat btn-flat-dark">Login</a>
                                            <a id="send_coment_modal" class="btn-flat btn-flat-gray">Send</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div id="comment_modal" data-page="1" data-id="" class="row-fluid comment-container">

                        </div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a id="comment_modal_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                        <div class="div-line"></div>
                        <!-- END KOMENTAR -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <div class="modal-footer">-->
    <!--        <a href="#" class="btn">Close</a>-->
    <!--        <a href="#" class="btn btn-primary">Save changes</a>-->
    <!--    </div>-->
</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>


            <!-- SKANDAL -->
            <?php echo $skandal; ?>


            <!-- HOT ISSUE -->
            <?php //echo $issue; ?>
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>

<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>