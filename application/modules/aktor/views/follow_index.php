<div class="container">
    <!--?php $this->load->view('template/tpl_header_profile'); ?-->
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container" id="profile-detail">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/aktor/tpl_aktor_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span12"><h3>Follows</h3></div>
                </div>
                <br>

                <ul class="nav nav-tabs" id="followTab">
                    <li class="active"><a data-id="<?=$user['account_id'];?>" data-user='<?=$user['page_id'];?>' data-tipe="2" href="#follower">FOLLOWER</a></li>

                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="follower">
                        <div data-id="<?=$user['account_id'];?>" data-user='<?=$user['page_id'];?>' data-tipe="2" id="follower_list"></div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a data-page="2" data-id="<?=$user['account_id'];?>" data-user='<?=$user['page_id'];?>' data-tipe="2" id="follower_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>


            <!-- SKANDAL -->
            <?php echo $skandal; ?>


            <!-- HOT ISSUE -->
            <?php //echo $issue; ?>
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>

<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>