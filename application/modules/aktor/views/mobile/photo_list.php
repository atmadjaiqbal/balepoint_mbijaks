<?php foreach($photo as $row=>$value){ 
		$content_date 	= date('d M Y', strtotime($value['entry_date']));
?>
	
	<div class="row">
		<div class="col-xs-12">
			<img class="img-media-list" src="<?php echo $value['large_url'];?>" alt="<?php echo $value['title'];?>" >
			<small class="media-heading-nomargin"><?php echo $content_date;?></small>
		</div>
	</div>
	<hr class="line-mini">
<?php } ?>