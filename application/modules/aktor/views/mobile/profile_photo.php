<?php 	$url = base_url('aktor/profile/'.$user['page_id']);
		$image  = base_url().'public/img/no-image-politisi.png';
        if(count($user['profile_photo']) > 0){
        	$image = ($user['profile_photo'][0]['large_url'] != 'None') ? $user['profile_photo'][0]['large_url'] : base_url().'public/img/no-image-politisi.png';
        } 
        if(count($user['headline_photo']) > 0){
            $image  = ($user['headline_photo'][0]['headline_url'] != 'None') ? $user['headline_photo'][0]['headline_url'] : base_url().'public/img/no-image-politisi.png';    
        }
		$content_date = ($user['partai_name'] != 'None') ? ' - '.$user['partai_name'] : ''; 
?>
<h4 class="kanal-title kanal-title-green">PROFILE PHOTO</h4>
<div class="row">
	<div class="col-xs-12">
		<img class="img-media-list" src="<?php echo $image; ?>">
	</div>
	<div class="col-xs-12">
		<p></p>
		<a class="" href="<?php echo $url;?>">
        	<h4 class="media-heading"><?php echo $user['page_name'];?><small><?php echo $content_date;?></small></h4>
        </a>
        <a class="a-block a-block-3" href="<?php echo base_url();?>aktor/scandals/<?php echo $user['page_id'];?>">SKANDAL <span class="clearfix"><?=$user['count_scandal'];?></span></a>
        <a class="a-block" href="<?php echo base_url();?>aktor/news/<?php echo $user['page_id'];?>">BERITA <span class="clearfix"><?=$user['count_news'];?></span></a>
        <a class="a-block a-block-3" href="<?php echo base_url();?>aktor/photo/<?php echo $user['page_id'];?>">PHOTO <span class="clearfix"><?=$user['count_photo'];?></span></a>
        
	</div>
</div>
<hr class="line-mini">
<!-- <pre> -->
<?php
    // var_dump($list_photo);
    $dt['photo'] = $list_photo;
    $this->load->view('mobile/photo_list', $dt);
 ?>
 <!-- </pre> -->