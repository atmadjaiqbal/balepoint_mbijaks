<?php foreach($result as $row=>$val){ ?>
    <?php
    $skandal_url = base_url() . 'scandal/index/'.$val['scandal_id'].'-'.urltitle($val['title']);
        $photo_url = $val['photo'][0]['thumb_landscape_url'];
        foreach($val['photo'] as $photo){
            if(intval($photo['content_group_type']) == 60)
            {
                $photo_url = $photo['thumb_landscape_url'];
            }
        }
    ?>
<div class="row-fluid aktor-skandal-list">
    <div class="span4 aktor-skandal-left">
        <img class="aktor-skandal-photo" src="<?=$photo_url;?>">
        <div class="score-place score-place-overlay score" data-tipe="0" data-id="<?=$val['content_id'];?>"></div>
    </div>
    <div class="span8 aktor-skandal-right">
        <a href="<?php echo $skandal_url; ?>"><h4><?=$val['title'];?></h4></a>
        <em><small><?=mdate('%d %M %Y', strtotime($val['date']));?></small></em>

        <div class="row-fluid ">
            <div class="span2"><span>Politisi Terkait</span></div>
            <?php if (count($val['player']) > 0){ ?>
            <?php foreach ($val['player'] as $key => $pro) { ?>
                <?php if($key === 7) break; ?>
                <div class="content-politisi-terkait">
                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                        <img title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                    </a>
                </div>
                <?php } ?>

            <?php }else{ ?>
            <div class="span10"><span>Tidak ada politisi terkait</span></div>
            <?php } ?>
        </div>

    </div>
</div>
    <div class="div-line-small"></div>
<?php } ?>

<script>
    if(<?=$count_result;?> < 10 && <?=$count_result;?> != -1){
        $('#load_more_place').remove();
        $('#div_line_bottom').remove();
    }
</script>