<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Aktor_Lib {

    protected $CI;

    public function __construct()
    {    	
        $this->CI =& get_instance();

        $this->db = $this->CI->load->database('slave', true);
    }

    public function get_user($select='*', $where=array())
    {
        $this->db->select($select);
        $this->db->from('tusr_account ta');
        $this->db->join('tobject_page tp', 'tp.page_id = ta.user_id', 'left');
        $this->db->join('tcontent_attachment tat', 'tat.content_id = tp.profile_content_id', 'left');

        $this->db->where($where);
        $data = $this->db->get();
//        echo $this->db->last_query();
        return $data;
    }

    public function get_info_address($select='*', $where=array())
    {
        $this->db->select($select);
        $this->db->where($where);
        $data = $this->db->get('tobject_info_address');
        return $data;
    }

    public function insert_kontak($data=array())
    {
        $this->db->insert('tobject_info_address', $data);
    }

    public function update_kontak($data=array(), $where=array())
    {
        $this->db->where($where);
        $this->db->update('tobject_info_address', $data);
    }

    public function get_count_post($account_id, $page_id)
    {
        $query = "
                SELECT * FROM
                (
                    SELECT 'photo' AS tipe, photo.* FROM
                    (
                        SELECT COUNT(*) AS total  FROM tcontent tc WHERE tc.page_id = '$page_id'
                        AND content_type = 'IMAGE' AND content_group_type IN (40)
                    ) AS photo UNION
                    SELECT 'follower' AS tipe, follower.* FROM
                    (
                        SELECT COUNT(*) AS total FROM `tusr_follow` WHERE page_id = '$page_id'
                    )AS follower UNION
                    SELECT 'scandal' AS tipe, scandal.* FROM
                    (
			SELECT COUNT(*) AS total FROM tscandal_player WHERE page_id = '$page_id'
                    )AS scandal UNION
                    SELECT 'news' AS tipe, news.* FROM
                    (
			SELECT COUNT(*) AS total FROM tcontent_has_politic WHERE page_id = '$page_id'
                    )AS news

                )AS total
                ";

        $data = $this->db->query($query);
        return $data;

    }

    public function get_follow($account_id, $limit, $offset=0)
    {
        $query = "SELECT tf.*, tp.`page_id` AS page_id_fl, tp.page_name, tp.`about`, tp.`personal_message`, ta.`display_name`, ta.`gender`, ta.`current_city`, tat.attachment_title
            FROM `tusr_follow` tf
            LEFT JOIN tobject_page tp ON tp.`page_id` = tf.`page_id`
            LEFT JOIN tusr_account ta ON ta.`user_id` = tf.`page_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id` = tp.`profile_content_id`
            WHERE tf.account_id = '$account_id' AND tf.page_id NOT IN
            (
                SELECT page_id FROM `v_usr_politik`
            ) ORDER BY ta.display_name ASC LIMIT $limit OFFSET $offset;
        ";

        $data = $this->db->query($query);
        return $data;

    }

    public function is_follow($select='*', $where=array())
    {
        $this->db->select($select);
        $this->db->where($where);
        $data = $this->db->get('tusr_follow');
        return $data;
    }

    public function get_follow_politisi($account_id, $limit, $offset=0)
    {
        $query = "SELECT tf.*, tp.`page_id` AS page_id_fl, tp.page_name, tp.`about`, tp.`personal_message`, ta.`display_name`,
            ta.`gender`, ta.`current_city`, tat.attachment_title
            FROM `tusr_follow` tf
            LEFT JOIN tobject_page tp ON tp.`page_id` = tf.`page_id`
            LEFT JOIN tusr_account ta ON ta.`user_id` = tf.`page_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id` = tp.`profile_content_id`
            WHERE tf.account_id = '$account_id' AND tf.page_id IN
            (
                SELECT page_id FROM `v_usr_politik`
            ) ORDER BY ta.display_name ASC LIMIT $limit OFFSET $offset;
        ";
        $data = $this->db->query($query);
        return $data;

    }

    public function get_tusr_follow($select=array(), $where=array())
    {
        $this->db->select($select);
        $this->db->where($where);
        $data = $this->db->get('tusr_follow');
        return $data;
    }


    public function get_follower($user_id, $limit=null, $offset=0)
    {
        $query = "SELECT tf.*, tp.`page_id` AS page_id_fl, tp.page_name, tp.`about`, tp.`personal_message`, ta.`display_name`, ta.`gender`,
            ta.`current_city`, tat.attachment_title
            FROM `tusr_follow` tf
            LEFT JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
            LEFT JOIN tobject_page tp ON tp.`page_id` = ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id` = tp.`profile_content_id`
            WHERE tf.page_id = '$user_id'
            ORDER BY ta.display_name ASC ;
        ";

        if(!empty($limit))
        {
            $query .= ' LIMIT $limit OFFSET $offset';
        }
        $data = $this->db->query($query);
        return $data;

    }

    public function get_friend($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->from('tusr_friend tf');
        $this->db->join('tusr_account ta', 'ta.account_id = tf.friend_account_id', 'left');
        $this->db->join('tobject_page tp', 'tp.page_id = ta.user_id', 'left');
        $this->db->join('tcontent_attachment tat', 'tat.content_id = tp.profile_content_id', 'left');
        $this->db->where($where);
        if($limit != 0){
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        return $data;

    }

    public function get_friend_list($account_id, $limit=0, $offset=0)
    {
        $lm = '';
        if($limit != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }
        $query = "SELECT grp.*, ta.user_id AS page_id, ta.gender, ta.display_name AS
            page_name, tp.about, tat.attachment_title FROM
            (
                SELECT * FROM
                (
                    SELECT friend_account_id  FROM tusr_friend WHERE
                    account_id = '$account_id'
                )AS fr UNION ALL
                SELECT * FROM
                (
                    SELECT account_id AS friend_account_id FROM tusr_friend WHERE
                    friend_account_id = '$account_id'
                )AS fr2
            ) AS grp
            LEFT JOIN tusr_account ta ON ta.`account_id` = grp.friend_account_id
            LEFT JOIN tobject_page tp ON tp.`page_id` = ta.`user_id`
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tp.`profile_content_id`
            GROUP BY friend_account_id $lm;
            ";
        $data = $this->db->query($query);
        return $data;
    }

    public function get_friend_request($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->from('tusr_friend_request tf');
        $this->db->join('tusr_account ta', 'ta.account_id = tf.to_account_id', 'left');
        $this->db->join('tobject_page tp', 'tp.page_id = ta.user_id', 'left');
        $this->db->join('tcontent_attachment tat', 'tat.content_id = tp.profile_content_id', 'left');
        $this->db->join('tusr_account ta2', 'ta2.account_id = tf.account_id', 'left');
        $this->db->join('tobject_page tp2', 'tp2.page_id = ta2.user_id', 'left');
        $this->db->join('tcontent_attachment tat2', 'tat2.content_id = tp2.profile_content_id', 'left');
        $this->db->where($where);
        if($limit != 0){
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('tf.request_date desc');
        $data = $this->db->get();
        return $data;

    }

    public function get_blog($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->from('tcontent tc');
        $this->db->join('tcontent_blog tb', 'tb.content_id = tc.content_id', 'left');
        $this->db->where($where);
        if($limit != 0){
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        return $data;

    }

    public function get_blog_photo($select='*', $where=array(), $limit=0, $offset=0)
    {
        $this->db->select($select);
        $this->db->from('tcontent tc');
        $this->db->join('tcontent_attachment tat', 'tat.content_id = tc.content_id', 'left');
        $this->db->where($where);
        if($limit != 0){
            $this->db->limit($limit, $offset);
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        return $data;

    }

    public function get_foto_profile($page_id, $limit=0, $offset=0)
    {
        $lm = '';
        if (intval($limit) != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }
        $query = "SELECT t.content_id, t.account_id, t.`content_group_type`, t.`page_id`,
                    t.`title`, t.`description`, t.`entry_date`, tat.`attachment_title`
                    FROM tcontent t
                    LEFT JOIN tcontent_attachment tat ON tat.`content_id`=t.`content_id`
                    WHERE t.content_group_type IN (20, 22, 40, 60, 61, 70, 71) AND t.content_type = 'IMAGE'
                    AND t.page_id = '$page_id'
                    AND t.`status` = 0 ORDER BY t.content_group_type ASC $lm
                   ";

        $data = $this->db->query($query);
//        echo $this->db->last_query();
        return $data;
    }

    public function get_foto_profile_album($type=40, $page_id, $limit=0, $offset=0)
    {
        $lm = '';
        if (intval($limit) != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }

        // scandal = 60, race = 61, profile = 22
        $query = "SELECT t.content_id, t.account_id, t.`content_group_type`, t.`page_id`,
                    t.`title`, t.`description`, t.`entry_date`, tat.`attachment_title`
                    FROM tcontent t
                    LEFT JOIN tcontent_attachment tat ON tat.`content_id`=t.`content_id`
                    WHERE t.content_group_type = '$type' AND t.content_type = 'IMAGE'
                    AND t.page_id = '$page_id'
                    AND t.`status` = 0 ORDER BY t.content_group_type ASC $lm
                   ";
        $data = $this->db->query($query);
        return $data->result_array();
    }

    public function get_foto_group($account_id, $page_id, $limit=0, $offset=0)
    {
        $lm = '';
        if (intval($limit) != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }
        $query = "SELECT * FROM
                    (
                        SELECT * FROM
                        (
                            SELECT t.content_id, t.account_id, t.`content_group_type`, t.`page_id`, t.`title`, t.`description`, t.`entry_date`
                            FROM tcontent t
                            WHERE t.content_group_type = 22 AND t.content_type = 'GROUP'
                            AND (t.account_id = '$account_id' AND t.page_id = '$page_id')
                            AND t.`status` = 0
                        )AS profile_picture UNION ALL

                        SELECT * FROM
                        (
                            SELECT t.content_id, t.account_id, t.`content_group_type`, t.`page_id`, t.`title`, t.`description`, t.`entry_date`
                            FROM tcontent t
                            WHERE t.content_group_type = 20 AND t.content_type = 'GROUP'
                            AND (t.account_id = '$account_id' OR t.page_id = '$page_id')
                            AND t.`status` = 0
                        )AS wall_photo UNION ALL

                        SELECT * FROM
                        (
                            SELECT t.content_id, t.account_id, t.`content_group_type`, t.`page_id`, t.`title`, t.`description`, t.`entry_date`
                            FROM tcontent t
                            WHERE t.content_group_type = 40 AND t.content_type = 'GROUP'
                            AND (t.account_id = '$account_id' AND t.page_id = '$page_id')
                            AND t.`status` = 0 ORDER BY t.entry_date DESC
                        )AS album
                    )AS photo_group $lm
            ";

        $data = $this->db->query($query);
        return $data;

    }

    public function get_foto_album($content_id, $limit=0, $offset=0)
    {
        $lm = '';
        if($limit != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }
        $query = "SELECT tc.* , tat.`attachment_title`
            FROM tcontent tc
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tc.`content_id`
            WHERE group_content_id = '$content_id' ORDER BY entry_date DESC $lm;";
        $data = $this->db->query($query);
        return $data;

    }

    public function tusr_account($data=array(), $where=array())
    {
        $this->db->where($where);
        $this->db->update('tusr_account', $data);
    }

    public function tobject_page($data=array(), $where=array())
    {
        $this->db->where($where);
        $this->db->update('tobject_page', $data);
    }

    public function get_foto_list($content_id, $limit=0, $offset=0)
    {
        $lm = '';
        if($limit != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }
        $query = "SELECT tc.* , tat.`attachment_title`
            FROM tcontent tc
            LEFT JOIN tcontent_attachment tat ON tat.`content_id`=tc.`content_id`
            WHERE tc.content_id = '$content_id' ORDER BY entry_date DESC $lm;";
        $data = $this->db->query($query);
        return $data;

    }

    public function get_wall_list($account_id, $page_id, $limit=10, $offset=0)
    {
        $query = "
            SELECT st.*,
            tat.attachment_title, c_blog.content_blog,
            ta.`user_id` AS from_page_id, tp.page_name AS from_page_name, tc.attachment_title AS from_attachment_title,
            tp2.page_id AS to_page_id, tp2.page_name AS to_page_name, tc2.attachment_title AS to_attachment_title
            FROM
            (
                SELECT * FROM
                (
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND (t.account_id = '$account_id' OR t.page_id = '$page_id')
                        AND t.`status` = 0
                    )AS own_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND t.account_id IN
                        (
                            SELECT friend_account_id FROM tusr_friend WHERE `account_id` = '$account_id' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`friend_account_id`
                            WHERE tf.`account_id` = '$account_id'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND t.account_id IN
                        (
                            SELECT account_id
                            FROM tusr_friend
                            WHERE `friend_account_id` = '$account_id' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
                            WHERE tf.`friend_account_id` = '$account_id'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status_2 UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND (t.account_id = '$account_id' OR t.page_id = '$page_id')
                        AND t.`status` = 0
                    )AS own_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND t.account_id IN
                        (
                            SELECT friend_account_id FROM tusr_friend WHERE `account_id` = '$account_id' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`friend_account_id`
                            WHERE tf.`account_id` = '$account_id'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT * FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND t.account_id IN
                        (
                            SELECT account_id
                            FROM tusr_friend
                            WHERE `friend_account_id` = '$account_id' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
                            WHERE tf.`friend_account_id` = '$account_id'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status_ph
                )AS stat GROUP BY content_id
            )AS st
            LEFT JOIN tcontent_attachment tat ON tat.content_id = st.content_id
            LEFT JOIN tcontent_blog c_blog ON c_blog.content_id = st.content_id
            LEFT JOIN tusr_account ta ON ta.`account_id` = st.account_id
            LEFT JOIN tobject_page tp ON tp.page_id = ta.user_id
            LEFT JOIN tcontent_attachment tc ON tc.content_id = tp.profile_content_id
            LEFT JOIN tobject_page tp2 ON tp2.page_id = st.page_id
            LEFT JOIN tcontent_attachment tc2 ON tc2.content_id = tp2.profile_content_id
            ORDER BY entry_date DESC LIMIT $limit OFFSET $offset
        ";
        $data = $this->db->query($query);
        return $data;
    }

    public function get_wall_list_content_id($account_id, $page_id, $limit=10, $offset=0)
    {
        $query = "
            SELECT st.*
            FROM
            (
                SELECT * FROM
                (
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.`entry_date` FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND (t.account_id = '$account_id' OR t.page_id = '$page_id')
                        AND t.`status` = 0
                    )AS own_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.`entry_date` FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND t.account_id IN
                        (
                            SELECT friend_account_id FROM tusr_friend WHERE `account_id` = '$account_id' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`friend_account_id`
                            WHERE tf.`account_id` = '$account_id'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.`entry_date` FROM tcontent t
                        WHERE t.content_group_type IN (10, 11, 20, 22)
                        AND t.account_id IN
                        (
                            SELECT account_id
                            FROM tusr_friend
                            WHERE `friend_account_id` = '$account_id' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
                            WHERE tf.`friend_account_id` = '$account_id'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status_2 UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.`entry_date` FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND (t.account_id = '$account_id' OR t.page_id = '$page_id')
                        AND t.`status` = 0
                    )AS own_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.`entry_date` FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND t.account_id IN
                        (
                            SELECT friend_account_id FROM tusr_friend WHERE `account_id` = '$account_id' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`friend_account_id`
                            WHERE tf.`account_id` = '$account_id'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status UNION ALL
                    SELECT * FROM
                    (
                        SELECT t.content_id, t.`entry_date` FROM tcontent t
                        WHERE t.content_group_type = 40 AND content_type = 'GROUP'
                        AND t.account_id IN
                        (
                            SELECT account_id
                            FROM tusr_friend
                            WHERE `friend_account_id` = '$account_id' ORDER BY friend_date ASC
                        )
                        AND t.page_id IN
                        (
                            SELECT ta.`user_id`
                            FROM tusr_friend tf
                            JOIN tusr_account ta ON ta.`account_id` = tf.`account_id`
                            WHERE tf.`friend_account_id` = '$account_id'
                            ORDER BY tf.friend_date ASC
                        )
                        AND t.`status` = 0
                    )AS friend_status_ph
                )AS stat GROUP BY content_id
            )AS st
            ORDER BY entry_date DESC LIMIT $limit OFFSET $offset
        ";
        $data = $this->db->query($query);
        return $data;
    }

    function get_scandal_by_aktor($aktor_id, $limit=0, $offset=0)
    {
        $lm = '';
        if($limit != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }
        $query = "SELECT ts.scandal_id, t.content_id
                    FROM tscandal ts
                    LEFT JOIN tcontent t ON t.`location` = ts.`scandal_id` AND t.`content_group_type`=50
                    WHERE scandal_id
                    IN(
                        SELECT scandal_id FROM tscandal_player WHERE page_id = '$aktor_id' GROUP BY scandal_id
                    ) ORDER BY created_date DESC $lm";

        $data = $this->db->query($query);
        return $data;

    }

    function get_news_by_aktor($aktor_id, $limit=0, $offset=0)
    {
        $lm = '';
        if($limit != 0){
            $lm = 'limit '.$limit.' offset '.$offset;
        }
        $query = "SELECT * FROM
                    (
                        SELECT tt.`news_id`, tt.`content_id`, t.`entry_date`
                        FROM tcontent_text tt
                        LEFT JOIN tcontent t ON t.`content_id` = tt.`content_id`
                        WHERE tt.content_id
                        IN
                        (
                            SELECT content_id FROM tcontent_has_politic WHERE page_id = '$aktor_id'
                        )
                        GROUP BY tt.content_id

                    ) AS news ORDER BY entry_date DESC $lm";

        $data = $this->db->query($query);
        return $data;

    }

}

?>  