<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends Application
{
	function __construct()
	{
		parent::__construct();
   }
	
	function index()
	{
      $this->home();
	}

   function home()
   {
		$data = array_merge($this->data, array(
            'title'   => ' Home'
      ));
      
      $data['navbar']['controller'] = 'home';
      
		$this->load->view('pages/tpl_home', $data);
      
   }

   function article_list()
   {
		$data = array_merge($this->data, array(
            'title'   => ' news list'
      ));
      
      $data['navbar']['controller'] = 'news';
      $data['navbar']['method']     = 'index';
      
		$this->load->view('pages/tpl_article_list', $data);
   }

   function article()
   {
		$data = array_merge($this->data, array(
            'title'   => ' news list'
      ));
      
      $data['navbar']['controller'] = 'news';
      $data['navbar']['method']     = 'detail';
      
		$this->load->view('pages/tpl_article', $data);
   }


   function style_google()
   {
		$data = array_merge($this->data, array(
            'title'   => ' Google Style'
      ));
      
      $data['navbar']['controller'] = 'news';
      $data['navbar']['method']     = 'detail';
      
		$this->load->view('pages/tpl_style_google', $data);
   }
	
	function timeline()
	{
		$data = array_merge($this->data, array(
            'title'   => ' Timeline',
            'styles' => $this->uri->rsegment(2)
      ));

	 $this->load->view('pages/tpl_timeline', $data);  
	}
	
	
	function timeline_simple()
	{
		$data = array_merge($this->data, array(
            'title'   => $this->uri->rsegment(2)
      ));

	 $this->load->view('pages/tpl_timeline_simple', $data);  
	}	
	

	function page_header()
	{
		$data = array_merge($this->data, array(
            'title'   => $this->uri->rsegment(2)
      ));

	 $this->load->view('pages/tpl_page_header', $data);  
	}	
	
	
	
   function blank()
   {
		$data = array_merge($this->data, array(
            'title'   => ' HOme',
            'styles' => array('timeline')
      ));

		$this->load->view('pages/tpl_page', $data);
   }
}