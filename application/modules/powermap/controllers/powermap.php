<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Powermap extends MX_Controller
{
	public $membership;
	public $uID;
	public $isLogin;

	protected  $CI;
	public function __construct()
	{
		parent::__construct();

		$this->CI =& get_instance();

		$this->load->helper('url');
		$this->load->helper('imagefull');
		$this->load->helper('date');
		$this->load->helper('m_date');
		$this->load->library('session');
      	$this->load->library('core/bijak');

      	$this->load->library('powermap/powermap_lib');
		//$this->load->model('Powermap_model');
		$this->load->model('member');
		$this->load->helper('text');

		$this->load->helper('bijak');

		$this->current_url = '/'.$this->uri->uri_string();
		$this->session->set_flashdata('referrer', $this->current_url);
		$this->checkMember();
		$this->checkUID();
	}

	public function index(){

                redirect('/notice/error_404'); 


		$data['member'] = $this->membership;
		$data['activePage'] = "powermap";
		$data['isLogin'] = $this->isLogin;
		$data['sidebar']  = $this->bijak->getSidebarSentimen();
		
//		$this->load->view('powermap/powermap_index', $data);
	}

	public function maps()
	{
		/*if(!$this->isLogin()){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'must login')));
			return;
		}*/
		$this->CI->load->library('politik/politik_lib');
		
		$id = $this->input->post('id');
		if(is_bool($id) || $id == '0'){
			// $hot = $this->CI->politik_lib->getHotPolitisi(1, 1);
			$id = 'anasurbaningrum5119de3b29896';//$hot[0]['page_id'];
		}

		$width = $this->input->post('w');
		$height = $this->input->post('h');
		$retas = $this->input->post('retas');
		// $idx = $this->input->post('idx');

		$data['width'] = $width;
		$data['height'] = $height;
		$data['retas'] = $retas;
		$data['id'] = $id;

		// var_dump($hot[0]['page_id']);

		$this->load->view('powermap/powermap_profile', $data);
		// $this->load->view('powermap/powermap_draggable', $data);
	}

	public function nodes($idx=6)
	{
		/*if(!$this->isLogin()){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'must login')));
			return;
		}*/

		$db = $this->Powermap_model;
        $dd = $this->input->post('dd', TRUE);
		$noin = $this->input->post('notin') ? $this->input->post('notin') : "0";
		$id = $idx;

		/*if(!$id){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'no content')));
			return;
		}*/
		
		$noin_exp = explode(", ", $noin);

		$nodes = array();
		$i = 0;
		$id_node = array();
		$id_node[] = $id;

		$where = array('idx' => $id);

		$node = $db->getpowermapnodes($where, $select='*')->row_array();
		$node['thumb'] = photo_name($node['page_id'], 'icon', 'politisi');
		$elip = ellipsize($node['name'], 10, .5);
		$node['short_name'] = str_replace('&hellip;', '...', $elip);
		
		//$or_where = array('source' => $id, 'target' => $id);
		$where_s = "(source = ".$id." OR target = ".$id.") AND id_power_map_links NOT IN (".$noin.")";
        if($dd != '0'){
            $where_s .= " AND scandal_id is not null ";
        }
        $ln = $db->getlinks($where_s, $or_where=array());
		
		$links = array();
		$nodes = array();
		$i=0;
		$j = 0;
		
		foreach($ln->result_array() as $row){
			
			$where = array('idx' => $row['source']);
			$mode = $db->getpowermapnodes($where, $select='*')->row_array();
			$mode['thumb'] = photo_name($mode['page_id'], 'icon', 'politisi');
			$elip = ellipsize($mode['name'], 10, .5);
			$mode['short_name'] = str_replace('&hellip;', '...', $elip);
			
			// $row['source'] = $mode;
			
			$where = array('idx' => $row['target']);
			$kode = $db->getpowermapnodes($where, $select='*')->row_array();
			$kode['thumb'] = photo_name($kode['page_id'], 'icon', 'politisi');
			$elip = ellipsize($kode['name'], 10, .5);
			$kode['short_name'] = str_replace('&hellip;', '...', $elip);

			$links[$i] = $row;

			$nodes[] = $mode;
			$nodes[] = $kode;

			$j++;
			$i++;
		}

		// $noes = array_keys($nodes);
		asort($nodes);
		$n = array_unique($nodes, SORT_REGULAR);
		
		$id_power_map_nodes = array();
		$nd = array();
		$m=0; 
		foreach ($n as $value) {
			// $value['index'] = intval($value['idx']);
			if(($value['idx'] == $node['idx']) && ($dd != '0')){
				$value['lebar'] = intval($value['lebar']);
			}
			$nd[$m] = $value;
			$id_power_map_nodes[$m] = $value['idx'];
			$m++;
		}

		$new_links = array();
		$id_power_map_links = array();
		$l=0;
		foreach ($links as $value) {
			
			$new_links[$l] = $value;
			$id_power_map_links[$l] = $value['id_power_map_links'];
			$l++;
		}

		$data['nodes'] = $nd;
		$data['links'] = $new_links;
		$data['ls_link'] = implode(', ', $id_power_map_links);
		$data['ls_node'] = implode(', ', $id_power_map_nodes);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));

	}

	public function nextpend()
	{
		/*if(!$this->isLogin()){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'must login')));
			return;
		}*/

		$db = $this->Powermap_model;
        $dd = $this->input->post('dd', TRUE);
		$ls_node = $this->input->post('node');
		$ls_link = $this->input->post('link');
		
		$id = $this->input->post('idx');

		if(!$id){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'no content')));
			return;
		}
		
		$data['status'] = false;

		$where_s = "(source = ".$id." OR target = ".$id.") AND id_power_map_links NOT IN (". $ls_link .")";
        if($dd != '0'){
            $where_s .= " AND scandal_id is not null ";
        }
		$ln = $db->getlinks($where_s, $or_where=array());
		if($ln->num_rows() > 0){
			$data['status'] = true;
		}
		
		$links = array();
		$nodes = array();
		$i=0;
		$j = 0;
		
		foreach($ln->result_array() as $row){
			
			$where = 'idx =' . $row['source'] . ' AND idx NOT IN ('.$ls_node.')';
			$mode = $db->getpowermapnodes($where, $select='*');
			if($mode->num_rows() > 0){
				$mode = $mode->row_array();
				$mode['thumb'] = photo_name($mode['page_id'], 'icon', 'politisi');
				$elip = ellipsize($mode['name'], 10, .5);
				$mode['short_name'] = str_replace('&hellip;', '...', $elip);
				$nodes[] = $mode;
			}
			
			
			
			$where = 'idx =' . $row['target'] . ' AND idx NOT IN ('.$ls_node.')';
			$kode = $db->getpowermapnodes($where, $select='*');
			if($kode->num_rows() > 0){
				$kode = $kode->row_array();
				$kode['thumb'] = photo_name($kode['page_id'], 'icon', 'politisi');
				$elip = ellipsize($kode['name'], 10, .5);
				$kode['short_name'] = str_replace('&hellip;', '...', $elip);
				$nodes[] = $kode;
			}
			
			$links[$i] = $row;

			$j++;
			$i++;
		}

		// $noes = array_keys($nodes);
		// asort($nodes);
		$n = array_unique($nodes, SORT_REGULAR);
		
		$id_power_map_nodes = array();
		$nd = array();
		$m=0; 
		foreach ($n as $value) {
			// $value['index'] = intval($value['idx']);
			$nd[$m] = $value;
			$id_power_map_nodes[$m] = $value['idx'];
			$m++;
		}

		$new_links = array();
		$id_power_map_links = array();
		$l=0;
		foreach ($links as $value) {
			
			/*if(!is_bool(array_search($value['source'], $nd)) && !is_bool(array_search($value['target'], $nd)) ){
				$value['source'] = array_search($value['source'], $nd);
				$value['target'] = array_search($value['target'], $nd);
			}*/
			
			$new_links[$l] = $value;
			$id_power_map_links[$l] = $value['id_power_map_links'];
			$l++;
		}

		
		$ls_link = explode(', ', $ls_link);
		$ls_node = explode(', ', $ls_node);

		$no_links = array_merge($id_power_map_links, $ls_link);
		$no_nodes = array_merge($id_power_map_nodes, $ls_node);

		$data['nodes'] = $nd;
		$data['links'] = $new_links;
		$data['ls_link'] = implode(', ', $no_links);
		$data['ls_node'] = implode(', ', $no_nodes);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data));

	}

	public function polprofile($id='')
	{
		/*if(!$this->isLogin()){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'must login')));
			return;
		}*/
		
		$db = $this->Powermap_model;
		
		$width = ($this->input->get('w')) ? $this->input->get('w') : $this->input->post('w');
		$height = ($this->input->get('h')) ? $this->input->get('h') : $this->input->post('h');

		$tipe = ($this->input->get('t')) ? $this->input->get('t') : '1';
		
		/*$where = array('page_id' => $id);
		$select = 'idx';
		$idx = $db->getIDX($select, $where);
		if($idx->num_rows() == 0){
			// $this->output->set_content_type('application/json');
			// $this->output->set_output(json_encode(array('msg' => 'no content')));
			echo '<p>Powermap belum tersedia</p>';
			return;
		}

		$id_profile = $idx->row_array()*/;

		$data['width'] = $width;
		$data['height'] = $height;
		
		// $data['idx'] = $id_profile['idx'];
		$data['id'] = $id;

		if($tipe == '1'){
			$this->load->view('powermap/powermap_profile', $data);	
		}else{
			$this->load->view('powermap/powermap_profile_lite', $data);	
		}
		
	}

	public function skandal($id=0)
	{
		$this->load->model('Powermap_model');			
		$width = ($this->input->get('w')) ? $this->input->get('w') : $this->input->post('w');
		$height = ($this->input->get('h')) ? $this->input->get('h') : $this->input->post('h');

		$tipe = ($this->input->get('t')) ? $this->input->get('t') : '1';
		
		if($id == 0){
			echo '<p>Powermap belum tersedia</p>';
			return;
		}

		$data['width'] = $width;
		$data['height'] = $height;
		
		$data['id'] = intval($id);
		
		if($tipe == '1'){
			$this->load->view('powermap/powermap_skandal', $data);	
		}else{
			$this->load->view('powermap/powermap_lite_view', $data);	
			
		}

	}

	public function profile()
	{
		/*if(!$this->isLogin()){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'must login')));
			return;
		}*/

		$db = $this->Powermap_model;
		$pageid = $this->input->post('id');

		$where = array('page_id' => $pageid);
		
		$scandal = $db->skandalwithplayer($where, 3, 0, "tscandal.date desc");
		

		$scandals = array();
		$i = 0;
		foreach ($scandal->result_array() as $row) {
			$wh = array('scandal_id' => $row['scandal_id']);
			$foto = $this->Powermap_model->getscandalphoto($wh, $limit=1, $offset=0);
			if($foto->num_rows() > 0){
				$res_foto = $foto->row_array();
				$row['foto_scandal'] = thumb_url($res_foto['attachment_title']);
			}
			//$row['title'] = character_limiter($row['title'], 16);
			$row['date'] = mdate('%d-%M-%Y', strtotime($row['date']));
			$row['url'] = base_url() . "scandal/index/" . $row['scandal_id'] . "-" . urltitle($row['title']) . "/";
			$scandals['scandal'][$i] = $row;
			$i++;
		}
		$scandals['thumb'] = photo_name($pageid, 'badge', 'politisi');
		$scandals['msg'] = 'ok';
		if($scandal->num_rows() == 0)
		{
			$scandals['msg'] = 'bad';
		}
		

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($scandals));
		

	}

	public function nodelist()
	{
		/*if(!$this->isLogin()){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode(array('msg' => 'must login')));
			return;
		}*/

		$db = $this->Powermap_model;

		$term = $this->input->get('q');
		$page_limit = $this->input->get('page_limit');

		$where = "`name` like '%".$term."%'";
		$select = 'idx, name, page_id';
		$nodes = $db->getpowermapnodes($where, $select);
		$m_node = array();
		$i = 0;
		foreach($nodes->result_array() as $row){
			$row['photo_profile'] = photo_name($row['page_id'], 'badge', 'politisi');
			$m_node[$i] = $row;
			$i++;
		}

		$politisi['politisi'] = $m_node;
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($politisi));

	}


	private function checkMember() {
		$this->membership = $this->session->userdata('member');
		if($this->membership) {
			$this->membership['current_city'] = $this->member->getUserCurrentCity($this->membership['account_id']);
		} else {
			return;
		}
	}

   public function checkUID() {
		$uID = $this->input->post('uID');
		if(isset($_GET['uid']) && ! empty($_GET['uid'])) {
			$this->uID = $_GET['uid'];
			$this->isLogin = $this->isLogin($this->uID, $this->membership['account_id']);
		} else if(! empty($uID)) {
			$this->uID = $uID;
			$this->isLogin = $this->isLogin($this->uID, $this->membership['account_id']);
		} else {
			$this->uID  = $this->membership['account_id'];
			$this->isLogin = true;
		}
	}

	private function isLogin($str1, $str2) {
		if(! strcmp($str1, $str2)) {
			return true;
		} else {
			return false;
		}
	}

	public function powerscandal($id=0, $level=1)
	{
		if($id == 0){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$scandal_id = $id;

		$select = '*';
		$where = array('scandal_id' => $id);
		$scandal = $this->Powermap_model->getSkandal($select, $where);
		
		if($scandal->num_rows() == 0){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$scandal_array = $scandal->row_array();

		$kelek = 'tm.*, tps.page_id as source_page_id, tps.page_name as s_name, tps.node_size as s_size, tps.node_color as s_color, tpt.page_id as target_page_id, tpt.page_name as t_name, tpt.node_size as t_size, tpt.node_color as t_color';
		$where = "tm.scandal_id = ".$id;
		/*." AND (tm.source_id = '".$scandal_array['title']."' OR tm.target_id = '".$scandal_array['title']."')";*/
		$maps = $this->Powermap_model->relationsJoinObjectPage($kelek, $where, $limit=0, $offset=0);

		if($maps->num_rows() == 0){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$id = array();
		$unik_node = array();
		$i = 0;
		foreach ($maps->result_array() as $row) {
			$name_s = strtolower((!empty($row['source'])) ? $row['source'] : $row['s_name']);
			$unik_node[$i]['name'] = ucwords($name_s); 
			$unik_node[$i]['page_id'] = (!empty($row['source_page_id'])) ? $row['source_page_id'] : ucwords(strtolower($row['source_id']));
			$unik_node[$i]['size'] = (!empty($row['s_size'])) ? $row['s_size'] : $row['source_size'];
			$unik_node[$i]['color'] = (!empty($row['s_color'])) ? $row['s_color'] : 'C4C4C4';
			$i++;

			$name_t = strtolower((!empty($row['target'])) ? $row['target'] : $row['t_name']);
			$unik_node[$i]['name'] = ucwords($name_t);
			$unik_node[$i]['page_id'] = (!empty($row['target_page_id'])) ? $row['target_page_id'] : ucwords(strtolower($row['target_id']));
			$unik_node[$i]['size'] = (!empty($row['t_size'])) ? $row['t_size'] : $row['target_size'];
			$unik_node[$i]['color'] = (!empty($row['t_color'])) ? $row['t_color'] : 'C4C4C4';
			$id[] = $row['relations_id'];

			$i++;
		}

		$relations_id = implode($id, ', ');

		$unik_node = array_unique($unik_node, SORT_REGULAR);
		$unik_node = array_values($unik_node);

		// Change source_id to index
		$links = array();
		foreach ($maps->result_array() as $row) {
			$page_id = (!empty($row['source_page_id'])) ? $row['source_page_id'] : ucwords(strtolower($row['source_id']));
			foreach ($unik_node as $key => $value) {
				if ($page_id == $value['page_id']){
					$row['source_id'] = $key;
					$links[] = $row;
					break;
				}
			}
			
		}

		// Change target_id to index
		foreach ($links as $id => $row) {
			$page_id = (!empty($row['target_page_id'])) ? $row['target_page_id'] : ucwords(strtolower($row['target_id']));
			foreach ($unik_node as $key => $value) {
				if ($page_id == $value['page_id']){
					$row['target_id'] = $key;
					break;
				}
			}
			$links[$id] = $row;
		}

		$simple_links = array();
		$j = 0;
		foreach ($links as $key => $value) {
			$simple_links[$j]['source'] = $value['source_id'];
			$simple_links[$j]['target'] = $value['target_id'];
			$j++;
		}

		$unik_node[0]['color'] = 'ff0000';
		$unik_node[0]['size'] = '20';
		$unik_node[0]['dampak'] = $scandal_array['uang'];

		$data['nodes'] = $unik_node;
		$data['links'] = $simple_links;
		$data['id'] = $relations_id;
		$data['last_index'] = @max(array_keys($unik_node));

		$data_node = $data;
		$node_baru = array('nodes' => array(), 'links' => array(), 'id' => '', 'last_index' => '');
		
		$level = $level - 1;
		if($level >= 2){
			for ($i=1; $i < $level; $i++) { 
				
				$this->nextlevelnode($scandal_id, $data_node, $data, $node_baru);
				$data = $node_baru;
				$node_baru = array();
			}
		
		}

		$data_node['msg'] = 'ok';
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data_node));

		/*echo '<pre>';
		var_dump($node_baru);
		// var_dump($links);
		echo '<pre>';*/

	}

	public function powerfullscandal($id=0, $level=1)
	{
		if($id == 0){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$scandal_id = $id;

		$select = '*';
		$where = array('scandal_id' => $id);
		$scandal = $this->Powermap_model->getSkandal($select, $where);
		
		if($scandal->num_rows() == 0){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$scandal_array = $scandal->row_array();

		$kelek = 'tm.*, tps.page_id as source_page_id, tps.page_name as s_name, tps.node_size as s_size, tps.node_color as s_color, tpt.page_id as target_page_id, tpt.page_name as t_name, tpt.node_size as t_size, tpt.node_color as t_color';
		$where = "tm.scandal_id = ".$id;
		/*." AND (tm.source_id = '".$scandal_array['title']."' OR tm.target_id = '".$scandal_array['title']."')";*/
		$maps = $this->Powermap_model->relationsJoinObjectPage($kelek, $where, $limit=0, $offset=0);

		if($maps->num_rows() == 0){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$id = array();
		$unik_node = array();
		$i = 0;
		foreach ($maps->result_array() as $row) {
			$name_s = strtolower((!empty($row['source'])) ? $row['source'] : $row['s_name']);
			$unik_node[$i]['name'] = ucwords($name_s); 
			$unik_node[$i]['page_id'] =  (!empty($row['source_page_id'])) ? $row['source_page_id'] : ucwords(strtolower($row['source_id']));
			$unik_node[$i]['size'] = (!empty($row['s_size'])) ? $row['s_size'] : $row['source_size'];
			$unik_node[$i]['color'] = (!empty($row['s_color'])) ? $row['s_color'] : 'C4C4C4';
			$i++;

			$name_t = strtolower((!empty($row['target'])) ? $row['target'] : $row['t_name']);
			$unik_node[$i]['name'] = ucwords($name_t);
			$unik_node[$i]['page_id'] =  (!empty($row['target_page_id'])) ? $row['target_page_id'] : ucwords(strtolower($row['target_id']));
			$unik_node[$i]['size'] = (!empty($row['t_size'])) ? $row['t_size'] : $row['target_size'];
			$unik_node[$i]['color'] = (!empty($row['t_color'])) ? $row['t_color'] : 'C4C4C4';
			$id[] = $row['relations_id'];

			$i++;
		}

		$relations_id = implode($id, ', ');

		$unik_node = array_unique($unik_node, SORT_REGULAR);
		$unik_node = array_values($unik_node);

		// Change source_id to index
		$links = array();
		foreach ($maps->result_array() as $row) {
			$page_id = (!empty($row['source_page_id'])) ? $row['source_page_id'] : ucwords(strtolower($row['source_id']));
			foreach ($unik_node as $key => $value) {
				if ($page_id == $value['page_id']){
					$row['source_id'] = $key;
					$links[] = $row;
					break;
				}
			}
			
		}

		// Change target_id to index
		foreach ($links as $id => $row) {
			$page_id = (!empty($row['target_page_id'])) ? $row['target_page_id'] : ucwords(strtolower($row['target_id']));
			foreach ($unik_node as $key => $value) {
				if ($page_id == $value['page_id']){
					$row['target_id'] = $key;
					break;
				}
			}
			$links[$id] = $row;
		}

		$simple_links = array();
		$j = 0;
		foreach ($links as $key => $value) {
			$simple_links[$j]['source'] = $value['source_id'];
			$simple_links[$j]['target'] = $value['target_id'];
			$j++;
		}

		/*$unik_node[0]['color'] = 'ff0000';
		$unik_node[0]['size'] = '20';*/
		// $unik_node[0]['dampak'] = $scandal_array['uang'];

		$data['nodes'] = $unik_node;
		$data['links'] = $simple_links;
		$data['id'] = $relations_id;
		$data['last_index'] = @max(array_keys($unik_node));

		$data_node = $data;
		$node_baru = array('nodes' => array(), 'links' => array(), 'id' => '', 'last_index' => '');
		
		$level = $level - 1;
		if($level >= 2){
			for ($i=1; $i < $level; $i++) { 
				
				$this->nextlevelnode($scandal_id, $data_node, $data, $node_baru);
				$data = $node_baru;
				$node_baru = array();
			}
		
		}

		$data_node['msg'] = 'ok';
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data_node));

		/*echo '<pre>';
		var_dump($node_baru);
		// var_dump($links);
		echo '<pre>';*/

	}

	public function powerprofile($id='', $level=1)
	{
		if(empty($id)){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$id = urldecode($id);

		$kelek = 'tm.*, tps.page_id as source_page_id, tps.page_name as s_name, tps.node_size as s_size, tps.node_color as s_color, tpt.page_id as target_page_id, tpt.page_name as t_name, tpt.node_size as t_size, tpt.node_color as t_color';
		$where = "(tm.source_id = '".$id."' OR tm.target_id = '".$id."')";
		$maps = $this->Powermap_model->relationsJoinObjectPage($kelek, $where, $limit=0, $offset=0);

		if($maps->num_rows() == 0){
			// echo '<p>Powermap belum tersedia</p>';
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$id = array();
		$unik_node = array();
		$i = 0;
		foreach ($maps->result_array() as $row) {
			$name_s = strtolower((!empty($row['source'])) ? $row['source'] : $row['s_name']);
			$unik_node[$i]['name'] = ucwords($name_s); 
			$unik_node[$i]['page_id'] = (!empty($row['source_page_id'])) ? $row['source_page_id'] : ucwords(strtolower($row['source_id']));
			$unik_node[$i]['size'] = (!empty($row['s_size'])) ? $row['s_size'] : $row['source_size'];
			$unik_node[$i]['color'] = (!empty($row['s_color'])) ? $row['s_color'] : 'C4C4C4';
			$i++;

			$name_t = strtolower((!empty($row['target'])) ? $row['target'] : $row['t_name']);
			$unik_node[$i]['name'] = ucwords($name_t);
			$unik_node[$i]['page_id'] = (!empty($row['target_page_id'])) ? $row['target_page_id'] : ucwords(strtolower($row['target_id']));
			$unik_node[$i]['size'] = (!empty($row['t_size'])) ? $row['t_size'] : $row['target_size'];
			$unik_node[$i]['color'] = (!empty($row['t_color'])) ? $row['t_color'] : 'C4C4C4';
			$id[] = $row['relations_id'];

			$i++;
		}

		$relations_id = implode($id, ', ');

		$unik_node = array_unique($unik_node, SORT_REGULAR);
		$unik_node = array_values($unik_node);

		/*echo '<pre>';
		var_dump($unik_node);
		// var_dump($links);
		echo '<pre>';*/

		// Change source_id to index
		$links = array();
		foreach ($maps->result_array() as $row) {
			$page_id = (!empty($row['source_page_id'])) ? $row['source_page_id'] : ucwords(strtolower($row['source_id']));
			// echo $row['source_id'] . '<br>';
			foreach ($unik_node as $key => $value) {
				if ($page_id == $value['page_id']){
					$row['source_id'] = $key;
					$links[] = $row;
					break;
				}
			}
			
		}

		// Change target_id to index
		foreach ($links as $id => $row) {
			$page_id = (!empty($row['target_page_id'])) ? $row['target_page_id'] : ucwords(strtolower($row['target_id']));
			foreach ($unik_node as $key => $value) {
				if ($page_id == $value['page_id']){
					$row['target_id'] = $key;
					break;
				}
			}
			$links[$id] = $row;
		}

		$simple_links = array();
		$j = 0;
		foreach ($links as $key => $value) {
			$simple_links[$j]['source'] = $value['source_id'];
			$simple_links[$j]['target'] = $value['target_id'];
			$j++;
		}

		/*$unik_node[0]['color'] = '0000FF';
		$unik_node[0]['size'] = '20';*/
		
		$data['nodes'] = $unik_node;
		$data['links'] = $simple_links;
		$data['id'] = $relations_id;
		$data['last_index'] = @max(array_keys($unik_node));

		$data_node = $data;
		$node_baru = array('nodes' => array(), 'links' => array(), 'id' => '', 'last_index' => '');
		
		$level = $level - 1;
		if($level >= 2){
			for ($i=1; $i < $level; $i++) { 
				
				$this->nextlevelnode(0, $data_node, $data, $node_baru);
				$data = $node_baru;
				$node_baru = array();
			}
		
		}

		$data_node['msg'] = 'ok';
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data_node));

		/*echo '<pre>';
		var_dump($data_node);
		// var_dump($links);
		echo '<pre>';*/

	}

	public function nextnode(){
		$obj_graph = $this->input->post('graph');
		$id = $this->input->post('id');
		$id = urldecode($id);
		
		if(is_bool($obj_graph) || is_bool($id) || $obj_graph == '0' || empty($obj_graph) || $id == '0' || empty($id)){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$graph_array = json_decode($obj_graph, true);
		
		/*echo "<pre>";
		var_dump($graph_array['id']);
		echo "</pre>";*/

		$kelek = 'tm.*,tps.page_id as source_page_id, tps.page_name as s_name, tps.node_size as s_size, tps.node_color as s_color, tpt.page_id as target_page_id, tpt.page_name as t_name, tpt.node_size as t_size, tpt.node_color as t_color';
		$where = "tm.relations_id not in(".$graph_array['id'].") AND (tm.source_id = '".$id."' OR tm.target_id = '".$id."')";
		
		$maps = $this->Powermap_model->relationsJoinObjectPage($kelek, $where, $limit=0, $offset=0);
		
		if($maps->num_rows() == 0){
			$data_node['msg'] = 'bad';
		
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data_node));
			return;
		}

		$nodes_baru = array();

		$relasi_id = $this->generatenode($graph_array['nodes'], $nodes_baru, $maps->result_array());
		$this->generatelinks($graph_array['links'], $graph_array['nodes'], $maps->result_array());
		if(!empty($relasi_id)){
			$graph_array['id'] = $graph_array['id'].', '.$relasi_id;	
			
		}
		
		$graph_array['last_index'] = @max(array_keys($graph_array['nodes']));
		
		$data_node = $graph_array;
		$data_node['msg'] = 'ok';
		
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data_node));
			

	}

	private function nextlevelnode($scandal_id=0, &$temp_data, $data, &$data_baru)
	{
		// $temp_data = $data;
		$nodes = $data['nodes'];
		$links = $data['links'];
		$id = $data['id'];
		$last_index = $data['last_index'];

		$nodes_baru = array();

		foreach ($nodes as $key => $value) {
			$kelek = 'tm.*,tps.page_id as source_page_id, tps.page_name as s_name, tps.node_size as s_size, tps.node_color as s_color, tpt.page_id as target_page_id, tpt.page_name as t_name, tpt.node_size as t_size, tpt.node_color as t_color';
			$where = "tm.relations_id not in(".$id.") AND (tm.source_id = '".$value['page_id']."' OR tm.target_id = '".$value['page_id']."')";
			if($scandal_id != 0){
				$where .= " AND tm.scandal_id is not null "; //.$scandal_id;
			}
			$maps = $this->Powermap_model->relationsJoinObjectPage($kelek, $where, $limit=0, $offset=0);
			// echo $maps->num_rows();
			if($maps->num_rows() > 0){
				// echo $maps->num_rows();
				$relasi_id = $this->generatenode($temp_data['nodes'], $nodes_baru, $maps->result_array());
				$this->generatelinks($temp_data['links'], $temp_data['nodes'], $maps->result_array());
				if(!empty($relasi_id)){
					$id = $id.', '.$relasi_id;	
					$temp_data['id'] = $id;
					$data_baru['id'] = $id;
				}
				
				$last_index = @max(array_keys($temp_data['nodes']));
				$temp_data['last_index'] = $last_index;
				

				$data_baru['links'] = $temp_data['links'];
				$data_baru['last_index'] = $last_index;
				// echo $last_index.'<br>';
			}
			
		}

		$data_baru['nodes'] = $nodes_baru;

		// return $temp_data;
	}

	private function generatenode(&$nodes, &$data_baru, $links)
	{
		$new_nodes = array();
		$i = @max(array_keys($nodes)) + 1;
		$id_relasi = array();
		
		foreach ($links as $row) {
			
			$name_s = strtolower((!empty($row['source'])) ? $row['source'] : $row['s_name']);
			$new_nodes[$i]['name'] = ucwords($name_s);
			$new_nodes[$i]['page_id'] = (!empty($row['source_page_id'])) ? $row['source_page_id'] : ucwords(strtolower($row['source_id']));
			$new_nodes[$i]['size'] = (!empty($row['s_size'])) ? $row['s_size'] : '8';
			$new_nodes[$i]['color'] = (!empty($row['s_color'])) ? $row['s_color'] : 'C4C4C4';
			// array_push($nodes, $new_nodes[$i]);
			$i++;
			
			$name_t = strtolower((!empty($row['target'])) ? $row['target'] : $row['t_name']);
			$new_nodes[$i]['name'] = ucwords($name_t);
			$new_nodes[$i]['page_id'] = (!empty($row['target_page_id'])) ? $row['target_page_id'] : ucwords(strtolower($row['target_id']));
			$new_nodes[$i]['size'] = (!empty($row['t_size'])) ? $row['t_size'] : '8';
			$new_nodes[$i]['color'] = (!empty($row['t_color'])) ? $row['t_color'] : 'C4C4C4';
			// array_push($nodes, $new_nodes[$i]);
			$i++;
			
			$id_relasi[] = $row['relations_id'];

		}

		$new_nodes = array_unique($new_nodes, SORT_REGULAR);
		// $unik_node = array_values($new_nodes);

		$old_nodes = $nodes;

		foreach ($new_nodes as $key => $value) {
			$no_exist = true;
			foreach ($old_nodes as $row) {
				if($value === $row){
					$no_exist = false;
					break;
				}
			}

			if($no_exist){
				array_push($nodes, $value);	
				array_push($data_baru, $value);
			}
			
		}

		// array_unique($nodes);

		$relasi_id = implode($id_relasi, ', ');

		return $relasi_id;
	}

	private function generatelinks(&$links, &$nodes, $maps)
	{
		$i = @max(array_keys($nodes)) + 1;
		$new_links = array();
		foreach ($maps as $row) {
			$page_id = (!empty($row['source_page_id'])) ? $row['source_page_id'] : ucwords(strtolower($row['source_id']));
			foreach ($nodes as $key => $value) {
				if ($page_id == $value['page_id']){
					$row['source_id'] = $key;
					$new_links[$i] = $row;
					$i++;
					break;
				}
				
			}
			
		}

		// Change target_id to index
		foreach ($new_links as $ids => $row) {
			$page_id = (!empty($row['target_page_id'])) ? $row['target_page_id'] : ucwords(strtolower($row['target_id']));
			foreach ($nodes as $ken => $value) {
				if ($page_id == $value['page_id']){
					$row['target_id'] = $ken;
					break;
				}
			}
			// echo $row['target_id'];
			$new_links[$ids] = $row;
		}

		array_unique($new_links, SORT_REGULAR);

		$simple_links = array();
		foreach ($new_links as $key => $value) {
			$simple_links[$key]['source'] = $value['source_id'];
			$simple_links[$key]['target'] = $value['target_id'];
			// array_push($links, $simple_links[$key]);
		}

		$old_links = array();
		foreach ($simple_links as $key => $value) {
			$is_exist = true;
			foreach ($old_links as $row) {
				if($value == $row){
					$is_exist = false;
					break;
				}
				
			}
			if($is_exist){
				array_push($links, $value);
			}
		}
		
	}

	

}
