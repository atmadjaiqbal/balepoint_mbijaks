<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel='stylesheet' href='<?php echo base_url() ?>public/style/powermap.css'>

  <script src="<?php echo base_url(); ?>public/script/bijaks.min.js" ></script>
  <script src="<?php echo base_url(); ?>public/script/graph/d3.v2.js"></script>
</head>
<body>

<div class="" id="chart_<?php echo $id; ?>">
	<h5 class="powermap-label">Powermap</h5>
</div>

<script type="text/javascript">
  
	//var ready_hover = true;
	var dist = 100,
		ch = -5000;

	var width = <?php echo $width; ?>,
	    height = <?php echo $height; ?>,
	    root_node = [],
    	root_links = [];

	var fill = d3.scale.category20();

	var force = d3.layout.force()
	    .gravity(0.8)
	    .linkDistance(150)
	    .charge(-1000)
	    // .friction(0.5)
	    .linkStrength([1])
	    .size([width, height]);


	var svg = d3.select("#chart_<?php echo $id; ?>").append("svg")
	    .attr("width", width)
	    .attr("height", height)
	    .data([{x: 0, y: 0}]);

	var g = svg.append("svg:g")
		.attr("transform", "translate(-20, 0)")
		.append("svg:g");
    
    g.append('rect')
		.attr("width", width*2)
		.attr("height", height*2)
		.style("fill", "transparent")
		.style("stroke-width", "0")
		.style("stroke", "transparent")
		.style("cursor", "move")
		.data([{x: width / 2, y: height / 2}]);

    g.attr("opacity", 1e-6)
		.transition()
		.duration(5000)
		.attr("opacity", 1);

	var ls_links,
		ls_nodes;

// d3.json("<?php //echo base_url();?>powermap/nodes/<?php //echo $idx; ?>", function(graph) {
// 	ls_links = graph.ls_link;
// 	ls_nodes = graph.ls_node;
// 	root_links = graph.links;
// 	root_node = graph.nodes;
// 	// console.debug(root_links);
// 	var n_links = relasi(graph.nodes, graph.links);
// 	update(root_node, n_links);
  
// });
   

$.ajax({
    type:'post',
    url:"<?php echo base_url();?>powermap/powerscandal/<?php echo $id; ?>/3",
    async : false,
    success: function(graph){
     if(graph.msg == 'ok'){
        root_links = graph.links;
        root_node = graph.nodes;
        update(root_node, root_links);
      }else{
        $('#chart_<?php echo $id; ?>').html('<h5 class="powermap-label">Powermap belum tersedia</h5>');
      }
    }
});


function update(nodes, links){
  
  force
      .nodes(nodes)
      .links(links);

  var link = g.selectAll(".link")
      .data(links);

  var line = link.enter().insert("svg:line", ".node")
    .attr("class", "link")
    .style("stroke-width", function(d, i) { 
      var stro = Math.sqrt(parseInt(d.lebar) + parseInt(d.lebar));
      return Math.sqrt(stro); 
    })
    .attr("x1", function(d) { return d.source.x; })
    .attr("y1", function(d) { return d.source.y; })
    .attr("x2", function(d) { return d.target.x; })
    .attr("y2", function(d) { return d.target.y; });

  link.exit().remove();

  var node = g.selectAll(".node")
      .data(nodes, function(d) { return d.index; })
      
  // node.select("circle");
      
  var nodeEnter = node.enter().append("svg:g")
    .attr("class", "node")
    .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
    .call(force.drag);

  nodeEnter.append("svg:circle")
    .attr("class", "node")
    .attr("r", function(d) { 
        // console.debug(d);
        var lebar = d.size;
        return lebar;
    })
    .style("fill", function(d, i) { 
        return '#'+d.color;
    })
    .on('click', function(d){
      // console.log('click');
    })
    .on("mouseover", function(d,i){
      var dtext = d3.select(this)[0][0].nextSibling;
      var nm = d.name;
      if(d.size <= 8){
        $(dtext).text(nm);
      }
    })
    .on("mouseout", function(d,i){
      var dtext = d3.select(this)[0][0].nextSibling;
      if(d.size <= 8){
        $(dtext).text('');
      }
    });


  nodeEnter.append("svg:text")
    .attr("dx", function(d){ 
      var lebarnya = parseInt(d.size) + 8;
      return lebarnya; 
    })
    .attr("dy", ".35em")
    // .attr("class", "nodetext")
    .attr("font-size", function(d){ 
      var si = "14px";
      if(d.size <= 8){
        si = "11px";
      }
      return si;
    
    })
    .attr("font-weight", function(d){ 
      var si = "bold";
      if(d.size <= 8){
        si = "plain";
      }
      return si;
    
    })
    .text(function(d) { 
      var nm = d.name;
      if(d.dampak){
        nm = d.dampak;
      }

      if(d.size > 8){
        return nm;
      }else{
        return "";
      }
      
    }).style("cursor", "pointer")
    .on('click', function(d){
    	// window.location = "<?php echo base_url();?>politisi/index/"+d.page_id;
    });
    

    

  // node.exit().remove();

  force.on("tick", function() {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    /*nodeEnter.attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; });*/
    node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
  });

  force.start();

  
}



function relasi(nodes, links){
	var n_link = [];
	$.each(links, function(i, val){
	  $.each(nodes, function(j, row){
	      
	      if(parseInt(val.source) == parseInt(row.idx)){

	        val.source = j;
	        return false;
	      }
	  });
	  // console.debug(val.target);
	  $.each(nodes, function(k, mow){
	    
	    if(parseInt(val.target) == parseInt(mow.idx)){
	        // console.debug(i + ' ==> '+val.target + ' = '+ mow.idx);
	        val.target = k;
	        return false;
	      }
	  });
	 
	  n_link.push(val);
	});
	return n_link;

}

</script>

</body>
</html>