<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Komunitas extends Application
{
    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->helper('date');
        $this->load->helper('m_date');

        $this->load->helper('text');
        $this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');

        $this->load->library('komunitas/komunitas_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->load->library('suksesi/suksesi_lib');
    }

    public function _remap($method, $params=array())
    {
        if($method == 'index'){
//            show_404();
            redirect(base_url('home/login'));
        }
        if(method_exists($this, $method))
        {
            $this->$method($params);
        }else{

            $this->isBijaksUser($method);
            $this->index($method);
        }

    }

    private function isBijaksUser($user_id)
    {
        $select = 'ta.`account_id`, ta.`user_id`, ta.`display_name`, ta.account_type, ta.`registration_date`, ta.`last_login_date`, ta.`email`, ta.`fname`, ta.`lname`,
ta.`gender`, ta.`birthday`, ta.`current_city`, tp.`profile_content_id`, tp.`about`, tp.`personal_message`, tat.`attachment_title`';
        $user_id = urldecode($user_id);
//        echo $user_id;
//        exit;
        $where = array('ta.user_id' => strval($user_id));

        $users = $this->komunitas_lib->get_user($select, $where);

        if($users->num_rows() > 0)
        {
            $this->user = $users->row_array(0);
            if($this->user['account_type'] != 0){
                show_404();
            }

        }else{
            show_404();
        }
    }

    function index($id='')
    {
        if(empty($id))
            show_404();

        $this->isLogin();
//        if($this->isLogin(true) == false){
////            $data['rcode'] = 'bad';
////            $data['msg'] = 'Anda harus login !';
////            $this->message($data);
//            show_404();
//        }
        $user = $this->user;

        // check if friend
        if($user['user_id'] != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if(intval($is_friend['total']) == 0){
                $this->session->set_flashdata('msg', 'tambah sebagai teman untuk dapat melihat wall '.$user['display_name']);
                $rui = base_url().'komunitas/profile/'.$user['user_id'];
//                redirect($rui, 'refresh');
                header('location:'.$rui);
                return;
            }
        }

        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.js');
        $data['category'] = 'komunitas';
        $data['topic_last_activity'] = 'bijak';


        $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
        $where = array('tf.account_id' => $user['account_id']);
        $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
        $user['friend'] = $friend->result_array();

        $data['user'] = $user;

        $limit = 9;

        $page = 6;

        $offset = ($limit*$page) - $limit;
        if($offset != 0) $offset += ($page - 1);
//        $data['wall_list'] = $this->get_wall_list($user['account_id'], $user['user_id'], $limit, $offset);

        $html['html']['content']  = $this->load->view('komunitas_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);


    }

    public function wall($page_id='')
    {
        if($this->isLogin(true) == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $limit = 9;
        $account_id = $this->input->post('id');
        $page = $this->input->post('page');
        if(empty($account_id) || is_bool($account_id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){
            $offset += (intval($page) - 1);
        }
        $this->isBijaksUser($page_id[0]);
        $data['user'] = $this->user;
        $data['wall_list'] = $this->get_wall_list($account_id, $page_id[0], $limit, $offset);

        $this->load->view('template/komunitas/tpl_komunitas_wall', $data);

    }

    private function get_wall_list($account_id, $page_id, $limit, $offset)
    {
        if($offset > 1000){
            $limit += 1;
            $data = $this->komunitas_lib->get_wall_list($account_id, $page_id, $limit, $offset);
            $result = array();
            foreach($data->result_array() as $row=>$val){
                if(intval($val['content_group_type']) == 40){
                    $foto_list = $this->komunitas_lib->get_foto_album($val['content_id']);
                    $val['photo'] = $foto_list->result_array();
                }
                $result[$row] = $val;
            }
            return $result;
        }else{
            $result = array();
            $last_key = $this->redis_slave->get('wall:user:key:'.$account_id);

            $zrange = $limit + $offset;
            $list_key = 'wall:user:list:'.$last_key.':'. $account_id;
            $list_len = $this->redis_slave->lLen($list_key);
            if ($zrange > intval($list_len)){
                $zrange = $list_len;
            }

            if($offset > intval($list_len))
                return $result;

            $list_wall = $this->redis_slave->lRange($list_key, $offset, $zrange);

            //try get list from mysql and detail from redis
//            $list_wall = $this->komunitas_lib->get_wall_list_content_id($account_id, $page_id, $limit, $offset);

            $content_key = 'wall:user:detail:';
            foreach($list_wall as $row=>$val)
            {
                $content = $this->redis_slave->get($content_key.$val);
                $result[$row] = json_decode($content, true);
            }
            return $result;

        }
    }

    function profile($id)
    {
        $this->isLogin();
        $user_id = $id[0];
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.profile.js');
        $data['category'] = 'profile';
        $data['topic_last_activity'] = 'bijak';

        $this->isBijaksUser($user_id);
        $user = $this->user;

        // check if friend
        $data['is_teman'] = false;
        if($user['user_id'] != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if($is_friend['total'] > 0) $data['is_teman'] = true;
        }

        //check if follow
        $data['is_follow'] = false;
        if($id != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = array('account_id' => $this->member['account_id'], 'page_id' => $user['user_id']);
            $dt = $this->komunitas_lib->is_follow($select, $where)->row_array();
            if($dt['total'] > 0) $data['is_follow'] = true;
        }

        $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
        $where = array('tf.account_id' => $user['account_id']);
        $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
        $user['friend'] = $friend->result_array();

        $where = array('page_id' => $user['user_id']);
        $info_address = $this->komunitas_lib->get_info_address('*', $where);
        $data['address'] = array();
        if($info_address->num_rows() > 0){
            $data['address'] = $info_address->row_array(0);
        }

        $data['user'] = $user;

        $html['html']['content']  = $this->load->view('mobile_profile', $data, true);
//        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
//        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
//        $this->load->view('template/tpl_one_column', $html);
        $this->load->view('m_tpl/layout', $html);
    }

    function follow($id)
    {
        $this->isLogin();

        $user_id = $id[0];
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.follow.js');
        $data['category'] = 'follow';
        $data['topic_last_activity'] = 'bijak';

        $this->isBijaksUser($user_id);

        $user = $this->user;

        // check if friend
        if($user['user_id'] != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if(intval($is_friend['total']) == 0){
                $this->session->set_flashdata('msg', 'tambah sebagai teman untuk dapat melihat follower '.$user['display_name']);
                $rui = base_url().'komunitas/profile/'.$user['user_id'];
                redirect($rui);
            }
        }

        $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
        $where = array('tf.account_id' => $user['account_id']);
        $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
        $user['friend'] = $friend->result_array();

        $data['user'] = $user;

        $html['html']['content']  = $this->load->view('follow_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);

    }

    function friends($id='')
    {
        $this->isLogin();
        $user_id = $id[0];
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.friend.js');
        $data['category'] = 'friends';
        $data['topic_last_activity'] = 'bijak';

        $this->isBijaksUser($user_id);

        $user = $this->user;

        // check if friend
        if($user['user_id'] != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if(intval($is_friend['total']) == 0){
                $this->session->set_flashdata('msg', 'tambah sebagai teman untuk dapat melihat teman '.$user['display_name']);
                $rui = base_url().'komunitas/profile/'.$user['user_id'];
                redirect($rui);
            }
        }

        $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
        $where = array('tf.account_id' => $user['account_id']);
        $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
        $user['friend'] = $friend->result_array();

        $data['user'] = $user;


        $html['html']['content']  = $this->load->view('friend_index', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);

    }

    function blog($dt='')
    {
        if(count($dt) == 1){
            // $this->isLogin();
            $user_id = $dt[0];
            $data['title'] = "Bijaks | Life & Politics";
            $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.blog.js');
            $data['category'] = 'blogs';
            $data['topic_last_activity'] = 'bijak';

            $this->isBijaksUser($user_id);

            $user = $this->user;

            // check if friend
//            if($user['user_id'] != $this->member['user_id']){
//                $select = 'count(*) as total';
//                $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
//                $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
//                if(intval($is_friend['total']) == 0){
//                    $rui = base_url().'komunitas/profile/'.$user['user_id'];
//                    redirect($rui);
//                }
//            }

            $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
            foreach($count_post->result_array() as $val){
                $user['count_'.$val['tipe']] = $val['total'];
            }
            $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
            $where = array('tf.account_id' => $user['account_id']);
            $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
            $user['friend'] = $friend->result_array();

            $data['user'] = $user;

            // $html['html']['content']  = $this->load->view('blog_index', $data, true);
            // $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
            // $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
            // $this->load->view('template/tpl_one_column', $html);

            $html['html']['content']  = $this->load->view('blog_index', $data, true);
            $this->load->view('m_tpl/layout', $html);

        }elseif(count($dt) == 3){
            $user_id = $dt[0];
            $content_id = $dt[1];
            $content_url = $dt[2];

            $data['title'] = "Bijaks | Life & Politics";
            $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.blog.js');
            $data['category'] = 'blogs';
            $data['topic_last_activity'] = 'bijak';

            $this->isBijaksUser($user_id);

            $user = $this->user;
            $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
            foreach($count_post->result_array() as $val){
                $user['count_'.$val['tipe']] = $val['total'];
            }
            $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
            $where = array('tf.account_id' => $user['account_id']);
            $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
            $user['friend'] = $friend->result_array();

            $data['user'] = $user;

            $blog = $this->get_blog_list($content_id, $user['account_id'], 1, 0, 1);

            if(count($blog) == 0){
                show_404();
            }

            $data['val'] = $blog[0];

            $html['html']['content']  = $this->load->view('komunitas/mobile/blog_detail', $data, true);
            $this->load->view('m_tpl/layout', $html);

        }elseif(count($dt) == 2){
            if($this->isLogin(true) == false){
                $data['rcode'] = 'bad';
                $data['msg'] = 'Anda harus login !';
                $this->message($data);
            }

            $user_id = $dt[0];
            $this->isBijaksUser($user_id);

            $user = $this->user;

            if($this->member['account_id'] != $user['account_id'] || $this->member['user_id'] != $user['user_id'])
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'Anda harus login !';
                $this->message($data);
            }

            $this->tambah_blog($user);
        }elseif(count($dt) == 4){
            if($this->isLogin(true) == false){
                $data['rcode'] = 'bad';
                $data['msg'] = 'Anda harus login !';
                $this->message($data);
            }

            $user_id = $dt[0];
            $id = $dt[2];
            $this->isBijaksUser($user_id);

            $user = $this->user;

            if($this->member['account_id'] != $user['account_id'] || $this->member['user_id'] != $user['user_id'])
            {
                $data['rcode'] = 'bad';
                $data['msg'] = 'Anda harus login !';
                $this->message($data);
            }

            $this->edit_blog($user, $id);
        }
    }

    private function edit_blog($user, $id)
    {
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.blog');
        $data['category'] = 'blogs';
        $data['topic_last_activity'] = 'bijak';

        $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
        $where = array('tf.account_id' => $user['account_id']);
        $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
        $user['friend'] = $friend->result_array();

        $data['user'] = $user;


        // CHECK IF OWN BLOG
        $blog = $this->get_blog_list($id, $user['account_id'], 0, 0, 2);

        if(count($blog) == 0){
            show_404();
        }

        $data['blog'] = $blog[0];

        $html['html']['content']  = $this->load->view('blog_tambah', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);
    }

    private function tambah_blog($user)
    {
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.blog');
        $data['category'] = 'blogs';
        $data['topic_last_activity'] = 'bijak';

        $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
        $where = array('tf.account_id' => $user['account_id']);
        $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
        $user['friend'] = $friend->result_array();

        $data['user'] = $user;

        $html['html']['content']  = $this->load->view('blog_tambah', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);
    }

    function photo($dt)
    {
        $this->isLogin();
        if(count($dt) == 1){

            $user_id = $dt[0];
            $data['title'] = "Bijaks | Life & Politics";
            $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.photo.js');
            $data['category'] = 'album';
            $data['topic_last_activity'] = 'bijak';

            $this->isBijaksUser($user_id);

            $user = $this->user;
            // check if friend
            if($user['user_id'] != $this->member['user_id']){
                $select = 'count(*) as total';
                $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
                $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
                if(intval($is_friend['total']) == 0){
                    $this->session->set_flashdata('msg', 'tambah sebagai teman untuk dapat melihat photo '.$user['display_name']);
                    $rui = base_url().'komunitas/profile/'.$user['user_id'];
                    redirect($rui);
                }
            }

            $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
            foreach($count_post->result_array() as $val){
                $user['count_'.$val['tipe']] = $val['total'];
            }
            $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
            $where = array('tf.account_id' => $user['account_id']);
            $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
            $user['friend'] = $friend->result_array();

            $data['user'] = $user;

            $photo_profile = $this->komunitas_lib->get_foto_profile($user['account_id'], $user['user_id'], 0, 0);
            $result = array();
            foreach($photo_profile->result_array() as $row=>$val){
                $photo = $this->komunitas_lib->get_foto_album($val['content_id']);
                $val['count'] = $photo->num_rows();
                $val['photo'] = $photo->result_array();
                $result[$row] = $val;
            }

            $data['profile_pic'] = $result;

            $html['html']['content']  = $this->load->view('album_index', $data, true);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
            $this->load->view('template/tpl_one_column', $html);

        }elseif(count($dt) == 3){
            $user_id = $dt[0];
            $album_id = $dt[1];

            $data['title'] = "Bijaks | Life & Politics";
            $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.photo.js');
            $data['category'] = 'album';
            $data['topic_last_activity'] = 'bijak';

            $this->isBijaksUser($user_id);

            $user = $this->user;
            $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
            foreach($count_post->result_array() as $val){
                $user['count_'.$val['tipe']] = $val['total'];
            }
            $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
            $where = array('tf.account_id' => $user['account_id']);
            $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
            $user['friend'] = $friend->result_array();

            $data['user'] = $user;

            $data['album_id'] = $album_id;

            $select = '*';
            $where = array('content_id' => $album_id, 'account_id' => $user['account_id'], 'page_id' => $user['user_id']);
            $cnt = $this->timeline_lib->get_content($select, $where);
            if($cnt->num_rows() == 0){
                show_404();
            }

            $data['album'] = $cnt->row_array(0);

//            $photo = $this->komunitas_lib->get_foto_album($album_id);
//            $data['photo'] = $photo->result_array();

            $html['html']['content']  = $this->load->view('foto_index', $data, true);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
            $this->load->view('template/tpl_one_column', $html);
        }elseif(count($dt) == 2){
            $user_id = $dt[0];
            $album_id = $dt[1];

            $this->isBijaksUser($user_id);

            $user = $this->user;

            $this->edit_album($user);
        }
    }

    function profile_picture()
    {
        $this->isLogin();
        $photo_profile = $this->komunitas_lib->get_foto_profile($this->member['account_id'], $this->member['user_id'], 0, 0);
        $result = array();
        foreach($photo_profile->result_array() as $row=>$val){
            $photo = $this->komunitas_lib->get_foto_album($val['content_id']);
            $val['count'] = $photo->num_rows();
            $val['photo'] = $photo->result_array();
            $result[$row] = $val;
        }

        $data['profile_pic'] = $result;

        $this->load->view('profile_picture_frame', $data);

    }

    private function edit_album($user=array())
    {
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js', 'bijaks.side.js', 'bijaks.komunitas.photo.js');
        $data['category'] = 'album';
        $data['topic_last_activity'] = 'bijak';

        $count_post = $this->komunitas_lib->get_count_post($user['account_id'], $user['user_id']);
        foreach($count_post->result_array() as $val){
            $user['count_'.$val['tipe']] = $val['total'];
        }
        $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
        $where = array('tf.account_id' => $user['account_id']);
        $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
        $user['friend'] = $friend->result_array();

        $data['user'] = $user;

        $html['html']['content']  = $this->load->view('album_edit', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);
    }

    function album_list($dt)
    {
        if(count($dt) != 3){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = $dt[0];
        $tipe = $dt[1];
        $page = $dt[2];

        $this->isBijaksUser($id);

        $user = $this->user;
        $data['user'] = $user;
        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        $photo = $this->get_album_list($id, $user['account_id'], $limit, $offset);

        $data['photo'] = $photo;
        $data['count_result'] = count($photo);
        $this->load->view('template/komunitas/tpl_komunitas_album', $data);
    }

    private function get_album_list($id, $account_id, $limit=1, $offset, $tipe=0)
    {
        $result = array();
        $album = $this->komunitas_lib->get_foto_group($account_id, $id, $limit, $offset);
        foreach($album->result_array() as $row=>$val){
            $photo = $this->komunitas_lib->get_foto_album($val['content_id']);
            $val['count'] = $photo->num_rows();
            $val['photo'] = $photo->result_array();
            $result[$row] = $val;
        }
        return $result;
    }

    function foto_list($dt)
    {
        if(count($dt) != 3){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = $dt[0];
        $cid = $dt[1];
        $page = $dt[2];

        $this->isBijaksUser($id);

        $user = $this->user;
        $data['user'] = $user;
        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;

        $photo = $this->komunitas_lib->get_foto_album($cid, $limit, $offset);

        $data['photo'] = $photo->result_array();
        $this->load->view('template/komunitas/tpl_komunitas_foto', $data);

    }

    function foto_modal($dt)
    {
        $id = $dt[0];
        $cid = $dt[1];

        $this->isBijaksUser($id);

        $user = $this->user;
//        $data['user'] = $user;

        $photo = $this->komunitas_lib->get_foto_list($cid);
        if($photo->num_rows == 0){
            $dat['rcode'] = 'bad';
            $dat['message'] = 'content not found';
            header('application/json');
            echo json_encode($dat);
            exit;
        }
        $dat['rcode'] = 'ok';
        $dat['message'] = $photo->row_array(0);
        $dat['message']['image_uri'] = large_url($dat['message']['attachment_title']);
        header('application/json');
        echo json_encode($dat);

    }

    public function blog_list($dt)
    {
        if(count($dt) != 3){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = urldecode($dt[0]);
        $tipe = $dt[1];
        $page = $dt[2];

        $this->isBijaksUser($id);

        $user = $this->user;
        $data['user'] = $user;
        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        $blog = $this->get_blog_list($id, $user['account_id'], $limit, $offset);

        $data['blogs'] = $blog;
        $data['count_result'] = count($blog);
        $this->load->view('template/komunitas/tpl_komunitas_blog', $data);

    }

    private function get_blog_list($id, $account_id, $limit=1, $offset, $tipe=0)
    {
        $select = 'tc.`content_id`, tc.`account_id`, tc.`page_id`, tc.`title`, tc.`description`, tc.`entry_date`, tb.content_blog';

        if($tipe == 0){
            $where = array(
                'tc.content_group_type' => 11,
                'tc.`account_id`' => $account_id,
                'tc.page_id' => $id,
                'tc.status' => 0
            );
        }elseif($tipe == 1){
            $where = array(
                'tc.content_id' => $id,
                'tc.`account_id`' => $account_id,
                'tc.status' => 0
            );
        }elseif($tipe == 2){
            $where = array(
                'tc.content_id' => $id,
                'tc.`account_id`' => $account_id,
                'tc.page_id' => $this->member['user_id'],
                'tc.status' => 0
            );
        }
        $blogs = $this->komunitas_lib->get_blog($select, $where, $limit, $offset);
        $result = array();
        foreach($blogs->result_array() as $row=>$val)
        {
            $select = 'tc.content_id, tat.`attachment_title`';
            $where = array('tc.content_group_type' => 59, 'tc.group_content_id' => $val['content_id']);
            $blog_photo = $this->komunitas_lib->get_blog_photo($select, $where, 1, 0);
            $val['photo'] = $blog_photo->result_array();
            $result[$row] = $val;
        }

        return $result;
    }

    function follower_list($dt)
    {
        if($this->isLogin(true) == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        if(count($dt) != 4){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = $dt[0];
        $acc_id = $dt[1];
        $tipe = $dt[2];
        $page = $dt[3];

        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $this->isBijaksUser($id);
        $data['user'] = $this->user;

        $offset = ($limit*intval($page)) - $limit;
        $fl_list = array();
        if(intval($tipe) == 1){
            $follow_lembaga = $this->komunitas_lib->get_follow_politisi($acc_id, $limit, $offset);
            $fl_list = $follow_lembaga->result_array();
        }elseif(intval($tipe) == 2){
            $follow_lembaga = $this->komunitas_lib->get_follower($id, $limit, $offset);
            $fl_list = $follow_lembaga->result_array();
        }elseif(intval($tipe) == 3){
            $follow_lembaga = $this->komunitas_lib->get_follow($acc_id, $limit, $offset);
            $fl_list = $follow_lembaga->result_array();
        }
        $data['tipe'] = $tipe;
        $data['follow'] = $fl_list;
        $this->load->view('template/komunitas/tpl_komunitas_follow', $data);

    }

    function change_password()
    {
        $this->isLogin();
        $this->load->library('user_agent');
        $referal = $this->agent->referrer();
        $this->load->library('form_validation');

        $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim|xss_clean');
        $this->form_validation->set_rules('new_password', 'New Password', 'required|trim|xss_clean|min_length[6]');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|trim|xss_clean|min_length[6]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('current_password', form_error('current_password'));
            $this->session->set_flashdata('new_password', form_error('new_password'));
            $this->session->set_flashdata('confirm_password', form_error('confirm_password'));
            $this->session->set_flashdata('rcode', 'bad');
            $this->session->set_flashdata('msg', 'Isian tidak lengkap');

            redirect($referal, 'refresh');
        }

        $current_password = $this->input->post('current_password');
        $new_password = $this->input->post('new_password');
        $confirm_password = $this->input->post('confirm_password');

        //check current password
        $select = 'ta.email, ta.passwd, ta.last_change_pwd, ta.salt';
        $where = array('account_id' => $this->member['account_id']);
        $usr = $this->komunitas_lib->get_user($select, $where);
        if(intval($usr->num_rows()) == 0){
            $this->session->set_flashdata('rcode', 'bad');
            $this->session->set_flashdata('msg', 'Account tidak di temukan');

            redirect($referal, 'refresh');
        }
        $usr_array = $usr->row_array(0);

//        //check interval
//        $dt_time = strtotime($usr_array['last_change_pwd']);
//        $last_minute = mdate('%i', $dt_time);
//        $now_minute = now();
//        var_dump($dt_time);
//        var_dump($now_minute);

        // check true passwor
        $sl_mt = $this->_generatePassword($current_password, $usr_array['salt']);
        if($sl_mt != $usr_array['passwd']){
            $this->session->set_flashdata('rcode', 'bad');
            $this->session->set_flashdata('msg', 'Account tidak di temukan');

            redirect($referal, 'refresh');
        }


        if($new_password != $confirm_password){
            $this->session->set_flashdata('rcode', 'bad');
            $this->session->set_flashdata('msg', 'Isian tidak lengkap');
            redirect($referal, 'refresh');
        }

        $max = 14;
        $salt = $this->_generateSalt($max);
        $n_pa = $this->_generatePassword($new_password, $salt);
        $data_ar = array(
            'passwd' => $n_pa,
            'last_change_pwd' => mysql_datetime(),
            'salt' => $salt
        );

        $where = array('account_id' => $this->member['account_id']);

        $this->komunitas_lib->tusr_account($data_ar, $where);

        $mail_data = array('name' => $this->member['username']);

        $this->load->config();
        $this->config->load('mail');
        $mail_config = $this->config->item('mail');
        $this->load->library('email', $mail_config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreply.balepoint@gmail.com', 'Bijak Admin');
        $this->email->to($usr_array['email']);

        $this->email->subject('Bijak Password Changed');


        $this->email->message($this->load->view('email/pass_changed', $mail_data, true));
        try
        {
            $this->email->send();
        } catch (Exception $e)  {

        }
        $this->session->set_flashdata('rcode', 'ok');
        $this->session->set_flashdata('msg', 'Berhasil merubah password');
        redirect($referal, 'refresh');
    }

    function friend_list($dt)
    {
        if($this->isLogin(true) == false){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }
        if(count($dt) != 3){
            echo '<p>Tidak ada user</p>';
            return;
        }

        $id = $dt[0];
        $tipe = $dt[1];
        $page = $dt[2];

        $this->isBijaksUser($id);

        $user = $this->user;
        $data['user'] = $user;

        $limit = 10;
        if(empty($id)){
            echo '<p>Tidak ada teman</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        $fl_list = array();
        if(intval($tipe) == 1){
//            $select = 'tf.*,ta.user_id as page_id, ta.gender, ta.display_name as page_name, tp.about, tat.attachment_title';
////            $where = array('tf.account_id' => $user['account_id']);
//            $where = "tf.account_id = '".$user['account_id']."' or tf.friend_account_id = '".$user['account_id']."'";
//            $friend = $this->komunitas_lib->get_friend($select, $where, $limit, $offset);
            $friend = $this->komunitas_lib->get_friend_list($user['account_id'], $limit, $offset);
            $fl_list = $friend->result_array();
        }elseif(intval($tipe) == 2){
            $select = 'tf.*, ta2.current_city, ta2.gender, tp2.page_id AS page_id, tp2.page_name AS page_name, tp2.about, tat2.attachment_title';
            $where = array('tf.to_account_id' => $user['account_id'], 'tf.flag' => 0);
            $friend_request = $this->komunitas_lib->get_friend_request($select, $where, $limit, $offset);
            $fl_list = $friend_request->result_array();
        }elseif(intval($tipe) == 3){
            $select = 'tf.*, ta.current_city, ta.gender, tp.page_id AS page_id, tp.page_name AS page_name, tp.about, tat.attachment_title';
            $where = array('tf.account_id' => $user['account_id'], 'tf.flag' => 0);
            $pending_friend_request = $this->komunitas_lib->get_friend_request($select, $where, $limit, $offset);
            $fl_list = $pending_friend_request->result_array();
        }
        $data['tipe'] = $tipe;
        $data['follow'] = $fl_list;
        $this->load->view('template/komunitas/tpl_komunitas_friend', $data);
    }

    function isLogin($return=false)
    {
        $cr = current_url();
        $this->member = $this->session->userdata('member');
        if(!$this->member['logged_in']){
            if($this->input->is_ajax_request()){
                return false;
            }else{
                if($return){
                    return false;
                }else{
                    redirect(base_url().'home/login?frm='.urlencode($cr));
                }
            }

        }

        return true;
        //exit('please');
    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    private function _generateSalt($max)
    {
        $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
        $i = 0;
        $salt = "";
        while ($i < $max) {
            $salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
            $i++;
        }

        return $salt;
    }

    private function _generatePassword($pass, $salt)
    {
        return crypt($pass, $salt);
    }

}