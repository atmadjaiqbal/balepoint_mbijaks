<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/profile.picture.frame.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" media="screen"/>

    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <script>
        $(function(){
            $('body').slimscroll({
                height: '420px'
            });

            $('.pp-pic').click(function(e){
                e.preventDefault();
                var id = $(this).data('id');
            });

        });
    </script>
</head>
<body>
<?php foreach($profile_pic as $row=>$val){ ?>
    <?php foreach($val['photo'] as $ros=>$phot){ ?>
        <?php if($ros % 6 == 0){ ?>
                    <div class="row-fluid">
                    <?php } ?>
        <div class="span2">
            <img data-id="<?=$phot['content_id'];?>" class="pp-pic" src="<?=thumb_url($phot['attachment_title']);?>">
        </div>
        <?php if($ros % 6 == 5 || ($ros == count($val['photo'])-1)){ ?>

                    </div>
                        <hr>
                    <?php } ?>
        <?php } ?>
    <?php } ?>
</body>
</html>