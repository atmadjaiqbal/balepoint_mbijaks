<div class="row">
    <div class="col-xs-12">
        <?php $this->load->view('m_tpl/komunitas_side2'); ?>
    </div>
</div>
    
    <h4 class="kanal-title kanal-title-gray">BLOG</h4>
<div class="row">
    <div class="col-xs-12 col-sm-12" id="blog-detail">
        <div class="blog-title"><?php echo $val['title'];?></div>
        <p><em><small><?php echo date('d/m/Y', strtotime($val['entry_date']));?></small></em></p>
        <?php
        $this->load->library("html_dom");
        if(!empty($val['content_blog']))
        {
            $html = str_get_html($val['content_blog']);
            $imgs = $html->find('img');
            $image = 'http://www.bijaks.net/assets/images/noimage.jpg'; $image = 'none';
            if(count($imgs) > 0)
            {
                $image = $imgs[0]->src;
            }
            $_imageblog = $image;
        } else { $_imageblog = 'http://www.bijaks.net/assets/images/noimage.jpg'; $_imageblog = 'none';}
        ?>
        <div style="width: 100%;height: auto;">
           <img src="<?php echo $_imageblog; ?>" style="width: 100%;height: auto;">
        </div>

        <p style="margin-top:5px;" class="blog-detail-content">
            <?php echo strip_tags($val['content_blog'],'<p>'); ?>
        </p>
    </div>
    <hr class="line-mini">

</div>
