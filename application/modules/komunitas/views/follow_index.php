<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span12"><h3>Follows</h3></div>
                </div>
                <br>

                <ul class="nav nav-tabs" id="followTab">
                    <li class="active"><a data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="1" href="#lembaga">POLITISI/LEMBAGA</a></li>
                    <li><a data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="2" href="#follower">FOLLOWER</a></li>
                    <li><a data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="3" href="#follow">FOLLOW</a></li>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="lembaga">
                        <div data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="1" id="lembaga_list"></div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a data-page="2" data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="1" id="lembaga_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="follower">
                        <div data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="2" id="follower_list"></div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a data-page="2" data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="2" id="follower_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="follow">
                        <div data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="3" id="follow_list"></div>
                        <div class="row-fluid komu-follow-list komu-wall-list-bg">
                            <div class="span12 text-center">
                                <a data-page="2" data-id="<?=$user['account_id'];?>" data-user='<?=$user['user_id'];?>' data-tipe="3" id="follow_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>