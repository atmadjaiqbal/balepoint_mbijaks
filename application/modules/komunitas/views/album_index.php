<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php $this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span8"><h3>Foto Album</h3></div>
                    <div class="span4 text-right">
                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <a href="<?=base_url().'komunitas/photo/'.$user['user_id'].'/tambah';?>" class="btn-flat btn-flat-large btn-flat-dark">BUAT ALBUM</a>
                        <?php } ?>
                    </div>
                </div>
                <br>
                <?php foreach($profile_pic as $row=>$val){ ?>
                <?php if($row == 0){ ?>
                <div class="row-fluid">
                    <div class="span6">

                        <span class="komu-photo-profile"><?=(strlen($val['title']) > 16) ? substr(ucwords($val['title']),0, 16) : ucwords($val['title']);?></span>
                        <span class="komu-photo-count"><?=$val['count'];?> Photos</span>

                    </div>
                    <div class="span6">

                    </div>
                </div>
                <div class="div-line-small"></div>
                <?php } ?>


                    <?php foreach($val['photo'] as $ros=>$phot){ ?>
                    <?php if($ros % 4 == 0){ ?>
                    <div class="row-fluid">
                    <?php } ?>
                        <div class="span3">
                        <img src="<?=thumb_url($phot['attachment_title']);?>">
                        </div>
                        <?php if($ros % 4 == 3 || ($ros == count($val['photo'])-1)){ ?>

                    </div>
                    <?php } ?>
                    <?php } ?>
                <?php } ?>

                <div class="div-line-small"></div>
                <div id="album_container" data-user="<?=$user['user_id'];?>" data-tipe="1" data-page='1'>

                </div>
                <br>
                <div class="row-fluid komu-wall-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a id="photo_load_more" class="komu-wall-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

