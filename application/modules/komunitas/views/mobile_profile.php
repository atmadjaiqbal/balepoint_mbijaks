
<?php $this->load->view('m_tpl/komunitas_side'); ?>
<div class="row">
    
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <h4 class="kanal-title kanal-title-gray"><a href="<?php echo base_url('home/logout');?>">LOGOUT</a></h4>
            </div>    
        </div>

        <div class="row">
            <div class="col-xs-12">
                <h4 class="kanal-title kanal-title-gray">Informasi Dasar</h4>
            </div>    
        </div>
        
        
        <form id="edit_form_dasar" action="">
            <input type="hidden" name='id' value="<?=$user['user_id'];?>"/>
            <div class="row">
                <div class="col-xs-12">
                    <input placeholder="Nama Depan" name="fname" type="text" value="<?=$user['fname'];?>" class="inputlogin informasiDasar" id="fname_id">
                </div>
                <div class="col-xs-12">
                    <input placeholder="Nama Belakang" name="lname" type="text" class="inputlogin informasiDasar" value="<?=$user['lname'];?>" id="lname_id">
                </div>
                <div class="col-xs-12">
                    <select name="gender" class="inputlogin informasiDasar">
                        <option value="M" <?=($user['gender'] == 'M') ? 'checked':'';?>>Laki Laki</option>
                        <option value="F" <?=($user['gender'] == 'F') ? 'checked':'';?>>Perempuan</option>
                    </select>
                </div>
                <div class="col-xs-12">
                    <input placeholder="Tanggal Lahir(format: YYYY-mm-dd)" id="dob" class="inputlogin informasiDasar" name="birthday" type="text" value="<?=$user['birthday'];?>">
                </div>
                <div class="col-xs-12">
                    <input placeholder="Lokasi" name="current_city" type="text" class="inputlogin informasiDasar" value="<?=$user['current_city'];?>" id="current_city_id">
                </div>
                <div class="col-xs-12">
                    <textarea placeholder="About" rows="3" class="inputlogin informasiDasar" name="about" id="about_id"><?=$user['about'];?></textarea>
                </div>
            </div>
            <?php if($this->member && ($user['user_id'] == $this->member['user_id'])){ ?>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-light" style="width: 100%;">
                </div>
            </div>
            <?php };?>
        </form>
        <div class="row">
            <div class="col-xs-12">
                <h4 class="kanal-title kanal-title-gray">Informasi Kontak</h4>
            </div>    
        </div>
        <form id="edit_form_kontak" action="">
        <input type="hidden" name='id' value="<?=(!empty($address['user_id']))? $user['user_id']:'0';?>">
            <div class="row">
                <div class="col-xs-12">
                    <input placeholder="Alamat" name="address" type="text" class="inputlogin informasiKontak" value="<?=(!empty($address['address']))? $address['address'] : '';?>" id="alamat_id">
                </div>
                <div class="col-xs-12">
                    <input placeholder="Kota" name="city_state" type="text" class="inputlogin informasiKontak" value="<?=(!empty($address['address']))? $address['city_state'] : '';?>" id="city_state_id">                
                </div>
                <div class="col-xs-12">
                    <input placeholder="Propinsi" name="province_state" type="text" class="inputlogin informasiKontak" value="<?=(!empty($address['address']))? $address['province_state'] : '';?>" id="province_state_id">
                </div>
                <div class="col-xs-12">
                    <input placeholder="Negara" name="country" type="text" class="inputlogin informasiKontak" value="<?=(!empty($address['address']))? $address['country'] : '';?>" id="country_id">
                </div>
                <div class="col-xs-12">
                    <input placeholder="Kode Pos" name="postalcode" type="text" class="inputlogin informasiKontak" value="<?=(!empty($address['address']))? $address['postalcode'] : '';?>">
                </div>
                <?php if($this->member && ($user['user_id'] == $this->member['user_id'])){ ?>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-light" style="width: 100%;">
                    </div>
                </div>
                <?php };?>
            </div>
        </form>
        <div class="row">
            <div class="col-xs-12">
                <h4 class="kanal-title kanal-title-gray">Ganti Password</h4>
            </div>    
        </div>
        <form id="change_password" action="<?=base_url('komunitas/change_password');?>" method="post">
        <input type="hidden" name='id' value="<?=$user['user_id'];?>">
            <div class="row">
                <div class="col-xs-12">
                    <input placeholder="Current Password" name="current_password" type="password" class="inputlogin chpass">
                </div>
                <div class="col-xs-12">
                    <input placeholder="New password (min 6 karakter)" name="new_password" type="password" class="inputlogin chpass">
                </div>
                <div class="col-xs-12">
                    <input placeholder="Confirm Password (min 6 karakter)" name="confirm_password" type="password" class="inputlogin chpass" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <input type="reset" value="BATAL" class="btn-flat btn-flat-large btn-flat-dark chpass mtop1">
                </div>
                <div class="col-xs-6">
                    <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-dark chpass mtop1">
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
<?php 
if($this->member && ($user['user_id'] == $this->member['user_id'])){
    echo '$(".informasiDasar").attr("disabled",false);';
    echo '$(".informasiKontak").attr("disabled",false);';
}else{
    echo '$(".informasiDasar").attr("disabled",true);';
    echo '$(".informasiKontak").attr("disabled",true);';
};
?>

<?php if($this->member && ($user['user_id'] == $this->member['user_id'])){ ?>
$('#edit_form_dasar').submit(function(){
    var data = $(this).serialize();
    var fname = $('#fname_id').val();
    var lname = $('#lname_id').val();
    var dob = $('#dob').val();
    var current_city = $('#current_city').val();
    var about_id = $('#about_id').val();
    if(fname != '' && lname != '' && dob != '' && current_city != ''){
        $.post(Settings.base_url+'timeline/post_dasar', data, function(res){
            if(res.rcode == 'ok'){
                location.reload();
            };
            location.reload();
        });
        return false;

    }else{
        alert('Harap pastikan bahwa data tersebut terisi: Nama Depan, Nama Belakang, Tanggal Lahir, Lokasi');
        return false;
    }
})

$('#edit_form_kontak').submit(function(){
    var data = $(this).serialize();
    var alamat = $('#alamat_id').val();
    var city_state = $('#city_state_id').val();
    var province_state = $('#province_state_id').val();
    var country = $('#country_id').val();
    if(alamat != '' && city_state != '' && province_state != '' && country != ''){
        $.post(Settings.base_url+'timeline/post_kontak', data, function(res){
            if(res.rcode == 'ok'){
                location.reload();
            };
            location.reload();
        });
        return false;

    }else{
        alert('Harap pastikan bahwa data tersebut terisi: Alamat, Kota, Provinsi, Negara');
        return false;
    }    
})

<?php };?>
</script>
<?php /*
--------------------------------------
<div class="container">
    <?php //$this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <!-- LEFT SIDE BAR -->
            <div class="span4">
                <?php //$this->load->view('template/komunitas/tpl_komunitas_side'); ?>
            </div>

            <!-- KOMUNITAS KONTENT -->
            <div class="span8">
                <!-- TITTLE -->
                <div class="row-fluid komu-wall-title">
                    <div class="span5"><h3>Profile</h3></div>
                    <div class="span7 text-right">
                        <?php if(is_array($this->member)){?>
                        <?php if($user['user_id'] != $this->member['user_id']){ ?>
                        <a data-tipe="<?=($is_teman) ? '1' : '2';?>" data-id="<?=$this->member['account_id'];?>" data-user="<?=$user['account_id'];?>" id="teman" href="#" class="btn-flat btn-flat-large btn-flat-dark"> <?=(!$is_teman) ? 'TAMBAH TEMAN <i class="icon-ok icon-white"></i>' : 'HAPUS TEMAN <i class="icon-remove icon-white"></i>';?></a>
                        <a data-tipe="<?=($is_follow) ? '1' : '2';?>" data-id="<?=$user['user_id'];?>" id="follow_user" href="#" class="btn-flat btn-flat-large btn-flat-dark"><?=(!$is_follow) ? 'FOLLOW <i class="icon-ok icon-white"></i>' : 'UNFOLLOW <i class="icon-remove icon-white"></i>';?></a>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <br>
                <?php if($this->session->flashdata('msg')){ ?>
                <div class="alert alert-info fade in">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?=$this->session->flashdata('msg');?>
                </div>
                <?php } ?>
                <?php if($this->session->flashdata('rcode')){?>
                <div class="alert alert-error fade in">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <?=$this->session->flashdata('msg');?>
                    <?=$this->session->flashdata('current_password');?>
                    <?=$this->session->flashdata('new_password');?>
                    <?=$this->session->flashdata('confirm_password');?>
                </div>
                <?php } ?>
                <?php $abot = trim($user['about']); if(!empty($abot)){?>
                <p class=""><em><?=$abot;?></em></p>
                <?php } ?>
                <div class="row-fluid" id="info">
                    <div class="span6">
                        <h4>Informasi Dasar</h4>
                        <div id="info_dasar" class="">
                        <?php if(!empty($user['fname'])){?>
                            <p class="komu-profile-info"><?=$user['fname'];?></p>
                        <?php }else{ ?>
                            <p class="komu-profile-info-null"><em>First Name</em></p>
                        <?php } ?>

                        <?php if(!empty($user['lname'])){?>
                            <p class="komu-profile-info"><?=$user['lname'];?></p>
                        <?php }else{ ?>
                            <p class="komu-profile-info-null"><em>Last Name</em></p>
                        <?php } ?>

                        <?php if(!empty($user['gender'])){?>
                            <p class="komu-profile-info"><?=($user['gender'] == 'M') ? 'Laki Laki' : 'Perempuan';?></p>
                        <?php }else{ ?>
                            <p class="komu-profile-info-null"><em>Jenis Kelamin</em></p>
                        <?php } ?>

                        <?php if(!empty($user['birthday'])){?>
                            <p class="komu-profile-info"><?=mdate('%d %M %Y', strtotime($user['birthday']));?></p>
                        <?php }else{ ?>
                            <p class="komu-profile-info-null"><em>Tanggal Lahir</em></p>
                        <?php } ?>

                        <?php if(!empty($user['current_city'])){?>
                            <p class="komu-profile-info"><?=$user['current_city'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Lokasi</em></p>
                        <?php } ?>

<!--                        --><?php //$abot = trim($user['about']); if(!empty($abot)){?>
<!--                        <p class="komu-profile-info">--><?//=$user['about'];?><!--</p>-->
<!--                        --><?php //}else{ ?>
<!--                        <p class="komu-profile-info-null"><em>Tentang</em></p>-->
<!--                        --><?php //} ?>

                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <a id="edit_dasar" class="btn-flat btn-flat-large btn-flat-dark pull-right">EDIT</a>
                        <?php } ?>
                        </div>

                        
                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <div id="edit_info_dasar" class="hide">
                            <form id="edit_form_dasar" action="">
                                <input type="hidden" name='id' value="<?=$user['user_id'];?>">
                                <input placeholder="nama depan" name="fname" type="text" class="span12 komu-profile-info" value="<?=$user['fname'];?>">
                                <input placeholder="nama belakang" name="lname" type="text" class="span12 komu-profile-info" value="<?=$user['lname'];?>">
                                <select name="gender" class="span12 komu-profile-info-select">
                                    <option value="M" <?=($user['gender'] == 'M') ? 'checked':'';?>>Laki Laki</option>
                                    <option value="F" <?=($user['gender'] == 'F') ? 'checked':'';?>>Perempuan</option>
                                </select>
                                <input placeholder="tanggal lahir" id="dob" name="birthday" type="text" class="span12 komu-profile-info" value="<?=$user['birthday'];?>">
                                <input placeholder="lokasi" name="current_city" type="text" class="span12 komu-profile-info" value="<?=$user['current_city'];?>">
                                <textarea placeholder="about" rows="3" class="span12 komu-profile-info" name="about"><?=$user['about'];?></textarea>

                                <div class="text-right">
                                <img id="dasar_loading" class="hide" src="<?=base_url('assets/images/loading.gif');?>">
                                <input id="batal_dasar" type="reset" value="BATAL" class="btn-flat btn-flat-large btn-flat-dark">
                                <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-dark">
                                </div>
                            </form>
                        </div>
                        <?php } ?>

                    </div>

                    <div class="span6">
                        <h4>Informasi Kontak</h4>
                        <div id="info_kontak">
                        <?php if(!empty($address['address'])){?>
                        <p class="komu-profile-info"><?=$address['address'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Alamat</em></p>
                        <?php } ?>

                        <?php if(!empty($address['city_state'])){?>
                        <p class="komu-profile-info"><?=$address['city_state'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Kota</em></p>
                        <?php } ?>

                        <?php if(!empty($address['province_state'])){?>
                        <p class="komu-profile-info"><?=$address['province_state'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Propinsi</em></p>
                        <?php } ?>

                        <?php if(!empty($address['country'])){?>
                        <p class="komu-profile-info"><?=$address['country'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Negara</em></p>
                        <?php } ?>

                        <?php if(!empty($address['postalcode'])){?>
                        <p class="komu-profile-info"><?=$address['postalcode'];?></p>
                        <?php }else{ ?>
                        <p class="komu-profile-info-null"><em>Kode Pos</em></p>
                        <?php } ?>

                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <a id="edit_kontak" class="btn-flat btn-flat-large btn-flat-dark pull-right">EDIT</a>
                        <?php } ?>
                        </div>

                        <?php if($user['user_id'] == $this->member['user_id']){ ?>
                        <div id="edit_info_kontak" class="hide">
                            <form id="edit_form_kontak" action="">
                                <input type="hidden" name='id' value="<?=(!empty($address['user_id']))? $user['user_id']:'0';?>">
                                <input placeholder="alamat" name="address" type="text" class="span12 komu-profile-info" value="<?=(!empty($address['address']))? $address['address'] : '';?>">

                                <input placeholder="kota" name="city_state" type="text" class="span12 komu-profile-info" value="<?=(!empty($address['address']))? $address['city_state'] : '';?>">
                                <input placeholder="propinsi" name="province_state" type="text" class="span12 komu-profile-info" value="<?=(!empty($address['address']))? $address['province_state'] : '';?>">
                                <input placeholder="negara" name="country" type="text" class="span12 komu-profile-info" value="<?=(!empty($address['address']))? $address['country'] : '';?>">
                                <input placeholder="kode pos" name="postalcode" type="text" class="span4 komu-profile-info" value="<?=(!empty($address['address']))? $address['postalcode'] : '';?>">


                                <div class="text-right">
                                    <img id="kontak_loading" class="hide" src="<?=base_url('assets/images/loading.gif');?>">
                                    <input id="batal_kontak" type="reset" value="BATAL" class="btn-flat btn-flat-large btn-flat-dark">
                                    <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-dark">
                                </div>
                            </form>
                        </div>
                        <?php } ?>

                    </div>
                </div>

                <?php if($user['user_id'] == $this->member['user_id']){ ?>
                <div class="row-fluid" id="">
                    <div class="span6">
                        <h4>Ganti Password</h4>
                        <form id="change_password" action="<?=base_url('komunitas/change_password');?>" method="post">
                            <input type="hidden" name='id' value="<?=$user['user_id'];?>">
                            <input placeholder="current password" name="current_password" type="password" class="span12 komu-profile-info" value="">
                            <input placeholder="new password (min 6 karakter)" name="new_password" type="password" class="span12 komu-profile-info" value="">
                            <input placeholder="confirm password (min 6 karakter)" name="confirm_password" type="password" class="span12 komu-profile-info" value="">
                            <div class="text-right">
                                <input id="" type="reset" value="BATAL" class="btn-flat btn-flat-large btn-flat-dark">
                                <input type="submit" value="SIMPAN" class="btn-flat btn-flat-large btn-flat-dark">
                            </div>
                        </form>
                    </div>
                </div>
                <?php } ?>


            </div>
        </div>
    </div>
</div>


<!--- ###### MODAL #######-->
<div id="modal_teman" class="modal hide fade">
    <div class="modal-body">
        <div class="row-fluid">
            <h4>Kirim pesan sebagai teman.</h4>
            <form id="submit_friend">
                <input name="id" type="hidden" value="" id="fid">
                <input id="report_message" type="text" name="message" class="media-input comment_type span12">

                <div class="pull-right">
                    <input type="reset" id="reste" value="Batal" class="btn-flat btn-flat-gray">
                    <input type="submit" id="report" value="Kirim" class="btn-flat btn-flat-gray">
                </div>

            </form>
        </div>
    </div>
</div>
*/?>
