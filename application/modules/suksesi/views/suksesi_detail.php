<?php
$suksesi_url = base_url() . 'suksesi/index/'.$suksesi['id_race'].'-'.urltitle($suksesi['race_name']);
$map_photo = '';
$logo_photo = '';
foreach($suksesi['photos'] as $foto){
    if(intval($foto['type']) === 2){
        if( file_exists('public/upload/image/suksesi/thumb/portrait/'.$foto['attachment_title'])){
            $map_photo = $foto['thumb_portrait_url'];
        } else {
            $map_photo = base_url(). 'assets/images/thumb/noimage.jpg';
        }

    }elseif(intval($foto['type']) === 1){

        if( file_exists('public/upload/image/suksesi/thumb/portrait/'.$foto['attachment_title'])){
            $logo_photo = $foto['thumb_portrait_url'];
        } else {
            $logo_photo = base_url(). 'assets/images/thumb/noimage.jpg';
        }
    }
}
$status_suksesi = 'Survey / Prediksi';
$index_trace_status = 0;
$jmlh_putaran = 0;
$status_text = array(1 => 'Survey / Prediksi', 2 => 'Putaran Pertama', 3 => 'Putaran Kedua');

foreach($suksesi['status'] as $idx => $status){
    if(intval($status['draft']) == 1){
//        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
        $index_trace_status = $idx;
        $jmlh_putaran = (intval($status['status']) == 1) ? 0 : (intval($status['status']) == 2) ? 1 : 2;

        if(intval($status['status']) == 1)
        {
            $status_suksesi = 'Survey / Prediksi';
            $_keterangan = "Survey";
        } else {
            if(intval($status['status']) == 2)
            {
                $status_suksesi = 'Putaran Pertama';
                $_keterangan = "1 Putaran";
            } else {
                $status_suksesi = 'Putaran Kedua';
                $_keterangan = "2 Putaran";
            }
        }
//        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
        $index_trace_status = $idx;
    }
}


$total_kandidat = count( $suksesi['status'][$index_trace_status]['kandidat'] );
$tabSection = "section-tab-".$suksesi['id_race'];
?>
<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <div class="span8">

                <div class="row-fluid">
                    <div class="span6">
                        <div class="row-fluid">
                            <div class="span3 suksesi-img-logo">
                                <img src="<?php echo $logo_photo; ?>">
                            </div>
                            <div class="span9">
                                <a href="<?php echo $suksesi_url; ?>"><h4 title="<?php echo $suksesi['race_name']; ?>"><?php echo $suksesi['race_name']; ?></h4></a>

                            </div>
                        </div>
                        <table class="table">
                            <tr>
                                <td class="suksesi-ket-place">
                                    <span>Tanggal pelaksanaan</span>
                                </td>
                                <td class="suksesi-ket-place">
                                    <span>: <?php echo mdate('%d %M %Y', strtotime($suksesi['tgl_pelaksanaan'])); ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="suksesi-ket-place">
                                    <span>Suksesi</span>
                                </td>
                                <td class="suksesi-ket-place">
                                    <span>: <?php echo $suksesi['kdname']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="suksesi-ket-place">
                                    <span>Daerah pemilihan</span>
                                </td>
                                <td class="suksesi-ket-place">
                                    <span>: <?php echo $suksesi['dname']; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="suksesi-ket-place">
                                    <span>Daftar pemilih tetap</span>
                                </td>
                                <td class="suksesi-ket-place">
                                    <span>: <?php echo $suksesi['daftar_pemilih_tetap']; ?> Jiwa</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="suksesi-ket-place">
                                    <span>Jumlah kandidat</span>
                                </td>
                                <td class="suksesi-ket-place">
                                    <span>: <?php echo $total_kandidat; ?></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="suksesi-ket-place">
                                    <span>Status</span>
                                </td>
                                <td class="suksesi-ket-place">
                                    <span>: <?php echo $status_suksesi; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="suksesi-ket-place">
                                    <span>Jumlah putaran</span>
                                </td>
                                <td class="suksesi-ket-place">
                                    <span>: <?php echo $jmlh_putaran; ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="suksesi-ket-place">
                                    <span>Keterangan</span>
                                </td>
                                <td class="suksesi-ket-place">
                                    <span>: <?php echo $_keterangan;?></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- carosel -->
                    <div class="span6">
                        <div id="myCarousel" data-interval="false" data-pause="hover" class="carousel-suksesi slide">

                            <!-- Carousel items -->
                            <div class="carousel-inner">
                                <?php foreach($suksesi['photos'] as $kes => $item){ ?>
                                <div class="<?php echo ($kes == 0) ? 'active' : ''; ?> item suksesi-carousel-image">
                                    <img src="<?php echo $item['thumb_landscape_url'] ;?>">
                                </div>
                                <?php } ?>

                            </div>
                            <div class="score-place score-place-overlay score" data-id="<?php echo $suksesi['content_id']; ?>"></div>
                            <div class="row-fluid suksesi-carousel-control-container">
                                <!--div class="suksesi-carousel-control-button">
                                    <!-- Carousel nav
                                    <a class="pull-left left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                                </div-->
                                <div class="suksesi-carousel-control-thumbs">
                                    <!-- Carousel thumb -->
                                    <ol class="suksesi-carousel-indicators">
                                        <?php $i=0;foreach($suksesi['photos'] as $kem => $item){ ?>
                                        <li data-target="#myCarousel" data-slide-to="<?php echo $kem; ?>" class="<?php echo ($kem == 0) ? 'active' : ''; ?>">
                                            <img class="img-polaroid" src="<?php echo $item['thumb_url'] ;?>">
                                        </li>
                                        <?php $i++;} ?>


                                        <?php
                                        if( $i < 2 || $i < 3 || $i < 4 || $i <  5)
                                        {
                                            for ($o = 1; $o <= (5 - $i); $o++) {
                                                ?>
                                                <li>
                                                    <img class="img-polaroid" src="<?php echo base_url(). 'assets/images/thumb/noimage.jpg';;?>">
                                                </li>
                                            <?php
                                            }
                                        }
                                        ?>



                                    </ol>




                                </div>
                                <!--div class="suksesi-carousel-control-button">
                                    <a class="suksesi-carousel-control pull-right right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                                </div-->
                            </div>
                        </div>
                    </div>

                </div>

                <!-- score -->

                <ul class="nav nav-tabs" id="suksesiTab">
                    <?php $idk = 0; ?>
                    <?php foreach($suksesi['status'] as $index=>$status){ ?>
                    <?php if(intval($status['draft']) == 1){ ?>

                    <li class="<?php echo ($idk == 0) ? 'active' : ''; ?>">
                        <a href="#suksesi_<?=$status['id_trace_status'];?>"><?=$status_text[intval($status['status'])];?></a>
                    </li>
                        <?php $idk++; } ?>
                    <?php } ?>

                </ul>

                <div class="tab-content">
                    <?php $imk = 0; ?>
                    <?php foreach($suksesi['status'] as $index=>$status){ ?>
                    <?php if(intval($status['draft']) == 1){ ?>
                        <div class="tab-pane <?php echo ($imk == 0) ? 'active' : ''; ?>" id="suksesi_<?=$status['id_trace_status'];?>">
                            <?php foreach($status['kandidat'] as $rw=>$kandidat){

                                $kandidat_page_id = $kandidat['page_id'];
                                $pasangan_page_id = $kandidat['page_id_pasangan'];

                                $profile_kandidat = $this->redis_slave->get('profile:detail:'.$kandidat_page_id);
                                $arrProfKandidat = @json_decode($profile_kandidat, true);

                                $pasangan_profile_kandidat = $this->redis_slave->get('profile:detail:'.$pasangan_page_id);
                                $arrPasanganProfKandidat = @json_decode($pasangan_profile_kandidat, true);


                                if($kandidat['profile_badge_url'] <> 'None')
                                {
                                    $kandidat_pic = $kandidat['profile_badge_url'];
                                } else {
                                    $kandidat_pic = $arrProfKandidat['icon_partai_url'];
                                    if($kandidat_pic == 'None')
                                    {
                                        $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                                    }
                                }

                                if($kandidat['profile_badge_pasangan_url'] <> 'None')
                                {
                                    $pasangan_pic = $kandidat['profile_badge_pasangan_url'];
                                } else {

                                    $pasangan_pic = $arrPasanganProfKandidat['icon_partai_url'];
                                    if($pasangan_pic == 'None')
                                    {
                                        $pasangan_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                                    }
                                }



                            ?>

                                <?php if($rw % 2 == 0){ ?>
                                <div class="row-fluid">
                                <?php } ?>
                                    <?php
                                        $kandidat_uri = base_url() . 'aktor/profile/' . $kandidat['page_id'];
                                        $pasangan_uri = base_url() . 'aktor/profile/' . $kandidat['page_id_pasangan'];
                                    ?>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span1">
                                                <?=$rw+1;?>
                                            </div>
                                            <div class="span3">
                                                <a href="<?=$kandidat_uri;?>">
                                                <div title="<?=$kandidat['page_name'];?>" style="background:
                                                    url(<?=$kandidat_pic;?>) no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi">

                                                </div>
                                                </a>
                                            </div>
                                            <div class="span3">
                                                <a href="<?=$pasangan_uri;?>">
                                                <div title="<?=$kandidat['page_name_pasangan'];?>" style="background:
                                                    url(<?=$pasangan_pic;?>) no-repeat; background-position: center; background-size:55px 55px;" class="circular-suksesi">

                                                </div>
                                                </a>
                                            </div>
                                            <div class="span5">
                                                <strong><?=$kandidat['page_name'];?></strong>
                                                <strong><?=$kandidat['page_name_pasangan'];?></strong>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                <?php if($rw % 2 == 1 || $rw == count($status['kandidat']) - 1){?>
                                </div>
                                <?php } ?>
                            <?php } ?>

                            <h4>Rekapitulasi</h4>
                            <table class="table">
                                <thead>
                                    <th>Sumber</th>
                                    <?php foreach($status['kandidat'] as $rw=>$kandidat){?>
                                        <th><span class="badge"><?=$rw+1;?></span></th>
                                    <?php } ?>
                                </thead>
                                <tbody>
                                    <?php foreach($status['lembaga'] as $rk=>$lembaga){?>
                                        <tr>
                                            <td><?=$lembaga['lembaga_name'];?></td>
                                            <?php $total_score = 0; ?>
                                            <?php foreach($lembaga['kandidat'] as $kandidat){
                                                    if(!empty($kandidat['score'])){
                                                        if ($kandidat['score']['score_type'] == '1'){
                                                            $total_score += intval($kandidat['score']['score']);
                                                        }
                                                    }
                                            }
                                            ?>
                                            <?php foreach($lembaga['kandidat'] as $kandidat){ ?>
                                            <?php

                                            $kandidat_score = '-';
                                            if(!empty($kandidat['score'])){
                                                $lg = $kandidat['score'];
                                                if ($kandidat['score']['score_type'] == '0') {
                                                    $kandidat_score = number_format($lg['score'], 2);
                                                } else {
                                                    $kandidat_score = number_format(($lg['score'] / $total_score)*100, 2);
                                                    $cals = (($lg['score'] /$total_score ) * 100) ;
                                                }

                                            }
                                                ?>
                                                <td><?=$kandidat_score;?> <?=($kandidat_score == '-' ? '' : '%');?></td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php foreach($status['kandidat'] as $rw=>$kandidat){?>
                                <span class="badge"><?=$rw+1;?></span> <?=$kandidat['alias'];?>
                            <?php } ?>
                        </div>
                        <?php $imk++; } ?>

                    <?php } ?>

                </div>

                <hr>

                <!-- description -->
                <p><?php echo nl2br($suksesi['description']); ?></p>
                <h4>Sengketa <?php echo ucwords($suksesi['race_name']); ?></h4>
                <?php if(count($suksesi['sengketa']) > 0){ ?>
                    <?php foreach ($suksesi['sengketa'] as $sengketa) { ?>
                        <?php if(!empty($sengketa['title_sengketa'])){ ?>
                        <div class="row-fluid">
                            <h5><?php echo $sengketa['title_sengketa']; ?></h5>
                        </div>
                        <div class="row-fluid">
                            <span><i class="icon-time"></i>&nbsp;<strong><?php echo mdate('%d %M %Y', strtotime($sengketa['tanggal_sengketa'])); ?></strong></span>
                        </div>
                        <div class="row-fluid">
                            <p><?php echo nl2br($sengketa['deskripsi_sengketa']); ?></p>
                        </div>

                        <div class="row-fluid">
                            <h5>Penyelesaian</h5>
                        </div>
                            <?php if (!empty($sengketa['tanggal_selesai'])){ ?>
                            <div class="row-fluid">
                                <span><i class="icon-time"></i>&nbsp;<strong><?php echo (!empty($sengketa['tanggal_selesai'])) ? mdate('%d %M %Y', strtotime($sengketa['tanggal_selesai'])) : ''; ?></strong></span>
                            </div>
                                <?php } ?>
                        <div class="row-fluid">
                            <p><?php echo (!empty($sengketa['penyelesaian'])) ? nl2br($sengketa['penyelesaian']) : 'Belum ada penyelesaian'; ?></p>
                        </div>
                        <hr>
                            <?php }else{ ?>

                        <div class="row-fluid">
                            <p>Tidak ada sengketa</p>
                        </div>
                            <?php } ?>
                        <?php } ?>
                    <?php }else{ ?>
                    <br>
                    <div class="row-fluid">
                        <p>Tidak ada sengketa</p>
                    </div>
                    <?php } ?>

                <br>
                <!-- KOMENTAR -->
                <div class="div-line"></div>
                <div class="row-fluid">

                    <h5>BERI KOMENTAR</h5>
                    <div class="row-fluid">
                        <div class="comment-side-left">
                            <div style="background:
                                    url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                            </div>
                        </div>

                        <div class="comment-side-right">
                            <div class="row-fluid">
                                <input id='comment_type' data-id="<?=$suksesi['content_id'];?>" type="text" name="comment" class="media-input" >
                            </div>
                            <div class="row-fluid">
                                <div class="span12 text-right">
                                    <?php if(!is_array($this->member)){?>
    <span>Login untuk komentar</span>&nbsp;
    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
    <?php }elseif(is_array($this->member)){?>
                                    <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="comment" data-page="1" data-id="<?=$suksesi['content_id'];?>" class="row-fluid comment-container">

                </div>
                <div id="load_more_place" class="row-fluid komu-follow-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a data-page="1" data-id="<?=$suksesi['content_id'];?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>
                <div id="div_line_bottom" class="div-line"></div>
                <!-- END -->
                <br>
<!--            </div>-->
        </div>
            <!-- suksesi terkait -->
        <div class="span4">
            <div class="suksesi-other-title">
                <a style="cursor: default;">SUKSESI TERKAIT</a>
            </div><br/>
            <div id="suksesi_terkait" data-id="<?=$suksesi['id_race'];?>"></div>
        </div>
        <br>

        </div>
    </div>
</div>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>


<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>


            <!-- TERKINI -->
            <?php echo $skandal; ?>


            <!-- HOT ISSUE -->
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>
    <div class="container-bawah">

        <div class="container">
            <div class="sub-header-container">
                <div class="row-fluid">

                    <!-- EKONOMI -->
                    <?php echo $ekonomi; ?>


                    <!-- HUKUM -->
                    <?php echo $hukum; ?>


                    <!-- PARLEMEN -->
                    <?php echo $parlemen; ?>

                </div>
            </div>
        </div>
        <br>

        <div class="container">
            <div class="sub-header-container">
                <div class="row-fluid">

                    <!-- EKONOMI -->
                    <?php echo $nasional; ?>


                    <!-- HUKUM -->
                    <?php echo $daerah; ?>


                    <!-- PARLEMEN -->
                    <?php echo $internasional; ?>

                </div>
            </div>
        </div>
        <br>
    </div>
<?php $this->load->view('template/modal/report_spam_modal'); ?>


<script>
    $(document).ready(function() {
        $('.carousel-suksesi').carousel({interval:false,wrap:false})
    });
</script>
