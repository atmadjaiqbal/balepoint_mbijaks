<h4 class="kanal-title kanal-title-yellow">SUKSESI DAN SURVEY</h4>
<?php
	$val = $suksesi;
  
	$suksesi_url = base_url() . 'suksesi/index/'.$val['id_race'].'-'.urltitle($val['race_name']);
	$map_photo = '';
	$logo_photo = '';
	foreach($val['photos'] as $foto){
	    if(intval($foto['type']) === 2){
	        $map_photo = $foto['thumb_url'];
	    }elseif(intval($foto['type']) === 1){
	        $logo_photo = $foto['thumb_url'];
	    }
	}
	$status_suksesi = 'Survey / Prediksi';
	$index_trace_status = 0;
	foreach($val['status'] as $idx => $status){
	    if(intval($status['draft']) == 1){
	        if(intval($status['status']) == 1)
	        {
	            $status_suksesi = 'Survey / Prediksi';
	        } else {
	            if(intval($status['status']) == 2)
	            {
	                $status_suksesi = 'Putaran Pertama';
	            } else {
	                $status_suksesi = 'Putaran Kedua';
	            }
	        }
	//        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
	        $index_trace_status = $idx;
	    }
	}


	$total_kandidat = count( $val['status'][$index_trace_status]['kandidat'] );
	$tabSection = "section-tab-".$val['id_race'];

	$status_suksesi = 'Survey / Prediksi';
	$index_trace_status = 0;
	$jmlh_putaran = 0;
	$status_text = array(1 => 'Survey / Prediksi', 2 => 'Putaran Pertama', 3 => 'Putaran Kedua');

	foreach($suksesi['status'] as $idx => $status){
	    if(intval($status['draft']) == 1){
	//        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
	        $index_trace_status = $idx;
	        $jmlh_putaran = (intval($status['status']) == 1) ? 0 : (intval($status['status']) == 2) ? 1 : 2;

	        if(intval($status['status']) == 1)
	        {
	            $status_suksesi = 'Survey / Prediksi';
	            $_keterangan = "Survey";
	        } else {
	            if(intval($status['status']) == 2)
	            {
	                $status_suksesi = 'Putaran Pertama';
	                $_keterangan = "1 Putaran";
	            } else {
	                $status_suksesi = 'Putaran Kedua';
	                $_keterangan = "2 Putaran";
	            }
	        }
	//        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
	        $index_trace_status = $idx;
	    }
	}

?>

<div class="row">
	<div class="col-xs-12">
		<h4 title="<?php echo $val['race_name']; ?>"><?php echo $val['race_name']; ?></h4>
		<table class="table">
            <tr>
                <td class="suksesi-ket-place">
                    <span>Tanggal pelaksanaan</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo mdate('%d %M %Y', strtotime($suksesi['tgl_pelaksanaan'])); ?></span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Suksesi</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo $suksesi['kdname']; ?></span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Daerah pemilihan</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo $suksesi['dname']; ?></span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Daftar pemilih tetap</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo $suksesi['daftar_pemilih_tetap']; ?> Jiwa</span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Jumlah kandidat</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo $total_kandidat; ?></span>
                </td>
            </tr>

            <tr>
                <td class="suksesi-ket-place">
                    <span>Status</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo $status_suksesi; ?></span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Jumlah putaran</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo $jmlh_putaran; ?></span>
                </td>
            </tr>
            <tr>
                <td class="suksesi-ket-place">
                    <span>Keterangan</span>
                </td>
                <td class="suksesi-ket-place">
                    <span>: <?php echo $_keterangan;?></span>
                </td>
            </tr>
        </table>
	</div>

	<div class="col-xs-12">
		<div class="panel-group" id="accordion">

			<?php
            $t = 0;
            $limit_lembaga = 7;
            $limit_candidat = 10;

            ?>
            
                <?php
                $limit_lembaga = 7;
                $limit_candidat = 10;
                $t=0; $_lembaga = array();$sort_name = array();
                foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $sortlembaga){$sort_name[]  = $sortlembaga['lembaga_name'];}
                if(in_array('Rekapitulasi KPU', $sort_name)){krsort($val['status'][$index_trace_status]['lembaga']);}

                foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $lembaga):
                	if(!empty($lembaga['start_date']) && $lembaga['start_date'] <> 'None')
                    {
                        $lembaga_date = '<span class="">('.date('d-m-Y', strtotime($lembaga['start_date'])).')</span>';
                    } else {
                        $lembaga_date = '';
                    }

                    if($racekey >= $limit_lembaga){ break; }
                    $lembaga_name  = $lembaga['lembaga_name'];
                    if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];

                    $id_lembaga = $lembaga['id_trace_lembaga'];
                    $words	= preg_split("/\s+/", $lembaga_name);
                    $acronym = (count($words) == 1) ? substr($words[0], 0,3) : '';
                    $tabName = 'tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                    $target = 'conten-tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                    if(count($words) > 1) {
                        foreach($words as $w) {
                            if (isset($w[0])) {
                                if(ctype_alnum($w[0]))  $acronym .= $w[0];
                            }
                        }
                    } else {
                        $acronym = substr($lembaga_name, 0,3);
                    }
                    $acronym = strtoupper($acronym);
                    if($acronym == '') $acronym = '&hellip;' ;
                    if($acronym == 'RK') { $acronym = 'KPU'; $backtab = 'style="background-color:#cecece;"'; } else { $backtab = '';}

                    $_lembaga[$t] = $acronym;

                    ?>
                    <div class="panel panel-default">
					    <div class="panel-heading">
					      <h6 class="panel-title panel-title-xs">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $tabName;?>">
					          <?php echo $acronym;?>
					        </a>
					      </h6>
					    </div>
					    <div id="collapse<?php echo $tabName;?>" class="panel-collapse collapse">
					      <div class="panel-body panel-body-xs">
					        	<h6><?php echo $lembaga_name;?> <?php echo $lembaga_date;?></h6>


					        	<table summary='' class='table'>
	                            <?php
	                            $total_score 	= 0;
	                            $winner		 	= '';
	                            $winner_score	= 0;
	                            $pct = 0;
	                            foreach($lembaga['kandidat'] as $kandidat) {
	                                if(count($kandidat['score']) > 0){
	                                    $score = $kandidat['score'];
	                                    $kandidat = $kandidat['kandidat'];
	                                    if ($score['score_type'] == '1') {
	                                        $total_score += (int) $score['score'];
	                                    }
	                                    if ($score['score'] > $winner_score) {
	                                        $winner 			= $kandidat['page_id'];
	                                        $winner_score 	= $score['score'];
	                                    }
	                                }
	                            }
	                            foreach($lembaga['kandidat'] as $kandidatkey => $kandidat) :

	                                $score = $kandidat['score'];
	                                $kandidat = $kandidat['kandidat'];
	                               // if($kandidatkey >= $limit_candidat){break;}

	                                $kandidat_page_id = $kandidat['page_id'];
	                                $kandidat_name	= $kandidat['page_name'];
	                                $kandidat_link	= base_url() . 'aktor/profile/' . $kandidat['page_id'];

	                                $pasangan_page_id = $kandidat['page_id_pasangan'];

	                                if($kandidat['page_name_pasangan'] != 'None' && $kandidat['page_name_pasangan'] != 'None' &&
	                                   $kandidat['page_name_pasangan'] != 'Tidak Ada Pasangan ' &&  $kandidat['page_name_pasangan'] != 'Pasangan Politisi' &&
	                                   ! empty($kandidat['page_name_pasangan'])
	                                )
	                                {
	                                    $pasangan_name	= trim($kandidat['page_name_pasangan']);
	                                } else {
	                                    $pasangan_name	= '';
	                                }

	                                $pasangan_link	= base_url() . 'aktor/profile/' . $kandidat['page_id_pasangan'];

	                                $profile_kandidat = $this->redis_slave->get('profile:detail:'.$kandidat_page_id);
	                                $arrProfKandidat = @json_decode($profile_kandidat, true);

	                                $pasangan_profile_kandidat = $this->redis_slave->get('profile:detail:'.$pasangan_page_id);
	                                $arrPasanganProfKandidat = @json_decode($pasangan_profile_kandidat, true);

	                                $partai_kandidat_id = $arrProfKandidat['partai_id'];
	                                $partai_kandidat_pasangan_id = $arrPasanganProfKandidat['partai_id'];

	                                $prof_partai_kandidat = $this->redis_slave->get('profile:detail:'.$partai_kandidat_id);
	                                $arrProfpartaiKandidat = @json_decode($prof_partai_kandidat, true);

	                                $prof_partai_paskandidat = $this->redis_slave->get('profile:detail:'.$partai_kandidat_pasangan_id);
	                                $arrProfpartaiPasKandidat = @json_decode($prof_partai_paskandidat, true);

	                                if($val['id_race'] != '275')
	                                {
	                                   if(isset($arrProfpartaiKandidat['alias']))
	                                   {
	                                      $kandidat_partai = ($arrProfpartaiKandidat['alias'] <> 'None' ? '('.$arrProfpartaiKandidat['alias'].')' : '');
	                                   } else { $kandidat_partai = ''; }
	                                } else {
	                                    if(isset($arrProfKandidat['alias']))
	                                    {
	                                        $kandidat_partai = ($arrProfKandidat['alias'] <> 'None' ? '('.$arrProfKandidat['alias'].')' : '');
	                                    } else { $kandidat_partai = ''; }
	                                }

	                                if(isset($arrProfpartaiPasKandidat['alias']))
	                                {
	                                    $pasangan_partai = ($arrProfpartaiPasKandidat['alias'] <> 'None' ? '('.$arrProfpartaiPasKandidat['alias'].')' : '');
	                                } else { $pasangan_partai = ''; }

	                                $kandidat_logo_partai = ($arrProfKandidat['icon_partai_url'] <> 'None' ? '<img src="'.$arrProfKandidat['icon_partai_url'].'" width="25px" height="25px" >' : '');
	                                $pasangan_logo_partai = ($arrPasanganProfKandidat['icon_partai_url'] <> 'None' ? '<img src="'.$arrPasanganProfKandidat['icon_partai_url'].'" width="25px" height="25px" >' : '');

	                                if($kandidat['profile_badge_url'] <> 'None')
	                                {
	                                    $kandidat_pic = $kandidat['profile_badge_url'];
	                                } else {
	                                    $kandidat_pic = $arrProfKandidat['icon_partai_url'];
	                                    if($kandidat_pic == 'None')
	                                    {
	                                        $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
	                                    }
	                                }

	                                if($kandidat['profile_badge_pasangan_url'] <> 'None')
	                                {
	                                    $pasangan_pic = $kandidat['profile_badge_pasangan_url'];
	                                } else {

	                                    $pasangan_pic = $arrPasanganProfKandidat['icon_partai_url'];
	                                    if($pasangan_pic == 'None')
	                                    {
	                                        $pasangan_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
	                                    }
	                                }

	                                if(!empty($score)){

	                                    if ($score['score_type'] == '0') {
	                                        $pct			= number_format($score['score'],2);
	                                    } else {
	                                        $pct			= number_format(($score['score'] / $total_score)*100,2);
	                                    }
	                                }

	                                if($lembaga_name == 'Rekapitulasi KPU')
	                                {
	                                    $winner_bar 	= ($winner == $kandidat['page_id'] ) ? 'background-color:#128405;' : '';
	                                    $winner_text = ($winner == $kandidat['page_id'] ) ? 'Pemenang' : '';
	                                } else {
	                                    $winner_bar 	= ($winner == $kandidat['page_id'] ) ? 'background-color:#11097A;' : '';
	                                    $winner_text = '';
	                                }

	                                ?>
	                                <tr>
	                                	<td style="width: 10px;">
	                                		<span><?php echo $kandidat['nomor_urut']; ?></span>
	                                	</td>
	                                    <td class=''>
	                                        <p class="media-heading">
	                                            <?php echo $kandidat_name.' '.$kandidat_partai;?>
	                                        </p>
	                                        <?php if(!empty($pasangan_name)){ ?>
	                                        <p class="media-heading">
	                                            <?php echo $pasangan_name.' '.$pasangan_partai; ?>
	                                        </p>
	                                        <?php } ?>
	                                        <?php if(!empty($winner_text)){ ?>
	                                        <p class="media-heading"><?php echo $winner_text;?></p>
	                                        <?php } ?>
	                                    </td>
	                                    <td>
	                                        <h5><?php echo $pct; ?>%</h5>
	                                    </td>
	                                </tr>
	                            <?php endforeach;?>
	                        </table>


					      </div>
					    </div>
				    </div>
                    
                    <?php
                    $t++;
                endforeach;

                if(in_array('KPU', $_lembaga))
                {

                } else {
                ?>
                	<div class="panel panel-default">
					    <div class="panel-heading">
					      <h6 class="panel-title panel-title-xs">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $index_trace_status;?>-9999-<?php echo $id_lembaga;?>">
					          KPU
					        </a>
					      </h6>
					    </div>
					    <div id="collapse<?php echo $index_trace_status;?>-9999-<?php echo $id_lembaga;?>" class="panel-collapse collapse">
					      <div class="panel-body">
					        	<h6>Komisi Pemilihan Umum</h6>
					        	<p>Data Belum Tersedia</p>
					      </div>
					    </div>
				    </div>
                    
                <?php
                }

                ?>
            

		  
		  
		</div>
	</div>

</div>

<div class="row">
	<div class="col-xs-12">
		<!-- description -->
                <p><?php echo nl2br($suksesi['description']); ?></p>
                <h5>Sengketa <?php echo ucwords($suksesi['race_name']); ?></h5>
                <?php if(count($suksesi['sengketa']) > 0){ ?>
                    <?php foreach ($suksesi['sengketa'] as $sengketa) { ?>
                        <?php if(!empty($sengketa['title_sengketa'])){ ?>
                        
                        <h6><?php echo $sengketa['title_sengketa']; ?></h6>
                        
                            <span><i class="icon-time"></i>&nbsp;<strong><?php echo mdate('%d %M %Y', strtotime($sengketa['tanggal_sengketa'])); ?></strong></span>
                        
                        
                            <p><?php echo nl2br($sengketa['deskripsi_sengketa']); ?></p>
                        

                        
                            <h6>Penyelesaian</h6>
                        
                            <?php if (!empty($sengketa['tanggal_selesai'])){ ?>
                            
                                <span><i class="icon-time"></i>&nbsp;<strong><?php echo (!empty($sengketa['tanggal_selesai'])) ? mdate('%d %M %Y', strtotime($sengketa['tanggal_selesai'])) : ''; ?></strong></span>
                            
                                <?php } ?>
                        
                            <p><?php echo (!empty($sengketa['penyelesaian'])) ? nl2br($sengketa['penyelesaian']) : 'Belum ada penyelesaian'; ?></p>
                        
                        <hr class="line-mini">
                            <?php }else{ ?>
					    <p>Tidak ada sengketa</p>
                        
                            <?php } ?>
                        <?php } ?>
                    <?php }else{ ?>
                    <br>
                    
                    <p>Tidak ada sengketa</p>
                    
                    <?php } ?>
	</div>
</div>
<br>
<h4 class="kanal-title kanal-title-yellow">SUKSESI DAN SURVEY LAINNYA</h4>
<?php 
	echo $suksesi_more;
?>
