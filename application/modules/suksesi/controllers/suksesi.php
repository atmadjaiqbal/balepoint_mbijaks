<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suksesi extends Application
{
   
	function __construct()
	{
		    parent::__construct();

    		$this->load->helper('date');
    		$this->load->helper('m_date');
    		
    		$this->load->helper('text');
    		$this->load->helper('seourl');

        $this->load->module('timeline/timeline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');
        $this->load->module('headline/headline');
        $this->load->module('survey/survey');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
		    $this->load->library('suksesi/suksesi_lib');

        // $this->member = $this->session->userdata('member');
   }
	

	function index($URL='')
	{
   

      $data = $this->data;

      if(!empty($URL)) 
      {

          $data['category'] = 'suksesi';

          $arrUrl     = explode("-", $URL);
          $suksesiID  = intval($arrUrl[0]);

          $suksesi_detail = $this->getsuksesicache($suksesiID, $limit=0, $offset=0, $result=0);

          $data['suksesi'] = $suksesi_detail;
          $data['suksesi_more'] = $this->terkait($suksesiID);

          $data['head_title'] = 'Suksesi ' . $suksesi_detail['race_name'];

          $html['html']['content'] = $this->load->view('mobile/suksesi_detail', $data, true);
          $this->load->view('m_tpl/layout', $html);

      }
      else 
      {
          $data['head_title'] = 'Suksesi ';

          $limit = 20;
          $page = 1;
          $per_page = $this->input->get('per_page');
          if(!is_bool($per_page)){
              if(!empty($per_page)){
                  $page = $per_page;
              }
          }

          $offset = ($limit*intval($page)) - $limit;
//          if($offset != 0){
//              $offset += (intval($page) - 1);
//          }

          // echo $offset;

          $qr = $_SERVER['QUERY_STRING'];

          $rkey = 'suksesi:list:all';
          $user_count = $this->redis_slave->lLen($rkey);

          $suksesi_list = $this->getsuksesicache($id=0, $limit, $offset, $result=1);

          $this->load->library('pagination');

          $config['base_url'] = base_url().'suksesi/index/?'; //.$qr;
          $config['total_rows'] = $user_count;
          $config['per_page'] = $limit;
          $config['use_page_numbers'] = TRUE;
          $config['page_query_string'] = TRUE;
          $choice = $config["total_rows"] / $config["per_page"];
          $config["num_links"] = 2; // round($choice);
          $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
          $config['full_tag_close'] = '</ul>';
          $config['cur_tag_open'] = '<li class="disabled"><a>';
          $config['cur_tag_close'] = '</a></li>';
          $config['num_tag_open'] = '<li>';
          $config['num_tag_close'] = '</li>';
          $config['next_link'] = '...';
          $config['next_tag_open'] = '<li>';
          $config['next_tag_close'] = '</li>';
          $config['prev_link'] = '...';
          $config['prev_tag_open'] = '<li>';
          $config['prev_tag_close'] = '</li>';
          $config['first_link'] = FALSE;
          $config['last_link'] = FALSE;

          $this->pagination->initialize($config);

          $data['pagi'] =  $this->pagination->create_links();
          $data['suksesi'] = $suksesi_list;
          $data['offset'] = $offset;
          $data['total']	= $user_count;

          $html['html']['content'] = $this->load->view('mobile/suksesi_index', $data, true);
          $this->load->view('m_tpl/layout', $html);

	   }
   }

    public function terkait($id=0)
    {
        
        $limit = 9;
        $page = 1;
        $per_page = $this->input->get('per_page');
        if(!is_bool($per_page)){
            if(!empty($per_page)){
                $page = $per_page;
            }
        }

        $offset = ($limit*$page) - $limit;
        // echo $offset;

        $qr = $_SERVER['QUERY_STRING'];



        $rkey = 'suksesi:list:all';
        $user_count = $this->redis_slave->lLen($rkey);

        

        $suksesi_list = $this->getsuksesicache(0, $limit, $offset, $result=1, $id);
        // foreach($suksesi_list as $row=>$suksesi){
        //     $suksesi['last_score'] = $this->timeline->last_score($suksesi['content_id'],'true');
        //     $suksesi_list[$row] = $suksesi;
        // }

        $data['suksesi'] = $suksesi_list;

        return $this->load->view('suksesi/mobile/suksesi_list', $data, true);

    }

    private function getsuksesicache($id=0, $limit=9, $offset=0, $result=1, $key_exist=0)
    {
        if($result === 1){

            $rkey = 'suksesi:list:all';

            $rrange = $offset + $limit;
//            echo '<pre>';
//            echo $offset;
//            echo $rrange;
//            echo '</pre>';
            $co = $this->redis_slave->lLen($rkey);
            $suksesi_list = $this->redis_slave->lRange($rkey, $offset, $rrange);
            if(intval($key_exist) != 0 ){
                $suksesi_list = $this->redis_slave->lRange($rkey, $offset, intval($co) - 1);

                if(in_array($key_exist, $suksesi_list)){
                    $idk = array_search($key_exist, $suksesi_list);
                    unset($suksesi_list[$idk]);

                }
                shuffle($suksesi_list);

            }

            $result = array();
            $i = 0;
            if(count($suksesi_list) > 0){
                foreach ($suksesi_list as $key => $value) {

                    if($key_exist == intval($value))
                        continue;

                    if ($key == $limit) break;

                    $suksesi_detail = $this->redis_slave->get('suksesi:detail:'.$value);
                    $suksesi_array = @json_decode($suksesi_detail, true);
                    $result[$i] = $suksesi_array;

                    $i++;
                }
            }
            return $result;
        }else{
            $suksesi_detail = $this->redis_slave->get('suksesi:detail:'.$id);
            $suksesi_array = @json_decode($suksesi_detail, true);
            return $suksesi_array;

        }
    }
   

   function getSuksesiList($page='0', $limit='10', $where=array())
   {
         $suksesi_lib = $this->load->library('suksesi/suksesi_lib');
   		$arr        = array(intval($limit), $page);
   		$count      = $suksesi_lib->getSuksesiCount();
   		$race       = $suksesi_lib->getSuksesiList($page, $limit,$where);
   		$race_arr   = array();
   		$cm         = $this->load->module('ajax');           	
   		$i = 0;
         
   		foreach ($race as $row) {
   			$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['update_date']));
   			$row['content_date']    = $contentDate;
   			$row['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate); 

   			$where = array('id_race' => $row['id_race']);
   			$row['photos']    = $suksesi_lib->getSuksesiPhoto($row['id_race']);
   			if(!empty($row['photos'])){
   				$row['thumb'] = thumb_url($row['photos'][0]['attachment_title']);
   			}
            
   			$order_by = 'status asc';
   			$status = $suksesi_lib->getSuksesiStatus($where, $order_by);
   			
   			$status_arr = array();
            $score      = array();
            $score_count= 0;

   			$status_aktif = '';
   			$j = 0;
   			foreach($status->result_array() as $rr){
   				$con_status = '';
   				$st = $rr['status'];
   				
   				switch ($st) {
   					case 1:
   						$con_status = 'Survey / Prediksi';
   						break;
   					case 2:
   						$con_status = 'Putaran Pertama';
   						break;
   					case 3:
   						$con_status = 'Putaran Kedua';
   						break;
   					
   				}
   
   				if($rr['draft'] == '1'){
   					$status_aktif = $con_status;
   				}
   				
   				$rr['draft'] = (intval($rr['draft']) == 1) ? 'Publish' : 'Draft';
   				$rr['status'] = $con_status;
   				//$lembaga = $suksesi_lib->getLembaga(array($rr['id_trace_status']));
   				
   				$rr['lembaga'] = $suksesi_lib->getLembagaScoreList($rr['id_trace_status']);
   
   				//$rr['kandidat'] = $suksesi_lib->getKandidat(array($rr['id_trace_status']))->result_array();
   				$rr['kandidat']   = $suksesi_lib->getKandidatScoreList($rr['id_trace_status']);  
   				$status_arr[$j] = $rr;
               
               // get latest publis scrore 
   				if($rr['draft'] == 'Publish') $score = $rr;
   				
   				if($rr['draft'] == 'Publish' && $st <> '1') $score_count++;
   				
                 				
   				$j++;	
   			}
   			$row['status'] = $status_aktif;
   			 // no need to add detail data tuk list, just to get status 
   			$row['kandidat_count'] = $suksesi_lib->getKandidatListCount($row['id_race']);

   			$row['score'] = $score;
            $row['score_count'] = $score_count;
   			
   			$race_arr[$i] = $row;
   			$i++;
   		}
   
   		$data['total_rows']  = $count;
   		$data['list']     = $race_arr; 
      return $data;
   }

   private function getSuksesiDetail($suksesiID)
   {
         $where      = array('id_race' => $suksesiID);
         
   		$race       = $this->suksesi_lib->getSuksesiList(0, 1, $where);
   		
   		$race_arr   = array();
   		$cm         = $this->load->module('ajax');           	
   		$i = 0;
         
   		foreach ($race as $row) {
   			$contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['last_update_date']));
   			$row['content_date']    = $contentDate;
   			$row['content_count']   = $cm->count_content($row['content_id'], '2', '2', NULL, $contentDate); 

   			$where = array('id_race' => $row['id_race']);
   			$row['photos']    = $this->suksesi_lib->getSuksesiPhoto($row['id_race']);
   			if(!empty($row['photos'])){
   				$row['thumb'] = thumb_url($row['photos'][0]['attachment_title']);
   			}
            
   			$order_by = 'status asc';
   			$status = $this->suksesi_lib->getSuksesiStatus($where, $order_by);
   			
   			$status_arr = array();
            $score      = array();
            $score_count= 0;

   			$status_aktif = '';
   			$j = 0;
   			foreach($status->result_array() as $key => $rr){
   				$con_status = '';
   				$st = $rr['status'];
   				
   				switch ($st) {
   					case 1:
   						$con_status = 'Survey / Prediksi';
   						break;
   					case 2:
   						$con_status = 'Putaran Pertama';
   						break;
   					case 3:
   						$con_status = 'Putaran Kedua';
   						break;
   					
   				}
   
               $status_aktif =  ($rr['draft'] == '1') ? $con_status : '';

   				$rr['draft'] = (intval($rr['draft']) == 1) ? 'Publish' : 'Draft';
   				
   				if ($rr['draft'] == 'Publish') {
                  $score['id_trace_status']  = $rr['id_trace_status'];
      				$score['score_name']  = $con_status;
      				$score['status']      = $rr['draft'];
      				$score['lembaga']      = $this->suksesi_lib->getLembagaScoreList($rr['id_trace_status']);
      				$score['kandidat']   = $this->suksesi_lib->getKandidatScoreList($rr['id_trace_status']);  

      				 	

      				
      			   $row['score'][] = $score;               
      			}
   				$j++;	
   			}
   			$row['status'] = $status_aktif;
   			$race_arr[$i] = $row;
   			$i++;
   		}
   
   		$data['list']     = $race_arr; 
      return $data;
   }


   public function load_more($page='1', $limit='50')
   {
     // $this->checkMember();
      //$this->checkUID();     
   	
      // first page use limit 10 so next offset is 11
      $offset           = ($page =="1") ? '11' :  $page*$limit;
      $data['suksesi']  = $this->getSuksesiList($offset,$limit);
      $data['offset']   = $offset;
      $totalPage        = ceil($data['suksesi']['total_rows']/$limit);
 		$nextPage         = (int)$page+1;
      $load_more        = ($page < $totalPage) ? '1': '0';         
      
      $html = $this->load->view('suksesi/suksesi_list', $data, true);      

      // HTML Response, Flag Load More, Next Page
      echo $html .'||'. $load_more .'||'. $nextPage;      
   }   
 
 
 
 	private function checkMember() {
 	   $this->load->model('member', 'member_model');
		if($this->member) {
			$this->member['current_city'] = $this->member_model->getUserCurrentCity($this->member['account_id']); 
		} else {
			redirect($this->data['base_url'].'home/login/?redirect='.$this->current_url);
		}
	}

   private function checkUID() {
		$uID = $this->input->post('uID');
		if(isset($_GET['uid']) && ! empty($_GET['uid'])) {
			$this->uID = $_GET['uid'];
			$this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);
			
		} else if(! empty($uID)) {
			$this->uID = $uID;
			$this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);
			
		} else {
			$this->uID  = $this->member['account_id'];
			$this->isLogin = true;
		}
	}
	
	private function isLogin($str1, $str2) {
		if(! strcmp($str1, $str2)) {
			return true;
		} else {
			return false;
		}
	}

   

}