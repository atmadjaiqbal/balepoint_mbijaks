<?php if(!empty($suksesi['list'])):?>
	<?php foreach ($suksesi['list'] as $key => $row) :?>
		<?php
		$rid		= $offset + $key;
		$race_name	= $row['race_name'];
		$race_link	= base_url().'suksesi/index/'. $row['id_race'] . '-' . urltitle($race_name) . '/';
		$race_map	= '/public/img/zero.png';
		$race_logo	= !empty($row['photos']) ? large_url($row['photos'][0]['attachment_title'], 'suksesi') : '/public/image/noimage.jpg';

		$img_logo = '';
		$img_map  = '';

      if (!empty($row['photos'])) {
         foreach($row['photos'] as $photo) {
            if ($photo['type'] == '1')    $img_logo = large_url($photo['attachment_title'],'suksesi');
            if ($photo['type'] == '2')    $img_map  = large_url($photo['attachment_title'],'suksesi');
         }
         if (empty($img_logo))  $img_logo = $race_logo;
         if (empty($img_map))   $img_map  = $race_map;
      }

		$race_dpt = '&nbsp;';
		if(!empty($row['daftar_pemilih_tetap'])) {
			$race_dpt = number_format($row['daftar_pemilih_tetap'], 0, ',', '.');
			}
		$race_kandidat = '&nbsp;';

		if (!empty($row['score']['lembaga'])) {
		   $kandidat_count = 0;
		   if (!empty($row['score']['lembaga'][0]['kandidat'])) {
		      $kandidat_count = count($row['score']['lembaga'][0]['kandidat']);
         }
         $race_kandidat = number_format($kandidat_count, 0, ',', '.');
      }
		?>

		<div class='row-fluid'>
			<div class='span8'>
				<div class='row-fluid home-race-head'>
					<div class='span3 home-race-logo'><img src='<?=$img_logo?>' alt=''/></div>
					<div class='span9 home-race-name'>
						<h4><a href='<?=$race_link?>'><?=$race_name?></a></h4>
						<p><?php echo $row['content_count'];?></p>
					</div>
				</div>
				<div class=''><!-- postmeta -->
					<table>
						<tbody><tr><td style='width:10em;'>Daftar Pemilih Tetap</td><td style='width:1em;'>:</td><td><?=$race_dpt?></td></tr>
						<tr><td>Jumlah Kandidat</td><td>:</td><td><?=$race_kandidat?></td></tr>
					</tbody></table>
				</div><!-- postmeta -->
			</div>
			<div class='span4 home-race-map'><img src='<?=$img_map?>' alt=''/></div>
		</div>

<!-- ### TABBABLE ########################################################## -->
<?php if(!empty($row['score']['lembaga'])):?>
	<div class="row-fluid">
		<?php $tabSection = "section-tab-".$rid;?>
		<div class="list-content-suksesi tabbable tabs-below" id="<?=$tabSection;?>">
			<div class="tab-content" id="content-<?=$tabSection;?>" style="overflow: hidden;zoom: 1;">
				<?php
				$t = 0;
				foreach($row['score']['lembaga'] as $racekey => $lembaga):
					$tabName = 'tab-'.$lembaga['id_trace_status'] . '-'.$lembaga['id_trace_lembaga'];
					//$lembaga_name =  (strpos($lembaga['page_id'], ' ') == TRUE) ?  $lembaga['page_id'] : $lembaga['page_name'];
					$lembaga_name  = $lembaga['page_name'];
					if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];

				?>
				<div class="tab-pane" id="conten-<?=$tabName;?>" <?=($t==0) ? ' style="display: block;"' : ' style="display: none;"';?> >
					<!-- ### isi tab ####################################### -->
					<h5><?=$lembaga_name?></h5>
					<table summary='' class='home-race-candidates'>
					<?php
   					$total_score = 0;
						$winner		 	= '';
						$winner_score	= 0;

   					foreach($lembaga['kandidat'] as $kandidat) {
   					   if ($kandidat['score_type'] == '1') {
   					      $total_score += (int) $kandidat['score'];
   					   }

							if ($kandidat['score'] > $winner_score) {
								$winner 			= $kandidat['page_id'];
								$winner_score 	= $kandidat['score'];
							}

   					}
   					foreach($lembaga['kandidat'] as $kandidat) :

						$kandidat_name	= $kandidat['page_name'];
						$kandidat_link	= base_url() . 'politisi/index/' . $kandidat['page_id'];
						$kandidat_pic	= badge_url($kandidat['kandidat_attachment'], 'politisi/'.$kandidat['page_id']);

						$pasangan_name	= $kandidat['page_name_pasangan'];;
						$pasangan_link	= base_url() . 'politisi/index/' . $kandidat['page_id_pasangan'];
						$pasangan_pic	= badge_url($kandidat['pasangan_attachment'], 'politisi/'.$kandidat['page_id_pasangan']);
						if ($kandidat['score_type'] == '0') {
						   $pct			= number_format($kandidat['score'],2);
						} else {
						   $pct			= number_format(($kandidat['score'] / $total_score)*100,2);
						}

						$winner_bar 	= ($winner == $kandidat['page_id'] ) ? 'background-color:#11097A;' : '';
						$winner_text 	= ($winner == $kandidat['page_id'] ) ? '<div style="color:#ffffff;margin-top:-38px;padding:10px;font-weight:bold;">Pemenang</div>' : '';

						?>
      				<tr>
      					<td class='home-race-candidate'>
      						<a href='<?=$kandidat_link?>'><img src='<?=$kandidat_pic?>' alt="<?=$kandidat_name?>"/></a>
      					</td>
      					<td class='home-race-candidate'>
      						<a href='<?=$pasangan_link?>'><img src='<?=$pasangan_pic?>' alt="<?=$pasangan_name?>"/></a>
      					</td>
							<td class='home-race-percentage'>
								<div class='home-race-percentage-box'><div class='home-race-percentage-bar' style='width:<?=$pct?>%;<?=$winner_bar;?>'></div><?=$winner_text;?></div>
								<p><?=$pct?>%</p>
							</td>

      				</tr>
				   <?php endforeach;?>
					</table>
				</div>
				<?php
				$t++;
				endforeach;
				?>
			</div>
			<ul class="nav nav-tabs">
				<?php
				$t=0;
				foreach($row['score']['lembaga'] as $racekey => $lembaga):
					$lembaga_name  = $lembaga['page_name'];
					if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];

					$words	= preg_split("/\s+/", $lembaga_name);
					$acronym = (count($words) == 1) ? substr($words[0], 0,3) : '';
					$tabName = 'tab-'.$lembaga['id_trace_status'] . '-'.$lembaga['id_trace_lembaga'];
					if(count($words) > 1) {
						foreach($words as $w) {
						   if (isset($w[0])) {
   							if(ctype_alnum($w[0]))  $acronym .= $w[0];
							}
						}
				   } else {
				      $acronym = substr($lembaga_name, 0,3);
				   }
				   $acronym = strtoupper($acronym);
				   if($acronym == '') $acronym = '&hellip;' ;

				?>

					<li id="list-<?=$tabName;?>" <?php if ($t==0) echo ' class="active"' ;?>>
						<a id="<?=$tabName; ?>" href="#"><?=$acronym;?></a>
					</li>
				<?php
				$t++;
				endforeach;
				?>
			</ul>
		</div>
	</div>
<?php endif;?>
<!-- ### TABBABLE ########################################################## -->

	<hr/>
	<?php endforeach;?>
<?php endif;?>