<h4 class="kanal-title kanal-title-green">SPECIAL EVENT</h4>

<div class="row">
	<div class="col-xs-12">
		<table class="table">
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Event</th>
					<th>Tanggal Pelaksanaan</th>
				</tr>
			</thead>
			<?php $offset++; foreach ($event as $key => $value) { ?>
				<tr class="<?php echo ($offset%2 == 0) ? 'success' : ''; ?>" >
					<td><?php echo $offset; ?></td>
					<td><?php echo $value['event_name'];?></td>
					<td><?php echo date('d M Y', strtotime( $value['event_date']));?></td>
				</tr>
			<?php $offset++; } ?>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 text-center">
		<?php echo $pagi; ?>		
	</div>
</div>

