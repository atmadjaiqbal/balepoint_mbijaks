<?php

$mailto = '&#109;&#97;&#105;&#108;&#116;&#111;';
$email = 'administrator@bijaks.net';
$email_ob = '';
$l = strlen($email);
for($i = 0; $i < $l; $i++) {
    $c = $email[$i];
    $x = ord($c);
    $email_ob .= "&#$x;";
}
?>

<div class="container" style="margin-top: 31px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right category-news-title-merah">
                    <h1>SYARAT DAN KETENTUAN</h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>

    <div class="sub-header-container" style="background-color: #fff;margin-top:170px;">
        <div style="margin-left: 20px;padding-top: 30px;margin-right: 20px;">
            <p>Bijaks merupakan sarana komunikasi dan informasi mengenai politik di Indonesia baik melalui status, komentar, profil, foto, blog dan forum diskusi maupun perangkat lain yang ada di <strong class='brand'>bijaks.net</strong>.</p>
            <p>Sebagai pengakses atau pengguna bijaks, anda menyatakan setuju dengan beberapa syarat dan ketentuan berikut ini:</p>
            <ol>
                <li>Tidak akan menggunakan bijaks untuk kepentingan komersial, penghasutan, fitnah, pornografi maupun tindakan lainnya yang melanggar hukum, baik peraturan perundang-undangan maupun susila. <strong class='brand'>bijaks.net</strong> berhak untuk mengganti, mengubah dan menambah isi dari syarat dan ketentuan yang berlaku ini tanpa pemberitahuan sebelumnya. </li>
                <li>Resiko penggunaan bijaks, sepenuhnya di tangan masing-masing pengguna, dan sesama pengguna bertanggung jawab atas interaksi dengan pengguna lain. Bijaks, tidak bertanggung jawab akan penyalahgunaan sebagaimana yang disebutkan di atas, dan tidak berkewajiban memantau interaksi antar pengguna. </li>
                <li><strong class='brand'>bijaks.net</strong> berhak menghapus kontent dan/atau memblokir pengguna setiap saat, dengan alasan apapun. Baik karena ada laporan atau tidak, tentang tuduhan penyalahgunaan kontent dan/atau pengguna dari sesama pengguna, pihak ketiga dan pihak berwajib. </li>
                <li>Pengguna bijaks tidak mem-posting unauthorized commercial communications (iklan dan komunikasi perniagaan tanpa izin maupun terselubung), spam, virus atau kode berbahaya lainnya di <strong class='brand'>bijaks.net</strong>, serta tidak akan menggunakan <strong class='brand'>bijaks.net</strong> melalui sarana otomatis seperti robots, spider atau scrapers tanpa pemberitahuna dan ijin tertulis dari <strong class='brand'>bijaks.net</strong>.</li>
                <li>Pengguna bijaks tidak diperbolehkan mengambil informasi maupun isi yang lainnya, baik secara utuh maupun sebagian, serta mengumpulkan informasi login dan menggunakan akun kepunyaan pengguna lain.</li>
                <li>Segala informasi di <strong class='brand'>bijaks.net</strong> merupakan hal yang sangat penting, <strong class='brand'>bijaks.net</strong> akan berusaha untuk menjaga setiap data dan informasi aman, tapi <strong class='brand'>bijaks.net</strong> tidak dapat memberikan jaminan.</li>
                <li>Laporan penyalahgunaan kontent dikirimkan melalui alamat email: <a href='<?=$mailto?>:<?=$email_ob?>'><?=$email_ob?></a>.</li>
            </ol>
        </div>
    </div>

</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>

            <!-- TERKINI -->

            <?php echo $skandal_mini; ?>

            <!-- HOT ISSUE -->
            <?php //echo $issue; ?>
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>
<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>
