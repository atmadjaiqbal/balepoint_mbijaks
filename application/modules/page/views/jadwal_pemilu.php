<div class="container" style="margin-top: 31px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right category-news-title-merah">
                    <h1>JADWAL PEMILU <?php echo date('Y');?></h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>

    <div class="sub-header-container" style="background-color: #fff;margin-top:230px;">
        <div style="margin-left: 20px;padding-top: 30px;margin-right: 20px;margin-bottom: 60px;">
            <div  id="jadwalpemilu">
            <table>
                <tr>
                    <th class="col-tgl">TANGGAL</th>
                    <th class="col-agenda">ACARA KEGIATAN</th>
                </tr>
                <tr>
                    <td class="col-tgl">11 Januari – 05 April</td><td class="col-agenda">Pelaksanaan Kampanye</td>
                </tr>
                <tr>
                    <td class="col-tgl">06 April - 08 April</td><td class="col-agenda">Masa Tenang</td>
                </tr>
                <tr>
                    <td class="col-tgl" style="background-color: #c0c0c0;color: #000000;font-weight: bold;">09 April</td>
                    <td class="col-agenda" style="background-color: #c0c0c0;color: #000000;font-weight: bold;">Pemungutan dan Penghitungan Suara (Pemilu Legislatif)</td>
                </tr>
                <tr>
                    <td class="col-tgl">25 April – 25 Mei</td><td class="col-agenda">Audit Dana Kampanye</td>
                </tr>
                <tr>
                    <td class="col-tgl">26 April – 06 Mei</td><td class="col-agenda">Rekapitulasi Hasil Penghitungan Suara Pemilu Tingkat Nasional</td>
                </tr>
                <tr>
                    <td class="col-tgl">07 Mei - 09 Mei</td><td class="col-agenda">Penetepan Hasil Pemilu Secara Nasional</td>
                </tr>
                <tr>
                    <td class="col-tgl">07 Mei - 09 Mei</td><td class="col-agenda">Penetapan Partai Politik Memenuhi Ambang Batas (PT 3%)</td>
                </tr>
                <tr>
                    <td class="col-tgl">11 Mei - 18 Mei</td><td class="col-agenda">Penetapan Perolehan Kursi dan Calon Terpilih Tingkat Nasional s/d Kabupaten/Kota</td>
                </tr>
                <tr>
                    <td class="col-tgl">Juni - September</td><td class="col-agenda">Peresmian Keanggotaan DPR-RI, DPD-RI, DPRD Provinsi, dan DPRD Kabupaten/Kota</td>
                </tr>
                <tr>
                    <td class="col-tgl" style="background-color: #c0c0c0;color: #000000;font-weight: bold;">09 Juli</td>
                    <td class="col-agenda" style="background-color: #c0c0c0;color: #000000;font-weight: bold;">Pemungutan dan Perhitungan Suara Pilpres (Pemilu Presiden)</td>
                </tr>
                <tr>
                    <td class="col-tgl">Juli - Oktober</td><td class="col-agenda">Pengucapan Sumpah dan Janji Anggota Terpilih DPR-RI, DPD-RI, DPRD Provinsi, dan DPRD Kabupaten/Kota</td>
                </tr>
            </table>
            </div>
        </div>
    </div>

</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>

            <!-- TERKINI -->

            <?php echo $skandal_mini; ?>

            <!-- HOT ISSUE -->
            <?php //echo $issue; ?>
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>
<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>
