<h4 class="kanal-title kanal-title-gray">SYARAT & KETENTUAN</h4>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div style="margin-left: 20px;padding-top: 30px;margin-right: 20px;">
            <img src="<?php echo base_url().'assets/images/header-mobile-640.jpg'; ?>" style="width: 100%;height: auto;">
                <p>Bijaks merupakan sarana komunikasi dan informasi mengenai politik di Indonesia baik melalui status, komentar, profil, foto, blog dan forum diskusi maupun perangkat lain yang ada di <strong class='brand'>bijaks.net</strong>.</p>
            <p>Sebagai pengakses atau pengguna bijaks, anda menyatakan setuju dengan beberapa syarat dan ketentuan berikut ini:</p>

            <ol>
                <li>Tidak akan menggunakan bijaks untuk kepentingan komersial, penghasutan, fitnah, pornografi maupun tindakan lainnya yang melanggar hukum, baik peraturan perundang-undangan maupun susila. <strong class='brand'>bijaks.net</strong> berhak untuk mengganti, mengubah dan menambah isi dari syarat dan ketentuan yang berlaku ini tanpa pemberitahuan sebelumnya. </li>
                <li>Resiko penggunaan bijaks, sepenuhnya di tangan masing-masing pengguna, dan sesama pengguna bertanggung jawab atas interaksi dengan pengguna lain. Bijaks, tidak bertanggung jawab akan penyalahgunaan sebagaimana yang disebutkan di atas, dan tidak berkewajiban memantau interaksi antar pengguna. </li>
                <li><strong class='brand'>bijaks.net</strong> berhak menghapus kontent dan/atau memblokir pengguna setiap saat, dengan alasan apapun. Baik karena ada laporan atau tidak, tentang tuduhan penyalahgunaan kontent dan/atau pengguna dari sesama pengguna, pihak ketiga dan pihak berwajib. </li>
                <li>Pengguna bijaks tidak mem-posting unauthorized commercial communications (iklan dan komunikasi perniagaan tanpa izin maupun terselubung), spam, virus atau kode berbahaya lainnya di <strong class='brand'>bijaks.net</strong>, serta tidak akan menggunakan <strong class='brand'>bijaks.net</strong> melalui sarana otomatis seperti robots, spider atau scrapers tanpa pemberitahuna dan ijin tertulis dari <strong class='brand'>bijaks.net</strong>.</li>
                <li>Pengguna bijaks tidak diperbolehkan mengambil informasi maupun isi yang lainnya, baik secara utuh maupun sebagian, serta mengumpulkan informasi login dan menggunakan akun kepunyaan pengguna lain.</li>
                <li>Segala informasi di <strong class='brand'>bijaks.net</strong> merupakan hal yang sangat penting, <strong class='brand'>bijaks.net</strong> akan berusaha untuk menjaga setiap data dan informasi aman, tapi <strong class='brand'>bijaks.net</strong> tidak dapat memberikan jaminan.</li>
                <li>Laporan penyalahgunaan kontent dikirimkan melalui alamat email: <a href='&#109;&#97;&#105;&#108;&#116;&#111;:&#97;&#100;&#109;&#105;&#110;&#105;&#115;&#116;&#114;&#97;&#116;&#111;&#114;&#64;&#98;&#105;&#106;&#97;&#107;&#115;&#46;&#110;&#101;&#116;'>&#97;&#100;&#109;&#105;&#110;&#105;&#115;&#116;&#114;&#97;&#116;&#111;&#114;&#64;&#98;&#105;&#106;&#97;&#107;&#115;&#46;&#110;&#101;&#116;</a>.</li>
            </ol>
        </div>
    </div>
</div>
<hr class="line-mini">
<br/>
