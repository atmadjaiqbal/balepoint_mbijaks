<h4 class="kanal-title kanal-title-gray">KEBIJAKAN PRIVASI</h4>
<div class="container" style="margin-top: 31px;">
        <div style="margin-left: 20px;margin-right: 20px;">
                <ol>
                    <li>Ketika anda mendaftar menjadi pengguna <strong class='brand'>bijaks.net</strong> anda diminta untuk memberikan informasi dengan sebenar-benarnya, baik informasi dasar berupa nama dan alamat email, maupun informasi lengkap berupa foto, jenis kelamin, tanggal lahir, alamat, nomor telepon dan biografi anda. <strong class='brand'>bijaks.net</strong> tidak akan pernah meminta anda untuk memberikan informasi tambahan lainnya, berupa informasi keuangan anda.</li>
                    <li>Segala bentuk informasi yang anda berikan, bersifat publik dan bisa dilihat oleh semua pengguna <strong class='brand'>bijaks.net</strong>. Karena itu, harap berhati-hati dan selektif dalam memberikan informasi, yang dapat diakses oleh publik. <strong class='brand'>bijaks.net</strong> tidak bertanggung jawab atas penyalahgunaan informasi,  yang anda bagikan.</li>
                    <li><strong class='brand'>bijaks.net</strong> berhak dan wajib memberikan data maupun informasi yang ada, untuk kepentingan hukum yang berlaku di Indonesia. <strong class='brand'>bijaks.net</strong> berhak menggunakan data yang ada untuk kepentingan lain, tanpa pemberitahuan sebelumnya kepada pengguna. Karena semua data dan informasi yang ada di <strong class='brand'>bijaks.net</strong>, merupakan milik <strong class='brand'>bijaks.net</strong>. Segala penggunaan materi yang ada di <strong class='brand'>bijaks.net</strong> harus mendapatkan ijin tertulis dari <strong class='brand'>bijaks.net</strong>.</li>
                    <li><strong class='brand'>bijaks.net</strong> berhak untuk mengganti, mengubah dan menambah kebijakan privasi ini, tanpa pemberitahuan terlebih dahulu.</li>
                </ol>
        </div>
    </div>

</div>
