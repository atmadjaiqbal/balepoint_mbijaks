<div class="container" style="margin-top: 31px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right category-news-title-merah">
                    <h1>KONTAK</h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe='<?php echo $topic_last_activity; ?>'></div>
    <div id='disclaimer' class="sub-header-container"></div>

    <div class="sub-header-container" style="background-color: #fff;margin-top:170px;">
        <div style="margin-left: 20px;padding-top: 30px;margin-right: 20px;">
            <p>Kami mengharapkan partisipasi aktif Anda dalam memberi masukan, saran serta kritik membangun, karena ini akan membuat kami lebih baik.</p><p>Silakan hubungi kami di:
            <a href='&#109;&#97;&#105;&#108;&#116;&#111;:&#97;&#100;&#109;&#105;&#110;&#105;&#115;&#116;&#114;&#97;&#116;&#111;&#114;&#64;&#98;&#105;&#106;&#97;&#107;&#115;&#46;&#110;&#101;&#116;'>&#97;&#100;&#109;&#105;&#110;&#105;&#115;&#116;&#114;&#97;&#116;&#111;&#114;&#64;&#98;&#105;&#106;&#97;&#107;&#115;&#46;&#110;&#101;&#116;</a></p>
        </div>
    </div>

</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
            <?php echo $hot_profile; ?>

            <!-- TERKINI -->

            <?php echo $skandal_mini; ?>

            <!-- HOT ISSUE -->
            <?php //echo $issue; ?>
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>
<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>
