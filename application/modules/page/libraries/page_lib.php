<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_Lib {

    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->db = $this->CI->load->database('slave', true);
    }

    public function get_event($select='*', $limit=0, $offset=0, $order_by='no_urut')
    {
        $this->db->select($select);
        $this->db->from('tcontent_event');
        if($limit != 0)
        {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by($order_by);
        $data = $this->db->get();
        return $data;
    }

}

?>