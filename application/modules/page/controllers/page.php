<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends Application {

	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
        $this->load->library('page/page_lib');
        $this->load->library('aktor/aktor_lib');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();

        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->module('news/news');
        $this->load->module('headline');
        $this->load->module('survey');

	}

	
	public function index()
	{
		$this->tentang();		
	}


	public function tentang()
	{

        $data = $this->data;
        $html['html']['content'] = $this->load->view('page/tentang', $data, true);
        $this->load->view('m_tpl/layout', $html);
	}

    public function quickcount()
    {
        $data = $this->data;
        $this->load->view('page/mobile/quickcount', $data);
    }

    public function pilpres()
    {
        $data = $this->data;

        $prabowo = $this->redis_slave->get('profile:detail:prabowosubiyantodjojohadikusumo50c1598f86d91');
        $data['prabowo'] = @json_decode($prabowo, true);
        $hatta = $this->redis_slave->get('profile:detail:hattarajasa50459f43e3b51');
        $data['hatta'] = @json_decode($hatta, true);
        $jokowi = $this->redis_slave->get('profile:detail:irjokowidodo50ee1dee5bf19');
        $data['jokowidodo'] = @json_decode($jokowi, true);
        $kalla = $this->redis_slave->get('profile:detail:drshmuhammadjusufkalla50ee870b99cc9');
        $data['kalla'] = @json_decode($kalla, true);

        $count_prabowo = $this->aktor_lib->get_count_post('prabowosubiyantodjojohadikusumo50c1598f86d91', 'prabowosubiyantodjojohadikusumo50c1598f86d91');
        foreach($count_prabowo->result_array() as $val){
           $data['prabowo_count_'.$val['tipe']] = $val['total'];
        }

        $count_hatta = $this->aktor_lib->get_count_post('hattarajasa50459f43e3b51', 'hattarajasa50459f43e3b51');
        foreach($count_hatta->result_array() as $val){
            $data['hatta_count_'.$val['tipe']] = $val['total'];
        }

        $count_jokowi = $this->aktor_lib->get_count_post('irjokowidodo50ee1dee5bf19', 'irjokowidodo50ee1dee5bf19');
        foreach($count_jokowi->result_array() as $val){
            $data['jokowi_count_'.$val['tipe']] = $val['total'];
        }

        $count_kalla = $this->aktor_lib->get_count_post('drshmuhammadjusufkalla50ee870b99cc9', 'drshmuhammadjusufkalla50ee870b99cc9');
        foreach($count_kalla->result_array() as $val){
            $data['kalla_count_'.$val['tipe']] = $val['total'];
        }

        $topik_prabowo = $this->aktor_lib->get_news_by_aktor('prabowosubiyantodjojohadikusumo50c1598f86d91', 5, 0);
        $rkey_min = 'news:detail:'; $result = array();
        $have_redis_empty = false;
        foreach($topik_prabowo->result_array() as $row=>$val)
        {
            $newsprabo = $this->redis_slave->get($rkey_min.$val['news_id']);
            $berprabo_json = json_decode($newsprabo, true);
            if(empty($berprabo_json)){ $have_redis_empty = true; continue;}
            $berprabo_json['entry_date'] = $val['entry_date'];
            $news_prabowo[$row] = $berprabo_json;
        }

        $topik_hatta = $this->aktor_lib->get_news_by_aktor('hattarajasa50459f43e3b51', 5, 0);
        $rkey_min = 'news:detail:'; $result = array();
        $have_redis_empty = false;
        foreach($topik_hatta->result_array() as $row=>$val)
        {
            $newshatta = $this->redis_slave->get($rkey_min.$val['news_id']);
            $berhatta_json = json_decode($newshatta, true);
            if(empty($berhatta_json)){ $have_redis_empty = true; continue;}
            $berhatta_json['entry_date'] = $val['entry_date'];
            $news_hatta[$row] = $berhatta_json;
        }

        $topik_jokowi = $this->aktor_lib->get_news_by_aktor('irjokowidodo50ee1dee5bf19', 5, 0);
        $rkey_min = 'news:detail:'; $result = array();
        $have_redis_empty = false;
        foreach($topik_jokowi->result_array() as $row=>$val)
        {
            $newsjoko = $this->redis_slave->get($rkey_min.$val['news_id']);
            $berjoko_json = json_decode($newsjoko, true);
            if(empty($berhatta_json)){ $have_redis_empty = true; continue;}
            $berjoko_json['entry_date'] = $val['entry_date'];
            $news_jokowi[$row] = $berjoko_json;
        }

        $topik_kalla = $this->aktor_lib->get_news_by_aktor('drshmuhammadjusufkalla50ee870b99cc9', 5, 0);
        $rkey_min = 'news:detail:'; $result = array();
        $have_redis_empty = false;
        foreach($topik_kalla->result_array() as $row=>$val)
        {
            $newskalla = $this->redis_slave->get($rkey_min.$val['news_id']);
            $berkalla_json = json_decode($newskalla, true);
            if(empty($berkalla_json)){ $have_redis_empty = true; continue;}
            $berkalla_json['entry_date'] = $val['entry_date'];
            $news_kalla[$row] = $berkalla_json;
        }

        $arrSuksesi = array();
        $resultSuksesi = $this->redis_slave->lrange("suksesi:list:hot", 0, 0);
        if(count($resultSuksesi))
        {
            foreach($resultSuksesi as $key => $value)
            {
                $rowSuksesi = $this->redis_slave->get('suksesi:detail:'.$value);
                $arrsuksesi = @json_decode($rowSuksesi, true);
                $rsSuksesi[$key] = $arrsuksesi;
            }
        }

        $data['news_prabowo'] = $news_prabowo;
        $data['news_hatta'] = $news_hatta;
        $data['news_jokowi'] = $news_jokowi;
        $data['news_kalla'] = $news_kalla;
        $data['suksesi'] = $rsSuksesi;

        $html['html']['content'] = $this->load->view('page/pilpres', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    function about()
    {
        $this->load->view('about');
    }

    function contact()
    {
        $this->load->view('contact');
    }

    function privacy()
    {
        $this->load->view('privacy');
    }

    function term_condition()
    {
        $data = $this->data;
        $html['html']['content'] = $this->load->view('page/term_condition', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

	public function kontak()
	{
        $data = $this->data;
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['topic_last_activity'] =  '';

        $data['hot_profile'] = $this->profile->hot('true');
        $data['terkini'] = $this->news->sesi('0', 0, 'true');

//               $data['issue'] = $this->news->sesi('hot-issues', 0, 'true');
        $data['survey'] = $this->survey->mini_survey();
        $data['reses']    = $this->headline->reses_list();
        $data['skandal_mini'] = $this->scandal->sesi('true');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 9, 'true');
        $html['html']['content']  = $this->load->view('page/kontak', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
	}


	public function privasi()
	{
        // $data = $this->data;
        // $data['title'] = "Bijaks | Life & Politics";
        // $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        // $data['topic_last_activity'] =  '';

        // $data['hot_profile'] = $this->profile->hot('true');
        // $data['terkini'] = $this->news->sesi('0', 0, 'true');

        // $data['survey'] = $this->survey->mini_survey();
        // $data['reses']    = $this->headline->reses_list();
        // $data['skandal_mini'] = $this->scandal->sesi('true');

        // $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
        // $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
        // $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');

        // $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
        // $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
        // $data['internasional'] = $this->news->sesi('internasional', 9, 'true');
        // $html['html']['content']  = $this->load->view('page/privasi', $data, true);
        // $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        // $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        // $this->load->view('template/tpl_one_column', $html, false);
        $data = $this->data;
        $html['html']['content'] = $this->load->view('page/privasi', $data, true);
        $this->load->view('m_tpl/layout', $html);

	}

	public function syarat_ketentuan()
	{
        $data = $this->data;
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts']     = array('bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['topic_last_activity'] =  '';

        $data['hot_profile'] = $this->profile->hot('true');
        $data['terkini'] = $this->news->sesi('0', 0, 'true');

//               $data['issue'] = $this->news->sesi('hot-issues', 0, 'true');
        $data['survey'] = $this->survey->mini_survey();
        $data['reses']    = $this->headline->reses_list();
        $data['skandal_mini'] = $this->scandal->sesi('true');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 9, 'true');
        $html['html']['content']  = $this->load->view('page/syarat_ketentuan', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
	}

    public function jadwalpemilu()
    {
        $data = $this->data;
        $data['title'] = "Bijaks | Life & Politics";
        
        $limit = 20;
        $page = 1;
        $per_page = $this->input->get('per_page');
        if(!is_bool($per_page)){
            if(!empty($per_page)){
                $page = $per_page;  
            }
        }

        $offset = ($limit*$page) - $limit;
        // echo $offset;

        $qr = $_SERVER['QUERY_STRING'];

        $user_count = $this->page_lib->get_event('count(*) co', 0, 0)->row_array(0);

        $user_count = $user_count['co'];

        $result = $this->page_lib->get_event('*', $limit, $offset)->result_array();

        $this->load->library('pagination');

        $config['base_url'] = base_url().'page/jadwalpemilu/?'; //.$qr;
        $config['total_rows'] = $user_count;
        $config['per_page'] = $limit; 
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2; // round($choice);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';
        $config['cur_tag_open'] = '<li class="disabled"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '...';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '...';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;

        $this->pagination->initialize($config); 

        $data['pagi'] =  $this->pagination->create_links();
        $data['event'] = $result;
        $data['offset'] = $offset;
        $data['total']  = $user_count;

        $html['html']['content'] = $this->load->view('mobile/jadwal_event', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function jadwal_list()
    {
        $page = $this->uri->segment(3);
        $limit = 20;
        $offset = ($limit*intval($page)) - $limit;
        $list_jadwal = $this->page_lib->get_event('*', $limit, $offset)->result_array();
        $data['result'] = $list_jadwal;
        $data['count_result'] = count($list_jadwal);
        $this->load->view('jadwal_list', $data);
    }
	
}