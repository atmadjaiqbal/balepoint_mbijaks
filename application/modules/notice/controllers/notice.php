<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notice extends Application {


	public function __construct()
	{
		parent::__construct();

	}

	function error_404(){
		$html['html']['content'] = $this->load->view('mobile/404_error', $data, true);
        $this->load->view('m_tpl/layout', $html);
	}
}