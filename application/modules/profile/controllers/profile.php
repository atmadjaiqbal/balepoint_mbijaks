<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('CACHE_PROFILE_MENU', 'PROFILE'.DIRECTORY_SEPARATOR.'PROFILE_MENU');

class Profile extends Application {

    private $user = array();
    function __construct()
    {
        parent::__construct();

        $this->load->library('cache');
        $this->load->library('typography');
        $this->load->library('politik/politik_lib');

        date_default_timezone_set('Asia/Jakarta');
        $data = $this->data;

//        $this->load->library('timeline/timeline_lib');
        $this->load->module('timeline/timeline');
        $this->load->module('news/news');

        $this->load->library('politik/politik_lib');
        $this->load->library('profile_lib');
        $this->load->library('news/news_lib');

        $this->load->library('scandal/scandal_lib');
        $this->load->module('timeline');
        $this->load->module('scandal');
        $this->load->module('profile');
        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->member = $this->session->userdata('member');
    }

    function getHeaderMenu()
    {
        //$return = $this->cache->get(CACHE_PROFILE_MENU);
        //if (!$return) $return = $this->profile_header();
        $return = $this->profile_header();
        return $return;
    }

    function hot($return='false')
    {
        $profile_hot_list = $this->getProfileCache(0,'hot', 28, 0);
        $data['hot_profile'] = $profile_hot_list;

//        $profile_hot_small = $this->getProfileCache(0, 'hot',6, 12);
//        $data['hot_profile_small'] = $profile_hot_small;

        if($return == 'true')
            return $this->load->view('template/profile/tpl_mini_hot_profile', $data, true);

        $this->load->view('template/profile/tpl_mini_hot_profile', $data);

    }

    private function getProfileCache($id=0, $result='hot', $limit=7, $offset=0)
    {
        $profile_keys = 'profile:detail:';
        if($result == 'hot')
        {
            $rrange = $offset + $limit;
            $hot_keys = 'profile:list:hot';
            $page_id_list = $this->redis_slave->lRange($hot_keys, $offset, $rrange);
            $profile_array = array();
            foreach($page_id_list as $row=>$val){
                $profile = $this->redis_slave->get($profile_keys.$val);
                $profiles = @json_decode($profile, true);
                $profile_array[$row] = $profiles;
            }
            return $profile_array;

        }else{
            return '';
        }
    }

    function sesi($return='false')
    {
        $select = 'p.page_id, p.page_name, p.profile_content_id, ta.attachment_title';
        $where = array('p.publish' => 1);
        $order = 'p.publish_date desc';
        $hot_profile = $this->politik_lib->getProfile($select, $where, 6, 0, $order);

        $result = $hot_profile->result_array();
        foreach ($result as $key => $value) {
            $select = 'content_id';
            $where = array('content_group_type' => 14, 'page_id' => $value['page_id']);
            $result[$key]['content_id'] = '';
            $ct = $this->profile_lib->getProfileContent($select, $where, $limit=1, $offset=0);
            if($ct->num_rows() > 0){
                $content = $ct->row_array();
                $result[$key]['content_id'] = $content['content_id'];
            }
        }


        $data['hot_profile'] = $result;
        // echo "<pre>";
        // var_dump($data);
        // echo "</pre>";

        if($return == 'true')
            return $this->load->view('template/profile/tpl_mini_hot_profile', $data, true);

        $this->load->view('template/profile/tpl_mini_hot_profile', $data);

    }

    function listArticle($page_id)
    {
        $arr_detail = array(); $pro_news = array();
        $data = $this->data;
        $data['scripts']     = array('bijaks.profile');
        $detailProfile = $profile_detail = $this->redis_slave->get('profile:detail:'.$page_id);
        $data['detailProf'] = @json_decode($detailProfile, true);
        $data['topic_last_activity'] = 'profile';
        $porfile_news = $this->politik_lib->GetProfileBerita($page_id);

        $_i = 0;
        foreach($porfile_news as $row)
        {
            $content_id = $row['content_id'];
            $news_id = $row['news_id'];
            $topic_id = $row['topic_id'];
            $content_url = $row['content_url'];

            $news_detail = $this->redis_slave->get('news:detail:'.$news_id);
            $news_array = @json_decode($news_detail, true);
            $pro_news[$_i] = $news_array;
            $pro_news[$_i]['news_id'] = $news_id;

            if(!empty($pro_news))
            {
                $pro_news['topic_id'] = $news_array['categories'][0]['id'];
            }
            $_i++;
        }

        $data['news'] = $pro_news;
        $data['count_news'] = count($porfile_news);
        $data['count_scandal'] = count($data['detailProf']['scandal']);
        $data['count_photo'] = count($data['detailProf']['profile_photo']);

        $data['category'] = 'profile';
        $data['terkini'] = $this->news->sesi('0', 0, 'true');
        $data['reses'] = $this->news->sesi('reses', 0, 'true');
        $data['issue'] = $this->news->sesi('hot-issues', 0, 'true');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 1, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 1, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 1, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 1, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 1, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 1, 'true');

        $html['html']['content']  = $this->load->view('profile/profile_news', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }

    function listScandals($page_id)    
    {
        $data = $this->data;        
        $data['scripts']     = array('bijaks.profile');
        $detailProfile = $profile_detail = $this->redis_slave->get('profile:detail:'.$page_id);
        $data['detailProf'] = @json_decode($detailProfile, true);
        $data['topic_last_activity'] = 'profile';
        $this->user = $this->politik_lib->isBijakUser($page_id);
        $user = $this->user;
        $data['user'] = $user;

        $data['is_teman'] = 0;
        if($page_id != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if($is_friend['total'] > 0) $data['is_teman'] = 1;
        }
        //check if followe
        $data['is_follow'] = 0;
        if($page_id != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = array('account_id' => $this->member['account_id'], 'page_id' => $user['user_id']);
            $dt = $this->komunitas_lib->is_follow($select, $where)->row_array();
            if($dt['total'] > 0) $data['is_follow'] = 1;
        }

        foreach($data['detailProf']['scandal'] as $key => $value)
        {
          $skandalProfile[$key] = $value;
          $scanPhoto = $this->redis_slave->get('scandal:photo:'.$value['scandal_id']);
          $skandalProfile[$key]['photos'] = @json_decode($scanPhoto, true);
          $skandal_players = $this->redis_slave->get('scandal:players:'.$value['scandal_id']);
          $skandalProfile[$key]['players'] = @json_decode($skandal_players, true);
          $skandal_detail = $this->redis_slave->get('scandal:min:'.$value['scandal_id']);
          $skandalProfile[$key]['details'] = @json_decode($skandal_detail, true);                    
        }

        $data['scandalnews'] = $skandalProfile;
        $porfile_news = $this->politik_lib->GetProfileBerita($page_id);
        $data['count_news'] = count($porfile_news);

        $data['category'] = 'profile';
        $data['count_scandal'] = count($data['detailProf']['scandal']);
        $data['count_photo'] = count($data['detailProf']['profile_photo']);

        $data['terkini'] = $this->news->sesi('0', 0, 'true');
        $data['reses'] = $this->news->sesi('reses', 0, 'true');
        $data['issue'] = $this->news->sesi('hot-issues', 0, 'true');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 1, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 1, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 1, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 1, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 1, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 1, 'true');

        $html['html']['content']  = $this->load->view('profile/profile_scandals', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);    	    	    	
    }


    function detailprofile($page_id)
    {
        $arr_detail = array();
        $data = $this->data;
        $data['scripts']     = array('bijaks.profile', 'bijaks.komunitas.profile');
        $detailProfile = $profile_detail = $this->redis_slave->get('profile:detail:'.$page_id);
        $data['topic_last_activity'] = 'profile';
        $data['detailProf'] = @json_decode($detailProfile, true);

        $this->user = $this->politik_lib->isBijakUser($page_id);
        $user = $this->user;
        $data['user'] = $user;

        $data['is_teman'] = 0;
        if($page_id != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = "(tf.account_id = '".$this->member['account_id']."' and tf.friend_account_id = '".$user['account_id']."') OR (tf.friend_account_id = '".$this->member['account_id']."' and tf.account_id = '".$user['account_id']."')";
            $is_friend = $this->komunitas_lib->get_friend($select, $where)->row_array();
            if($is_friend['total'] > 0) $data['is_teman'] = 1;
        }
        //check if followe
        $data['is_follow'] = 0;
        if($page_id != $this->member['user_id']){
            $select = 'count(*) as total';
            $where = array('account_id' => $this->member['account_id'], 'page_id' => $user['user_id']);
            $dt = $this->komunitas_lib->is_follow($select, $where)->row_array();
            if($dt['total'] > 0) $data['is_follow'] = 1;
        }

        $porfile_news = $this->politik_lib->GetProfileBerita($page_id);
        foreach($porfile_news as $row)
        {
            $content_id = $row['content_id'];
            $news_id = $row['news_id'];
            $topic_id = $row['topic_id'];
            $content_url = $row['content_url'];

            $news_detail = $this->redis_slave->get('news:detail:'.$news_id);
            $news_array = @json_decode($news_detail, true);
            $pro_news = $news_array;
            $pro_news['topic_id'] = $news_array['categories'][0]['id'];
        }

        $data['count_news'] = count($porfile_news);

        $data['category'] = 'profile';
        $data['count_scandal'] = count($data['detailProf']['scandal']);
        $data['count_photo'] = count($data['detailProf']['profile_photo']);

        $data['terkini'] = $this->news->sesi('0', 0, 'true');
        $data['reses'] = $this->news->sesi('reses', 0, 'true');
        $data['issue'] = $this->news->sesi('hot-issues', 0, 'true');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 1, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 1, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 1, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 1, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 1, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 1, 'true');

        $html['html']['content']  = $this->load->view('profile/profile_detail', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
        $this->load->view('template/tpl_one_column', $html, false);
    }


    function sublistprofile($_profileNode)
    {
        $data = $this->data;
        $data['category'] = 'hot-profile';
        $data['ProfileTitle'] = $this->input->get("header", true);
        $page_id = $this->input->get("page_id", true);
        $data['profileheader'] = $this->getHeaderMenu();
        //$data['rsProfile'] = $this->profile_lib->GetDetailStrukture($_profileNode);
        $data['topic_last_activity'] =  'profile';

        $keyStruk = "structure:group:";
        $listStruk = $this->redis_slave->get($keyStruk.$_profileNode);
        $arrStruk = @json_decode($listStruk, true);
        $listArr = array(); $arrProf = array(); $resultProfile = array();
        if(count($arrStruk))
        {
            $resultProfile = $arrStruk; $total_data = 0;
            foreach($arrStruk as $key => $value)
            {
                $listStruk2 = $this->redis_slave->get($keyStruk.$value['id_structure_node']);
                $arrStruk2 = @json_decode($listStruk2, true);
                $resultStruk = $arrStruk2;
                $i = 0;
                if(count($resultStruk))
                {
                  foreach($resultStruk as $rowProfile)
                  {
                    $detailProfile = $this->redis_slave->get('profile:detail:'.$rowProfile['page_id']);
                    $arrProf = @json_decode($detailProfile, true);

                    if(!empty($arrProf))
                    {
                      $resultProfile[$key]['profile_detail'][$i] = $arrProf;
                    } else {

                      $profStrukture = $this->redis_slave->get($keyStruk.$rowProfile['id_structure_node']);
                      $listProfStruk = @json_decode($profStrukture, true);

                      $resultProfile[$key]['profile_structure'][$i] = $listProfStruk;
                      if(count($listProfStruk))
                      {
                          $_t = 0;
                          foreach($listProfStruk as $rownext)
                          {

                              $detailProf2 = $this->redis_slave->get('profile:detail:'.$rownext['page_id']);
                              $arrProf2 = @json_decode($detailProf2, true);
                              if(!empty($arrProf2))
                              {
                                  $resultProfile[$key]['profile_detail'][$i] = $arrProf2;
                              }

                              $_t++;

                          }

                      }

                    }
                    $i++;
                  }
                  $resultProfile[$key]['count_profile'] = $i;
                  $total_data = ($total_data + $resultProfile[$key]['count_profile']);
                  $resultProfile['count_total'] = $total_data;
                } else {
                  $resultProfile[$key]['testing'] = $arrStruk2;
                }
            }
        }

        $data['rsProfile'] = $resultProfile;

        $data['terkini'] = $this->news->sesi('0', 0, 'true');
        $data['reses'] = $this->news->sesi('reses', 0, 'true');
        $data['issue'] = $this->news->sesi('hot-issues', 0, 'true');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 1, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 1, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 1, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 1, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 1, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 1, 'true');

        if(count($data['rsProfile']) > 1)
        {
            $html['html']['content']  = $this->load->view('profile/profile_sublist', $data, true);
            $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
            $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);;
            $this->load->view('template/tpl_one_column', $html, false);
        } else {
            $this->detailprofile($page_id);
        }
    }


    public function SubTemplateHeader()
    {
       $data['tipe'] = "profile";
       $data['category'] = "hot-profile";
       echo $this->load->view('template/tpl_sub_header', $data);
    }

    public function submenu()
    {
        $node_id = $this->input->get('id', true);
        $data['submenu'] = $this->profile_lib->GetStrukturByParentNode($node_id);
        echo $this->load->view('profile/profile_menu_simple', $data, true);

    }

    public function getStrukturData($nodeID)
    {
        $data = $this->data;
        $this->output->set_content_type('application/json');
        $data_struktur = $this->profile_lib->GetStrukturByParentNode($nodeID);
        echo json_encode($data_struktur);
    }


    public function profile_header()
    {
        $data = $this->data;
        $data['topic_last_activity'] = '';
//        $data['comment_latest'] = $this->GetLastComment('#fe4200');
//        $data['like_latest']    = $this->GetLastLike('#ff801a');
//        $data['dislike_latest'] = $this->GetLastDislike('#fead00');

        $data['eksekutif'] = $this->profile_lib->GetAllStruktur('20708');
        $data['legislatif'] = $this->profile_lib->GetAllStruktur('21323');
        $data['yudikatif'] = $this->profile_lib->GetAllStruktur('21326');
        $data['partai'] = $this->profile_lib->GetAllStruktur('8591');
        $data['perusahaan'] = $this->profile_lib->GetAllStruktur('5');

        $data['player'] = $this->profile_lib->GetAllStruktur('6');
        $return = $this->load->view('profile/template/profile_header', $data, true);
        $this->cache->write($return, CACHE_PROFILE_MENU);
        return $return;

    }

    public function wall($page_id=NULL)
    {
        $detailProfile = $profile_detail = $this->redis_slave->get('profile:detail:'.$page_id);
        $data['topic_last_activity'] = 'profile';
        $data['detailProf'] = @json_decode($detailProfile, true);
        $data['count_scandal'] = count($data['detailProf']['scandal']);
        $data['count_photo'] = count($data['detailProf']['profile_photo']);

        if(empty($page_id))
            show_404();

//        $this->isLogin();
//        if($this->isLogin(true) == false){
////            $data['rcode'] = 'bad';
////            $data['msg'] = 'Anda harus login !';
////            $this->message($data);
//            show_404();
//        }
//        $user = $this->user;

        $user = $this->politik_lib->GetPolitikByUserID($page_id);
        $data['title'] = "Bijaks | Life & Politics";
        $data['scripts'] = array('bijaks.js', 'bijaks.profile.js');
        $data['category'] = 'profile';
        $data['topic_last_activity'] = 'bijak';

        $count_post = $this->politik_lib->GetCountPostPolitic($page_id);
        foreach($count_post as $val_post)
        {
            $user['count_'.$val_post['tipe']] = $val_post['total'];
        }

        $select = 'tf.*,ta.user_id, ta.display_name, tat.attachment_title';
        $where = array('tf.account_id' => $page_id);
        $friend = $this->komunitas_lib->get_friend($select, $where, 17, $offset=0);
        $user['friend'] = $friend->result_array();

        $data['user'] = $user;

        $limit = 9;

        $page = 6;

        $offset = ($limit*$page) - $limit;
        if($offset != 0) $offset += ($page - 1);
//        $data['wall_list'] = $this->get_wall_list($user['account_id'], $user['user_id'], $limit, $offset);

        $html['html']['content']  = $this->load->view('profile_wall', $data, true);
        $html['html']['header']   = $this->load->view('template/tpl_header', $html, true);
        $html['html']['footer']   = $this->load->view('template/tpl_footer', $html, true);
        $this->load->view('template/tpl_one_column', $html);
    }

    public function walllist($page_id='')
    {
        $limit = 9;
        $account_id = $this->input->post('id');
        $page = $this->input->post('page');
        if(empty($account_id) || is_bool($account_id)){
            echo '<p>Tidak ada aktifitas user</p>';
            return;
        }

        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){
            $offset += (intval($page) - 1);
        }
        $this->isBijaksUser($page_id[0]);
        $data['user'] = $this->user;
        $data['wall_list'] = $this->get_wall_list($account_id, $page_id[0], $limit, $offset);

        $this->load->view('template/komunitas/tpl_komunitas_wall', $data);

    }

    public function GetLastComment($BackColor=null)
    {
        $comunity = $this->load->module('ajax');
        $data['rsSocial'] = $comunity->GetCommentByContentGroupType('12');
        $data['BackColor'] = $BackColor;
        $data['width'] = "320px;";
        return $this->load->view('home/home_comunity_update', $data, true);
    }

    public function GetLastLike($BackColor=null)
    {
        $comunity = $this->load->module('ajax');
        $data['rsSocial'] = $comunity->GetLikeByContentGroupType('12');
        $data['BackColor'] = $BackColor;
        $data['width'] = "320px;";
        return $this->load->view('home/home_comunity_update', $data, true);
    }

    public function GetLastDislike($BackColor=null)
    {
        $comunity = $this->load->module('ajax');
        $data['rsSocial'] = $comunity->GetDisikeByContentGroupType('12');
        $data['BackColor'] = $BackColor;
        $data['width'] = "320px;";
        return $this->load->view('home/home_comunity_update', $data, true);
    }

    public function GetLastUpdateComunity($BackColor=null, $topicID)
    {
        $comunity = $this->load->module('ajax');
        $data['rsSocial'] = $comunity->GetLastSocialInfoByTopicID($topicID);
        $data['BackColor'] = $BackColor;
        $data['width'] = "300px;";
        return $this->load->view('home/home_comunity_update', $data, true);
    }

}