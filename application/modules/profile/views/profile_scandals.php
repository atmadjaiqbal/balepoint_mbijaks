<div class="container">
    <?php $this->load->view('template/tpl_header_profile'); ?>
</div>
<div class="container" id="profile-detail">
    <div class="sub-header-container">
        <div class="badge-info">    
         <?php echo $this->load->view('template/aktor/tpl_aktor_side'); ?>
        </div>   
        <div class="profile-info">
            <div class="profile-name">
                <p>Skandal <?php echo $detailProf['page_name']; ?></p>
            </div>
            <div class="row-fluid" id="profile-skandal">
            <?php
              if(isset($scandalnews))
              {
                foreach($scandalnews as $rwscandal)
                {
                  $data['skandals'] = $rwscandal;
                  $this->load->view('template/profile/tpl_profile_scandals', $data);
                }
              }
            ?>
            </div>

        </div>
    </div>
</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- TERKINI -->
            <?php echo $terkini; ?>

            <!-- RESES -->
            <?php echo $reses; ?>

            <!-- HOT ISSUE -->
            <?php echo $issue; ?>

        </div>
    </div>
</div>
<br>
<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>

