<?php
$content_callback 	= base_url('ajax/count_content');
$content_like_uri 	= '';
$content_dislike_uri = '';

foreach($rsNews as $row) {
   $content_id = $row['content_id'];
   $content_date 	= mysql_date("%d %M %Y - %h:%i", strtotime($row['entry_date']));
   $image			= (!empty($row['content_image_url'])) ? config_item('news_url').'uploads'.$row['content_image_url'] : '';
   $url				= base_url() . "news/article/" . $row['topic_id'] . "-" . $row['news_id'] . "/" . $row['content_url'];

   if(!empty($account_id)){
     $content_like_uri 	= ($row['is_like'] == '0') ? base_url('ajax/like') : base_url('ajax/unlike') ;
     $content_dislike_uri = ($row['is_dislike'] == '0') ? base_url('ajax/dislike') : base_url('ajax/undislike');
   }

   $content_data	= array(
      'content_id' 		=> $content_id,
      'content_date' 	=> $content_date,
      'count_comment' 	=> $row['count_comment'],
      'count_like' 		=> $row['count_like'],
      'count_dislike' 	=> $row['count_dislike'],
      'is_like'			=> $row['is_like'],
      'is_dislike' 		=> $row['is_dislike'],
      'tipe' 			=> '2',
      'restype'			=> '2',
      'callback'		=> $content_callback,
      'like_uri'		=> $content_like_uri,
      'dislike_uri'		=> $content_dislike_uri
   );

?>
<div class="headline">
   <div class="headline-title clearfix">
      <h4><a href="<?php echo $url; ?>" title="<?php echo $row['title']; ?>"><?php echo $row['title']; ?></a></h4>
      <div class='more-space'><?php echo $content_data['content_date']?></div>
   </div>
   <div class="headline-detail clearfix">
     <div class='post-pic'>
       <?php // echo $this->load->view('ajax/count_content_action', $content_data); ?>
       <a href="<?php echo $url;?>"><img src="<?php echo $image;?>" alt="" style="width: 300px; height: 160px;"/></a>
     </div>
     <div class="related-politicians clearfix">
       <span class="pull-left related-politician-label">Politisi Terkait :</span>
       <span class="pull-left related-politician-img"><?php echo $row['politisi_terkait']; ?></span>
     </div>

   </div>
</div>
<div class="col-6 social">
   <div id="home-comunity"><?php echo $last_comunity; ?></div>
</div>
<?php
    }
?>

