<?php foreach ($rsNews as $row) {   ?>
  <div class="media">
    <div class="media-image pull-left" style="background:
         url('<?php echo config_item('news_url').'uploads'.$row['content_image_url']; ?>') no-repeat; background-position : center; background-size:auto 80px;">
         <!-- <a class="pull-left" href="<?php //echo $value['news_url']; ?>"> -->
         <!-- <img alt="<?php echo $row['title']; ?>" class="media-object" src="<?php echo $row['content_image_url']; ?>"> -->
         <!-- </a> -->
    </div>
    <div class="media-body">
       <h5 class="media-heading"><a title="<?php echo $row['title']; ?>" href="<?php echo $row['content_url']; ?>"><?php echo (strlen($row['title']) > 24) ? substr($row['title'], 0, 24).'...' : $row['title']; ?></a></h5>
       <p><?php echo date('d M Y - h:i', strtotime($row['content_date'])); ?></p>
    </div>
  </div>
<?php } ?>
