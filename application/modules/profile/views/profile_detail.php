<div class="container">
    <?php $this->load->view('template/tpl_header_profile'); ?>
</div>
<div class="container" id="profile-detail" style="margin-top:-130px;">
    <div class="sub-header-container">
       <?php echo $this->load->view('template/profile/tpl_profile_side'); ?>
        <div class="profile-info">
           <div class="profile-name">
               <p <?php echo (strlen($detailProf['page_name']) > 23 ? 'style="font-size:20px;"' : '');?>>
                   <?php echo $detailProf['page_name']; ?>
               </p>
               <div style="float:right; margin-top:-28px;">
                   <?php if($this->member['user_id']){?>
                       <?php if($user['user_id'] != $this->member['user_id']){ ?>
                           <a data-id="<?=$user['user_id'];?>" data-follow="<?php echo $is_follow; ?>" id="follow" href="#" class="btn-flat btn-flat-large btn-flat-dark"><?=($is_follow == 0) ? 'FOLLOW <i class="icon-ok icon-white"></i>' : 'UNFOLLOW <i class="icon-remove icon-white"></i>';?></a>
                       <?php } ?>
                   <?php } ?>
               </div>

           </div>
           <div class="about">
               <?php echo $this->typography->auto_typography($detailProf['about'], true); ?>
           </div>


            <div class="div-line"></div>
            <div class="row-fluid">

                <h5>BERI KOMENTAR</h5>
                <div class="media media-comment">
                    <div class="pull-left media-side-left">
                        <div style="background:
                            url('<?php if ($this->member['account_type'] == 0 ||$this->member['account_type'] == 2) { echo icon_url($this->member['xname'],'user/'.$this->member['user_id']) ; } else {  echo icon_url($this->member['xname'],'politisi/'.$this->member['user_id']) ; } ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                        </div>
                    </div>

                    <div class="media-body">
                        <div class="row-fluid">
                            <input id='comment_type' data-id="<?=$detailProf['page_id'];?>" type="text" name="comment" class="media-input" >
                        </div>
                        <div class="row-fluid">
                            <div class="span12 text-right">
                                <span>Login untuk komentar</span>&nbsp;
                                <a class="btn-flat btn-flat-dark">Register</a>
                                <a class="btn-flat btn-flat-dark">Login</a>
                                <a id="send_comment" class="btn-flat btn-flat-gray">Send</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="politisi_comment" data-page="1" data-limit="10" data-offset="0" data-id="<?=$detailProf['page_id'];?>" class="row-fluid comment-container">

            </div>
            <div class="row-fluid komu-follow-list komu-wall-list-bg">
                <div class="span12 text-center">
                    <a data-page="1" data-id="<?=$detailProf['page_id'];?>" id="politisi_comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                </div>
            </div>
            <div class="div-line"></div>

        </div>
    </div>
</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- TERKINI -->
            <?php echo $terkini; ?>

            <!-- RESES -->
            <?php echo $reses; ?>

            <!-- HOT ISSUE -->
            <?php echo $issue; ?>

        </div>
    </div>
</div>
<br>
<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>

