<div style="font-family: helvetica; font-size: 20px; color: #000;margin-top:20px;">
    <?php echo $ProfileTitle; ?>
</div>
<div id='sub-profile'>
    <?php
    $_y = 0;
    foreach($rsProfile as $row)
    {
        if(!empty($row['profile_detail']))
        {
        if($_y == 0 || $_y == 1)
        {
            if($_y == 0)
            {
    ?>
                <div id="politic-structure">
    <?php
            }

            if($_y == 0 || $_y == 1)
            {
                echo  '<div class="title">'.$row['structure_name'].'</div>';
            }

            foreach($row['profile_detail'] as $rwDetail)
            {
                $profileURL = base_url().'profile/detailprofile/'.$rwDetail['page_id'];

                if(!empty($rwDetail['page_name']))
                {
    ?>
                <div class="profile">
                    <div class="profile-foto">
                        <a href="<?php echo $profileURL;?>">
                            <img alt="" title="<?php echo $rwDetail['page_name']; ?>"
                                 src='<?php echo $rwDetail['badge_url']; ?>' data-src="<?php echo $rwDetail['badge_url']; ?>"
                                 data-toggle='tooltip' class='tooltip-bottom' />
                        </a>
                    </div>
                    <div class="profile-detail">
                        <a href="<?php echo $profileURL;?>" title="<?php echo $rwDetail['page_name']; ?>">
                            <strong><?php echo (strlen($rwDetail['page_name']) > 25 ? substr($rwDetail['page_name'], 0, 25).' ...' : $rwDetail['page_name']); ?></strong>
                        </a>
                        <p><?php echo (strlen($row['structure_name']) > 28 ? substr($row['structure_name'], 0, 28).' ...' : $row['structure_name']); ?></p>
                        <p><span><?php
                                if($rwDetail['icon_partai_url'] != 'None')
                                {
                                    echo '<img src="'.$rwDetail['icon_partai_url'].'" width="25px" height="25px" ';
                                } else {
                                    echo '';
                                }
                                ?></span> <?php
                            if($rwDetail['partai_name'] != 'None')
                            {
                                if(strlen($rwDetail['partai_name']) > 25)
                                {
                                    echo substr($rwDetail['partai_name'], 0, 25) . ' ...';
                                } else {
                                    echo $rwDetail['partai_name'];
                                }
                            }
                            ?></p>
                    </div>
                </div>

            <?php
                }
            }

            if($_y == 1)
            {
            ?>
                </div>
            <?php
            }

        } else {

          if($_y > 1)
          {
            if(count($row['profile_detail'] > 20))
            {
              $_brs = ceil(count($row['profile_detail'])/2);
            } else {
              $_brs = 0;
            }

            $_last = count($row['profile_detail']);
            $_i = 0;
            foreach($row['profile_detail'] as $rwDetail2)
            {
                $profileURL = base_url().'profile/detailprofile/'.$rwDetail2['page_id'];
                if($_i == 0 || $_i == ($_brs + 1))
                {
                    ?>
                    <div id="politic-structure" style="margin-left:30px;">
                    <div class="title"><?php echo $row['structure_name']; ?> </div>
                <?php
                }

                if(!empty($rwDetail2['page_name']))
                {
                    ?>

                    <div class="profile">
                        <div class="profile-foto">
                            <a href="<?php echo $profileURL;?>">
                                <img alt="" title="<?php echo $rwDetail['page_name']; ?>"
                                     src='<?php echo $rwDetail2['badge_url']; ?>' data-src="<?php echo $rwDetail2['badge_url']; ?>"
                                     data-toggle='tooltip' class='tooltip-bottom' />
                            </a>
                        </div>
                        <div class="profile-detail">
                            <a href="<?php echo $profileURL;?>" title="<?php echo $rwDetail2['page_name']; ?>">
                                <strong><?php echo (strlen($rwDetail2['page_name']) > 28 ? substr($rwDetail2['page_name'], 0, 28).' ...' : $rwDetail2['page_name']); ?></strong>
                            </a>
                            <p><?php echo (strlen($row['structure_name']) > 28 ? substr($row['structure_name'], 0, 28).' ...' : $row['structure_name']); ?></p>
                            <p><span><?php
                                    if($rwDetail2['icon_partai_url'] != 'None')
                                    {
                                        echo '<img src="'.$rwDetail2['icon_partai_url'].'" width="25px" height="25px" ';
                                    } else {
                                        echo '';
                                    }
                                    ?></span> <?php
                                if($rwDetail2['partai_name'] != 'None')
                                {
                                    if(strlen($rwDetail2['partai_name']) > 25)
                                    {
                                        echo substr($rwDetail2['partai_name'], 0, 25) . ' ...';
                                    } else {
                                        echo $rwDetail2['partai_name'];
                                    }
                                }
                                ?></p>
                        </div>
                    </div>

                <?php
                }
                if($_i == $_brs || $_i == $_last)
                {
                    ?>
                    </div>
                <?php
                }

                $_i++;
            }

          }  // ---> y > 1
        }  // y == 0 \\ y == 1

        $_y++;
      }
    } // foreach
    ?>
</div>
</div>
