<div class="sub-header-container" style="height:500px;">
    <div class='sub-header-container-bg-image' style="background: url(<?php echo $profile_photos; ?>) no-repeat; background-size: 960px 500px; "></div>
    <div class="logo-small-container">
        <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
    </div>
    <div class="right-container">
        <div class="banner">

        </div>
        <div class="category-news">
            <div class="category-news-title category-news-title-left category-news-title-<?php echo category_color(strtolower($category)); ?>">
                <h1><?php echo strtoupper($category); ?></h1>
            </div>

        </div>
    </div>

</div>
