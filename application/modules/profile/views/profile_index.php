<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<?php echo $profileheader; ?>
<br/>

<div class="container">
 <div class="sub-header-container" style="margin-bottom: 30px;">
  <!--div class="home-title-section hp-label hp-label-hot-profile" style="background-color: #FF7E00;">
     <span class="hitam"></span>
  </div-->
  <div id='politik'>
    <ul>
    <?php
      $i = 0;
      foreach ($hot_profile as $row)
      {
          if($i >= 0 && $i <= 9)
          {
    ?>
        <li>
          <a href="<?php echo base_url().'aktor/profile/'.$row['page_id']; ?>" data-id="<?php echo $row['page_id']; ?>">
            <img src='<?php echo $row['badge_url'] ; ?>'
                 data-src='<?php echo $row['badge_url'] ; ?>'
                 data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $row['page_name']; ?>"
                 alt=''/>
          </a>
        </li>
    <?php
          } else {

            if($i >= 10 && $i <= 50)
            {
    ?>
              <li class="small-politik">
                  <a href="<?php echo base_url().'aktor/profile/'.$row['page_id']; ?>" data-id="<?php echo $row['page_id']; ?>">
                      <img src='<?php echo $row['badge_url'] ; ?>'
                           data-src='<?php echo $row['badge_url'] ; ?>'
                           data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $row['page_name']; ?>"
                           alt=''/>
                  </a>
              </li>
    <?php
            }
          }

          $i++;
       }
    ?>
    </ul>
  </div>
 </div>
</div>
<br>

<?php
if($hot_partai)
{
?>
<div class="container">
    <div class="sub-header-container" style="margin-bottom: 30px;">
        <!--div class="home-title-section hp-label hp-label-hot-profile" style="background-color: #FF7E00;">
            <span class="hitam"></span>
        </div-->
        <div id='politik'>
            <ul>
                <?php
                $y = 0;
                foreach ($hot_partai as $rwPartai)
                {
                        ?>
                        <li class="small-partai">
                            <a href="<?php echo base_url().'aktor/profile/'.$rwPartai['page_id']; ?>" data-id="<?php echo $rwPartai['page_id']; ?>">
                                <img class="img-polaroid lazy" src='<?php echo $rwPartai['badge_url'] ; ?>'
                                     data-src='<?php echo $rwPartai['badge_url'] ; ?>'
                                     data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $rwPartai['page_name']; ?>"
                                     alt=''/>
                            </a>
                        </li>
                    <?php
                    $y++;
                }
                ?>
            </ul>
        </div>
    </div>
</div>
<br/>
<?php
}
?>

<?php
if($hot_lembaga)
{
?>
    <div class="container">
        <div class="sub-header-container" style="margin-bottom: 30px;">
            <!--div class="home-title-section hp-label hp-label-hot-profile" style="background-color: #FF7E00;">
                <span class="hitam"></span>
            </div-->
            <div id='politik'>
                <ul>
                    <?php
                    $y = 0;
                    foreach ($hot_lembaga as $rwLem)
                    {
                        ?>
                        <li class="small-lembaga">
                            <a href="<?php echo base_url().'aktor/profile/'.$rwLem['page_id']; ?>" data-id="<?php echo $rwLem['page_id']; ?>">
                                <img class="img-polaroid lazy" src='<?php echo $rwLem['badge_url'] ; ?>'
                                     data-src='<?php echo $rwLem['badge_url'] ; ?>'
                                     data-toggle="tooltip" class="tooltip-bottom" title="<?php echo $rwLem['page_name']; ?>"
                                     alt=''/>
                            </a>
                        </li>
                        <?php
                        $y++;
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <br/>
<?php
}
?>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- TERKINI -->
            <?php echo $mini_profile; ?>

            <!-- RESES -->
            <?php echo $skandal; ?>

            <!-- HOT ISSUE -->
            <?php echo $survey; ?>

        </div>
    </div>
</div>
<br>
<div class="container-bawah">

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $ekonomi; ?>


                <!-- HUKUM -->
                <?php echo $hukum; ?>


                <!-- PARLEMEN -->
                <?php echo $parlemen; ?>

            </div>
        </div>
    </div>
    <br>

    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- EKONOMI -->
                <?php echo $nasional; ?>


                <!-- HUKUM -->
                <?php echo $daerah; ?>


                <!-- PARLEMEN -->
                <?php echo $internasional; ?>

            </div>
        </div>
    </div>
    <br>
</div>