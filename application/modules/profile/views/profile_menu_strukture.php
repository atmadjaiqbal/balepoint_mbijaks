<?php
foreach($struktur as $row)
{
    ?>
    <li class="dropdown-submenu" data-toggle="dropdown-submenu" role="menu">
        <a href="<?php
        if($row['parent_node'] == '5')
        {
            echo base_url().'profile/sublistprofile/'.$row['id_structure_node'];
        } else {
            if($row['node_count'] > 0)
            {
                echo base_url().'profile/sublistprofile/'.$row['id_structure_node'];
            } else {
                echo '#';
            }
        }
        ?>?header=<?php echo $row['structure_name']; ?>"><?php echo $row['structure_name']; ?></a>
        <ul class="dropdown-menu">
            <?php
            foreach($row['eksekutif'] as $rwEks)
            {
                ?>
                <li <?php echo ($rwEks['node_count'] > 0) ? 'class="dropdown-submenu '.(($rwEks['parent_node'] == '14885') ? 'pull-left' : '').'" data-toggle="dropdown-submenu" role="menu"' : ''; ?>>
                    <a href="<?php
                    if($rwEks['parent_node'] == '5')
                    {
                       echo base_url().'profile/sublistprofile/'.$rwEks['id_structure_node'];
                    } else {
                       if($rwEks['node_count'] > 0)
                       {
                           echo base_url().'profile/sublistprofile/'.$rwEks['id_structure_node'];
                       } else {
                           echo '#';
                       }
                    }
                    ?>?header=<?php echo $rwEks['structure_name']; ?>">
                        <?php echo $rwEks['structure_name']; ?>
                    </a>
                    <ul class="dropdown-menu">
                        <?php
                        foreach($rwEks['pejabat'] as $rwJabat)
                        {
                            ?>
                            <li <?php echo ($rwJabat['node_count'] > 0) ? 'class="dropdown-submenu pull-left" data-toggle="dropdown-submenu" role="menu"' : ''; ?>>
                                <a href="<?php echo base_url().'profile/sublistprofile/'.$rwJabat['id_structure_node']; ?>?page_id=<?php echo $rwJabat['page_id']; ?>&header=<?php echo $rwJabat['structure_name']; ?>">
                                    <?php echo $rwJabat['structure_name']; ?>
                                </a>
                                <?php
                                if(!empty($rwJabat['subpejabat']))
                                {
                                    ?>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach($rwJabat['subpejabat'] as $rwSubJab)
                                        {
                                            ?>
                                            <li <?php echo ($rwSubJab['node_count'] > 0) ? 'class="dropdown-submenu  pull-left" data-toggle="dropdown-submenu" role="menu"' : ''; ?>>
                                                <a href="<?php echo base_url().'profile/sublistprofile/'.$rwSubJab['id_structure_node']; ?>?page_id=<?php echo $rwSubJab['page_id']; ?>&header=<?php echo $rwSubJab['structure_name']; ?>">
                                                    <?php echo $rwSubJab['structure_name']; ?>
                                                </a>
                                                <?php
                                                if(!empty($rwSubJab['subnextpejabat']))
                                                {
                                                    ?>
                                                    <ul class="dropdown-menu">
                                                        <?php
                                                        foreach($rwSubJab['subnextpejabat'] as $rwSJ)
                                                        {
                                                            ?>
                                                            <li <?php echo ($rwSJ['node_count'] > 0) ? 'class="dropdown-submenu pull-left" data-toggle="dropdown-submenu" role="menu"' : ''; ?>>
                                                                <a tabindex="-1" href="<?php echo base_url().'profile/sublistprofile/'.$rwSJ['id_structure_node']; ?>?page_id=<?php echo $rwSJ['page_id']; ?>&header=<?php echo $rwSJ['structure_name']; ?>">
                                                                    <?php echo $rwSJ['structure_name']; ?>
                                                                </a>
                                                        <?php
                                                        if(!empty($rwSJ['sublevel4']))
                                                        {
                                                        ?>
                                                                 <ul class="dropdown-menu" role="menu">
                                                        <?php
                                                                 foreach($rwSJ['sublevel4'] as $rwLevel4)
                                                                 {
                                                        ?>
                                                                     <li><a tabindex="-1" href="<?php echo base_url().'profile/sublistprofile/'.$rwLevel4['id_structure_node']; ?>?page_id=<?php echo $rwLevel4['page_id']; ?>&header=<?php echo $rwLevel4['structure_name']; ?>"><?php echo $rwLevel4['structure_name']; ?></a></li>
                                                        <?php
                                                                 }
                                                        ?>
                                                                 </ul>
                                                        <?php
                                                        }
                                                        ?>

                                                            </li>
                                                        <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                <?php
                                                }
                                                ?>
                                            </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                <?php
                                }
                                ?>
                            </li>
                        <?php
                        }
                        ?>
                    </ul>
                </li>
            <?php
            }
            ?>
        </ul>
    </li>
<?php
}
?>
