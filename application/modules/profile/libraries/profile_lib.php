<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile_Lib {

	protected  $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database('slave');
	}

	public function getProfileContent($select='*', $where=array(), $limit=0, $offset=0)
	{
		$this->CI->db->select($select);
		$this->CI->db->where($where);
		if($limit != 0){
			$this->CI->db->limit($limit, $offset);
		}

		$query = $this->CI->db->get('tcontent');
		return $query;
	}

    public function GetHotProfile()
    {

    }

    public function GetStruktur($where=null, $sortby=null)
    {
        $sql = <<<QUERY
                SELECT SQL_CALC_FOUND_ROWS node.*, (SELECT COUNT(*) FROM tusr_follow tf WHERE tf.page_id = node.page_id) AS user_follow,
                       (SELECT IFNULL(COUNT(*),0) FROM structure_node sn WHERE sn.parent_node = node.id_structure_node) AS node_count,
                       ta.attachment_title
                FROM structure_node node
                LEFT JOIN tobject_page tp ON tp.page_id = node.page_id
                LEFT JOIN tcontent_attachment ta ON tp.profile_content_id = ta.content_id
                {$where}
                {$sortby}
QUERY;
        $query = $this->CI->db->query($sql);
        $rsTop = $query->result_array();
        return $rsTop;
    }

    public function GetStrukturByParentNode($nodeID)
    {
        $where = "WHERE node.status='0' AND node.parent_node = '".$nodeID."' ";
        $sortby = "ORDER BY level_node LIMIT 0, 10";
        return $this->GetStruktur($where, $sortby);
    }

    public function GetStrukturINParentNode($nodeID)
    {
        $where = "WHERE node.status='0' AND node.parent_node IN (".$nodeID.") ";
        $sortby = "ORDER BY level_node";
        return $this->GetStruktur($where, $sortby);
    }

    public function GetDetailStrukture($nodeID)
    {
        $arr_strukturID = array();
        $rsTopLevel = $this->GetStrukturByParentNode($nodeID);
        if(count($rsTopLevel) > 1)
        {
            foreach($rsTopLevel as $rwTop)
            {
                $arr_strukturID[] = $rwTop['id_structure_node'];
            }
            $listStruk = implode(",", $arr_strukturID);
            $rsListProf = $this->GetStrukturINParentNode($listStruk);
        } else {
            $rsListProf = $this->GetStrukturByParentNode($nodeID);
        }
        return $rsListProf;
    }

    public function GetAllStruktur($nodeID)
    {
        $rsTop = $this->GetStrukturByParentNode($nodeID);
        if(!empty($rsTop))
        {
            $arr_strukture = array();

            foreach($rsTop as $rwTop)
            {
                $arr_level = array();
                $arr_level2 = array();
                $arr_level3 = array();
                $arr_level4 = array();
                $arr_level5 = array();
                $arr_level6 = array();

                $TopNode = $rwTop['id_structure_node'];
                $rsEks = $this->GetStrukturByParentNode($TopNode);

                foreach($rsEks as $rwEks)
                {
                    $node_eksekutif = $rwEks['id_structure_node'];
                    $rsPeriode = $this->GetStrukturByParentNode($node_eksekutif);

                    if($rwEks['node_count'] > 0)
                    {
                        foreach($rsPeriode as $rwPeriod)
                        {
                            $nodePeriod = $rwPeriod['id_structure_node'];
                            $rsSubPeriod = $this->GetStrukturByParentNode($nodePeriod);

                            if($rwPeriod['node_count'] > 0)
                            {
                                foreach($rsSubPeriod as $rwSubPer)
                                {
                                    $nodePeriod = $rwSubPer['id_structure_node'];
                                    $rsSubPerNext = $this->GetStrukturByParentNode($nodePeriod);

                                    if($rwSubPer['node_count'] > 0)
                                    {
                                        foreach($rsSubPerNext as $rwLevel3)
                                        {
                                            $nodePeriod = $rwLevel3['id_structure_node'];
                                            $rsLevel4 = $this->GetStrukturByParentNode($nodePeriod);

                                            if($rwLevel3['node_count'] > 0)
                                            {
                                                foreach($rsLevel4 as $rwLevel4)
                                                {
                                                    $nodePeriod = $rwLevel4['id_structure_node'];
                                                    $rsLevel5 = $this->GetStrukturByParentNode($nodePeriod);


                                                    $arr_level5[] = array('id_structure_node' => $rwLevel4['id_structure_node'], 'structure_name' => $rwLevel4['structure_name'],
                                                        'page_id' => $rwLevel4['page_id'], 'parent_node' => $rwLevel4['parent_node'], 'entry_by' => $rwLevel4['entry_by'],
                                                        'entry_date' => $rwLevel4['entry_date'], 'level_node' => $rwLevel4['level_node'], 'status' => $rwLevel4['status'],
                                                        'user_follow' => $rwLevel3['user_follow'], 'node_count' => $rwSubPer['node_count'],
                                                        'attachment_title' => $rwLevel3['attachment_title'], 'sublevel5' => $rsLevel5
                                                    );
                                                }
                                            } else {
                                                $arr_level5 = $rsLevel4;
                                            }

                                            $arr_level4[] = array('id_structure_node' => $rwLevel3['id_structure_node'], 'structure_name' => $rwLevel3['structure_name'],
                                                'page_id' => $rwLevel3['page_id'], 'parent_node' => $rwLevel3['parent_node'], 'entry_by' => $rwLevel3['entry_by'],
                                                'entry_date' => $rwLevel3['entry_date'], 'level_node' => $rwLevel3['level_node'], 'status' => $rwLevel3['status'],
                                                'user_follow' => $rwLevel3['user_follow'], 'node_count' => $rwSubPer['node_count'],
                                                'attachment_title' => $rwLevel3['attachment_title'], 'sublevel4' => $rsLevel4
                                            );
                                        }

                                    } else {
                                        $arr_level4 = $rsSubPerNext;
                                    }

                                    $arr_level3[] = array('id_structure_node' => $rwSubPer['id_structure_node'], 'structure_name' => $rwSubPer['structure_name'],
                                        'page_id' => $rwSubPer['page_id'], 'parent_node' => $rwSubPer['parent_node'], 'entry_by' => $rwSubPer['entry_by'],
                                        'entry_date' => $rwSubPer['entry_date'], 'level_node' => $rwSubPer['level_node'], 'status' => $rwSubPer['status'],
                                        'user_follow' => $rwSubPer['user_follow'], 'node_count' => $rwSubPer['node_count'],
                                        'attachment_title' => $rwSubPer['attachment_title'], 'subnextpejabat' => $arr_level4
                                    );
                                    unset($arr_level4);
                                }

                            } else {
                                $arr_level3 = $rsSubPeriod;
                            }

                            $arr_level2[] = array(
                                'id_structure_node' => $rwPeriod['id_structure_node'], 'structure_name' => $rwPeriod['structure_name'], 'page_id' => $rwPeriod['page_id'],
                                'parent_node' => $rwPeriod['parent_node'], 'entry_by' => $rwPeriod['entry_by'], 'entry_date' => $rwPeriod['entry_date'],
                                'level_node' => $rwPeriod['level_node'], 'status' => $rwPeriod['status'], 'user_follow' => $rwPeriod['user_follow'],
                                'node_count' => $rwPeriod['node_count'], 'attachment_title' => $rwPeriod['attachment_title'], 'subpejabat' => $arr_level3
                            );

                            unset($arr_level3);

                        }
                    } else {
                        $arr_level2 = $rsPeriode;
                    }

                    $arr_level[] = array(
                        'id_structure_node' => $rwEks['id_structure_node'], 'structure_name' => $rwEks['structure_name'], 'page_id' => $rwEks['page_id'],
                        'parent_node' => $rwEks['parent_node'], 'entry_by' => $rwEks['entry_by'], 'entry_date' => $rwEks['entry_date'], 'level_node' => $rwEks['level_node'],
                        'status' => $rwEks['status'], 'user_follow' => $rwEks['user_follow'], 'node_count' => $rwEks['node_count'],
                        'attachment_title' => $rwEks['attachment_title'], 'pejabat' => $arr_level2
                    );

                    unset($arr_level2);

                }

                $arr_strukture[] = array(
                    'id_structure_node' => $rwTop['id_structure_node'], 'structure_name' => $rwTop['structure_name'], 'page_id' => $rwTop['page_id'],
                    'parent_node' => $rwTop['parent_node'], 'entry_by' => $rwTop['entry_by'], 'entry_date' => $rwTop['entry_date'], 'level_node' => $rwTop['level_node'],
                    'status' => $rwTop['status'], 'user_follow' => $rwTop['user_follow'], 'node_count' => $rwTop['node_count'],
                    'attachment_title' => $rwTop['attachment_title'], 'eksekutif' => $arr_level
                );

                unset($arr_level);
            }
        }

        return $arr_strukture;
    }



}