<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Politik extends Application {

    private $user = array();
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $data = $this->data;

        $this->load->library('cache');
        $this->load->library('typography');
        $this->load->library('politik/politik_lib');
        $this->load->library('timeline/timeline_lib');
        $this->load->library('profile/profile_lib');
        $this->load->library('news/news_lib');
        $this->load->module('profile/profile');
        $this->load->module('timeline/timeline');
        $this->load->module('news/news');
        $this->load->module('survey/survey');
        $this->load->module('headline/headline');
        $this->load->module('profile/profile');
        $this->load->module('scandal/scandal');
        $this->load->library('caleg/caleg_lib');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->load->library('redis', array('connection_group' => 'master'), 'redis_master');
        $this->redis_slave = $this->redis_slave->connect();
        $this->redis_master = $this->redis_master->connect();
        $this->member = $this->session->userdata('member');
    }

    function index()
    {
        $data = $this->data;

        $data['head_title'] = 'Profile Politisi';
        
        $arr_profile = array(); $arr_partai = array(); $arr_lembaga = array();
        $result = array(); $respart = array(); $reslem = array();
        $headline = $this->redis_slave->lrange("profile:list:headline", 0, 0);
        if(count($headline))
        {
            foreach($headline as $key => $headval)
            {
                $profileHead = $this->redis_slave->get('profile:detail:'.$headval);
                $arrprofHead = @json_decode($profileHead, true);

                $arrprofHead['scandal_count'] = (array_key_exists('scandal', $arrprofHead)) ? count($arrprofHead['scandal']) : 0;

                $where = array('page_id' => $headval);

                $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                $arrprofHead['news_count'] = $news_terkait['cou'];

                $result[0] = $arrprofHead;
            }
        }

        $i = 1; $profile_key = 'profile:list:hot';
        $list_profile = $this->redis_slave->lrange($profile_key, 0, 19);
        //$i = 1; $list_profile = $this->politik_lib->getHotlist();

        if(!empty($list_profile))
        {
            foreach ($list_profile as $key => $value)
            {
                $profile_detail = $this->redis_slave->get('profile:detail:'.$value);
                $arr_profile = @json_decode($profile_detail, true);

                $arr_profile['scandal_count'] = (array_key_exists('scandal', $arr_profile)) ? count($arr_profile['scandal']) : 0;

                $where = array('page_id' => $value);

                $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                $arr_profile['news_count'] = $news_terkait['cou'];

                $result[$i] = $arr_profile;
                $i++;
            }
        }
        $data['hot_profile'] = $result;

        $y = 0; $partai = $this->politik_lib->getPartaiPolitic();
        if(!empty($partai))
        {
            foreach($partai as $rwpartai)
            {
                $partai_detail = $this->redis_slave->get('profile:detail:'.$rwpartai['page_id']);
                $arr_partai = @json_decode($partai_detail, true);

                $arr_partai['scandal_count'] = (array_key_exists('scandal', $arr_partai)) ? count($arr_partai['scandal']) : 0;

                $where = array('page_id' => $rwpartai['page_id']);

                $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                $arr_partai['news_count'] = $news_terkait['cou'];

                $respart[$y] = $arr_partai;
                $y++;
            }
        }
        $data['hot_partai'] = $respart;

        $s = 0; $lembaga = $this->politik_lib->getLembagaNegara();
        if(!empty($lembaga))
        {
            foreach($lembaga as $rwLm)
            {
                $lembaga_detail = $this->redis_slave->get('profile:detail:'.$rwLm['page_id']);
                $arr_lembaga = @json_decode($lembaga_detail, true);

                $arr_lembaga['scandal_count'] = (array_key_exists('scandal', $arr_lembaga)) ? count($arr_lembaga['scandal']) : 0;

                $where = array('page_id' => $rwLm['page_id']);

                $news_terkait = $this->caleg_lib->get_news_player($select='count(*) cou', $where)->row_array(0);
                $arr_lembaga['news_count'] = $news_terkait['cou'];

                
                $reslem[$s] = $arr_lembaga;
                $s++;
            }
        }
        $data['hot_lembaga'] = $reslem;

        // $data['mini_profile'] = $this->profile->hot('true');
        
        $html['html']['content'] = $this->load->view('mobile/index_view', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function followaction()
    {
        $member = $this->session->userdata('member');
        if($member)
        {
            $account_id = $member['account_id'];
            if(isset($_POST['pid']) and isset($_POST['follow']))
            {
                $pid = $_POST['pid']; $follow = $_POST['follow'];
                if(intval($follow) == 0)
                {
                    $data = array('account_id' => $account_id, 'page_id' => $pid, 'entry_date' => mysql_datetime());
                    $this->politik_lib->InsertFollow($data);
                }else{
                    $this->politik_lib->DeleteFollow($account_id, $pid);
                }
                $this->output->set_content_type('application/json');
                $msg = array('message' => 'OK');
                $this->output->set_output(json_encode($msg));
            }else{
                $this->output->set_content_type('application/json');
                $msg = array('message' => 'error message');
                $this->output->set_output(json_encode($msg));
            }
        }
    }


    function user_politisi_comment($page_id='', $page=1)
    {
        $limit = 9;
        if(empty($page_id)){echo ''; return;        }
        $offset = ($limit*intval($page)) - $limit;
        if($offset != 0){$offset += (intval($page) - 1);}
        $result = array();
        $zrange = $limit + $offset;

        $list_key = 'profile:comment:list:';

        $list_len = $this->redis_slave->lLen($list_key.$page_id);
        if($zrange > intval($list_len)){ $zrange = $list_len;}
        if($offset > intval($list_len)){ echo ''; return;}

        $comment_list = $this->redis_slave->lRange($list_key.$page_id, $offset, $zrange);

        foreach($comment_list as $row=>$val){
            $comment_detail = $this->redis_slave->get('profile:comment:detail:'.$val);
            $comment_array = @json_decode($comment_detail, true);
            if(!empty($comment_array)){
                $comment_array['image_url'] = icon_url($comment_array['attachment_title']);
            }
            $result[$row] = $comment_array;
        }
       // var_dump($result);
        $data['result'] = $result;
        $this->load->view('timeline/timeline_profile_comment', $data);
    }

    function user_post_comment()
    {
        if(!$this->isLogin()){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login!';
            $this->message($data);
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('id', '', 'required|trim|xss_clean');
        $this->form_validation->set_rules('val', '', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE)
        {
            $data['rcode'] = 'bad';
            $data['msg'] = 'Komentar harus di isi';
            $this->message($data);
        }

        $id = $this->input->post('id');
        $val = $this->input->post('val', true);

        if(is_bool($id) || is_bool($val)){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        // check if content exist
        $select = 'count(*) as count';
        $where = array('page_id' => trim($id));
        $cnt = $this->timeline_lib->get_content($select, $where)->row_array(0);

        if(intval($cnt['count']) == 0){
            $data['rcode'] = 'bad';
            $data['msg'] = 'Anda harus login !';
            $this->message($data);
        }

        $cid = sha1( uniqid().time() );

        $data_post = array(
            'page_comment_id' => $cid,
            'account_id' => $this->member['account_id'],
            'page_id' => $id,
            'comment_text' => trim($val),
            'entry_date' => mysql_datetime()
        );

        $this->timeline_lib->insert_comment_profile($data_post);

        $result = array();
        // insert to redis
        $last_comment = $this->timeline_lib->get_comment_politisi($cid);
        //var_dump($last_comment->result_array());
        foreach($last_comment->result_array() as $row=>$value){
            $this->redis_master->lPush('profile:comment:list:'. $id, $value['page_comment_id']);
            $this->redis_master->set('profile:comment:detail:' . $value['page_comment_id'], json_encode($value));
            $value['image_url'] = icon_url($value['attachment_title']);
            $result[$row] = $value;
        }

        $dt['result'] = $result;
        $htm = $this->load->view('timeline/timeline_profile_comment', $dt, true);

        $lst_score = $this->timeline_lib->get_content_profile_score($id)->result_array();

        $this->redis_master->set('content:last:score:'.$id, json_encode($lst_score));

        $data['rcode'] = 'ok';
        $data['msg'] = $htm;
        $this->message($data);

    }

    private function message($data=array())
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    function isLogin($return=false)
    {
        $cr = current_url();
        $this->member = $this->session->userdata('member');
        if(!$this->member){
            if($this->input->is_ajax_request()){
                return false;
            }else{
                if($return){
                    return false;
                }else{
                    redirect(base_url().'home/login?frm='.urlencode($cr));
                }
            }

        }

        return true;
        //exit('please');
    }

}