<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Politik_Lib {

	protected  $CI;

	public function __construct()
	{
        $this->CI =& get_instance();
		$this->CI->load->database('slave');
	}

	public function getHotPolitisi($limit, $publish=1) {
		$member 					= $this->CI->session->userdata('member');
		$member_account_id 	= (isset($member['account_id'])) ? $member['account_id'] : NULL;

		$sql = "
		  SELECT * FROM (
		  SELECT tp.page_id, tp.page_name, tp.profile_content_id content_id, tp.profile_content_id, LEFT(tp.about,190) about, ta.attachment_title,
		  			tp.publish_order, tp.publish_date, IFNULL(tacc.account_type,1) account_type, tp.entry_date,

					( select count(tf.account_id) as count
						from tusr_follow tf
						left join tusr_account acc on tf.account_id = acc.account_id
						left join tobject_page p on p.page_id = acc.user_id
						left join tobject_info_address ad on ad.page_id = p.page_id
						where tf.page_id = tp.page_id
					) count_follower,
					( SELECT COUNT(account_id) count FROM tcontent_comment
						WHERE content_id = (SELECT content_id FROM tcontent WHERE page_id = tp.page_id AND `content_group_type` = 14 LIMIT 1)
					) count_comment,
					(
						SELECT COUNT(account_id) count FROM tcontent_like
						WHERE content_id = (SELECT content_id FROM tcontent WHERE page_id = tp.page_id AND `content_group_type` = 14 LIMIT 1)
					) count_like,
					(
						SELECT COUNT(account_id) count FROM tcontent_dislike
						WHERE content_id = (SELECT content_id FROM tcontent WHERE page_id = tp.page_id AND `content_group_type` = 14 LIMIT 1)
					) count_dislike,
					get_count_like_content_by_account('".$member_account_id."', ta.content_id) as is_like,
					get_count_dislike_content_by_account('".$member_account_id."', ta.content_id) as is_dislike,

					tpp.page_name partai_name,
					tp.posisi, tp.prestasi, tp.prestasi_lain,
					(
						SELECT tca.attachment_title
						FROM tcontent tc LEFT JOIN  tcontent_attachment tca on tc.content_id = tca.content_id
						WHERE tc.page_id=tp.page_id AND tc.content_type='IMAGE' AND tc.content_group_type='40'
						LIMIT 1
					) headline_attachment
				FROM `tobject_page` tp
				LEFT JOIN `tcontent_attachment` ta ON ta.content_id = tp.profile_content_id
				LEFT JOIN `tusr_account` tacc ON tacc.user_id = tp.page_id
				LEFT JOIN `tobject_page` tpp ON tp.partai_id = tpp.page_id

				WHERE tp.`publish` = '$publish '

		  ) final_result WHERE 1 = 1 AND account_type != 2 order by publish_order, publish_date DESC LIMIT 0, $limit";
		$query = $this->CI->db->query($sql)->result_array();
		return $query;
	}

    public function getPartaiPolitik()
    {
        $sql_top = 'SELECT id_structure_node as id, structure_name as label, page_id, parent_node, status
                    FROM structure_node
                    WHERE parent_node = 8591 ORDER BY level_node asc limit 1';
        $query = $this->CI->db->query($sql_top);
        $parent_node = $query->row_array();

        $sql_partai = 'SELECT id_structure_node as id, structure_name as label, page_id, parent_node, status
                       FROM structure_node WHERE parent_node = '.$parent_node['id'].' order by level_node asc';
        $qry = $this->CI->db->query($sql_partai);
        return $qry->result_array();
    }

    public function getPartaiPolitic()
    {
        $sql = "SELECT * FROM tusr_partai ORDER BY id_tusr_partai ASC";
        $qry = $this->CI->db->query($sql);
        return $qry->result_array();
    }

    public function getLembagaNegara()
    {
        $sql = "SELECT * FROM tusr_lembaga_negara ORDER BY id_tusr_negara ASC";
        $qry = $this->CI->db->query($sql);
        return $qry->result_array();
    }

    // public function getHotList()
    // {
    //     $sql = "SELECT * FROM tusr_hotlist ORDER BY id_tusr_hotlist ASC";
    //     $qry = $this->CI->db->query($sql);
    //     return $qry->result_array();
    // }

    public function getHotList($limit=50)
    {
        $sql = "SELECT * FROM tusr_hotlist ORDER BY id_tusr_hotlist ASC LIMIT $limit";
        $qry = $this->CI->db->query($sql);
        return $qry->result_array();
    }

    public function getGedungNegara()
    {
        $sql = 'SELECT id_structure_node, place_name, place_description, place_image_url
                FROM structure_node_place';
        $query = $this->CI->db->query($sql);
        return $query->result_array();
    }

	public function getScandalByPolitisi($page_id, $limit=4)
	{
		$sql 	 = "SELECT ts.scandal_id, ts.title, tsp.pengaruh 
					 FROM tscandal_player tsp
					 INNER JOIN tscandal ts ON tsp.scandal_id = ts.scandal_id
					 WHERE tsp.page_id = '$page_id'
					 LIMIT $limit";
		$query = $this->CI->db->query($sql)->result_array();
		return $query;
	}

	public function getProfile($select='*', $where=array(), $limit=0, $offset=0, $order='')
	{
		$this->CI->db->select($select);
		$this->CI->db->from('tobject_page p');
		$this->CI->db->join('tcontent_attachment ta', 'ta.content_id = p.profile_content_id');
		$this->CI->db->where($where);
		if($limit != 0){
			$this->CI->db->limit($limit, $offset);
		}

		$this->CI->db->order_by($order);

		$qr = $this->CI->db->get();
		return $qr;

	}

    public function GetProfileBerita($page_id=NULL, $account_id=NULL, $limit=10, $offset=0)
    {
        if(!empty($page_id))
        {
           $sql = "SELECT tc.*, tt.*, tp.topic_id, get_count_comment_content(tc.content_id) AS count_of_comment,
                       get_count_like_content(tc.content_id) AS count_of_like,
                       get_count_like_content_by_account('".$account_id."', tc.content_id) AS isLike
                   FROM tcontent tc
                   INNER JOIN tcontent_text tt ON tc.content_id = tt.content_id
                   LEFT JOIN tcontent_has_topic tp ON tp.content_id=tc.content_id
                   WHERE content_group_type = 12 AND content_type = 'TEXT'
                   AND tc.page_id = '".$page_id."'
                   OR tc.content_id IN (SELECT content_id FROM tcontent_has_politic WHERE page_id = '".$page_id."')
                   ORDER BY tc.entry_date DESC LIMIT $limit OFFSET $offset ";
           $query = $this->CI->db->query($sql);
           return $query->result_array();
        }
    }
    
    public function CountProfileBerita($page_id=null)
    {
        if(!empty($page_id))
        {
           $sql = "SELECT count(*) as 'count_news'
                   FROM tcontent tc
                   INNER JOIN tcontent_text tt ON tc.content_id = tt.content_id
                   LEFT JOIN tcontent_has_topic tp ON tp.content_id=tc.content_id
                   WHERE content_group_type = 12 AND content_type = 'TEXT'
                   AND tc.page_id = '".$page_id."'
                   OR tc.content_id IN (SELECT content_id FROM tcontent_has_politic WHERE page_id = '".$page_id."')";
           $query = $this->CI->db->query($sql);
           return $query->row_array();
        }       	
  	 }

    public function GetCountPostPolitic($page_id=NULL)
    {
        $sql = <<<QUERY
                SELECT * FROM
                (
                    SELECT 'photo' AS tipe, photo.* FROM
                    (
                        SELECT COUNT(*) AS total  FROM tcontent tc WHERE tc.page_id = '$page_id' AND content_group_type
                        IN (20, 22, 40)
                    ) AS photo UNION
                    SELECT 'blog' AS tipe, blog.* FROM
                    (
                        SELECT COUNT(*) AS total  FROM tcontent tc WHERE tc.page_id = '$page_id' AND content_group_type = 11
                    )AS blog UNION
                    SELECT 'follow' AS tipe, follow.* FROM
                    (
                        SELECT COUNT(*) AS total FROM `tusr_follow` WHERE page_id = '$page_id'
                    )AS follow UNION
                    SELECT 'follower' AS tipe, follower.* FROM
                    (
                        SELECT COUNT(*) AS total FROM `tusr_follow` WHERE page_id = '$page_id'
                    )AS follower
                )AS total
QUERY;
        $query = $this->CI->db->query($sql);
        return $query->result_array();
    }

    public function GetPolitikInfo($where=NULL, $sortby=NULL, $limit=NULL, $row=false)
    {
        $sql = <<<QUERY
        SELECT * FROM tusr_account
        {$where}
        {$sortby}
        {$limit}
QUERY;
        $query = $this->CI->db->query($sql);
        if($row)
        {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function GetPolitikByUserID($page_id)
    {
        $where = "WHERE user_id = '".$page_id."' ";
        return $this->GetPolitikInfo($where, null, null, true);
    }

    public function isBijaksUser($page_id)
    {
        $select = 'ta.`account_id`, ta.`user_id`, ta.`display_name`, ta.account_type, ta.`registration_date`, ta.`last_login_date`, ta.`email`, ta.`fname`, ta.`lname`,
ta.`gender`, ta.`birthday`, ta.`current_city`, tp.`profile_content_id`, tp.`about`, tp.`personal_message`, tat.`attachment_title`';
        $user_id = urldecode($page_id);
//        echo $user_id;
//        exit;
        $where = array('ta.user_id' => strval($page_id));

        $users = $this->komunitas_lib->get_user($select, $where);

        if($users->num_rows() > 0)
        {
            $this->user = $users->row_array(0);
            if($this->user['account_type'] != 0){
                show_404();
            }

        }else{
            show_404();
        }
    }

    public function GetUser($where=null, $sortby=null, $limit=null, $is_row=false)
    {
        $sql = <<<QUERY
        SELECT ta.`account_id`, ta.`user_id`, ta.`display_name`, ta.account_type, ta.`registration_date`, ta.`last_login_date`,
                ta.`email`, ta.`fname`, ta.`lname`, ta.`gender`, ta.`birthday`, ta.`current_city`, tp.`profile_content_id`,
                tp.`about`, tp.`personal_message`, tat.`attachment_title`
        FROM tusr_account ta
        LEFT JOIN tobject_page tp ON tp.page_id = ta.user_id
        LEFT JOIN tcontent_attachment tat ON tat.content_id = tp.profile_content_id
        {$where}
        {$sortby}
        {$limit}
QUERY;
        $query = $this->CI->db->query($sql);
        if($is_row)
        {
            return $query->row_array();
        } else {
            return $query->result_array();
        }
    }

    public function isBijakUser($page_id)
    {
        $where = "WHERE ta.user_id = '".$page_id."' ";
        $rwUser = $this->GetUser($where, null, null, true);
        if(count($rwUser) > 0)
        {
            return $rwUser;
        } else {
            return false;
        }
    }

    public function InsertFollow($data)
    {
        $this->CI->db->insert('tusr_follow', $data);
    }

    public function DeleteFollow($account_id, $page_id)
    {
        $where = array('account_id' => $account_id, 'page_id' => $page_id);
        $this->CI->db->delete('tusr_follow', $where);
    }

}

/* End of file file.php */