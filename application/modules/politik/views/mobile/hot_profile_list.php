<?php foreach($hot_profile as $row=>$value){ 
		$url = base_url('aktor/profile/'.$value['page_id']);
        $image  = base_url().'public/img/no-image-politisi.png';
        if(count($value['profile_photo']) > 0){
        	$image = ($value['profile_photo'][0]['large_url'] != 'None') ? $value['profile_photo'][0]['large_url'] : base_url().'public/img/no-image-politisi.png';
        } 
        if(count($value['headline_photo']) > 0){
            $image  = ($value['headline_photo'][0]['headline_url'] != 'None') ? $value['headline_photo'][0]['headline_url'] : base_url().'public/img/no-image-politisi.png';    
        }
        
        $content_date = $value['partai_name'];
	?>
	<div class="row">
		<div class="col-xs-4 col-sm-4 nopadding">
			<a class="pull-left " href="<?php echo $url;?>">
                <img class="img-media-list" src="<?php echo $image;?>" alt="<?php echo $value['page_name'];?>" >
            </a>
		</div>
		<div class="col-xs-8 col-sm-8">
			<a class="" href="<?php echo $url;?>">
            <h5 class="media-heading"><?php echo $value['page_name'];?><?php echo ($value['partai_alias'] != 'None') ? '<small> - '.$value['partai_alias'].'</small>' : '';?></h5>
            </a>
            <a target="__blank" class="a-block a-block-3" href="<?php echo base_url();?>aktor/scandals/<?php echo $value['page_id'];?>">SKANDAL <span class="clearfix"><?php echo $value['scandal_count'];?></span></a>
            <a target="__blank" class="a-block" href="<?php echo base_url();?>aktor/news/<?php echo $value['page_id'];?>">BERITA <span class="clearfix"><?php echo $value['news_count'];?></span></a>
		</div>
	</div>
	<hr class="line-mini">
<?php } ?>