<div class="container" style="margin-top: 31px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right category-news-title-merah">
                    <h1>BERITA</h1>
                </div>

            </div>
        </div>
    </div>
    <div id='sub_header_activity' class="sub-header-container" data-tipe=''></div>
    <div id='disclaimer' class="sub-header-container"></div>
</div>

<div class="container">
    <div class="sub-header-container">

    <div class="span8" style="margin-left: 0px;">

       <div class="row">

           <!-- EKONOMI -->
           <?php echo $ekonomi; ?>

           <!-- HUKUM -->
           <?php echo $hukum; ?>

<!--

           <div class="span4" style="border-bottom: 5px solid #bbbbbb;height:525px">
            <!-- Terkini

              <div  class="home-title-section hp-label hp-label-hitam" style="background-color: #ffffff;">
                  <span class="hitam"><a href="#">TERKINI</a></span>
              </div>

              <div class="headline-terkini-container" style="margin-top:0px;background-color: #ffffff;">
                  <div id="head_terkini_next">
                      <?=$terkini;?>
                  </div>
              </div>

              <div id="" class="row-fluid">
                  <div class="span6 text-left">
                      <a data-tipe="1" id="head_terkini_news_next" class="btn btn-mini" >5 Berikutnya</a>
                  </div>
                  <div class="span6 text-right">
                      <a href="<?php echo base_url().'news/index/'; ?>" class="btn btn-mini" >Selengkapnya</a>
                  </div>
              </div>

          </div>
-->

       </div>

       <div class="row" style="margin-top:20px;margin-bottom: 10px;">
           <img style="margin-left:19px;width: 620px;height:75px;" src="<?php echo base_url('assets/images/banner_indonesia_baik.png'); ?>">
       </div>

       <div class="row">


           <!-- PARLEMEN -->
           <?php echo $parlemen; ?>

           <div class="span4" style="border-bottom: 5px solid #CA8868;height:525px">
               <!-- RESES -->
               <div  class="home-title-section hp-label hp-label-coklat" style="background-color: #ffffff;">
                   <span class="hitam"><a href="<?php echo base_url(); ?>news/index/reses">RESES</a></span>
               </div>

               <div class="hp-place-reses">
                   <div class="headline-reses-container">
                       <div id="head_reses_next">
                           <?=$reses;?>
                       </div>
                   </div>
               </div>

               <div id="" class="row-fluid">
                   <div class="span6 text-left">
                       <a data-tipe="1" id="head_reses_news_next" class="btn btn-mini" >5 Berikutnya</a>
                   </div>
                   <div class="span6 text-right">
                       <a href="<?php echo base_url().'news/index/reses'; ?>" class="btn btn-mini" >Selengkapnya</a>
                   </div>
               </div>

           </div>


       </div>

       <div class="row">

          <!-- DAERAH -->
          <?php echo $daerah; ?>

           <!-- NASIONAL -->
           <?php echo $nasional; ?>

       </div>

    </div>

    <div class="span4">

       <div  style="border-bottom: 5px solid #CA8868;height:1152px">
         <div class="home-title-section hp-label hp-label-coklat">
           <span class="hitam"><a style="cursor: default;">HEADLINE</a></span>
         </div>

         <div class="hp-place-headline">
           <div class="head-headline-container">
             <div id="head-headline-next">
               <?=$headline;?>
             </div>
           </div>
         </div>

         <div id="" class="row-fluid">
           <div class="span6 text-left">
               <a data-tipe="1" id="headline_headline_next" class="btn btn-mini" >13 Berikutnya</a>
           </div>
           <div class="span6 text-right">
               <a href="<?php echo base_url().'news/index/headline'; ?>" class="btn btn-mini" >Selengkapnya</a>
           </div>
         </div>
       </div>

       <div style="margin-left:-20px;">
          <!-- INTERNATIONAL -->
          <?php echo $internasional; ?>
       </div>

    </div>

    </div>
</div>



