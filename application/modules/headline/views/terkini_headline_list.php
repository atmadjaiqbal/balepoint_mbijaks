<?php
foreach($val as $key=>$result){

    $_topic_id = !empty($result['topic_id']) ? $result['topic_id'] : '';
    $_id = !empty($result['id']) ? $result['id'] : '';
    $_slug = !empty($result['slug']) ? $result['slug'] : '';
    $_title = !empty($result['title']) ? $result['title'] : '';

    $news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
    $short_title = (strlen ($_title) > 38) ? substr($_title, 0, 38). '...' : $_title;
    if(!empty($result['image_thumbnail']))
    {
        $_image_thumb = $result['image_thumbnail'];
    } else {
        $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
    }
    ?>

    <div id="media-headline">
        <a title="<?php echo !empty($result['title']) ? $result['title'] : ''; ?>" href="<?php echo $news_url; ?>">
            <div style="float:left;">

                <img class="lazy" data-original="<?php echo $_image_thumb; ?>">

            </div>
            <div style="float:left;width:170px;margin-left:5px;">
                <h5 class="media-heading"><?php echo $_title; ?></h5>
                <p class="news-date"><?php echo !empty($result['date']) ? mdate('%d %M %Y - %h:%i', strtotime($result['date'])) : ''; ?></p>
                <div class="score-place score-place-left score" data-tipe="1" data-id="<?php echo !empty($result['content_id']) ? $result['content_id'] : 0;?>" style="margin-top:-17px;"></div>
            </div>
        </a>

    </div>
    <div class="div-line-small"></div>
<?php } ?>