<h4 class="kanal-title kanal-title-cokelat">HEADLINE</h4>
<?php echo $headline;?>
<a href="<?php echo base_url().'news/index/headline'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
<br>

<h4 class="kanal-title kanal-title-cokelat">EKONOMI</h4>
<?php echo $ekonomi;?>
<a href="<?php echo base_url().'news/index/ekonomi'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
<br>

<h4 class="kanal-title kanal-title-cokelat">HUKUM</h4>
<?php echo $hukum;?>
<a href="<?php echo base_url().'news/index/hukum'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
<br>

<h4 class="kanal-title kanal-title-cokelat">PARLEMEN</h4>
<?php echo $parlemen;?>
<a href="<?php echo base_url().'news/index/parlemen'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
<br>

<h4 class="kanal-title kanal-title-cokelat">RESES</h4>
<?php echo $reses;?>
<a href="<?php echo base_url().'news/index/reses'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
<br>

<h4 class="kanal-title kanal-title-gray">DAERAH</h4>
<?php echo $daerah;?>
<a href="<?php echo base_url().'news/index/daerah'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
<br>

<h4 class="kanal-title kanal-title-gray">NASIONAL</h4>
<?php echo $nasional;?>
<a href="<?php echo base_url().'news/index/nasional'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
<br>

<h4 class="kanal-title kanal-title-gray">INTERNASIONAL</h4>
<?php echo $internasional;?>
<a href="<?php echo base_url().'news/index/internasional'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
<br>
