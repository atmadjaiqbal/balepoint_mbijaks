<?php
foreach($val as $key=>$result){

    $_topic_id = !empty($result['topic_id']) ? $result['topic_id'] : '';
    $_id = !empty($result['id']) ? $result['id'] : '';
    $_slug = !empty($result['slug']) ? $result['slug'] : '';
    $_title = !empty($result['title']) ? $result['title'] : '';

    $news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
    $short_title = (strlen ($_title) > 38) ? substr($_title, 0, 38). '...' : $_title;
    if(!empty($result['image_thumbnail']))
    {
        $_image_thumb = $result['image_thumbnail'];
    } else {
        $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
    }
    ?>

    <div id="media-headline" class="media">
        <div class="media-image pull-left" style="background:
            url('<?php echo $_image_thumb; ?>') no-repeat; background-position : center; background-size:auto 80px;">

        </div>
        <div class="media-body">
            <h5 class="media-heading">
                <a title="<?php echo !empty($result['title']) ? $result['title'] : ''; ?>" href="<?php echo $news_url; ?>">
                    <?php echo $_title; ?>
                </a>
            </h5>
            <p class="news-date" style="margin-top:-2px;"><?php echo !empty($result['date']) ? mdate('%d %M %Y - %h:%i', strtotime($result['date'])) : ''; ?></p>
            <div class="score-place score-place-left score" data-tipe="1" data-id="<?php echo !empty($result['content_id']) ? $result['content_id'] : 0;?>"  style="margin-top:-17px;"></div>
        </div>
    </div>

<?php } ?>