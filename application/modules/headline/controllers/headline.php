<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Headline extends Application {


    public function __construct()
    {
        parent::__construct();

        $this->current_url = '/'.$this->uri->uri_string();
        $this->session->set_flashdata('referrer', $this->current_url);

        $data = $this->data;
        $this->current_url = '/'.$this->uri->uri_string();
        $this->load->module('timeline/timeline');
        $this->load->module('news/news');

        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
    }


    public function index()
    {
        $this->load->library('core/cache_lib');
        $this->load->helper('imagefull');
        $cm = $this->load->module('ajax');
        $data				 = $this->data;
        $data['topic_last_activity'] =  'headline';

        $data['scripts']     = array('bijaks.home.js', 'bijaks.suksesi.js', 'bijaks.js', 'highcharts.js', 'bijaks.survey.js');
        $data['terkini']     = $this->terkini_list();
        $data['reses']       = $this->reses_list();
        $data['headline']       = $this->headline_list(); //$this->news(8, 'hot-issues');

        $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
        $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
        $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');

        $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
        $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
        $data['internasional'] = $this->news->sesi('internasional', 9, 'true');

        $html['html']['content'] = $this->load->view('mobile/headline_index', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    public function terkini_list($page=1)
    {
        $topic_id = 0;
        $limit = 10;

        $offset = (($limit*$page) - $limit);
        //if($page == 1){
        //    $offset = (($limit*$page) - $limit);
        //}

        $rrange = $offset + $limit;
        $last_key = $this->redis_slave->get('news:key');
        $rkey = 'news:topic:list:'.$last_key.':0';
        $news = $this->redis_slave->lrange($rkey, $offset, $rrange);
        $resnews = array();

        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_array = json_decode($rowNews, true);
            $resnews[$i] = $news_array;
            $resnews[$i]['topic_id'] = $news_array['categories'][0]['id'];
            $i++;
        }
        $data['val'] = $resnews;

        return $this->load->view('terkini_headline_list', $data, true);
    }


    public function reses_list($page=1)
    {
        $data['category'] = 'reses';
        $topic_id = 0; $limit = 9; $id = 18;

        $offset = (($limit*$page) - $limit);

        $rrange = $offset + $limit;
        $last_key = $this->redis_slave->get('news:key');

        $rkey = 'news:topic:list:'.$last_key.':'.$id;
        $news = $this->redis_slave->lrange($rkey, $offset, $rrange);

        $resnews = array();

        $i = 0;
        foreach($news as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_array = json_decode($rowNews, true);
            $resnews[$i] = $news_array;
            $resnews[$i]['topic_id'] = $news_array['categories'][0]['id'];
            $i++;
        }
        $data['val'] = $resnews;
        $data['tipe'] = '2';

        return $this->load->view('mobile/news_list', $data, true);
    }


    public function headline_list($page=1)
    {
        $limit = 9;
        $rkey = "news:list:headline";
        $offset = (($limit*$page) - $limit) + 0;

        $rrange = $offset + $limit;
        $resultNews = $this->redis_slave->lrange($rkey, $offset, $rrange);
        $result = array();

        $i = 0;
        foreach($resultNews as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_array = json_decode($rowNews, true);
            $result[$i] = $news_array;
            $result[$i]['topic_id'] = $news_array['categories'][0]['id'];
            $i++;
        }
        $data['val'] = $result;
        $data['tipe'] = '1';

        return $this->load->view('mobile/news_list', $data, true);
    }

}

?>