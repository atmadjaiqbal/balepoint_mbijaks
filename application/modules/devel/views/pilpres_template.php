<?php
$suksesi_url = base_url() . 'suksesi/index/'.$val['id_race'].'-'.urltitle($val['race_name']);
$map_photo = '';
$logo_photo = '';
foreach($val['photos'] as $foto){
    if(intval($foto['type']) === 2){
        $map_photo = $foto['thumb_url'];
    }elseif(intval($foto['type']) === 1){
        $logo_photo = $foto['thumb_url'];
    }
}
$status_suksesi = 'Survey / Prediksi';
$index_trace_status = 0;
foreach($val['status'] as $idx => $status){
    if(intval($status['draft']) == 1){
        if(intval($status['status']) == 1)
        {
            $status_suksesi = 'Survey / Prediksi';
        } else {
            if(intval($status['status']) == 2)
            {
                $status_suksesi = 'Putaran Pertama';
            } else {
                $status_suksesi = 'Putaran Kedua';
            }
        }
//        $status_suksesi = (intval($status['status']) == 1) ? 'Survey / Prediksi' : (intval($status['status']) == 2) ? 'Putaran Pertama' : 'Putaran Kedua';
        $index_trace_status = $idx;
    }
}

$hasil_akhir = array_merge($val['status'][1]['lembaga'], $val['status'][0]['lembaga']);

$lembaga_1 = [];
foreach ($val['status'][1]['lembaga'] as $key => $value) {
    $lembaga_1[] = array('page_id' => $value['page_id'], 'lembaga_name' => $value['lembaga_name'], 'lembaga_alias' => $value['lembaga_alias']);
}

$lembaga_all =  array_values($lembaga_1);
$lembaga_0 = [];
foreach ($val['status'][0]['lembaga'] as $key => $value) {
    $lembaga_k = array('page_id' => $value['page_id'], 'lembaga_name' => $value['lembaga_name'], 'lembaga_alias' => $value['lembaga_alias']);
    $is_exist = false;
    foreach ($lembaga_all as $lk => $vl) {
        if($value['page_id'] == $vl['page_id']){
            $is_exist = true;
            break;
        }
    }
    if($is_exist == false)
        $lembaga_all[] = $lembaga_k;
}

// $lembaga_all = array_unique($lembaga_all);

// echo "<pre>";
// var_dump($lembaga_all);
// echo "</pre>";


$total_kandidat = count( $val['status'][$index_trace_status]['kandidat'] );
$tabSection = "section-tab-".$val['id_race'];
?>
<div class="row-fluid">
    <div class="span7">
        <div class="row-fluid">
            <div class="span3 suksesi-img-logo">
                <img src="<?php echo $logo_photo; ?>">
            </div>
            <div class="span9">
                <a href="<?php echo $suksesi_url; ?>"><h4 title="<?php echo $val['race_name']; ?>"><?php echo (strlen($val['race_name']) > 40) ? substr($val['race_name'], 0, 40).'...' : $val['race_name']; ?></h4></a>

            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <table class="table">
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Daftar pemilih tetap</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo number_format($val['daftar_pemilih_tetap'],0,'','.'); ?> Jiwa</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Jumlah kandidat</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo $total_kandidat; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Tanggal pelaksanaan</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <?php echo mdate('%d %M %Y', strtotime($val['tgl_pelaksanaan'])); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="suksesi-ket-place">
                            <span>Status</span>
                        </td>
                        <td class="suksesi-ket-place">
                            <span>: <strong><?php echo (isset($val['status_akhir_suksesi']) && $val['status_akhir_suksesi'] ? $val['status_akhir_suksesi'] : $status_suksesi);?></strong></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    </div>
    <div class="span5 suksesi-img-map">

        <img class="lazy" data-original="<?php echo base_url().'assets/images/suksesi-pilpres.png'; ?>">


        <br>
        <br>
        <br>
        <div class="score-place score-place-overlay score" data-id="<?php echo $val['content_id']; ?>" >
        </div>
        <!--        <div class="score-place score-place-overlay score" data-id="--><?php ////echo $val['content_id']; ?><!--" >-->
        <!--        </div>-->
    </div>
</div>

<!-- <div class=" div-line-small"></div> -->

<!-- OPEN TAB -->
<?php //if(!empty($val['status'][$index_trace_status]['lembaga'])):?>
    <?php
    $t = 0;
    
    ?>
    <div class="row-fluid">
    <div class="span12">

    <!-- ul class="inline ul-inline" -->

    <table summary='' class='home-race-pilpres table' >
        <tr>
            <td></td>
            <td colspan="2" class="well well-small" style="text-align:center;"><h4>QUICK COUNT</h4></td>
            <td style="width: 70px;"></td>
            <td colspan="3" style="text-align:center;"><h4>SURVEY / PREDIKSI</h4></td>
        </tr>
        <tr>
            <td></td>

            <!-- KANDIDAT QUICK COUNT -->
            <?php foreach ($val['status'][1]['kandidat'] as $kes => $vs) {
                $kandidat_page_id = $vs['page_id'];
                $kandidat_name  = $vs['page_name'];
                $kandidat_link  = base_url() . 'aktor/profile/' . $vs['page_id'];

                if($vs['profile_badge_url'] <> 'None')
                {
                    $kandidat_pic = $vs['profile_badge_url'];
                } else {
                    $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                }

                $pasangan_page_id = $vs['page_id_pasangan'];

                if($vs['page_name_pasangan'] != 'None' && $vs['page_name_pasangan'] != 'None' &&
                    $vs['page_name_pasangan'] != 'Tidak Ada Pasangan ' &&  $vs['page_name_pasangan'] != 'Pasangan Politisi' &&
                    ! empty($vs['page_name_pasangan'])
                )
                {
                    $pasangan_name  = $vs['page_name_pasangan'];
                } else {
                    $pasangan_name  = '';
                }

                $pasangan_link  = base_url() . 'aktor/profile/' . $vs['page_id_pasangan'];

                if($vs['profile_badge_pasangan_url'] <> 'None')
                {
                    $pasangan_pic = $vs['profile_badge_pasangan_url'];
                } else {
                    $pasangan_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                }

                ?>

                <?php
                if($pasangan_page_id != 'belummenentukanpilihan539509e07277f')
                {
                    ?>
                <td class="well well-small" class='pilpres-race-kandidat'>
                    <?php if ($kes == 0){ ?>
                    <img class="pilpres-image" src="<?php echo base_url(); ?>assets/images/pilpres-2014/prabowo-hatta.jpg">
                    <?php }else{ ?>
                    <img class="pilpres-image" src="<?php echo base_url(); ?>assets/images/pilpres-2014/jokowi-jk.jpg">
                    <?php } ?>
                    <!-- <div style="width: 150px;height: 60px;">
                      <div style="width: 150px;text-align: center;margin-left:10px;">
                        <a href='<?=$kandidat_link?>' style="float: left;">
                            <div  title="<?=$kandidat_name?>"  class="circular-suksesi-mini" style="background:
                                url(<?=$kandidat_pic?>) no-repeat; background-position: center; background-size:55px 55px;">
                            </div>
                        </a>
                        <a href='<?=$pasangan_link?>' style="float: left;">
                            <div  title="<?=$pasangan_name?>" class="circular-suksesi-mini" style="background:
                                url(<?=$pasangan_pic?>) no-repeat; background-position: center; background-size:55px 55px;">
                            </div>
                        </a>
                      </div>
                    </div>
                    <div style="font-size: 11px;text-align: center;width: 150px;line-height: 12px;height: 30px;"><?=$kandidat_name?></div>
                    <div style="font-size: 11px;text-align: center;width: 150px;line-height: 12px;height: 30px;"><?=$pasangan_name?></div> -->
                </td>
                <?php } ?>
                

            <?php } ?>

            <td></td>
            <!-- KANDIDAT SURVEY -->
            <?php foreach ($val['status'][0]['kandidat'] as $kes => $vs) {
                $kandidat_page_id = $vs['page_id'];
                $kandidat_name  = $vs['page_name'];
                $kandidat_link  = base_url() . 'aktor/profile/' . $vs['page_id'];

                if($vs['profile_badge_url'] <> 'None')
                {
                    $kandidat_pic = $vs['profile_badge_url'];
                } else {
                    $kandidat_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                }

                $pasangan_page_id = $vs['page_id_pasangan'];

                if($vs['page_name_pasangan'] != 'None' && $vs['page_name_pasangan'] != 'None' &&
                    $vs['page_name_pasangan'] != 'Tidak Ada Pasangan ' &&  $vs['page_name_pasangan'] != 'Pasangan Politisi' &&
                    ! empty($vs['page_name_pasangan'])
                )
                {
                    $pasangan_name	= $vs['page_name_pasangan'];
                } else {
                    $pasangan_name	= '';
                }

                $pasangan_link	= base_url() . 'aktor/profile/' . $vs['page_id_pasangan'];

                if($vs['profile_badge_pasangan_url'] <> 'None')
                {
                    $pasangan_pic = $vs['profile_badge_pasangan_url'];
                } else {
                    $pasangan_pic = base_url() . 'assets/images/badge/no-image-politisi.png';
                }

                ?>

                <?php
                if($pasangan_page_id != 'belummenentukanpilihan539509e07277f')
                {
                    ?>
                    <td style="max-width:100px;" class='pilpres-race-kandidat-sm'>
                        <?php if ($kes == 0){ ?>
                        <img class="pilpres-image" src="<?php echo base_url(); ?>assets/images/pilpres-2014/prabowo-hatta.jpg">
                        <?php }else{ ?>
                        <img class="pilpres-image" src="<?php echo base_url(); ?>assets/images/pilpres-2014/jokowi-jk.jpg">
                        <?php } ?>
                        <!-- <div style="width: 150px;height: 60px;">
                          <div style="width: 150px;text-align: center;margin-left:10px;">
                            <a href='<?=$kandidat_link?>' style="float: left;">
                                <div  title="<?=$kandidat_name?>"  class="circular-suksesi-mini" style="background:
                                    url(<?=$kandidat_pic?>) no-repeat; background-position: center; background-size:55px 55px;">
                                </div>
                            </a>
                            <a href='<?=$pasangan_link?>' style="float: left;">
                                <div  title="<?=$pasangan_name?>" class="circular-suksesi-mini" style="background:
                                    url(<?=$pasangan_pic?>) no-repeat; background-position: center; background-size:55px 55px;">
                                </div>
                            </a>
                          </div>
                        </div>
                        <div style="font-size: 11px;text-align: center;width: 150px;line-height: 12px;height: 30px;"><?=$kandidat_name?></div>
                        <div style="font-size: 11px;text-align: center;width: 150px;line-height: 12px;height: 30px;"><?=$pasangan_name?></div> -->
                    </td>
                <?php } else { ?>
                    <td style="max-width: 100px;" class='pilpres-race-kandidat-sm'>
                        <div style="width: 100px;height: 60px;">
                            <div style="text-align: center;width: 100px;">
                            <a href='<?=$kandidat_link?>'>
                                <div  title="<?=$kandidat_name?>"  class="circular-suksesi-mini" style="background:
                                    url(<?=$kandidat_pic?>) no-repeat; background-position: center; background-size:55px 55px;margin-left: 20px;">
                                </div>
                            </a>
                            </div>
                        </div>
                        <div style="font-size: 11px;text-align: center;width: 100px;line-height: 12px;height: 30px;">Belum Menentukan</div>
                        <div style="font-size: 11px;text-align: center;width: 100px;line-height: 12px;height: 30px;"></div>
                    </td>
                <?php } ?>

            <?php } ?>
        </tr> <!-- end of candidate -->


        <?php
        $t=0; $_lembaga = array();$sort_name = array();
        foreach($val['status'][$index_trace_status]['lembaga'] as $racekey => $sortlembaga){
            $sort_name[]  = $sortlembaga['lembaga_name'];
        }
        if(in_array('Rekapitulasi KPU', $sort_name)){
            krsort($val['status'][$index_trace_status]['lembaga']);
        }

        
        foreach ($lembaga_all as $lk => $vl) {

        ?>
            <tr>
                <td class="pilpres-race-title">
                    <strong title="<?php echo $vl['lembaga_name'];?>"><a style="cursor: default;font-size: 14px;"><?php echo $vl['lembaga_name'];?></a></strong>
                </td>

                <!-- QUICK COUNT HASIL-->
                <?php 
                $quick_count_exist = false;
                foreach($val['status'][1]['lembaga'] as $racekey => $lembaga):

                    if($lembaga['page_id'] == $vl['page_id']){
                        $quick_count_exist = true;

                        if(!empty($lembaga['start_date']) && $lembaga['start_date'] <> 'None')
                        {
                            $lembaga_date = date('d-m-Y', strtotime($lembaga['start_date']));
                        } else {
                            $lembaga_date = '';
                        }

                        if(!empty($lembaga['kandidat'][$racekey]['score']['created_date']) && $lembaga['kandidat'][$racekey]['score']['created_date'] <> 'None')
                        {
                            $score_date = date('d/m/Y H:i:s', strtotime($lembaga['kandidat'][$racekey]['score']['created_date']));
                        } else {
                            $score_date = date('d-m-Y', strtotime($lembaga['start_date']));;
                        }

                        
                        $lembaga_name  = $lembaga['lembaga_name'];
                        if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];

                        $id_lembaga = $lembaga['id_trace_lembaga'];
                        $words  = preg_split("/\s+/", $lembaga_name);
                        $acronym = $lembaga['lembaga_alias']; // (count($word)s == 1) ? substr($words[0], 0,3) : '';
                        $tabName = 'tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                        $target = 'conten-tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];

                        if($acronym == 'RK') {
                            $backtab = 'style="background-color:#cecece;"';
                        } else {
                            $backtab = '';
                        }

                        ?>
                        
                        <?php

                            $total_score    = 0;
                            $winner         = '';
                            $winner_score   = 0;
                            $pct = 0;
                            foreach($lembaga['kandidat'] as $kandidat) {
                                if(count($kandidat['score']) > 0){
                                    $score = $kandidat['score'];
                                    $kandidat = $kandidat['kandidat'];
                                    if ($score['score_type'] == '1') {
                                        $total_score += (int) $score['score'];
                                    }
                                    if ($score['score'] > $winner_score) {
                                        $winner             = $kandidat['page_id'];
                                        $winner_score   = $score['score'];
                                    }
                                }
                            }
                            foreach($lembaga['kandidat'] as $kandidatkey => $kandidat)
                            {

                                $score = $kandidat['score'];
                                $kandidat = $kandidat['kandidat'];
                                if(!empty($score)){

                                    if ($score['score_type'] == '0') {
                                        $pct            = number_format($score['score'],2);
                                    } else {
                                        $pct            = number_format(($score['score'] / $total_score)*100,2);
                                    }
                                }

                                if($lembaga_name == 'Rekapitulasi KPU')
                                {
                                    $winner_bar     = ($winner == $kandidat['page_id'] ) ? '' : '';
                                    $winner_text = ($winner == $kandidat['page_id'] ) ? '<div class="pilpres-bar-1">Pemenang  '.$pct.' %</div>' : '<div class="pilpres-bar-1">'.$pct.' %</div>';
                                } else {
                                    $winner_bar     = ($winner == $kandidat['page_id'] ) ? '' : '';
                                    $winner_text = ($winner == $kandidat['page_id'] ) ? '<div class="pilpres-bar-2">'.$pct.' %</div>' : '<div class="pilpres-bar-2">'.$pct.' %</div>';
                                }

                                ?>

                                <td  style="text-align:center;" class='pilpres-race-percentage well well-small'>
                                    <div class='pilpres-percentage-box'>
                                       <div class='pilpres-percentage-bar pilpres-percentage-bar-<?=$kandidatkey;?>' style='height:<?php echo $pct; ?>px;<?=$winner_bar;?>'></div><?=$winner_text;?>
                                       
                                    </div>
                                    <em><small><?php echo $score_date;?></small></em>
                                </td>

                            <?php
                            }

                            ?>
                    <?php
                    $t++;
                        break;
                    }else{
                    ?>
                       
                    <?php 
                    }
                    endforeach;
                    if($quick_count_exist == false){
                        ?>

                        <td class="well well-small">
                            <div class='pilpres-percentage-box'>
                               <div class='pilpres-percentage-bar' ></div><div class="pilpres-bar-2">0.00 %</div>
                            </div>
                        </td>
                        <td class="well well-small">
                            <div class='pilpres-percentage-box'>
                               <div class='pilpres-percentage-bar' ></div><div class="pilpres-bar-2">0.00 %</div>
                            </div>
                        </td>

                    <?php } ?>
                    
                
                <td></td>    
                <!-- SURVEY HASIL-->
                <?php 
                $survey_exist = false;
                foreach($val['status'][0]['lembaga'] as $racekey => $lembaga):
                    if($lembaga['page_id'] == $vl['page_id']){
                        $survey_exist = true;
                        if(!empty($lembaga['start_date']) && $lembaga['start_date'] <> 'None')
                        {
                            $lembaga_date = date('d-m-Y', strtotime($lembaga['start_date']));
                        } else {
                            $lembaga_date = '';
                        }

                        if(!empty($lembaga['kandidat'][$racekey]['score']['created_date']) && $lembaga['kandidat'][$racekey]['score']['created_date'] <> 'None')
                        {
                            $score_date = date('d/m/Y H:i:s', strtotime($lembaga['kandidat'][$racekey]['score']['created_date']));
                        } else {
                            $score_date = date('d-m-Y', strtotime($lembaga['start_date']));;
                        }

                        
                        $lembaga_name  = $lembaga['lembaga_name'];
                        if(empty($lembaga_name)) $lembaga_name  = $lembaga['page_id'];

                        $id_lembaga = $lembaga['id_trace_lembaga'];
                        $words  = preg_split("/\s+/", $lembaga_name);
                        $acronym = $lembaga['lembaga_alias']; // (count($word)s == 1) ? substr($words[0], 0,3) : '';
                        $tabName = 'tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];
                        $target = 'conten-tab-'.$index_trace_status . '-'.$lembaga['id_trace_lembaga'];

                        if($acronym == 'RK') {
                            $backtab = 'style="background-color:#cecece;"';
                        } else {
                            $backtab = '';
                        }

                        ?>
                        
                        <?php

                            $total_score    = 0;
                            $winner         = '';
                            $winner_score   = 0;
                            $pct = 0;
                            foreach($lembaga['kandidat'] as $kandidat) {
                                if(count($kandidat['score']) > 0){
                                    $score = $kandidat['score'];
                                    $kandidat = $kandidat['kandidat'];
                                    if ($score['score_type'] == '1') {
                                        $total_score += (int) $score['score'];
                                    }
                                    if ($score['score'] > $winner_score) {
                                        $winner             = $kandidat['page_id'];
                                        $winner_score   = $score['score'];
                                    }
                                }
                            }
                            foreach($lembaga['kandidat'] as $kandidatkey => $kandidat)
                            {

                                $score = $kandidat['score'];
                                $kandidat = $kandidat['kandidat'];
                                if(!empty($score)){

                                    if ($score['score_type'] == '0') {
                                        $pct            = number_format($score['score'],2);
                                    } else {
                                        $pct            = number_format(($score['score'] / $total_score)*100,2);
                                    }
                                }

                                if($lembaga_name == 'Rekapitulasi KPU')
                                {
                                    $winner_bar     = ($winner == $kandidat['page_id'] ) ? 'background-color:#128405;' : '';
                                    $winner_text = ($winner == $kandidat['page_id'] ) ? '<div class="pilpres-bar-1">Pemenang  '.$pct.' %</div>' : '<div class="pilpres-bar-1">'.$pct.' %</div>';
                                } else {
                                    $winner_bar     = ($winner == $kandidat['page_id'] ) ? 'background-color:#847CE2;;' : '';
                                    $winner_text = ($winner == $kandidat['page_id'] ) ? '<div class="pilpres-bar-2">'.$pct.' %</div>' : '<div class="pilpres-bar-2">'.$pct.' %</div>';
                                }

                                

                                ?>

                                <td style="text-align:center;" class='pilpres-race-percentage-sm'>
                                    <div class='pilpres-percentage-box-sm'>
                                       <div class='pilpres-percentage-bar' style='height:<?php echo $pct; ?>px;<?=$winner_bar;?>'></div><?=$winner_text;?>

                                    </div>
                                    <em><small><?php echo $score_date;?></small></em>
                                </td>

                            <?php
                            }

                            ?>
                    <?php
                    $t++;
                        break;
                    }else{
                    ?>
                    <?php 
                    }
                    endforeach;
                    if($survey_exist == false){
                        ?>

                        <td>
                            <div class='pilpres-percentage-box'>
                               <div class='pilpres-percentage-bar' ></div><div class="pilpres-bar-2">0.00 %</div>
                            </div>
                        </td>
                        <td>
                            <div class='pilpres-percentage-box'>
                               <div class='pilpres-percentage-bar' ></div><div class="pilpres-bar-2">0.00 %</div>
                            </div>
                        </td>
                        <td>
                            <div class='pilpres-percentage-box'>
                               <div class='pilpres-percentage-bar' ></div><div class="pilpres-bar-2">0.00 %</div>
                            </div>
                        </td>

                    <?php } ?>

            </tr>

        <?php } ?>



        

    </table>
    <!-- /ul-->
    </div>
    </div>
<?php //endif; ?>

