<div class="container" style="margin-top: 30px;">
    <div class="sub-header-container">
        <div class="logo-small-container">
            <a href="<?php echo base_url(); ?>">
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" >
            </a>
        </div>
        <div class="right-container" style="background-color: #ffffff;">
            <div class="banner">
                <img src="<?php echo base_url('assets/images/indonesia-lebih-baik.png'); ?>">
            </div>
            <div class="category-news">
                <div class="category-news-title category-news-title-right" style="background: none;">
                    <h1>DEVELOPMENT</h1>
                </div>

            </div>
        </div>
    </div>
</div>
<br/>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <div class="span8">
                <div class="row-fluid" id="home-opini">
                    <div class="home-title-section hp-label hp-label-coklat" style="background-color: #9c6851;margin-top:0px;">
                        <span class="hitam"><a style="cursor:default;color:#ffffff;">KOMENTAR ANDA</a></span>
                    </div>
                    <div id="komentar_container" data-tipe="1" data-page='1'></div>
                </div>
                <div class="row-fluid">
                    <div class="span12 text-right">
                        <a id="komentar_loadmore"  data-tipe="1" class="btn btn-mini" >20 Berikutnya</a>
                    </div>
                </div>

            </div>

            <div class="span4">
                <div class="row-fluid" id="home-blog">
                    <div class="home-title-section hp-label hp-label-coklat" style="background-color: #9c6851;margin-top:0px;">
                        <span class="hitam"><a style="cursor:default;color:#ffffff;">BLOG FAVORIT ANDA</a></span>
                    </div>

                    <div id="blog_container" data-tipe="1" data-page='1'></div>
                </div>

                <div class="row-fluid">
                    <div class="span12 text-right">
                        <a id="blog_loadmore"  data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                    </div>
                </div>
            </div>

        </div>

        <div class="row-fluid">
          <div class="span12">
            <div class="row-fluid" id="home-latest">
                <div class="home-title-section hp-label hp-label-coklat" style="background-color: #9c6851;margin-top:0px;">
                    <span class="hitam"><a style="cursor:default;color:#ffffff;">LATEST BLOG</a></span>
                </div>

                <div id="bloglatest_container" data-tipe="1" data-page='1'></div>
            </div>

            <div class="row-fluid">
                <div class="span12 text-right">
                    <a id="bloglatest_loadmore"  data-tipe="1" class="btn btn-mini" >5 Berikutnya</a>
                </div>
            </div>
          </div>
        </div>


    </div>
</div>