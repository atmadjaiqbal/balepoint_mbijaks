<div class="container">
	<?php $this->load->view('template/tpl_sub_header'); ?>		
</div>
<br>

<div class="container">
	<div class="sub-header-container">
		<div class="row-fluid">
			<!-- CONTENT DETAIL -->
            <div class="span8">
                <?php //echo '<pre>'; var_dump($news['type_news']); echo '</pre>';?>
                <?php if($news['type_news'] == 3){ ?>


                <div class="news-detail">
                    <h2><?php echo $news['title']; ?></h2>
                    <h4><?php echo mdate('%d %M %Y %h:%i', strtotime($news['date'])); ?></h4>
                    <img class="news-img" alt="<?php echo $news['title']; ?>" src="<?php echo $news['image_large']; ?>">
                    <div class="score-place score-place-overlay score" data-id="<?php echo $news['content_id']; ?>" >
                    </div>
                    <br>
                    <div class="row-fluid">
                        <span class="pull-left">Politisi Terkait</span>
                        <em class="pull-right"><?php echo $news['caption']; ?></em>
                    </div>
                    <div class="row-fluid news-profile-holder">
                        <div class="span6">
                            <div class="row-fluid ">
<!--                                <div class="span"><span>Politisi Terkait</span></div>-->
                                <div class="span12">
                                <?php if (count($news['news_profile']) > 0){ ?>
                                <?php foreach ($news['news_profile'] as $key => $pro) { ?>
                                    <?php
                                        $img_pola = key_exists('score', $pro) ? 'img-btm-border-'.$pro['score'] : '';
                                    ?>
                                    <div class="content-politisi-terkait">
                                        <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                            <img class="img-btm-border <?php echo $img_pola;?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                                        </a>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php }else{ ?>
                                <div class="span10"><span>Tidak ada politisi terkait</span></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="span6">
                            <div class="time-line-content pull-right" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo $news['content_id']; ?>">
                                <?php //echo $news['timeline']; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $conten = strip_tags($news['content'], '<p>');
                    $conten = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $conten);
                    ?>
                    <p><?php echo strip_tags($conten, '<p>'); ?></p>

                </div>

                <?php }else{ ?>
                <div class="news-detail">
                    <h2><?php echo $news['title']; ?></h2>
                    <h4><?php echo mdate('%d %M %Y %h:%i', strtotime($news['date'])); ?></h4>
                    <div class="news-detail-small">
                        <img class="news-img" alt="<?php echo $news['title']; ?>" src="<?php echo $news['image_large']; ?>">
                        <div class="score-place score-place-overlay score" data-id="<?php echo $news['content_id']; ?>" >
                        </div>
                        <div class="row-fluid text-right" style="margin-top: 10px;">
                            <em class=""><?php echo $news['caption']; ?></em>
                        </div>
                        <div class="row-fluid ">
                            <div class="span2"><span>Politisi Terkait</span></div>
                            <div class="span10">
                            <?php if (count($news['news_profile']) > 0){ ?>
                            <?php foreach ($news['news_profile'] as $key => $pro) { ?>
                                <?php
                                $img_pola = key_exists('score', $pro) ? 'img-btm-border-'.$pro['score'] : '';
                                ?>
                                <div class="content-politisi-terkait">
                                    <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                                        <img class="img-btm-border <?php echo $img_pola;?>" title="<?php echo $pro['page_name']; ?>" alt="<?php echo $pro['page_name']; ?>" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                                    </a>
                                </div>
                                <?php } ?>


                            <?php }else{ ?>
                            <span>Tidak ada politisi terkait</span>
                            <?php } ?>
                            </div>
                        </div>
                        <p></p>
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="time-line-content pull-right" data-cat="<?php echo strtolower($category); ?>" data-uri="<?php echo $news['content_id']; ?>">
                                    <?php //echo $news['timeline']; ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <?php
                    $conten = strip_tags($news['content'], '<p>');
                    $conten = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $conten);
                    ?>
                    <p><?php echo strip_tags($conten, '<p>'); ?></p>
                </div>
                <?php } ?>
                <!-- KOMENTAR -->
                <div class="div-line"></div>
                <div class="row-fluid">

                    <h5>BERI KOMENTAR</h5>
                    <div class="row-fluid">
                        <div class="comment-side-left">
                            <div style="background:
                                    url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                            </div>
                        </div>

                        <div class="comment-side-right">
                            <div class="row-fluid">
                                <input id='comment_type' data-id="<?=$news['content_id'];?>" type="text" name="comment" class="media-input" >
                            </div>
                            <div class="row-fluid">
                                <div class="span12 text-right">
                                    <?php if(!is_array($this->member)){?>
                                    <span>Login untuk komentar</span>&nbsp;
                                    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
                                    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
                                    <?php }elseif(is_array($this->member)){?>
                                    <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="comment" data-page="1" data-id="<?=$news['content_id'];?>" class="row-fluid comment-container">

                </div>
                <div id="load_more_place" class="row-fluid komu-follow-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a data-page="1" data-id="<?=$news['content_id'];?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>
                <div id="div_line_bottom" class="div-line"></div>

			</div>



			<!-- RELATED NEWS -->
			<div class="span4 right-side">
                <div class="home-title-section hp-label hp-label-hitam" style="background-color: #bbbbbb;margin-top:-25px;">
                    <span class="hitam"><a style="cursor:default;">Berita <?php echo ucfirst($category);?> Terkait</a></span>
                </div>
                <p></p>
                <div class="">
                <?php
                    $dt['val'] = $news_list;
                    $this->load->view('home/news_headline_list', $dt);
                ?>
                </div>
<!--				--><?php //foreach ($news_list as $key => $row) { ?>
<!--					<div class="row-fluid">-->
<!--						<div class="span12">-->
<!--						--><?php //
//							$dt['news_detail'] = $row;
//							$dt['val'] = $row;
////							$this->load->view('template/content/tpl_news_index', $dt);
//							$this->load->view('home/news_headline_list', $dt);
//
//						?>
<!--						</div>-->
<!--					</div>-->
<!--				--><?php //} ?>
			</div>
		</div>
	</div>
</div>
<br>

<div class="container">
	<div class="sub-header-container">
		<div class="row-fluid">
			<img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
		</div>
	</div>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">

            <!-- HOT PROFILE -->
			<?php echo $hot_profile; ?>


			<!-- SKANDAL -->
			<?php echo $skandal; ?>


			<!-- HOT ISSUE -->
			<?php //echo $issue; ?>
            <?php echo $survey; ?>

		</div>
	</div>
</div>
<br>
<div class="container-bawah">

	<div class="container">
		<div class="sub-header-container">
			<div class="row-fluid">
				
				<!-- EKONOMI -->
				<?php echo $ekonomi; ?>
				
				
				<!-- HUKUM -->
				<?php echo $hukum; ?>
				

				<!-- PARLEMEN -->
				<?php echo $parlemen; ?>
				
			</div>
		</div>
	</div>
	<br>

	<div class="container">
		<div class="sub-header-container">
			<div class="row-fluid">
				
				<!-- EKONOMI -->
				<?php echo $nasional; ?>
				
				
				<!-- HUKUM -->
				<?php echo $daerah; ?>
				

				<!-- PARLEMEN -->
				<?php echo $internasional; ?>
				
			</div>
		</div>
	</div>
	<br>
</div>
<?php $this->load->view('template/modal/report_spam_modal'); ?>