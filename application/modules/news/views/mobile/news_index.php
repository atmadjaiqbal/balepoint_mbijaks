<?php
switch($category)
{
    case "hukum" : $_head_title = "HUKUM"; break;
    case "kesenjangan" : $_head_title = "KESENJANGAN"; break;
    case "parlemen" : $_head_title = "PARLEMEN"; break;
    case "nasional" : $_head_title = "NASIONAL"; break;
    case "ekonomi" : $_head_title = "EKONOMI"; break;
    case "teknologi" : $_head_title = "TEKNOLOGI"; break;
    case "hankam" : $_head_title = "HANKAM"; break;
    case "daerah" : $_head_title = "DAERAH"; break;
    case "reses" : $_head_title = "SENGGANG"; break;
    case "internasional" : $_head_title = "INTERNASIONAL"; break;
    case "headline" : $_head_title = "TAJUK UTAMA"; break;
    case 'gayahidup' : $_head_title = "GAYA HIDUP"; break;
    case 'komunitas' : $_head_title = "SOSIAL MEDIA"; break;
    case 'lingkungan' : $_head_title = "LINGKUNGAN"; break;
    case 'sara' : $_head_title = "SARA"; break;
    case 'indobarat' : $_head_title = "DAERAH BARAT"; break;
    case 'indoteng' : $_head_title = "DAERAH TENGAH"; break;
    case 'indotim' : $_head_title = "DAERAH TIMUR"; break;

}

?>

<h4 class="kanal-title kanal-title-gray"><?php echo 'POLITIK '.strtoupper($_head_title);?></h4>
<?php
foreach($news as $key=>$result){

    $_topic_id = !empty($result['topic_id']) ? $result['topic_id'] : '';
    $_id = !empty($result['id']) ? $result['id'] : '';
    $_slug = !empty($result['slug']) ? $result['slug'] : '';
    $_title = !empty($result['title']) ? $result['title'] : '';

    $news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
    $short_title = (strlen ($_title) > 38) ? substr($_title, 0, 38). '...' : $_title;
    if(!empty($result['image_thumbnail']))
    {
        $_image_thumb = $result['image_thumbnail'];
    } else {
        $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
    }
    ?>

<div class="row">
    <?php if($key < 10 || $category == 'headline'){ ?>
    <div class="col-xs-4 col-sm-4 nopadding">
        <a class="" href="<?php echo $news_url;?>">
            <img class="img-media-list" src="<?php echo $_image_thumb;?>" alt="<?php echo !empty($result['title']) ? $result['title'] : ''; ?>" >
        </a>
<!--        <h4 class="labes labes---><?php //echo $_color;?><!--" style="background-color:--><?php //echo $_color;?><!--;">--><?php //echo $segment_title;?><!--</h4>-->
    </div>
    <div class="col-xs-8 col-sm-8">
        <a class="" href="<?php echo $news_url;?>">
            <h5 class="media-heading-nomargin"><?php echo $_title; ?></h5>
        </a>
        <small class="media-heading-nomargin"><?php echo !empty($result['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($result['date'])) : ''; ?></small>
        <p><?php echo (strlen($result['excerpt']) < 65 ? $result['excerpt'] : substr($result['excerpt'], 0, 62) . '...'); ?></p>
    </div>
    <?php }else{ ?>

    <div class="col-xs-12 col-sm-12">
        <a class="" href="<?php echo $news_url;?>">
            <h5 class="media-heading-nomargin"><?php echo $_title; ?></h5>
        </a>
        <small class="media-heading-nomargin"><?php echo !empty($result['date']) ? mdate('%d %M %Y - %h:%i', strtotime($result['date'])) : ''; ?></small>
    </div>
    <?php } ?>

</div>
<hr class="line-mini">

<?php } ?>
<div class="row">
    <div class="col-xs-12 text-center">
        <?php echo $pagi;?>
    </div>
</div>
