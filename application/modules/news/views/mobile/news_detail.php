<style>
html, body {
    max-width: 100% !important;
    overflow-x: hidden !important;
}
.boxed{
    border: solid 1px #c4c4c4;
    padding: 3px 3px 3px 3px;
}

.socmedicon{
    width: 22px;
    height: 22px;
    margin-right: 3px;
}

</style>

<div style="margin-top: 5px;">
    <?php echo $ads_banner[1]; ?>
</div>


<?php
switch($category)
{
    case "hukum" : $_head_title = "HUKUM"; break;
    case "kesenjangan" : $_head_title = "KESENJANGAN"; break;
    case "parlemen" : $_head_title = "PARLEMEN"; break;
    case "nasional" : $_head_title = "NASIONAL"; break;
    case "ekonomi" : $_head_title = "EKONOMI"; break;
    case "teknologi" : $_head_title = "TEKNOLOGI"; break;
    case "hankam" : $_head_title = "HANKAM"; break;
    case "daerah" : $_head_title = "DAERAH"; break;
    case "reses" : $_head_title = "SENGGANG"; break;
    case "internasional" : $_head_title = "INTERNASIONAL"; break;
    case "headline" : $_head_title = "TAJUK UTAMA"; break;
    case 'gayahidup' : $_head_title = "GAYA HIDUP"; break;
    case 'komunitas' : $_head_title = "SOSIAL MEDIA"; break;
    case 'lingkungan' : $_head_title = "LINGKUNGAN"; break;
    case 'sara' : $_head_title = "SARA"; break;
    case 'indobarat' : $_head_title = "DAERAH BARAT"; break;
    case 'indoteng' : $_head_title = "DAERAH TENGAH"; break;
    case 'indotim' : $_head_title = "DAERAH TIMUR"; break;

}
?>
<h4 class="kanal-title kanal-title-gray">POLITIK <?php echo strtoupper($_head_title);?></h4>
<div class="row">
	
	<div class="col-xs-12 col-sm-12">
	       <p></p>
        <h4 class="media-heading"><?php echo $news['title']; ?></h4>
        <em><small><?php echo mdate('%d %M %Y %h:%i %A', strtotime($news['date'])); ?></small></em>
        <div style="float:right;margin-top:-5px;margin-bottom: 5px;">
            <!-- <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $og_slug; ?>&t=<?php echo $news['title']; ?>"
               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
               target="_blank" title="Share on Facebook">
               <img class="socmedicon" src="<?php echo base_url();?>assets/images/fb_bw.png">
            </a> -->
            <div class="fb-share-button" data-href="<?php echo $og_slug; ?>" data-layout="button"></div>
            <a href="https://twitter.com/share?url=<?php echo $og_slug; ?>&via=bijaksdotnet&text=<?php echo $news['title']; ?>"
               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
               target="_blank" title="Share on Twitter">
                <img  class="socmedicon" src="<?php echo base_url();?>assets/images/tw_bw.png">
            </a>
            <a href="https://plus.google.com/share?url=<?php echo $og_slug; ?>"
               onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');return false;"
               target="_blank" title="Share on Google+">
                <img class="socmedicon" src="<?php echo base_url();?>assets/images/gplus_bw.png">
            </a>
            <a href="http://pinterest.com/pin/create/button/?url={<?php echo $og_slug; ?>}&media={<?php echo $og_image; ?>}" class="pin-it-button" count-layout="horizontal">
                <img class="socmedicon" src="<?php echo base_url();?>assets/images/pin_bw.png">
            </a>
        </div>
        <img class="img-media-list" src="<?php echo $news['image_large']; ?>" alt="<?php echo $news['title']; ?>">
        <div class="score-place score-place-overlay score" data-id="<?php echo $news['content_id']; ?>" ></div>
        <div class="clear"></div>

        <p style="margin-top: 10px;">Politisi Terkait :</p>
        <?php if (count($news['news_profile']) > 0){ 
    		echo '<ul class="ul-img-hr">';
    	?>
        <?php foreach ($news['news_profile'] as $key => $pro) { ?>
            <li class="">
                <a href="<?php echo base_url(); ?>aktor/profile/<?php echo $pro['page_id']; ?>">
                    <img class="img-btm-border img-btm-border-<?php echo (key_exists('pengaruh', $pro) ? $pro['pengaruh']: '0');?>" title="<?php echo $pro['page_name']; ?>" alt="" src="<?php echo icon_url($pro['attachment_title'], 'politisi/'. $pro['page_id'], False ); ?>">
                </a>
            </li>
            
            <?php } echo '</ul>'; ?>
        <?php }else{ ?>
        <span>Tidak ada politisi terkait</span>
        <?php } ?>
        <?php
        $conten = strip_tags($news['content'], '<p>');
        $conten = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $conten);
        ?>
        <p><?php echo strip_tags($conten, '<p>'); ?></p>
    </div>
</div>
<hr class="line-mini"/>
<!-- KOMENTAR -->
<div class="div-line"></div>

<?php
if(count($news_terkait) > 0){
?>
<div>
   <h5 class="sub-kanal-title kanal-title-gray-soft">BERITA TERKAIT</h5>
<?php
   foreach($news_terkait as $key => $rwterkait)
   {
      $data['result'] = $rwterkait; ?>
      <?php echo $this->load->view('news/mobile/news_terkait', $data); ?>
<?php
   }
?>
  <br>
</div>
<div class="div-line"></div>
<?php
    }
?>

<h5>BERI KOMENTAR</h5>
<div class="row-fluid">
<!--     <div class="comment-side-left">
        <div style="background:url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

        </div>
    </div> -->

    <div class="comment-side-right">
        <div class="row-fluid">
            <!-- input id='comment_type' data-id="<?=$news['content_id'];?>" type="text" name="comment" class="media-input" -->
            <textarea id='comment_type' data-id="<?=$news['content_id'];?>" name="comment" class="media-input" style="width:80%;height: 80px;"></textarea>
        </div>
        <div class="row-fluid">
            <div class="span12 text-left">
                <?php if(!is_array($this->member)){?>
                <span>Login untuk komentar</span>&nbsp;
                <a href="<?php echo base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
                <a href="<?php echo base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
                <?php }elseif(is_array($this->member)){?>
                <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                    <?php } ?>
            </div>
        </div>
    </div>
</div>

<!-- komentar -->
<div style="clear: both;"></div>
<div>&nbsp;</div>
<hr class="line-mini"/>
<div id="comment" data-page="1" data-id="<?=$news['content_id'];?>" class="row-fluid comment-container"></div>
<div id="load_more_place" class="row-fluid komu-follow-list komu-wall-list-bg">
    <div class="span12 text-center">
        <a data-page="1" data-id="<?=$news['content_id'];?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
    </div>
</div>

<!-- Begin Banner Ads -->

<div>
<!-- <a href="<?php echo $_link_download;?>"> -->
    <?php echo $ads_banner[2]; ?>
<!-- </a> -->
</div>
<hr class="line-mini">
<!-- End Of Ads Banner -->

<div style="clear: both;"></div>
<p><a href="<?php echo base_url(); ?>">[ Kembali ke Beranda ]</a></p>
<div id="div_line_bottom" class="div-line"></div>


<div>
    <h4 class="kanal-title kanal-title-cokelat">TAJUK UTAMA</h4>
    <?php echo $issue;?>
    <a href="<?php echo base_url().'news/index/headline'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br><br>

    <div style="margin-top: 5px;">
       <?php echo $ads_banner[3]; ?>
    </div><br/>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK NASIONAL</h5>
    <?php echo $nasional;?>
    <a href="<?php echo base_url().'news/index/nasional'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK HUKUM</h5>
    <?php echo $hukum;?>
    <a href="<?php echo base_url().'news/index/hukum'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br><br>

    <div style="margin-top: 5px;">
       <?php echo $ads_banner[4]; ?>
    </div>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK EKONOMI</h5>
    <?php echo $ekonomi;?>
    <a href="<?php echo base_url().'news/index/ekonomi'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK PARLEMEN</h5>
    <?php echo $parlemen;?>
    <a href="<?php echo base_url().'news/index/parlemen'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br><br>

    <div style="margin-top: 5px;">
       <?php echo $ads_banner[5]; ?>
    </div>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK INTERNASIONAL</h5>
    <?php echo $internasional;?>
    <a href="<?php echo base_url().'news/index/internasional'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK DAERAH BARAT</h5>
    <?php echo $indobarat;?>
    <a href="<?php echo base_url().'news/index/indobarat'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK DAERAH TENGAH</h5>
    <?php echo $indoteng;?>
    <a href="<?php echo base_url().'news/index/indoteng'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK DAERAH TIMUR</h5>
    <?php echo $indotim;?>
    <a href="<?php echo base_url().'news/index/indotim'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br><br>

    <div style="margin-top: 5px;">
       <?php echo $ads_banner[6]; ?>
    </div>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK LINGKUNGAN</h5>
    <?php echo $lingkungan;?>
    <a href="<?php echo base_url().'news/index/lingkungan'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK KESENJANGAN</h5>
    <?php echo $kesra;?>
    <a href="<?php echo base_url().'news/index/kesenjangan'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK SARA</h5>
    <?php echo $sara;?>
    <a href="<?php echo base_url().'news/index/sara'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK SOSIAL MEDIA</h5>
    <?php echo $komunitas;?>
    <a href="<?php echo base_url().'news/index/komunitas'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br><br>
    
    <div style="margin-top: 5px;">
       <?php echo $ads_banner[7]; ?>
    </div>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK GAYA HIDUP</h5>
    <?php echo $gayahidup;?>
    <a href="<?php echo base_url().'news/index/gayahidup'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>

    <h5 class="sub-kanal-title kanal-title-gray-soft">POLITIK SENGGANG</h5>
    <?php echo $reses;?>
    <a href="<?php echo base_url().'news/index/reses'; ?>" class="pull-right btn btn-default btn-xs" >Selengkapnya</a>
    <br>
</div>

<?php /*
<h4 class="kanal-title kanal-title-gray">POLITIK <?php echo strtoupper($_head_title);?> lainnya</h4>
<?php
foreach($news_list as $key=>$result){

    $_topic_id = !empty($result['topic_id']) ? $result['topic_id'] : '';
    $_id = !empty($result['id']) ? $result['id'] : '';
    $_slug = !empty($result['slug']) ? $result['slug'] : '';
    $_title = !empty($result['title']) ? $result['title'] : '';

    $news_url = base_url() . "news/article/" . $_topic_id . "-" . $_id . "/" . trim( str_replace('/', '', $_slug));
    $short_title = (strlen ($_title) > 38) ? substr($_title, 0, 38). '...' : $_title;
    if(!empty($result['image_thumbnail']))
    {
        $_image_thumb = $result['image_thumbnail'];
    } else {
        $_image_thumb = base_url(). 'assets/images/thumb/noimage.jpg';
    }
    ?>

<div class="row">
    <div class="col-xs-12 col-sm-12">
        <a class="" href="<?php echo $news_url;?>">
            <h5 class="media-heading"><?php echo $_title; ?> - <small><?php echo !empty($result['date']) ? mdate('%d %M %Y - %h:%i %A', strtotime($result['date'])) : ''; ?></small></h5>
        </a>
        
    </div>

</div>
<hr class="line-mini">

<?php } ?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="btn btn-small">
        <a class="" href="<?php echo base_url('news/index/'.$category);?>">
            Selengkapnya
        </a>
        </div>
    </div>

</div>
*/?>