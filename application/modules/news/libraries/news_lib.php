<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_Lib {

	protected  $CI;
	public $arrCategory = array(
			1 => "hukum",
			3 => "kesra",
			4 => "ekonomi",
			7 => "parlemen",
			8 => "hot-issues",
			9 => "nasional",
			10 => "teknologi",
			12 => "hankam",
			17 => "daerah",
			18 => "reses",
			19 => "internasional"
	);

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database('slave');
	}

	function getNewsCount($select='*', $where=array(), $limit=0, $offset=0)
	{
		$this->CI->db->select($select);
		$this->CI->db->where($where);
		if($limit != 0){
			$this->CI->db->limit($limit, $offset);
		}
		$this->CI->db->group_by('content_id');
		$data = $this->CI->db->get('tcontent_has_topic');
		// echo $this->CI->db->last_query();
		return $data;
	}

	function getNewsListTopic($select='*', $where=array(), $limit=0, $offset=0, $order='')
	{
		$this->CI->db->select($select);
		$this->CI->db->from('tcontent_has_topic tht');
		$this->CI->db->join('tcontent_text tt', 'tt.content_id = tht.content_id');
		$this->CI->db->join('tcontent t', 't.content_id = tht.content_id');
		$this->CI->db->where($where);
		if($limit != 0){
			$this->CI->db->limit($limit, $offset);
		}
		$this->CI->db->order_by($order);
		$this->CI->db->group_by('tht.content_id');

		$query = $this->CI->db->get();
		// echo $this->CI->db->last_query();
		return $query;

	}

	function getNewsProfile($select='*', $where=array(), $limit=0, $offset=0)
	{
		$this->CI->db->select($select);
		$this->CI->db->from('tcontent_has_politic thp');
		$this->CI->db->join('tobject_page tp', 'tp.page_id = thp.page_id');
		$this->CI->db->join('tcontent_attachment t', 't.content_id = tp.profile_content_id');
		$this->CI->db->where($where);
		if($limit != 0){
			$this->CI->db->limit($limit, $offset);
		}
		// $this->CI->db->order_by($order);

		$query = $this->CI->db->get();
		return $query;
	}

	function getCategoryID($category)
	{
	   $return = array_search($category, $this->arrCategory);
	   return $return;
	}

	function getCategoryName($category)
	{
	   $return = "Berita " . ucwords($category);

		if( $category == "hankam" ) {
			$return = "Berita Hankam";

		} else if($category == "hukum") {
			$return = "Berita Hukum";

		} else if( ! in_array($category, $this->arrCategory) ) {
			$return = "Berita Aktual";
		}

	   return $return;
   }

	public function getNewsList($category_id, $limit='10', $offset='0', $slug='', $page_id='', $limit_politisi='5')
	{
		$member = $this->CI->session->userdata('member');
		$member_account_id = (isset($member['account_id'])) ? $member['account_id'] : NULL;

		$where = " WHERE 1=1 "; //
		if( ! empty($category_id)) {
			$where .= " AND tp.topic_id = '".$category_id."'";
		}

		if( !empty($slug)){
			$where .= " AND  tt.content_url != '" . $slug . "'";
		}

		// TO HANDLE PAGE ID NYA POLITISI OR SIAPALAH
		if( ! empty($page_id) ) {
			$where .= " AND (tc.page_id = '" . $page_id . "' or tc.content_id in (select content_id from tcontent_has_politic where page_id = '" . $page_id . "')) ";
		}

		$sql = "SELECT tp.topic_id, tc.title, tt.*,tc.entry_date,
						tc.count_comment, tc.count_like, tc.count_dislike,
						get_count_like_content_by_account('".$member_account_id."', tt.content_id) as is_like,
						get_count_dislike_content_by_account('".$member_account_id."', tt.content_id) as is_dislike
				FROM tcontent_text tt
				JOIN tcontent tc ON tc.content_id = tt.content_id
				JOIN tcontent_has_topic tp on tp.content_id = tt.content_id
				".$where."
				GROUP BY tc.content_id
				ORDER BY tt.content_date DESC limit ".$limit." offset ".$offset;

		$result = $this->CI->db->query($sql)->result_array();
		if  (!empty($result))
		{
			foreach($result as $key => $row) {
				$result[$key]['politisi_terkait'] = $this->CI->bijak->getPolitisiTerkait($row['content_id'], 'cid', $limit_politisi);
				$result[$key]['content_activity'] = $this->CI->bijak->getContentActivity($row['content_id']);
			}
		}

		return $result;
	}

	public function getNewsListCount($category_id, $slug='')
	{
		$where = "";
		if( !empty($category_id)) 	$where .= "AND tp.topic_id = '".$category_id."'";
		if( !empty($slug))			$where .= " AND  tt.content_url != '" . $slug . "'";

		$sql = "SELECT count(*) AS count_row
					FROM
					(
        				SELECT tc.content_id
        				FROM tcontent_text tt
        				JOIN tcontent tc ON tc.content_id = tt.content_id
						JOIN  tcontent_has_topic tp on tp.content_id = tt.content_id
						WHERE 1=1 ".$where."
						GROUP BY tc.content_id
					) as temp ";

		$result = $this->CI->db->query($sql)->row_array();
		$return = (!empty($result)) ? $result['count_row'] : 0;

		return $return;
	}

	public function getHeadline($limit, $type, $start = 0)
	{
		$this->CI->load->library('core/bijak');
		$member = $this->CI->session->userdata('member');
		$member_account_id = (isset($member['account_id'])) ? $member['account_id'] : NULL;

		$sql = "SELECT tc.content_id, tc.title, tc.description, tc.content_type, tc.entry_date,
						tt.content_image_url, tt.content_url, tt.content_date, tt.content_text, tt.news_id,
						tc.count_comment, tc.count_like, tc.count_dislike,
						get_count_like_content_by_account('".$member_account_id."', tc.content_id) as is_like,
						get_count_dislike_content_by_account('".$member_account_id."', tc.content_id) as is_dislike
				  FROM tcontent tc
				  JOIN tcontent_text tt on tt.content_id = tc.content_id
				  WHERE tc.content_group_type = 12 and tt.type_news = ". $type ."
				  ORDER BY tt.type_news_date desc LIMIT ". $start . ", " . $limit;
		$result = $this->CI->db->query($sql)->result_array();
		if (!empty($result)) {
			foreach($result as $key => $row) {
				$result[$key]['politisi'] = $this->CI->bijak->getPolitisiTerkait($row['content_id'], 'cid', '5', 'headline');
			}
		}
		return $result;

	}


	public function newshomepage($cat, $limit, $offset, $slug='', $pageID="")
	{
		$member = $this->CI->session->userdata('member');
		$member_account_id = (isset($member['account_id'])) ? $member['account_id'] : NULL;

		$whe = " WHERE 1=1 "; //
		if( ! empty($cat)) {
			$whe .= " AND tp.topic_id = '".$cat."'";
		}

		if( ! empty($slug)){
			$whe .= " AND  tt.content_url != '" . $slug . "'";
		}

		// TO HANDLE PAGE ID NYA POLITISI OR SIAPALAH
		if( ! empty($pageID) ) {
			$whe .= " AND (tc.page_id = '" . $pageID . "' or tc.content_id in (select content_id from tcontent_has_politic where page_id = '" . $pageID . "')) ";
		}

		$sql = "SELECT tp.topic_id, tc.title, tt.*,tc.entry_date,
						tc.count_comment, tc.count_like, tc.count_dislike,
						get_count_like_content_by_account('".$member_account_id."', tt.content_id) as is_like,
						get_count_dislike_content_by_account('".$member_account_id."', tt.content_id) as is_dislike
				FROM tcontent_text tt
				JOIN tcontent tc ON tc.content_id = tt.content_id
				JOIN tcontent_has_topic tp on tp.content_id = tt.content_id
				".$whe."
				GROUP BY tc.content_id
				ORDER BY tt.content_date DESC limit ".$limit." offset ".$offset;

		$query = $this->CI->db->query($sql)->result_array();
		return $query;
	}


	public function getNews($newsID) {
		$member 					= $this->CI->session->userdata('member');
		$member_account_id 	= (isset($member['account_id'])) ? $member['account_id'] : NULL;

		$sql = "SELECT tt.news_id as 'id', tt.content_id, tc.entry_date, tc.title, tt.content_url as 'slug' ,tt.content_image_url, tt.type_news, tht.topic_id,
						ifnull(tc.count_comment,0) count_comment, 
						ifnull(tc.count_like,0) count_like, 
						ifnull(tc.count_dislike,0) count_dislike,
						get_count_like_content_by_account('".$member_account_id."', tt.content_id) as is_like,
						get_count_dislike_content_by_account('".$member_account_id."', tt.content_id) as is_dislike
					FROM tcontent_text tt 
					LEFT JOIN tcontent tc on tc.content_id = tt.content_id 
					LEFT JOIN tcontent_has_topic tht ON (tht.content_id = tt.content_id) 
					WHERE news_id = '".$newsID."'";
		$rs = $this->CI->db->query($sql)->result_array();
		foreach($rs as $key => $post)
		{
			$contentID = $post['content_id'];            
            $politisi = $this->getContentHasPolitisi($contentID);        
            $rs[0]['news_profile'] = $politisi->result_array();
        }                        
		if(count($rs) > 0) {
			return $rs[0];
		} else {
			return false;
		}
	}

 	public function getBeritaList($category, $page=0, $limit=10)
	{
	    //$limit   = (!$limit) ? $this->limit : $limit;
		$offset  = $page * $limit;
        
		$where = " WHERE 1 "; //
		if( !empty($category)) $where .= " AND tht.topic_id = '".$category."'";

		$sql = "SELECT tt.content_id, tt.news_id, tt.type_news, tt.type_news_date, t.entry_date, tht.topic_id
                     FROM tcontent_text tt
                      JOIN tcontent t ON t.`content_id` = tt.`content_id`
                      JOIN tcontent_has_topic tht ON tht.`content_id` = t.`content_id`
                      ".$where." and tt.news_id IS NOT NULL AND tt.news_id != ' ' AND tt.news_id != 0
                      ORDER BY t.`entry_date` DESC LIMIT ".$offset.", ".$limit;
                                            
		$result = $this->CI->db->query($sql)->result_array();

        return $result;
	}

	public function getContentHasPolitisi($content_id)
	{
         $sql = '
            SELECT
            	thp.*, ifnull(p.page_name, thp.page_id) AS page_name,
            	(
            		CASE
            		WHEN ifnull(p.page_name, thp.page_id) = thp.page_id THEN
            		"true"
            ELSE
            		"false"
            		END
            	)AS equal_page_id_name,
            	tca.attachment_title
            FROM
            	tcontent_has_politic thp left
            	JOIN tobject_page p ON thp.page_id = p.page_id LEFT
            	JOIN tcontent_attachment tca ON(
            		tca.content_id = p.profile_content_id
            	)
            WHERE
            	thp.content_id = ?';

		$query = $this->CI->db->query($sql, $content_id);
		return $query;

	}

	public function getContentIDbyNewsID($newsID) {
		$sql = "SELECT tt.content_id, tt.content_date, content_url, tht.topic_id FROM tcontent_text tt LEFT JOIN tcontent_has_topic tht ON (tht.content_id = tt.content_id) WHERE news_id = ?";
		$rs = $this->CI->db->query($sql,array($newsID))->result_array();
		if(count($rs) > 0) {
			return $rs[0];
		} else {
			return false;
		}
	}


	public function getNewsShortInfo($content_id) {
		$sql = "SELECT * FROM tcontent WHERE content_id = ?";
		$rs = $this->CI->db->query($sql, array($content_id))->result_array();
		if(count($rs) > 0) {
			return $rs[0];
		} else {
			return false;
		}
	}

  public function getNewsTerkait($content_id)
  {
    $sql = "SELECT * FROM tcontent_terkait WHERE content_id = '".$content_id."' ";
    $query = $this->CI->db->query($sql);
    return $query->result_array();
  }

}

/* End of file file.php */