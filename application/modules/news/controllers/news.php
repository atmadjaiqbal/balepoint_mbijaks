<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends Application {

	var $arrNews = array(
        1 => "hukum",
        3 => "kesenjangan",
        7 => "parlemen",
        9 => "nasional",
        4 => "ekonomi",
        10 => "teknologi",
        12 => "hankam",
        17 => "daerah",
        18 => "reses",
        19 => "internasional",
//				8 => "hot-issues"
        8 => "headline",
        1150 => 'gayahidup',
        1151 => 'komunitas',
        1152 => 'lingkungan',
        347 => 'sara',
        1221 => 'indobarat',
        1222 => 'indoteng',
        1223 => 'indotim'
);
    var $member = array();
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $data = $this->data;
        $this->load->library('news_lib');
        $this->load->library('politik/politik_lib');
        $this->load->library('scandal/scandal_lib');
        $this->load->module('timeline');
        $this->load->module('scandal');
        $this->load->module('profile');
        $this->load->module('survey');
        $this->load->module('headline');


        $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
        $this->redis_slave = $this->redis_slave->connect();
        $this->member = $this->session->userdata('member');

        $this->member = $this->session->userdata('member');

    }

   
    public function category($category='') {

		if( ! empty($category) ) {
			$this->index($category);
		}
	}

    function index($category="")
    {
    	
    	$topic_id = array_search($category, $this->arrNews);

    	$data['category'] = (empty($category)) ? 'terkini' : $category;

    	$id = 0;
    	$ct = 'index';
		if(!empty($category)){
			$id = $topic_id;
			$ct = 'category/' . $category;
		}

    	$limit = 40;
		$page = 1;
		$per_page = $this->input->get('per_page');
		if(!is_bool($per_page)){
			if(!empty($per_page)){
				$page = $per_page;	
			}
		}

		$offset = ($limit*$page) - $limit;
		// echo $offset;

		$qr = $_SERVER['QUERY_STRING'];

		
		/** 
			TRY USE REDIS
		*/
//        $last_key = $this->redis_slave->lRange('news:list:rkey', 0, 0);
        $last_key = $this->redis_slave->get('news:key');
        $rkey = 'news:topic:list:'.$last_key.':'.$id;

		$user_count = $this->redis_slave->lLen($rkey);
        
        // kalo udah 1000 tergantung dari news list categories dari redis

       if($offset > $user_count)
       {
           $limit = $limit + 1;
           $result = $this->getNewsMysql($id, $limit, $offset, $result=1);           
        } else {
	       $result = $this->getNewsCache($id, $limit, $offset, $result=1);
        }

		$this->load->library('pagination');

		$config['base_url'] = base_url().'news/' . $ct . '?'; //.$qr;
		$config['total_rows'] = $user_count;
		$config['per_page'] = $limit; 
		$config['use_page_numbers'] = TRUE;
		$config['page_query_string'] = TRUE;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2; // round($choice);
		$config['full_tag_open'] = '<ul class="pagination pagination-sm">';
		$config['full_tag_close'] = '</ul>';
		$config['cur_tag_open'] = '<li class="disabled"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_link'] = '...';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '...';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$this->pagination->initialize($config); 

		$data['pagi'] =  $this->pagination->create_links();
		$data['news'] = $result;
		$data['offset'] = $offset;
		$data['total']	= $user_count;

        $data['head_title'] = 'News';
		
		$html['html']['content'] = $this->load->view('mobile/news_index', $data, true);
        $this->load->view('m_tpl/layout', $html);
    }

    function article($ID="0-0", $slug="")
    {
    	  $data['title'] = "Bijaks | Life & Politics";
		    $data['scripts'] = array('bijaks.js', 'bijaks.spam.js');

    	  $IDs = explode("-", $ID);
    	  $id = intval($IDs[0]);
		    $news_id = intval(@$IDs[1]);

		    $data['category'] = ($id === 0) ? 'terkini' : $this->arrNews[$id];
		    $data['topic_last_activity'] = $id; // $this->timeline->topic($id);
		    $data['news'] = $this->getNewsCache($news_id, $limit=0, $offset=0, $result=2);     		

        $terkait = array(); $_t = 0; $flag_terkait = true;
        if(isset($data['news']['news_terkait']))
        {
            foreach($data['news']['news_terkait'] as $rk)
            {
                $_terkait = $rk['terkait_news_id'];
                $terkait[$_t] = $this->getNewsCache($_terkait, $limit=0, $offset=0, $result=2);
                if($terkait[$_t] == '' || empty($terkait[$_t])) {
                    $terkait[$_t] = $this->getNewsMysql($_terkait, $limit=0, $offset=0, $result=2);
                }
                $_t++;
            }
        } else {
            $terkait = $this->news_lib->getNewsTerkait($data['news']['content_id']);
            if($terkait) {
                foreach($terkait as $rk) {
                    $_terkait = $rk['terkait_news_id'];
                    $terkait[$_t] = $this->getNewsCache($_terkait, $limit=0, $offset=0, $result=2);
                    if($terkait[$_t] == '' || empty($terkait[$_t])) {
                        $terkait[$_t] = $this->getNewsMysql($_terkait, $limit=0, $offset=0, $result=2);
                    }
                    $_t++;
                }
            }
        }

        if($data['news'] == '')
        {
            $data['news'] = $this->getNewsMysql($news_id, $limit=0, $offset=0, $result=2);
            $terkait = $this->news_lib->getNewsTerkait($data['news']['content_id']);
            if($terkait) {
                foreach($terkait as $rk) {
                    $_terkait = $rk['terkait_news_id'];
                    $terkait[$_t] = $this->getNewsCache($_terkait, $limit=0, $offset=0, $result=2);
                    if($terkait[$_t] == '' || empty($terkait[$_t])) {
                        $terkait[$_t] = $this->getNewsMysql($_terkait, $limit=0, $offset=0, $result=2);
                    }
                    $_t++;
                }
            }
        }
        
        $sort_terkait = array();
        foreach ($terkait as $row) { $sort_terkait[$row['date']] = $row; }
        krsort($sort_terkait);
        $data['news_terkait'] = $sort_terkait;

        /** NEWS **/
        $data['issue']       = $this->headline_list(1, 10);  # TAJUK UTAMA 
        $data['nasional'] = $this->sesi('nasional', 9, 'true', 0);
        $data['hukum'] = $this->sesi('hukum', 9, 'true', 0);
        $data['ekonomi'] = $this->sesi('ekonomi', 9, 'true', 0);
        $data['parlemen'] = $this->sesi('parlemen', 9, 'true', 0);
        $data['internasional'] = $this->sesi('internasional', 9, 'true', 0);
        $data['indobarat'] = $this->sesi('indobarat', 9, 'true', 0);
        $data['indoteng'] = $this->sesi('indoteng', 9, 'true', 0);
        $data['indotim'] = $this->sesi('indotim', 9, 'true', 0);

        $data['lingkungan'] = $this->sesi('lingkungan', 4, 'true', 0);
        $data['kesra'] = $this->sesi('kesenjangan', 4, 'true', 0);
        $data['sara'] = $this->sesi('sara', 4, 'true', 0);
        $data['komunitas'] = $this->sesi('komunitas', 4, 'true', 0);
        $data['gayahidup'] = $this->sesi('gayahidup', 4, 'true', 0);
        $data['reses'] = $this->sesi('reses', 4, 'true', 0);

        //$data['daerah'] = $this->news->sesi('daerah', 4, 'true', 0);
        //$data['hankam'] = $this->news->sesi('hankam', 4, 'true', 0);
        /** END NEWS **/

        // Managing banners
        $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
        $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
        $webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
        # 1 = ios
        # 2 = android
        if( $iPod || $iPhone ){
            $type = "ios";
        }elseif($iPad){
            $type = "ios";
        }elseif($Android){
            $type = "a";
        }elseif($webOS){
            $type = "a";
        }else{
            $type = 'a';
        }        


        $ads_banner = [];
        $adbanners = $this->redis_slave->get("list:banners");
        $temp = '';
        if(!empty($adbanners)){
            $adbanners = json_decode($adbanners,true);
            foreach($adbanners as $adb){
                if($adb['is_image_shown'] == 1){
                    $ilink = base_url('home/ad?id='.$adb['id'].'-'.$type);
                    $temp .= '<a href="'.$ilink.'" target="_blank">';
                    if(substr($adb['image_url'], 0,7) != 'http://'){
                        $urlArray = explode('/', $adb['image_url']);
                        $tempNb = count($urlArray);
                        $fname = 'small_'.$urlArray[$tempNb-1];
                        $imageUrl = '';
                        for($i=0;$i<=$tempNb-2;$i++){
                            $imageUrl .= $urlArray[$i].'/';
                        }
                        $imageUrl .= $fname;
                        $temp .= '<img src="'.base_url($imageUrl).'" style="width: 100%;height: auto;"/></a>';
                    }else{
                        $urlArray = explode('/', $adb['image_url']);
                        $tempNb = count($urlArray);
                        $fname = 'small_'.$urlArray[$tempNb-1];
                        $imageUrl = '';
                        for($i=0;$i<=$tempNb-2;$i++){
                            $imageUrl .= $urlArray[$i].'/';
                        }
                        $imageUrl .= $fname;
                        $temp .= '<img src="'.$imageUrl.'" style="width: 100%;height: auto;"/></a>';
                    }
                }

                if($adb['is_text_shown'] == 1){
                    $tlink = base_url('home/ad?id='.$adb['id'].'-'.$type);
                    $temp .= '<a href="'.$tlink.'" target="_blank">';
                    $temp .= '<div class="text_adds">'.$adb['text_shown'];
                    $temp .= '<span style="margin-left: 40px;color:red;font-size: 14px;font-weight: bold;font-family : Courier New, Courier, monospace;">';
                    $temp .= '(download here)</span></div></a><br>';
                }
                $ads_banner[] = $temp;
                shuffle($ads_banner);
                $temp = '';
            }
        }
        $data['ads_banner'] = $ads_banner;
        // End Managing banners

        $_id_catnews = isset($data['news']['categories'][0]['id']) ? $data['news']['categories'][0]['id'] : 0;
        $data['og_slug'] = "http://m.bijaks.net/news/article/".$_id_catnews.'-'.$data['news']['id'].'/'.$data['news']['slug'];
		$data['news_list']  = $this->getNewsCache($id, $limit=19, $offset=0, 1, $news_id);
        $data['head_title'] = $data['news']['title'];

		$html['html']['content'] = $this->load->view('mobile/news_detail', $data, true);
        $this->load->view('m_tpl/layout', $html);

    }

    private function getNewsMysql($id=0, $limit=10, $offset=0, $result=1, $key_exist=0)
    {
        if($result === 1)
        {
             $news = $this->news_lib->getBeritaList($id, $offset, $limit);            
			 $result = array();
             $i = 0;
             if(count($news) > 0)
             {
                 foreach($news as $key => $value)
                 {                     
                       $result[$i] = $this->news_lib->getNews($value['news_id']);
                       $result[$i]['topic_id'] = $result[$i]['topic_id'];
                       $result[$i]['image_thumbnail'] = 'http://news.bijaks.net/uploads/'.$result[$i]['content_image_url'];
                       $i++;
                 }
            }
            return $result;
        } else {
            $news_array = $this->news_lib->getNews($id);
            $news_array['topic_id'] = $news_array['topic_id'];
            $news_array['image_thumbnail'] = 'http://news.bijaks.net/uploads/'.$news_array['content_image_url'];
            
            $news_url = 'http://news.bijaks.net/mobile/get_post/?dev=1&id='.$id;
			$ch = curl_init($news_url);				
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $body  = substr($response, $header_size);
            $json_output = json_decode($body);
            $conten  = $json_output->post->content;

            $conten = strip_tags($conten, '<p>'); 
            $conten = preg_replace('/(<p class="wp-caption-text">.+?)+(<\/p>)/i', "", $conten); 
		    $attachments 	= $json_output->post->attachments;
		    $image_url		= '';
		    $image_title 	= '';
		    $image_caption = '';
		    $image_height	= '';
		    if (!empty($attachments)) {
			    $image_url		= $attachments[0]->url;
			    $image_title 	= $attachments[0]->title;
			     $image_caption = $attachments[0]->caption;
			     $image_height 	= $attachments[0]->images->full->height;
		     }
		
            $news_array = array_merge($news_array, 
		     array(
                'date' => $news_array['entry_date'],
			    'message'			=> 'ok',
			    'content' 			=> $conten, 
			    'image_large'			=> $image_url,
			    'image_title' 		=> $image_title,
			    'caption' 	=> $image_caption,
			    'image_height' 	=> $image_height,
			    'category' 			=> $json_output->post->categories[0]->title
		     ));

			return $news_array;
       }
    }

    private function getNewsCache($id=0, $limit=9, $offset=0, $result=1, $key_exist=0)
    {
    	if($result === 1){
//            $last_key = $this->redis_slave->lRange('news:list:rkey', 0, 0);

            if($id == 8)
            {
                $rkey = "news:list:headline";
            } else {
                $last_key = $this->redis_slave->get('news:key');
                $rkey = 'news:topic:list:'.$last_key.':'.$id;
            }

			$rrange = $offset + $limit;

			$news = $this->redis_slave->lRange($rkey, $offset, $rrange);

            if($key_exist != 0)
                if(@in_array($key_exist, $news) ){
                    $news = $this->redis_slave->lRange($rkey, $offset, intval($rrange) + 1);
                }

			$result = array();
			$i = 0;
            if(count($news) > 0){
                foreach ($news as $key => $value) {
                    // echo $key;
                    if($key_exist == intval($value))
                        continue;
//                    if($key == $limit)break;
    //				$news_array = array();
                    $news_detail = $this->redis_slave->get('news:detail:'.$value);
//                    echo $key. ' => ' . $news_detail . '<br>';
                    if($news_detail != 'None'){
                        $news_array = @json_decode($news_detail, true);
                        $result[$i] = $news_array;
                        $result[$i]['topic_id'] = $news_array['categories'][0]['id'];

                        $i++;
                   } else {
                        $news = $this->news_lib->getBeritaList($id, $offset, $limit);
                        if(count($news) > 0)
                        {
                            $i = 0;
                            foreach($news as $key => $value)
                            {
                                $result[$i] = $this->news_lib->getNews($value['news_id']);
                                $result[$i]['topic_id'] = $result[$i]['topic_id'];
                                $result[$i]['image_thumbnail'] = 'http://news.bijaks.net/uploads/'.$result[$i]['content_image_url'];
                                $i++;
                            }
                        }
                   }
                }
            }
			return $result;
		}else{
			// $result = array();
			$news_detail = $this->redis_slave->get('news:detail:'.$id);
//            $news_detail = $this->redis_slave->get('news:'.$id);
			$news_array = @json_decode($news_detail, true);
			// var_dump($news_array);
			return $news_array;

		}
    }

    function sesi($category='0', $limit=0, $return='false', $img_show = 5 ){
    	$data['scripts'] = array('bijaks.js');

        $data['img_show'] = intval($img_show);
    	// print $category;
    	$topic_id = (array_search($category, $this->arrNews)) ? array_search($category, $this->arrNews) : 0;
    	$data['category'] = 'terkini';
    	$where = array();
    	if($category != '0'){
			$where = array('tht.topic_id' => $topic_id);
			$data['category'] = $category;
		}
		
		$result = $this->getNewsCache($topic_id, $limit, 0, 1);
		$data['result'] = $result;
		
		$tpl = $this->load->view('mobile/news_list', $data, True);
		return $tpl;
    }

    public function headline_list($page=1, $img_show = 5)
    {
        $limit = 9;

           $rkey = "news:list:headline";
        $offset = (($limit*$page) - $limit);
        if($page == 1){
            $offset = (($limit*$page) - $limit);
        }

        $rrange = $offset + $limit;
        $resultNews = $this->redis_slave->lrange($rkey, $offset, $rrange);
        // echo '<pre>';
        // var_dump($resultNews);
        // echo '</pre>';
        $result = array();

        $i = 0;
        foreach($resultNews as $key => $value)
        {
            $rowNews = $this->redis_slave->get('news:detail:'.$value);
            $news_array = json_decode($rowNews, true);
            $result[$i] = $news_array;
            $result[$i]['topic_id'] = $news_array['categories'][0]['id'];
            $i++;
        }
        $data['val'] = $result;

        $data['img_show'] = $img_show;

        return $this->load->view('mobile/news_headline_list', $data, true);

//        echo '<pre>';
//        var_dump($result);
//        echo '</pre>';
    }
}