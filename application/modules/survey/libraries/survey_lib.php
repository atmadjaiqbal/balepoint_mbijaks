<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Survey_Lib {

	protected  $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->database('slave');
		$this->db_master  = $this->CI->load->database('master', true);
	}

    public function getSurveyCount()
    {
        $where = array('publish' => '1');
        $this->CI->db->where($where);
        return $this->CI->db->count_all_results('survey');
    }

	public function getSurvey($tipe = '', $offset=0, $limit=1, $where='')
	{
	    $member           = $this->CI->session->userdata('member');
	    $member_account_id  = (isset($member['account_id'])) ? $member['account_id'] : NULL;

	   $where = (!empty($where)) ? " AND $where " : "";
      if ($tipe == 'hotlist') {
      	/*
         $where .= " AND hotlist = '1'
                    AND survey_id in ( select q.survey_id
                           from question q
                           inner join question_answer qa on  q.question_id = qa.question_id  group by q.question_id
                           having count(qa.account_id) > 0 )";
			*/
			$where .= " AND hotlist = '1' ";

      }

      $sql = "SELECT s.*, tc.content_id, tc.count_comment, tc.count_like, tc.count_dislike,
                    get_count_like_content_by_account('".$member_account_id."', tc.content_id) as is_like,
                    get_count_dislike_content_by_account('".$member_account_id."', tc.content_id) as is_dislike
               FROM survey s
               INNER JOIN tcontent tc ON s.survey_id = tc.location AND tc.content_group_type = '54'
               WHERE publish = '1'  AND DATE(expired_date) >=  CURDATE() ".$where."  ORDER BY  created_date  DESC LIMIT ".$offset.",".$limit;
      $query = $this->db_master->query($sql);
      $data = $query->result_array();
      
		return $data;
	}

	public function getQuestion($where)
	{
		$this->CI->db->where($where);
		$this->CI->db->order_by('updated_time', 'desc');
		$data = $this->CI->db->get('question')->result_array();
		return $data;
	}

	public function getOption($where)
	{
		$this->CI->db->where($where);
		$data = $this->CI->db->get('question_option')->result_array();
		return $data;
	}


    public function addAnswer($data, $surveyID)
    {
    	$this->CI->db->delete('question_answer',  array(
    		'question_id' => $data['question_id'],
    		'account_id' => $data['account_id']
    	));

		$this->CI->db->insert('question_answer', $data);
		$id = $this->CI->db->insert_id();

		$sql = "update question_option qo
					inner join (
						select  qa.question_option_id, count(qa.answer_id) count
						from question_answer qa
						where question_option_id = '".$data['question_option_id']."'
						group by qa.question_option_id
					) x on qo.question_option_id = x.question_option_id

					set qo.votes = x.count
					where qo.question_option_id = '".$data['question_option_id']."'";
		$this->CI->db->query($sql);

		/*
		$sql = "update question set updated_time = '".$data['answer_date'] ."' where question_id = '".$data['question_id']."'";
		$this->CI->db->query($sql);

		$sql = "update survey set updated_date = '".$data['answer_date'] ."' where survey_id = '".$surveyID."'";
		$this->CI->db->query($sql);
		*/

		return $id;
	}

	public function getAnswer($where)
	{
		$this->CI->db->where($where);
		$data = $this->CI->db->get('question_answer')->result_array();
		return $data;
	}

	public function getAnswerAccount($questions=null,$accountID=null)
	{
		$data = array();
		if(!empty($questions))
		{
		   foreach ($questions  as $row) {
		     $where = array('question_id' => $row['question_id'], 'account_id' => $accountID);
			   $this->CI->db->where($where);
			   $answer = $this->CI->db->get('question_answer')->row_array();

			   $qa['question_id'] = $row['question_id'];
			   $qa['is_answer']   = (!empty($answer)) ? '1' : '0';
			   $qa['answer'] 		 = (!empty($answer)) ? $answer['answer'] : '';
			   $qa['answer_date'] = (!empty($answer)) ? $answer['answer_date'] : '';
			   $data[] = $qa;
		   }
		}   
		return $data;
	}


   public function getSurveyList($type='',$page=0, $limit=10, $where='')
   {
      $offset           = $page * $limit;
      $where            = (!empty($where)) ? $where : '';
      $limit_question   = 1;

		$survey_count  = $this->getSurvey();
		$surveys       = $this->getSurvey($type, $offset, $limit, $where) ;
				
		$result        = array();
		$i=0;

		if (!empty($surveys)) {
		   //$cm  = $this->CI->load->module('ajax');
   		foreach ($surveys as $key_arr => $key) {
   			$where      = array('survey_id' => $key['survey_id']);

   			$question   = $this->getQuestion($where);
            if (!empty($question)) {
      			$res        = array();
      			$n          = 0;

      			foreach ($question  as $key_q => $row) {
      			   //if ($n >= $limit_question) break;

      				$wh      = array('question_id' => $row['question_id']);
      				$option = $this->getOption($wh);
      				if (!empty($option)) {
         				$row['option'] = $option;
         				$res[$n]       = $row;
         				$n++;
         			}
         			if ($key_q == '0') {
         				$key['updated_date'] = $question[$key_q]['updated_time'];
         			}
         			$key['question']  = $res;
      			}

					$contentDate = mysql_date("%d %M %Y - %h:%i", strtotime($key['updated_date']));
      			$key['content_date']   = $contentDate;
      			//$key['content_count']   = $cm->count_content($key['content_id'], '2', '2', NULL, $contentDate);
      			$key['content_count']   = '';

      			$result[$i]       = $key;


      		}
   			$i++;
   		}
      }

		return $result;
   }

}

/* End of file file.php */