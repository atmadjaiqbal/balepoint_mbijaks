<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Survey extends Application
{
   var $member;

   function __construct()
   {
      parent::__construct();

      $this->load->helper('date');
      $this->load->helper('m_date');

      $this->load->helper('text');
      $this->load->helper('seourl');

       $this->load->module('timeline/timeline');
       $this->load->module('profile/profile');
       $this->load->module('scandal/scandal');
       $this->load->module('news/news');
       $this->load->module('headline');
       $this->load->library('redis', array('connection_group' => 'slave'), 'redis_slave');
       $this->redis_slave = $this->redis_slave->connect();
      $this->load->library('survey/survey_lib');

       $this->member = $this->session->userdata('member');
//      $this->checkMember();
//      $this->checkUID();
   }


    function index($URL='')
    {
        if(!empty($URL))
        {
            $data['category'] = 'survey';
            $arrUrl     = explode("-", $URL);
            $surveyID   = intval($arrUrl[0]);

            $survey_list = $this->getsurveycache($surveyID, $limit=0, $offset=0, $result=0);
            $data['survey'] = $survey_list;

            $data['survey_more'] = $this->terkait($surveyID);

            $data['head_title'] = 'Pooling Komunitas ' . $survey_list['name'];

            $html['html']['content'] = $this->load->view('mobile/survey_detail', $data, true);
            $this->load->view('m_tpl/layout', $html);

        }else{
            $data['head_title'] = 'Pooling Komunitas';

            $data['category'] = 'survey';

            $limit = 29;
            $page = 1;
            $per_page = $this->input->get('per_page');
            if(!is_bool($per_page)){
                if(!empty($per_page)){
                    $page = $per_page;
                }
            }

            $offset = ($limit*$page) - $limit;
            // echo $offset;

            $qr = $_SERVER['QUERY_STRING'];

            $rkey = 'survey:list:all';
            $user_count = $this->redis_slave->lLen($rkey);

            $survey_list = $this->getsurveycache($id=0, $limit, $offset, $result=1);

            $this->load->library('pagination');

            $config['base_url'] = base_url().'survey/index/?'; //.$qr;
            $config['total_rows'] = $user_count;
            $config['per_page'] = $limit;
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = 2; // round($choice);
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['cur_tag_open'] = '<li class="disabled"><a>';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['next_link'] = '...';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = '...';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['first_link'] = FALSE;
            $config['last_link'] = FALSE;

            $this->pagination->initialize($config);

            $data['pagi'] =  $this->pagination->create_links();
            $data['survey'] = $survey_list;
            $data['offset'] = $offset;
            $data['total']	= $user_count;

//             $data['topic_last_activity'] = 'survey'; // $this->timeline->sub_header('survey');

//             $data['hot_profile'] = $this->profile->hot('true');

//             $data['terkini'] = $this->news->sesi('0', 0, 'true');

// //            $data['issue'] = $this->news->sesi('hot-issues', 0, 'true');
//             $data['surveymini'] = $this->mini_survey();
//             $data['skandal'] = $this->scandal->sesi('true');

//             $data['reses']    = $this->headline->reses_list();
//             $data['headline'] = $this->headline->headline_list(); //$this->news(8, 'hot-issues');

//             $data['ekonomi'] = $this->news->sesi('ekonomi', 9, 'true');
//             $data['hukum'] = $this->news->sesi('hukum', 9, 'true');
//             $data['parlemen'] = $this->news->sesi('parlemen', 9, 'true');

//             $data['nasional'] = $this->news->sesi('nasional', 9, 'true');
//             $data['daerah'] = $this->news->sesi('daerah', 9, 'true');
//             $data['internasional'] = $this->news->sesi('internasional', 9, 'true');

            $html['html']['content'] = $this->load->view('mobile/survey_index', $data, true);
            $this->load->view('m_tpl/layout', $html);
        }
    }

    public function terkait($id=0)
    {
        $limit = 10;
        $page = 1;

        $offset = ($limit*$page) - $limit;

        $survey_list = $this->getsurveycache(0, $limit, $offset, $result=1, $id);
        foreach($survey_list as $idk=>$survey){
            $survey['last'] = $this->timeline->last('content', $survey['content_id'], $result='true', $category='survey');
            $survey_list[$idk] = $survey;
        }
        $data['survey'] = $survey_list;
        return $this->load->view('mobile/survey_list', $data, true);

    }

    private function getsurveycache($id=0, $limit=9, $offset=0, $result=1, $key_exist=0)
    {
        if($result === 1){

            $rkey = 'survey:list:all';

            $rrange = $offset + $limit;

            $co = $this->redis_slave->lLen($rkey);

            $suksesi_list = $this->redis_slave->lRange($rkey, $offset, $rrange);
            if(intval($key_exist) != 0 ){
                $suksesi_list = $this->redis_slave->lRange($rkey, $offset, intval($co) - 1);

                if(in_array($key_exist, $suksesi_list)){
                    $idk = array_search($key_exist, $suksesi_list);
                    unset($suksesi_list[$idk]);

                }
                shuffle($suksesi_list);

            }

            $result = array();
            $i = 0;
            if(count($suksesi_list) > 0){
                foreach ($suksesi_list as $key => $value) {

                    if($key_exist == intval($value))
                        continue;

                    if ($key == $limit) break;

                    $suksesi_detail = $this->redis_slave->get('survey:detail:'.$value);
                    $suksesi_array = @json_decode($suksesi_detail, true);
                    $result[$i] = $suksesi_array;

                    $i++;
                }
            }
            return $result;
        }else{
            $suksesi_detail = $this->redis_slave->get('survey:detail:'.$id);
            $suksesi_array = @json_decode($suksesi_detail, true);
            return $suksesi_array;

        }
    }


   private function getSurveyList($page='0', $limit='10',$where=array())
   {
      $count      = $this->survey_lib->getSurveyCount();
      $result     = $this->survey_lib->getSurveyList('', $page, $limit, $where);
      $cm         = $this->load->module('ajax');
      $i = 0;

      foreach ($result as $key => $row) {
         $contentDate = mdate("%d %M %Y - %h:%i", strtotime($row['updated_date']));

         $result[$key]['content_date']    = $contentDate;
         $result[$key]['content_count']   =  $cm->count_content($row['content_id'], '1', '2', NULL);
         $i++;
      }

      $data['total_rows']  = $count;
      $data['list']        = $result;

      return $data;
   }

   public function load_more($page='1', $limit='10')
   {
      $offset           = $page*$limit;
      $data['survey']   = $this->getSurveyList($offset,$limit);
      $data['offset']   = $offset;
      $totalPage        = ceil($data['survey']['total_rows']/$limit);
      $nextPage         = (int)$page+1;
      $load_more        = ($page < $totalPage) ? '1': '0';

      $html = $this->load->view('survey/survey_list', $data, true);

      // HTML Response, Flag Load More, Next Page
      echo $html .'||'. $load_more .'||'. $nextPage;
   }

    public function mini_survey()
    {
        $arrSurvey = array();
        $data['category'] = 'survey';
        $surveyKey = 'survey:list:all';
        $surveyList = $this->redis_slave->lrange($surveyKey, 0, 0);

        if(count($surveyList))
        {
            foreach($surveyList as $key => $value)
            {
                $rowSurvey = $this->redis_slave->get('survey:detail:'.$value);
                $arrSurvey = @json_decode($rowSurvey, true);
                $rsSurvey[$key] = $arrSurvey;
            }
        }

        $data['surveymini'] = $rsSurvey;
        return $this->load->view('template/survey/tpl_survey_mini', $data, true);
    }

   private function checkMember() {
      $this->load->model('member', 'member_model');
      if($this->member) {
         $this->member['current_city'] = $this->member_model->getUserCurrentCity($this->member['account_id']);
      } else {
         redirect($this->data['base_url'].'home/login/?redirect='.$this->current_url);
      }
   }

   private function checkUID() {
      $uID = $this->input->post('uID');
      if(isset($_GET['uid']) && ! empty($_GET['uid'])) {
         $this->uID = $_GET['uid'];
         $this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);

      } else if(! empty($uID)) {
         $this->uID = $uID;
         $this->isLogin = $this->isLogin($this->uID, $this->member['account_id']);

      } else {
         $this->uID  = $this->member['account_id'];
         $this->isLogin = true;
      }
   }

   private function isLogin($str1, $str2) {
      if(! strcmp($str1, $str2)) {
         return true;
      } else {
         return false;
      }
   }



}