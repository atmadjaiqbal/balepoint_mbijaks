<?php
if(! isset($width)) { $width = 300; }
if(! isset($height)) { $height = 130; }

if (isset($show_title)) {
    if (!$show_title) $title_chart='';
}

?>
<script type="text/javascript">
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container<?php echo $question_id; ?>',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                backgroundColor:'rgba(255, 255, 255, 0)'
            },
            title: {
                text: "<?php echo trim($title_chart); ?>",
                style: {
                    color: '#a00',
                    fontSize: '11px',
                    fontFamily: 'Arvo, serif'

                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage}%</b>',
                percentageDecimals: 1
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                            point: {
                        events: {
                            legendItemClick: function(dt) {
                                console.debug(dt);

/**                               console.log(Settings.accountID);
                               if(!(Settings.accountID)) { window.location = Settings.base_url + 'home/login'; }
                                series_data = <?php //echo json_encode($series_data);?>;
                                    option_voted = this.options.id
                                    console.log(option_voted);
                                            $.ajax({
                                                 url: '<?php //echo $url;?>',
                                                 type: "post",
                                                 data: {question_id: '<?php //echo $question_id;?>', survey_id: '<?php //echo $survey_id;?>', question_option_id : this.options.id,  answer:this.options.name},
                                                 cache: false,
                                                 success: function(response, textStatus, jqXHR){
                                                    if (response =='ok') {
                                                        alert('Terima Kasih telah memilih.');
                                                        chart_data = [];
                                                        $.each( series_data, function( key, value ) {
                                                            legend = value.name;
                                                            voted = parseFloat(value.voted);
                                                            if (value.id == option_voted) {
                                                                voted++;
                                                                legend = value.name + ' (voted)';
                                                            }
                                                            chart_data.push({ name: legend, y: voted, id:value.id, value:voted  });
                                                        });
                                                        chart.series[0].setData(chart_data);
                                                    }
                                                 },
                                                 error: function(jqXHR, textStatus, errorThrown){
                                                    console.log("The following error occured: "+textStatus, errorThrown);
                                                 },
                                                 complete: function(){}
                                            });
                                            return false;
 */
                            }
                        }
                        },
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: <?php echo (isset($show_legend)) ? $show_legend: 'true';?>
                }
            },
            series: [{
                type: '<?php echo (isset($chart_type)) ? $chart_type: 'pie';?>',
                name: '<?php echo trim($title_chart); ?>',
                data: [
               <?php
                    if (!empty($legends)) {
                        $total = 0;
                        foreach ($values as $value) {
                            $total += (int)$value;
                        }

                        foreach ($legends as $key => $legend) {
                            if($key > 0) { echo ","; }

                            $persen             = ($values[$key] > 0) ? round($values[$key] / $total, 2) * 100 : 0;
                            $optionID       = 0;
                            $optionValue    = '';
                            if (isset($option_id))      {$optionID = $option_id[$key];}
                            if (isset($option_value))   {$optionValue = $option_value[$key];}
                            //echo "{ name: '" . addslashes($legend) . "', y: " . $persen . ", id: '".$optionID."', value: '".$optionValue."'}";
                            echo "{ name: '" . addslashes($legend) . "', y: " . $values[$key] . ", id: '".$optionID."', value: '".$optionValue."'}";
                        }
                    }

                    ?>
                ]
            }]
        });
    });

});
        </script>
<?php //var_dump($values); ?>
<div id="container<?php echo $question_id; ?>" style="min-width: <?php echo $width; ?>px; height: <?php echo $height; ?>px; margin: 0 auto"></div>
