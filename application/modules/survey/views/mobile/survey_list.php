<?php foreach($survey as $key => $val) { ?>
    <div class="row">
            <?php 
                foreach($val['question'] as $key => $que) {
                    $values = $legends = array();
                    $url = base_url() .'survey/index/'. $val['survey_id'] . "-" . urltitle($val['name']);
                    $survey_title = $val['name'];
                    foreach($que['option'] as $opt) {
                        $legends[]        = ucwords($opt['name']);
                        $values[]         = (isset($opt['votes'])) ? $opt['votes'] : 0;
                        
                    }   

            ?>
            <div class="col-xs-12 col-sm-12">
                <a class="" href="<?php echo $url;?>">
                    <h5 class="media-heading"><?php echo $survey_title; ?></h5>
                </a>
                    <?php if (!empty($values)) {
                        $total = 0;
                        foreach ($values as $value) {
                            $total += (int)$value;
                        }
                        foreach ($legends as $key => $legend) {
                             $persen             = ($values[$key] > 0) ? round($values[$key] / $total, 2) * 100 : 0;
                             
                      ?>
                      <span><?php echo $legend;?> : <?php echo $persen. '%';?>, </span>
                        <?php } ?>
                    <?php } ?>
                
            </div>  
        <?php } ?>

    </div>
    <hr class="line-mini">
<?php } ?>