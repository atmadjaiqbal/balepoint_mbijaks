<?php
$url = base_url() .'survey/index/'. $survey['survey_id'] . "-" . urltitle($survey['name']);

?>
<h4 class="kanal-title kanal-title-blue">POLLING KOMUNITAS</h4>
<div class="row">
	<div class="col-xs-12">
		<h4><?php echo $survey['name']; ?> - <small><?php echo mdate('%d %M %Y %h:%i', strtotime($survey['begin_date'])); ?> s/d <?php echo mdate('%d %M %Y %h:%i', strtotime($survey['expired_date'])); ?></small></h4>
	
		<?php
            foreach($survey['question'] as $key => $val) {
            $values = $legends = $option_id = $option_value = $series_data = array(); $i = 0;
            $chartData = array();
                $_wordlen = strlen($val['question']);
                $_baris = ceil($_wordlen / 40);
                $_wordTitle = '';
                $_y = 0;
                for ($_i = 1; $_i <= $_baris; $_i++) {
                    $_wordTitle .= substr($val['question'], $_y, 41) . ' <br> ';
                    if($_i == 1)
                    {
                        $_y = 40 + $_i;
                    } else {
                        $_y = 40 + $_y;
                    }
                }

                $chartData['title_chart'] = str_replace( '&#8230;','...', $_wordTitle);

//                    $chartData['title_chart'] = "Apakah motif Yusril dan beberapa partai berhasrat untuk menggelar pemilu serentak di Pileg dan Pilpres 2014 karena khawatir tak lolos parliamentary treshhold?";

            foreach($val['option'] as $opt) {
            $legends[]        = ucwords($opt['name']);
            $values[]         = (isset($opt['votes'])) ? $opt['votes'] : 0;
            $option_id[]      = (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0;
            $option_value[]   = (isset($opt['name'])) ? $opt['name'] : '';
            $series_data[]    = array(
            'name'=> ucwords($opt['name']),
            'id' => (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0,
            'voted' => (isset($opt['votes'])) ? $opt['votes'] : 0
            );
            }
            if (!empty($values)) {
            $chartData['width']        = 222;
            $chartData['height']       = 250;
            $chartData['values']       = $values;
            $chartData['legends']      = $legends;
            $chartData['show_legend']  = 'true';
            $chartData['question_id']  = $val['question_id'];
            $chartData['survey_id']    = $survey['survey_id'];
            $chartData['option_id']    = $option_id;
            $chartData['option_value'] = $option_value;
            $chartData['series_data']  = $series_data;
            $chartData['url']          = $url .'?source=survey_list';
            $chartData['memberlogin']  = ''; // $memberlogin;
            $chartData['title'] = $survey['name'];
            $this->load->view('home/survey_chart', $chartData);
            }

            }
            ?>
	</div>
</div>
<br>
<h4 class="kanal-title kanal-title-blue">POLLING KOMUNITAS LAINNYA</h4>
<?php 
	echo $survey_more;
	
?>
