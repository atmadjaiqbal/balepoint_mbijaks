<?php
$url = base_url() .'survey/index/'. $survey['survey_id'] . "-" . urltitle($survey['name']);

?>

<div class="container">
    <?php $this->load->view('template/tpl_sub_header'); ?>
</div>
<br>
<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <div class="span8">
                <h2><?php echo $survey['name']; ?></h2>

                <h4><?php echo mdate('%d %M %Y %h:%i', strtotime($survey['begin_date'])); ?> - <?php echo mdate('%d %M %Y %h:%i', strtotime($survey['expired_date'])); ?>

                </h4>
                <div class="row-fluid">
                    <div class="span6">
                <?php
                foreach($survey['question'] as $key => $val) {
                $values = $legends = $option_id = $option_value = $series_data = array(); $i = 0;
                $chartData = array();
                    $_wordlen = strlen($val['question']);
                    $_baris = ceil($_wordlen / 40);
                    $_wordTitle = '';
                    $_y = 0;
                    for ($_i = 1; $_i <= $_baris; $_i++) {
                        $_wordTitle .= substr($val['question'], $_y, 41) . ' <br> ';
                        if($_i == 1)
                        {
                            $_y = 40 + $_i;
                        } else {
                            $_y = 40 + $_y;
                        }
                    }

                    $chartData['title_chart'] = str_replace( '&#8230;','...', $_wordTitle);

//                    $chartData['title_chart'] = "Apakah motif Yusril dan beberapa partai berhasrat untuk menggelar pemilu serentak di Pileg dan Pilpres 2014 karena khawatir tak lolos parliamentary treshhold?";

                foreach($val['option'] as $opt) {
                $legends[]        = ucwords($opt['name']);
                $values[]         = (isset($opt['votes'])) ? $opt['votes'] : 0;
                $option_id[]      = (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0;
                $option_value[]   = (isset($opt['name'])) ? $opt['name'] : '';
                $series_data[]    = array(
                'name'=> ucwords($opt['name']),
                'id' => (isset($opt['question_option_id'])) ? $opt['question_option_id'] : 0,
                'voted' => (isset($opt['votes'])) ? $opt['votes'] : 0
                );
                }
                if (!empty($values)) {
                $chartData['width']        = 222;
                $chartData['height']       = 350;
                $chartData['values']       = $values;
                $chartData['legends']      = $legends;
                $chartData['show_legend']  = 'true';
                $chartData['question_id']  = $val['question_id'];
                $chartData['survey_id']    = $survey['survey_id'];
                $chartData['option_id']    = $option_id;
                $chartData['option_value'] = $option_value;
                $chartData['series_data']  = $series_data;
                $chartData['url']          = $url .'?source=survey_list';
                $chartData['memberlogin']  = ''; // $memberlogin;
                $this->load->view('survey/survey_chart', $chartData);
                }

                }
                ?>

                    </div>
                    <div class="span6">
                        <?php $is_open = true;
                        if($survey['begin_date'] >= mysql_datetime() || $survey['expired_date'] <= mysql_datetime()){
                            $is_open = false;
                        }
                        ?>
                        <?php if($is_open){ ?>
                        <form id="form_vote" action="<?=base_url('timeline/post_vote');?>" method="post" >
                            <?php } ?>
                            <input type="hidden" name="cid" value="<?=$survey['content_id'];?>">
                            <input type="hidden" name="survey_id" value="<?=$survey['survey_id'];?>">
                        <p><?=$survey['desc'];?></p>
                        <?php foreach($survey['question'] as $question){?>
                            <p><?=$question['question'];?></p>
                            <input type="hidden" name="question_id" value="<?=$question['question_id'];?>">
                            <?php foreach($question['option'] as $option){?>

                            <label class="radio">
                                <input <?=($is_open)? '':'disabled';?> type="radio" data-question="<?=$option['question_id'];?>" data-id="<?=$option['question_option_id'];?>" data-text="<?=$option['name'];?>" name="vote" id="optionsRadios1" value="<?=$option['question_option_id'];?>" >
                                <?=$option['name'];?>
                            </label>

                            <?php } ?>
                        <?php } ?>
                        <div class="row-fluid">
                            <div class="span8 score-place score-place-full score" data-tipe='0' data-id="<?php echo $survey['content_id']; ?>" >
                            </div>
                            <div class="span4">
                            <input <?=($is_open)? 'false':'readonly=';?> type="submit" class="btn-flat btn-flat-xlarge btn-flat-red pull-right" value="Vote" >
                            </div>
                        </div>
                        <?php if($is_open){ ?>
                        </form>
                            <?php } ?>
                        <?php  if($this->session->flashdata('rcode')){ ?>

                            <div class="alert fade in">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <?=$this->session->flashdata('cid');?>
                                <?=$this->session->flashdata('survey_id');?>
                                <?=$this->session->flashdata('question_id');?>
                                <?=$this->session->flashdata('vote');?>
                                <?=$this->session->flashdata('msg');?>

                            </div>
                        <?php } ?>

                        <div class="time-line-content" data-cat="survey" data-uri="<?=$survey['content_id'];?>">
                        </div>
                    </div>
                </div>
                <br>

                <!-- KOMENTAR -->
                <div class="div-line"></div>
                <div class="row-fluid">

                    <h5>BERI KOMENTAR</h5>
                    <div class="row-fluid">
                        <div class="comment-side-left">
                            <div style="background:
                                    url('<?=(is_array($this->member)) ? icon_url( $this->member['xname']) : 'icon_default.png'; ?>') no-repeat; background-position: center; background-size:40px 40px;" class="circular-comment">

                            </div>
                        </div>

                        <div class="comment-side-right">
                            <div class="row-fluid">
                                <input id='comment_type' data-id="<?=$survey['content_id'];?>" type="text" name="comment" class="media-input" >
                            </div>
                            <div class="row-fluid">
                                <div class="span12 text-right">
                                    <?php if(!is_array($this->member)){?>
    <span>Login untuk komentar</span>&nbsp;
    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Register</a>
    <a href="<?=base_url('home/login');?>" class="btn-flat btn-flat-dark">Login</a>
    <?php }elseif(is_array($this->member)){?>
                                    <a id="send_coment" class="btn-flat btn-flat-dark">Send</a>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div id="comment" data-page="1" data-id="<?=$survey['content_id'];?>" class="row-fluid comment-container">

                </div>
                <div id="load_more_place" class="row-fluid komu-follow-list komu-wall-list-bg">
                    <div class="span12 text-center">
                        <a data-page="1" data-id="<?=$survey['content_id'];?>" id="comment_load_more" class="komu-follow-list-more" href="#">LOAD MORE</a>
                    </div>
                </div>
                <div id="div_line_bottom" class="div-line"></div>
                <!-- END -->

            </div>
            <!-- suksesi terkait -->
            <div class="span4">
                <div class="survey-other-title">
                    <a style="cursor: default;">POLLING KOMUNITAS LAINNYA</a>
                </div><br/>
                <div id="survey_terkait" data-id="<?=$survey['survey_id']; ?>"></div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="sub-header-container">
        <div class="row-fluid">
            <img class="span12" src="<?php echo base_url() . 'assets/images/banner_indonesia_baik.png'; ?>">
        </div>
    </div>
</div>
<br>
    <div class="container">
        <div class="sub-header-container">
            <div class="row-fluid">

                <!-- HOT PROFILE -->
                <?php echo $hot_profile; ?>


                <!-- TERKINI -->
                <?php echo $skandal; ?>


                <!-- HOT ISSUE -->
                <?php echo $surveymini; ?>

            </div>
        </div>
    </div>
<br/>
    <div class="container-bawah">

        <div class="container">
            <div class="sub-header-container">
                <div class="row-fluid">

                    <!-- EKONOMI -->
                    <?php echo $ekonomi; ?>


                    <!-- HUKUM -->
                    <?php echo $hukum; ?>


                    <!-- PARLEMEN -->
                    <?php echo $parlemen; ?>

                </div>
            </div>
        </div>
        <br>

        <div class="container">
            <div class="sub-header-container">
                <div class="row-fluid">

                    <!-- EKONOMI -->
                    <?php echo $nasional; ?>


                    <!-- HUKUM -->
                    <?php echo $daerah; ?>


                    <!-- PARLEMEN -->
                    <?php echo $internasional; ?>

                </div>
            </div>
        </div>
        <br>
    </div>
<?php $this->load->view('template/modal/report_spam_modal'); ?>