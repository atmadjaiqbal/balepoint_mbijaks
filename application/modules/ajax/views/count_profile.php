<small class='profile_<?php echo $page_id; ?>'>

   <span class='friend-profile btn btn-small <?php echo ($is_friend == "yes") ? 'btn-danger' : ''; ?>' data-cb="<?php echo $callback; ?>" data-id="<?php echo $page_id; ?>" data-uri="<?php echo $friend_uri; ?>" data-redirect="<?php echo $friend_uri_redirect; ?>"  data-is_friend="<?php echo $is_friend; ?>" data-profile_name="<?php echo $profile_name; ?>" data-myname="<?php echo $username; ?>" id="btn_friend">
   
    <?php echo $friend_status; ?>
   </span>

   <span class='follow-profile btn btn-small' data-cb="<?php echo $callback; ?>" data-id="<?php echo $page_id; ?>" data-uri="<?php echo $follow_uri; ?>"  data-redirect="<?php echo $friend_uri_redirect; ?>" id="btn_follow">
    <?php echo $follow_status; ?>
   </span>
</small>

<?php
    if($show_request_friend_modal)
    {
?>

<div class="modal hide" id="modaldialog" style="width:460px;">
  <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">x</button>
	<h2 style="padding: 0; margin:0;">Kirim Friend Request</h2>
  </div>
  <div class="modal-body" style="width:411px;">
	<form>
		Input pesan permintaan berteman:<br />
		<input style="width:100%" type="text" id="prompt_text" value="Halo, saya <?php echo $username; ?>"/>
		<div> <span class="pull-right">
			<a class="btn" id="dialog_okbutton">Ok</a>
			<a class="btn" id="cancelbutton">Cancel</a>
			</span></div>
	</form>
  </div>
</div>


<?php
    }
?>
