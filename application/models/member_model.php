<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_model extends CI_Model{

	var $DB_Master;
	var $DB_SLAVE;
	public function __construct()
	{
		parent::__construct();
		$this->DB_Master = $this->load->database('master', true);
		$this->DB_SLAVE = $this->load->database('slave', true);
	}
    
    
    public function __call($method, $args) {
        
        $haystack = $method;
        $needle = '_cache_';
        $needle_plus = '_cache';
        
        $rstrstr = substr($haystack, 0,strpos($haystack, $needle));
        
        $pos = strpos($haystack, $needle);
        
        if (is_int($pos)) {
            
            $fstrstr = substr($haystack, $pos + strlen($needle));
            
        } else {
            
            $fstrstr = $pos;
        }
        
        $params_call = explode('_',$fstrstr);
        
        if($rstrstr != '' && method_exists(__CLASS__, $rstrstr) == true)
        {
        
            $method_reflect = new ReflectionMethod($this, $rstrstr);
            
            if(!$method_reflect->isPublic())
            {
                return false;
            }
            
            
            $ttl = (int)$params_call[0];
            
            if(is_null($ttl) || $ttl === false || $ttl === 0)
            {
                $ttl = 600;
            }
            
            $cache_config = array(
                'container' => 'File',
                'ttl' => $ttl, // Cache will expired in seconds
                'File' => array(
                    'store' => APPPATH.'cache'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.strtolower(__CLASS__),
                    'auto_clean'      => false,
                    'auto_clean_life' => 900,
                    'auto_clean_all'  => false,
                    'max_estimate_process' =>  0, // Cache process about 0 second
                    'debug'  => false
                )
            );
            
            $lib_name = 'smartcache_member_'.$ttl;
    
    		$this->load->library('Smartcache',$cache_config,$lib_name);
            
            
            $args_implode = @implode(',',$args);

	        if($args_implode == 'Array')
	        {
	            $args_implode = md5(serialize($args));
	        }
            
            if(isset($params_call[2]))
            {
                
            
                if(strtolower(@$params_call[2]) == 'key')
                {
                    $key_string = __CLASS__.'::'.$rstrstr.'::'.str_replace("_{$params_call[2]}", "", $method).'::'.$args_implode;
                    
                    
                    $key = $this->{$lib_name}->generatekey($key_string);
                    
                    return $key;
                }
                
                if(strtolower(@$params_call[2]) == 'delete')
                {
                     $this->{$lib_name}->delete(@$args[0]);
                     
                     return true;
                }
            }

            $key_string = __CLASS__.'::'.$rstrstr.'::'.$method.'::'.$args_implode;
    
            $key = $this->{$lib_name}->generatekey($key_string);

            if (($data = $this->{$lib_name}->fetch($key)) === false){
                
                $data = call_user_func_array(array(__CLASS__,$rstrstr), $args);
                    
                if(strtolower(@$params_call[1]) == 'array')
                {
                    $data = $data->result_array();
                    
                } else if(strtolower(@$params_call[1]) == 'row')
                {
                    $data = $data->row();
                    
                }
    
                $this->{$lib_name}->store($key, $data);
            }
    
            return $data;
            
        } else {
            
            return false;
        }
    }

	public function mongotest()
	{
		return $this->mongo_db->get('authors');
	}


	public function checkEmail($email)
	{
		$query = $this->DB_Master->get_where('tusr_account', array('email' => $email), 1, 0);
		return $query->num_rows();
	}

	public function saveMember($data)
	{
		$this->DB_Master->insert('tusr_account', $data);
		//if($this->DB_Master->_error_message()){
		//echo 'error ' . $this->DB_Master->_error_message();
		//}
		
		return $this->DB_Master->affected_rows();
	}

   public function updateMemberStatus($data)
   {
      $result = false;
      $sql  = "UPDATE tusr_account SET 
               account_status = ?, 
   				display_name = ?,
   				gender = ?,
   				birthday = ?,
   				fname = ?,
   				lname = ?,
   				user_id = ?
               WHERE account_id = ?";
		$query = $this->DB_Master->query($sql, array(
		   $data['account_status'] ,$data['display_name'], $data['gender'], $data['birthday'],
		   $data['fname'],$data['lname'],$data['user_id'], $data['account_id'])
		);
		      
		$result = $this->DB_Master->affected_rows();

      $sql  = "Select count(*) count_page from tobject_page where page_id = '".$data['user_id']."'";
      $page = $this->DB_Master->query($sql)->row_array();
      if ($page['count_page'] > 0) {
         $sql  = "UPDATE tobject_page SET page_name = '".$data['display_name']."' WHERE page_id = '".$data['user_id']."'";
		   $query = $this->DB_Master->query($sql);      
      } else {
         $sql  = "INSERT INTO tobject_page (page_id, page_name, profile_content_id, entry_date, entry_by, last_update_date, user_page) VALUES (
                 '".$data['user_id']."', '".$data['display_name']."',  NULL, SYSDATE(), '".$data['account_id']."',  SYSDATE(), 1)";		
		   $query = $this->DB_Master->query($sql);               
      }
      return $result;		         
   }
   
	public function updateMember($data)
	{
		$sql = "UPDATE tusr_account SET
				display_name = ?,
				gender = ?,
				birthday = ?,
				fname = ?,
				lname = ?,
				current_city = ?
				WHERE account_id = ?";

		$query = $this->DB_Master->query($sql, array($data['display_name'], $data['gender'], $data['birthday'],
										$data['fname'],$data['lname'], $data['current_city'], $data['account_id']));

		$sql = "UPDATE tobject_page SET
				about = ?,
				page_name = ?
				WHERE page_id = ?";

		$query = $this->DB_Master->query($sql, array($data['about'], $data['display_name'], $data['page_id']));

		/*$sql = "DELETE FROM tuser_affiliasi WHERE user_id = ?";
		$this->DB_Master->query($sql, $data['page_id']);
		if( isset($data['politisi_id']) && ! empty($data['politisi_id']) ) {
		for($i=0;$i<count($data['politisi_id']); $i++) {
			$sql = 'INSERT INTO tuser_affiliasi (user_id, politic_id) VALUES (?, ?)';
			$this->DB_Master->query($sql, array($data['page_id'], $data['politisi_id'][$i]));
		} }*/
		
		return $this->DB_Master->affected_rows();
	}


	public function updateLastLoginDate($data)
	{
		$sql     = "UPDATE tusr_account SET last_login_date=?  WHERE account_id = ?";
		$query   = $this->DB_Master->query($sql, array($data['last_login_date'], $data['account_id']));
	}
	
	public function moveLastLoginDate($accountID)
	{
		$sql = "UPDATE tusr_account SET prev_login_date = last_login_date WHERE account_id = '".$accountID."' ";
		$query = $this->DB_Master->query($sql);		
	}

	public function updateOrInsertAddressInfo($data)
	{
		$arr = array( $data['address_id'], $data['address'], $data['city_state'], $data['country'], $data['postalcode'], $data['phone'], $data['location'], $data['main_address'], $data['province_state'], $data['page_id']);
		$sql = "SELECT `page_id` FROM tobject_info_address WHERE `page_id` = ?";
		$q = $this->DB_Master->query($sql, $data['page_id'])->result_array();
		if(count($q) > 0) {
			$sql = 'UPDATE `tobject_info_address` SET `address_id` = ?, `address` = ?, `city_state` = ?, `country` = ?, `postalcode` = ?, `phone` = ?, `location` = ?, `main_address` = ?, `province_state` = ? WHERE `page_id` = ?';
			$this->DB_Master->query($sql, $arr);
		} else {
			$sql = 'INSERT INTO tobject_info_address (address_id, address, city_state, country, postalcode, phone, location, main_address, province_state, page_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
			$this->DB_Master->query($sql, $arr);
		}
	}

	public function getMemberByEmail($email)
	{
		$sql     = 'select ta.*, p.profile_content_id, p.page_id from tusr_account ta left join tobject_page p on p.page_id = ta.user_id  where email = ?';
		$query   = $this->DB_SLAVE->query($sql, array($email));

		return $query;
	}

	public function getMemberByAccount($account_id)
	{
		$sql = 'select ta.*, p.profile_content_id, p.page_id from tusr_account ta left join tobject_page p on p.page_id = ta.user_id where account_id = ?';
		$query = $this->DB_SLAVE->query($sql, array($account_id))->row_array();
		return $query;
	}


	public function getMemberByAccountId($account_id)
	{
		$sql = 'select ta.*, p.profile_content_id, p.page_id, t.attachment_title from tusr_account ta left join
				tobject_page p on p.page_id = ta.user_id left join tcontent_attachment t on t.content_id = p.profile_content_id
				where ta.account_id = ? and ta.account_status = 1 '; //
		//$query = $this->DB_SLAVE->get_where('tusr_account', array('email' => $email));

		$query = $this->DB_SLAVE->query($sql, array($account_id));

		return $query;

	}

	public function getMemberNotActive($account_id)
	{
		$sql = 'select ta.*, p.profile_content_id, p.page_id, t.attachment_title from tusr_account ta left join
				tobject_page p on p.page_id = ta.user_id left join tcontent_attachment t on t.content_id = p.profile_content_id
				where ta.account_id = ? '; //
		//$query = $this->DB_SLAVE->get_where('tusr_account', array('email' => $email));

		$query = $this->DB_SLAVE->query($sql, array($account_id));

		return $query;

	}

	public function updateAccountStatus($data, $email)
	{
		$this->DB_Master->where('email', $email);
		$this->DB_Master->update('tusr_account', $data);
	}

	public function getMemberProfilePictureList($account_id)
	{
		$query = $this->DB_SLAVE->query("SELECT `user_id` FROM `tusr_account` WHERE `account_id` = ?", $account_id);
		$rs = $query->result_array();

		$sql = "SELECT t1.content_id, t2.attachment_path, t2.attachment_title FROM tcontent t1
				JOIN tcontent_attachment t2 ON t1.content_id = t2.content_id
			    JOIN tobject_page t3 on t1.account_id = t3.entry_by
				WHERE t1.content_group_type = '22' AND t1.page_id = ? AND t1.page_id = t3.page_id ORDER BY t1.entry_date LIMIT 1000"; // last 1000 profile pics might be enough

		$query = $this->DB_SLAVE->query($sql, array($rs[0]['user_id']));
		return $query;

	}

	public function saveObjectPage($data)
	{
		$this->DB_Master->insert('tobject_page', $data);
	}

	public function updateObjectPage($data, $id)
	{
		$this->DB_Master->where('page_id', $id);
		$this->DB_Master->update('tobject_page', $data);
	}

	public function saveObjectInfoAddress($data)
	{
		$this->DB_Master->insert('tobject_info_address', $data);
	}

	public function getObjectInfoAddressByAccountId($account_id)
	{
		$sql = "SELECT * FROM tobject_info_address t1
				WHERE t1.page_id in (SELECT t2.page_id FROM tobject_page t2
									WHERE t2.entry_by = ?)";
		$query = $this->DB_SLAVE->query($sql, array($account_id));
		return $query;
	}

	public function savePageHasType($data)
	{
		$this->DB_Master->insert('tpage_has_types', $data);
	}

	public function savePageParent($data)
	{
		$this->DB_Master->insert('tpage_parent', $data);
	}

	public function getUserCurrentCity($account_id)
	{
		$sql = 'SELECT current_city FROM tusr_account WHERE account_id = ? LIMIT 1';
		$query = $this->DB_SLAVE->query($sql, array($account_id));
		$row = $query->row();
		return $row->current_city;
	}


	public function getBlobImage()
	{
		$sql = 'SELECT * FROM tcontent_attachment'; // WHERE content_id = \'0fa0fe1d-4151-444f-bf86-87351db29ef6\'';
		$query = $this->DB_SLAVE->query($sql);
		return $query;
	}

	public function updatecontentAttachment($id, $file_name, $path)
	{
		$data = array(
               'attachment_title' => $file_name,
               'attachment_path' => $path

            );

		$this->DB_SLAVE->where('content_id', $id);
		$this->DB_SLAVE->update('tcontent_attachment', $data);
	}

	public function getMemberFavouritePoliticians($account_id, $limit)
	{
		$sql = 'select fl.account_id, fl.page_id, p.page_name, p.profile_content_id, ta.attachment_title,
    					ifnull(ac.account_type,1) as account_type,
				    	tht.object_type_id, ifnull(pp.parent_page_id,0) as parent_page_id
						from tusr_follow fl join v_usr_politik p on fl.page_id = p.page_id
						join tpage_has_types tht on p.page_id = tht.page_id
						left join tusr_account ac on p.page_id = ac.user_id
						left join tpage_parent pp on tht.page_id = pp.page_id
						left join tcontent_attachment ta on ta.content_id = p.profile_content_id
				where
						fl.account_id = ?
						GROUP BY fl.page_id

		    	LIMIT ?';
		//ac.account_type = 1 AND
		$query = $this->DB_SLAVE->query($sql, array($account_id, $limit));
		//echo $this->DB_SLAVE->last_query();
		return $query;
	}

	public function getMemberFollowsList($account_id) {
		$sql = "SELECT A.user_id IS NOT NULL AS pty, is_friend(?,P.entry_by) AS friend, P.entry_by,
                   P.page_name, P.profile_content_id, AT.attachment_title, R.rate_val, T.page_id
            FROM     (SELECT F.account_id, F.page_id, F.entry_date
                      FROM   tusr_follow F
                      WHERE  F.account_id = ?) T
            LEFT OUTER JOIN tpage_rating R   ON  R.account_id = ? AND T.page_id = R.page_id
            LEFT JOIN  tobject_page P        ON  P.page_id = T.page_id
			LEFT JOIN tcontent_attachment AT ON AT.content_id = P.profile_content_id
            LEFT OUTER JOIN tusr_account A   ON  A.user_id = T.page_id
			WHERE A.account_status = 1
            ORDER BY pty,friend";
		$query = $this->DB_SLAVE->query($sql, array($account_id, $account_id, $account_id));
	  return $query;
	}

	public function getMemberFollowAnFollowerList($account_id, $page_id) {
		$sql = "
            SELECT
            follow_list.type,
            follow_list.user_type,
            follow_list.page_id,
            follow_list.account_id,
            IFNULL(top.page_name, follow_list.display_name) AS page_name ,
            tca.attachment_title
            FROM
            (
            	(
            	SELECT
            	'follow' AS type,
            	IF(IFNULL(tua.account_type,1) = 1, 'politisi', 'user') AS user_type,
            	tuf.page_id,
            	tua.account_id,
            	tua.display_name
            	FROM
            	tusr_follow tuf 
            	LEFT JOIN tusr_account tua ON tua.user_id = tuf.page_id
            	WHERE tuf.account_id = ? AND tua.account_status = 1
            	)
            
            	UNION ALL
            	(
            		SELECT
            		'follower' AS type,
            		IF(IFNULL(tua.account_type,1) = 1, 'politisi', 'user') AS user_type,
            		tua.user_id as page_id,
            		tuf.account_id,
            		tua.display_name
            		FROM
            		tusr_follow tuf 
            		LEFT JOIN tusr_account tua ON tua.account_id = tuf.account_id
            		WHERE tuf.page_id = ? AND tua.account_status = 1
            	)
            ) AS follow_list
            
            LEFT JOIN tobject_page top ON top.page_id = follow_list.page_id
            LEFT JOIN tcontent_attachment tca ON tca.content_id = top.profile_content_id
            
            ORDER BY follow_list.user_type, follow_list.type 
        
        ";
		$query = $this->DB_SLAVE->query($sql, array($account_id, $page_id));
	  return $query;
	}

	/*public function getMemberFollowsCount($account_id) {
	  $sql = "SELECT COUNT(*) AS rowCount FROM tusr_follow F join tusr_account T on T.account_id = F.account_id  WHERE F.account_id = '$account_id' and T.account_status = 1";
	  $result = $this->DB_SLAVE->query($sql);
    $row = $result->row();
    $rowCount = $row->rowCount;
	  return $rowCount;
	}*/

	public function getMemberFollowsCountRev($account_id, $page_id) {
    $sql = "
            SELECT COUNT(*) AS count
            FROM
            (
            SELECT
            follow_list.page_id
            FROM
            (
            	(
            	SELECT
            	tuf.page_id
            	FROM
            	tusr_follow tuf 
            	LEFT JOIN tusr_account tua ON tua.user_id = tuf.page_id
            	WHERE tuf.account_id = ? AND tua.account_status = 1
            	)
            
            	UNION ALL
            	(
            		SELECT
            		tua.user_id as page_id
            		FROM
            		tusr_follow tuf 
            		LEFT JOIN tusr_account tua ON tua.account_id = tuf.account_id
            		WHERE tuf.page_id = ? AND tua.account_status = 1
            	)
            ) AS follow_list
            
            GROUP BY follow_list.page_id
            ) as x
            ";
        $query = $this->DB_SLAVE->query($sql, array($account_id, $page_id));
	  return $query->result_array();
	}

	public function getMemberFollowsCount($account_id) {
    $sql = "SELECT
                   P.page_name
            FROM     (SELECT F.account_id, F.page_id, F.entry_date
                      FROM   tusr_follow F
                      WHERE  F.account_id = ?) T
            LEFT OUTER JOIN tpage_rating R   ON  R.account_id = ? AND T.page_id = R.page_id
            LEFT JOIN  tobject_page P        ON  P.page_id = T.page_id
			LEFT JOIN tcontent_attachment AT ON AT.content_id = P.profile_content_id
            LEFT OUTER JOIN tusr_account A   ON  A.user_id = T.page_id
			WHERE A.account_status = 1
            ";
    $query = $this->DB_SLAVE->query($sql, array($account_id, $account_id, $account_id));
	  return $query->num_rows();
	}

	public function getMemberFriendList($account_id, $limit, $offset = 0)
	{
		$sql = 'SELECT m.account_id, m.user_id, m.display_name, p.profile_content_id, t.attachment_title, x.friend_date
  					FROM tusr_account m
  				JOIN (SELECT f.friend_account_id \'account_id\', friend_date
          				FROM tusr_friend f
         			WHERE f.account_id = ?
        			UNION
        			SELECT t.account_id, friend_date
         				FROM tusr_friend t
         			WHERE t.friend_account_id = ?) x ON x.account_id = m.account_id
				LEFT JOIN tobject_page p on p.page_id = m.user_id
				LEFT JOIN tcontent_attachment t on t.content_id = p.profile_content_id
				WHERE m.account_status = 1
		    	LIMIT ? OFFSET ?';

		$query = $this->DB_SLAVE->query($sql, array($account_id, $account_id, $limit, $offset));
		return $query;
	}

	public function getMemberFriendListCount($account_id)
	{
		$sql = 'SELECT m.account_id
		FROM tusr_account m
		JOIN (SELECT f.friend_account_id \'account_id\'
		FROM tusr_friend f
		WHERE f.account_id = ?
		UNION
		SELECT t.account_id
		FROM tusr_friend t
		WHERE t.friend_account_id = ?) x ON x.account_id = m.account_id
		WHERE m.account_status = 1
		';

		$query = $this->DB_SLAVE->query($sql, array($account_id, $account_id))->result_array();
		if(count($query) > 0) {
			return count($query);
		} else {
			return 0;
		}
	}

   public function getMyFriendList($account_id, $limit, $offset = 0)
	{
	   /*
		$sql = 'SELECT m.account_id, m.user_id, m.display_name, p.profile_content_id, t.attachment_title, x.friend_date
  					FROM tusr_account m
  				JOIN (SELECT f.friend_account_id \'account_id\', friend_date FROM tusr_friend f WHERE f.account_id = ? ) x ON x.account_id = m.account_id
				LEFT JOIN tobject_page p on p.page_id = m.user_id
				LEFT JOIN tcontent_attachment t on t.content_id = p.profile_content_id
				WHERE m.account_status = 1 
		    	LIMIT ? OFFSET ? ';
      */
   $sql = 'SELECT m.account_id, m.user_id, m.display_name, p.profile_content_id, t.attachment_title, x.friend_date
  					FROM tusr_account m
  				JOIN (SELECT f.friend_account_id \'account_id\', friend_date
          				FROM tusr_friend f
         			WHERE f.account_id = ?
        			UNION
        			SELECT t.account_id, friend_date
         				FROM tusr_friend t
         			WHERE t.friend_account_id = ?) x ON x.account_id = m.account_id
				LEFT JOIN tobject_page p on p.page_id = m.user_id
				LEFT JOIN tcontent_attachment t on t.content_id = p.profile_content_id
				WHERE m.account_status = 1
		    	LIMIT ? OFFSET ?';

		$query = $this->DB_SLAVE->query($sql, array($account_id, $account_id, $limit, $offset));      
		//$query = $this->DB_SLAVE->query($sql, array($account_id, $limit, $offset));
		return $query;
	}

	public function getMyFriendListCount($account_id)
	{
	   /*
		$sql = 'SELECT count(m.account_id) count_rows
         		FROM tusr_account m 
         		JOIN (SELECT f.friend_account_id \'account_id\' FROM tusr_friend f WHERE f.account_id = ? ) x ON x.account_id = m.account_id 
         		WHERE m.account_status = 1';
      */
$sql = 'SELECT count(m.account_id) count_rows
		FROM tusr_account m
		JOIN (SELECT f.friend_account_id \'account_id\'
		FROM tusr_friend f
		WHERE f.account_id = ?
		UNION
		SELECT t.account_id
		FROM tusr_friend t
		WHERE t.friend_account_id = ?) x ON x.account_id = m.account_id
		WHERE m.account_status = 1
		';      
		$query = $this->DB_SLAVE->query($sql, array($account_id, $account_id))->row_array();
		if(count($query) > 0) {
			return $query['count_rows'];
		} else {
			return 0;
		}
	}

	public function isAFriend($account_id, $guest_id)
	{
		$sql = "select friend_id from tusr_friend
				where (account_id = ? and friend_account_id = ?) or ((account_id = ? and friend_account_id = ?))";

		$query = $this->DB_SLAVE->query($sql, array($account_id, $guest_id, $guest_id, $account_id));
		return $query->num_rows() > 0;
	}

	public function isFriendRequested($account_id, $guest_id)
	{
		$sql = "select account_id from tusr_friend_request
				where (account_id = ? and to_account_id = ? and flag = 0)";

		$query = $this->DB_SLAVE->query($sql, array($account_id, $guest_id));
		return $query->num_rows() > 0;
	}

	public function isFolowing($account_id, $guest_id)
	{

		$sql = "select t1.page_id from tusr_follow t1
				join tobject_page t2
				on t1.page_id = t2.page_id
				where t1.account_id = ? and t2.entry_by=?";

		$query = $this->DB_SLAVE->query($sql, array($account_id, $guest_id));
		return $query->num_rows() > 0;
	}

	public function addProfilePic($input)
	{
		$query = $this->DB_SLAVE->query("SELECT `user_id` FROM `tusr_account` WHERE `account_id` = ?", $input['account_id']);
		$rs = $query->result_array();

        $rowAccount = $this->GetUserAccountByAccountID($input['account_id']);
        $SQLObjectPage = "SELECT * FROM tobject_page WHERE page_id = '".$rowAccount['user_id']."' ";
        $query = $this->DB_Master->query($SQLObjectPage);
        $rsObjectPage = $query->row_array();

        if(empty($rsObjectPage))
        {
            $this->InsertObjectPageByAccountID($rowAccount['user_id'], $rowAccount['display_name'], $input['account_id']);
            $this->InsertContentGroup($rs[0]['user_id'].'-22', $input['account_id'], '22', $rowAccount['user_id'], 'Profile Pictures');
        }
		$query1 = $this->DB_Master->query("INSERT INTO `tcontent` (`content_id`, `account_id`, `content_group_type`, `page_id`, `title`, `description`, `content_type`, `privacy`, `last_update_date`, `entry_date`, `group_content_id`) VALUES (?, ?, 22, ?, ?, ?, 'IMAGE', 0, NOW(), NOW(), ?)",
											array($input['content_id'], $input['account_id'], $rs[0]['user_id'], $input['title'], $input['description'], $rs[0]['user_id'].'-22') );

		$query2 = $this->DB_Master->query("INSERT INTO `tcontent_attachment` (`content_id`, `attachment_length`, `attachment_type`, `attachment_title`, `attachment_path`, `attachment_width`, `attachment_height`) VALUES (?, ?, ?, ?, '/public/upload/image/', ?, ?)",
											array($input['content_id'], $input["upload"]['file_size'], $input["upload"]['file_type'], $input["upload"]['file_name'], $input["upload"]['image_width'], $input["upload"]['image_height']) );

		$query1 = $this->DB_Master->query("UPDATE `tobject_page` SET `profile_content_id` = '".$input['content_id']."' WHERE `page_id` = '".$rs[0]['user_id']."'");

	}

    public function GetUserAccount($where=null)
    {
        $sql = <<<QUERY
               SELECT * FROM tusr_account {$where}
QUERY;
        $query = $this->DB_Master->query($sql);
        return $query->row_array();
    }

    public function GetUserAccountByAccountID($accountID)
    {
        $where = "WHERE account_id = '".$accountID."' ";
        return $this->GetUserAccount($where);
    }

    public function InsertObjectPageByAccountID($page_id, $page_name, $profile_content_id)
    {
        $sql = "INSERT INTO tobject_page(page_id, page_name, entry_date, entry_by, last_update_date, status,node_color)
                VALUES('".$page_id."', '".$page_name."', '".date('Y-m-d H:i:s')."', '".$profile_content_id."', '".date('Y-m-d H:i:s')."',
                '0','C4C4C4')";
        $query = $this->DB_Master->query($sql);
    }

    public function InsertContentGroup($content_id, $account_id, $group_type, $page_id, $title)
    {
        $sql = "INSERT INTO tcontent(content_id, account_id, content_group_type, page_id, title, content_type, privacy, last_update_date, entry_date)
                VALUES('".$content_id."', '".$account_id."', '".$group_type."', '".$page_id."', '".$title."', 'GROUP','2', '".date('Y-m-d H:i:s')."',
                '".date('Y-m-d H:i:s')."')";
        $query = $this->DB_Master->query($sql);
    }

	public function changeProfilePic($account_id, $content_id)
	{
		$query = $this->DB_SLAVE->query("SELECT `user_id` FROM `tusr_account` WHERE `account_id` = ?", $account_id);
		$rs = $query->result_array();

		$query1 = $this->DB_Master->query("UPDATE `tobject_page` SET `profile_content_id` = ? WHERE `page_id` = ?", array($content_id, $rs[0]['user_id']));
	}


	public function addNotification($input)
	{
			$query = $this->DB_Master->query("INSERT INTO tusr_notification SET
											notification_id = ?,
											account_id = ?,
											notification_type = ?,
											reference_id = ?,
											ref_from_account_id = ?,
											notification_date = NOW(),
											flag = 0,
											text_notification = ?",
											array($input['notification_id'], $input['account_id'],
												$input['notification_type'], $input['reference_id'],
												$input['ref_from_account_id'], $input['text_notification']));
	}

	public function markNotificationAsRead($notification_id)
	{
			$query = $this->DB_Master->query("UPDATE tusr_notification SET flag = 1, opened_date = NOW()
											WHERE notification_id = ?", array($notification_id));
	}

	public function getUsersNotification($account_id, $limit, $offset, $unreadOnly = FALSE)
	{
		$query = $this->DB_SLAVE->query("SELECT * FROM tusr_notification WHERE account_id = ?".($unreadOnly? " AND flag = 0 " : " ").
										"ORDER BY notification_date LIMIT ? OFFSET ?", array($account_id, $limit, $offset));
		return $query;
	}
    
	public function getUsersNotificationCount($account_id, $unreadOnly = FALSE)
	{
		$query = $this->DB_SLAVE->query("SELECT COUNT(notification_id) as notification_count FROM tusr_notification WHERE account_id = ".($unreadOnly? " AND flag = 0" : ""), array($account_id));
		return $query;
	}

	public function getFriendRequestDetail($friend_request_id)
	{
		$query = $this->DB_SLAVE->query("SELECT * FROM tusr_friend_request WHERE friend_request_id = ? LIMIT 1", array($friend_request_id));
		return $query;
	}

	public function addFriendRequest($account_id, $guest_account_id, $request_message)
	{
		$friend_request_id = NULL;
		$query = $this->DB_Master->query("SELECT friend_request_id FROM tusr_friend_request WHERE account_id = ? and to_account_id = ?", array($account_id, $guest_account_id));
		if($query->num_rows() == 0)
		{
			$friend_request_id = sha1( uniqid() );
			$query1 = $this->DB_Master->query("INSERT INTO tusr_friend_request SET friend_request_id = ?, flag = 0, account_id = ?, to_account_id = ?, request_msg = ?, request_date = NOW()",
										array($friend_request_id, $account_id, $guest_account_id, $request_message));
		}
		else
		{
			$friend_request_id = $query->row()->friend_request_id;
			$query1 = $this->DB_Master->query("UPDATE tusr_friend_request SET flag = 0, request_msg = ?, request_date = NOW() WHERE account_id = ? AND to_account_id = ?",
										array($request_message, $account_id, $guest_account_id));
		}
		return $friend_request_id;
	}

	public function confirmFriendRequest($account_id, $guest_account_id)
	{
		$friend_request_id = NULL;
		$query = $this->DB_Master->query("SELECT friend_request_id FROM tusr_friend_request
											WHERE account_id = ? AND to_account_id = ? LIMIT 1",
											array($guest_account_id, $account_id));

		if($query->num_rows() > 0)
		{
			$friend_request_id = $query->row()->friend_request_id;
			$query = $this->DB_Master->query("UPDATE tusr_friend_request SET flag = 1, response_date = NOW()
											WHERE friend_request_id = ?", array($friend_request_id));
		}

		$query = $this->DB_Master->query("SELECT friend_id FROM tusr_friend
				      						WHERE account_id = ? AND friend_account_id = ?",
				      						array($account_id, $guest_account_id));
		if($query->num_rows() == 0)
		{
			$query = $this->DB_Master->query("SELECT friend_id FROM tusr_friend
					      						WHERE friend_account_id = ? AND account_id = ?", array($account_id, $guest_account_id));
			if($query->num_rows() == 0)
			{
				$friend_id = sha1( uniqid() );
				$query = $this->DB_Master->query("INSERT INTO tusr_friend SET friend_id = ?, account_id = ?, friend_account_id = ?, relation_type_id = 0, friend_date = NOW()",
											array($friend_id, $account_id, $guest_account_id));
			}
		}
		return $friend_request_id;
	}

	public function denyFriendRequest($account_id, $guest_account_id)
	{
		$friend_request_id = NULL;
		$query = $this->DB_Master->query("SELECT friend_request_id FROM tusr_friend_request
											WHERE account_id = ? AND to_account_id = ? LIMIT 1", array($guest_account_id, $account_id));
		if($query->num_rows() > 0)
		{
			$friend_request_id = $query->row()->friend_request_id;
			$query = $this->DB_Master->query("UPDATE tusr_friend_request SET flag = 2, response_date = NOW()
											WHERE friend_request_id = ?", array($friend_request_id));
		}
		return $friend_request_id;
	}

	public function addFollowUser($account_id, $guest_account_id)
	{
		$query = $this->DB_Master->query("SELECT `page_id` FROM `tobject_page` WHERE `entry_by` = ?", $guest_account_id);
		$rs = $query->row_array();
        echo "<pre>";
        print_r($rs);
        echo "</pre>";

        exit;
		$page_id = $rs[0]['page_id'];

		$query1 = $this->DB_Master->query("INSERT INTO tusr_follow SET account_id = ?,  page_id = ?, entry_date = NOW() ON DUPLICATE KEY UPDATE entry_date = NOW()", array($account_id, $page_id));
	}

	public function unfriend($account_id, $user_id)
	{
		$query = $this->DB_Master->query("DELETE FROM tusr_friend
				WHERE (account_id = ? AND friend_account_id = ?)
				OR (account_id = ? AND friend_account_id = ?)",
				array($account_id, $user_id, $user_id, $account_id));
	}


	public function removeFollowUser($account_id, $guest_account_id)
	{
		$query = $this->DB_Master->query("SELECT `page_id` FROM `tobject_page` WHERE `entry_by` = ?", $guest_account_id);
		$rs = $query->result_array();
		$page_id = $rs[0]['page_id'];

		$query1 = $this->DB_Master->query("DELETE FROM tusr_follow WHERE account_id = ? AND  page_id = ?", array($account_id, $page_id));
	}

	public function getFriendRequestsFor($account_id)
	{
		$sql = "SELECT * FROM tusr_friend_request WHERE (to_account_id = ? and flag = 0)";
		$query = $this->DB_SLAVE->query($sql, array($account_id));
		return $query;
	}

	public function getFriendRequestsFrom($account_id)
	{
		$sql = "SELECT * FROM tusr_friend_request WHERE (account_id = ? and flag = 0)";
		$query = $this->DB_SLAVE->query($sql, array($account_id));
		return $query;
	}

	public function getUserDisplayName($account_id)
	{
		$sql = "select display_name from tusr_account
				where account_id = '$account_id'";

		$query = $this->DB_SLAVE->query($sql);
		return $query->num_rows() > 0? $query->row()->display_name : NULL;
	}

	public function getPageDisplayName($page_id)
	{
		$sql = "select page_name from tobject_page
				where page_id = ?";

		$query = $this->DB_SLAVE->query($sql, array($page_id));
		return $query->num_rows() > 0? $query->row()->page_name : NULL;
	}

	public function setUserPassword($account_id, $pass, $salt)
	{
		$sql = "UPDATE tusr_account SET passwd = ?, salt= ? WHERE account_id = ?";

		$query = $this->DB_Master->query($sql, array($pass, $salt, $account_id));

	}

	public function isPolitisi($page_id)
	{
		$sql = "select t1.* from tobject_page t1
				join tusr_account t2 on t1.entry_by = t2.account_id
				where (t1.user_page = 0 or t2.account_type = 1) and (t1.page_id = ?)";
		$query = $this->DB_SLAVE->query($sql, array($page_id));
		return $query->num_rows() > 0;

	}

	public function hasPasswordRequest($account_id)
	{
		$sql = "select token_passwd from tpwd_reset where account_id = ?";
		$query = $this->DB_Master->query($sql, array($account_id));
		return $query->num_rows() > 0;
	}

	public function deletePasswordRequest($account_id)
	{
		$sql = "delete from tpwd_reset where account_id = ?";
		$this->DB_Master->query($sql, array($account_id));
	}

	public function deletePasswordByToken($token_id)
	{
		$sql = "delete from tpwd_reset where token_passwd = ?";
		$this->DB_Master->query($sql, array($token_id));
	}

	public function getPasswordRequest($token_id)
	{
		$sql = "select account_id, token_passwd, HOUR(TIMEDIFF(request_date, NOW())) as hour_age from tpwd_reset where token_passwd = ?";
		$query = $this->DB_Master->query($sql, array($token_id));
		return $query;
	}

	public function setPasswordRequest($account_id, $token_id)
	{
		$sql = "insert into tpwd_reset set account_id = ?, token_passwd = ?, request_date = NOW()";
		$this->DB_Master->query($sql, array($account_id, $token_id));
	}

	public function getUserAffiliasi($user_id) {
		if(! empty($user_id)) {
			// TO GET LIST OF POLITISI & PARTAI FROM AFFILIASI USER
			$sql = "SELECT politic_id as page_id FROM `tuser_affiliasi` WHERE user_id = ?";
			$query = $this->DB_SLAVE->query($sql, array($user_id))->result_array();
			foreach($query as &$row) {
				$sql = "SELECT page_name, profile_content_id FROM `tobject_page` WHERE page_id = ?";
				$rs = $this->DB_SLAVE->query($sql, array($row['page_id']))->result_array();
				if(! empty($rs)) {
					$row['page_name'] = $rs[0]['page_name'];
					$row['profile_content_id'] = $rs[0]['profile_content_id'];
				} else {
					$row['page_name'] = $row['page_id'];
					$row['profile_content_id'] = "";
				}
			}
			return $query;
		} else {
			return array();
		}
	}

	public function getUserAffiliasiText($user_id) {
		if(! empty($user_id)) {
			$rs = $this->getUserAffiliasi($user_id);
			$str = array();
			foreach($rs as $row) {
				if($row['page_name'] != $row['page_id']) {
					$str[] = "<a href='" . base_url() . "politisi/index/" . $row['page_id'] . "' title='" . $row['page_name'] . "'>" . $row['page_name'] . "</a>";
				} else {
					$str[] = "<a title='" . $row['page_name'] . "'>" . $row['page_name'] . "</a>";
				}
			}
			return implode(", ", $str);
		} else {
			return "";
		}
	}

	public function saveInvite($arr)
	{
		$this->DB_Master->insert('temail_invite', $arr);

	}

	public function inviteIsOk($email)
	{
		$query = $this->DB_SLAVE->get_where('temail_invite', array('email_address' => $email));
		$isok = false;
		if($query->num_rows() == 0){
			$sl = $this->DB_SLAVE->get_where('tusr_account', array('email' => $email));
			if($sl->num_rows() == 0){
				$isok = true;
			}
		}
		return $isok;
	}

	public function okforsignup($email){
		$query = $this->DB_SLAVE->get_where('temail_invite', array('email_address' => $email));
		$isok = false;
		if($query->num_rows() == 1){
			$isok = true;
		}
		return $isok;
	}

}