<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model{

	var $DB_Master;
	public function __construct()
	{
		parent::__construct();
		$this->load->database('slave');
		$this->DB_Master = $this->load->database('master', true);
	}

	 public function __call($method, $args) {

		  $haystack = $method;
		  $needle = '_cache_';
		  $needle_plus = '_cache';

		  $rstrstr = substr($haystack, 0,strpos($haystack, $needle));

		  $pos = strpos($haystack, $needle);

		  if (is_int($pos)) {

				$fstrstr = substr($haystack, $pos + strlen($needle));

		  } else {

				$fstrstr = $pos;
		  }

		  $params_call = explode('_',$fstrstr);

		  if($rstrstr != '' && method_exists(__CLASS__, $rstrstr) == true)
		  {

				$method_reflect = new ReflectionMethod($this, $rstrstr);

				if(!$method_reflect->isPublic())
				{
					 return false;
				}

				$ttl = (int)$params_call[0];

				if(is_null($ttl) || $ttl === false || $ttl === 0)
				{
					 $ttl = 600;
				}

				$cache_config = array(
					 'container' => 'File',
					 'ttl' => $ttl, // Cache will expired in seconds
					 'File' => array(
						  'store' => APPPATH.'cache'.DIRECTORY_SEPARATOR.'model'.DIRECTORY_SEPARATOR.strtolower(__CLASS__),
						  'auto_clean'      => false,
						  'auto_clean_life' => 900,
						  'auto_clean_all'  => false,
						  'max_estimate_process' =>  0, // Cache process about 0 second
						  'debug'  => false
					 )
				);

				//$lib_name = 'smartcache_politik_'.$ttl;
				$lib_name = 'smartcache_'.str_replace('_model','', strtolower(__CLASS__)).'_'.$ttl;

				$this->load->library('Smartcache',$cache_config,$lib_name);


				$args_implode = @implode(',',$args);

			  if($args_implode == 'Array')
			  {
					$args_implode = md5(serialize($args));
			  }

				if(isset($params_call[2]))
				{


					 if(strtolower(@$params_call[2]) == 'key')
					 {
						  $key_string = __CLASS__.'::'.$rstrstr.'::'.str_replace("_{$params_call[2]}", "", $method).'::'.$args_implode;


						  $key = $this->{$lib_name}->generatekey($key_string);

						  return $key;
					 }

					 if(strtolower(@$params_call[2]) == 'delete')
					 {
							$this->{$lib_name}->delete(@$args[0]);

							return true;
					 }
				}

				$key_string = __CLASS__.'::'.$rstrstr.'::'.$method.'::'.$args_implode;
				$key = $this->{$lib_name}->generatekey($key_string);

				if (($data = $this->{$lib_name}->fetch($key)) === false){

					 $data = call_user_func_array(array(__CLASS__,$rstrstr), $args);

					 if(strtolower(@$params_call[1]) == 'array')
					 {
						  $data = $data->result_array();

					 } else if(strtolower(@$params_call[1]) == 'row')
					 {
						  $data = $data->row();

					 }

					 $this->{$lib_name}->store($key, $data);
				}

				return $data;

		  } else {

				return false;
		  }
	 }

	public function saveContent($data)
	{
		$this->DB_Master->insert('tcontent', $data);
	}

	public function saveContentAttachment($data)
	{
		$this->DB_Master->insert('tcontent_attachment', $data);
	}



    public function getBlogList($offset=0, $limit=4) {
        $this->load->library('session');
        $member = $this->session->userdata('member');
        $member_account_id = (isset($member['account_id'])) ? $member['account_id'] : NULL;
        $orderBy = '';$blogImg = array(); $blogNoImg = array();$blogtitle = '';

        $orderBy = ' ORDER BY entry_date desc ';
        $sql = "SELECT * FROM (
                SELECT tc.`content_id`, tc.`account_id`, tc.`page_id`, tp.page_name, ta.attachment_title as page_attachment,
                       tc.`title`, tc.`description`, tc.`entry_date`, tb.content_blog, tc.count_like
                FROM tcontent tc
                LEFT JOIN tcontent_blog tb ON tb.content_id = tc.content_id
                LEFT join tobject_page tp on tp.page_id = tc.page_id
                LEFT JOIN tcontent_attachment ta ON ta.content_id = tp.profile_content_id
                WHERE tc.content_group_type = '11' AND tc.status = '0'
                ) as bloglist GROUP BY content_id $orderBy LIMIT $offset, $limit";
        $rsblog = $this->db->query($sql)->result_array();
        foreach($rsblog as $key => $row)
        {
            $sqlfoto = "SELECT tc.content_id, tat.attachment_title FROM tcontent tc  LEFT JOIN tcontent_attachment tat ON tat.content_id = tc.content_id
                        WHERE tc.content_group_type = 59 AND tc.group_content_id = '".$row['content_id']."'";
            $rsfoto = $this->db->query($sqlfoto);
            $row['photo'] = $rsfoto->result_array();
            $value[$key] = $row;
        }

        $img = 0;
        foreach($value as $key => $val)
        {
           if($blogtitle != $val['title'])
           {
               $blogtitle = $val['title'];
               $blogImg[$img]['content_id'] = $val['content_id'];
               $blogImg[$img]['account_id'] = $val['account_id'];
               $blogImg[$img]['page_id'] = $val['page_id'];
               $blogImg[$img]['page_name'] = $val['page_name'];
               $blogImg[$img]['page_attachment'] = $val['page_attachment'];
               $blogImg[$img]['title'] = $val['title'];
               $blogImg[$img]['description'] = $val['description'];
               $blogImg[$img]['entry_date'] = $val['entry_date'];
               $blogImg[$img]['content_blog'] = $val['content_blog'];
               $blogImg[$img]['photo'] = $val['photo'];
               $img++;
           }
        }

        return $blogImg;
    }

	public function getCommentList($type='latest', $limit=4) {
		$orderBy = '';
		if ($type == 'latest')  $orderBy = ' ORDER BY tcc.entry_date desc ';

		$sql = "
					SELECT tcc.account_id, tua.user_id, tcc.content_id, tcc.entry_date, tp.page_id, tp.page_name, tca.attachment_title, IFNULL(tua.account_type, 1) account_type,
						tc.title, tc.content_group_type, tgt.content_group_name, tc.location, tct.news_id, tct.content_url,
						left(tcc.text_comment, 80) text_comment
					FROM tcontent_comment tcc
					LEFT JOIN tusr_account tua ON tcc.account_id = tua.account_id
					LEFT JOIN tcontent tc on tc.content_id = tcc.content_id
					LEFT JOIN tcontent_text tct on tc.content_id = tct.content_id
					LEFT JOIN tm_content_group_type tgt on tc.content_group_type = tgt.content_group_type
					LEFT join tobject_page tp on tp.page_id = tua.user_id
					LEFT JOIN tcontent_attachment tca ON tca.content_id = tp.profile_content_id
					WHERE tcc.status ='0' and tgt.content_group_name in ('STATUS', 'BLOG', 'NEWS', 'SCANDAL','SURVEY', 'RACE')
					$orderBy LIMIT 0,$limit  ";
		$result = $this->db->query($sql)->result_array();

		return $result;
	}


	public function getPolitisiAffiliasi($pageID, $offset=0, $limit=100){
		$sql = "SELECT vu.page_id, vu.page_name, left(vu.about,130) about
					FROM tuser_affiliasi ta
					INNER  JOIN  v_usr_politisi vu ON ta.user_id = vu.page_id
					WHERE politic_id='$pageID' Order by vu.page_name limit $offset, $limit";
		$result = $this->db->query($sql)->result_array();

		return $result;
	}

    public function getContentEvent($limit=2){
        $sql = "SELECT * FROM tcontent_event ORDER BY no_urut ASC LIMIT 5";
        return $this->db->query($sql)->result_array();
    }

    public function getNewsHotIssue($where, $offset, $limit)
    {
        $sql = "SELECT tt.news_id, content_date
                FROM tcontent tc JOIN tcontent_text tt ON tc.content_id = tt.content_id
                WHERE content_group_type = '12' AND content_type = 'TEXT' ".$where." ORDER BY tc.entry_date DESC
                LIMIT $offset, $limit";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}