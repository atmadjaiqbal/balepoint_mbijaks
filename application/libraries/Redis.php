<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CI_Redis {

    private $_ci;
    private $redis;

    public function __construct($params = array())
    {
//        parent::__construct();
//        var_dump($this);
        $this->_ci = get_instance();
        $this->_ci->load->config('redis');

        $this->redis = new Redis();

        if (isset($params['connection_group']))
        {
            // Specific connection group
            $this->config = $this->_ci->config->item('redis_' . $params['connection_group']);
        }
        elseif (is_array($this->_ci->config->item('redis_default')))
        {
            // Default connection group
            $this->config = $this->_ci->config->item('redis_default');
        }

    }

    public function connect()
    {
        $this->redis->connect($this->config['host'], $this->config['port']);
        $this->redis->auth($this->config['password']);
        return $this->redis;
    }

}