var host = '';
$(document).ready(function(){
    //Suksesi Tabs When page loads...
    $('.home-content-suksesi').each(function() {
        console.debug($(this));
        $(this).find(".nav-tabs > li:first").addClass("active"); //Activate first tab
        $(this).find(".tab-pane:first").addClass("active"); //Show first tab content

     /* --   $(this).find(".tab-content").slimScroll({height: '485px', railDraggable : true, railVisible: true, alwaysVisible: true}); --*/
        //$("#content-"+this.id).slimScroll({height: '200px'});
        //$(this).find(".tab-content").slimScroll({height: '300px'}); // make scroable

    });

    //Suksesi Tabs On Click Event
    $(".home-content-suksesi > ul.nav-tabs > li > a").click(function(e) {
        $(this).parents('.home-content-suksesi').find(".tab-pane").removeClass("active").hide(); //Remove any "active" & "hide" tab content
        $(this).parents('.home-content-suksesi').find(".nav-tabs > li").removeClass("active"); //Remove any "active" tab pange

        var tabID = this.id;
        $(".home-content-suksesi #conten-"+tabID).addClass("active").show();
        $(".home-content-suksesi #list-"+tabID).addClass("active");
        e.preventDefault();
    });

    $('#suksesiTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })

    var race_id = $('#suksesi_terkait').data('id');
//    console.debug(race_id);
    $('#suksesi_terkait').load('/suksesi/terkait/' + race_id);

})