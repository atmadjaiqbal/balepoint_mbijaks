$(function() {

    var powermap_ready = false;
    var base_uri = '';
    $('.home #hot-profile .col-1-1').height('auto');
    $('.home-hot-profile-detail').eq(0).addClass('active');
    $('#home-hot-profile-menu ul li').eq(0).addClass(function(){
        var id = $(this).data('id');
        var place = 'pm_'+id;
        var emp = $('#'+place).html();
        // console.debug(id);
        if($.trim(emp).length == 0){
            // console.debug(place);
            var uri = $('#'+place).data('uri');
            var width = '575';
            var height = '205';

            var data = '<iframe marginheight="0px" marginwidth="0px" width="575" height="205" scrolling="no" frameborder="0" src="'+uri+'?w=575&h=205&t=2">';
            data += '</iframe>';


            $('#'+place).append(data);

            // $.post(uri, {'w': width , 'h': height, 't' : '2'}, function(data){
            // 	$('#'+place).append(data);
            // });

            /*$.ajax({
             type:'post',
             url:uri,
             data:{'w': width , 'h': height, 't' : '2'},
             async : false
             }).done(function(data){
             $('#'+place).html(data);
             powermap_ready = true;
             }); */

        }

        return 'active';
    });
//    $('#home-hot-profile-marker').animate({top:65});
    $('#home-hot-profile-menu ul li').click(function() {
        // console.debug($(this).data('id'));
        idx = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        $('.home-hot-profile-detail').eq(idx).fadeIn('slow').addClass('active').siblings().removeClass('active').hide();
        ttop = $(this).position().top;
        mpos = ttop + 65; // http://i.imgur.com/zZHCqKh.gif
        $('#home-hot-profile-marker').animate({top:mpos});

        var id = $(this).data('id');
        var place = 'pm_'+id;
        var emp = $('#'+place).html();
        console.debug(id);
        if($.trim(emp).length == 0){
            // console.debug(place);
            var uri = $('#'+place).data('uri');
            var width = '575';
            var height = '205';

            var data = '<iframe marginheight="0px" marginwidth="0px" width="575" height="205" scrolling="no" frameborder="0" src="'+uri+'?w=575&h=205&t=2">';
            data += '</iframe>';


            $('#'+place).append(data);

            /*$.post(uri, {'w': width , 'h': height, 't' : '2'}, function(data){
             $('#'+place).append(data);
             });*/
            // home_map(width, height, uri, place, '2');
        }

    });

    $('.tooltip-top').tooltip();
    $('.tooltip-left').tooltip({placement:'left'});
    $('.tooltip-right').tooltip({placement:'right'});
    $('.tooltip-bottom').css('color', '#cecece;');
    $('.tooltip-bottom').tooltip({placement:'bottom'})


    //Suksesi Tabs When page loads...
    $('.home-content-suksesi').each(function() {
        $(this).find(".nav-tabs > li:first").addClass("active"); //Activate first tab
        $(this).find(".tab-pane:first").addClass("active"); //Show first tab content

       /*-- $(this).find(".tab-content").slimScroll({height: '485px', railDraggable : true, railVisible: true, alwaysVisible: true}); --*/
        //$("#content-"+this.id).slimScroll({height: '200px'});
        //$(this).find(".tab-content").slimScroll({height: '300px'}); // make scroable

    });

    //Suksesi Tabs On Click Event
    $(".home-content-suksesi > ul.nav-tabs > li > a").click(function(e) {
        $(this).parents('.home-content-suksesi').find(".tab-pane").removeClass("active").hide(); //Remove any "active" & "hide" tab content
        $(this).parents('.home-content-suksesi').find(".nav-tabs > li").removeClass("active"); //Remove any "active" tab pange

        var tabID = this.id;
        $(".home-content-suksesi #conten-"+tabID).addClass("active").show();
        $(".home-content-suksesi #list-"+tabID).addClass("active");
        e.preventDefault();
    });
    
    	// generic load more
	$('.load-more').click(function() {
		self = $(this);
		var page = $(this).attr("data-page");
		var url  =  $(this).attr("data-url") + page;

		self.parent().find('.load-more-loader').addClass('loading');
		$.ajax({url: url,cache: false}).done(function( response ) {
			 self.parent().parent().append(response)
			 self.parent().remove();
		});
	});


    bloglatest_onscroll('#bloglatest_container');
    $('#bloglatest_loadmore').click(function(e){
        e.preventDefault();
        bloglatest_onscroll('#bloglatest_container');
    });


});


function bloglatest_onscroll(holder)
{
    var the_loader = '<div id="wall_loader" class="loader"></div>';
    $(holder).append(the_loader);
    $('#wall_loader').css('visibility', 'visible');
    var page = $(holder).data('page');

    $.get(Settings.base_url+'home/BlogLatest/'+page, function(data){
        pageg = $(holder).data('page') + 1;
        $(holder).data('page', pageg);
        $(holder).append(data);

        $('#wall_loader').remove();
    });
}