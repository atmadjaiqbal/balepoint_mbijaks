    /** PROFILE PICTURE MODAL ***/
    $('#change_pp').click(function(){
        $('#pp_modal').modal({
            backdrop:'true',
            keyboard:'false'
        });

    });

    $('#pp_modal').on('shown', function(){
       $('#iframe_modal_pp').attr('src', Settings.base_url+'komunitas/profile_picture');
    });
    $('#pp_modal').on('hidden', function(){
       $('#iframe_modal_pp').attr('src', '');
    });

    $("#btnpostpp").click(function(){
        var please = '<div id="comment_loader" class="loader"></div>';
        $('#form_foto_side').prepend(please);
        $('#comment_loader').css('visibility', 'visible');
    });

    $("#ppuploadiframe").load(function () {
        iframeContents = $("#ppuploadiframe")[0].contentWindow.document.body.innerHTML;
        var data = $(iframeContents).text();
        data = eval("(" + data + ")");
        if(data.rcode == 'bad'){
            $('#form_foto_side').before(alert_msg(data.msg));
        }else{
            $('.komu-side-img').attr('src', data.badge_url);
            $('#icon_pp').attr('src', data.icon_url);
            $('#iframe_modal_pp').attr('src', Settings.base_url+'komunitas/profile_picture');
        }

        $('#form_pp')[0].reset();
        $('#comment_loader').remove();
    });


$(document).ready(function(){
    //$(".informasiDasar").prop("disabled",true);
    //$(".informasiKontak").prop("disabled",true);
})