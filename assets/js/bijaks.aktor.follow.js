var host = '';
$(function(){
    var id = $('#follower_list').data('id');
    var user = $('#follower_list').data('user');
    follow_list('#follower_list', id, user, 2, 1);

    $('#followTab a').click(function (e) {
        e.preventDefault();
        var target = e.target;
        var target_id = $(target).attr('href');
//        console.debug(target_id);
        var id = $(target).data('id');
        var user = $(target).data('user');
        var tipe = $(target).data('tipe');
        if($(target_id+ '_list').is(':empty')){
            follow_list(target_id + '_list', id, user, tipe, 1);
        }
        $(this).tab('show');
    });

    $('#follower_load_more').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var user = $(this).data('user');
        var tipe = $(this).data('tipe');
        var page = $(this).data('page');
        follow_list('#follower_list', id, user, tipe, page);
        $(this).data('page', page+1);
    });

})

function follow_list(holder, id, user_id, tipe, page)
{
    var foll = $(holder);
    var please = '<div id="'+tipe+'_wall_loader" class="loader"></div>'; // '<p id="wall_loader">Please wait...</p>';
    foll.append(please);
    $('#'+tipe+'_wall_loader').css('visibility', 'visible');
    var to_id = id;

    $.ajax({
        url:Settings.base_url+'/aktor/follower_list/'+user_id+'/'+to_id+'/'+tipe+'/'+page
    }).done(function(html){
            $('#'+tipe+'_wall_loader').hide('blind').remove();
            foll.append(html);
            page = foll.data('page') + 1;
            foll.data('page', page);

        });
}

