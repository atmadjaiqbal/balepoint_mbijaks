
$(function(){

    $('#komunitasTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    var komu = $('#wall_list');
    wall_list(komu);
    $('#wall_load_more').click(function(e){
        e.preventDefault();
        wall_list(komu);
    });

    $('#send_status').click(function(){
        var id = $('#val').data('id');
        var user = $('#val').data('user');
        var val = $('#val').val();
        if(val){
            var please = '<div id="comment_loader" class="loader"></div>';
            $('#wall_list').prepend(please);
            $('#comment_loader').css('visibility', 'visible');
            $.post(Settings.base_url+'/timeline/post_status/', {'id':id, 'val':val, 'user':user}, function(data){
                $('#comment_loader').remove();
                if(data.rcode == 'ok'){
                    $('#wall_list').prepend(data.msg);
                }else{
                    $('#wall_list').before(alert_msg(data.msg));
                }
                $('#val').val('');
                $('.score').each(function(a, b){
                    var id = $(b).data('id');
                    var tipe = '0';
                    if($(b).data('tipe')){
                        tipe = $(b).data('tipe');
                    }
                    $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                });
            })
        }else{
            $('#wall_list').before(alert_msg('Title harus di isi'));
        }
    });

    $("#wallphotobtnpost").click(function(){
        var please = '<div id="comment_loader" class="loader"></div>';
        $('#wall_list').prepend(please);
        $('#comment_loader').css('visibility', 'visible');
    });

    $("#wallphotouploadiframe").load(function () {
        iframeContents = $("#wallphotouploadiframe")[0].contentWindow.document.body.innerHTML;
        var data = $(iframeContents).text();
        data = eval("(" + data + ")");
        if(data.rcode == 'bad'){
            $('#wall_list').before(alert_msg(data.msg));
        }else{
            $('#wall_list').prepend(data.msg);
        }

        $('#addwallphotoform')[0].reset();
        $('#comment_loader').remove();
    });

    //SEND COMMENT
    $('#wall_list').on('click', '.send_coment', function(e){
        e.preventDefault();
        var ids = $(this).data('id');
        var tipe = $('#inpt_'+ids);

        var id = tipe.data('id');
        var val = tipe.val();

        var please = '<div id="comment_loader" class="loader"></div>';
        var cmnt_holder = $('#cmnt_'+ids);
        cmnt_holder.show();
        cmnt_holder.prepend(please);
        $('#comment_loader').css('visibility', 'visible');
        $.post(Settings.base_url+'/timeline/post_comment/', {'id':id, 'val':val}, function(data){
            $('#comment_loader').remove();
            if(data.rcode == 'ok'){
                cmnt_holder.prepend(data.msg);
                $('.score').each(function(a, b){
                    var id = $(b).data('id');
                    var tipe = '0';
                    if($(b).data('tipe')){
                        tipe = $(b).data('tipe');
                    }
                    $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                });
            }else{
                alert(data.msg);
            }
            tipe.val('');
        })

    });

    //CLICK LIKE UNLIKE
    $("#wall_list").on('click', ".score-btn-simple", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });

    $('#wall_list').on('mouseenter', '.komu-wall-header', function(e){
        e.preventDefault();
        $(this).find('.flag').show();

    }).on('mouseleave', '.komu-wall-header' ,function(e){
            $(this).find('.flag').hide();
            $(this).find('.dropdown').removeClass('open');
//            $('#wall_loader').remove();
     });

    $('#wall_list').on('click', '.flag', function(e){
        e.preventDefault();

    });

    //REPORT SPAM KLIK
    $('#wall_list').on('click', '.report-spam', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var tipe = $(this).data('tipe');
        $('#submit_report>#tipe').val(tipe);
        $('#submit_report>#cid').val(id);
        $('#modal_report').modal('show');
    });

    // REMOVE STATUS KLIK
    $('#wall_list').on('click', '.remove-status', function(e){
        e.preventDefault();
        var $this = $(this);
        var id = $(this).data('id');
        var tipe = $(this).data('tipe');
        if(confirm('yakin ingin menghapus status ini ?')){
            $.post(Settings.base_url+'timeline/delete_content', {'tipe':tipe, 'content_id':id}, function(data){
//                console.debug(data);
                if(data.rcode == 'ok'){

                    if(tipe == 'comment'){
                        var $parent = $this.parent().parent().parent().parent().parent();
//                        console.debug($parent);
                        $parent.hide('blind').remove();
                    }else if(tipe == 'content'){
                        var $parent = $this.parent().parent().parent().parent().parent().parent().parent();
                        $parent.hide('blind').remove();
                    }
                    $('.score').each(function(a, b){
                        var id = $(b).data('id');
                        var tipe = '0';
                        if($(b).data('tipe')){
                            tipe = $(b).data('tipe');
                        }
                        $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                    });
                }
            });
        }
    });


    $('#submit_report').submit(function(){
        $('.alert').remove();
        var please = '<div id="comment_loader" class="loader"></div>';
        $('#submit_report').before(please);
        $('#comment_loader').css('visibility', 'visible');
        var dt = $(this).serialize();
        $.post(Settings.base_url+'timeline/report_spam', dt, function(data){
           if(data.rcode == 'ok'){
                $('#submit_report').before(alert_msg(data.msg));
           }else{
               var text_msg = 'Data yang di input tidak benar';

               if(typeof data.msg === "string"){
                   text_msg = data.msg;
               }

               if(data.msg.content_id){
                   text_msg += ', '+ data.msg.content_id;
               }
               if(data.msg.report_message){
                   text_msg += ', '+ data.msg.report_message;
               }
               var mksg = alert_msg(text_msg);
               $('#submit_report').before(mksg);
           }
           $('#comment_loader').remove();
        });
        return false;
    });

    $('#reste').click(function(){
        $('#modal_report').modal('hide');
    })

    $('#modal_report').on('hidden', function(){
        $('#submit_report>#tipe').val('');
        $('#submit_report>#report_message').val('');
        $('#submit_report>#cid').val('');
        $('.alert').remove();
    });

    $('#wall_list').on('mouseenter', '.comment-row', function(e){
        e.preventDefault();
//        console.debug('ok');
        $(this).find('.flag').show();

    }).on('mouseleave', '.comment-row' ,function(e){
            $(this).find('.flag').hide();
            $(this).find('.dropdown').removeClass('open');
//            $('#wall_loader').remove();
        });

    /** PROFILE PICTURE MODAL ***/
    $('#change_pp').click(function(){
        $('#pp_modal').modal('show');
    });

});

function load_coment()
{
    $.each('.comment-container-wall', function(a, b){
        console.debug(b);
        wall_list(b);
    })
}


function wall_list(komu)
{
    var please = '<div id="wall_loader" class="loader"></div>';
    komu.append(please);
    $('#wall_loader').css('visibility', 'visible');
    $.ajax({
        url: Settings.base_url+'/komunitas/wall/'+komu.data('user')+'/'+komu.data('page'),
        data: {'id':komu.data('id'), 'page':komu.data('page')},
        type: "POST"
    }).done(function(html){
            $('#wall_loader').hide('blind').remove();
            komu.append(html);
            page = komu.data('page') + 1;
            komu.data('page', page);
            var coment = $('.comment-container-wall');
            $.each(coment, function(a, b){
//                console.debug(b);
                comment_list(b);
            });

            var score = $('.score');
            wall_score(score);

        });
};

function comment_list(holder)
{

    var id = $(holder).data('id');
    var limit = $(holder).data('limit');
    var offset = $(holder).data('offset');

    var please = '<div id="comment_loader" class="loader"></div>';
    $(holder).append(please);
    $('#comment_loader').css('visibility', 'visible');

    var uri = Settings.base_url+'timeline/comment/'+ id + '/'+limit+'/'+offset;
    $.get(uri, function(data){
        if(data.rcode == 'ok'){
            someText = data.msg.replace(/(\r\n|\n|\r)/gm," ");
//            console.debug(someText.toString().trim().length);
            if(someText.toString().trim().length > 0){
                $(holder).show();
                $(holder).append(data.msg);
                $(holder).data('offset', parseInt(limit) + parseInt(offset));
            }

        }

        $('#comment_loader').hide('blind').remove();
    })

}

function wall_score(holder)
{
    $.each(holder, function(a, b){
        var id = $(b).data('id');
        var tipe = '0';
        if($(b).data('tipe')){
            tipe = $(b).data('tipe');
        }
        $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
    });

}