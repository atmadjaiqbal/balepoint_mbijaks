//var Settings.base_url = window.location.Settings.base_urlname;
//console.debug(Settings.base_url);

$(function(){

    $('#head_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#dv_next").animate(
                {"top": "-=1715px"},
                "slow");
            $(this).text('20 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#dv_next").animate(
                {"top": "+=1715px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('20 Berikutnya');
        }

    })

    $('#terkini_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#terkini_next").animate(
                {"top": "-=1030px"},
                "slow");
            $(this).text('12 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#terkini_next").animate(
                {"top": "+=1030px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('12 Berikutnya');
        }

    })

    $('#head_terkini_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#head_terkini_next").animate(
                {"top": "-=430px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#head_terkini_next").animate(
                {"top": "+=430px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('5 Berikutnya');
        }

    })

    $('#head_reses_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#head_reses_next").animate(
                {"top": "-=375px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#head_reses_next").animate(
                {"top": "+=375px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('5 Berikutnya');
        }

    })

    $('#headline_headline_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#head-headline-next").animate(
                {"top": "-=975px"},
                "slow");
            $(this).text('13 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#head-headline-next").animate(
                {"top": "+=975px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('13 Berikutnya');
        }

    })

    $('#reses_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("#reses_next").animate(
                {"top": "-=425px"},
                "slow");
            $(this).text('5 Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("#reses_next").animate(
                {"top": "+=425px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('5 Berikutnya');
        }

    })

    $('#opini_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        $(this).hide();
        $('#opini_news_prev').show()
        $("#opini_next").animate({"top": "-=500px"}, "slow");
        $(this).text('15 Berikutnya');
        $(this).data('tipe', "0");
    })

    $('#opini_news_prev').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        $(this).hide();
        $('#opini_news_next').show();
        $("#opini_next").animate({"top": "+=500px"},"slow");
        $(this).data('tipe', "1");
        $(this).text('12 Sebelumnya');
    })


    $('#komentar_news_prev').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        $(this).hide();
        $('#komentar_news_next').show();
        $("#komentar_next").animate({"top": "+=495px"}, "slow");
        $(this).data('tipe', "1");
        $(this).text('15 Sebelumnya');
    })

    $('#komentar_news_next').click(function(e){
        e.preventDefault();
        var tipe = $(this).data('tipe');
        $(this).hide();
        $('#komentar_news_prev').show();
        $("#komentar_next").animate({"top": "-=495px"},"slow");
        $(this).text('15 Berikutnya');
        $(this).data('tipe', "0");
    })

    $('.next-list').click(function(e){
        e.preventDefault();
        var holder = $(this).data('holder')
        var tipe = $(this).data('tipe');
        console.debug(tipe);
        if(parseInt(tipe) == 1){
            $("."+holder).animate(
                {"top": "-=375px"},
                "slow");
            $(this).text('List Sebelumnya');
            $(this).data('tipe', "0");
        }else{
            $("."+holder).animate(
                {"top": "+=375px"},
                "slow");
            $(this).data('tipe', "1");
            $(this).text('List Berikutnya');
        }

    })

    $('.time-line-content').each( function(a, b){
        var uri = $(b).data('uri');
        var cat = $(b).data('cat');
        // console.debug(uri);
        $(b).load(Settings.base_url+'timeline/last/content/'+uri+'/false/'+cat);
    });

    $('.time-line-profile').each( function(a, b){
        var uri = $(b).data('uri');
        var cat = $(b).data('cat');
        // console.debug(uri);
        $(b).load(Settings.base_url+'timeline/last/profile/'+uri+'/false/'+cat);
    });

    $('.score').each(function(a, b){
        var id = $(b).data('id');
        var tipe = '0';
        if($(b).data('tipe')){
            tipe = $(b).data('tipe');
        }
        $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
    });

    var tipe_last = $('#sub_header_activity').data('tipe');
    $('#sub_header_activity').load(Settings.base_url+'timeline/sub_header/'+tipe_last);

    $('#disclaimer').load(Settings.base_url+'timeline/disclaimer');

    $(".score").on('click', ".score-btn", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });

    //CLICK LIKE UNLIKE
    $(".score").on('click', ".score-btn-simple", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });

    comment_list('#comment');

    $('#comment_load_more').click(function(e){
        e.preventDefault();
        comment_list('#comment');
    });

    $('#send_coment').click(function(e){
        var id = $('#comment_type').data('id');
        var val = $('#comment_type').val();
        var please = '<div id="comment_loader" class="loader"></div>';
        $('#comment').prepend(please);
        $('#comment_loader').css('visibility', 'visible');
        $.post(Settings.base_url+'timeline/post_comment/', {'id':id, 'val':val}, function(data){
            $('#comment_loader').remove();
            if(data.rcode == 'ok'){
                $('#comment').prepend(data.msg);
                $('.score').each(function(a, b){
                    var id = $(b).data('id');
                    var tipe = '0';
                    if($(b).data('tipe')){
                        tipe = $(b).data('tipe');
                    }
                    $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                });
            }else{
                alert(data.msg);
            }
            $('#comment_type').val('');
        })
    });

});

function comment_list(holder)
{
    var id = $(holder).data('id');
    var page = $(holder).data('page');

    var please = '<div id="comment_loader" class="loader"></div>';
    $(holder).append(please);
    $('#comment_loader').css('visibility', 'visible');

    var uri = Settings.base_url+'timeline/content_comment/'+ id + '/'+page;
    $.get(uri, function(data){
        $(holder).append(data);
        $(holder).data('page', parseInt(page) + 1);
        $('#comment_loader').hide('blind').remove();
    })

}

function alert_msg(msg)
{
    var str = '<div class="alert alert-error">';
    str += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
    str += msg + '</div>';
    return str;
}