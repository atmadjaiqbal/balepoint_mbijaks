var host = '';
$(function(){
    id = $('#blog_container').data('user');
    tipe = $('#blog_container').data('tipe');
    page = $('#blog_container').data('page');
    blog_list('#blog_container', id, tipe, page);

    $('#blog_load_more').click(function(e){
        e.preventDefault();
        id = $('#blog_container').data('user');
        tipe = $('#blog_container').data('tipe');
        page = $('#blog_container').data('page');
        blog_list('#blog_container', id, tipe, page);
    });



    var img = $('.komu-blog-content').children('img');
//    console.debug(img);
    $.each(img, function(a, b){
        var url = $(b).attr('src');
        var url_replace = url.replace('thumb', 'large');
        $(b).attr('src', url_replace);

    });


    $("#blog_container").on('click', ".score-btn", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });

})

function blog_list(holder, id, tipe, page)
{
    var foll = $(holder);
    var please = '<div id="'+tipe+'_wall_loader" class="loader"></div>';
    foll.append(please);
    $('#'+tipe+'_wall_loader').css('visibility', 'visible');

    $.ajax({
        url:Settings.base_url+'/komunitas/blog_list/'+id+'/'+tipe+'/'+page
    }).done(function(html){
            $('#'+tipe+'_wall_loader').hide('blind').remove();
            foll.append(html);
            pageg = foll.data('page') + 1;
//            console.debug(page);
            foll.data('page', pageg);
            $('.score').each(function(a, b){
                var id = $(b).data('id');
                var tipe = '0';
                if($(b).data('tipe')){
                    tipe = $(b).data('tipe');
                }
                $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
            });
        })
}