var host = '';
$(function(){
    id = $('#album_container').data('user');
    tipe = $('#album_container').data('tipe');
    page = $('#album_container').data('page');
    photo_list('#album_container', id, tipe, page);

    $('#photo_load_more').click(function(e){
        e.preventDefault();
        id = $('#album_container').data('user');
        tipe = $('#album_container').data('tipe');
        page = $('#album_container').data('page');
        photo_list('#album_container', id, tipe, page);
    });

    $('#foto_container').on('click', '.komu-blog-title', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var user = $(this).data('user');
        $('#photo_modal').data('id', id);
        $('#photo_modal').data('user', user);
        $('#photo_modal').modal({
            backdrop:'true',
            keyboard:'false'
        });
    });


    $('#photo_modal').on('show', function(){
        var please = '<div id="modal_loader" class="loader" style="margin-top: 4px; height: 2px;"></div>';
        $('.modal-container').prepend(please);
        $('#modal_loader').css('visibility', 'visible');
    })

    $('#photo_modal').on('shown', function(){
        var id = $(this).data('id');
        var user = $(this).data('user');
        $.getJSON(Settings.base_url+'komunitas/foto_modal/'+user+'/'+id, function(data){
            $('.modal-header h3').text(data.message.title);
            $('.img-modal').attr('src', data.message.image_uri);
            $('.desc-modal').text(data.message.description);
            $('.modal-sub-container').show('blind');
            $('#modal_loader').remove();

            //comment
            $('#comment_modal').data('id', data.message.content_id);
            $('#comment_modal_type').data('id', data.message.content_id);
            //load comment
            comment_list('#comment_modal');
        })
        $('#modal_loader').remove();
    });

    $('#photo_modal').on('hidden', function(){
        $(this).data('id', '');
        $(this).data('user', '');
        $('.modal-header h3').text('');
        $('.img-modal').attr('src', '');
        $('.desc-modal').text('');
        $('.modal-sub-container').hide();

        $('#comment_modal').data('id', '');
        $('#comment_modal_type').data('id', '');

        $('#comment_modal').html('');
        $('#comment_modal').data('page', '1');
        $('#modal_loader').remove();
    })

    $('#photo_modal').on('click', '#send_coment_modal', function(){
        var id = $('#comment_modal_type').data('id');
        var val = $('#comment_modal_type').val();
        var please = '<div id="comment_loader" class="loader" style=""></div>';
        $('#comment_modal').prepend(please);
        $('#comment_loader').css('visibility', 'visible');
        $.post(Settings.base_url+'timeline/post_comment/', {'id':id, 'val':val}, function(data){
            $('#comment_loader').remove();
            if(data.rcode == 'ok'){
                $('#comment_modal').prepend(data.msg);
                $('.score').each(function(a, b){
                    var id = $(b).data('id');
                    $(b).load(Settings.base_url+'/timeline/last_score/'+id);
                });
            }
            $('#comment_modal_type').val('');
        });
    });

    $('#comment_modal').slimScroll({
        height: '350px',
        alwaysVisible: true
    });

    $('.komu-foto-modal').slimScroll({
        height: '490px',
        alwaysVisible: true
    })

    foto_list('#foto_container');
    $('#foto_load_more').click(function(){
        foto_list('#foto_container');
    });

    // UPLOAD FOTO ALBUM
    $('#tambah_foto').click(function(e){
        e.preventDefault();
        $('#form_foto').show('blind');
    });

    $('#batal_post').click(function(e){
        e.preventDefault();
        $('#form_foto').hide('blind');
    });

    $("#wallphotobtnpost").click(function(){
        var please = '<div id="comment_loader" class="loader"></div>';
        $('#foto_container').prepend(please);
        $('#comment_loader').css('visibility', 'visible');
    });

    $("#albumphotouploadiframe").load(function () {
        iframeContents = $("#albumphotouploadiframe")[0].contentWindow.document.body.innerHTML;
        var data = $(iframeContents).text();
        data = eval("(" + data + ")");
        if(data.rcode == 'bad'){
            $('#foto_container').before(alert_msg(data.msg));
        }else{
            $('#foto_container').prepend(data.msg);
        }

        $('#addalbumphotoform')[0].reset();
        $('#comment_loader').remove();
        $('#form_foto').hide('blind');
    });

});
function alert_msg(msg)
{
    var str = '<div class="alert alert-error">';
    str += '<button type="button" class="close" data-dismiss="alert">&times;</button>';
    str += msg + '</div>';
    return str;
}

function photo_list(holder, id, tipe, page)
{
    var foll = $(holder);
    var please = '<div id="'+tipe+'_wall_loader" class="loader"></div>';
    foll.append(please);
    $('#'+tipe+'_wall_loader').css('visibility', 'visible');

    $.ajax({
        url: Settings.base_url+'/komunitas/album_list/'+id+'/'+tipe+'/'+page
    }).done(function(html){
            $('#'+tipe+'_wall_loader').hide('blind').remove();
            foll.append(html);
            pageg = foll.data('page') + 1;
//            console.debug(page);
            foll.data('page', pageg);



        });
}

function foto_list(holder)
{
    var foll = $(holder);

    id = foll.data('user');
    tipe = foll.data('tipe');
    page = foll.data('page');
    cid = foll.data('cid');

    var please = '<div id="'+tipe+'_wall_loader" class="loader"></div>';
    foll.append(please);
    $('#'+tipe+'_wall_loader').css('visibility', 'visible');

    $.ajax({
        url: Settings.base_url+'/komunitas/foto_list/'+id+'/'+cid+'/'+page
    }).done(function(html){
            $('#'+tipe+'_wall_loader').hide('blind').remove();
            foll.append(html);
            pageg = foll.data('page') + 1;
//            console.debug(page);
            foll.data('page', pageg);

            $('.score').each(function(a, b){
                var id = $(b).data('id');
                var tipe = '0';
                if($(b).data('tipe')){
                    tipe = $(b).data('tipe');
                }
                $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
            });
            photo_score_click();
        });
}

function photo_score_click()
{
    $(".score").on('click', ".score-btn-simple", function(e){
        e.preventDefault();
        var thi = $(this);
//        var parent = thi.closest('.score');
        var tipe = $(this).data('tipe');
        if(tipe != 'comment'){
            var id = $(this).data('id');
            var loader = $(this).parent().parent().prev('.loader');
            $(loader).css("visibility", "visible");
            $.ajax({
                url : Settings.base_url+'timeline/content_vote',
                type : 'POST',
                data: {'id': id, 'tipe': tipe},
                dataType: "json",
                success: function(data)
                {

                    if(data.rcode == 'ok'){
//                        var exist_score = thi.children('span').text();
//                        console.debug();
                        if(tipe == 'like'){
                            thi.parent().prev().children('a').children('span').text(data.msg[0].total);
                            thi.children('span').text(data.msg[1].total);
                            thi.parent().next().children('a').children('span').text(data.msg[2].total);
                        }else{
                            thi.children('span').text(data.msg[2].total);
                            thi.parent().prev().children('a').children('span').text(data.msg[1].total);
                            thi.parent().prev().prev().children('a').children('span').text(data.msg[0].total);
                        }

                        $('.score').each(function(a, b){
                            var id = $(b).data('id');
                            var tipe = '0';
                            if($(b).data('tipe')){
                                tipe = $(b).data('tipe');
                            }
                            $(b).load(Settings.base_url+'timeline/last_score/'+id+'/false/'+tipe);
                        });

                    }else{
                        alert(data.msg)
                    }

                    $(loader).css("visibility", "hidden");
                }
            });
        }

    });
}